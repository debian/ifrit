/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggimagefactory.h"


#include "iarray.h"
#include "ifileloader.h"
#include "ierror.h"
#include "ifile.h"
#include "iimage.h"
#include "istring.h"
#include "isystem.h"

#include <vtkCharArray.h>
#include <vtkDirectory.h>
#include <vtkImageData.h>
#include <vtkPointData.h>

//
//  templates
//
#include "iarray.tlh"


bool iggImageFactory::mInitialized(false);
iggImageFactoryHelper iggImageFactory::mHelper;


namespace iggImageFactory_Private
{
	//
	//  A class that allows to order image data by name, for faster search
	//
	class ListItem
	{
		
	public:
		
		ListItem()
		{
			Name = "";
			Image = 0;
		}

		ListItem(const iString &s)
		{
			Name = s;
			Image = 0;
		}

		ListItem(const iString &s, const iImage *i)
		{
			Name = s;
			Image = i;
		}

		void operator=(const ListItem &p)
		{
			Name = p.Name;
			Image = p.Image;
		}
		
		bool operator==(const ListItem &p) const
		{
			return (Name == p.Name);
		}
		
		bool operator<(const ListItem &p) const
		{
			return (Name < p.Name);
		}
		
		iString Name;
		const iImage *Image;
	};

	class List : public iOrderedArray<ListItem>
	{

	public:

		List() : iOrderedArray<ListItem>(100)
		{
		}
	};

	List& IconList()
	{
		static List list;
		return list;
	}

	List& HelpList()
	{
		static List list;
		return list;
	}

	void CreateData(const iString &dirname, const iString &outname, const char *sel)
	{
		//
		//  Actually create the data
		//
		int n = 0;
		iImage tmp;
		iString ws, wlist;
		ws.Init(1200);  // pre-allocate to reduce the number of new allocations

		iFile f(outname);
		if(!f.Open(iFile::_Write,iFile::_Text))
		{
			IBUG_FATAL("Unable to open output file.");
		}

		f.WriteLine("//LICENSE A");
		f.WriteLine("#if 0");
		f.WriteLine("#include \"../../Work/help/iimagefactorydataempty.h\"");
		f.WriteLine("#else");
		wlist = "const iggImageFactory::ByteData data[] = {\n";

		vtkDirectory *dir = vtkDirectory::New(); IERROR_CHECK_MEMORY(dir);
		if(dir->Open(dirname.ToCharPointer()) == 0)
		{
			f.Close();
			dir->Delete();
			IBUG_FATAL("Unable to open images directory.");
		}

#ifdef I_DEBUG
		iOutput::DisplayDebugMessage("Packing images:");
#endif

		int i, j, nj, *d;
		for(i=0; i<dir->GetNumberOfFiles(); i++) if(dir->GetFile(i)[0]>=sel[0] && dir->GetFile(i)[0]<=sel[1])
		{
			if(tmp.LoadFromFile(dirname+"/"+dir->GetFile(i)))
			{
#ifdef I_DEBUG
				iOutput::DisplayDebugMessage(dir->GetFile(i));
#endif
				//
				//  Int array name
				//
				ws = dir->GetFile(i);
				ws.Replace(".","_");
				//
				//  Add list entry
				//
				wlist += "{" + iString::FromNumber(tmp.Depth()) + "," + iString::FromNumber(tmp.Width()) + "," + iString::FromNumber(tmp.Height()) + ",\"" + dir->GetFile(i) + "\"," + ws + "},\n";
				nj = (tmp.Width()*tmp.Height()*tmp.Depth()+sizeof(int)-1)/sizeof(int);
				d = (int *)tmp.DataPointer();
				ws = "static unsigned int " + ws + "[] = {0x" + iString::FromNumber(d[0],"%x");
				for(j=1; j<nj; j++)
				{
					ws += ",0x" + iString::FromNumber(d[j],"%x");
					if(j%100 == 0)
					{
						//  start a new line to avoid too long lines
						f.WriteLine(ws);
						ws.Clear();
					}
				}
				ws += "};";
				f.WriteLine(ws);
				n++;
			}
		}
		dir->Delete();

		wlist = wlist.Part(0,wlist.Length()-2) + "};";
		f.WriteLine(wlist);
		if(iSystem::IsBigEndianMachine()) f.WriteLine("const bool isBigEndianData = true;"); else f.WriteLine("const bool isBigEndianData = false;"); 
		f.WriteLine("const int num = "+iString::FromNumber(n)+";");
		f.WriteLine("#endif");
		f.Close();
	}

	void ReadData(int num, bool isBigEndianData, const iggImageFactory::ByteData *data, List &list)
	{
		iImage *tmp;

		int i;
		bool isBigEndianMachine = iSystem::IsBigEndianMachine();
		for(i=0; i<num; i++)
		{
			tmp = new iImage;
			if(isBigEndianMachine != isBigEndianData)
			{
				iFileLoader::SwapBytesRange((int *)data[i].Data,(data[i].Width*data[i].Height*data[i].Depth+sizeof(int)-1)/sizeof(int));
			}
			tmp->SetDataPointer((unsigned char *)data[i].Data,data[i].Width,data[i].Height,data[i].Depth);
			list.Add(ListItem(data[i].Name,tmp));
		}
	}

	//
	//  Load data engine
	//
	void LoadData1(List &list);
	void LoadData2(List &list);

	void LoadData()
	{
		LoadData1(IconList());
		LoadData2(HelpList());
	}
};


using namespace iggImageFactory_Private;


//
//  Helper class
//
iggImageFactoryHelper::iggImageFactoryHelper()
{
}


iggImageFactoryHelper::~iggImageFactoryHelper()
{
	while(mItems.Size() > 0) delete mItems.RemoveLast().Image;
}

	
const iImage* iggImageFactoryHelper::FindImage(const iString &name)
{
	int i;
	for(i=0; i<mItems.Size(); i++)
	{
		if(mItems[i].Name == name) return mItems[i].Image;
	}

	iImage *tmp = new iImage;
	if(tmp == 0) return 0;

	if(!tmp->LoadFromFile(name))
	{
		delete tmp;
		return 0;
	}

	mItems.Add(Item());
	mItems[mItems.MaxIndex()].Image = tmp;
	mItems[mItems.MaxIndex()].Name = name;

	return tmp;
}


//
//  Main class
//
const iImage* iggImageFactory::FindIcon(const iString &name)
{
	if(!mInitialized)
	{
		mInitialized = true;
		LoadData();
	}

	int n = IconList().Find(ListItem(name));
	if(n >= 0) return IconList()[n].Image; else return mHelper.FindImage(name);
}


const iImage* iggImageFactory::FindHelpChart(const iString &name)
{
	if(!mInitialized)
	{
		mInitialized = true;
		LoadData();
	}

	int n = HelpList().Find(ListItem(name));
	if(n >= 0) return HelpList()[n].Image; else return 0;
}

#endif
