/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A static class that provides access to pre-loaded images
//

#ifndef IGGIMAGEFACTORY_H
#define IGGIMAGEFACTORY_H


#include "iarray.h"
#include "istring.h"

class iImage;


class iggImageFactoryHelper
{

public:

	iggImageFactoryHelper();
	~iggImageFactoryHelper();

	const iImage* FindImage(const iString &name);

private:

	struct Item
	{
		iImage* Image;
		iString Name;
		Item(){ Image = 0; }
	};

	iArray<Item> mItems;
};


class iggImageFactory
{
	
public:

	struct ByteData
	{
		int Depth;
		int Width;
		int Height;
		const char* Name;
		unsigned int* Data;  // we keep data in ints as the most efficient form
	};

	static const iImage* FindIcon(const iString &name);
	static const iImage* FindHelpChart(const iString &name);

private:

	static bool mInitialized;
	static iggImageFactoryHelper mHelper;
};

#endif // IGGIMAGEFACTORY_H

