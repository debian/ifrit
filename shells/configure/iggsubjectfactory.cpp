/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)

#include "ierror.h"
#include "ishell.h"
#include "istring.h"
#include "iviewmodule.h"

#include "iggsubjectfactory.h"

#include "iggdialog.h"
#include "iggextensionwindow.h"
#include "iggframe.h"
#include "iggmainwindow.h"
#include "iggrenderwindow.h"
#include "iggshell.h"
#include "iggwidgetarea.h"
#include "iggwidgettext.h"

//
//  Include Qt-specific headers 
//
#if ISHELL_INCLUDED(ISHELL_QT)
#include "iqtdialogsubject.h"
#include "iqtextensionwindowsubject.h"
#include "iqtframesubject.h"
#include "iqtmainwindowsubject.h"
#include "iqtmenuwindowsubject.h"
#include "iqtrenderwindowsubject.h"
#include "iqtshellsubject.h"
#include "iqtwidgetareasubject.h"
#include "iqtwidgetbuttonsubject.h"
#include "iqtwidgetcolorselectionsubject.h"
#include "iqtwidgetentrysubject.h"
#include "iqtwidgethelpbrowsersubject.h"
#include "iqtwidgetprogressbarsubject.h"
#include "iqtwidgetselectionboxsubject.h"
#include "iqtwidgettexteditorsubject.h"
#include "iqtwidgettrackballsubject.h"
#endif


const iString iggSubjectFactory::qt = "qt";
const iString iggSubjectFactory::fx = "fx";

//
//  Local macros for simplied creation
//
#define RETURN_SUBJECT0(_prefix_,_name_) \
	i##_prefix_##_name_##Subject *tmp = new i##_prefix_##_name_##Subject(owner); \
	owner->AttachSubject(tmp); \
	return tmp

#define RETURN_SUBJECT1(_prefix_,_name_,arg1) \
	i##_prefix_##_name_##Subject *tmp = new i##_prefix_##_name_##Subject(owner,arg1); \
	owner->AttachSubject(tmp); \
	return tmp

#define RETURN_SUBJECT2(_prefix_,_name_,arg1,arg2) \
	i##_prefix_##_name_##Subject *tmp = new i##_prefix_##_name_##Subject(owner,arg1,arg2); \
	owner->AttachSubject(tmp); \
	return tmp

#define RETURN_SUBJECT3(_prefix_,_name_,arg1,arg2,arg3) \
	i##_prefix_##_name_##Subject *tmp = new i##_prefix_##_name_##Subject(owner,arg1,arg2,arg3); \
	owner->AttachSubject(tmp); \
	return tmp

#define RETURN_SUBJECT4(_prefix_,_name_,arg1,arg2,arg3,arg4) \
	i##_prefix_##_name_##Subject *tmp = new i##_prefix_##_name_##Subject(owner,arg1,arg2,arg3,arg4); \
	owner->AttachSubject(tmp); \
	return tmp

#define RETURN_SUBJECT5(_prefix_,_name_,arg1,arg2,arg3,arg4,arg5) \
	i##_prefix_##_name_##Subject *tmp = new i##_prefix_##_name_##Subject(owner,arg1,arg2,arg3,arg4,arg5); \
	owner->AttachSubject(tmp); \
	return tmp

#define RETURN_SUBJECT6(_prefix_,_name_,arg1,arg2,arg3,arg4,arg5,arg6) \
	i##_prefix_##_name_##Subject *tmp = new i##_prefix_##_name_##Subject(owner,arg1,arg2,arg3,arg4,arg5,arg6); \
	owner->AttachSubject(tmp); \
	return tmp


ibgDialogSubject* iggSubjectFactory::CreateDialogSubject(iggDialog *owner, const ibgWindowSubject *base, unsigned int mode, const iImage *icon, const iString &title)
{
	if(owner->GetShell() == 0) return 0;

	if(owner->GetShell()->Type() == qt)
	{
#if ISHELL_INCLUDED(ISHELL_QT)
		RETURN_SUBJECT4(qt,Dialog,base,mode,icon,title);
#else
		return 0;
#endif
	}
	
	return 0;
}


ibgExtensionWindowSubject* iggSubjectFactory::CreateExtensionWindowSubject(iggExtensionWindow *owner)
{
	if(owner->GetShell() == 0) return 0;

	if(owner->GetShell()->Type() == qt)
	{
#if ISHELL_INCLUDED(ISHELL_QT)
		return new iqtExtensionWindowSubject(owner);
#else
		return 0;
#endif
	}
	
	return 0;
}


ibgFrameSubject* iggSubjectFactory::CreateFrameSubject(iggFrame *owner, int cols)
{
	if(owner->GetShell() == 0) return 0;

	if(owner->GetShell()->Type() == qt)
	{
#if ISHELL_INCLUDED(ISHELL_QT)
		RETURN_SUBJECT1(qt,Frame,cols);
#else
		return 0;
#endif
	}
	
	return 0;
}


ibgFrameBookSubject* iggSubjectFactory::CreateFrameBookSubject(iggFrameBook *owner, bool withFeedback)
{
	if(owner->GetShell() == 0) return 0;

	if(owner->GetShell()->Type() == qt)
	{
#if ISHELL_INCLUDED(ISHELL_QT)
		RETURN_SUBJECT1(qt,FrameBook,withFeedback);
#else
		return 0;
#endif
	}
	
	return 0;
}


ibgFrameFlipSubject* iggSubjectFactory::CreateFrameFlipSubject(iggFrameFlip *owner, bool expanding)
{
	if(owner->GetShell() == 0) return 0;

	if(owner->GetShell()->Type() == qt)
	{
#if ISHELL_INCLUDED(ISHELL_QT)
		RETURN_SUBJECT1(qt,FrameFlip,expanding);
#else
		return 0;
#endif
	}
	
	return 0;
}


ibgFrameScrollSubject* iggSubjectFactory::CreateFrameScrollSubject(iggFrameScroll *owner, bool withHor, bool withVer)
{
	if(owner->GetShell() == 0) return 0;

	if(owner->GetShell()->Type() == qt)
	{
#if ISHELL_INCLUDED(ISHELL_QT)
		RETURN_SUBJECT2(qt,FrameScroll,withHor,withVer);
#else
		return 0;
#endif
	}
	
	return 0;
}


ibgFrameSplitSubject* iggSubjectFactory::CreateFrameSplitSubject(iggFrameSplit *owner, bool hor)
{
	if(owner->GetShell() == 0) return 0;

	if(owner->GetShell()->Type() == qt)
	{
#if ISHELL_INCLUDED(ISHELL_QT)
		RETURN_SUBJECT1(qt,FrameSplit,hor);
#else
		return 0;
#endif
	}
	
	return 0;
}


ibgMainWindowSubject* iggSubjectFactory::CreateMainWindowSubject(iggMainWindow *owner, int cols)
{
	if(owner->GetShell() == 0) return 0;

	if(owner->GetShell()->Type() == qt)
	{
#if ISHELL_INCLUDED(ISHELL_QT)
		iqtMainWindowSubject *tmp = new iqtMainWindowSubject(owner);
		owner->AttachSubject(tmp,cols);
		return tmp;
#else
		return 0;
#endif
	}
	
	return 0;
}


ibgMenuWindowSubject* iggSubjectFactory::CreateMenuWindowSubject(iggMenuWindow *owner, const iImage *icon, const iString &title)
{
	if(owner->GetShell() == 0) return 0;

	if(owner->GetShell()->Type() == qt)
	{
#if ISHELL_INCLUDED(ISHELL_QT)
		return new iqtMenuWindowSubject(owner,icon,title);
#else
		return 0;
#endif
	}
	
	return 0;
}


ibgRenderWindowSubject* iggSubjectFactory::CreateRenderWindowSubject(iggRenderWindow *owner)
{
	if(owner->GetViewModule()->GetShell() == 0) return 0;

	if(owner->GetViewModule()->GetShell()->Type() == qt)
	{
#if ISHELL_INCLUDED(ISHELL_QT)
		return new iqtRenderWindowSubject(owner);
#else
		return 0;
#endif
	}
	
	return 0;
}

ibgShellSubject* iggSubjectFactory::CreateShellSubject(iggShell *owner)
{
	if(owner == 0) return 0;

	if(owner->Type() == qt)
	{
#if ISHELL_INCLUDED(ISHELL_QT)
		return new iqtShellSubject(owner);
#else
		return 0;
#endif
	}
	
	return 0;
}


ibgWidgetButtonSubject* iggSubjectFactory::CreateWidgetButtonSubject(iggWidget *owner, int type, const iString &text, int slot, int minsize)
{
	if(owner->GetShell() == 0) return 0;

	if(owner->GetShell()->Type() == qt)
	{
#if ISHELL_INCLUDED(ISHELL_QT)
		RETURN_SUBJECT4(qt,WidgetButton,type,text,slot,minsize);
#else
		return 0;
#endif
	}
	
	return 0;
}
	

ibgWidgetColorSelectionSubject* iggSubjectFactory::CreateWidgetColorSelectionSubject(iggWidget *owner)
{
	if(owner->GetShell() == 0) return 0;

	if(owner->GetShell()->Type() == qt)
	{
#if ISHELL_INCLUDED(ISHELL_QT)
		RETURN_SUBJECT0(qt,WidgetColorSelection);
#else
		return 0;
#endif
	}
	
	return 0;
}
	
ibgWidgetComboBoxSubject* iggSubjectFactory::CreateWidgetComboBoxSubject(iggWidget *owner, const iString &title, bool bold)
{
	if(owner->GetShell() == 0) return 0;

	if(owner->GetShell()->Type() == qt)
	{
#if ISHELL_INCLUDED(ISHELL_QT)
		RETURN_SUBJECT2(qt,WidgetComboBox,title,bold);
#else
		return 0;
#endif
	}
	
	return 0;
}
	

ibgWidgetDisplayAreaSubject* iggSubjectFactory::CreateWidgetDisplayAreaSubject(iggWidget *owner, const iString &text)
{
	if(owner->GetShell() == 0) return 0;

	if(owner->GetShell()->Type() == qt)
	{
#if ISHELL_INCLUDED(ISHELL_QT)
		RETURN_SUBJECT1(qt,WidgetDisplayArea,text);
#else
		return 0;
#endif
	}
	
	return 0;
}
	

ibgWidgetDrawAreaSubject* iggSubjectFactory::CreateWidgetDrawAreaSubject(iggWidgetDrawArea *owner, bool interactive)
{
	if(owner->GetShell() == 0) return 0;

	if(owner->GetShell()->Type() == qt)
	{
#if ISHELL_INCLUDED(ISHELL_QT)
		RETURN_SUBJECT1(qt,WidgetDrawArea,interactive);
#else
		return 0;
#endif
	}
	
	return 0;
}
	

ibgWidgetEntrySubject* iggSubjectFactory::CreateWidgetEntrySubject(iggWidget *owner, bool slider, int numdig, const iString &label)
{
	if(owner->GetShell() == 0) return 0;

	if(owner->GetShell()->Type() == qt)
	{
#if ISHELL_INCLUDED(ISHELL_QT)
		RETURN_SUBJECT3(qt,WidgetEntry,slider,numdig,label);
#else
		return 0;
#endif
	}
	
	return 0;
}


ibgWidgetHelpBrowserSubject* iggSubjectFactory::CreateWidgetHelpBrowserSubject(iggWidgetHelpBrowser *owner)
{
	if(owner->GetShell() == 0) return 0;

	if(owner->GetShell()->Type() == qt)
	{
#if ISHELL_INCLUDED(ISHELL_QT)
		RETURN_SUBJECT0(qt,WidgetHelpBrowser);
#else
		return 0;
#endif
	}
	
	return 0;
}


ibgWidgetMultiImageDisplayAreaSubject* iggSubjectFactory::CreateWidgetMultiImageDisplayAreaSubject(iggWidget *owner)
{
	if(owner->GetShell() == 0) return 0;

	if(owner->GetShell()->Type() == qt)
	{
#if ISHELL_INCLUDED(ISHELL_QT)
		RETURN_SUBJECT0(qt,WidgetMultiImageDisplayArea);
#else
		return 0;
#endif
	}
	
	return 0;
}
	
	
ibgWidgetProgressBarSubject* iggSubjectFactory::CreateWidgetProgressBarSubject(iggWidget *owner)
{
	if(owner->GetShell() == 0) return 0;

	if(owner->GetShell()->Type() == qt)
	{
#if ISHELL_INCLUDED(ISHELL_QT)
		RETURN_SUBJECT0(qt,WidgetProgressBar);
#else
		return 0;
#endif
	}
	
	return 0;
}


ibgWidgetRadioBoxSubject* iggSubjectFactory::CreateWidgetRadioBoxSubject(iggWidget *owner, int cols, const iString &title)
{
	if(owner->GetShell() == 0) return 0;

	if(owner->GetShell()->Type() == qt)
	{
#if ISHELL_INCLUDED(ISHELL_QT)
		RETURN_SUBJECT2(qt,WidgetRadioBox,cols,title);
#else
		return 0;
#endif
	}
	
	return 0;
}


ibgWidgetSpinBoxSubject* iggSubjectFactory::CreateWidgetSpinBoxSubject(iggWidget *owner, int min, int max, const iString &title, int step)
{
	if(owner->GetShell() == 0) return 0;

	if(owner->GetShell()->Type() == qt)
	{
#if ISHELL_INCLUDED(ISHELL_QT)
		RETURN_SUBJECT4(qt,WidgetSpinBox,min,max,title,step);
#else
		return 0;
#endif
	}
	
	return 0;
}


ibgWidgetTextEditorSubject* iggSubjectFactory::CreateWidgetTextEditorSubject(iggWidgetTextEditor *owner, unsigned int mode)
{
	if(owner->GetShell() == 0) return 0;

	if(owner->GetShell()->Type() == qt)
	{
#if ISHELL_INCLUDED(ISHELL_QT)
		RETURN_SUBJECT1(qt,WidgetTextEditor,mode);
#else
		return 0;
#endif
	}
	
	return 0;
}


ibgWidgetTrackBallSubject* iggSubjectFactory::CreateWidgetTrackBallSubject(iggWidget *owner, bool followCamera)
{
	if(owner->GetShell() == 0) return 0;

	if(owner->GetShell()->Type() == qt)
	{
#if ISHELL_INCLUDED(ISHELL_QT)
		RETURN_SUBJECT1(qt,WidgetTrackBall,followCamera);
#else
		return 0;
#endif
	}
	
	return 0;
}


const iString iggSubjectFactory::GetIncludedShells()
{ 
	iString tmp;
#if ISHELL_INCLUDED(ISHELL_QT)
	tmp += iString("Qt-based GUI shell (Qt ") + QT_VERSION_STR + ");";
#endif
	return tmp;
}

#endif

