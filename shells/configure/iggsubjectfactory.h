/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A factory that creates various toolkit-specific subjects
//

#ifndef IGGSUBJECTFACTORY_H
#define IGGSUBJECTFACTORY_H


class iImage;
class iShell;
class iString;

class iggDialog;
class iggExtensionWindow;
class iggFrame;
class iggFrameBook;
class iggFrameFlip;
class iggFrameScroll;
class iggFrameSplit;
class iggMainWindow;
class iggMenuWindow;
class iggRenderWindow;
class iggShell;
class iggWidget;
class iggWidgetDrawArea;
class iggWidgetHelpBrowser;
class iggWidgetTextEditor;

class ibgDialogSubject;
class ibgExtensionWindowSubject;
class ibgFrameSubject;
class ibgFrameBookSubject;
class ibgFrameFlipSubject;
class ibgFrameScrollSubject;
class ibgFrameSplitSubject;
class ibgMainWindowSubject;
class ibgMenuWindowSubject;
class ibgRenderWindowSubject;
class ibgShellSubject;
class ibgWidgetButtonSubject;
class ibgWidgetColorSelectionSubject;
class ibgWidgetComboBoxSubject;
class ibgWidgetDisplayAreaSubject;
class ibgWidgetDrawAreaSubject;
class ibgWidgetEntrySubject;
class ibgWidgetHelpBrowserSubject;
class ibgWidgetMultiImageDisplayAreaSubject;
class ibgWidgetProgressBarSubject;
class ibgWidgetRadioBoxSubject;
class ibgWidgetSpinBoxSubject;
class ibgWidgetTrackBallSubject;
class ibgWidgetTextEditorSubject;
class ibgWindowSubject;


class iggSubjectFactory
{
	
public:

	static ibgDialogSubject*						CreateDialogSubject(iggDialog *owner, const ibgWindowSubject *base, unsigned int mode, const iImage *icon, const iString &title);

	static ibgExtensionWindowSubject*				CreateExtensionWindowSubject(iggExtensionWindow *owner);

	static ibgFrameSubject*							CreateFrameSubject(iggFrame *owner, int cols);
	static ibgFrameBookSubject*						CreateFrameBookSubject(iggFrameBook *owner, bool withFeedback);
	static ibgFrameFlipSubject*						CreateFrameFlipSubject(iggFrameFlip *owner, bool expanding);
	static ibgFrameScrollSubject*					CreateFrameScrollSubject(iggFrameScroll *owner, bool withHor, bool withVer);
	static ibgFrameSplitSubject*					CreateFrameSplitSubject(iggFrameSplit *owner, bool hor);

	static ibgMainWindowSubject*					CreateMainWindowSubject(iggMainWindow *owner, int cols);
	static ibgMenuWindowSubject*					CreateMenuWindowSubject(iggMenuWindow *owner, const iImage *icon, const iString &title);

	static ibgRenderWindowSubject*					CreateRenderWindowSubject(iggRenderWindow *owner);

	static ibgShellSubject*							CreateShellSubject(iggShell *owner);

	static ibgWidgetButtonSubject*					CreateWidgetButtonSubject(iggWidget *owner, int type, const iString &text, int slot, int minsize = 0);
	static ibgWidgetColorSelectionSubject*			CreateWidgetColorSelectionSubject(iggWidget *owner);
	static ibgWidgetComboBoxSubject*				CreateWidgetComboBoxSubject(iggWidget *owner, const iString &title, bool bold = false);
	static ibgWidgetDisplayAreaSubject*				CreateWidgetDisplayAreaSubject(iggWidget *owner, const iString &text);
	static ibgWidgetDrawAreaSubject*				CreateWidgetDrawAreaSubject(iggWidgetDrawArea *owner, bool interactive);
	static ibgWidgetEntrySubject*					CreateWidgetEntrySubject(iggWidget *owner, bool slider, int numdig, const iString &label);
	static ibgWidgetHelpBrowserSubject*				CreateWidgetHelpBrowserSubject(iggWidgetHelpBrowser *owner);
	static ibgWidgetMultiImageDisplayAreaSubject*	CreateWidgetMultiImageDisplayAreaSubject(iggWidget *owner);
	static ibgWidgetProgressBarSubject*				CreateWidgetProgressBarSubject(iggWidget *owner);
	static ibgWidgetRadioBoxSubject*				CreateWidgetRadioBoxSubject(iggWidget *owner, int cols, const iString &title);
	static ibgWidgetSpinBoxSubject*					CreateWidgetSpinBoxSubject(iggWidget *owner, int min, int max, const iString &title, int step);
	static ibgWidgetTextEditorSubject*				CreateWidgetTextEditorSubject(iggWidgetTextEditor *owner, unsigned int mode);
	static ibgWidgetTrackBallSubject*				CreateWidgetTrackBallSubject(iggWidget *owner, bool followCamera);

	static const iString GetIncludedShells();

private:

	static const iString qt, fx;
};

#endif // IGGSUBJECTFACTORY_H
