/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggpagecrosssection.h"


#include "icamera.h"
#include "icrosssectionviewsubject.h"
#include "irendertool.h"

#include "iggframepaintingcontrols.h"
#include "igghandle.h"
#include "iggimagefactory.h"
#include "iggwidgetpropertycontrolbutton.h"
#include "iggwidgetpropertycontrolselectionbox.h"
#include "iggwidgetpropertycontrolslider.h"
#include "iggwidgetotherbutton.h"

#include "ibgwidgetbuttonsubject.h"
#include "ibgwidgetselectionboxsubject.h"

#include "iggsubjectfactory.h"

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"
#include "iggwidgetpropertycontrol.tlh"
#include "iggwidgetpropertycontrolslider.tlh"


using namespace iParameter;


namespace iggPageCrossSection_Private
{
	//
	//  Helper widgets
	//
	class DownsampleCheckBox : public iggWidget
	{
		
	public:
		
		DownsampleCheckBox(iggFrame *parent) : iggWidget(parent)
		{
			mSubject = iggSubjectFactory::CreateWidgetButtonSubject(this,ButtonType::CheckBox,"Downsample while moving",1);
			this->SetBaloonHelp("Toggles whether the cross section is downsampled while moving.","When the cross section is moving across the box, the data are being constantly resampled. If the size of the dataset is too large, resampling will take too much time, and the position slider will not feel interactive, but will be \"dragging\" behind. To avoid this, if this box is checked, IFrIT will downsample the data to maintain the interactive performance.");
		}

		bool IsDown() const
		{
			return mSubject->IsDown();
		}

	protected:

		virtual void UpdateWidgetBody()
		{
		}
		
		ibgWidgetButtonSubject *mSubject;
	};
	

	class PositionSlider : public iggWidgetPropertyControlPositionSlider
	{
		
		friend class ScanBoxButton;

	public:
		
		PositionSlider(const iggPageObject *owner, iggFrame *parent) : iggWidgetPropertyControlPositionSlider(8,"Position",owner->CreatePropertyHandle("Location"),-1,parent,RenderMode::Immediate)
		{
			mDownsampleBuddy = 0;
		}

		void SetDownsampleBuddy(DownsampleCheckBox *b)
		{
			mDownsampleBuddy = b;
		}

	protected:
		
		virtual void OnInt1Body(int i)
		{
			iggWidgetPropertyControlPositionSlider::OnInt1Body(i);

			if(mDownsampleBuddy->IsDown())
			{
				const iCrossSectionViewSubject *sub = dynamic_cast<const iCrossSectionViewSubject*>(this->Handle()->Object());
				if(sub != 0)
				{
					int oldrf = sub->SampleRate.GetValue(0);
					int newrf = iMath::Round2Int(oldrf*sqrt(30.0*sub->GetViewModule()->GetUpdateTime()));
					if(newrf > 0)
					{
						int i;
						for(i=0; i<sub->GetNumberOfInstances(); i++) sub->SampleRate.SetValue(i,newrf);
					}
				}
			}
		}

		virtual void OnVoid2Body()
		{
			const iCrossSectionViewSubject *sub = dynamic_cast<const iCrossSectionViewSubject*>(this->Handle()->Object());
			if(sub != 0)
			{
				int i;
				for(i=0; i<sub->GetNumberOfInstances(); i++) sub->SampleRate.SetValue(i,1);
			}
			iggWidgetPropertyControlPositionSlider::OnVoid2Body();
		}

		void GetRange(int &min, int &max)
		{
			mSubject->GetRange(min,max);
		}

		void Drive(int v)
		{
			mSubject->FreezeNumberOfDigits(true);
			mSubject->SetValue(v);
			this->OnInt1Body(v);
			mSubject->FreezeNumberOfDigits(false);
		}

		DownsampleCheckBox *mDownsampleBuddy;
	};


	class OrthogonalizeViewButton : public iggWidgetSimpleButton
	{
		
	public:
		
		OrthogonalizeViewButton(iggFrame *parent) : iggWidgetSimpleButton("Orthogonalize view",parent)
		{
			this->SetBaloonHelp("Re-orient the view along the principal directions","This button re-orients the view along the nearest principal directions (X, Y, or Z).");
		}

	protected:

		virtual void Execute()
		{
			this->GetShell()->GetViewModule()->GetRenderTool()->GetCamera()->OrthogonalizeView();
			this->Render();
		}
	};


	class SpecialLocationComboBox : public iggWidget
	{

	public:

		SpecialLocationComboBox(const iggPropertyHandle& ph, iggFrame *parent) : iggWidget(parent), mHandle(ph)
		{
			mSubject = iggSubjectFactory::CreateWidgetComboBoxSubject(this,"");

			this->SetBaloonHelp("Place cross-section at one of several special locations","Chose one of the special locations from the list below to place the cross-section there.");
		}

		virtual void OnInt1Body(int val)
		{
			const iCrossSectionViewSubject *sub = dynamic_cast<const iCrossSectionViewSubject*>(mHandle.Object());
			if(sub == 0)
			{
				IBUG_ERROR("Invalid cast in SpecialLocationComboBox.");
				return;
			}
			iCrossSectionViewInstance *ins = sub->GetInstance(mHandle.Index());
			if(ins == 0)
			{
				IBUG_ERROR("Invalid cast in SpecialLocationComboBox.");
				return;
			}

			ins->SetToSpecialLocation(val-1+SpecialLocation::MaxData);

			mSubject->SetValue(0);
			this->Render();
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			int i;

			mSubject->Clear();

			mSubject->InsertItem("Place cross-section at...");
			mSubject->InsertItem("  Maximum of data");
			mSubject->InsertItem("  Minimum of data");
			mSubject->InsertItem("  Picked point");
			mSubject->InsertItem("  Focal point");
			mSubject->InsertItem("  Box Center");
			
			for(i=0; i<this->GetShell()->GetViewModule()->GetNumberOfMarkers(); i++)
			{
				mSubject->InsertItem("  Marker #"+iString::FromNumber(i+1));
			}

			mSubject->SetValue(0);
		}

		const iggPropertyHandle mHandle;
		ibgWidgetComboBoxSubject *mSubject;
	};

	//
	//  Main page
	// ************************************************
	//
	class MainPage : public iggMainPage2
	{

	public:
	
		MainPage(iggPageObject *owner, iggFrameBase *parent) : iggMainPage2(owner,parent,true), mSlider(0)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

		PositionSlider* Slider() const { return mSlider; }

	protected:

		PositionSlider *mSlider;

		virtual void InsertSectionA(iggFrame *page)
		{
			//
			//  Paint with and Direction
			//
			iggFramePaintingControls *pl = new iggFramePaintingControls("Paint with...",false,mOwner->GetDataSubjectHandle(),mOwner->CreatePropertyHandle("PaintVar"),page);
			page->AddLine(pl,3);

			iggWidgetPropertyControlRadioBox *cd = new iggWidgetPropertyControlRadioBox(3,"Direction",0,mOwner->CreatePropertyHandle("Direction"),this);
			cd->InsertItem("X");
			cd->InsertItem("Y");
			cd->InsertItem("Z");
			this->AddLine(cd);

			this->AddSpace(10);

			//
			//  Position
			//
			mSlider = new PositionSlider(mOwner,page);
			cd->AddDependent(mSlider);
			DownsampleCheckBox *dc = new DownsampleCheckBox(page);
			mSlider->SetDownsampleBuddy(dc);
			this->AddLine(mSlider,3);
			this->AddLine(dc);

			this->AddSpace(2);
			SpecialLocationComboBox *sl = new SpecialLocationComboBox(mOwner->CreatePropertyHandle("Location"),page);
			sl->AddDependent(mSlider);
			this->AddLine(sl);

			this->AddLine(new OrthogonalizeViewButton(page));
		}
	};


	//
	//  Paint page
	// ************************************************
	//
	class PaintPage : public iggPaintPage2
	{

	public:

		PaintPage(iggPageObject *owner, iggFrameBase *parent) : iggPaintPage2(owner,parent,iggPaintPage2::WithShading|iggPaintPage2::WithAllInstances)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		virtual void InsertSectionA(iggFrame *page)
		{
			//
			//  Method
			//
			iggFrame *mf = new iggFrame(page,2);
			iggWidgetPropertyControlRadioBox *cm = new iggWidgetPropertyControlRadioBox(1,"Method",0,mOwner->CreatePropertyHandle("Method"),mf);
			cm->InsertItem("Polygons");
			cm->InsertItem("Textures");
			mf->AddLine(cm,new iggWidgetPropertyControlCheckBox("Interpolate data",mOwner->CreatePropertyHandle("InterpolateData"),mf));
			
			page->AddLine(mf);
			page->AddSpace(10);
		}
	};
};


using namespace iggPageCrossSection_Private;


iggPageCrossSection::iggPageCrossSection(iggFrameBase *parent) : iggPageObject("Instance",parent,"x",iggImageFactory::FindIcon("xsec.png"))
{
	//
	//  Main page
	// ************************************************
	//
	mBook->AddPage("Main",this->Icon(),new MainPage(this,mBook));

	//
	//  Paint page
	// ************************************************
	//
	mBook->AddPage("Paint",this->Icon(),new PaintPage(this,mBook));
}

#endif
