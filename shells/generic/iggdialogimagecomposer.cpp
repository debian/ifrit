/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggdialogimagecomposer.h"


#include "iimagecomposer.h"
#include "iimagecomposerwindows.h"
#include "ishell.h"
#include "iviewmodule.h"

#include "iggframe.h"
#include "iggimagefactory.h"
#include "iggwidgetarea.h"
#include "iggwidgetmisc.h"
#include "iggwidgetpropertycontrolbutton.h"
#include "iggwidgetpropertycontrolcolorselection.h"
#include "iggwidgetpropertycontrolselectionbox.h"
#include "iggwidgetpropertycontrolslider.h"
#include "iggwidgetotherbutton.h"

#include "ibgwidgetareasubject.h"
#include "ibgwidgetselectionboxsubject.h"

#include "iggsubjectfactory.h"

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"
#include "iggwidgetpropertycontrol.tlh"


using namespace iParameter;


namespace iggDialogImageComposer_Private
{
	class ImageComposerHandle : public iggObjectHandle
	{

	public:

		ImageComposerHandle(iggShell *s) : iggObjectHandle(s)
		{
		}

		virtual const iObject* Object(int mod = -1) const
		{
			return this->GetShell()->GetImageComposer();
		}
	};

	class BackgroundWindowHandle : public iggIndexHandle
	{

	public:

		BackgroundWindowHandle(iggDialogImageComposer *owner)
		{
			mOwner = owner; IASSERT(mOwner);
		}

		virtual int Index() const
		{
			return mOwner->GetActiveBackgroundWindow();
		}

	private:

		iggDialogImageComposer *mOwner;
	};

	class ForegroundWindowHandle : public iggIndexHandle
	{

	public:

		ForegroundWindowHandle(iggDialogImageComposer *owner)
		{
			mOwner = owner; IASSERT(mOwner);
		}

		virtual int Index() const
		{
			return mOwner->GetActiveForegroundWindow();
		}

	private:

		iggDialogImageComposer *mOwner;
	};

	class Area : public iggWidgetDrawArea
	{

	public:

		Area(iggDialogImageComposer *dialog, iggFrame *parent) : iggWidgetDrawArea(parent,true), mSizeFactor(0.75)
		{
			mDialog = dialog;
			mSubject->SetBackgroundColor(iColor(100,100,100));

			mZoomFrame = 0;

			mPosX = mPosY = 0;
			mIsMovingForegroundWindow = false;
			mNeedsBaloonHelp = false;
		}

		void AddZoomFrame(iggFrame *f)
		{
			mZoomFrame = f;
		}

		virtual void OnMousePress(int dx, int dy, int b)
		{
			int i, j, n, x, y;
			iImageComposer *ic = mDialog->GetImageComposer();
			iImageComposerForegroundWindow *fw;

			mIsMovingForegroundWindow = false;
			if(b!=MouseButton::LeftButton || !this->FindPosition(ic,dx,dy,x,y)) return;

			//
			//  Find what is under mouse cursor
			//
			mPosX = mOldPosX = x;
			mPosY = mOldPosY = y;

			//
			//  Is there a foreground window? Search them backwards, as the later
			//  windows are higher
			//
			n = ic->GetNumberOfForegroundWindows();
			for(i=n-1; i>=0; i--) //  later added windows are on top, so loop in the reverse order
			{
				fw = ic->GetForegroundWindow(i);

				if(x>=fw->GetPositionX() && x<fw->GetPositionX()+fw->GetImageWidth() && y>=fw->GetPositionY() && y<fw->GetPositionY()+fw->GetImageHeight())
				{
					//
					//  Found a window - make it current
					//
					mDialog->SetActiveForegroundWindow(i);
					mIsMovingForegroundWindow = true;
					mSubject->RequestPainting(DrawMode::Foreground);
					return;
				}
			}

			//
			//  No foreground window - is there a background one?
			//
			int bw = ic->GetBorderWidth();
			int tw = ic->GetTileWidth();
			int th = ic->GetTileHeight();
			if(ic->GetInnerBorder())
			{
				tw += bw;
				th += bw;
			}
			i = (x-bw)/tw;
			j = (y-bw)/th;
			if(i>=0 && i<ic->GetNumTilesX() && j>=0 && j<ic->GetNumTilesY())
			{
				mDialog->SetActiveBackgroundWindow(i+j*ic->GetNumTilesX());
				mSubject->RequestPainting();
				this->UpdateDependents();
			}
		}

		virtual void OnMouseRelease(int , int , int b)
		{
			if(b != MouseButton::LeftButton) return;

			if(mIsMovingForegroundWindow)
			{
				iImageComposer *ic = mDialog->GetImageComposer();
				iImageComposerForegroundWindow *fw = ic->GetForegroundWindow(mDialog->GetActiveForegroundWindow());
				fw->SetPosition(fw->GetPositionX()+mPosX-mOldPosX,fw->GetPositionY()+mPosY-mOldPosY);
				mIsMovingForegroundWindow = false;
				mSubject->RequestPainting(DrawMode::Foreground);
				this->UpdateDependents();
			}
		}

		virtual void OnMouseMove(int dx, int dy, int b)
		{
			if(b != MouseButton::LeftButton)
			{
				mIsMovingForegroundWindow = false;
				return;
			}

			if(mIsMovingForegroundWindow)
			{
				int x, y;
				iImageComposer *ic = mDialog->GetImageComposer();
				this->FindPosition(ic,dx,dy,x,y);
				mPosX = x;
				mPosY = y;
				mSubject->RequestPainting(DrawMode::Foreground);
				if(mZoomFrame != 0) mZoomFrame->UpdateWidget();
			}
		}

	protected:

		iggFrame *mZoomFrame;

		virtual void UpdateWidgetBody()
		{
			//
			//  Compute reduction factor
			//
			float f1 = mSizeFactor*mSubject->Width()/mDialog->GetImageComposer()->GetImageWidth(); 
			float f2 = mSizeFactor*mSubject->Height()/mDialog->GetImageComposer()->GetImageHeight(); 
			mScaleFactor = (f1 > f2) ? f2 : f1;
			if(mScaleFactor > 1.0) mScaleFactor = 1.0;

			this->iggWidgetDrawArea::UpdateWidgetBody();
		}

		virtual void DrawBackgroundBody()
		{
			static const iColor white = iColor(255,255,255);
			int i, x, y, v, n, nx, ny;
			bool ib;
			iColor c;
			iImageComposerBackgroundWindow *bw;

			//
			//  Cache a few things
			//
			iImageComposer *ic = mDialog->GetImageComposer();
			ib = ic->GetInnerBorder();
			nx = ic->GetNumTilesX();
			ny = ic->GetNumTilesY();

			//
			//  Compute reduced sizes
			//
			int drawnBorderWidth = iMath::Round2Int(mScaleFactor*ic->GetBorderWidth());
			if(ic->GetBorderWidth()>0 && drawnBorderWidth<1) drawnBorderWidth = 1;
			int drawnTileWidth = iMath::Round2Int(mScaleFactor*ic->GetTileWidth());
			int drawnTileHeight = iMath::Round2Int(mScaleFactor*ic->GetTileHeight());
			mDrawnFullWidth = nx*drawnTileWidth + 2*drawnBorderWidth;
			mDrawnFullHeight = ny*drawnTileHeight + 2*drawnBorderWidth;
			if(ib)
			{
				mDrawnFullWidth += drawnBorderWidth*(nx-1);
				mDrawnFullHeight += drawnBorderWidth*(ny-1);
			}

			mStartX = (mSubject->Width()-mDrawnFullWidth)/2;
			mStartY = (mSubject->Height()-mDrawnFullHeight)/2;

			//
			//  Draw
			//
			if(!ic->GetInnerBorder())
			{
				this->DrawWindowBase(mStartX,mStartY,mDrawnFullWidth,mDrawnFullHeight,white,drawnBorderWidth,ic->GetBorderColor());
			}

			n = nx*ny;
			for(i=0; i<n; i++)
			{
				bw = ic->GetBackgroundWindow(i);

				x = mStartX + bw->GetTileX()*drawnTileWidth;
				y = mStartY + bw->GetTileY()*drawnTileHeight;
				if(ic->GetInnerBorder())
				{
					x += bw->GetTileX()*drawnBorderWidth;
					y += bw->GetTileY()*drawnBorderWidth;
				}
				else
				{
					x += drawnBorderWidth;
					y += drawnBorderWidth;
				}
				if(bw->GetViewModule() != 0)
				{
					v = bw->GetViewModule()->GetWindowNumber();
					c = bw->GetViewModule()->GetBackgroundColor();
				}
				else
				{
					v = -1;
					c = white;
				}
				if(ic->GetInnerBorder())
				{
					this->DrawWindow(x,y,drawnTileWidth+2*drawnBorderWidth,drawnTileHeight+2*drawnBorderWidth,c,drawnBorderWidth,ic->GetBorderColor(),v,&bw->GetWallpaperImage(),i==mDialog->GetActiveBackgroundWindow());
				}
				else
				{
					this->DrawWindow(x,y,drawnTileWidth,drawnTileHeight,c,0,ic->GetBorderColor(),v,&bw->GetWallpaperImage(),i==mDialog->GetActiveBackgroundWindow());
				}
			}
		}

	
		virtual void DrawForegroundBody()
		{
			int i, x, y, w, h, v, n, bw;
			iColor bc;
			iImageComposerForegroundWindow *fw;

			//
			//  Cache a few things
			//
			iImageComposer *ic = mDialog->GetImageComposer();

			//
			//  Draw
			//
			n = ic->GetNumberOfForegroundWindows();
			int cfw = mDialog->GetActiveForegroundWindow();
			for(i=0; i<n; i++)
			{
				fw = ic->GetForegroundWindow(i);

				//
				//  Compute window parameters
				//
				if(i==cfw && mIsMovingForegroundWindow)
				{
					x = iMath::Round2Int(mScaleFactor*(fw->GetPositionX()+mPosX-mOldPosX));
					y = iMath::Round2Int(mScaleFactor*(fw->GetPositionY()+mPosY-mOldPosY));
				}
				else
				{
					x = iMath::Round2Int(mScaleFactor*fw->GetPositionX());
					y = iMath::Round2Int(mScaleFactor*fw->GetPositionY());
				}

				w = iMath::Round2Int(mScaleFactor*fw->GetImageWidth());
				h = iMath::Round2Int(mScaleFactor*fw->GetImageHeight());

				if(x < 0) x = 0;
				if(y < 0) y = 0;
				if(x+w >= mDrawnFullWidth)
				{
					x = mDrawnFullWidth - w;
				}
				if(y+h >= mDrawnFullHeight)
				{
					y = mDrawnFullHeight - h;
				}
				x += mStartX;
				y += mStartY;
				v = fw->GetViewModule()->GetWindowNumber();
				bw = 1 + int(mScaleFactor*fw->GetBorderWidth()); if(fw->GetBorderWidth() == 0) bw = 0;
				bc = fw->GetBorderColor();

				//
				//  Is there a zoom source?
				//
				if(fw->GetZoomSource() != 0)
				{
					fw->UpdateWindow();

					int zx = mStartX + iMath::Round2Int(mScaleFactor*(fw->GetZoomSource()->GetContentsX()+(fw->GetZoomX()*fw->GetZoomSource()->GetContentsWidth()-0.5*fw->GetZoomWidth())));
					int zy = mStartY + iMath::Round2Int(mScaleFactor*(fw->GetZoomSource()->GetContentsY()+(fw->GetZoomY()*fw->GetZoomSource()->GetContentsHeight()-0.5*fw->GetZoomHeight())));
					int zw = iMath::Round2Int(mScaleFactor*fw->GetZoomWidth());
					int zh = iMath::Round2Int(mScaleFactor*fw->GetZoomHeight());

					this->DrawWindowBase(zx,zy,zw,zh,iColor::Invalid(),bw,bc);

					int wx, wy;
					if(fw->GetCoordinatesForZoomLine(0,wx,wy,x+bw/2,y+bw/2,w-bw+1,h-bw+1)) mSubject->DrawLine(zx+bw/2,zy+bw/2,wx,wy,bc,bw);
					if(fw->GetCoordinatesForZoomLine(1,wx,wy,x+bw/2,y+bw/2,w-bw+1,h-bw+1)) mSubject->DrawLine(zx+zw-1-bw/2,zy+bw/2,wx,wy,bc,bw);
					if(fw->GetCoordinatesForZoomLine(2,wx,wy,x+bw/2,y+bw/2,w-bw+1,h-bw+1)) mSubject->DrawLine(zx+bw/2,zy+zh-1-bw/2,wx,wy,bc,bw);
					if(fw->GetCoordinatesForZoomLine(3,wx,wy,x+bw/2,y+bw/2,w-bw+1,h-bw+1)) mSubject->DrawLine(zx+zw-1-bw/2,zy+zh-1-bw/2,wx,wy,bc,bw);
				}

				//
				//  Draw the window itself
				//
				this->DrawWindow(x,y,w,h,fw->GetViewModule()->GetBackgroundColor(),bw,bc,v,0,i==mDialog->GetActiveForegroundWindow());
			}

			if(!ic->IsActive())
			{
				w = iMath::Round2Int(0.9*mSubject->Width());
				h = iMath::Round2Int(0.9*mSubject->Height());
				x = (mSubject->Width()-w)/2;
				y = (mSubject->Height()-h)/2;
				mSubject->DrawTextLine(x,y,x+w-1,y+h-1,"BYPASSED",iColor(200,0,0));
			}
		}

		//
		//  Helpers
		//
		void DrawWindow(int x, int y, int w, int h, const iColor &c, int b, const iColor &bc, int v, const iImage* image, bool focus)
		{
			this->DrawWindowBase(x,y,w,h,c,b,bc);

			int x1 = x + b;
			int y1 = y + b;
			int x2 = x + w - b - 1;
			int y2 = y + h - b - 1;

			if(image != 0)
			{
				mSubject->DrawImage(x1,y1,x2,y2,*image,iImage::_Both);
			}
			if(v >= 0)
			{
				mSubject->DrawTextLine(x1,y1,x2,y2,iString::FromNumber(v+1),c.Reverse());
			}
			if(focus)
			{
				int j;
				for(j=0; j<4; j++)
				{
					mSubject->DrawLine(x1+j,y1+j,x2-j,y1+j,iColor::Black(),1,true);
					mSubject->DrawLine(x1+j,y2-j,x2-j,y2-j,iColor::Black(),1,true);
					mSubject->DrawLine(x1+j,y1+j,x1+j,y2-j,iColor::Black(),1,true);
					mSubject->DrawLine(x2-j,y1+j,x2-j,y2-j,iColor::Black(),1,true);
				}
			}
		}

		void DrawWindowBase(int x, int y, int w, int h, const iColor &c, int b, const iColor &bc)
		{
			if(b > 1)
			{
				mSubject->DrawRectangle(x    ,y    ,x+b-1,y+h-1,bc,bc,0);
				mSubject->DrawRectangle(x+w-b,y    ,x+w-1,y+h-1,bc,bc,0);
				mSubject->DrawRectangle(x    ,y    ,x+w-1,y+b-1,bc,bc,0);
				mSubject->DrawRectangle(x    ,y+h-b,x+w-1,y+h-1,bc,bc,0);
				if(c.IsValid()) mSubject->DrawRectangle(x+b  ,y+b  ,x+w-b-1,y+h-b-1,bc,c,0);
			}
			else
			{
				if(c.IsValid()) mSubject->DrawRectangle(x,y,x+w-1,y+h-1,bc,c,b); else if(b == 1)
				{
					mSubject->DrawLine(x    ,y    ,x    ,y+h-1,bc,1);
					mSubject->DrawLine(x+w-1,y    ,x+w-1,y+h-1,bc,1);
					mSubject->DrawLine(x    ,y    ,x+w-1,y    ,bc,1);
					mSubject->DrawLine(x    ,y+h-1,x+w-1,y+h-1,bc,1);
				}
			}
		}

		bool FindPosition(iImageComposer *ic, int dx, int dy, int &x, int &y)
		{
			x = iMath::Round2Int((dx-mStartX)/mScaleFactor);
			y = iMath::Round2Int((dy-mStartY)/mScaleFactor);
			return x>=0 && y>=0 && x<ic->GetImageWidth() && y<ic->GetImageHeight();
		}

		const float mSizeFactor;
		float mScaleFactor;
		int mStartX, mStartY, mDrawnFullWidth, mDrawnFullHeight;
		int mPosX, mPosY, mOldPosX, mOldPosY;
		bool mIsMovingForegroundWindow;
		iggDialogImageComposer *mDialog;
	};

	//
	//  Image size label
	//
	class ImageSizeLabel : public iggWidgetTextLabel
	{

	public:

		ImageSizeLabel(iggDialogImageComposer *dialog, iggFrame *parent) : iggWidgetTextLabel("%b640 x 480",parent)
		{
			mDialog = dialog;
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			this->SetText("%b"+iString::FromNumber(mDialog->GetImageComposer()->GetImageWidth())+" x "+iString::FromNumber(mDialog->GetImageComposer()->GetImageHeight()));
			this->iggWidgetTextLabel::UpdateWidgetBody();
		}

		iggDialogImageComposer *mDialog;
	};

	//
	//  Window list combo box
	//
	class WindowListComboBox : public iggWidget
	{

	public:

		WindowListComboBox(bool bg, int comp, const iString &title, iggDialogImageComposer *dialog, iggFrame *parent) : iggWidget(parent)
		{
			mDialog = dialog;
			mComponent = (comp>0 && comp<3) ? comp : 0;
			mBackground = bg;
			mSubject = iggSubjectFactory::CreateWidgetComboBoxSubject(this,title);
			this->SetBaloonHelp("Assign a visualization window to a background tile");
		}

		~WindowListComboBox()
		{
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			int i, val = this->GetShell()->GetNumberOfViewModules();
			if(mSubject->Count() != val+1)
			{
				mSubject->Clear();
				if(mBackground || mComponent>0) mSubject->InsertItem("None"); else mSubject->InsertItem("Select...");
				for(i=0; i<val; i++) mSubject->InsertItem("Window #"+iString::FromNumber(i+1));
				mSubject->SetValue(0);
			}
			if(mBackground)
			{
				if(mDialog->GetActiveBackgroundWindow() < 0) this->Enable(false); else
				{
					this->Enable(true);
					if(mDialog->GetImageComposer()->GetBackgroundWindow(mDialog->GetActiveBackgroundWindow())->GetViewModule(mComponent) == 0)
					{
						mSubject->SetValue(0);
					}
					else
					{
						mSubject->SetValue(mDialog->GetImageComposer()->GetBackgroundWindow(mDialog->GetActiveBackgroundWindow())->GetViewModule(mComponent)->GetWindowNumber()+1);
					}
				}
			}
			else if(mComponent > 0)
			{
				if(mDialog->GetActiveForegroundWindow() < 0) this->Enable(false); else
				{
					this->Enable(true);
					if(mDialog->GetImageComposer()->GetForegroundWindow(mDialog->GetActiveForegroundWindow())->GetViewModule(mComponent) == 0)
					{
						mSubject->SetValue(0);
					}
					else
					{
						mSubject->SetValue(mDialog->GetImageComposer()->GetForegroundWindow(mDialog->GetActiveForegroundWindow())->GetViewModule(mComponent)->GetWindowNumber()+1);
					}
				}
			}
		}

		virtual void OnInt1Body(int v)
		{
			if(mBackground)
			{
				if(mDialog->GetActiveBackgroundWindow() >= 0)
				{
					if(v == 0)
					{
						mDialog->GetImageComposer()->GetBackgroundWindow(mDialog->GetActiveBackgroundWindow())->SetViewModule(0,mComponent);
					}
					else
					{
						mDialog->GetImageComposer()->GetBackgroundWindow(mDialog->GetActiveBackgroundWindow())->SetViewModule(this->GetShell()->GetViewModule(v-1),mComponent);
					}
				}
			}
			else
			{
				if(mComponent == 0)
				{
					if(v > 0)
					{
						mDialog->GetImageComposer()->AddForegroundWindow(this->GetShell()->GetViewModule(v-1));
						mDialog->SetActiveForegroundWindow(mDialog->GetImageComposer()->GetNumberOfForegroundWindows()-1);
					}
				}
				else
				{
					if(v == 0)
					{
						mDialog->GetImageComposer()->GetForegroundWindow(mDialog->GetActiveForegroundWindow())->SetViewModule(0,mComponent);
					}
					else
					{
						mDialog->GetImageComposer()->GetForegroundWindow(mDialog->GetActiveForegroundWindow())->SetViewModule(this->GetShell()->GetViewModule(v-1),mComponent);
					}
				}
			}
		}

		bool mBackground;
		int mComponent;
		ibgWidgetComboBoxSubject *mSubject;
		iggDialogImageComposer *mDialog;
	};

	//
	//  Load wallpaper
	//
	class LoadWallpaperButton : public iggWidgetSimpleButton
	{

	public:

		LoadWallpaperButton(iggDialogImageComposer *dialog, iggFrame *parent) : iggWidgetSimpleButton("Empty tile background >>",parent)
		{
			mDialog = dialog;
			this->SetBaloonHelp("Load a background image for an empty tile");
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			this->Enable(mDialog->GetActiveBackgroundWindow()>=0 && mDialog->GetImageComposer()->GetBackgroundWindow(mDialog->GetActiveBackgroundWindow())->GetViewModule() == 0);
		}

		virtual void Execute()
		{
			iString fn;
			if(mOldFileName.IsEmpty()) mOldFileName = this->GetShell()->GetEnvironment(Environment::Base);
			fn = this->GetMainWindow()->GetFileName("Open image file",mOldFileName,"Images (*.jpg *.jpeg *.pnm *.bmp *.png *.tif *.tiff)");

			if(mDialog->GetActiveBackgroundWindow()>=0 && !mDialog->GetImageComposer()->GetBackgroundWindow(mDialog->GetActiveBackgroundWindow())->LoadWallpaperImage(fn))
			{
				this->GetShell()->PopupWindow(mDialog,"Failed to load an image from file.",MessageType::Error);
			}
		}

		iString mOldFileName;
		iggDialogImageComposer *mDialog;
	};

	//
	//  remove foreground window
	//
	class RemoveWindowButton : public iggWidgetSimpleButton
	{

	public:

		RemoveWindowButton(iggDialogImageComposer *dialog, iggFrame *parent) : iggWidgetSimpleButton("Remove",parent)
		{
			mDialog = dialog;
			this->SetBaloonHelp("Remove current foreground window");
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			this->Enable(mDialog->GetImageComposer()->GetNumberOfForegroundWindows()>0);
		}

		virtual void Execute()
		{
			mDialog->GetImageComposer()->RemoveForegroundWindow(mDialog->GetActiveForegroundWindow());
			mDialog->SetActiveForegroundWindow(mDialog->GetActiveForegroundWindow());
			if(mDialog->GetImageComposer()->GetNumberOfForegroundWindows() == 0) this->Enable(false);
		}

		iggDialogImageComposer *mDialog;
	};

	//
	//  Scale discrete float spin box
	//
	class ScaleSpinBox : public iggWidgetPropertyControl<iType::Float>
	{

	public:

		ScaleSpinBox(const iggPropertyHandle &ph, iggDialogImageComposer *dialog, iggFrame *parent) : iggWidgetPropertyControl<iType::Float>(ph,parent,RenderMode::DoNotRender)
		{
			mSubject = iggSubjectFactory::CreateWidgetSpinBoxSubject(this,5,100,"",5);
			mDialog = dialog;
			this->SetBaloonHelp("Scale foreground window","Use this box to scale the image of the foreground window in the composed final image. The window size on the screen will not be changed, only the image of it will.");
		}

	protected:

		virtual void OnInt1Body(int)
		{
			this->ExecuteControl(true);
		}

		virtual void QueryValue(float &v) const
		{
			v = 0.01f*mSubject->GetValue();
		}

		virtual void UpdateValue(float v)
		{
			mSubject->SetValue(iMath::Round2Int(100.0*v));
		}

		ibgWidgetSpinBoxSubject *mSubject;
		iggDialogImageComposer *mDialog;
	};

	//
	//  Frame box that becomes disabled when there are no foreground widgets
	//
	class WindowPropertiesBox : public iggFrame
	{

	public:

		WindowPropertiesBox(iggDialogImageComposer *dialog, iggFrame *parent) : iggFrame("Current window...",parent,2)
		{
			mDialog = dialog;
		}

		virtual ~WindowPropertiesBox()
		{
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			if(mDialog->GetActiveForegroundWindow() >= 0)
			{
				this->Enable(true);
				this->iggFrame::UpdateWidgetBody();
			}
			else this->Enable(false);
		}

		iggDialogImageComposer *mDialog;
	};

	//
	//  Frame box with extra VisualModule selections for pseudo-color composition
	//
	class PseudoColorBox : public iggFrame
	{

	public:

		PseudoColorBox(bool bg, iggDialogImageComposer *dialog, iggFrame *parent) : iggFrame("Pseudo color",parent,2)
		{
			mDialog = dialog;
			mBackground = bg;
			this->AddLine(new iggWidgetTextLabel("Green channel",this),new WindowListComboBox(bg,1,"",dialog,this));
			this->AddLine(new iggWidgetTextLabel("Blue channel",this),new WindowListComboBox(bg,2,"",dialog,this));
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			if(mBackground)
			{
				this->Enable(mDialog->GetImageComposer()->GetBackgroundWindow(mDialog->GetActiveBackgroundWindow())!=0 && mDialog->GetImageComposer()->GetBackgroundWindow(mDialog->GetActiveBackgroundWindow())->GetViewModule()!=0);
			}
			else
			{
				this->Enable(mDialog->GetImageComposer()->GetForegroundWindow(mDialog->GetActiveForegroundWindow())!=0 && mDialog->GetImageComposer()->GetForegroundWindow(mDialog->GetActiveForegroundWindow())->GetViewModule()!=0);
			}
			this->iggFrame::UpdateWidgetBody();
		}

		bool mBackground;
		iggDialogImageComposer *mDialog;
	};

	//
	//  Zoom source list combo box
	//
	class ZoomSourceListComboBox : public iggWidget
	{

	public:

		ZoomSourceListComboBox(const iString &title, iggDialogImageComposer *dialog, iggFrame *parent) : iggWidget(parent)
		{
			mNumBG = mNumFG = 0;
			mDialog = dialog;
			mSubject = iggSubjectFactory::CreateWidgetComboBoxSubject(this,title);
			this->SetBaloonHelp("Attach this window to another window as a zoom");
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			int nbg = mDialog->GetImageComposer()->GetNumberOfBackgroundWindows();
			int nfg = mDialog->GetImageComposer()->GetNumberOfForegroundWindows();

			if(mNumFG!=nfg || mNumBG!=nbg)
			{
				mNumBG = nbg;
				mNumFG = nfg;
				mSubject->Clear();
				mSubject->InsertItem("None");

				int i;
				iImageComposer *c = mDialog->GetImageComposer();
				iImageComposerBackgroundWindow *wbg;
				for(i=0; i<nbg; i++)
				{
					wbg = c->GetBackgroundWindow(i);
					mSubject->InsertItem("Background ("+iString::FromNumber(wbg->GetTileX()+1)+","+iString::FromNumber(wbg->GetTileY()+1)+")");
				}
				for(i=0; i<nfg; i++)
				{
					mSubject->InsertItem("Foreground #"+iString::FromNumber(i+1));
				}
			}

			iImageComposerForegroundWindow *w = mDialog->GetImageComposer()->GetForegroundWindow(mDialog->GetActiveForegroundWindow());
			if(w==0 || w->GetZoomSource()==0)
			{
				mSubject->SetValue(0);
			}
			else
			{
				int v = w->GetZoomSource()->GetId();
				if(v < 0) mSubject->SetValue(-v); else mSubject->SetValue(v+mNumBG);
			}
		}

		virtual void OnInt1Body(int v)
		{
			iImageComposerForegroundWindow *w = mDialog->GetImageComposer()->GetForegroundWindow(mDialog->GetActiveForegroundWindow());
			if(w == 0)
			{
				IBUG_WARN("Current foreground window not set.");
				return;
			}

			if(v == 0)
			{
				w->SetZoomSource(0);
			}
			else if(v <= mNumBG)
			{
				w->SetZoomSource(mDialog->GetImageComposer()->GetBackgroundWindow(v-1));
			}
			else
			{
				w->SetZoomSource(mDialog->GetImageComposer()->GetForegroundWindow(v-mNumBG-1));
				this->UpdateWidget();
			}
		}

		int mNumBG, mNumFG;
		ibgWidgetComboBoxSubject *mSubject;
		iggDialogImageComposer *mDialog;
	};

	//
	//  zoom widgets frame
	//
	class ZoomWidgetsFrame : public iggFrame
	{

	public:

		ZoomWidgetsFrame(iggDialogImageComposer *dialog, iggFrame *parent) : iggFrame(parent,2)
		{
			mDialog = dialog;
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			iImageComposerForegroundWindow *win = mDialog->GetImageComposer()->GetForegroundWindow(mDialog->GetActiveForegroundWindow());

			if(win!=0 && win->GetZoomSource()!=0)
			{
				win->UpdateWindow();
				this->Enable(!mDialog->GetImageComposer()->GetForegroundWindow(mDialog->GetActiveForegroundWindow())->IsAutoZoom());
				this->iggFrame::UpdateWidgetBody();
			}
		}

		iggDialogImageComposer *mDialog;
	};
};


using namespace iggDialogImageComposer_Private;


iggDialogImageComposer::iggDialogImageComposer(iggMainWindow *parent) : iggDialog(parent,0U,iggImageFactory::FindIcon("imcomp.png"),"Image Composer","sr.gg.dic",2)
{
	mActiveBackgroundWindow = -1;
	mActiveForegroundWindow = -1;
	mArea = 0;

	mSelfHandle = new ImageComposerHandle(this->GetShell()); IERROR_CHECK_MEMORY(mSelfHandle);

	mBackgroundWindowHandle = new BackgroundWindowHandle(this); IERROR_CHECK_MEMORY(mBackgroundWindowHandle);
	mForegroundWindowHandle = new ForegroundWindowHandle(this); IERROR_CHECK_MEMORY(mForegroundWindowHandle);

	if(this->ImmediateConstruction()) this->CompleteConstruction();
}


iggDialogImageComposer::~iggDialogImageComposer()
{
	delete mBackgroundWindowHandle;
	delete mForegroundWindowHandle;

	delete mSelfHandle;
}


void iggDialogImageComposer::CompleteConstructionBody()
{

	iggFrame *tmp, *tmp2;

	tmp = new iggFrame(mFrame);
	tmp2 = new iggFrame(tmp,3);
	mLabel = new ImageSizeLabel(this,tmp2);
	tmp2->AddLine(new iggWidgetTextLabel("Image size: ",tmp2),mLabel);
	tmp2->SetColStretch(0,0);
	tmp2->SetColStretch(1,0);
	tmp2->SetColStretch(2,10);
	tmp->AddLine(tmp2);

	Area *area = new Area(this,tmp);
	mArea = area;
	tmp->AddLine(mArea);
	tmp->SetRowStretch(1,10);

	iggFrameBook *book = new iggFrameBook(mFrame);

	mFrame->AddLine(tmp,book);

	//
	//  Background page
	//
	tmp = new iggFrame(book,2);
	book->AddPage("Background",0,tmp);

	iggFrame *b = new iggFrame("Number of tiles",tmp,2);
	iggWidget *wid;
	wid = new iggWidgetPropertyControlSpinBox(1,10,"",0,iggPropertyHandle("NumTiles",mSelfHandle,0),b);
	wid->AddDependent(mArea);
	wid->AddDependent(mLabel);
	b->AddLine(new iggWidgetTextLabel("Horizontally",b),wid);
	wid = new iggWidgetPropertyControlSpinBox(1,10,"",0,iggPropertyHandle("NumTiles",mSelfHandle,1),b);
	wid->AddDependent(mArea);
	wid->AddDependent(mLabel);
	b->AddLine(new iggWidgetTextLabel("Vertically",b),wid);
	
	tmp->AddLine(b,2);

	b = new iggFrame("Current tile...",tmp);
	mBackgroundWindowsList = new WindowListComboBox(true,0,"Assign window",this,b);
	mBackgroundWindowsList->AddDependent(mArea);
	mBackgroundWindowsList->Enable(false);
	wid = new LoadWallpaperButton(this,b);
	wid->AddBuddy(mArea);
	PseudoColorBox *psb = new PseudoColorBox(true,this,b);
	mBackgroundWindowsList->AddDependent(psb);
	mArea->AddDependent(psb);
	b->AddLine(mBackgroundWindowsList);
	b->AddLine(wid);
	b->AddLine(psb);

	tmp->AddLine(b,2);
	tmp->AddSpace(10);

	tmp2 = new iggFrame(tmp,3);
	wid = new iggWidgetPropertyControlSpinBox(0,100,"",0,iggPropertyHandle("BorderWidth",mSelfHandle),tmp2);
	wid->AddDependent(mArea);
	wid->AddDependent(mLabel);
	tmp2->AddLine(new iggWidgetTextLabel("Border width  ",tmp2),wid);
	iggWidgetPropertyControlColorSelection *bc = new iggWidgetPropertyControlColorSelection(iggPropertyHandle("BorderColor",mSelfHandle),tmp2);
	bc->SetText("Set");
	bc->AddDependent(mArea);
	tmp2->AddLine(new iggWidgetTextLabel("Border color  ",tmp2),bc);
	wid = new iggWidgetPropertyControlCheckBox("Draw border between tiles",iggPropertyHandle("InnerBorder",mSelfHandle),tmp2);
	wid->AddDependent(mArea);
	wid->AddDependent(mLabel);
	tmp2->AddLine(wid,2);
	wid = new iggWidgetPropertyControlCheckBox("Scale background to tile size",iggPropertyHandle("ScaleBackground",mSelfHandle),tmp2);
	wid->AddDependent(mArea);
	wid->AddDependent(mLabel);
	tmp2->AddLine(wid,2);
	tmp2->SetColStretch(1,3);
	tmp2->SetColStretch(2,10);

	tmp->AddLine(tmp2);
	tmp->AddSpace(10);
	tmp->SetColStretch(1,10);

	//
	//  Foreground page
	//
	tmp = new iggFrame(book);
	book->AddPage("Foreground",0,tmp);

	wid = new WindowListComboBox(false,0,"Add new window",this,tmp);
	wid->AddDependent(mArea);

	tmp->AddLine(wid);
	tmp->AddSpace(10);

	b = new WindowPropertiesBox(this,tmp);
	wid->AddDependent(b);
	wid = new RemoveWindowButton(this,b);
	wid->AddDependent(mArea);
	b->AddLine(wid);
	wid = new ScaleSpinBox(iggPropertyHandle("ForegroundWindowScale",mSelfHandle,mForegroundWindowHandle),this,b);
	wid->AddDependent(mArea);
	b->AddLine(new iggWidgetTextLabel("Scale",b),wid);
	wid = new iggWidgetPropertyControlSpinBox(0,100,"",0,iggPropertyHandle("ForegroundWindowBorderWidth",mSelfHandle,mForegroundWindowHandle),b);
	wid->AddDependent(mArea);
	b->AddLine(new iggWidgetTextLabel("Border width",b),wid);
	iggWidgetPropertyControlColorSelection *cs = new iggWidgetPropertyControlColorSelection(iggPropertyHandle("ForegroundWindowBorderColor",mSelfHandle,mForegroundWindowHandle),b);
	cs->SetText("Set");
	cs->AddDependent(mArea);
	b->AddLine(new iggWidgetTextLabel("Border color",b),cs);
	psb = new PseudoColorBox(false,this,b);
	wid->AddDependent(psb);
	mArea->AddDependent(psb);
	b->AddLine(psb,2);

	tmp2 = new iggFrame("Show as zoom",b,2);
	wid = new ZoomSourceListComboBox("",this,tmp2);
	wid->AddDependent(mArea);
	tmp2->AddLine(new iggWidgetTextLabel("Source",tmp2),wid);

	mZoomFrame = new ZoomWidgetsFrame(this,tmp2);
	wid = new iggWidgetPropertyControlFloatSlider(0.0,1.0,20,0,3,false,false,"",iggPropertyHandle("ForegroundWindowZoomX",mSelfHandle,mForegroundWindowHandle),mZoomFrame,RenderMode::DoNotRender);
	wid->AddDependent(mArea);
	mZoomFrame->AddLine(new iggWidgetTextLabel("Center X",mZoomFrame),wid);
	wid = new iggWidgetPropertyControlFloatSlider(0.0,1.0,20,0,3,false,false,"",iggPropertyHandle("ForegroundWindowZoomY",mSelfHandle,mForegroundWindowHandle),mZoomFrame,RenderMode::DoNotRender);
	wid->AddDependent(mArea);
	mZoomFrame->AddLine(new iggWidgetTextLabel("Center Y",mZoomFrame),wid);
	wid = new iggWidgetPropertyControlFloatSlider(0.0,1.0,20,0,3,false,false,"",iggPropertyHandle("ForegroundWindowZoomFactor",mSelfHandle,mForegroundWindowHandle),mZoomFrame,RenderMode::DoNotRender);
	wid->AddDependent(mArea);
	mZoomFrame->AddLine(new iggWidgetTextLabel("Factor",mZoomFrame),wid);
	mZoomFrame->SetColStretch(1,10);

	area->AddZoomFrame(mZoomFrame);

	tmp2->AddLine(mZoomFrame,2);

	wid = new iggWidgetPropertyControlCheckBox("Use 4-line zoom",iggPropertyHandle("ForegroundWindowZoom4Line",mSelfHandle,mForegroundWindowHandle),tmp2);
	wid->AddDependent(mArea);
	tmp2->AddLine(wid,2);

	tmp2->SetColStretch(1,10);

	b->AddSpace(2);
	b->AddLine(tmp2,2);

	tmp->AddLine(b);
	tmp->AddSpace(10);

	mFrame->SetColStretch(0,10);

	this->ResizeContents(700,500);

}


void iggDialogImageComposer::SetActiveBackgroundWindow(int w)
{
	if(w>=0 && w<this->GetImageComposer()->GetNumTilesX()*this->GetImageComposer()->GetNumTilesY())
	{
		mActiveBackgroundWindow = w;
	}
	else mActiveBackgroundWindow = -1;
	mBackgroundWindowsList->UpdateWidget();
}


void iggDialogImageComposer::SetActiveForegroundWindow(int w)
{
	if(w>=0 && w<this->GetImageComposer()->GetNumberOfForegroundWindows())
	{
		mActiveForegroundWindow = w;
	}
	else mActiveForegroundWindow = -1;
	this->UpdateDialog();

}


void iggDialogImageComposer::UpdateView()
{
	this->GetImageComposer()->UpdateSize();
	if(this->IsVisible())
	{
		mArea->UpdateWidget();
		mLabel->UpdateWidget();
	}
}


iggWidget* iggDialogImageComposer::GetArea()
{
	this->CompleteConstruction();

	return mArea; 
}


iImageComposer* iggDialogImageComposer::GetImageComposer() const
{
	return this->GetShell()->GetImageComposer();
}

#endif
