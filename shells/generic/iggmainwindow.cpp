/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggmainwindow.h"


#include "iactor.h"
#include "idata.h"
#include "idatareader.h"
#include "iedition.h"
#include "ierror.h"
#include "ifileloader.h"
#include "ihelpfactory.h"
#include "iparallelmanager.h"
#include "iparticleviewsubject.h"
#include "iversion.h"
#include "iviewmodule.h"

#include "iggdialoganimatingprogress.h"
#include "iggdialogauto.h"
#include "iggdialogdataexplorer.h"
#include "iggdialogfilesetexplorer.h"
#include "iggdialoghelp.h"
#include "iggdialogimagecomposer.h"
#include "iggdialogloadfile.h"
#include "iggdialogpaletteeditor.h"
#include "iggdialogparallelcontroller.h"
#include "iggdialogperformancemeter.h"
#include "iggdialogpickerwindow.h"
#include "iggdialogrenderingprogress.h"
#include "iggextensionwindow.h"
#include "iggframetopparent.h"
#include "igghandle.h"
#include "iggimagefactory.h"
#include "iggrenderwindow.h"
#include "iggpagecrosssection.h"
#include "iggpagedata.h"
#include "iggpageparticles.h"
#include "iggpagesurface.h"
#include "iggpagetensorfield.h"
#include "iggpagevectorfield.h"
#include "iggpageview.h"
#include "iggpagevolume.h"
#include "iggscriptwindow.h"
#include "iggshell.h"
#include "iggwidgetarea.h"
#include "iggwidgetpropertycontrollineedit.h"
#include "iggwidgetpropertycontrolslider.h"
#include "iggwidgetmisc.h"
#include "iggwidgetotherbutton.h"
#include "iggwidgetprogressbar.h"
#include "iggwidgettext.h"

#include "ibgdialogsubject.h"
#include "ibgextensionwindowsubject.h"
#include "ibgmainwindowsubject.h"
#include "ibgmenuwindowsubject.h"
#include "ibgrenderwindowsubject.h"
#include "ibgwidgetareasubject.h"
#include "ibgwidgetbuttonsubject.h"
#include "ibgwidgetselectionboxsubject.h"
#include "ibgwindowhelper.h"

#include <vtkRenderWindowCollection.h>
#include <vtkVersion.h>

#include "iggsubjectfactory.h"

//
//  templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


using namespace iParameter;


namespace iggMainWindow_Private
{
	//
	//  Menu ids
	//
	namespace ToolBar
	{
		const int OpenWindowsPage = 801;
		const int MinimizeWindows = 802;
		const int MaximizeWindows = 803;
		const int MoveAllTogether = 804;

		const int ShowSurface = 901;
		const int ShowCrossSection = 902;
		const int ShowVolume = 903;
		const int ShowParticles = 904;
		const int ShowVectorField = 905;
		const int ShowTensorField = 906;
		const int Max = 999;
	};

	class InteractorHelpTextView : public iggWidgetTextBrowser
	{

	private:

		struct Entry
		{
			const char* Name;
			const char* Body;
			Entry(const char *n, const char *b) : Name(n), Body(b) {}
		};

	public:

		InteractorHelpTextView(iggFrame *parent) : iggWidgetTextBrowser(true,false,parent)
		{
			mShownData = 0;
		}

		virtual void Show(bool s)
		{
			this->UpdateWidget();
			iggWidgetTextBrowser::Show(s);
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			static Entry data0[] = 
			{ 
				Entry("U key","dump the current view into an image."),
				Entry("F key","toggle the Full Screen mode on and off."),
				Entry("","--------------------"),
				Entry("","In addition, when the measuring box is activated, the following keys are accepted:"),
				Entry("+/- keys","scale the measuring box up/down."),
				Entry("</> keys","adjust box opacity."),
				Entry("","(use Style->Show interactor help menu to turn this help off)")
			};

			static Entry dataD[] = 
			{ 
				Entry(   "Trackball/Joystick mode",""),
				Entry("","-------------------------"),
				Entry("Left button","rotate the camera around its focal point."),
				Entry("[Ctrl]+Left button","spin the camera around the Z-axis (axis perpendicular to the screen)."),
				Entry("Middle button","pan (move sideways) the camera."),
				Entry("Right button","zoom in/out."),
				Entry("P key","perform a pick operation."),
				Entry("R key","reset the camera view along the current view direction."),
				Entry("S key","show all solid objects as surfaces."),
				Entry("W key","show all solid objects as wireframe (faster).")
			};

			static Entry dataF[] = 
			{ 
				Entry(   "Fly-by mode",""),
				Entry("","-------------"),
				Entry("Left button","forward motion."),
				Entry("Right button","reverse motion."),
				Entry("[Shift] key","accelerator in mouse and key modes."),
				Entry("[Ctrl]+[Shift] keys","causes sidestep instead of steering."),
				Entry("+/- keys","increase/deacrease normal speed.")
			};

			static Entry dataK[] = 
			{ 
				Entry(   "Keyboard mode",""),
					Entry("","---------------"),
					Entry("Arrow keys (or HJKL keys)","rotate the camera around its focal point."),
					Entry("[Ctrl]+Arrow keys (or [Ctrl]+HJKL keys)","spin the camera around the Z-axis (axis perpendicular to the screen)."),
					Entry("[Shift]+Arrow keys (or [Shift]+HJKL keys)","pan (move sideways) the camera."),
					Entry("+/- keys","zoom in/out."),
					Entry("A/Z keys","speed up/slow down the interaction."),
					Entry("P key","perform a pick operation."),
					Entry("R key","reset the camera view along the current view direction."),
					Entry("S key","show all solid objects as surfaces."),
					Entry("W key","show all solid objects as wireframe (faster).")
			};

			int n = 0;
			Entry *data = 0;
			switch(this->GetShell()->GetViewModule()->GetCurrentInteractorStyle())
			{
			case InteractorStyle::Trackball:
			case InteractorStyle::Joystick:
				{
					data = dataD;
					n = sizeof(dataD)/sizeof(Entry);
					break;
				}
			case InteractorStyle::Flight:
				{
					data = dataF;
					n = sizeof(dataF)/sizeof(Entry);
					break;
				}
			case InteractorStyle::Keyboard:
				{
					data = dataK;
					n = sizeof(dataK)/sizeof(Entry);
					break;
				}
			}
			if(n == 0)
			{
#ifndef I_NO_CHECK
				IBUG_WARN("Should not be here");
#endif
			}

			if(data != mShownData)
			{
				this->Clear();
				int i;
				for(i=0; i<n; i++)
				{
					this->AppendTextLine(data[i].Name,data[i].Body,iColor(0,0,255));
				}
				n = sizeof(data0)/sizeof(Entry);
				for(i=0; i<n; i++)
				{
					this->AppendTextLine(data0[i].Name,data0[i].Body,iColor(0,0,255));
				}
				mShownData = data;
			}
			this->iggWidgetTextBrowser::UpdateWidgetBody();
		}

		Entry* mShownData;
	};


	class BusyIndicator : public iggWidgetImageFlipper
	{

	public:

		BusyIndicator(iggFrame *parent) : iggWidgetImageFlipper(parent)
		{
			mSubject->AddImage(*iggImageFactory::FindIcon("light_green.png"),true);
			mSubject->AddImage(*iggImageFactory::FindIcon("light_red.png"),true);

			this->SetBaloonHelp("Ready/busy indicator","A green light indicates that IFrIT is ready for user input. The red light comes on during the time IFrIT is busy doing some work. User input is blocked while the red light is on.");
		}

		void SetBusy(bool s)
		{
			if(s) mSubject->ShowImage(1); else mSubject->ShowImage(0);
		}

	protected:

		virtual void UpdateWidgetBody()
		{
		}
	};


	class VisitedFileList : public iggWidget
	{

	public:

		VisitedFileList(iggFrame *parent) : iggWidget(parent)
		{
			mSubject = iggSubjectFactory::CreateWidgetComboBoxSubject(this,"File:");

			this->SetBaloonHelp("List of visited files","This drop-down box shows the list of recently visited files. A file from the list can be reloaded by simply selecting it.");
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			int i, k;
			const iArray<iDataReader::VisitedFile> &list(this->GetShell()->GetViewModule()->GetReader()->GetVisitedFilesList());

			k = 0;
			do
			{
				mSubject->Clear();
				for(i=list.MaxIndex(); i>=0; i--)
				{
					mSubject->InsertItem(iString((k>0)?"...":"")+list[i].Name.Part(k));
				}
				if(k == 0) k += 4; else k++;
			}
			while(!mSubject->DoesContentFit());
		}

		virtual void OnInt1Body(int v)
		{
			iDataReader *r = this->GetShell()->GetViewModule()->GetReader();

			const iArray<iDataReader::VisitedFile> &list(r->GetVisitedFilesList());

			if(v>=0 && v<list.Size())
			{
				r->LoadFile(list[list.MaxIndex()-v].Loader->GetDataType(0),list[list.MaxIndex()-v].Name,false);
			}
		}

		ibgWidgetComboBoxSubject *mSubject;
	};


	class ImageButton : public iggWidgetSimpleButton
	{

	public:

		ImageButton(iggFrame *parent) : iggWidgetSimpleButton("Image",parent)
		{
			this->SetBaloonHelp("Dump an image","Dump the scene in the current visualization window as an image. Image format and zoom factor are set in the Options menu.");
		}

	protected:

		virtual void Execute()
		{
			this->GetMainWindow()->BLOCK(true);
			this->GetShell()->GetViewModule()->WriteImages(ImageType::Image);
			this->GetMainWindow()->BLOCK(false);
		}
	};


	class RenderButton : public iggWidgetSimpleButton
	{

	public:

		RenderButton(bool all, iggFrame *parent) : iggWidgetSimpleButton(all?"Render all":"Render",parent)
		{
			this->SetRenderTarget(all?ViewModuleSelection::All:ViewModuleSelection::One);
			this->SetBaloonHelp("Render the scene",all?"Render the current scenes in all visualization windows.":"Render the current scene in the current visualization window.");
		}

	protected:

		virtual void Execute()
		{
			this->GetMainWindow()->BLOCK(true);
			this->ActualRender();
			this->GetMainWindow()->BLOCK(false);
		}
	};


	class HideInteractorHelpButton : public iggWidgetSimpleButton
	{

	public:

		HideInteractorHelpButton(iggFrame *parent) : iggWidgetSimpleButton("Close help",parent)
		{
			this->SetBaloonHelp("Close interactor help");
		}

	protected:

		virtual void Execute()
		{
			this->GetMainWindow()->BLOCK(true);
			this->GetMainWindow()->ShowInteractorHelp(false);
			this->GetMainWindow()->BLOCK(false);
		}
	};


	class Book : public iggFrameBook
	{
		
	public:
		
		Book(iggFrame *parent) : iggFrameBook(parent,true)
		{
		}

		void PolishPages()
		{
			int i;
			for(i=0; i<this->mPages.Size(); i++)
			{
				iDynamicCast<iggPageMain,iggFrame>(INFO,this->GetPage(i))->Polish();
			}
		}

	protected:

		virtual void OnInt1Body(int i)
		{
			if(!this->GetShell()->CheckCondition(iParameter::Condition::SlowRemoteConnection) && this->GetMainWindow()->GetExtensionWindow()!=0)
			{
				this->GetMainWindow()->GetExtensionWindow()->OpenBookPageByIndex(i);
			}
		}
	};


	class DialogAbout : public iggDialog
	{

	public:

		DialogAbout(iggMainWindow *parent) : iggDialog(parent,DialogFlag::Modal,0,"About",0,3,"Ok")
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

		void CompleteConstructionBody()
		{
			int i, n;
			iString s;

			s = "Version " + iVersion::GetVersion() + " Rev " + iVersion::GetRevisionId();
			switch(sizeof(void*))
			{
			case 4:
				{
					s += " (32-bit)";
					break;
				}
			case 8:
				{
					s += " (64-bit)";
					break;
				}
			}

			mFrame->AddLine(new iggWidgetTextLabel("%b%+IFrIT - Ionization FRont Interactive Tool",mFrame),3);
			mFrame->AddLine(new iggWidgetTextLabel(s,mFrame),3);
			mFrame->AddLine(new iggWidgetTextLabel(iEdition::GetEditionName(),mFrame),3);

			mFlipper = new iggWidgetLogoFlipper(mFrame);
			mFrame->AddLine(0,mFlipper);

			mFrame->AddLine(new iggWidgetTextLabel(iString("VTK version ")+vtkVersion::GetVTKVersion(),mFrame),3);

			mFrame->AddLine(new iggWidgetTextLabel("%bThis installation includes the following extensions:",mFrame),3);
			s = iVersion::GetIncludedExtensions();
			n = s.Contains(';');
			for(i=0; i<n; i++) mFrame->AddLine(new iggWidgetTextLabel(s.Section(";",i,i),mFrame),3);
			mFrame->AddLine(new iggWidgetTextLabel("%bThis installation includes the following shells:",mFrame),3);
			s = iVersion::GetIncludedShells();
			n = s.Contains(';');
			for(i=0; i<n; i++) mFrame->AddLine(new iggWidgetTextLabel(s.Section(";",i,i),mFrame),3);
			mFrame->AddLine(new iggWidgetTextLabel("",mFrame),3);
		}

		virtual void ShowBody(bool s)
		{
			if(s) mFlipper->Start(); else mFlipper->Abort();
			iggDialog::ShowBody(s);
		}

	protected:

		iggWidgetLogoFlipper *mFlipper;
	};


	class DialogDocking : public iggDialog
	{

	public:

		DialogDocking(iggMainWindow *parent) : iggDialog(parent,DialogFlag::Blocking|DialogFlag::NoTitleBar,0,"Docking...",0,3,0)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

		void CompleteConstructionBody()
		{
			mFlipper = new iggWidgetLogoFlipper(mFrame);
			mFrame->AddLine(0,mFlipper);
			mFrame->AddLine(new iggWidgetTextLabel("%b%+Rearranging windows...",mFrame),3);
		}

		virtual void ShowBody(bool s)
		{
			if(s) mFlipper->Start(); else mFlipper->Abort();
			iggDialog::ShowBody(s);
		}

	protected:

		iggWidgetLogoFlipper *mFlipper;
	};


	//
	//  A small dialog with three line edit to set the axes labels
	//
	class AxesLabelsDialog : public iggDialogAuto
	{

	public:

		AxesLabelsDialog(iggMainWindow *parent) : iggDialogAuto(parent,"Axes Labels",3)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

		void CompleteConstructionBody()
		{
			iggObjectHandle *oh = this->GetShell()->GetObjectHandle("BoundingBox");

			mFrame->AddLine(
				new iggWidgetPropertyControlTextLineEdit(false,"X: Label",iggPropertyHandle("AxesLabels",oh,0),mFrame),
				new iggWidgetPropertyControlFloatLineEdit("Max",iggPropertyHandle("AxesRanges",oh,0),mFrame),
				new iggWidgetPropertyControlFloatLineEdit("Max",iggPropertyHandle("AxesRanges",oh,1),mFrame));
			mFrame->AddLine(
				new iggWidgetPropertyControlTextLineEdit(false,"Y: Label",iggPropertyHandle("AxesLabels",oh,1),mFrame),
				new iggWidgetPropertyControlFloatLineEdit("Min",iggPropertyHandle("AxesRanges",oh,2),mFrame),
				new iggWidgetPropertyControlFloatLineEdit("Max",iggPropertyHandle("AxesRanges",oh,3),mFrame));
			mFrame->AddLine(
				new iggWidgetPropertyControlTextLineEdit(false,"Z: Label",iggPropertyHandle("AxesLabels",oh,2),mFrame),
				new iggWidgetPropertyControlFloatLineEdit("Min",iggPropertyHandle("AxesRanges",oh,4),mFrame),
				new iggWidgetPropertyControlFloatLineEdit("Max",iggPropertyHandle("AxesRanges",oh,5),mFrame));
		}
	};

#ifdef I_DEBUG
	class DebugHelperRandomPolygonColoringCheckBox : public iggWidget
	{

	public:

		DebugHelperRandomPolygonColoringCheckBox(iggFrame *parent) : iggWidget(parent)
		{
			mSubject = iggSubjectFactory::CreateWidgetButtonSubject(this,ButtonType::CheckBox,"Show all polygons",1);
			mNeedsBaloonHelp = false;
        }

	protected:

		virtual void UpdateWidgetBody()
		{
		}

		void OnVoid1Body()
		{
			this->GetMainWindow()->BLOCK(true);
			iActor::SetRandomColoring(mSubject->IsDown());
			this->GetMainWindow()->BLOCK(false);
			this->GetMainWindow()->Render();
		}

		ibgWidgetButtonSubject *mSubject;
	};

	class DebugHelperEmphasizeAllWidgetsCheckBox : public iggWidget
	{

	public:

		DebugHelperEmphasizeAllWidgetsCheckBox(iggFrame *parent) : iggWidget(parent)
		{
			mSubject = iggSubjectFactory::CreateWidgetButtonSubject(this,ButtonType::CheckBox,"Show all layouts",1);
			mNeedsBaloonHelp = false;
        }

	protected:

		virtual void UpdateWidgetBody()
		{
		}

		void OnVoid1Body()
		{
			this->GetMainWindow()->BLOCK(true);
			iggWidget::EmphasizeLayouts(mSubject->IsDown());
			this->GetMainWindow()->BLOCK(false);
		}

		ibgWidgetButtonSubject *mSubject;
	};

	class DebugHelperWorkerCheckBox : public iggWidget
	{

	public:

		DebugHelperWorkerCheckBox(iggFrame *parent) : iggWidget(parent)
		{
			mSubject = iggSubjectFactory::CreateWidgetButtonSubject(this,ButtonType::CheckBox,"VL sliders centered",1);
			mNeedsBaloonHelp = false;
        }

	protected:

		virtual void UpdateWidgetBody()
		{
		}

		void OnVoid1Body()
		{
			iggWidgetPropertyControlVariableLimitsSlider::Method = mSubject->IsDown() ? 1 : 0;
			mMainWindow->UpdateAll();
		}

		ibgWidgetButtonSubject *mSubject;
	};

	class DebugHelperFlipAllPagesButton : public iggWidgetSimpleButton
	{

	public:

		DebugHelperFlipAllPagesButton(iggFrame *sf, iggFrame *parent) : iggWidgetSimpleButton("Flip all pages",parent)
		{
			mStartFrame = sf;
			mNeedsBaloonHelp = false;
        }

	protected:

		virtual void Execute()
		{
			this->GetMainWindow()->BLOCK(true);
			if(mStartFrame != 0) mStartFrame->FlipThroughAllChildren();
			this->GetMainWindow()->BLOCK(false);
		}

		iggFrame *mStartFrame;
	};

	class DebugHelperFlashAllDialogsButton : public iggWidgetSimpleButton
	{

	public:

		DebugHelperFlashAllDialogsButton(iggFrame *parent) : iggWidgetSimpleButton("Flash all dialogs",parent)
		{
			mNeedsBaloonHelp = false;
        }

	protected:

		virtual void Execute()
		{
			this->GetMainWindow()->BLOCK(true);
			iggDialog::FlashAllDialogs();
			this->GetMainWindow()->BLOCK(false);
		}
	};

	class DebugHelperCreateUGButton : public iggWidgetSimpleButton
	{

	public:

		DebugHelperCreateUGButton(iggFrame *parent) : iggWidgetSimpleButton("Create User Guide(s)",parent)
		{
			mNeedsBaloonHelp = false;
        }

	protected:

		virtual void Execute()
		{
			this->GetMainWindow()->BLOCK(true);
			iHelpFactory::CreateUserGuide();
			this->GetMainWindow()->BLOCK(false);
		}
	};

	class DebugHelperGenerateErrorButton : public iggWidgetSimpleButton
	{

	public:

		DebugHelperGenerateErrorButton(iggFrame *parent) : iggWidgetSimpleButton("Generate error",parent)
		{
			mNeedsBaloonHelp = false;
        }

	protected:

		virtual void Execute()
		{
			IBUG_FATAL("Test error generated");
		}
	};

	class DebugHelperParallelRadioBox : public iggWidget
	{

	public:

		DebugHelperParallelRadioBox(iggFrame *parent) : iggWidget(parent)
		{
			mSubject = iggSubjectFactory::CreateWidgetRadioBoxSubject(this,1,"Parallel execution");
			mSubject->InsertItem("Parallel");
			mSubject->InsertItem("Proc 1");
			mSubject->InsertItem("Proc 2");
			mSubject->InsertItem("Procs 1+2");
			mSubject->InsertItem("No stiches");
			mNeedsBaloonHelp = false;
        }

	protected:

		virtual void UpdateWidgetBody()
		{
			if(iParallelManager::DebugSwitch>0 && iParallelManager::DebugSwitch<mSubject->Count()) mSubject->SetValue(iParallelManager::DebugSwitch); else mSubject->SetValue(0);
		}

		void OnInt1Body(int v)
		{
			this->GetMainWindow()->BLOCK(true);
			iParallelManager::DebugSwitch = v;
			this->GetShell()->GetViewModule()->GetReader()->ResetPipeline();
			this->Render();
			this->GetMainWindow()->BLOCK(false);
		}

		ibgWidgetRadioBoxSubject *mSubject;
	};

	class DebugHelperDialog : public iggDialog
	{

	public:

		DebugHelperDialog(iggFrame *sf, iggMainWindow *parent) : iggDialog(parent,0U,0,"Debug Helper",0,2)
		{
			mStartFrame = sf;
			mLog = 0;

			int wg[4] = { 0, 0, 600, 1120 };
			mSubject->SetWindowGeometry(wg);

			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

		iggWidgetTextBrowser* Log()
		{
			if(mLog == 0)
			{
				this->CompleteConstruction();
				IASSERT(mLog);
			}
			return mLog; 
		}

	protected:

		void CompleteConstructionBody()
		{
			iggFrame *f1 = new iggFrame(mFrame);
			f1->AddLine(new DebugHelperParallelRadioBox(f1));
			f1->AddSpace(10);

			iggFrame *f2 = new iggFrame(mFrame);
			f2->AddLine(new DebugHelperRandomPolygonColoringCheckBox(f2));
			f2->AddLine(new DebugHelperEmphasizeAllWidgetsCheckBox(f2));
			f2->AddLine(new DebugHelperFlipAllPagesButton(mStartFrame,f2));
			f2->AddLine(new DebugHelperFlashAllDialogsButton(f2));
			f2->AddLine(new DebugHelperCreateUGButton(f2));
			f2->AddLine(new DebugHelperGenerateErrorButton(f2));
			f2->AddLine(new DebugHelperWorkerCheckBox(f2));

			mFrame->AddLine(f1,f2);

			mLog = new iggWidgetTextBrowser(false,false,mFrame);
			mLog->SetBaloonHelp("Debuging log window","In this window IFrIT logs debigging information about filter  execution etc.");
			mFrame->AddLine(mLog,2);

			mFrame->SetColStretch(0,10);
			mFrame->SetColStretch(1,0);
			mFrame->SetRowStretch(0,0);
			mFrame->SetRowStretch(1,10);
		}

		iggFrame *mStartFrame;
		iggWidgetTextBrowser *mLog;
	};
#endif

	struct Item
	{
		int Line;
		const char* File;
	};
	static iArray<Item> BlockRegistry;

	static const iString LongNameGUI = "GraphicalUserInterface";
	static const iString ShortNameGUI = "@gui";
};


using namespace iggMainWindow_Private;


//
//  **************************************************************************************
//
//      INITIALIZATION
//
//  **************************************************************************************
//


iggMainWindow::iggMainWindow(iggShell *shell) : iObject(shell,LongNameGUI,ShortNameGUI), iggMenuWindow(shell),
	iObjectConstructPropertyMacroV(Int,iggMainWindow,Geometry,g,4),
	iObjectConstructPropertyMacroV(Int,iggMainWindow,ExtensionGeometry,eg,4),
	iObjectConstructPropertyMacroS(Int,iggMainWindow,TabMode,tm),
	iObjectConstructPropertyMacroS(String,iggMainWindow,Theme,t),
	iObjectConstructPropertyMacroS(Int,iggMainWindow,BookOrientation,bo),
	iObjectConstructPropertyMacroS(Bool,iggMainWindow,Docked,d),
	iObjectConstructPropertyMacroS(Bool,iggMainWindow,ToolTips,tt),
	iObjectConstructPropertyMacroS(Bool,iggMainWindow,InteractorHelp,ih),
	iObjectConstructPropertyMacroS(Bool,iggMainWindow,IsIdiosyncratic,ii),
	iObjectConstructPropertyMacroS(Bool,iggMainWindow,OptionsAreGlobal,og),
	iObjectConstructPropertyMacroS(Bool,iggMainWindow,AllowPrePopulateToolBar,pp),
	iObjectConstructPropertyMacroS(Bool,iggMainWindow,WindowUnderFocusActive,wfa)
{
	mInitialized = mDocked = mAllowPrePopulateToolBar = mMoveTogether = false;
	mInteractorHelp = mToolTips = mOptionsAreGlobal = mWindowUnderFocusActive = true;
	mIsIdiosyncratic = true;

	mTabMode = mBookOrientation = 0;
	mBlockLevel = 0;

	mStateFileName = this->GetShell()->GetEnvironment(Environment::Base) + "ifrit.ini";
	mExportFileName = this->GetShell()->GetEnvironment(Environment::Image);

	mInitialGeometry[0] = mInitialGeometry[1] = mInitialGeometry[2] = mInitialGeometry[3] = 0;

	mInMove = false;
	mPrevPos[0] = mPrevPos[1] = -1;
	mNextPos[0] = mNextPos[1] = -1;

	mMainWindow = this;

	mLog = 0;
#if defined(I_DEBUG)
	mDialogDebugHelper = 0;
#endif

	ibgWindowSubject::Block(false);
}


void iggMainWindow::StartInitialization()
{
	//
	//  MainSubject must be created first
	//
	mMainSubject = iggSubjectFactory::CreateMainWindowSubject(this,3);

	//
	//  Have we created MenuSubject as well?
	//
	if(this->GetSubject() == 0)
	{
		mTwoSubjectsAreTheSame = false;
		this->AttachSubject(iggSubjectFactory::CreateMenuWindowSubject(this,iggImageFactory::FindIcon("genie1gui.png"),"Ionization Front Interactive Tool"),3);
	}
	else
	{
		mTwoSubjectsAreTheSame = true;
	}

	//
	//  Top frames
	//
	mBusyIndicatorFrame = new iggFrameTopParent(this->GetShell());
	mVisitedFileListFrame = new iggFrameTopParent(this->GetShell());

	mMainSubject->SetTopParentSubjects(mBusyIndicatorFrame,mVisitedFileListFrame);

	IASSERT(mBusyIndicatorFrame->GetSubject());
	IASSERT(mVisitedFileListFrame->GetSubject());

	//
	//  Other components
	//
	mProgressBar = new iggWidgetProgressBar(mGlobalFrame,true);

	//
	//  Status bar
	//
	mBusyIndicator = new BusyIndicator(mBusyIndicatorFrame);
	mBusyIndicatorFrame->AddLine(mBusyIndicator);
	mVisitedFileList = new VisitedFileList(mVisitedFileListFrame);
	mVisitedFileListFrame->AddLine(mVisitedFileList);

	mMainSubject->PopulateStatusBar(mBusyIndicatorFrame,mProgressBar->GetSubject(),mVisitedFileListFrame);

	mGlobalFrame->SetPadding(true);

	//
	//  Extension
	//
	mExtensionWindow = new iggExtensionWindow(this);

	//
	//  Base frame
	//
	mBaseFrame = new iggFrameFlip(mGlobalFrame);

	//
	//  Create the book frame but not layout it
	//
	iggFrame *tmpf = new iggFrame(mBaseFrame);
	mLog = new iggWidgetTextBrowser(false,false,mGlobalFrame);
	mLog->SetBaloonHelp("Log window","In this window IFrIT logs information about the data files, non-critical execution error, performance information, etc.");

	//
	//  Adjust our behaviour depending on the running conditions
	//
	iggShell *s = this->GetShell();
	
	//
	//  Create dialogs (needed for book pages)
	//
	mDialogAbout = new DialogAbout(this); IERROR_CHECK_MEMORY(mDialogAbout);
	mDialogAnimatingProgress = new iggDialogAnimatingProgress(this); IERROR_CHECK_MEMORY(mDialogAnimatingProgress);
	mDialogAxesLabels = new AxesLabelsDialog(this); IERROR_CHECK_MEMORY(mDialogAxesLabels);
	mDialogDataExplorer = new iggDialogDataExplorer(this); IERROR_CHECK_MEMORY(mDialogDataExplorer);
	mDialogDocking = new DialogDocking(this); IERROR_CHECK_MEMORY(mDialogDocking);
	mDialogFileSetExplorer = new iggDialogFileSetExplorer(this); IERROR_CHECK_MEMORY(mDialogFileSetExplorer);
	mDialogHelp = new iggDialogHelp(this); IERROR_CHECK_MEMORY(mDialogHelp);
	mDialogImageComposer = new iggDialogImageComposer(this); IERROR_CHECK_MEMORY(mDialogImageComposer);
	mDialogLoadFile = new iggDialogLoadFile(this); IERROR_CHECK_MEMORY(mDialogLoadFile);
	mDialogPaletteEditor = new iggDialogPaletteEditor(this); IERROR_CHECK_MEMORY(mDialogPaletteEditor);
	mDialogParallelController = new iggDialogParallelController(this); IERROR_CHECK_MEMORY(mDialogParallelController);
	mDialogPerformanceMeter = new iggDialogPerformanceMeter(this); IERROR_CHECK_MEMORY(mDialogPerformanceMeter);
	mDialogPickerWindow = new iggDialogPickerWindow(this); IERROR_CHECK_MEMORY(mDialogPickerWindow);
	mDialogRenderingProgress = 0; // we create this one later, so that it does not pop up during the initialization process
	mScriptWindow = new iggScriptWindow(this); IERROR_CHECK_MEMORY(mScriptWindow);

	//
	//  Create the book
	//
	Book *book = new Book(tmpf);
	mBook = book;

	mViewPage = new iggPageView(mBook);
	mDataPage = new iggPageData(mBook);
	
	mPages[0] = new iggPageSurface(mBook);
	mPages[1] = new iggPageCrossSection(mBook);
	mPages[2] = new iggPageVolume(mBook);
	mPages[3] = mParticlesPage = new iggPageParticles(mBook);
	mPages[4] = new iggPageVectorField(mBook);
	mPages[5] = new iggPageTensorField(mBook);

	mBook->AddPage("View",iggImageFactory::FindIcon("view.png"),mViewPage);
	mBook->AddPage("Surface",iggImageFactory::FindIcon("surf.png"),mPages[0]);
	mBook->AddPage("Cross section",iggImageFactory::FindIcon("xsec.png"),mPages[1]);
	mBook->AddPage("Volume",iggImageFactory::FindIcon("volv.png"),mPages[2]);
	mBook->AddPage("Particles",iggImageFactory::FindIcon("part.png"),mPages[3]);
	mBook->AddPage("Vector field",iggImageFactory::FindIcon("vect.png"),mPages[4]);
	mBook->AddPage("Tensor field",iggImageFactory::FindIcon("tens.png"),mPages[5]);
	mBook->AddPage("Data",iggImageFactory::FindIcon("data.png"),mDataPage);

	//
	//  Layout the book frame
	//
	tmpf->AddLine(mBook);
	mBaseFrame->AddLayer(tmpf);

	//
	//  Interactor help frame
	//
	tmpf = new iggFrame("",mBaseFrame,3);
	tmpf->AddLine(new iggWidgetTextLabel("%b%+Interactor Help",tmpf),3);
	tmpf->AddLine(new InteractorHelpTextView(tmpf),3);
	tmpf->AddLine((iggWidget *)0,new HideInteractorHelpButton(tmpf),(iggWidget *)0);
	tmpf->SetRowStretch(0,1);
	tmpf->SetRowStretch(1,10);
	mBaseFrame->AddLayer(tmpf);
	mBaseFrame->ShowLayer(0);

	mGlobalFrame->AddLine(mBaseFrame,3);

	iggFrameFlip *flip1 = new iggFrameFlip(mGlobalFrame,false);
	iggFrame *tmp1 = new iggFrame(flip1,3);
	mRenderButton = new RenderButton(false,tmp1);
	mRenderAllButton = new RenderButton(true,tmp1);
	tmp1->SetPadding(true);
	tmp1->AddLine(new ImageButton(tmp1),mRenderButton,mRenderAllButton);
	tmp1->SetColStretch(2,10);
	flip1->AddLayer(tmp1);
	flip1->ShowLayer(0);

	bool slow = s->CheckCondition(iParameter::Condition::SlowRemoteConnection);
	ibgRenderWindowSubject::RenderOnFocus = slow;
	this->GetShell()->SetAutoRender(!slow);
	mRenderButton->Show(slow);
	mRenderAllButton->Show(slow);

	mDataTypeFrame = new iggFrameFlip(mGlobalFrame,false);

	//mGlobalFrame->AddLine(tmp1,new iggWidgetTextLabel("",mGlobalFrame),tmp2);
	mGlobalFrame->AddLine(flip1,(iggWidget *)0,mDataTypeFrame);
	mGlobalFrame->AddLine(mLog,3);
	mGlobalFrame->SetRowStretch(0,1);
	mGlobalFrame->SetRowStretch(1,0);
	if(s->IsDesktopSmall(true))
	{
		mLog->Show(false);
		mGlobalFrame->SetRowStretch(2,0);
	}
	else
	{
		mGlobalFrame->SetRowStretch(2,10);
	}

	mExtensionWindow->CompleteInitialization();
	mParticlesPage->UpdateIcons();

	book->PolishPages();

	iEdition::ApplySettings(this);

	this->SetToolTips(mToolTips);

#if defined(I_DEBUG)
	mDialogDebugHelper = new DebugHelperDialog(mGlobalFrame,this);
#endif

	//
	//  Connect pre-existing RenderWindows
	//
	int i;
	for(i=0; i<this->GetShell()->GetNumberOfViewModules(); i++)
	{
		iRequiredCast<iggRenderWindow>(INFO,this->GetShell()->GetViewModule(i)->GetRenderWindow())->AttachToMainWindow(this);
	}
}


void iggMainWindow::PreShowInitialization()
{
	this->BuildMenus();
	if(mInitialGeometry[2]>0 && mInitialGeometry[3]>0) this->GetSubject()->SetWindowGeometry(mInitialGeometry);
}


void iggMainWindow::PostShowInitialization()
{
	mDialogRenderingProgress = new iggDialogRenderingProgress(this); IERROR_CHECK_MEMORY(mDialogRenderingProgress);

	int wg[4];
	this->GetSubject()->GetWindowGeometry(wg);
	mPrevPos[0] = wg[0];
	mPrevPos[1] = wg[1];

	mInitialized = true;
}


iggMainWindow::~iggMainWindow()
{
	if(mDocked)
	{
		mMainSubject->RestoreWindowsFromDockedPositions();
	}

	delete mExtensionWindow;
	mExtensionWindow = 0; // it does not exist any more

	if(mDialogAbout != 0) delete mDialogAbout;
	if(mDialogAnimatingProgress != 0) delete mDialogAnimatingProgress;
	if(mDialogAxesLabels != 0) delete mDialogAxesLabels;
	if(mDialogDataExplorer != 0) delete mDialogDataExplorer;
	if(mDialogDocking != 0) delete mDialogDocking;
	if(mDialogFileSetExplorer != 0) delete mDialogFileSetExplorer;
	if(mDialogHelp != 0) delete mDialogHelp;
	if(mDialogImageComposer != 0) delete mDialogImageComposer;
	if(mDialogLoadFile != 0) delete mDialogLoadFile;
	if(mDialogPaletteEditor != 0) delete mDialogPaletteEditor;
	if(mDialogParallelController != 0) delete mDialogParallelController;
	if(mDialogPerformanceMeter != 0) delete mDialogPerformanceMeter;
	if(mDialogPickerWindow != 0) delete mDialogPickerWindow;
	if(mDialogRenderingProgress != 0) delete mDialogRenderingProgress;
	if(mScriptWindow != 0) delete mScriptWindow;
#if defined(I_DEBUG)
	if(mDialogDebugHelper != 0) delete mDialogDebugHelper;
#endif

	delete mProgressBar;

	delete mGlobalFrame;
	delete mBusyIndicatorFrame;
	delete mVisitedFileListFrame;

	if(!mTwoSubjectsAreTheSame)
	{
		delete mMainSubject;
	}

	//
	//  Remove us from the windows
	//
	int i;
	ibgRenderWindowSubject *sub;
	for(i=0; i<mWindowList.Size(); i++)
	{
		sub = dynamic_cast<ibgRenderWindowSubject*>(mWindowList[i]);
		if(sub != 0)
		{
			sub->mOwner->mMainWindow = 0;
		}
	}
}


void iggMainWindow::StartTimer()
{
	mMainSubject->StartTimer();
}


void iggMainWindow::UpdateContents()
{
	mGlobalFrame->UpdateWidget();

	//
	//  Also update widgets that are not children of mGlobalFrame
	//
	mBusyIndicator->UpdateWidget();
	mVisitedFileList->UpdateWidget();

	//
	//  Update some of the dialogs
	//
	if(mDialogDataExplorer != 0) mDialogDataExplorer->UpdateDialog();
	if(mDialogFileSetExplorer != 0) mDialogFileSetExplorer->UpdateDialog();
	if(mDialogImageComposer != 0) mDialogImageComposer->UpdateDialog();
	if(mDialogPickerWindow != 0) mDialogPickerWindow->UpdateDialog();
	if(mViewPage != 0) mViewPage->GetWindowListDialog()->UpdateDialog();

	//
	//  Show/hide rendering buttons
	//
	mRenderButton->Show(!this->GetShell()->GetAutoRender());
	mRenderAllButton->Show(!this->GetShell()->GetAutoRender());

	//
	//  Update script window
	//
	mScriptWindow->UpdateAll();

	//
	//  Update extension too
	//
	mExtensionWindow->UpdateAll();
}


void iggMainWindow::Register(ibgWindowSubject *window)
{
	if(window != 0) mWindowList.AddUnique(window);
}


void iggMainWindow::UnRegister(ibgWindowSubject *window)
{
	if(window != 0) mWindowList.Remove(window);
}


void iggMainWindow::RegisterAutoDialog(iggDialogAuto *d)
{
	if(d != 0) mAutoDialogList.AddUnique(d);
}


void iggMainWindow::UnRegisterAutoDialog(iggDialogAuto *d)
{
	if(d != 0) mAutoDialogList.Remove(d);
}


//
//  **************************************************************************************
//
//      DATA MANIPULATION
//
//  **************************************************************************************
//
void iggMainWindow::AfterDataChanged()
{
	if(mDialogDataExplorer!=0 && mDialogDataExplorer->IsVisible()) mDialogDataExplorer->UpdateDialog();
	this->Render(ViewModuleSelection::Clones);
	this->UpdateAll();
}


//
//  **************************************************************************************
//
//      MISC FUNCTIONS
//
//  **************************************************************************************
//
bool iggMainWindow::SetToolTips(bool s)
{
	mToolTips = s;
	mMainSubject->ShowToolTips(s);
	return true;
}


void iggMainWindow::ShowInteractorHelp(bool s, iViewModule* /*vm*/)
{
	mBaseFrame->ShowLayer(s?1:0);
}


#ifndef I_NO_CHECK
void iggMainWindow::Block(bool s, int line, const char *file)
#define BLOCK(s) Block(s,__LINE__,__FILE__)
#else
void iggMainWindow::Block(bool s)
#endif
{
	if(s)
	{
		if(mBlockLevel == 0)
		{
			ibgWindowSubject::Block(true);
			iDynamicCast<BusyIndicator,iggWidget>(INFO,mBusyIndicator)->SetBusy(true);
		}
		mBlockLevel++;
#ifndef I_NO_CHECK
		Item item;
		item.Line = line;
		item.File = file;
		BlockRegistry.Add(item);
#endif
	}
	else
	{
#ifndef I_NO_CHECK
		Item last = BlockRegistry.RemoveLast();
		if(strcmp(last.File,file) != 0)
		{
			IBUG_ERROR("Incorrect order of MainWindow->Block() calls: block hold from file "+iString(last.File)+"#"+iString::FromNumber(last.Line)+" is matched by a block release from file "+iString(file)+"#"+iString::FromNumber(line)+". This is a bug, but IFrIT may continue to work properly.");
		}
#endif
		mBlockLevel--;
		if(mBlockLevel == 0)
		{
			iDynamicCast<BusyIndicator,iggWidget>(INFO,mBusyIndicator)->SetBusy(false);
			ibgWindowSubject::Block(false);
		}
		if(mDialogRenderingProgress!=0 && mDialogRenderingProgress->IsCancelled() && mDialogRenderingProgress->IsVisible())
		{
			mDialogRenderingProgress->Show(false);
		}
		if(mBlockLevel < 0)
		{
#ifndef I_NO_CHECK
			IBUG_ERROR("Incorrect order of MainWindow->Block() calls. This is a bug, but IFrIT may continue to work properly.");
#endif
			mBlockLevel = 0;
		}
	}
}


bool iggMainWindow::IsExitAllowed()
{
	return this->AskForConfirmation("Are you sure you want to exit IFrIT?","Exit");
}


void iggMainWindow::WriteToLog(const iString &prefix, const iString &message, const iColor &color)
{
	if(mLog != 0)
	{
		mLog->AppendTextLine(prefix,message,color);
		if(mLog->GetNumberOfLines() > 1000)
		{
			iString text = mLog->GetText();
			mLog->SetText(text.Part(text.Length()/5));
		}
	}
}


void iggMainWindow::WriteToLog(const iString &message)
{
	if(mLog != 0)
	{
		mLog->AppendTextLine("\t",message);
		if(mLog->GetNumberOfLines() > 1000)
		{
			iString text = mLog->GetText();
			mLog->SetText(text.Part(text.Length()/5));
		}
	}
}


#ifdef I_DEBUG
void iggMainWindow::WriteToDebugLog(const iString &message)
{
	DebugHelperDialog *d = dynamic_cast<DebugHelperDialog*>(mDialogDebugHelper);
	if(d != 0)
	{
		d->Log()->AppendTextLine(message);
		if(d->Log()->GetNumberOfLines() > 10000)
		{
			iString text = d->Log()->GetText();
			d->Log()->SetText(text.Part(text.Length()/5));
		}
	}
}
#endif


void iggMainWindow::ClearLog()
{
	mLog->Clear();
}


void iggMainWindow::ShowLog(bool s)
{
	mLog->Show(s);
	if(s)
	{
		mGlobalFrame->SetRowStretch(2,10);
		this->ProcessEvents();
	}
	else
	{
		mGlobalFrame->SetRowStretch(2,0);
		this->ProcessEvents();

		int wg[4];
		this->GetSubject()->GetWindowGeometry(wg,true);
		wg[3] = 10;
		this->GetSubject()->SetWindowGeometry(wg,true);
	}
}


bool iggMainWindow::IsLogVisible() const
{
	return mLog->IsVisible();
}


void iggMainWindow::OpenBookPage(int n)
{
	mBook->OpenPage(n);
}


bool iggMainWindow::SetTabMode(int m)
{
	switch(m)
	{
	case 0:
		{
			mBook->SetTabMode(BookTab::TitleOnly);
			mTabMode = 0;
			break;
		}
	case 1:
		{
			mBook->SetTabMode(BookTab::ImageOnly);
			mTabMode = 1;
			break;
		}
	case 2:
		{
			mBook->SetTabMode(BookTab::TitleAndImage);
			mTabMode = 2;
			break;
		}
	default:
		{
			return false;
		}
	}
	if(mExtensionWindow != 0) mExtensionWindow->SetTabMode(m);
	return true;;
	return true;
}


void iggMainWindow::ShowAll(bool s)
{
	this->GetSubject()->Show(s);
	if(!mDocked)
	{
		mExtensionWindow->GetSubject()->Show(s);
		
		FOR_EVERY_RENDER_WINDOW(w)
		{
			w->GetSubject()->Show(s);
		}
	}
}


void iggMainWindow::DockWindows(bool s, bool show)
{
	if(s == mDocked) return;
	
	if(show) mDialogDocking->Show(true);

	this->ProcessEvents(true); // god knows why this is needed

	if(s)
	{
		mDocked = true;

		//
		//  Save all geometries
		//
		this->GetSubject()->GetHelper()->SaveWindowGeometry();
		mExtensionWindow->GetSubject()->GetHelper()->SaveWindowGeometry();

		FOR_EVERY_RENDER_WINDOW(w)
		{
			w->GetSubject()->GetHelper()->SaveWindowGeometry();
		}

		mGlobalFrame->SetPadding(false);
		mExtensionWindow->SetPadding(false);

		mMainSubject->PlaceWindowsInDockedPositions();
	} 
	else 
	{
		mMainSubject->RestoreWindowsFromDockedPositions();

		mGlobalFrame->SetPadding(true);
		mExtensionWindow->SetPadding(true);

		this->GetSubject()->GetHelper()->RestoreWindowGeometry();
		this->GetSubject()->Show(true);
		mExtensionWindow->GetSubject()->GetHelper()->RestoreWindowGeometry();
		mExtensionWindow->GetSubject()->GetHelper()->RestoreDecoration();
		mExtensionWindow->GetSubject()->Show(true);

		FOR_EVERY_RENDER_WINDOW(w)
		{
			w->GetSubject()->GetHelper()->RestoreWindowGeometry();
			w->GetSubject()->GetHelper()->RestoreDecoration();
			w->GetSubject()->Show(true);
		}

		mDocked = false;
	}

	this->ProcessEvents(true); // god knows why this is needed

	int i;
	for(i=0; i<this->GetShell()->GetNumberOfViewModules(); i++)
	{
		//if(this->GetShell()->GetViewModule(i)->GetAntialiasing())
		{
			//this->GetShell()->GetViewModule(i)->SetAntialiasing(false);
			//this->Render();
			//this->GetShell()->GetViewModule(i)->SetAntialiasing(true);
		}
		//this->Render();
	}

	this->ProcessEvents(true); // god knows why this is needed

	if(show) mDialogDocking->Show(false);
}


void iggMainWindow::PlaceAutoDialogs()
{
	this->ProcessEvents(true); // god knows why this is needed
	//
	//  Shift dialogs off the way
	//
	int mwg[4], fs[2], wg[4];
	this->GetSubject()->GetWindowGeometry(mwg);
	this->GetSubject()->GetFrameSize(fs[0],fs[1]);

	int i, j, tmp, n = 0, topOffset = 0;
	int *width = new int[mAutoDialogList.Size()];
	int *index = new int[mAutoDialogList.Size()];
	if(width!=0 && index!=0)
	{
		for(i=0; i<mAutoDialogList.Size(); i++) if(mAutoDialogList[i]->IsVisible())
		{
			mAutoDialogList[i]->GetSubject()->GetWindowGeometry(wg);
			index[n] = i;
			width[n] = wg[2];
			n++;
		}

		for(i=0; i<n-1; i++)
		{
			for(j=i+1; j<n; j++)
			{
				if(width[i] < width[j])
				{
					tmp = width[i];
					width[i] = width[j];
					width[j] = tmp;
					tmp = index[i];
					index[i] = index[j];
					index[j] = tmp;
				}
			}
		}

		for(i=0; i<n; i++)
		{
			this->ProcessEvents(true); // god knows why this is needed

			mAutoDialogList[index[i]]->GetSubject()->GetWindowGeometry(wg);
			wg[0] = mwg[0] + mwg[2] + fs[0];
			wg[1] = mwg[1] + topOffset;
			mAutoDialogList[index[i]]->GetSubject()->SetWindowGeometry(wg);

			this->ProcessEvents(true); // god knows why this is needed

			topOffset += (wg[3]+fs[1]);
		}

		delete [] width;
		delete [] index;
	}
}


bool iggMainWindow::AskForConfirmation(const iString &message, const char *action)
{
	return !mIsIdiosyncratic || this->GetShell()->PopupWindow(message,MessageType::Information,action,"Cancel")==0;
}


void iggMainWindow::UpdateOnPick()
{
	mExtensionWindow->UpdateOnPick();
}


void iggMainWindow::UpdateMarkerWidgets()
{
	if(mViewPage != 0) mViewPage->UpdateMarkerWidgets();
}


void iggMainWindow::UpdateParticleWidgets(const iImage *icon)
{
	if(icon != 0)
	{
		mBook->ChangeIcon(4,icon);
	}
	if(mParticlesPage != 0)
	{
		if(icon != 0)
		{
			mParticlesPage->GetBook()->ChangeIcon(-1,icon);
		}
//		mParticlesPage->UpdateWidget();
	}
	if(icon != 0)
	{
		this->GetSubject()->SetToolBarIcon(ToolBar::ShowParticles,*icon);
	}
	this->GetSubject()->UpdateMenus();

	mExtensionWindow->UpdateParticleWidgets(icon);
}


void iggMainWindow::AddTheme(const iString &name)
{
	mThemeList.Add(name);
}


bool iggMainWindow::SetTheme(const iString& theme)
{
	if(theme.IsEmpty()) return true;  // not set

	int i;
	
	for(i=0; i<mThemeList.Size(); i++) if(theme == mThemeList[i])
	{
		mMainSubject->SetTheme(theme);
		return true;
	}
	
	return false;
}


void iggMainWindow::ProcessEvents(bool sync) const
{
	this->GetShell()->ProcessEvents(sync);
}


//
//  **************************************************************************************
//
//  iObject functionality
//
//  **************************************************************************************
//
int iggMainWindow::GetGeometry(int i) const
{
	int wg[4];
	this->GetSubject()->GetWindowGeometry(wg);
	if(i>=0 && i<4)
	{
		return wg[i];
	}
	else return 0;
}


int iggMainWindow::GetExtensionGeometry(int i) const
{
	int wg[4];
	mExtensionWindow->GetSubject()->GetWindowGeometry(wg);
	if(i>=0 && i<4)
	{
		return wg[i];
	}
	else return 0;
}


bool iggMainWindow::SetGeometry(int i, int v)
{
	int wg[4];

	if(i<0 || i>4) return false;

	if(mInitialized)
	{
		this->GetSubject()->GetWindowGeometry(wg);
		wg[i] = v;
		this->GetSubject()->SetWindowGeometry(wg);
	}
	else
	{
		mInitialGeometry[i] = v;
	}
	
	return true;
}


bool iggMainWindow::SetExtensionGeometry(int i, int v)
{
	int wg[4];

	if(i<0 || i>4) return false;

	mExtensionWindow->GetSubject()->GetWindowGeometry(wg);
	wg[i] = v;
	mExtensionWindow->GetSubject()->SetWindowGeometry(wg);

	return true;
}


bool iggMainWindow::SetDocked(bool b)
{
	this->DockWindows(b,false);
	return true;
}


bool iggMainWindow::SetInteractorHelp(bool b)
{
	mInteractorHelp = b;
	return true;
}


bool iggMainWindow::SetOptionsAreGlobal(bool b)
{
	mOptionsAreGlobal = b;
	return true;;
}


bool iggMainWindow::SetWindowUnderFocusActive(bool b)
{
	mWindowUnderFocusActive = b;
	return true;;
}


bool iggMainWindow::SetAllowPrePopulateToolBar(bool b)
{
	mAllowPrePopulateToolBar = b;
	return true;;
}


bool iggMainWindow::SetIsIdiosyncratic(bool b)
{
	mIsIdiosyncratic = b;
	return true;;
}


bool iggMainWindow::SetBookOrientation(int i)
{
	mBook->SetOrientation(i);
	return true;
}


//
//  **************************************************************************************
//
//    WINDOW MANIPULATION
//
//  **************************************************************************************
//
void iggMainWindow::OnRenderWindowMove(int /*wn*/)
{
}


void iggMainWindow::OnRenderWindowResize(int /*wn*/)
{
	if(mDialogImageComposer != 0) mDialogImageComposer->UpdateView();
}


void iggMainWindow::OnRenderWindowFocusIn(int wn)
{
	if(wn<-1 || wn>=this->GetShell()->GetNumberOfViewModules())
	{
		return; // can happen when a docked window is deleted
	}
	
	if(mWindowUnderFocusActive && wn>=0)
	{
		if(this->GetShell()->SetActiveViewModuleIndex(wn)) this->UpdateAll();
	}

//	if(mInteractorHelp) this->ShowInteractorHelp(true,this->GetShell()->GetView(wn));
}


void iggMainWindow::OnRenderWindowFocusOut(int /*wn*/)
{
	if(mInteractorHelp) this->ShowInteractorHelp(false);
}


void iggMainWindow::OnRenderWindowEnter(int wn)
{
	if(wn<-1 || wn>=this->GetShell()->GetNumberOfViewModules())
	{
		return; // can happen when a docked window is deleted
	}
	
	if(mInteractorHelp) this->ShowInteractorHelp(true,this->GetShell()->GetViewModule(wn));
}


void iggMainWindow::OnRenderWindowLeave(int /*wn*/)
{
	if(mInteractorHelp) this->ShowInteractorHelp(false);
}


void iggMainWindow::DisplayWindowsAsIcons()
{
	if(!mDocked)
	{
		int i;
		for(i=0; i<mWindowList.Size(); i++)
		{
			//
			//  On multi-desktop systems this may cause weird behaviour unless all windows are on the same desktop.
			//
			mWindowList[i]->GetHelper()->ShowAsIcon();
		}
	}
}


void iggMainWindow::DisplayWindowsAsWindows()
{
	if(!mDocked)
	{
		int i;
		for(i=0; i<mWindowList.Size(); i++) 
		{
			//
			//  On multi-desktop systems this may cause weird behaviour unless all windows are on the same desktop.
			//
			mWindowList[i]->GetHelper()->ShowAsWindow();
		}
	}
}


void iggMainWindow::MoveWindows(int pos[2])
{
	//
	//  We must make this function safe against multiple concurrent calls.
	//  Save the last call.
	//
	mNextPos[0] = pos[0];
	mNextPos[1] = pos[1];

	if(mInMove) return; // ignore rapidly arriving events
	mInMove = true;

	if(mMoveTogether && mInitialized && !mDocked) // do not move the first render window
	{
		int wg[4];
		int i, dp[2];
		dp[0] = pos[0] - mPrevPos[0];
		dp[1] = pos[1] - mPrevPos[1];
		if(dp[0]!=0 || dp[1]!=0)
		{
			ibgWindowSubject::Block(true); // needed to disable rendering while moving
			for(i=0; i<mWindowList.Size(); i++) if(mWindowList[i]->IsVisible())
			{
				mWindowList[i]->GetWindowGeometry(wg);
				wg[0] += dp[0];
				wg[1] += dp[1];
				mWindowList[i]->SetWindowGeometry(wg);
			}
			ibgWindowSubject::Block(false);
			this->ProcessEvents(true);
		}
	}

	mPrevPos[0] = pos[0];
	mPrevPos[1] = pos[1];

	mInMove = false;

	//
	//  Check that this function was not called while being executed.
	//
	if(mNextPos[0]!=pos[0] || mNextPos[1]!=pos[1])
	{
		this->MoveWindows(mNextPos);
	}
}


void iggMainWindow::Exit()
{
	if(this->IsExitAllowed())
	{
		int i;
		for(i=0; i<mWindowList.Size(); i++) mWindowList[i]->Show(false);
		mScriptWindow->Show(false);
		this->GetShell()->Stop();
	}
}


const iString iggMainWindow::ListBlockRegistry()
{
	int i;
	iString s;

	for(i=0; i<BlockRegistry.Size(); i++)
	{
		s += iString(BlockRegistry[i].File) + "#" + iString::FromNumber(BlockRegistry[i].Line) + "\n";
	}

	return s;
}

#endif
