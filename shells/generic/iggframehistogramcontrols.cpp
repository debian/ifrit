/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggframehistogramcontrols.h"


#include "ierror.h"
#include "ihistogram.h"
#include "istretch.h"

#include "iggwidgetarea.h"
#include "iggwidgetmisc.h"
#include "iggwidgetotherbutton.h"


using namespace iParameter;


namespace iggFrameHistogramControls_Private
{
	class ShowInLogCheckBox : public iggWidgetSimpleCheckBox
	{

	private:

		iggFrameHistogramControls *mOwner;

	public:

		ShowInLogCheckBox(iggFrameHistogramControls *owner, iggFrame *parent) : iggWidgetSimpleCheckBox("Show Y-axis in log",parent)
		{
			mOwner = owner;

			this->SetBaloonHelp("Show histogram in log scale");
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			this->SetChecked(mOwner->GetYLogScale());
		}

		virtual void OnChecked(bool s)
		{
			mOwner->SetYLogScale(s);
			mOwner->UpdateArea();
		}
	};

	class ShowInFullResolutionCheckBox : public iggWidgetSimpleCheckBox
	{

	private:

		iggFrameHistogramControls *mOwner;

	public:

		ShowInFullResolutionCheckBox(iggFrameHistogramControls *owner, iggFrame *parent) : iggWidgetSimpleCheckBox("Full resoluton (may be slow)",parent)
		{
			mOwner = owner;

			this->SetBaloonHelp("Show histogram with full resolution (slow for large data sets)");
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			this->SetChecked(mOwner->GetFullResolution());
		}

		virtual void OnChecked(bool s)
		{
			mOwner->SetFullResolution(s);
			mOwner->UpdateArea();
		}
	};

	class StretchComboBox : public iggWidgetSimpleComboBox
	{

	private:

		iggFrameHistogramControls *mOwner;

	public:

		StretchComboBox(iggFrameHistogramControls *owner, iggFrame *parent) : iggWidgetSimpleComboBox("Stretch",parent)
		{
			mOwner = owner;
			
			int i = 0;
			while(!iStretch::GetName(i).IsEmpty())
			{
				this->InsertItem(iStretch::GetName(i++));
			}

			this->OnInt1Body(mOwner->GetStretchId());

			this->SetBaloonHelp("Set the stretch for X-axis","This box sets the stretch (scaling) for the horizontal axis.");
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			this->SetValue(mOwner->GetStretchId());
		}

		void OnInt1Body(int s)
		{
			mOwner->SetStretchId(s);
			mOwner->UpdateArea();
		}
	};
};


using namespace iggFrameHistogramControls_Private;


//
//  Main class
//
iggFrameHistogramControls::iggFrameHistogramControls(bool withfull, bool withstretch, iggFrame *parent) : iggFrame(parent)
{
	mFullResolution = !withfull;
	mYLogScale = false;
	mStretchId = iStretch::Lin;

	mHistogramArea = 0;

	iggFrame *tmp = new iggFrame(this,3);
	mDisplayMin = new iggWidgetNumberLabel(tmp);
	mDisplayMin->SetAlignment(-1);
	mDisplayMax = new iggWidgetNumberLabel(tmp);
	mDisplayMin->SetAlignment(1);
	tmp->AddLine(mDisplayMin,new iggWidgetTextLabel("<-- range -->",tmp),mDisplayMax);
	tmp->SetColStretch(1,10);
	this->AddLine(tmp);

	tmp = new iggFrame(this,3);
	if(withstretch)
	{
		tmp->AddLine(new ShowInLogCheckBox(this,tmp),static_cast<iggWidget*>(0),new StretchComboBox(this,tmp));
	}
	else
	{
		tmp->AddLine(new ShowInLogCheckBox(this,tmp));
	}
	if(withfull)
	{
		tmp->AddLine(new ShowInFullResolutionCheckBox(this,tmp));
	}
	tmp->SetColStretch(1,10);
	this->AddLine(tmp);
}


void iggFrameHistogramControls::SetYLogScale(bool s)
{
	mYLogScale = s;
	this->UpdateWidget();
}


void iggFrameHistogramControls::SetStretchId(int s)
{
	mStretchId = s;
	this->UpdateWidget();
}


void iggFrameHistogramControls::SetFullResolution(bool s)
{
	mFullResolution = s;
	this->UpdateWidget();
}


void iggFrameHistogramControls::DisplayRange(float min, float max)
{
	mDisplayMin->SetNumber(min);
	mDisplayMax->SetNumber(max);
}


void iggFrameHistogramControls::Attach(iggWidgetHistogramArea *area)
{
	mHistogramArea = area;
}

void iggFrameHistogramControls::UpdateArea()
{
	if(mHistogramArea != 0) mHistogramArea->UpdateWidget();
}

#endif
