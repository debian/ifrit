/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggwidgetpropertycontrollineedit.h"


#include "iimage.h"
#include "iposition.h"

#include "iggframe.h"
#include "iggimagefactory.h"
#include "iggmainwindow.h"

#include "ibgwidgetbuttonsubject.h"

#include "iggsubjectfactory.h"

//
//  templates
//
#include "iarray.tlh"
#include "iggwidgetpropertycontrol.tlh"
#include "iggwidgetpropertycontrollineedit.tlh"


using namespace iParameter;


//
//******************************************
//
//  TextLineEdit class
//
//******************************************
//
iggWidgetPropertyControlTextLineEdit::iggWidgetPropertyControlTextLineEdit(bool readonly, const iString &label, const iggPropertyHandle& ph, iggFrame *parent, int rm) : iggWidgetPropertyControlLineEdit<iType::String>(false,readonly,label,ph,parent,rm)
{
}


void iggWidgetPropertyControlTextLineEdit::ConvertToString(const iString& val, iString &s) const
{
	s = val;
}


bool iggWidgetPropertyControlTextLineEdit::ConvertFromString(const iString &s, iString &val) const
{
	val = s;
	return true;
}


//
//******************************************
//
//  FloatLineEdit class
//
//******************************************
//
iggWidgetPropertyControlFloatLineEdit::iggWidgetPropertyControlFloatLineEdit(const iString &label, const iggPropertyHandle& ph, iggFrame *parent, int rm) : iggWidgetPropertyControlLineEdit<iType::Float>(true,false,label,ph,parent,rm)
{
}


void iggWidgetPropertyControlFloatLineEdit::ConvertToString(float val, iString &s) const
{
	s = iString::FromNumber(val);
}


bool iggWidgetPropertyControlFloatLineEdit::ConvertFromString(const iString &s, float &val) const
{
	bool ok;
	val = s.ToFloat(ok);
	if(!ok) val = 0.0f;
	return ok;
}


//
//******************************************
//
//  GenericPositionLineEdit class
//
//******************************************
//
iggWidgetPropertyControlGenericPositionLineEdit::iggWidgetPropertyControlGenericPositionLineEdit(const iString &label, const iggPropertyHandle& ph, iggFrame *parent, int rm) : iggWidgetPropertyControlLineEdit<iType::Double>(true,false,label,ph,parent,rm)
{
	iggWidgetPropertyControlBase::PositionList().AddUnique(this);
}


iggWidgetPropertyControlGenericPositionLineEdit::~iggWidgetPropertyControlGenericPositionLineEdit()
{
	iggWidgetPropertyControlBase::PositionList().Remove(this);
}


//
//******************************************
//
//  PositionLineEdit class
//
//******************************************
//
iggWidgetPropertyControlPositionLineEdit::iggWidgetPropertyControlPositionLineEdit(const iString &label, const iggPropertyHandle& ph, iggFrame *parent, int rm) : iggWidgetPropertyControlGenericPositionLineEdit(label,ph,parent,rm)
{
}


void iggWidgetPropertyControlPositionLineEdit::ConvertToString(double val, iString &s) const
{
	iCoordinate d(this->GetShell()->GetViewModule());
	d = val;
	s = iString::FromNumber(d.BoxValue());
}


bool iggWidgetPropertyControlPositionLineEdit::ConvertFromString(const iString &s, double &val) const
{
	bool ok;
	val = s.ToDouble(ok);
	if(!ok) val = 0.0;

	iCoordinate d(this->GetShell()->GetViewModule());
	d.SetBoxValue(val);
	val = d;

	return ok;
}


//
//******************************************
//
//  SizeLineEdit class
//
//******************************************
//
iggWidgetPropertyControlSizeLineEdit::iggWidgetPropertyControlSizeLineEdit(const iString &label, const iggPropertyHandle& ph, iggFrame *parent, int rm) : iggWidgetPropertyControlGenericPositionLineEdit(label,ph,parent,rm)
{
}


void iggWidgetPropertyControlSizeLineEdit::ConvertToString(double val, iString &s) const
{
	iDistance d(this->GetShell()->GetViewModule());
	d = val;
	s = iString::FromNumber(d.BoxValue());
}


bool iggWidgetPropertyControlSizeLineEdit::ConvertFromString(const iString &s, double &val) const
{
	bool ok;
	val = s.ToDouble(ok);
	if(!ok) val = 0.0;

	iDistance d(this->GetShell()->GetViewModule());
	d.SetBoxValue(val);
	val = d;

	return ok;
}


//
//******************************************
//
//  FileNameLineEdit class
//
//******************************************
//
#include "iggwidgetotherbutton.h"

namespace iggWidgetPropertyControlFileNameLineEdit_Private
{
	class OpenButton : public iggWidgetSimpleButton
	{

	public:

		OpenButton(iggWidgetPropertyControlFileNameLineEdit *owner, iggFrame *parent) : iggWidgetSimpleButton("",parent,true)
		{
			mOwner = owner;
			mWasLaidOut = true;
			this->SetBaloonHelp("Open Load File Dialog");
		}

		ibgWidgetButtonSubject* GetSubject() const { return mSubject; }

	protected:

		virtual void Execute()
		{
			mOwner->OpenLoadFileDialog();
		}

		iggWidgetPropertyControlFileNameLineEdit *mOwner;
	};

	class UnloadButton : public iggWidgetSimpleButton
	{

	public:

		UnloadButton(iggWidgetPropertyControlFileNameLineEdit *owner, iggFrame *parent) : iggWidgetSimpleButton("",parent,true)
		{
			mOwner = owner;
			mWasLaidOut = true;
			this->SetBaloonHelp("Unload the file");
		}

		ibgWidgetButtonSubject* GetSubject() const { return mSubject; }

	protected:

		virtual void Execute()
		{
			mOwner->UnloadFile();
		}

		iggWidgetPropertyControlFileNameLineEdit *mOwner;
	};
};


iggWidgetPropertyControlFileNameLineEdit::iggWidgetPropertyControlFileNameLineEdit(bool render, const iString &header, const iString &location, const iString &selection, bool exists, const iString &label, bool unloadable, const iggPropertyHandle& ph, iggFrame *parent) : 	iggWidgetPropertyControlTextLineEdit(true,label,ph,parent,RenderMode::DoNotRender)
{
	this->SetRenderTarget(ViewModuleSelection::All);

	mRender = render;
	mHeader = header;
	mLocation = location;
	mSelection = selection;
	mFileExists = exists;
	mUnloadable = unloadable;

	iggWidgetPropertyControlFileNameLineEdit_Private::OpenButton *w1 = new iggWidgetPropertyControlFileNameLineEdit_Private::OpenButton(this,parent); IERROR_CHECK_MEMORY(w1);
	w1->GetSubject()->SetFlat(true);
	w1->GetSubject()->SetSize(18,18);
	w1->GetSubject()->SetIcon(*iggImageFactory::FindIcon("fileopen2.png"));
	this->mSubject->AddButton(w1->GetSubject());

	iggWidgetPropertyControlFileNameLineEdit_Private::UnloadButton *w2 = new iggWidgetPropertyControlFileNameLineEdit_Private::UnloadButton(this,parent); IERROR_CHECK_MEMORY(w2);
	w2->GetSubject()->SetFlat(true);
	w2->GetSubject()->SetSize(18,18);
	w2->GetSubject()->SetIcon(*iggImageFactory::FindIcon(unloadable?"remove.png":"remove2.png"));
	this->mSubject->AddButton(w2->GetSubject());
	w2->Enable(unloadable);

}


void iggWidgetPropertyControlFileNameLineEdit::OnVoid1Body()
{
	this->OpenLoadFileDialog();
}


void iggWidgetPropertyControlFileNameLineEdit::UnloadFile()
{
	if(mUnloadable)
	{
		this->SetText("");
		this->iggWidgetPropertyControlTextLineEdit::ExecuteControl(true);
		this->UpdateWidget();
		this->UpdateDependents();
		if(mRender) this->Render();
	}
}


void iggWidgetPropertyControlFileNameLineEdit::OpenLoadFileDialog()
{
	iString name = this->GetMainWindow()->GetFileName(mHeader,mSubject->GetText().IsEmpty()?mLocation:mSubject->GetText(),mSelection,mFileExists);
	if(!name.IsEmpty() || mUnloadable)
	{
		this->SetText(name);
		this->iggWidgetPropertyControlTextLineEdit::ExecuteControl(true);
		this->UpdateWidget();
		this->UpdateDependents();
		if(mRender) this->Render();
	}
}


void iggWidgetPropertyControlFileNameLineEdit::UpdateWidgetBody()
{
	this->iggWidgetPropertyControlTextLineEdit::UpdateWidgetBody();
	mSubject->ShowTail();
}

#endif
