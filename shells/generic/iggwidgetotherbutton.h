/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IGGWIDGETOTHERBUTTON_H
#define IGGWIDGETOTHERBUTTON_H


#include "iggwidget.h"


#include "iarray.h"

class iImage;

class iggDialog;
class iggMenuWindow;
class iggObjectHandle;
class iggPage;
class iggPageObject;
class iggWidgetPropertyControlBase;
class iggWidgetPropertyControlSizeSlider;

class ibgWidgetButtonSubject;


class iggWidgetShowButton : public iggWidget
{

public:

	iggWidgetShowButton(iggPageObject *page, iggFrame *parent);

protected:

	virtual void UpdateWidgetBody();
	virtual void OnVoid1Body();

	ibgWidgetButtonSubject *mSubject;
	iggPageObject *mPage;
};


class iggWidgetLaunchButton : public iggWidget
{

public:

	iggWidgetLaunchButton(iggDialog *dialog, const iString &text, iggFrame *parent, bool destructive = false);
	iggWidgetLaunchButton(iggMenuWindow *window, const iString &text, iggFrame *parent, bool destructive = false);
	virtual ~iggWidgetLaunchButton();

protected:

	void Define(const iString &text, bool destructive = false);

	virtual void UpdateWidgetBody();
	virtual void OnVoid1Body();

	ibgWidgetButtonSubject *mSubject;
	iggDialog *mDialog;
	iggMenuWindow *mWindow;
	bool mDestructive;
};


class iggWidgetAllInstancesCheckBox : public iggWidget
{
	
public:
	
	iggWidgetAllInstancesCheckBox(iggFrame *parent);
	
protected:
	
	virtual void UpdateWidgetBody();
	virtual void OnVoid1Body();

	ibgWidgetButtonSubject *mSubject;
};


class iggWidgetSimpleButton : public iggWidget
{

public:

	iggWidgetSimpleButton(const iString &text, iggFrame *parent, bool tool = false, int minsize = 0);

protected:

	virtual void UpdateWidgetBody();
	virtual void OnVoid1Body();
	virtual void Execute() = 0;

	ibgWidgetButtonSubject *mSubject;
};


class iggWidgetDialogCloseButton : public iggWidgetSimpleButton
{

public:

	iggWidgetDialogCloseButton(iggDialog *dialog, iggFrame *parent, const char *text = "Close");

protected:

	virtual void Execute();

	iggDialog *mDialog;
};

#endif  // IGGWIDGETOTHERBUTTON_H

