/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggpageview.h"


#include "ianimator.h"
#include "icamera.h"
#include "iclipplane.h"
#include "icolorbar.h"
#include "idata.h"
#include "idatareader.h"
#include "ierror.h"
#include "ihelpfactory.h"
#include "imarker.h"
#include "imonitor.h"
#include "ipalette.h"
#include "ipalettecollection.h"
#include "ipointglyph.h"
#include "irendertool.h"
#include "istretch.h"
#include "iviewmodule.h"
#include "ivtk.h"
#include "iwriter.h"

#include "iggdialoganimatingprogress.h"
#include "iggdialogauto.h"
#include "iggdialogimagecomposer.h"
#include "iggextensionfactory.h"
#include "iggframeactiveinstance.h"
#include "iggframedatatypeselector.h"
#include "iggframedatavariablelist.h"
#include "iggframedisplay.h"
#include "iggframematerialproperties.h"
#include "iggframeobjectcontrols.h"
#include "iggframepaletteselection.h"
#include "iggframeposition.h"
#include "iggimagefactory.h"
#include "iggrenderwindowobserver.h"
#include "iggshell.h"
#include "iggwidgetarea.h"
#include "iggwidgetpropertycontrolbutton.h"
#include "iggwidgetpropertycontrolcolorselection.h"
#include "iggwidgetpropertycontrollineedit.h"
#include "iggwidgetpropertycontrolselectionbox.h"
#include "iggwidgetpropertycontrolslider.h"
#include "iggwidgetpropertycontroltrackball.h"
#include "iggwidgetotherbutton.h"
#include "iggwidgetmisc.h"
#include "iggwidgettext.h"

#include "ibgwidgetareasubject.h"
#include "ibgwidgetbuttonsubject.h"
#include "ibgwidgetselectionboxsubject.h"
#include "ibgwidgethelper.h"

#include <vtkCamera.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>

#include "iggsubjectfactory.h"

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"
#include "iggwidgetpropertycontrol.tlh"
#include "iggwidgetpropertycontrolslider.tlh"


using namespace iParameter;

namespace iggPageView_Private
{

	//
	//  Show color bar check box
	//
	class ShowColorBarCheckBox : public iggWidgetPropertyControl<iType::Bool>
	{
		
	public:
		
		ShowColorBarCheckBox(const iggPropertyHandle &ph, iggFrame *parent) : iggWidgetPropertyControl<iType::Bool>(ph,parent,RenderMode::Immediate)
		{
			mSubject = iggSubjectFactory::CreateWidgetButtonSubject(this,ButtonType::CheckBox,"Show",1);
		}
		
	protected:

		virtual void OnVoid1Body()
		{
			this->ExecuteControl(true);
		}

		virtual void QueryValue(bool &v) const
		{
			v = mSubject->IsDown();
		}
		
		virtual void UpdateValue(bool v)
		{
			mSubject->SetDown(v);
		}
		
		ibgWidgetButtonSubject *mSubject;
	};


	//
	//  Controls the DataType displayed on color bars
	//
	class ColorBarDataHandle : public iggDataTypeHandle
	{

	public:

		ColorBarDataHandle(iggFrame *frame)
		{
			mFrame = frame;
			iDataType::FindTypesByKeywords(mDataInfo,"scalars");
			mIndex = 0;
		}

		virtual const iDataInfo& GetDataInfo() const
		{
			return mDataInfo;
		}

		virtual int GetActiveDataTypeIndex() const
		{
			return mIndex;
		}

	protected:

		virtual void OnActiveDataTypeChanged(int v)
		{
			if(v>=0 && v<mDataInfo.Size())
			{
				mIndex = v;
				mFrame->UpdateWidget();
			}
		}

		virtual void AfterFileLoadBody(const iDataInfo &info)
		{
			this->SetActiveDataType(info.Type(0));
		}

	private:

		int mIndex;
		iDataInfo mDataInfo;
		iggFrame *mFrame;
	};


	//
	//  Color bar frame
	//
	class ColorBarFrame : public iggFrame
	{
		
	public:
		
		ColorBarFrame(iggWidgetPropertyControlCheckBox *autobox, int idx, const iString &title, iggFrame *parent) : iggFrame(title,parent,1)
		{
			mAutomaticBox = autobox;
			autobox->AddDependent(this);

			mIndex = idx;
			mDataHandle = new ColorBarDataHandle(this);

			//this->AddLine(new ShowColorBarCheckBox(key,this));
			this->AddLine(new iggWidgetTextLabel("Palette",this));
			const iggPropertyHandle ph("Palette",this->GetShell()->GetObjectHandle("ColorBar"),idx);
			mPaletteList = new iggWidgetPropertyControlComboBox("",0,ph,this);
			this->AddLine(mPaletteList);

			const iggPropertyHandle vh("Var",this->GetShell()->GetObjectHandle("ColorBar"),idx);
			iggFrameDataVariableList *vl = new iggFrameDataVariableList("Variable",mDataHandle,vh,this);
			vl->Complete();
			this->AddLine(vl);
			this->AddSpace(2);

			this->AddLine(new iggWidgetTextLabel("Data type",this));
			this->AddLine(new iggFrameDataTypeSelector(mDataHandle,"",this));
		}

		virtual ~ColorBarFrame()
		{
			delete mDataHandle;
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			//
			//  Are we enabled?
			//
			bool b;
			mAutomaticBox->UpdateWidget();
			mAutomaticBox->QueryValue(b);
			this->Enable(!b);

			if(!b)
			{
				//
				//  Update palettes
				//
				int i, np = iPaletteCollection::Global()->Size();
				mPaletteList->Clear();
				mPaletteList->SetOffset(np);
				for(i=np-1; i>=0; i--)
				{
					mPaletteList->InsertItem(iPaletteCollection::Global()->GetPalette(i)->GetName()+" reversed");
				}
				mPaletteList->InsertItem("Do not show");
				for(i=0; i<np; i++)
				{
					mPaletteList->InsertItem(iPaletteCollection::Global()->GetPalette(i)->GetName());
				}
				//
				//  Update data type
				//
				this->GetShell()->GetViewModule()->GetColorBar()->SetDataType(mIndex,mDataHandle->GetActiveDataType().GetId());
			}
			//
			//  Update the rest
			//
			this->iggFrame::UpdateWidgetBody();
		}

		int mIndex;
		ColorBarDataHandle *mDataHandle;
		iggWidgetPropertyControlComboBox *mPaletteList;
		iggWidgetPropertyControlCheckBox *mAutomaticBox;
	};


	//
	//  Animation controls
	//
	class AnimationStyleBox : public iggWidgetPropertyControlRadioBox
	{

	public:

		AnimationStyleBox(iggFrame *parent) : iggWidgetPropertyControlRadioBox(1,"Style",0,iggPropertyHandle("Style",parent->GetShell()->GetObjectHandle("Animator")),parent){}
	};


	class AnimationFramesSlider : public iggWidgetPropertyControlLargeIntSlider
	{
		
	public:

		AnimationFramesSlider(iggFrame *parent) : iggWidgetPropertyControlLargeIntSlider(1,150,5,"Frames per file",iggPropertyHandle("NumberOfFrames",parent->GetShell()->GetObjectHandle("Animator")),parent,RenderMode::DoNotRender){}
	};


	class AnimateButton : public iggWidgetSimpleButton
	{

	public:

		AnimateButton(iggFrame *parent) : iggWidgetSimpleButton("Animate",parent)
		{
			this->SetBaloonHelp("Start animation");
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			mWidgetHelper->Enable(this->GetShell()->GetViewModule()->GetReader()->IsFileAnimatable());
		}

		virtual void Execute()
		{
			iMonitor h(this);
			this->GetMainWindow()->GetDialogAnimatingProgress()->Animate();
			this->Render();

			if(h.IsStopped())
			{
				this->GetShell()->PopupWindow("Animation completed with the following message:\n"+h.LastErrorMessage(),MessageType::Warning);
			}
		}
	};

	//
	//  Window manipulation widgets
	//
	//
	//  A small dialog with the list of existing windows
	//
	class WindowList : public iggWidgetListView
	{

	public:

		WindowList(iggFrame *parent) : iggWidgetListView(false,parent)
		{
			this->SetBaloonHelp("Select the current window","This control selects a visualization window to be the \"current\" one. The current window is labeled by a marker placed in the upper left corner of the window, and is the window controled by the widgets.");
        }

	protected:

		virtual void UpdateWidgetBody()
		{
			int i, n = this->GetShell()->GetNumberOfViewModules();
			if(n != this->Count())
			{
				this->Clear();
				iString ws;
				for(i=0; i<n; i++)
				{
					ws = "Window " + iString::FromNumber(i+1);
					if(this->GetShell()->GetViewModule(i)->IsClone())
					{
						ws += " (Clone of #" + iString::FromNumber(this->GetShell()->GetViewModule(i)->GetCloneOfWindow()+1) + ")";
					}
					this->InsertItem(ws);
				}
			}
			this->Select(this->GetShell()->GetActiveViewModuleIndex(),this->GetShell()->GetActiveViewModuleIndex());
		}

		virtual void OnCursorPositionChanged(int line, int index)
		{
			if(this->GetShell()->SetActiveViewModuleIndex(line)) this->GetMainWindow()->UpdateAll();
		}
	};


	class WindowListDialog : public iggDialogAuto
	{

	public:

		WindowListDialog(iggMainWindow *parent) : iggDialogAuto(parent,"Windows",1)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

		void CompleteConstructionBody()
		{
			mFrame->AddLine(new WindowList(mFrame));
			this->ResizeContents(100,200);
		}
	};


	class WindowNumberComboBox : public iggWidget
	{

	public:

		WindowNumberComboBox(iggFrame *parent, iggPageView *page) : iggWidget(parent), mPage(page)
		{
			mSubject = iggSubjectFactory::CreateWidgetComboBoxSubject(this,"Set active window");
			this->SetBaloonHelp("Select the active window","This control selects a visualization window to be the \"active\" one. The active window is the one controled by the widgets.");
        }

	protected:

		virtual void UpdateWidgetBody()
		{
			mPage->GetWindowListDialog()->UpdateDialog();

			mSubject->Clear();

			int i, n = this->GetShell()->GetNumberOfViewModules();
			for(i=0; i<n; i++) mSubject->InsertItem("Window "+iString::FromNumber(i+1));
			mSubject->SetValue(this->GetShell()->GetActiveViewModuleIndex());
		}

		void OnInt1Body(int v)
		{
			if(this->GetShell()->SetActiveViewModuleIndex(v)) this->GetMainWindow()->UpdateAll();
		}

		ibgWidgetComboBoxSubject *mSubject;
		iggPageView *mPage;
	};


	class CopyFromWindowComboBox : public iggWidget
	{

	public:

		CopyFromWindowComboBox(iggFrame *parent) : iggWidget(parent)
		{
			mSubject = iggSubjectFactory::CreateWidgetComboBoxSubject(this,"Copy settings from...");
			this->SetBaloonHelp("Copy all internal settings from another window");
        }

	protected:

		virtual void UpdateWidgetBody()
		{
			mSubject->Clear();

			int i, n = this->GetShell()->GetNumberOfViewModules();
			mSubject->InsertItem("...");
			if(n > 1)
			{
				this->Enable(true);
				for(i=0; i<n; i++) if(i != this->GetShell()->GetActiveViewModuleIndex())
				{
					mSubject->InsertItem("Window "+iString::FromNumber(i+1));
				}
			}
			else this->Enable(false);
			mSubject->SetValue(0);
		}

		void OnInt1Body(int v)
		{
			if(v > 0)
			{
				if(v > this->GetShell()->GetActiveViewModuleIndex()) v++;

				iViewModule *vm1 = this->GetShell()->GetViewModule();
				iViewModule *vm2 = this->GetShell()->GetViewModule(v-1);
				if(vm1!=0 && vm2!=0 && vm1!=vm2)
				{
					this->GetMainWindow()->BLOCK(true);
					vm1->CopyState(vm2);
					this->GetShell()->RequestRender(vm1);
					this->GetMainWindow()->BLOCK(false);
				}
			}
			mSubject->SetValue(0);
		}

		ibgWidgetComboBoxSubject *mSubject;
	};


	class DialogWait : public iggDialog
	{

	public:

		DialogWait(bool create, iggMainWindow *parent) : iggDialog(parent,DialogFlag::Blocking|DialogFlag::NoTitleBar,0,"Working...",0,3,0)
		{
			mFlipper = new iggWidgetLogoFlipper(mFrame);
			mFrame->AddLine(0,mFlipper);
			mFrame->AddLine(new iggWidgetTextLabel(iString("%b%+")+(create?"Creat":"Delet")+"ing window...",mFrame),3);

			this->CompleteConstruction();
		}

		void CompleteConstructionBody()
		{
		}

		virtual void ShowBody(bool s)
		{
			if(s) mFlipper->Start(); else mFlipper->Abort();
			iggDialog::ShowBody(s);
		}

	protected:

		iggWidgetLogoFlipper *mFlipper;
	};


	class CreateDeleteWindowButton : public iggWidgetSimpleButton
	{

	public:

		CreateDeleteWindowButton(int type, iggFrame *parent) : iggWidgetSimpleButton("",parent)
		{
			mType = type;
			mDialog = new DialogWait(true,this->GetMainWindow());

			this->SetRenderTarget(ViewModuleSelection::Clones);

			switch(mType)
			{
			case -1:
				{
					mSubject->SetText("Delete active window");
					this->SetBaloonHelp("Delete the current window","This button deletes the current visualization window and all the objects and the data associated with it.");
					break;
				}
 			case 0:
				{
					mSubject->SetText("Create new window");
					this->SetBaloonHelp("Create a new independent window","This button creates a new visualization window. The new window is independent of other windows and has default values of all parameters.");
					break;
				}
			case 1:
				{
					mSubject->SetText("Copy active window");
					this->SetBaloonHelp("Copy the active window","This button creates a new visualization window which is a copy of the active visualization window, i.e. it has the same values of all parameters for all objects. The new window is independent of other windows, and has no data associated with it.");
					break;
				}
			case 2:
				{
					mSubject->SetText("Clone active window");
					this->SetBaloonHelp("Clone the active window","This button creates a clone of the active visualization window. The clone shares the data with the parent window, and at birth has the same values of all parameters for all visualization objects. It does have a separate set of visualization objects however, so the parameters of the parent and the clone window can be changed independently.");
					break;
				}
			default: IBUG_FATAL("Incorrect button type");
			}
        }

		virtual ~CreateDeleteWindowButton()
		{
			delete mDialog;
		}

	protected:

		virtual void CreateWindow()
		{
			if (mType == 0)
			{
				const iPropertyAction *prop = iDynamicCast<const iPropertyAction,const iProperty>(INFO,this->GetShell()->FindPropertyByName("Window.New"));
				if (prop == 0)
				{
					IBUG_ERROR("Unable to find the correct property");
				}

				if (!prop->CallAction())
				{
					this->GetShell()->PopupWindow("Unable to create a new window.",MessageType::Error);
				}
				else
				{
					if (this->GetShell()->SetActiveViewModuleIndex(this->GetShell()->GetNumberOfViewModules() - 1)) this->GetMainWindow()->UpdateAll();
				}
			}
			else
			{
				const iPropertyAction1<iType::Int> *prop;
				switch (mType)
				{
				case 1:
				{
					prop = iDynamicCast<const iPropertyAction1<iType::Int> >(INFO,this->GetShell()->FindPropertyByName("Window.Copy"));
					break;
				}
				case 2:
				{
					prop = iDynamicCast<const iPropertyAction1<iType::Int> >(INFO,this->GetShell()->FindPropertyByName("Window.Clone"));
					break;
				}
				default: IBUG_FATAL("Incorrect button type");
				}

				if (prop == 0)
				{
					IBUG_ERROR("Unable to find the correct property");
				}

				if (!prop->CallAction(this->GetShell()->GetActiveViewModuleIndex()))
				{
					this->GetShell()->PopupWindow("Unable to create a new window.",MessageType::Error);
				}
				else
				{
					if (this->GetShell()->SetActiveViewModuleIndex(this->GetShell()->GetNumberOfViewModules() - 1)) this->GetMainWindow()->UpdateAll();
				}
			}
		}

		virtual void DeleteWindow()
		{
			const iPropertyAction1<iType::Int> *prop = iDynamicCast<const iPropertyAction1<iType::Int> >(INFO,this->GetShell()->FindPropertyByName("Window.Delete"));
			if(prop == 0)
			{
				IBUG_ERROR("Unable to find the correct property");
			}

			if(!prop->CallAction(this->GetShell()->GetActiveViewModuleIndex()))
			{
				this->GetShell()->PopupWindow("Unable to delete the active window.",MessageType::Error);
			}
			else
			{
				this->GetMainWindow()->UpdateAll();
			}
		}

		virtual void Execute()
		{
			this->GetMainWindow()->BLOCK(true);
			mDialog->Show(true);

			if(mType < 0)
			{
				this->DeleteWindow();
			}
			else
			{
				this->CreateWindow();
			}
	
			mDialog->Show(false);
			this->GetMainWindow()->BLOCK(false);
			this->Render(ViewModuleSelection::All);
		}

		int mType;
		DialogWait *mDialog;
	};


	class WindowUnderFocusActiveCheckBox : public iggWidget
	{

	public:

		WindowUnderFocusActiveCheckBox(iggFrame *parent) : iggWidget(parent)
		{
			mSubject = iggSubjectFactory::CreateWidgetButtonSubject(this,ButtonType::CheckBox,"Make window under focus active",1);
			this->SetBaloonHelp("Toggles whether the visualization window with the keyboard focus becomes the active one","When this box is checked, moving the keyboard focus into a visualization window will make that window the active one.");
        }

	protected:

		virtual void UpdateWidgetBody()
		{
			mSubject->SetDown(this->GetMainWindow()->GetWindowUnderFocusActive());
		}

		void OnVoid1Body()
		{
			this->GetMainWindow()->SetWindowUnderFocusActive(mSubject->IsDown());
		}

		ibgWidgetButtonSubject *mSubject;
	};


	class WidgetsControlAllWindowsCheckBox : public iggWidget
	{

	public:

		WidgetsControlAllWindowsCheckBox(iggFrame *parent) : iggWidget(parent)
		{
			mSubject = iggSubjectFactory::CreateWidgetButtonSubject(this,ButtonType::CheckBox,"Widgets control all windows",1);
			this->SetBaloonHelp("Toggles whether widgets control all visualization windows or only the current one","When this box is checked, widget controls in the gui window will affect all visualization windows, not just the current one. This option should be used with care, since it only makes sense if different visualization windows display similar scenes (for example, the same visualization object for different data files). If the scenes in different visualization windows are unlike each other, the effects of this option may be quite unexpected.");
        }

	protected:

		virtual void UpdateWidgetBody()
		{
		}

		void OnVoid1Body()
		{
			if(mSubject->IsDown())
			{
				iggWidgetPropertyControlBase::SetGlobalExecuteFlags(ExecuteFlag::Index::One|ExecuteFlag::Module::All);
			}
			else
			{
				//
				//  Go back to default ones
				//
				iggWidgetPropertyControlBase::SetGlobalExecuteFlags(ExecuteFlag::Default);
			}
		}

		ibgWidgetButtonSubject *mSubject;
	};


	class SynchronizeCamerasExecButton : public iggWidgetActionControlExecButton
	{

	public:

		SynchronizeCamerasExecButton(iggFrame *parent) : iggWidgetActionControlExecButton("Synchronize cameras",iggPropertyHandle("SynchronizeCameras",parent->GetShell()->GetShellHandle()),parent)
		{
		}

		virtual bool ExecuteControl(bool final, bool val)
		{
			if(iDynamicCast<const iPropertyAction1<iType::Int> >(INFO,this->Handle()->Function())->CallAction(this->GetShell()->GetActiveViewModuleIndex()))
			{
				this->SetRenderTarget((this->GetExecuteFlags() & ExecuteFlag::Module::Mask));
				if (this->GetRenderMode() == RenderMode::Immediate || (final && this->GetRenderMode() != RenderMode::DoNotRender)) this->Render();
				return true;
			}
			else return false;
		}
	};


	class WindowStatusIcon : public iggFrame
	{

	public:

		WindowStatusIcon(iggFrame *parent) : iggFrame("Status",parent,1)
		{
			mIcon1 = iggImageFactory::FindIcon("winindiv.png");
			mIcon2 = iggImageFactory::FindIcon("winclone.png");

			mText1 = "Individual";
			mText2 = "Clone of #";

			mIconView = new iggWidgetImageArea(*mIcon1,false,this);
			mTextView = new iggWidgetTextLabel(mText1,this);
			this->AddLine(mIconView);
			this->AddLine(mTextView);
//			this->AddSpace(10);
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			if(this->GetShell()->GetViewModule()->IsClone())
			{
				mIconView->SetImage(*mIcon2,false);
				mTextView->SetText(mText2+iString::FromNumber(this->GetShell()->GetViewModule()->GetCloneOfWindow()+1));
			}
			else
			{
				mIconView->SetImage(*mIcon1,false);
				mTextView->SetText(mText1);
			}
			this->iggFrame::UpdateWidgetBody();
		}

		const iImage *mIcon1, *mIcon2;
		iString mText1, mText2;
		iggWidgetImageArea *mIconView;
		iggWidgetTextLabel *mTextView;
	};


	//
	//  Marker widgets
	//
	class MarkerPropertiesFrame : public iggFrame
	{

	public:

		MarkerPropertiesFrame(iggFrame *parent) : iggFrame("Marker",parent,2)
		{
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			if(this->GetShell()->GetViewModule()->GetMarkerObject()->GetNumber() > 0)
			{
				this->Enable(true);
				this->iggFrame::UpdateWidgetBody();
			}
			else
			{
				this->Enable(false);
			}
		}
	};


	class CurrentMarkerHandle : public iggIndexHandle
	{

	public:

		CurrentMarkerHandle(iggFrameActiveInstance *owner)
		{
			mOwner = owner; IASSERT(mOwner);
		}

		virtual int Index() const
		{
			return mOwner->GetActiveInstance();
		}

	private:

		iggFrameActiveInstance *mOwner;
	};


	class ActiveMarkerFrame : public iggFrameActiveInstance
	{

	public:

		ActiveMarkerFrame(iggFrame *parent) : iggFrameActiveInstance(true,"Active marker","marker",parent), mActiveInstance(parent->GetShell())
		{
			mActiveInstance = -1;
			mHandle = new CurrentMarkerHandle(this); IERROR_CHECK_MEMORY(mHandle);
		}

		virtual ~ActiveMarkerFrame()
		{
			delete mHandle;
		}

		const iggIndexHandle* GetIndexHandle() const { return mHandle; }

		virtual bool CreateInstance()
		{
			iMarkerObject *obj = this->GetShell()->GetViewModule()->GetMarkerObject();
 			if(obj!=0 && obj->Create.CallAction())
			{
				this->SetActiveInstance(obj->GetNumber()-1);
				return true;
			}
			else return false;
		}

		virtual bool DeleteInstance(int i)
		{
			iMarkerObject *obj = this->GetShell()->GetViewModule()->GetMarkerObject();
			if(obj!=0 && obj->Remove.CallAction(this->GetActiveInstance()))
			{
				if(mActiveInstance == obj->GetNumber()) this->SetActiveInstance(obj->GetNumber()-1);
				return true;
			}
			else return false;
		}

		virtual int GetNumberOfInstances() const
		{
			iMarkerObject *obj = this->GetShell()->GetViewModule()->GetMarkerObject();
			return obj->GetNumber();
		}

		virtual int GetActiveInstance() const
		{
			return mActiveInstance;
		}

		virtual void SetActiveInstance(int n)
		{
			if(n>=0 && n<this->GetNumberOfInstances())
			{
				mActiveInstance = n;
				mParent->UpdateWidget();
			}
		}

	protected:

		iggIndex mActiveInstance;
		CurrentMarkerHandle *mHandle;
	};


	//
	//  A small dialog with the line edit to set the ruler scale exactly
	//
	class RulerScaleDialog : public iggDialogAuto, public iggRenderWindowObserver
	{

	public:

		RulerScaleDialog(iggMainWindow *parent) : iggDialogAuto(parent,"Ruler Scale",2), iggRenderWindowObserver(mFrame)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

		void CompleteConstructionBody()
		{
			mFrame->AddLine(new iggWidgetTextLabel("Scale",mFrame),new iggWidgetPropertyControlFloatLineEdit("",iggPropertyHandle("Scale",this->GetShell()->GetObjectHandle("Ruler")),mFrame));
			mFrame->AddLine(new iggWidgetTextLabel("Title",mFrame),new iggWidgetPropertyControlTextLineEdit(false,"",iggPropertyHandle("Title",this->GetShell()->GetObjectHandle("Ruler")),mFrame));
			mFrame->AddSpace(10);
		}

	protected:

		virtual void OnRenderWindowModified()
		{
			this->UpdateDialog();
		}
	};

	class MarkerMoveCaptionsButton : public iggWidgetSimpleButton
	{

	public:

		MarkerMoveCaptionsButton(iggFrame *parent) : iggWidgetSimpleButton("Move captions",parent)
		{
			iString s = "or.m.mc";
			const iHelpDataBuffer &buf = iHelpFactory::FindData(s.ToCharPointer(),iHelpFactory::_ObjectReference);
			this->SetBaloonHelp("Starts the Caption Moving mode. Press Shift+F1 for more help.",(buf.IsNull())?"No help is set for this property.":buf.GetHTML());
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			bool hasCaptions = false;
			int i, n = this->GetShell()->GetViewModule()->GetMarkerObject()->GetNumber();
			for(i=0; !hasCaptions && i<n; i++)
			{
				if(!this->GetShell()->GetViewModule()->GetMarkerObject()->GetMarker(i)->GetCaptionText().IsEmpty()) hasCaptions = true;
			}
			this->Enable(hasCaptions);
		}

		virtual void Execute()
		{
			this->GetShell()->GetViewModule()->GetMarkerObject()->CallMoveCaptions();
		}
	};


	class ShowRulerCheckBox : public iggWidgetPropertyControlCheckBox
	{

	public:

		ShowRulerCheckBox(iggFrame *parent) : iggWidgetPropertyControlCheckBox("Ruler",iggPropertyHandle("Visible",parent->GetShell()->GetObjectHandle("Ruler")),parent)
		{
			mDialog = new RulerScaleDialog(this->GetMainWindow());
		}

		virtual ~ShowRulerCheckBox()
		{
			delete mDialog;
		}

	protected:

		void OnVoid1Body()
		{
			if(mDialog != 0) mDialog->Show(mSubject->IsDown());
			iggWidgetPropertyControlCheckBox::OnVoid1Body();
		}

		RulerScaleDialog *mDialog;
	};


	//
	//  A small dialog with several widgets to set the record label parameters
	//
	class RecordLabelDialog : public iggDialogAuto
	{

	public:

		RecordLabelDialog(iggMainWindow *parent) : iggDialogAuto(parent,"Record Label Properties",3)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

		void CompleteConstructionBody()
		{
			iggObjectHandle *oh = this->GetShell()->GetObjectHandle("Label");
			mFrame->AddLine(new iggWidgetTextLabel("Name",mFrame),new iggWidgetPropertyControlTextLineEdit(false,"",iggPropertyHandle("Name",oh),mFrame),static_cast<iggWidget*>(0));
			mFrame->AddLine(new iggWidgetTextLabel("Unit",mFrame),new iggWidgetPropertyControlTextLineEdit(false,"",iggPropertyHandle("Unit",oh),mFrame),static_cast<iggWidget*>(0));
			mFrame->AddLine(new iggWidgetTextLabel("Scale",mFrame),new iggWidgetPropertyControlFloatLineEdit("",iggPropertyHandle("Scale",oh),mFrame),static_cast<iggWidget*>(0));
			mFrame->AddLine(new iggWidgetTextLabel("Offset",mFrame),new iggWidgetPropertyControlFloatLineEdit("",iggPropertyHandle("Offset",oh),mFrame),static_cast<iggWidget*>(0));
			mFrame->AddLine(new iggWidgetPropertyControlSpinBox(0,7,"Number of digits after the point",0,iggPropertyHandle("NumDigits",oh),mFrame),3);
			mFrame->AddSpace(10);
		}
	};


	class ShowRecordLabelCheckBox : public iggWidgetPropertyControlCheckBox
	{

	public:

		ShowRecordLabelCheckBox(iggFrame *parent) : iggWidgetPropertyControlCheckBox("Record label",iggPropertyHandle("Visible",parent->GetShell()->GetObjectHandle("Label")),parent)
		{
			mDialog = new RecordLabelDialog(this->GetMainWindow());
		}

		virtual ~ShowRecordLabelCheckBox()
		{
			delete mDialog;
		}

	protected:

		void OnVoid1Body()
		{
			if(mDialog != 0) mDialog->Show(mSubject->IsDown());
			iggWidgetPropertyControlCheckBox::OnVoid1Body();
		}

		RecordLabelDialog *mDialog;
	};


	class ClippingRangeButton : public iggWidgetSimpleButton
	{

	public:

		ClippingRangeButton(int type, iggWidgetPropertyControlCheckBox *buddy, iggFrame *parent) : iggWidgetSimpleButton("",parent,true)
		{
			mBuddy = buddy;
			mBuddy->AddDependent(this);
			mType = type;

			if(mType%2 == 0)
			{
				mSubject->SetIcon(*iggImageFactory::FindIcon("moveleft.png"));
			}
			else
			{
				mSubject->SetIcon(*iggImageFactory::FindIcon("moveright.png"));
			}
			this->SetBaloonHelp("Adjust camera clipping range","This button adjust the clipping range of the current camera.");
        }

	protected:

		virtual void UpdateWidgetBody()
		{
			bool v;
			mBuddy->QueryValue(v);
			this->Enable(!v);
		}

		virtual void Execute()
		{
			iCamera *cam = this->GetShell()->GetViewModule()->GetRenderTool()->GetCamera();
			iPair cr = cam->GetClippingRange();
			switch(mType)
			{
			case 0:
				{
					cr.X *= 0.5;
					break;
				}
			case 1:
				{
					cr.X *= 2.0;
					break;
				}
			case 2:
				{
					cr.Y *= 0.5;
					break;
				}
			case 3:
				{
					cr.Y *= 2.0;
					break;
				}
			}
			cam->SetClippingRange(cr);
			this->Render();
		}

		int mType;
		iggWidgetPropertyControlCheckBox *mBuddy;
	};


	class ClippingRangeDisplay : public iggFrameNumberDisplay, protected iggRenderWindowObserver
	{

	public:

		ClippingRangeDisplay(int type, iggFrame *parent) : iggFrameNumberDisplay("",parent), iggRenderWindowObserver(parent)
		{
			mType = type;
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			iCamera *cam = this->GetShell()->GetViewModule()->GetRenderTool()->GetCamera();
			iPair cr = cam->GetClippingRange();

			switch(mType)
			{
			case 0:
				{
					this->Display(cr.X);
					break;
				}
			case 1:
				{
					this->Display(cr.Y);
					break;
				}
			}
		}

		virtual void OnRenderWindowModified()
		{
			this->UpdateWidget();
		}

		int mType;
	};


	//
	//  Window size dialog and its widgets
	//
	class WindowSizeButton : public iggWidgetSimpleButton
	{

	public:

		WindowSizeButton(iggWidgetSimpleSpinBox **boxes, const iString &title, float scale, iggFrame *parent) : iggWidgetSimpleButton(title,parent)
		{
			mBoxes = boxes;
			mScale = scale;
			this->SetBaloonHelp(title+" the window size","This button "+title+"s the size of the current view window.");
        }

	protected:

		virtual void Execute()
		{
			int i;
			for(i=0; i<2; i++) mBoxes[i]->SetValue(iMath::Round2Int(mScale*mBoxes[i]->GetValue()));
		}

		float mScale;
		iggWidgetSimpleSpinBox **mBoxes;
	};

	class WindowSizeList : public iggWidget
	{

	public:

		WindowSizeList(iggWidgetSimpleSpinBox **boxes, iggFrame *parent) : iggWidget(parent)
		{
			mBoxes = boxes;
			mSubject = iggSubjectFactory::CreateWidgetComboBoxSubject(this,"");

			this->SetBaloonHelp("Select the size for the visualization window");
		}

	protected:

		virtual void OnInt1Body(int n)
		{
			iString s = mSubject->GetText(n);

			int j, k;
			bool ok;
			for(j=0; j<2; j++)
			{
				k = s.Section(" x ",j,j).ToInt(ok);
				if(ok) mBoxes[j]->SetValue(k);
			}
		}

		virtual void UpdateWidgetBody()
		{
		}

		iggWidgetSimpleSpinBox **mBoxes;
		ibgWidgetComboBoxSubject *mSubject;
	};

	class WindowSizeList1 : public WindowSizeList
	{

	public:

		WindowSizeList1(iggWidgetSimpleSpinBox **boxes, iggFrame *parent) : WindowSizeList(boxes,parent)
		{
			mSubject->InsertItem("Rectangle 3x2...");
			mSubject->InsertItem("300 x 200");
			mSubject->InsertItem("450 x 300");
			mSubject->InsertItem("700 x 450");
			mSubject->InsertItem("1000 x 700");
			mSubject->InsertItem("1500 x 1000");
		}
	};

	class WindowSizeList2 : public WindowSizeList
	{

	public:

		WindowSizeList2(iggWidgetSimpleSpinBox **boxes, iggFrame *parent) : WindowSizeList(boxes,parent)
		{
			mSubject->InsertItem("Square...");
			mSubject->InsertItem("200 x 200");
			mSubject->InsertItem("300 x 300");
			mSubject->InsertItem("450 x 450");
			mSubject->InsertItem("700 x 700");
			mSubject->InsertItem("1000 x 1000");
		}
	};

	class WindowSizeDialog : public iggDialog
	{

	public:

		WindowSizeDialog(iggMainWindow *parent) : iggDialog(parent,DialogFlag::Modal,0,"Visualization Window Size",0,2,"Set")
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

		void CompleteConstructionBody()
		{
			mBoxes[0] = new iggWidgetSimpleSpinBox(120,32768,1,"",mFrame);
			mBoxes[1] = new iggWidgetSimpleSpinBox(120,32768,1,"",mFrame);

			iggFrame* tmp = new iggFrame(mFrame,2);
			tmp->AddLine(new WindowSizeButton(mBoxes,"half",0.5f,tmp),new WindowSizeButton(mBoxes,"double",2.0f,tmp));
			mFrame->AddLine(tmp,2);
			mFrame->AddLine(new WindowSizeList1(mBoxes,mFrame),new WindowSizeList2(mBoxes,mFrame));
			mFrame->AddLine(new iggWidgetTextLabel("%bWidth:",mFrame),mBoxes[0]);
			mFrame->AddLine(new iggWidgetTextLabel("%bHeight:",mFrame),mBoxes[1]);
			mFrame->AddSpace(10);

			mBoxes[0]->SetBaloonHelp("Set window width","Set the width of the current visualization window.");
			mBoxes[1]->SetBaloonHelp("Set window height","Set the height of the current visualization window.");
		}

		virtual void ShowBody(bool s)
		{
			if(s)
			{
				mBoxes[0]->SetValue(this->GetShell()->GetViewModule()->GetSize(0));
				mBoxes[1]->SetValue(this->GetShell()->GetViewModule()->GetSize(1));
			}
			iggDialog::ShowBody(s);
		}

	protected:

		virtual bool CanBeClosed()
		{
			if(this->GetShell()->GetViewModule()->GetSize(0)!=mBoxes[0]->GetValue() || this->GetShell()->GetViewModule()->GetSize(1)!=mBoxes[1]->GetValue())
			{
				this->GetShell()->GetViewModule()->SetSize(0,mBoxes[0]->GetValue());
				this->GetShell()->GetViewModule()->SetSize(1,mBoxes[1]->GetValue());
			}
			return true;
		}

		iggWidgetSimpleSpinBox *mBoxes[2];
	};


	//
	//  Misc buttons
	//
	class ShowLogCheckBox : public iggWidgetSimpleCheckBox
	{

	public:

		ShowLogCheckBox(iggFrame *parent) : iggWidgetSimpleCheckBox("Show log  ",parent)
		{
			this->SetChecked(true);
			this->SetBaloonHelp("Show the log window","This check box shows/hides the log window");
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			this->SetChecked(this->GetMainWindow()->IsLogVisible());
		}

		virtual void OnChecked(bool s)
		{
			this->GetMainWindow()->ShowLog(s);
		}
	};


	class ClearLogButton : public iggWidgetSimpleButton
	{

	public:

		ClearLogButton(iggFrame *parent) : iggWidgetSimpleButton("Clear log",parent)
		{
			this->SetBaloonHelp("Clear the log window","This button removes all the text in the log window.");
        }

	protected:

		virtual void Execute()
		{
			this->GetMainWindow()->ClearLog();
		}
	};


	class MoveCameraButton : public iggWidgetSimpleButton
	{

	public:

		MoveCameraButton(int dir, iggFrame *parent) : iggWidgetSimpleButton("",parent,true)
		{
			mDir = dir;
			switch(mDir)
			{
			case -1:
				{
					mSubject->SetIcon(*iggImageFactory::FindIcon("moveleft.png"));
					break;
				}
			case 1:
				{
					mSubject->SetIcon(*iggImageFactory::FindIcon("moveright.png"));
					break;
				}
			case -2:
				{
					mSubject->SetIcon(*iggImageFactory::FindIcon("movedown.png"));
					break;
				}
			case 2:
				{
					mSubject->SetIcon(*iggImageFactory::FindIcon("moveup.png"));
					break;
				}
			default:
				{
					IBUG_WARN("Invalid direction.");
				}
			}
			this->SetBaloonHelp("Move the camera focal point","This button moves the camera focal point by 10 degrees in the specified direction.");
		}

	protected:

		virtual void Execute()
		{
			static const double angle = 10.0;
			vtkCamera *cam = this->GetShell()->GetViewModule()->GetRenderer()->GetActiveCamera();

			switch(mDir)
			{
			case -1:
				{
					cam->Yaw(angle);
					break;
				}
			case 1:
				{
					cam->Yaw(-angle);
					break;
				}
			case -2:
				{
					cam->Pitch(-angle);
					break;
				}
			case 2:
				{
					cam->Pitch(angle);
					break;
				}
			default:
				{
					IBUG_WARN("Invalid direction.");
				}
			}
			cam->OrthogonalizeViewUp();
			this->Render();
		}

		int mDir;
	};


	class StereoModeSelector : public iggWidgetPropertyControlComboBox 
	{

	public:

		StereoModeSelector(iggFrame *parent) : 
		  iggWidgetPropertyControlComboBox("Method",0,iggPropertyHandle("StereoMode",parent->GetShell()->GetViewModuleHandle()),parent)
		  {
			  this->InsertItem("Mono");
			  this->InsertItem("Dual windows");
			  this->InsertItem("Crystal eyes");
			  this->InsertItem("Blue-red");
			  this->InsertItem("Interlaced");
			  this->InsertItem("Left eye only");
			  this->InsertItem("Right eye only");
			  this->InsertItem("Dresden display");
			  this->InsertItem("Anaglyph");
			  this->InsertItem("Checkerboard");
		  }

	protected:

		virtual void UpdateWidgetBody()
		{
			this->SetItem("Crystal eyes",2,this->GetShell()->GetViewModule()->GetRenderWindow()->GetStereoCapableWindow()!=0);
		}
	};

	
	class StereoHardwareStatus : public iggFrameTextDisplay
	{

	public:

		StereoHardwareStatus(iggFrame *parent) : iggFrameTextDisplay("Stereo harware status: ",parent)
		{
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			if(this->GetShell()->GetViewModule()->GetRenderWindow()->GetStereoCapableWindow() != 0)
			{
				this->SetDisplayColor(iColor(0,200,0));
				this->Display("DETECTED");
			}
			else
			{
				this->SetDisplayColor(iColor(200,0,0));
				this->Display("NOT FOUND");
			}
		}
	};


	//
	//  Lights page
	//
	class SceneLightsPage : public iggFrameDC
	{

	public:
	
		SceneLightsPage(iggFrameBase *parent) : iggFrameDC(parent,3)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		virtual void CompleteConstructionBody()
		{
			iggObjectHandle *loh = this->GetShell()->GetObjectHandle("Lights");

			iggFrame *db = new iggFrame("Main light direction",this,1);
			db->AddLine(new iggWidgetPropertyControlTrackBall(true,iggPropertyHandle("Direction",loh),db));
			this->AddLine(db);

			iggFrame *ib = new iggFrame("Intensities",this,1);
			iggWidgetPropertyControlFloatSlider *mli = new iggWidgetPropertyControlFloatSlider(0.01,10.0,30,iStretch::Log,0,false,false,"Main light",iggPropertyHandle("Intensities",loh,0),ib);
			iggWidgetPropertyControlFloatSlider *fli = new iggWidgetPropertyControlFloatSlider(0.01,10.0,30,iStretch::Log,0,false,false,"Fill light",iggPropertyHandle("Intensities",loh,1),ib);
			iggWidgetPropertyControlFloatSlider *hli = new iggWidgetPropertyControlFloatSlider(0.01,10.0,30,iStretch::Log,0,false,false,"Head light",iggPropertyHandle("Intensities",loh,2),ib);
			hli->AddBuddy(mli);
			fli->AddBuddy(mli);
			mli->SetStretch(3,10);
			hli->SetStretch(3,10);
			fli->SetStretch(3,10);
			ib->AddLine(mli);
			ib->AddLine(fli);
			ib->AddLine(hli);
			this->AddLine(ib,2);

			this->AddSpace(2);
			this->AddLine(new iggWidgetPropertyControlCheckBox("Two sided lighting",iggPropertyHandle("TwoSided",loh),this));

			this->AddSpace(10);
			this->SetColStretch(1,10);
			this->SetColStretch(2,3);
		}
	};


	//
	//  Multi-view modes page
	//
	class SceneModesPage : public iggFrameDC
	{

	public:
	
		SceneModesPage(iggFrameBase *parent) : iggFrameDC(parent,3)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		virtual void CompleteConstructionBody()
		{
			const iImage *icon = iggImageFactory::FindIcon("view.png");

			//
			//  Book
			//
			iggFrameBook *vb = new iggFrameBook(this);
			this->AddLine(vb);
			//
			//  Stereo mode page
			//
			iggFrame *vbpage0 = new iggFrame(vb,3);
			vb->AddPage("Stereo",icon,vbpage0);
			{
				StereoModeSelector *sm = new StereoModeSelector(vbpage0);
				vbpage0->AddLine(sm,new iggWidgetTextLabel("",vbpage0));
				vbpage0->AddLine(new iggWidgetPropertyControlCheckBox("Show alighnment marks in a dual window mode",iggPropertyHandle("StereoAlignmentMarks",this->GetShell()->GetViewModuleHandle()),vbpage0),2);
				vbpage0->AddLine(new iggWidgetPropertyControlDoubleSlider(0.1,10.0,20,iStretch::Log,4,false,false,"Angle between eyes",iggPropertyHandle("EyeAngle",this->GetShell()->GetObjectHandle("Camera")),vbpage0),2);

				vbpage0->AddSpace(2);

				vbpage0->AddLine(new StereoHardwareStatus(vbpage0),2);

				vbpage0->AddSpace(10);
				vbpage0->SetColStretch(0,0);
				vbpage0->SetColStretch(1,10);
				vbpage0->SetColStretch(2,3);
			}
			//
			//  Multi view mode page
			//
			iggFrame *vbpage1 = iggExtensionFactory::CreateMultiViewFrame(vb);
			if(vbpage1 != 0) vb->AddPage("Multi view",icon,vbpage1);
		}
	};


	//
	//  Color bar page
	//
	class SceneColorBarPage : public iggFrameDC
	{

	public:
	
		SceneColorBarPage(iggFrameBase *parent) : iggFrameDC(parent,3)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		virtual void CompleteConstructionBody()
		{
			iggObjectHandle *cbh = this->GetShell()->GetObjectHandle("ColorBar");

			iggWidgetPropertyControlCheckBox *acb = new iggWidgetPropertyControlCheckBox("Automatic",iggPropertyHandle("Automatic",cbh),this);

			this->AddLine(acb,2);
			this->AddLine(new ColorBarFrame(acb,0,"Left bar",this),new ColorBarFrame(acb,1,"Right bar",this));

			this->AddSpace(10);
			iggFrame *tmp = new iggFrame(this,2);
			tmp->AddLine(new iggWidgetTextLabel("Side offset",tmp),new iggWidgetPropertyControlFloatSlider(0.01,0.316,15,iStretch::Log,0,false,false,"",iggPropertyHandle("SideOffset",cbh),tmp));
			tmp->AddLine(new iggWidgetTextLabel("Bar size",tmp),new iggWidgetPropertyControlFloatSlider(0.2,1.0,8,0,0,false,false,"",iggPropertyHandle("Size",cbh),tmp));
			tmp->SetColStretch(1,10);
			this->AddLine(tmp,3);

			this->SetColStretch(2,10);
		}
	};


	//
	//  Tools page
	//
	class ActiveClipPlaneFrame : public iggFrameActiveInstance
	{

	public:

		ActiveClipPlaneFrame(iggFrame *parent) : iggFrameActiveInstance(true,"Active clip plane","Clip plane",parent), mActivePlane(parent->GetShell())
		{
			mActivePlane = -1;
		}

		virtual bool CreateInstance()
		{
 			if(this->GetShell()->GetViewModule()->GetClipPlane()->Create.CallAction())
			{
				this->SetActiveInstance(this->GetNumberOfInstances()-1);
				return true;
			}
			else return false;
		}

		virtual bool DeleteInstance(int i)
		{
			if(this->GetShell()->GetViewModule()->GetClipPlane()->Remove.CallAction(mActivePlane))
			{
				if(mActivePlane >= this->GetNumberOfInstances()) mActivePlane = this->GetNumberOfInstances() - 1;
				mParent->UpdateWidget();
				return true;
			}
			else return false;
		}

		virtual int GetMaxNumberOfInstances() const
		{
			return 6;
		}

		virtual int GetNumberOfInstances() const
		{
			return this->GetShell()->GetViewModule()->GetClipPlane()->GetNumber();
		}
		virtual int GetActiveInstance() const
		{
			return mActivePlane;
		}

		virtual void SetActiveInstance(int n)
		{
			if(n>=0 && n<this->GetNumberOfInstances())
			{
				mActivePlane = n;
				mParent->UpdateWidget();
			}
		}

	protected:

		virtual void UpdateWidget()
		{
		}

		iggIndex mActivePlane;
	};
		
	
	class ActiveClipPlaneHandle : public iggIndexHandle
	{

	public:

		ActiveClipPlaneHandle(ActiveClipPlaneFrame *frame)
		{
			mFrame = frame; IASSERT(mFrame);
		}

		virtual int Index() const
		{
			return mFrame->GetActiveInstance();
		}

	private:

		ActiveClipPlaneFrame *mFrame;
	};


	class SceneToolsPage : public iggFrameDC
	{

	public:
	
		SceneToolsPage(iggFrameBase *parent) : iggFrameDC(parent,3)
		{
			mClipPlaneHandle = 0;

			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

		virtual ~SceneToolsPage()
		{
			if(mClipPlaneHandle != 0) delete mClipPlaneHandle;
		}

	protected:

		ActiveClipPlaneHandle *mClipPlaneHandle;

		virtual void CompleteConstructionBody()
		{
			//
			//  Clip plane
			//
			iggObjectHandle *cph = this->GetShell()->GetObjectHandle("ClipPlane");

			iggFrame *cpf = new iggFrame("Clip planes",this,2);

			ActiveClipPlaneFrame *cpi = new ActiveClipPlaneFrame(cpf);
			mClipPlaneHandle = new ActiveClipPlaneHandle(cpi);

			cpf->AddLine(cpi,new iggWidgetTextLabel("",cpf));//(iggWidget *)0);
			cpf->AddSpace(2);

			cpf->AddLine(new iggFrameObjectControls("Clip plane",iggPropertyHandle("Position",cph,mClipPlaneHandle),iggPropertyHandle("Direction",cph,mClipPlaneHandle),cpf),2);
			cpf->AddLine(new iggWidgetPropertyControlCheckBox("Show as glass planes",iggPropertyHandle("Visible",cph),cpf),2);
			cpf->SetColStretch(0,0);
			cpf->SetColStretch(1,3);

			this->AddLine(cpf);
			this->AddSpace(2);

			//
			//  Measuring box
			//
			iggFrame *mbf = new iggFrame("Measuring box",this,3);
			mbf->AddLine(new iggWidgetPropertyControlCheckBox("Show measuring box",iggPropertyHandle("Visible",this->GetShell()->GetObjectHandle("MeasuringBox")),mbf));
			this->AddLine(mbf);

			this->AddSpace(10);
			this->SetColStretch(0,10);
			this->SetColStretch(1,3);
		}
	};

	//
	//  Scene page
	// ************************************************
	//
	class ScenePage : public iggFrameDC
	{

	public:
	
		ScenePage(iggFrameBase *parent) : iggFrameDC(parent,1)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		virtual void CompleteConstructionBody()
		{
			const iImage *icon = iggImageFactory::FindIcon("view.png");

			//
			//  Book
			//
			iggFrameBook *sb = new iggFrameBook(this);
			this->AddLine(sb);
			//
			//  Camera page (created immediately)
			//
			iggFrame *sbpage0 = new iggFrame(sb,3);
			sb->AddPage("Camera",icon,sbpage0);
			{
				iggObjectHandle *camh = this->GetShell()->GetObjectHandle("Camera");

				iggFrame *vf = new iggFrame("View angle",sbpage0,1);
				vf->AddSpace(1);
				vf->AddLine(new iggWidgetPropertyControlDoubleSlider(10.0,120.0,11,0,3,false,false,"",iggPropertyHandle("ViewAngle",camh),vf));
				vf->AddLine(new iggWidgetPropertyControlCheckBox("Vertical",iggPropertyHandle("ViewAngleVertical",camh),vf));
				vf->AddSpace(10);

				iggFrame *cb = new iggFrame("Camera orientation",sbpage0,3);
				cb->AddLine(static_cast<iggWidget*>(0),new MoveCameraButton(2,cb),static_cast<iggWidget*>(0));
				cb->AddLine(new MoveCameraButton(-1,cb),static_cast<iggWidget*>(0),new MoveCameraButton(1,cb));
				cb->AddLine(static_cast<iggWidget*>(0),new MoveCameraButton(-2,cb),static_cast<iggWidget*>(0));

				sbpage0->AddLine(vf,cb);
				sbpage0->AddSpace(2);

				iggFrame *crf = new iggFrame("Camera clipping range",sbpage0,5);
				iggWidgetPropertyControlCheckBox *cfa = new iggWidgetPropertyControlCheckBox("Automatic",iggPropertyHandle("ClippingRangeAuto",camh),crf);
				crf->AddLine(cfa,3);
				crf->AddLine(new ClippingRangeButton(0,cfa,crf),new ClippingRangeButton(1,cfa,crf),new iggWidgetTextLabel("Adjust clipping range",crf),new ClippingRangeButton(2,cfa,crf),new ClippingRangeButton(3,cfa,crf));
				crf->AddLine(new ClippingRangeDisplay(0,crf),2,new iggWidgetTextLabel("(in units of focal distance)",crf),1,new ClippingRangeDisplay(1,crf),2);
				sbpage0->AddLine(crf,2);
				sbpage0->AddSpace(2);

				sbpage0->AddSpace(4);
				sbpage0->SetColStretch(2,10);
			}
			//
			//  Lights page
			//
			SceneLightsPage *sbpage1 = new SceneLightsPage(sb);
			sb->AddPage("Lights",icon,sbpage1);

			//
			//  Multi-view modes page
			//
			SceneModesPage *sbpage2 = new SceneModesPage(sb);
			sb->AddPage("Modes",icon,sbpage2);

			//
			//  Color bars page
			//
			SceneColorBarPage *sbpage3 = new SceneColorBarPage(sb);
			sb->AddPage("Color bars",icon,sbpage3);

			//
			//  Tools page
			//
			SceneToolsPage *sbpage4 = new SceneToolsPage(sb);
			sb->AddPage("Tools",icon,sbpage4);
		}
	};


	//
	//  Animation page
	// ************************************************
	//
	class AnimationPage : public iggFrameDC
	{

	public:
	
		AnimationPage(iggFrameBase *parent) : iggFrameDC(parent,1)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		virtual void CompleteConstructionBody()
		{
			const iImage *icon = iggImageFactory::FindIcon("view.png");

			iggObjectHandle *animh = this->GetShell()->GetObjectHandle("Animator");

			//
			//  Book
			//
			iggFrameBook *ab = new iggFrameBook(this);
			this->AddLine(ab);
			//
			//  Style page
			//
			iggFrame *abpage0 = new iggFrame(ab,3);
			ab->AddPage("Style",icon,abpage0);
			{
				AnimationStyleBox *as = new AnimationStyleBox(abpage0);
				as->InsertItem("Static");
				as->InsertItem("Rotate/scale");
				as->InsertItem("Tumble");

				iggFrame *tmp = new iggFrame(abpage0);
				iggWidgetPropertyControlRadioBox *ao = new iggWidgetPropertyControlRadioBox(1,"Movie output",0,iggPropertyHandle("MovieOutput",this->GetShell()->GetObjectHandle("ImageWriter")),tmp);
				ao->InsertItem("Series of images");
				ao->InsertItem("MPEG movie");
#ifndef IVTK_SUPPORTS_MPEG
				ao->SetItem("MPEG movie",1,false); //  disable MPEG generation for VTK 5.0.1 due to a bug in vtkMPEGWriter
#endif

				ao->InsertItem("AVI movie");
#ifndef IVTK_SUPPORTS_AVI
				ao->SetItem("AVI movie",2,false);
#endif

				tmp->AddLine(ao);
				tmp->AddSpace(10);
				abpage0->AddLine(as,tmp);

				abpage0->AddSpace(10);

				AnimationFramesSlider *af = new AnimationFramesSlider(abpage0);
				af->SetRange(1,150,1);
				abpage0->AddLine(af,2);

				abpage0->AddSpace(10);

				abpage0->AddLine(new iggWidgetPropertyControlFileNameLineEdit(false,"IFrIT Animation Output",this->GetShell()->GetEnvironment(Environment::Image),"(*)",false,"Destination",false,iggPropertyHandle("MovieRootName",this->GetShell()->GetObjectHandle("ImageWriter")),abpage0),2);

				abpage0->AddSpace(10);

				iggFrame *abut = new iggFrame(abpage0,3);
				abut->AddLine(0,new AnimateButton(abut));
				abut->SetColStretch(0,10);
				abut->SetColStretch(2,10);
				abpage0->AddLine(abut,3);

				abpage0->SetColStretch(1,10);
				abpage0->SetColStretch(2,3);
			}
			//
			//  Settings page
			//
			iggFrame *abpage1 = new iggFrame(ab,2);
			ab->AddPage("Settings",icon,abpage1);
			{
				iggWidgetPropertyControlFloatSlider *tmpfs;

				iggFrame *ar = new iggFrame("Rotations",abpage1,1);
				tmpfs = new iggWidgetPropertyControlFloatSlider(-1.0,1.0,20,0,4,false,false,"Azimuth (phi)",iggPropertyHandle("Phi",animh),ar,RenderMode::DoNotRender);
				tmpfs->SetStretch(3,10,0);
				ar->AddLine(tmpfs);
				tmpfs = new iggWidgetPropertyControlFloatSlider(-1.0,1.0,20,0,4,false,false,"Elevation (theta)",iggPropertyHandle("Theta",animh),ar,RenderMode::DoNotRender);
				tmpfs->SetStretch(3,10,0);
				ar->AddLine(tmpfs);
				tmpfs = new iggWidgetPropertyControlFloatSlider(-1.0,1.0,20,0,4,false,false,"Roll angle",iggPropertyHandle("Roll",animh),ar,RenderMode::DoNotRender);
				tmpfs->SetStretch(3,10,0);
				ar->AddLine(tmpfs);
				abpage1->AddLine(ar);
				abpage1->AddLine(new iggWidgetPropertyControlFloatSlider(0.98,1.02,40,1,5,false,false,"Zoom",iggPropertyHandle("Zoom",animh),abpage1,RenderMode::DoNotRender));

				abpage1->AddSpace(3);

				iggWidgetPropertyControlSpinBox *tmpsb = new iggWidgetPropertyControlSpinBox(0,999,"Number of blended frames",0,iggPropertyHandle("NumberOfBlendedFrames",animh),abpage1,RenderMode::DoNotRender);
				tmpsb->SetStretch(10,0);
				abpage1->AddLine(tmpsb);
				tmpsb = new iggWidgetPropertyControlSpinBox(0,999,"Number of transition frames",0,iggPropertyHandle("NumberOfTransitionFrames",animh),abpage1,RenderMode::DoNotRender);
				tmpsb->SetStretch(10,0);
				abpage1->AddLine(tmpsb);

				abpage1->AddSpace(10);
				abpage1->SetColStretch(0,10);
				abpage1->SetColStretch(1,3);
			}
			//
			//  Title/logo page
			//
			iggFrame *abpage4 = new iggFrame(ab,2);
			ab->AddPage("Title/logo",icon,abpage4);
			{
				iggFrame *atbox = new iggFrame("Title",abpage4,3);
				iggWidgetPropertyControlFileNameLineEdit *atle = new iggWidgetPropertyControlFileNameLineEdit(false,"IFrIT Title Page File",this->GetMainWindow()->GetShell()->GetEnvironment(Environment::Base),"Images (*.jpg *.jpeg *.pnm *.bmp *.png *.tif *.tiff)",true,"",true,iggPropertyHandle("TitlePageFile",animh),atbox);
				atbox->AddLine(atle,3);

				iggWidgetPropertyControlSpinBox *tmpsb = new iggWidgetPropertyControlSpinBox(0,1000,"Frames to show title",0,iggPropertyHandle("NumberOfTitlePageFrames",animh),atbox,RenderMode::DoNotRender);
				tmpsb->SetStretch(10,0);
				atbox->AddLine(tmpsb,2);
				tmpsb = new iggWidgetPropertyControlSpinBox(0,1000,"Frames to dissolve title",0,iggPropertyHandle("NumberOfTitlePageBlendedFrames",animh),atbox,RenderMode::DoNotRender);
				tmpsb->SetStretch(10,0);
				atbox->AddLine(tmpsb,2);

				atbox->SetColStretch(1,1);
				atbox->SetColStretch(2,10);
				abpage4->AddLine(atbox);

				iggFrame *albox = new iggFrame("Logo",abpage4,3);
				iggWidgetPropertyControlFileNameLineEdit *alle = new iggWidgetPropertyControlFileNameLineEdit(false,"IFrIT Logo File",this->GetMainWindow()->GetShell()->GetEnvironment(Environment::Base),"Images (*.jpg *.jpeg *.pnm *.bmp *.png *.tif *.tiff)",true,"",true,iggPropertyHandle("LogoFile",animh),albox);
				//avb = new iggWidgetLaunchButton(static_cast<iggDialog*>(0),"View",parent,trueViewButton(alia,alf);
				//alf->AddLine(arb,avb);
				albox->AddLine(alle,3);

				iggWidgetPropertyControlComboBox *tmpcb = new iggWidgetPropertyControlComboBox("Location",0,iggPropertyHandle("LogoLocation",animh),albox,RenderMode::DoNotRender);
				tmpcb->InsertItem("Upper left");
				tmpcb->InsertItem("Upper right");
				tmpcb->InsertItem("Lower left");
				tmpcb->InsertItem("Lower rigt");
				tmpcb->SetStretch(10,0);
				albox->AddLine(tmpcb,2);
				iggWidgetPropertyControlOpacitySlider *tmpfs = new iggWidgetPropertyControlOpacitySlider(iggPropertyHandle("LogoOpacity",animh),albox,RenderMode::DoNotRender);
				tmpfs->SetStretch(10,0);
				albox->AddLine(tmpfs,2);

				albox->SetColStretch(1,1);
				albox->SetColStretch(2,10);
				abpage4->AddLine(albox);
				abpage4->AddSpace(10);
				abpage4->SetColStretch(0,10);
				abpage4->SetColStretch(1,3);
			}
		}
	};


	//
	//  Markers page
	// ************************************************
	//
	class MarkersPage : public iggFrameDC
	{

	public:
	
		MarkersPage(iggFrameBase *parent) : iggFrameDC(parent,3)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		virtual void CompleteConstructionBody()
		{
			int i;
			const iImage *icon = iggImageFactory::FindIcon("view.png");

			iggObjectHandle *mh = this->GetShell()->GetObjectHandle("Marker");

			//
			//  Book
			//
			iggFrameBook *mb = new iggFrameBook(this);
			this->AddLine(mb);
			//
			//  Main page
			//
			iggFrame *mbpage0 = new iggFrame(mb,3);
			mb->AddPage("Main",icon,mbpage0);

			ActiveMarkerFrame *mcm = new ActiveMarkerFrame(mbpage0);
			mcm->AddDependent(mbpage0);
			mbpage0->AddLine(mcm,2);
			
			{
				iggFrame *msb = new MarkerPropertiesFrame(mbpage0);

				iggWidgetPropertyControlComboBox *mcs = new iggWidgetPropertyControlComboBox("Type",0,iggPropertyHandle("Type",mh,mcm->GetIndexHandle()),msb);
				for(i=0; i<PointGlyphType::SIZE; i++)
				{
					mcs->InsertItem(iPointGlyph::GetName(i));
				}
				msb->AddLine(mcs,new iggWidgetPropertyControlColorSelection(iggPropertyHandle("Color",mh,mcm->GetIndexHandle()),msb));
				msb->AddLine(new iggWidgetPropertyControlSizeSlider(1.0e-5,0,"Size",iggPropertyHandle("Size",mh,mcm->GetIndexHandle()),msb),2);
				msb->SetColStretch(0,3);

				iggFrame *mfc = new iggFrame("Caption",mbpage0,1);
				iggWidget *mct = new iggWidgetPropertyControlTextLineEdit(false,"",iggPropertyHandle("CaptionText",mh,mcm->GetIndexHandle()),mfc);
				mfc->AddLine(mct);
				iggWidget *mmc = new MarkerMoveCaptionsButton(mfc);
				mct->AddDependent(mmc);
				mfc->AddLine(mmc);
				mfc->AddSpace(10);

				mbpage0->AddLine(msb,2,mfc,1);


				mbpage0->AddLine(new iggFramePosition("Position",iggPropertyHandle("Position",mh,mcm->GetIndexHandle()),mbpage0),2);
				mbpage0->AddLine(new iggWidgetPropertyControlOpacitySlider(iggPropertyHandle("Opacity",mh,mcm->GetIndexHandle()),mbpage0),2);

				mbpage0->AddLine(new iggWidgetPropertyControlCheckBox("Scale marker to keep size fixed",iggPropertyHandle("Scaled",mh,mcm->GetIndexHandle()),mbpage0),3);
				mbpage0->AddLine(new iggWidgetPropertyControlCheckBox("Captions are transparent",iggPropertyHandle("TransparentCaptions",mh),mbpage0),3);

				mbpage0->AddSpace(10);
				mbpage0->SetColStretch(1,10);
				mbpage0->SetColStretch(3,3);
			}
			//
			//  Legend page
			//
			iggFrame *mbpage1 = new iggFrame(mb,2);
			mb->AddPage("Legend",icon,mbpage1);
			{
				mbpage1->AddLine(new iggWidgetPropertyControlCheckBox("Show legend",iggPropertyHandle("Legend",mh),mbpage1));
				iggWidgetPropertyControlRadioBox *mlp = new iggWidgetPropertyControlRadioBox(1,"Legend position",0,iggPropertyHandle("LegendPosition",mh),mbpage1);
				mlp->InsertItem("Lower right corner");
				mlp->InsertItem("Lower left corner");

				mbpage1->AddLine(mlp);

				mbpage1->AddSpace(10);
				mbpage1->SetColStretch(1,10);
			}
 			//
			//  Material page
			//
			iggFrame *mbpage2 = new iggFrame(mb);
			mb->AddPage("Material ",icon,mbpage2);
			{
				mbpage2->AddLine(new iggFrameMaterialProperties(mh,mbpage2));

				mbpage2->AddSpace(10);
				mbpage2->SetColStretch(0,10);
				mbpage2->SetColStretch(1,3);
			}
			//
			//  Transform page
			//
			//  do we really need this?
			//iggFrame *mbpage3 = new iggFrame(mb,2);
			//mb->AddPage("Transform",icon,mbpage3);
			//{
			//	iggFrame *mtb = new iggFrame("Scale",mbpage3,1);
			//	mtb->AddLine(new iggWidgetPropertyControlFloatSlider(0.01f,100.0f,200,1,5,"X",iggPropertyHandle("Transform(),RenderMode::UseGlobal,mtb,3));
			//	mtb->AddLine(new iggWidgetPropertyControlFloatSlider(0.01f,100.0f,200,1,5,"Y",iggPropertyHandle("Transform(),RenderMode::UseGlobal,mtb,4));
			//	mtb->AddLine(new iggWidgetPropertyControlFloatSlider(0.01f,100.0f,200,1,5,"Z",iggPropertyHandle("Transform(),RenderMode::UseGlobal,mtb,5));
			//	mbpage3->AddLine(mtb);
			//
			//	mtb = new iggFrame("Rotate",mbpage3,1);
			//	mtb->AddLine(new iggWidgetPropertyControlFloatSlider(-180.0,180.0,360,0,5,"X",iggPropertyHandle("Transform(),RenderMode::UseGlobal,mtb,0));
			//	mtb->AddLine(new iggWidgetPropertyControlFloatSlider(-180.0,180.0,360,0,5,"Y",iggPropertyHandle("Transform(),RenderMode::UseGlobal,mtb,1));
			//	mtb->AddLine(new iggWidgetPropertyControlFloatSlider(-180.0,180.0,360,0,5,"Z",iggPropertyHandle("Transform(),RenderMode::UseGlobal,mtb,2));
			//	mbpage3->AddLine(mtb);
			//
			//	mbpage3->AddSpace(10);
			//	mbpage3->SetColStretch(0,10);
			//	mbpage3->SetColStretch(1,3);
			//}
		}
	};


	//
	//  Windows page
	// ************************************************
	//
	class WindowsPage : public iggFrameDC
	{

	public:
	
		WindowsPage(iggFrameBase *parent, iggPageView *page) : iggFrameDC(parent,3)
		{
			mPage = page;
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		iggPageView *mPage;

		virtual void CompleteConstructionBody()
		{
			iggFrame *wfb = new iggFrame("Windows",this,1);
			WindowNumberComboBox *wnb = new WindowNumberComboBox(wfb,mPage);
			wfb->AddLine(wnb);
			CreateDeleteWindowButton *wcb1 = new CreateDeleteWindowButton(0,wfb);
			wcb1->AddDependent(wnb);
			wfb->AddLine(wcb1);
			CreateDeleteWindowButton *wcb2 = new CreateDeleteWindowButton(1,wfb);
			wcb2->AddDependent(wnb);
			wfb->AddLine(wcb2);
			CreateDeleteWindowButton *wcb3 = new CreateDeleteWindowButton(2,wfb);
			wcb3->AddDependent(wnb);
			wfb->AddLine(wcb3);
			CreateDeleteWindowButton *wdb = new CreateDeleteWindowButton(-1,wfb);
			wdb->AddDependent(wnb);
			wfb->AddLine(wdb);
			wcb1->AddDependent(wdb);
			wcb2->AddDependent(wdb);
			wcb3->AddDependent(wdb);

			WindowStatusIcon *wsi = new WindowStatusIcon(this);
			wnb->AddDependent(wsi);
			wcb1->AddDependent(wsi);
			wcb2->AddDependent(wsi);
			wcb3->AddDependent(wsi);
			wdb->AddDependent(wsi);

			this->AddLine(wfb,wsi);

			this->AddLine(new CopyFromWindowComboBox(this));
			this->AddSpace(2);

			this->AddLine(new WidgetsControlAllWindowsCheckBox(this));
			this->AddLine(new WindowUnderFocusActiveCheckBox(this));
			this->AddLine(new iggWidgetPropertyControlCheckBox("Synchronize window interactors",iggPropertyHandle("SynchronizeInteractors",this->GetShell()->GetShellHandle()),this));
			this->AddLine(new SynchronizeCamerasExecButton(this));

			this->AddSpace(10);
			this->SetColStretch(2,10);
		}
	};
};


using namespace iggPageView_Private;


iggPageView::iggPageView(iggFrameBase *parent) : iggPageMain(parent,iggPage::WithBook)
{
	const iImage *icon = iggImageFactory::FindIcon("view.png");

	mWindowSizeDialog = new WindowSizeDialog(this->GetMainWindow());
	mWindowListDialog = new WindowListDialog(this->GetMainWindow());

	//
	//  Main page (always shown at start-up, hence no delayed construction)
	// ************************************************
	//
	iggFrame *page0 = new iggFrame(mBook,4);
	mBook->AddPage("Main",icon,page0);
	{
		page0->AddSpace(2);

		iggWidgetPropertyControlTextComboBox *ur = new iggWidgetPropertyControlTextComboBox(0,"Update rate",0,iggPropertyHandle("UpdateRate",this->GetShell()->GetViewModuleHandle()),page0);
		ur->InsertItem("Non-interactive");
		ur->InsertItem(" 3 frames/sec");
		ur->InsertItem("10 frames/sec");
		ur->InsertItem("30 frames/sec");
		ur->SetInvalidValue(0);
		page0->AddLine(ur,2);
		page0->AddSpace(2);

		iggFrame *pf = new iggFrame("Projection",page0,1);
		pf->AddLine(new iggWidgetPropertyControlCheckBox("Parallel",iggPropertyHandle("ParallelProjection",this->GetShell()->GetObjectHandle("Camera")),pf));
		iggWidgetPropertyControlCheckBox *pp = new iggWidgetPropertyControlCheckBox("Perspective",iggPropertyHandle("ParallelProjection",this->GetShell()->GetObjectHandle("Camera")),pf);
		pf->AddLine(pp);
		pp->SetReverse(true);

		page0->AddLine(pf);

		page0->AddLine(new iggWidgetActionControlExecButton("Reset view",iggPropertyHandle("Reset",this->GetShell()->GetObjectHandle("Camera")),page0),new iggWidgetLaunchButton(mWindowSizeDialog,"Set window size",page0));
		page0->AddSpace(2);

		iggFrame *fb = new iggFrame("Features",page0,1);
		fb->AddLine(new iggWidgetPropertyControlCheckBox("Bounding box",iggPropertyHandle("Visible",this->GetShell()->GetObjectHandle("BoundingBox")),fb));
		fb->AddLine(new ShowRecordLabelCheckBox(fb));
		fb->AddLine(new ShowRulerCheckBox(fb));
		fb->AddLine(new iggWidgetPropertyControlCheckBox("Color bars",iggPropertyHandle("Visible",this->GetShell()->GetObjectHandle("ColorBar")),fb));
		fb->AddLine(new iggWidgetPropertyControlCheckBox("Alignment axes",iggPropertyHandle("CameraAlignmentLabel",this->GetShell()->GetViewModuleHandle()),fb));

		iggFrame *fb2 = new iggFrame(page0,1);
		iggWidgetPropertyControlRadioBox *isb = new iggWidgetPropertyControlRadioBox(1,"Interactor style",0,iggPropertyHandle("CurrentInteractorStyle",this->GetShell()->GetViewModuleHandle()),fb2);
		isb->InsertItem("Trackball");
		isb->InsertItem("Joystick");
		isb->InsertItem("Flight");
		isb->InsertItem("Keyboard");
		fb2->AddLine(isb);

		page0->AddLine(fb,fb2);

		fb = new iggFrame(page0,2);
		fb->AddLine(new iggWidgetTextLabel("Wall paper",fb),new iggWidgetPropertyControlFileNameLineEdit(true,"Load wallpaper image",this->GetShell()->GetEnvironment(Environment::Script),"Images (*.jpg *.jpeg *.pnm *.bmp *.png *.tif *.tiff)",true," ",true,iggPropertyHandle("BackgroundImage",this->GetShell()->GetViewModuleHandle()),fb));
		fb->AddLine(new iggWidgetTextLabel("Image root",fb),new iggWidgetPropertyControlFileNameLineEdit(false,"IFrIT Image Output",this->GetShell()->GetEnvironment(Environment::Image),"(*)",false," ",false,iggPropertyHandle("ImageRootName",this->GetShell()->GetObjectHandle("ImageWriter")),fb));
		fb->SetColStretch(0,0);
		fb->SetColStretch(1,3);
		fb2 = new iggFrame(page0,1);
		iggWidgetPropertyControlColorSelection *bgc = new iggWidgetPropertyControlColorSelection(iggPropertyHandle("BackgroundColor",this->GetShell()->GetViewModuleHandle()),fb2);
		if(this->GetMainWindow()->GetDialogImageComposer() != 0) bgc->AddDependent(this->GetMainWindow()->GetDialogImageComposer()->GetArea());
		fb2->AddLine(bgc);
		fb2->AddSpace(10);

		page0->AddLine(fb,2,fb2,1);

#ifdef I_DEBUG
		if(!this->GetShell()->CheckCondition(iParameter::Condition::DisableDelayedInitialization))
		{
			page0->AddLine(new iggWidgetTextLabel(" ",page0),3);
			page0->AddLine(new iggWidgetTextLabel(" ",page0),3);
			page0->AddLine(new iggWidgetTextLabel(" ",page0),3);
			page0->AddLine(new iggWidgetTextLabel("__________________________________________________________",page0),3);
		}
#endif

		page0->AddSpace(10);
		page0->SetColStretch(3,10);
	}

	//
	//  Scene page
	// ************************************************
	//
	iggFrame *page1 = new ScenePage(mBook);
	mBook->AddPage("Scene",icon,page1);

	//
	//  Animation page
	// ************************************************
	//
	iggFrame *page2 = new AnimationPage(mBook);
	mBook->AddPage("Animation",icon,page2);

	//
	//  Markers page
	// ************************************************
	//
	iggFrame *page3 = new MarkersPage(mBook);
	mBook->AddPage("Markers",icon,page3);

	//
	//  Windows page
	// ************************************************
	//
	iggFrame *page4 = new WindowsPage(mBook,this);
	mBook->AddPage("Windows",icon,page4);
}


iggPageView::~iggPageView()
{
	delete mWindowSizeDialog;
	delete mWindowListDialog;
}


void iggPageView::UpdateMarkerWidgets()
{
	if(mBook->GetPage(3) != 0) mBook->GetPage(3)->UpdateWidget();
}


iggFrame* iggPageView::CreateFlipFrame(iggFrameBase *parent)
{
	iggFrame *tmp = new iggFrame(parent,3);

	tmp->AddSpace(1);
	tmp->AddLine(0,new ShowLogCheckBox(tmp),new ClearLogButton(tmp));
	tmp->AddSpace(1);
	tmp->SetColStretch(0,10);

	return tmp;
}

#endif
