/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Wrappers around iggWidgetTextEditor
//

#ifndef IGGWIDGETTEXT_H
#define IGGWIDGETTEXT_H


#include "iggwidget.h"


#include "iarray.h"
#include "icolor.h"
#include "istring.h"

class ibgWidgetHelpBrowserSubject;
class ibgWidgetTextEditorSubject;


namespace iParameter
{
	namespace TextEditorFlag
	{
		//
		//  Text editor masks
		//
		const unsigned int ReadOnly = 1U;
		const unsigned int WithHTML = 2U;
		const unsigned int Wrapping = 4U;
	};

	namespace TextEditorFunction
	{
		//
		//  Text editor capabilities
		//
		const int Undo = 1;
		const int Redo = 2;
		const int Cut = 3;
		const int Copy = 4;
		const int Paste = 5;
	}
};


class iggWidgetTextEditor : public iggWidget
{

	friend class ibgWidgetTextEditorSubject;

public:

	iggWidgetTextEditor(unsigned int mode, iggFrame *parent);
	virtual ~iggWidgetTextEditor();

//	bool AppendBreaksLine() const;
	bool SupportsHTML() const;
	bool TextModified() const;

	//
	//  Text manipulations
	//
	void Clear();
	void SetModified(bool s);
	virtual void SetText(const iString &text);
	iString GetText() const;
	void AdjustFontSize(int val);

	//
	//  Line-by-line manipulation
	//
	int GetNumberOfLines() const;
	void RemoveLine(int n);
	iString GetLine(int n) const;

	//
	//  Text attribute manipulations
	//
//	void SetBoldText(bool s);
//	void SetTextColor(const iColor &color);
//	void SetPointSize(int s);

	//
	//  Selection
	//
//	void Select(int lineFrom, int indexFrom, int lineTo, int indexTo, int selNum = 0);

	//
	//  Editing functions
	//
	bool HasEditingFunction(int type) const;
	void UseEditingFunction(int type);

protected:

	virtual void UpdateWidgetBody();

	virtual void OnTextChanged(){}
	virtual void OnSelectionChanged(int /*lineFrom*/, int /*indexFrom*/, int /*lineTo*/, int /*indexTo*/){}
	virtual void OnCursorPositionChanged(int /*line*/, int /*index*/){}
	virtual void OnMousePress(int /*line*/, int /*index*/){}
	virtual void OnMouseRelease(int /*line*/, int /*index*/){}
	virtual void OnReturnPressed(){}

	ibgWidgetTextEditorSubject *mSubject;
};


class iggWidgetTextBrowser : public iggWidgetTextEditor
{

public:

	iggWidgetTextBrowser(bool wrapping, bool blockselection, iggFrame *parent, bool withHTML = true);

	void AppendTextLine(const iString &text, const iColor &textcolor = iColor(0,0,0));
	void AppendTextLine(const iString &prefix, const iString &text, const iColor &textcolor = iColor(0,0,0), const iColor &prefixcolor = iColor(0,0,0), bool prefixbold = true);

protected:

	virtual void OnSelectionChanged(int lineFrom, int indexFrom, int lineTo, int indexTo);

	bool mBlockSelection;
};


class iggWidgetListView : public iggWidgetTextEditor
{

public:

	iggWidgetListView(bool withHTML, iggFrame *parent);

	void InsertItem(const iString &text, const iColor &color = iColor(0,0,0));
	iString GetItem(int n) const;
	inline int Count() const { return mNumItems; }

	inline const int* GetSelectedRange() const { return mSelectedRange; }
	inline bool HasSelection() const { return mSelectedRange[0] <= mSelectedRange[1]; }

	virtual void Clear();

protected:

	virtual void UpdateWidgetBody();
	virtual void Select(int lineFrom, int lineTo);

	virtual void OnSelectionChanged(int lineFrom, int indexFrom, int lineTo, int indexTo);
	virtual void OnCursorPositionChanged(int line, int index);

	int mSelectedRange[2];
	int mNumItems;
};


//
//  Help browser with separate subject (if the GUI toolkit supports it)
//  We use protected inheritance to hide all iggWidgetTextBrowser functions, since we
//  are going to set mSubject to zero, which is used in all those functions
//
class iggWidgetHelpBrowser : protected iggWidgetTextBrowser
{

	friend class iggSubjectFactory;
	friend class ibgWidgetHelpBrowserSubject;

public:

	iggWidgetHelpBrowser(iggFrame *parent);

	//
	//  Need these two because of protected inheritance
	//
	void AttachSubject(ibgWidgetSubject *subject){ this->iggWidgetTextBrowser::AttachSubject(subject); }
	iggShell* GetShell() const { return this->iggWidgetTextBrowser::GetShell(); }

	//
	//  Overloads that give access to inherited but protected members
	//
	inline iggFrameBase* GetParent() const { return mParent; }
	inline iggWidget* Self(){ return this; }
	void AddDependent(iggWidget *c);

	void SetContents(const char *tag, bool resetHistory = true);

	void Home();
	void Forward();
	void Backward();

	inline bool CanGoForward() const { return mCurrentTag>=0 && mCurrentTag < mVisitedTags.MaxIndex(); }
	inline bool CanGoBackward() const { return mCurrentTag>0 && mCurrentTag < mVisitedTags.Size(); }

	inline bool IsDummy() const { return mHTMLSubject == 0; }

protected:

	void SetContentsText(const iString &text);
	iString GetContentsText() const;

	//
	//  Customization engine
	//
	virtual void OnLinkFollowed(const char *tag);
	virtual void OnNullHelpBuffer();
	virtual void ModifyText(const char *tag, iString &text);
	//virtual void OnMousePress(int line, int index);
	//virtual void OnMouseRelease(int line, int index);

	int mCurrentTag;
	iArray<iString> mVisitedTags;
	ibgWidgetHelpBrowserSubject *mHTMLSubject;
};

#endif  // IGGWIDGETTEXT_H

