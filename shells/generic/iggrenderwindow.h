/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/
  
//
//  Simple generic wrapper around vtkOpenGLRenderWindow.
//  Very little functionality can be done genericaly for RenderWindow, so
//  this class is mostly for the future development.
//


#ifndef IGGRENDERWINDOW_H
#define IGGRENDERWINDOW_H


#include <vtkOpenGLRenderWindow.h>
#include "iviewmodulecomponent.h"


class iggMainWindow;
class iggShell;

class ibgRenderWindowSubject;


class iggRenderWindow : public vtkOpenGLRenderWindow, public iViewModuleComponent
{

	friend class iggShell;
	friend class iggMainWindow;
	friend class ibgRenderWindowSubject;

public:

	vtkTypeMacro(iggRenderWindow,vtkOpenGLRenderWindow);

	//
	//  Rendering process
	//
	virtual void Start();
	virtual void Render();
	virtual void Frame();
	virtual void MakeCurrent();
	inline bool IsInitialized() const { return mInitialized; }

	//
	//  Size and position
	//
	virtual int* GetPosition();
	virtual int* GetSize();

	virtual void SetSize(int,int);
	virtual void SetPosition(int,int);

	virtual void SetBorders(int);
	virtual void SetFullScreen(int);
	virtual int* GetScreenSize();

	//
	//  Window system access
	//
	virtual void *GetGenericDisplayId();
	virtual void *GetGenericWindowId();
	virtual void SetWindowName(const char *s);

	ibgRenderWindowSubject* GetSubject() const { return mSubject; }
	void OnCloseAttempt();

	virtual bool IsCurrent();
	virtual int IsDirect();

	//
	//  Interaction with the GUI stereo capabilities
	//
	virtual void SetStereoCapableWindow(int capable);

	//
	//  Shell-specific
	//
	void UpdateActiveStatus(bool last);
	bool IsActiveWindow() const;

protected:

	iggRenderWindow(iViewModule *vm);
	virtual ~iggRenderWindow();

	void AttachToMainWindow(iggMainWindow *mw);

	void Initialize();
	void Finalize();

	//
	//  These functions are helpers for image creation
	//
	void RenderIntoMemory();

	iggMainWindow* GetMainWindow() const { return mMainWindow; }

	//
	//  Work variables
	//
	int mScreenSize[2];
	int mMaxStep, mCurStep;
	bool mInitialized, mResizing, mMoving;
	iggMainWindow *mMainWindow;

	ibgRenderWindowSubject *mSubject;

	iggShell *mTrueShell;

private:
	//
	//  Unused pure virtual overwrites
	//
	virtual void SetDisplayId(void *);
	virtual void SetWindowId(void *);
	virtual void SetParentId(void *);
	virtual void *GetGenericParentId();
	virtual void *GetGenericContext();
	virtual void *GetGenericDrawable();  
	virtual void SetWindowInfo(char *);
	virtual void SetParentInfo(char *);

	virtual void HideCursor();
	virtual void ShowCursor();
	virtual void WindowRemap();
	virtual int GetEventPending();
	virtual void SetNextWindowId(void *);
	virtual void SetNextWindowInfo(char *);

	virtual void CreateAWindow();
	virtual void DestroyWindow(); 
};

#endif //IGGRENDERWINDOW_H
