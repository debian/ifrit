/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggdialoghelp.h"


#include "ioutput.h"
#include "ihelpfactory.h"

#include "iggframe.h"
#include "iggimagefactory.h"
#include "iggmainwindow.h"
#include "iggshell.h"
#include "iggwidgetmisc.h"
#include "iggwidgetotherbutton.h"
#include "iggwidgettext.h"

#include "ibgwidgetbuttonsubject.h"


using namespace iParameter;


namespace iggDialogHelp_Private
{
	class HelpBrowser : public iggWidgetHelpBrowser
	{

		friend class ContentsList;

	public:

		HelpBrowser(iggDialogHelp *dialog, iggFrame *parent) : iggWidgetHelpBrowser(parent), mDialog(dialog)
		{
		}

		iggDialogHelp* GetDialog() const { return mDialog; }

	protected:

		virtual void OnNullHelpBuffer()
		{
			this->GetShell()->PopupWindow(mDialog,"The data for this link are missing.",MessageType::Warning);
		}

		iggDialogHelp *mDialog;
	};


	class ContentsList : public iggWidgetHelpBrowser
	{

	public:

		ContentsList(HelpBrowser *browser, iggFrame *parent) : iggWidgetHelpBrowser(parent)
		{
			mBrowser = browser;
			mInit = true;
		}

	protected:

		bool mInit;

		virtual void GenerateContents(iString &text) const = 0;

		virtual void UpdateWidgetBody()
		{
			if(mInit)
			{
				iString text;
				this->GenerateContents(text);
				this->SetContentsText(text);
				mInit = false;
			}
			this->iggWidgetHelpBrowser::UpdateWidgetBody();
		}

		virtual void OnMousePress(int line, int index)
		{
			mBrowser->OnMousePress(-1-line,-1-index);
		}

		virtual void OnMouseRelease(int line, int index)
		{
			mBrowser->OnMouseRelease(-1-line,-1-index);
		}

		virtual void OnLinkFollowed(const char *tag)
		{
			mBrowser->OnLinkFollowed(tag);
		}

		HelpBrowser *mBrowser;
	};


	class TopicsList : public ContentsList
	{

	public:

		TopicsList(HelpBrowser *browser, iggFrame *parent) : ContentsList(browser,parent)
		{
		}

	protected:

		virtual void GenerateContents(iString &text) const
		{
			iHelpFactory::CreateTopicList(text);
		}
	};


	class MainObjectsList : public ContentsList
	{

	public:

		MainObjectsList(HelpBrowser *browser, iggFrame *parent) : ContentsList(browser,parent)
		{
		}

	protected:

		virtual void GenerateContents(iString &text) const
		{
			text = iHelpFactory::FormObjectTree(iHelpFactory::_MainObjects);
		}

		virtual void OnLinkFollowed(const char *tag)
		{
			this->ContentsList::OnLinkFollowed(tag);

			if(strcmp(tag,"or.d") == 0)
			{
				mBrowser->GetDialog()->OpenList(2);
			}
		}
	};


	class DataObjectsList : public ContentsList
	{

	public:

		DataObjectsList(HelpBrowser *browser, iggFrame *parent) : ContentsList(browser,parent)
		{
		}

	protected:

		virtual void GenerateContents(iString &text) const
		{
			text = iHelpFactory::FormObjectTree(iHelpFactory::_DataObjects);
		}
	};


	class NavigationButton : public iggWidgetSimpleButton
	{

	public:

		NavigationButton(int mode, iggWidgetHelpBrowser *browser, iggFrame *parent) : iggWidgetSimpleButton("",parent,true)
		{
			mMode = mode;
			mBrowser = browser;

			mBrowser->AddDependent(this);

			switch(mMode)
			{
			case -1:
				{
					mSubject->SetIcon(*iggImageFactory::FindIcon("moveleft.png"));
					this->SetBaloonHelp("Go back one page","Go back to the previous visited page.");
					break;
				}
			case 0:
				{
					mSubject->SetIcon(*iggImageFactory::FindIcon("movetop.png"));
					this->SetBaloonHelp("Go home","Go to the first visited page.");
					break;
				}
			case 1:
				{
					mSubject->SetIcon(*iggImageFactory::FindIcon("moveright.png"));
					this->SetBaloonHelp("Go forward one page","Go forward to the next visited page.");
					break;
				}
			}
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			switch(mMode)
			{
			case -1:
				{
					this->Enable(mBrowser->CanGoBackward());
					break;
				}
			case 1:
				{
					this->Enable(mBrowser->CanGoForward());
					break;
				}
			}
		}

		virtual void Execute()
		{
			switch(mMode)
			{
			case -1:
				{
					mBrowser->Backward();
					break;
				}
			case 0:
				{
					mBrowser->Home();
					break;
				}
			case 1:
				{
					mBrowser->Forward();
					break;
				}
			}
			this->UpdateWidget();
		}

		int mMode;
		iggWidgetHelpBrowser *mBrowser;
	};
};


using namespace iggDialogHelp_Private;


//
//  Main class
//
iggDialogHelp::iggDialogHelp(iggMainWindow *parent) : iggDialog(parent,0U,0,"Help","sr.gg.dhw",2)
{
	if(this->ImmediateConstruction()) this->CompleteConstruction();
}


void iggDialogHelp::CompleteConstructionBody()
{
	HelpBrowser *hb = new HelpBrowser(this,mFrame);
	mBrowser = hb;

	iggFrame *tmp = new iggFrame(mFrame,5);
	mFrame->AddLine(tmp,2);
	tmp->AddLine(new NavigationButton(-1,mBrowser,tmp),new NavigationButton(1,mBrowser,tmp),new NavigationButton(0,mBrowser,tmp),static_cast<iggWidget*>(0),new iggWidgetDialogCloseButton(this,tmp));
	tmp->AddLine(new iggWidgetTextLabel("Back",tmp),new iggWidgetTextLabel("Forward",tmp),new iggWidgetTextLabel("Home",tmp),static_cast<iggWidget*>(0),static_cast<iggWidget*>(0));
	tmp->SetColStretch(3,10);

	mBook = new iggFrameBook(mFrame);

	tmp = new iggFrame(mBook);
	TopicsList *tl = new TopicsList(hb,tmp);
	tmp->AddLine(tl->Self());
	mBook->AddPage("Topics",0,tmp);
	
	tmp = new iggFrame(mBook);
	MainObjectsList *ol = new MainObjectsList(hb,tmp);
	tmp->AddLine(ol->Self());
	mBook->AddPage("Objects",0,tmp);

	tmp = new iggFrame(mBook);
	DataObjectsList *dl = new DataObjectsList(hb,tmp);
	tmp->AddLine(dl->Self());
	mBook->AddPage("Data Objects",0,tmp);

	mFrame->AddLine(mBook,mBrowser->Self());

	mBrowser->SetContents("ug.pf");

	mFrame->SetRowStretch(1,10);
	mFrame->SetColStretch(0,5);
	mFrame->SetColStretch(1,10);

	this->ResizeContents(800,600);
}


void iggDialogHelp::Open(const char *tag)
{
	this->CompleteConstruction();

	mBrowser->SetContents(tag);
	this->Show(true);
}


void iggDialogHelp::OpenList(int n)
{
	mBook->OpenPage(n);
}

#endif
