/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggframedatatypeselector.h"


#include "idata.h"
#include "idatalimits.h"
#include "idatareader.h"
#include "ierror.h"
#include "iviewmodule.h"

#include "igghandle.h"
#include "iggimagefactory.h"
#include "iggshell.h"
#include "iggwidgetpropertycontrolselectionbox.h"
#include "iggwidgetotherbutton.h"

#include "ibgwidgetbuttonsubject.h"
#include "ibgwidgethelper.h"
#include "ibgwidgetselectionboxsubject.h"

#include "iggsubjectfactory.h"


using namespace iParameter;


namespace iggFrameDataTypeSelector_Private
{
	class DataTypeComboBox : public iggWidget
	{

	public:

		DataTypeComboBox(const iString &title, iggFrameDataTypeSelector *selector, iggFrame *parent) : iggWidget(parent)
		{
			mSelector = selector;
			mSubject = iggSubjectFactory::CreateWidgetComboBoxSubject(this,title,true);
			this->SetBaloonHelp("Select the type of data to work with","This control selects the type of data that is affected by the widgets on this page.");
        }

	protected:

		virtual void UpdateWidgetBody()
		{
			mSubject->Clear();

			int i, cur = -1;
			const iDataInfo& info(mSelector->GetListedInfo());
			for(i=0; i<info.Size(); i++)
			{
				mSubject->InsertItem(info.Type(i).TextName());
				if(info.Type(i) == mSelector->GetHandle()->GetActiveDataType()) cur = i;
			}

			if(cur == -1)
			{
				cur = 0;
				mSelector->GetHandle()->SetActiveDataType(info.Type(0));
			}

			mSubject->SetValue(cur);
			this->Enable(info.Size() > 1);
		}

		void OnInt1Body(int v)
		{
			const iDataInfo& info(mSelector->GetListedInfo());
			if(v>=0 && v<info.Size())
			{
				mSelector->GetHandle()->SetActiveDataType(info.Type(v));
			}
		}

		ibgWidgetComboBoxSubject *mSubject;
		iggFrameDataTypeSelector *mSelector;
	};


	class RestrictButton : public iggWidgetSimpleButton
	{

	public:

		RestrictButton(iggFrameDataTypeSelector *selector, iggFrame *parent) : iggWidgetSimpleButton("",parent,true)
		{
			mAllIcon = iggImageFactory::FindIcon("singleline.png");
			mSetIcon = iggImageFactory::FindIcon("multiline.png");
			if(mAllIcon==0 || mSetIcon==0)
			{
				IBUG_WARN("Icon images are not found.");
			}

			mSubject->SetFlat(true);

			mSelector = selector;
			this->SetBaloonHelp("Restrict the list to types with loaded data","This button toggles showing all types or only types with data actually loaded into memory.");
        }

	protected:

		virtual void UpdateWidgetBody()
		{
			switch(mSelector->GetMode())
			{
			case ListDataTypes::WithData:
				{
					if(mSetIcon != 0) mSubject->SetIcon(*mSetIcon);
					break;
				}
			default:
				{
					if(mAllIcon != 0) mSubject->SetIcon(*mAllIcon);
					break;
				}
			}
		}

		virtual void Execute()
		{
			switch(mSelector->GetMode())
			{
			case ListDataTypes::WithData:
				{
					mSelector->SetMode(ListDataTypes::All);
					break;
				}
			default:
				{
					mSelector->SetMode(ListDataTypes::WithData);
					break;
				}
			}
			this->UpdateWidget();
		}

		const iImage *mAllIcon, *mSetIcon;
		iggFrameDataTypeSelector *mSelector;
	};
};


using namespace iggFrameDataTypeSelector_Private;


iggFrameDataTypeSelector::iggFrameDataTypeSelector(iggDataTypeHandle *handle, const iString &title, iggFrameBase *parent, int mode) : iggFrame(parent)
{
	const bool hor = true;

	mHandle = handle; IASSERT(mHandle);

	mMode = mode;
	mAutoPilot = true;
	mListedInfo = new iDataInfo; IERROR_CHECK_MEMORY(mListedInfo);

	if(hor)
	{
		mBase = new iggFrame(this,3);
		mBase->AddSpace(1); // improves the layout
	}
	else
	{
		mBase = new iggFrame(title,this,3);
		mBase->AddSpace(1); // improves the layout
	}

	iggWidget *cb = new DataTypeComboBox(hor?title:"",this,mBase);
	RestrictButton *rb = 0;
	if(mode == iParameter::ListDataTypes::All) rb = new RestrictButton(this,mBase);

	mBase->AddLine(cb,2,rb,1);
	mBase->AddSpace(1); // improves the layout
	mBase->SetColStretch(0,10);
	this->AddLine(mBase);

	cb->UpdateWidget();
}

	
iggFrameDataTypeSelector::~iggFrameDataTypeSelector()
{
	delete mListedInfo;
}

	
const iDataInfo& iggFrameDataTypeSelector::GetListedInfo() const
{
	const iDataInfo &info(mHandle->GetDataInfo());
	int i, n = info.Size();
	iDataReader *dr = this->GetShell()->GetViewModule()->GetReader();

	switch(mMode)
	{
	case ListDataTypes::WithData:
		{
			mListedInfo->Clear();
			for(i=0; i<n; i++) if(dr->IsThereData(info.Type(i)))
			{
				*mListedInfo += info.Type(i);
			}
			break;
		}
	case ListDataTypes::WithVariables:
		{
			mListedInfo->Clear();
			for(i=0; i<n; i++) if(dr->IsThereData(info.Type(i)) && dr->GetLimits(info.Type(i))!=0 && dr->GetLimits(info.Type(i))->GetNumVars()>0)
			{
				*mListedInfo += info.Type(i);
			}
			break;
		}
	case ListDataTypes::All:
	default:
		{
			*mListedInfo = info;
			break;
		}
	}

	return *mListedInfo;
}


void iggFrameDataTypeSelector::SetMode(int m)
{
	if(m>=ListDataTypes::All && m<=ListDataTypes::WithVariables)
	{
		mMode = m;
		mAutoPilot = false;
		mBase->UpdateWidget();
	}
}


void iggFrameDataTypeSelector::UpdateWidgetBody()
{
	mBase->Show(mHandle->GetDataInfo().Size() > 1);

	if(mAutoPilot)
	{
		if(mMode==ListDataTypes::All && this->GetShell()->GetViewModule()->GetReader()->IsThereData(this->GetListedInfo()))
		{
			mMode = ListDataTypes::WithData;
		}
	}

	this->iggFrame::UpdateWidgetBody();
}

#endif
