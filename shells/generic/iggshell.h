/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


//
//  A generic GUI shell
//

#ifndef IGGSHELL_H
#define IGGSHELL_H


#include "ishell.h"


class iDataReader;
class iPicker;

class iggDialog;
class iggDialogFlashWindow;
class iggExtensionWindow;
class iggMainWindow;
class iggMenuWindow;
class iggObjectHandle;
class iggPropertyHandle;
class iggRenderWindow;

class ibgShellSubject;


namespace iParameter
{
	namespace ViewModuleSelection
	{
		const int One		= 1;
		const int All		= 2;
		const int Clones	= 3;
	}
};


class iggShell : public iShell
{

	friend class iShell;

public:

	vtkTypeMacro(iggShell,iShell);

	inline iggMainWindow* GetMainWindow() const { return mMainWindow; }

	iViewModule* GetViewModule(int mod = -1) const;
	bool SetActiveViewModuleIndex(int v);
	int GetActiveViewModuleIndex() const;
	void SelectViewModules(iArray<iViewModule*> &arr, int selection);

	void GetDesktopDimensions(int &w, int &h) const;
	void ProcessEvents(bool sync = false);

	bool IsDesktopSmall(bool vert) const;
	bool CheckCondition(int flag) const;
	void SetCondition(int flag);

	inline iggObjectHandle* GetShellHandle() const { return mShellHandle; }
	inline iggObjectHandle* GetViewModuleHandle() const { return mViewModuleHandle; }
	inline iggObjectHandle* GetDataReaderHandle() const { return mDataReaderHandle; }

	iggObjectHandle* GetObjectHandle(const iString &obj);

	//ibgShellSubject* GetSubject() const { return mSubject; }

	//
	//  User notification
	//
	int PopupWindow(const iString &text, int type, const char *button0 = "Ok", const char *button1 = 0, const char *button2 = 0) const;
	int PopupWindow(const iggDialog *parent, const iString &text, int type, const char *button0 = "Ok", const char *button1 = 0, const char *button2 = 0) const;
	int PopupWindow(const iggMenuWindow *parent, const iString &text, int type, const char *button0 = "Ok", const char *button1 = 0, const char *button2 = 0) const;
	int PopupWindow(const iggRenderWindow *parent, const iString &text, int type, const char *button0 = "Ok", const char *button1 = 0, const char *button2 = 0) const;
	int PopupWindow(const iggExtensionWindow *parent, const iString &text, int type, const char *button0 = "Ok", const char *button1 = 0, const char *button2 = 0) const;

	//
	//  Interface for multi-threader
	//
	virtual void OnTimerEvent();

	//
	//  Factory methods for ViewModule
	//
	virtual vtkRenderWindow* CreateRenderWindow(iViewModule *vm, bool stereo) const;
	virtual vtkRenderWindowInteractor* CreateRenderWindowInteractor(iViewModule *vm) const;
	virtual iEventObserver* CreateViewModuleEventObserver(iParameter::ViewModuleObserver::Type type, iViewModule *vm) const;

protected:

	iggShell(iRunner *runner, bool threaded, const iString &type, int argc, const char **argv);
	virtual ~iggShell();

	virtual bool StartConstructor();
	virtual bool ConstructorBody();  
	virtual void DestructorBody();  
	virtual void FinishDestructor();
	virtual void StartBody();
	virtual void StopBody();

	virtual void OnInitStart();
	virtual void OnInitState();
	virtual void OnInitFinish();

	virtual void OnInitAtomBody(bool timed);
	virtual void OnLoadStateAtomBody(int prog);

	virtual bool OnCommandLineOption(const iString &sname, const iString& value);

	//
	//  Factory methods for itself
	//
	virtual iDataReaderObserver* CreateDataReaderObserver();
	virtual iEventObserver* CreateShellEventObserver(iParameter::ShellObserver::Type);
	virtual iOutputChannel* CreateOutputChannel();

	//
	//  Use as a callback when the active window chages
	//
	virtual void OnActiveWindowChange() const;

private:

	iggObjectHandle *mShellHandle, *mViewModuleHandle, *mDataReaderHandle;
	iGenericOrderedArray<iggObjectHandle*,iString> mHandles;

	ibgShellSubject *mSubject;
	iggMainWindow *mMainWindow;
	bool mShowFlashWindow, mStartDocked;
	int mConditionFlags;
	iggDialogFlashWindow *mFlashWindow;
	mutable int mActiveViewModuleIndex;
};

#endif  // IGGSHELL_H

