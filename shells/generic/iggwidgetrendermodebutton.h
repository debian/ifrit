/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IGGWIDGETRENDERMODEBUTTON_H
#define IGGWIDGETRENDERMODEBUTTON_H


#include "iggwidget.h"


class iImage;
template<class T> class iLookupArray;

class iggWidgetPropertyControlBase;

class ibgWidgetButtonSubject;
class ibgWidgetEntrySubject;
class ibgWidgetTrackBallSubject;


class iggWidgetRenderModeButton : public iggWidget
{

public:

	iggWidgetRenderModeButton(iggWidgetPropertyControlBase *parent, bool registered = true);
	virtual ~iggWidgetRenderModeButton();

	static void ShowAll(bool s);
	static void UpdateAll();

	ibgWidgetButtonSubject* GetSubject() const { return mSubject; }

	//
	//  Allow selected subjects to clear the laid-out flag
	//
	void ClearLaidOutFlag(const ibgWidgetEntrySubject *e); 
	void ClearLaidOutFlag(const ibgWidgetTrackBallSubject *t); 

protected:

	virtual void UpdateWidgetBody();
	virtual void OnVoid1Body();

	ibgWidgetButtonSubject *mSubject;
	iggWidgetPropertyControlBase *mParent;
	const iImage *mDelayIcon, *mReadyIcon;
	//
	//  Registry functionality
	//
	static iLookupArray<iggWidgetRenderModeButton*> &List();
};

#endif  // IQTWIDGETRENDERMODEBUTTON_H

