/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggpageobject.h"


#include "idata.h"
#include "idatareader.h"
#include "idatasubject.h"
#include "ierror.h"
#include "ifieldviewsubject.h"
#include "imarker.h"
#include "iviewmodule.h"
#include "iviewobject.h"
#include "iviewsubject.h"

#include "iggframeactiveinstance.h"
#include "iggframedatatypeselector.h"
#include "iggframedatavariablelist.h"
#include "iggframedoublebutton.h"
#include "iggframematerialproperties.h"
#include "iggframepaletteselection.h"
#include "iggframereplicate.h"
#include "igghandle.h"
#include "iggmainwindow.h"
#include "iggshell.h"
#include "iggwidgetmisc.h"
#include "iggwidgetotherbutton.h"
#include "iggwidgetpropertycontrolcolorselection.h"
#include "iggwidgetpropertycontrolselectionbox.h"

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"


namespace iggPageObject_Private
{
	//
	//  Controls the DataType of this page
	//
	class DataTypeHandle : public iggDataTypeHandle
	{

	public:

		DataTypeHandle(iggPageObject *owner) : mIndex(owner->GetShell())
		{
			mOwner = owner;

			int i, n = mOwner->GetViewObject(-1)->GetNumberOfSubjects();
			for(i=0; i<n; i++)
			{
				mDataInfo += mOwner->GetViewObject(-1)->GetSubject(i)->GetDataType();
			}

			mIndex = 0;
		}

		virtual const iDataInfo& GetDataInfo() const
		{
			return mDataInfo;
		}

		virtual int GetActiveDataTypeIndex() const
		{
			return mIndex;
		}

	protected:

		virtual void OnActiveDataTypeChanged(int v)
		{
			if(v>=0 && v<mDataInfo.Size())
			{
				mIndex = v;
				mOwner->GetMainWindow()->UpdateAll();
			}
		}

		virtual void AfterFileLoadBody(const iDataInfo &info)
		{
			int j;

			//
			//  Try each loaded type in order, until succeeded, but only if it has data
			//  (info lists all types, but only some of them may actually have data)
			//
			for(j=0; j<info.Size(); j++)
			{
				if(mOwner->GetViewModule(-1)->GetReader()->IsThereData(info.Type(j)))
				{
					this->SetActiveDataType(info.Type(j));
				}
			}
		}

	private:

		iggIndex mIndex;
		iDataInfo mDataInfo;
		iggPageObject *mOwner;
	};

	//
	//  Returns the current VO
	//
	class ViewObjectHandle : public iggObjectHandle
	{

	public:

		ViewObjectHandle(iggPageObject *owner) : iggObjectHandle(owner)
		{
			mOwner = owner; IASSERT(mOwner);
		}

		virtual const iObject* Object(int mod = -1) const
		{
			return mOwner->GetViewObject(mod);
		}

	private:

		iggPageObject *mOwner;
	};

	//
	//  Returns the current VS
	//
	class ViewSubjectHandle : public iggObjectHandle
	{

	public:

		ViewSubjectHandle(iggPageObject *owner) : iggObjectHandle(owner)
		{
			mOwner = owner; IASSERT(mOwner);
		}

		virtual const iObject* Object(int mod = -1) const
		{
			return mOwner->GetViewSubject(mod);
		}

	private:

		iggPageObject *mOwner;
	};

	class InstanceHandle : public iggIndexHandle
	{

	public:

		InstanceHandle(iggPageObject *owner) : mIndex(owner->GetShell())
		{
			mOwner = owner;

			mIndex = 0;
		}

		virtual int Index() const
		{
			if(mIndex >= mOwner->GetViewSubject(-1)->GetNumberOfInstances())
			{
				mIndex = mOwner->GetViewSubject(-1)->GetNumberOfInstances() - 1;
				mOwner->GetMainWindow()->UpdateAll();
			}
			return mIndex;
		}

		void SetIndex(int v)
		{
			if(v >= 0 && v < mOwner->GetViewSubject(-1)->GetNumberOfInstances())
			{
				mIndex = v;
				mOwner->GetMainWindow()->UpdateAll();
			}
			else
			{
				IBUG_ERROR("Invalid instance id.");
			}
		}

	private:

		iggPageObject *mOwner;
		mutable iggIndex mIndex;
	};

	class DataSubjectHandle : public iggObjectHandle
	{

	public:

		DataSubjectHandle(iggPageObject *owner) : iggObjectHandle(owner)
		{
			mOwner = owner; IASSERT(mOwner);
		}

		virtual const iObject* Object(int mod = -1) const
		{
			return mOwner->GetViewSubject(mod)->GetSubject();
		}

	private:

		iggPageObject *mOwner;
	};

	class ScalarDataSubjectHandle : public iggObjectHandle
	{

	public:

		ScalarDataSubjectHandle(iggPageObject *owner) : iggObjectHandle(owner)
		{
			mOwner = owner; IASSERT(mOwner);
		}

		virtual const iObject* Object(int mod = -1) const
		{
			iFieldViewSubject *fs = iFieldViewSubject::SafeDownCast(mOwner->GetViewSubject(mod));
			if(fs != 0) return mOwner->GetViewModule(mod)->GetReader()->GetSubject(fs->GetPaintingDataType()); else return 0;
		}

	private:

		iggPageObject *mOwner;
	};

	//
	//  Frame for manipulating instances of this object
	//
	class ActiveInstanceFrame : public iggFrameActiveInstance
	{

	public:

		ActiveInstanceFrame(bool withCreateDelete, const iString &title, const iString &item, iggPageObject *owner, iggFrame *parent) : iggFrameActiveInstance(withCreateDelete,title,item,parent)
		{
			mOwner = owner; IASSERT(mOwner);
		}

		virtual bool CreateInstance()
		{
			iViewSubject *sub = dynamic_cast<iViewSubject*>(mOwner->GetViewSubject(-1));
 			if(sub!=0 && sub->CreateInstance())
			{
				this->SetActiveInstance(sub->GetNumberOfInstances()-1);
				return true;
			}
			else return false;
		}

		virtual bool DeleteInstance(int i)
		{
			iViewSubject *sub = dynamic_cast<iViewSubject*>(mOwner->GetViewSubject(-1));
			if(sub!=0 && sub->RemoveInstance(this->GetActiveInstance()))
			{
				return true;
			}
			else return false;
		}

		virtual int GetMinNumberOfInstances() const
		{
			const iViewSubject *sub = dynamic_cast<const iViewSubject*>(mOwner->GetViewSubject(-1));
 
 			if(sub != 0) return sub->GetMinNumberOfInstances(); else return 0;
		}

		virtual int GetNumberOfInstances() const
		{
			return mOwner->GetViewSubject(-1)->GetNumberOfInstances();
		}

		virtual int GetActiveInstance() const
		{
			return mOwner->GetActiveInstance();
		}

		virtual void SetActiveInstance(int n)
		{
			mOwner->SetActiveInstance(n);
		}

	protected:

		iggPageObject *mOwner;
	};
};


using namespace iggPageObject_Private;


iggPageObject::iggPageObject(iggFrameBase *parent, const iString &name, const iImage *icon) : iggPageMain(parent,iggPage::WithBook), mIcon(icon)
{
	IASSERT(mIcon);

	this->Define(name);
}


iggPageObject::iggPageObject(const iString &instanceName, iggFrameBase *parent, const iString &name, const iImage *icon) : iggPageMain(parent,iggPage::WithBook), mIcon(icon)
{
	IASSERT(mIcon);

	mInstanceName = instanceName;

	this->Define(name);
}


void iggPageObject::Define(const iString &name)
{
	mObjectIndex = this->GetShell()->GetViewModule()->GetViewObjectIndex(name);

	mObjectHandle = new ViewObjectHandle(this);
	mSubjectHandle = new ViewSubjectHandle(this);
	mInstanceHandle = new InstanceHandle(this);
	mDataSubjectHandle = new DataSubjectHandle(this);
	mScalarDataSubjectHandle = new ScalarDataSubjectHandle(this);

	mDataTypeHandle = new DataTypeHandle(this);
}


iggPageObject::~iggPageObject()
{
	delete mDataTypeHandle;

	delete mObjectHandle;
	delete mSubjectHandle;
	delete mInstanceHandle;
	delete mDataSubjectHandle;
	delete mScalarDataSubjectHandle;
}


iggFrame* iggPageObject::CreateFlipFrame(iggFrameBase *parent)
{
	if(!this->InstanceName().IsEmpty())
	{
		iggFrame *f = new iggFrame(parent,2);
		iggFrame *f2 = new iggFrame(f,1);
		f2->AddSpace(1);
		iggWidget *ci = new ActiveInstanceFrame(false,this->InstanceName(),this->InstanceName(),this,f2);
		ci->AddDependent(this);
		f2->AddLine(ci);
		f2->AddSpace(1);
		f->AddLine(f2,new iggFrameDataTypeSelector(mDataTypeHandle,"Use data",f));
		//f->SetColStretch(0,0);
		//f->SetColStretch(1,10);
		return f;
	}
	else return new iggFrameDataTypeSelector(mDataTypeHandle,"Use data",parent);
}


const iggPropertyHandle iggPageObject::CreatePropertyHandle(const iString &name) const
{
	return iggPropertyHandle(name,mSubjectHandle,mInstanceHandle);
}


iViewModule* iggPageObject::GetViewModule(int mod) const
{
	return this->GetShell()->GetViewModule(mod);
}


iViewObject* iggPageObject::GetViewObject(int mod) const
{
	return this->GetViewModule(mod)->GetViewObjectByIndex(mObjectIndex);
}


iViewSubject* iggPageObject::GetViewSubject(int mod) const
{
	return this->GetViewObject(mod)->GetSubject(mDataTypeHandle->GetActiveDataTypeIndex());
}


void iggPageObject::SetActiveInstance(int v)
{
	InstanceHandle *h = iDynamicCast<InstanceHandle>(INFO,mInstanceHandle);
	IASSERT(h);

	h->SetIndex(v);
}


int iggPageObject::GetActiveInstance() const
{
	InstanceHandle *h = iDynamicCast<InstanceHandle>(INFO,mInstanceHandle);
	IASSERT(h);

	return h->Index();
}


void iggPageObject::Show(bool s)
{
	this->GetViewSubject(-1)->Visible.SetValue(0,s);
	this->GetMainWindow()->UpdateAll();
	this->Render();
}


//
//  Second level pages - primarily to avoid code replication
//
iggPage2::iggPage2(iggPageObject *owner, iggFrameBase *parent, int ncols) : iggFrameDC(parent,ncols)
{
	mOwner = owner; IASSERT(mOwner);
}


//
//  Main page
//
iggMainPage2::iggMainPage2(iggPageObject *owner, iggFrameBase *parent, bool withCreateDelete) : iggPage2(owner,parent,3)
{
	mWithCreateDelete = withCreateDelete;
}


void iggMainPage2::CompleteConstructionBody()
{
	//
	//  Show
	//
	iggWidgetShowButton *sb = new iggWidgetShowButton(mOwner,this);
	iggFrameActiveInstance *ci;
	if(!mOwner->InstanceName().IsEmpty())
	{
		ci = new ActiveInstanceFrame(mWithCreateDelete,mOwner->InstanceName(),mOwner->InstanceName(),mOwner,this);
		ci->AddDependent(this);
	}
	else
	{
		ci = 0;
	}
	this->AddLine(sb,new iggWidgetTextLabel("",this),ci);

	this->InsertSectionA(this);

	this->SetColStretch(1,10);
	this->SetColStretch(2,5);
}


//
//  Paint page
//
iggPaintPage2::iggPaintPage2(iggPageObject *owner, iggFrameBase *parent, int flags) : iggPage2(owner,parent,1)
{
	mFlags = flags;
}


void iggPaintPage2::CompleteConstructionBody()
{
	//
	//  Book
	//
	iggFrameBook *pb = new iggFrameBook(this);
	this->AddLine(pb);

	//
	//  Palette page
	//
	iggFrame *pbpage0 = new iggFrame(pb,3);
	pb->AddPage("Palette",mOwner->Icon(),pbpage0);
	{
		this->InsertSectionA(pbpage0);

		iggFramePaletteSelection *ps = new iggFramePaletteSelection((mFlags&WithBrightness)!=0,mOwner->CreatePropertyHandle("Palette"),pbpage0);
		pbpage0->AddLine(ps,2);
		pbpage0->AddSpace(20);

		pbpage0->SetColStretch(1,10);
		pbpage0->SetColStretch(2,3);
	}

	//
	//  Material page
	//
	iggFrame *pbpage1 = new iggFrame(pb,2);
	pb->AddPage("Material",mOwner->Icon(),pbpage1);
	{
		iggFrameMaterialProperties *mp = new iggFrameMaterialProperties(mOwner->GetSubjectHandle(),pbpage1);
		pbpage1->AddLine(mp);

		pbpage1->AddSpace(10);
		pbpage1->SetColStretch(0,10);
		pbpage1->SetColStretch(1,3);
	}
}
	

//
//  Replicate page
//
iggReplicatePage2::iggReplicatePage2(iggPageObject *owner, iggFrameBase *parent, bool voxelBox) : iggPage2(owner,parent,2)
{
	mWithVoxelBox = voxelBox;

	if(this->ImmediateConstruction()) this->CompleteConstruction();
}


void iggReplicatePage2::CompleteConstructionBody()
{
	//
	//  Replicate
	//
	this->AddLine(new iggFrameReplicate(mOwner->GetSubjectHandle(),this));
	this->AddSpace(2);

	if(mWithVoxelBox)
	{
		this->AddLine(new iggWidgetTextLabel("For best results<br><b>Data->ScalarData->VoxelLocation</b><br>should be set to<br><b>Center of a cell</b>.",this));
		iggWidgetPropertyControlRadioBox *vl = new iggWidgetPropertyControlRadioBox(1,"Voxel location",0,iggPropertyHandle("VoxelLocation",this->GetShell()->GetDataReaderHandle()),this);
		vl->InsertItem("Vertex of a cell (\"point data\")");
		vl->InsertItem("Center of a cell (\"cell data\")");
		this->AddLine(vl);
	}

	this->AddSpace(10);
	this->SetColStretch(1,3);
}

#endif
