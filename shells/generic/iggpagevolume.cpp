/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggpagevolume.h"


#include "idatasubject.h"
#include "ihelpfactory.h"
#include "ihistogrammaker.h"
#include "iviewmodule.h"
#include "iviewobject.h"
#include "ivolumeviewsubject.h"

#include "iggframedatavariablelist.h"
#include "iggframepiecewisefunctioncontrols.h"
#include "igghandle.h"
#include "iggimagefactory.h"
#include "iggshell.h"
#include "iggwidgetpropertycontrolselectionbox.h"

#include "ibgframesubject.h"
#include "ibgwidgetbuttonsubject.h"

#include "iggsubjectfactory.h"

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"
#include"iggwidgetpropertycontrolslider.tlh"


using namespace iParameter;


namespace iggPageVolume_Private
{
	//
	//  Helper widgets
	//
	class WindowSizeFrame : public iggFrame
	{
		
	public:
		
		WindowSizeFrame(iggFrame *parent) : iggFrame("Window size",parent,1)
		{
			mSubject1 = iggSubjectFactory::CreateWidgetButtonSubject(this,ButtonType::PushButton,"256x256",1);
			mSubject2 = iggSubjectFactory::CreateWidgetButtonSubject(this,ButtonType::PushButton,"512x512",2);
			mSubject3 = iggSubjectFactory::CreateWidgetButtonSubject(this,ButtonType::PushButton,"1024x1024",3);

			mSubject->PlaceWidget(0,0,mSubject1,1,true);
			mSubject->PlaceWidget(0,1,mSubject2,1,true);
			mSubject->PlaceWidget(0,2,mSubject3,1,true);

			this->SetBaloonHelp("Sets the size of the current visualization window to a power of 2","When using the VolumePro board for volume rendering, IFrIT relies on OpenGL textures to display the scene. Since OpenGL textures must have dimensions that are powers of two, the performance is optimal when the visualization window size corresponds exactly to the texture size. These buttons will quickly resize the visualization window to be a power of two size.");
		}

	protected:
		
		virtual void OnVoid1Body()
		{
			this->GetShell()->GetViewModule()->SetSize(256,256);		
		}

		virtual void OnVoid2Body()
		{
			this->GetShell()->GetViewModule()->SetSize(512,512);		
		}

		virtual void OnVoid3Body()
		{
			this->GetShell()->GetViewModule()->SetSize(1024,1024);		
		}

		ibgWidgetButtonSubject *mSubject1, *mSubject2, *mSubject3;
	};


	class MethodBox : public iggWidgetPropertyControlComboBox
	{

	public:

		MethodBox(iggPageObject *owner, iggFrame *parent) : iggWidgetPropertyControlComboBox("",0,owner->CreatePropertyHandle("MethodId"),parent)
		{
			mOwner = owner;

			this->InsertItem("A volume rendering method");
		}

	protected:

		iggPageObject *mOwner;

		virtual void UpdateWidgetBody()
		{
			int i;
			iString s;

			//
			//  Optimization: direct access
			//
			iVolumeViewSubject *sub = iRequiredCast<iVolumeViewSubject>(INFO,mOwner->GetViewSubject(-1));

			this->Clear();
			i = 0;
			while(!(s = sub->GetMethodName(i)).IsEmpty())
			{
				this->SetItem(s,i,sub->IsMethodAvailable(i));
				i++;
			}

			this->iggWidgetPropertyControlComboBox::UpdateWidgetBody();
		}
	};


	class OpacityFunctionControls : public iggFramePiecewiseFunctionControls
	{

	public:

		OpacityFunctionControls(const iggPageObject *owner, iggFrame *parent) : iggFramePiecewiseFunctionControls(true,true,parent)
		{
			mOwner = owner; IASSERT(mOwner);

			const iHelpDataBuffer &buf = iHelpFactory::FindData("or.v.of",iHelpFactory::_ObjectReference);
			this->SetBaloonHelp("Interactively controls the piecewise function specified by the OpacityFunction property. Press Shift+F1 for more help.",buf.GetHTML()+"<p>The piecewise function can be controlled interactively by clicking on a control point and dragging it around with the mouse. Four buttons below the interactive area allow you to add or remove a control point and to render the scene. The latter button is important because while you are dragging a control point with the left mouse button pressed, the scene is actually not being rendered, even if the piecewise function is being changed - this is done to maintain the interactive performance. Because the scene may be complex and slow to render, the interactive area will appear \"jumpy\" if the scene is rendered all the time. If you would like to render the scene continously, drag the control point with the right mouse button pressed.");
		}

		virtual const iHistogram& GetHistogram(int sid, bool full) const
		{
			const iVolumeViewSubject *vs = iRequiredCast<const iVolumeViewSubject>(INFO,mOwner->GetViewSubject(-1)); IASSERT(vs);
			int var = vs->Var.GetValue(0);
			return vs->GetSubject()->GetHistogramMaker()->GetHistogram(var,sid,full);
		}

		iPiecewiseFunction* GetFunction()
		{
			return iRequiredCast<const iVolumeViewSubject>(INFO,mOwner->GetViewSubject(-1))->OpacityFunction.Function();
		}

	protected:

		const iggPageObject *mOwner;
	};


	//
	//  Main page
	// ************************************************
	//
	class MainPage : public iggMainPage2
	{

	public:

		MainPage(iggPageObject *owner, iggFrameBase *parent) : iggMainPage2(owner,parent,false)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		virtual void InsertSectionA(iggFrame *page)
		{
			//
			//  Field & method
			//
			iggFrame *tmp = new iggFrame("Method",page);
			tmp->AddLine(new MethodBox(mOwner,tmp));
			page->AddLine(tmp);

			iggFrameDataVariableList *pl = new iggFrameDataVariableList("Variable",mOwner->GetDataSubjectHandle(),mOwner->CreatePropertyHandle("Var"),page);
			pl->Complete();
#ifdef VTK_USE_VOLUMEPRO
			page->AddLine(pl,new iggPageVolume_Private::WindowSizeFrame(this));
#else
			page->AddLine(pl);
#endif
			page->AddSpace(10);

			page->SetColStretch(2,10);
		}
	};


	//
	//  Paint page
	// ************************************************
	//
	class PaintPage : public iggPaintPage2
	{

	public:

		PaintPage(iggPageObject *owner, iggFrameBase *parent) : iggPaintPage2(owner,parent,iggPaintPage2::WithShading)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		virtual void InsertSectionA(iggFrame *page)
		{
		}
	};


	//
	//  Opacity page
	// ************************************************
	//
	class OpacityPage : public iggPage2
	{

	public:

		OpacityPage(iggPageObject *owner, iggFrameBase *parent) : iggPage2(owner,parent,1)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		virtual void CompleteConstructionBody()
		{
			//
			//  FunctionMapping widget
			//
			this->AddLine(new OpacityFunctionControls(mOwner,this));
	
			this->AddLine(new iggWidgetPropertyControlFloatSlider(1.0,10.0,10,iStretch::Log,3,false,false,"Opacity scale factor",mOwner->CreatePropertyHandle("OpacityScaleFactor"),this));
		}
	};


	//
	//  Extras page
	// ************************************************
	//
	class ExtrasPage : public iggPage2
	{

	public:

		ExtrasPage(iggPageObject *owner, iggFrameBase *parent) : iggPage2(owner,parent,3)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		virtual void CompleteConstructionBody()
		{
			//
			//  Ray casting settings
			//
			iggFrame *rc = new iggFrame("Rendering settings",this,2);
			iggWidgetPropertyControlRadioBox *it = new iggWidgetPropertyControlRadioBox(1,"Interpolation type",0,mOwner->CreatePropertyHandle("InterpolationType"),rc);
			it->InsertItem("Nearest neighbor");
			it->InsertItem("Linear");
			iggWidgetPropertyControlRadioBox *bm = new iggWidgetPropertyControlRadioBox(1,"Blending mode",0,mOwner->CreatePropertyHandle("BlendMode"),rc);
			bm->InsertItem("Composite");
			bm->InsertItem("Maximum intensity");
			rc->AddLine(it,bm);
			this->AddLine(rc);
			this->AddSpace(10);

			iggFrame *rs = new iggFrame("Downsampling settings",this,1);
			iggWidgetPropertyControlFloatSlider *fs = new iggWidgetPropertyControlFloatSlider(0.1,10.0,20,iStretch::Log,4,false,false,"Image downsampling",mOwner->CreatePropertyHandle("ImageDownsampleFactor"),rs);
			fs->SetStretch(3,10);
			rs->AddLine(fs);
			fs = new iggWidgetPropertyControlFloatSlider(0.1,10.0,20,iStretch::Log,4,false,false,"Depth downsampling",mOwner->CreatePropertyHandle("DepthDownsampleFactor"),rs);
			fs->SetStretch(3,10);
			rs->AddLine(fs);

			this->AddLine(rs,2);
			this->AddSpace(10);

			this->SetColStretch(1,10);
			this->SetColStretch(2,3);
		}
	};
};


using namespace iggPageVolume_Private;


iggPageVolume::iggPageVolume(iggFrameBase *parent) : iggPageObject(parent,"v",iggImageFactory::FindIcon("volv.png"))
{
	//
	//  Main page
	// ************************************************
	//
	mBook->AddPage("Main",this->Icon(),new MainPage(this,mBook));

	//
	//  Paint page
	// ************************************************
	//
	mBook->AddPage("Paint",this->Icon(),new PaintPage(this,mBook));

	//
	//  Opacity page
	// ************************************************
	//
	mBook->AddPage("Opacity",this->Icon(),new OpacityPage(this,mBook));

	//
	//  Extras page
	// ************************************************
	//
	mBook->AddPage("Extras",this->Icon(),new ExtrasPage(this,mBook));

	//
	//  Replicate page
	// ************************************************
	//
	mBook->AddPage("Replicate",this->Icon(),new iggReplicatePage2(this,mBook,true));

}

#endif
