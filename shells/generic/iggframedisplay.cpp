/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggframedisplay.h"


#include "icolor.h"

#include "iggwidgetmisc.h"

#include "ibgwidgetareasubject.h"
#include "ibgwidgethelper.h"


using namespace iParameter;


namespace iggFrameNumberDisplay_private
{
	class NumberLabel : public iggWidgetNumberLabel
	{

	public:

		NumberLabel(iggFrame *parent) : iggWidgetNumberLabel(parent)
		{
			mSubject->ShowFrame(true);
			this->GetHelper()->SetBackgroundColor(iColor::IFrIT());
		}
	};

	class TextLabel : public iggWidgetTextLabel
	{

	public:

		TextLabel(iggFrame *parent) : iggWidgetTextLabel("",parent)
		{
			mSubject->ShowFrame(true);
			this->GetHelper()->SetBackgroundColor(iColor::IFrIT());
		}
	};
};


using namespace iggFrameNumberDisplay_private;


//
//  iggFrameNumberDisplay class
//
iggFrameNumberDisplay::iggFrameNumberDisplay(const iString &title, iggFrame *parent) : iggFrame(parent,2)
{
	mDisplay = new NumberLabel(this);

	if(title.IsEmpty())
	{
		this->AddLine(mDisplay,2);
	}
	else
	{
		this->AddLine(new iggWidgetTextLabel(title,this),mDisplay);
	}

	mNeedsBaloonHelp = false;
}


void iggFrameNumberDisplay::Display(double v, const char *format)
{
	mDisplay->SetNumber(v,format);
}


void iggFrameNumberDisplay::Display(int v, const char *format)
{
	mDisplay->SetNumber(v,format);
}


void iggFrameNumberDisplay::SetDisplayColor(const iColor& c)
{
	mDisplay->GetHelper()->SetBackgroundColor(c);
}


void iggFrameNumberDisplay::SetAlignment(int s)
{
	mDisplay->SetAlignment(s);
}


//
//  iggFrameTextDisplay class
//
iggFrameTextDisplay::iggFrameTextDisplay(const iString &title, iggFrame *parent) : iggFrame(parent,2)
{
	mDisplay = new TextLabel(this);

	if(title.IsEmpty())
	{
		this->AddLine(mDisplay,2);
	}
	else
	{
		this->AddLine(new iggWidgetTextLabel(title,this),mDisplay);
	}

	mNeedsBaloonHelp = false;
}


void iggFrameTextDisplay::Display(const iString &text)
{
	mDisplay->SetText("%b"+text);
}


void iggFrameTextDisplay::SetDisplayColor(const iColor& c)
{
	mDisplay->GetHelper()->SetBackgroundColor(c);
}


void iggFrameTextDisplay::SetAlignment(int s)
{
	mDisplay->SetAlignment(s);
}

#endif
