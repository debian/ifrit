/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggabstractextension.h"


#include "iabstractextension.h"
#include "idata.h"
#include "idatareader.h"
#include "idatasubject.h"
#include "ierror.h"
#include "ifileloader.h"
#include "iviewmodule.h"

#include "iggextensionwindow.h"
#include "iggframe.h"
#include "igghandle.h"
#include "iggimagefactory.h"
#include "iggmainwindow.h"
#include "iggmenuhelper.h"
#include "iggpageobjectextension.h"
#include "iggshell.h"
#include "iggwidgetarea.h"
#include "iggwidgetmisc.h"

#include "ibgmenuwindowsubject.h"

//
//  Templates
//
#include "iarray.tlh"
#include "iggmenuhelper.tlh"


//
//  Helper class
//
iggExtensionFrameBook::iggExtensionFrameBook(iggFrameBase *parent) : iggFrameBook(parent,true)
{
	mBlocked = false;
}


void iggExtensionFrameBook::OpenPageByIndex(int index)
{
	int i;
	for(i=0; i<mPages.Size(); i++) if(mPages[i].Index == index)
	{
		mPages[i].Frame->UpdateWidget();
		this->OpenPage(i);
		break;
	}
}


void iggExtensionFrameBook::Detach()
{
	mBlocked = true;
}

	
void iggExtensionFrameBook::Block(bool s)
{
	mBlocked = s;
}


void iggExtensionFrameBook::OnInt1Body(int i)
{
	if(!this->GetShell()->CheckCondition(iParameter::Condition::SlowRemoteConnection) && !mBlocked && i>=0 && i<mPages.Size() && mPages[i].Index>=0)
	{
		mBlocked = true; // avoid loops
		this->GetMainWindow()->OpenBookPage(mPages[i].Index);
		mBlocked = false;
	}
}


//
//  Main class
//
iggAbstractExtension::iggAbstractExtension(iggMainWindow *mw, int id, const iImage *icon, const iString &title, bool withbook) : iggFrame(mw?mw->GetExtensionWindow()->mMainPanel:0,1), mId(id), mIcon(icon), mTitle(title), mWithBook(withbook)
{
	IASSERT(mw);
	mMainWindow = mw;

	if(mWithBook)
	{
		mBook = new iggExtensionFrameBook(this); IERROR_CHECK_MEMORY(mBook);
		this->AddLine(mBook);
	}
	else
	{
		mBook = 0;
	}
 
	mWasLaidOut = true;
}

	
iggAbstractExtension::~iggAbstractExtension()
{
	if(mBook != 0) mBook->Detach();
}


iggFrame* iggAbstractExtension::CreateFrontPage(const iImage *image, const iString &line2)
{
	iggFrame *page0;
	
	if(mBook != 0)
	{
		page0 = new iggFrame(mBook,3);
		mBook->AddPage("View",iggImageFactory::FindIcon("view.png"),page0,0);
	}
	else
	{
		page0 = new iggFrame(this,3);
		this->AddLine(page0);
	}

	page0->AddSpace(2);

	page0->AddLine(new iggWidgetTextLabel("%b%+"+this->GetTitle()+" Extension of IFrIT",page0),3);
	if(image != 0)
	{
		iggWidgetImageArea *ia = new iggWidgetImageArea(*image,false,page0);
		ia->SetFixedSize(image->Width(),image->Height());
		page0->AddLine(static_cast<iggWidget*>(0),ia);
	}
	page0->AddLine(new iggWidgetTextLabel("Full support for "+this->GetTitle()+" data format",page0),3);
	if(!line2.IsEmpty()) page0->AddLine(new iggWidgetTextLabel(line2,page0),3);

	page0->AddSpace(10);

	page0->SetColStretch(0,10);
	page0->SetColStretch(2,10);

	return page0;
}


void iggAbstractExtension::CompleteInitialization()
{
	this->CompleteInitializationBody();
}


void iggAbstractExtension::OpenBookPageByIndex(int index)
{
	if(mBook != 0) mBook->OpenPageByIndex(index);
}


void iggAbstractExtension::SetTabMode(int m)
{
	if(mBook != 0) mBook->SetTabMode(m);
}


//
//  Default menu construction
//
void iggAbstractExtension::ConfigureDefaultOpenFileMenu(int id, const iString &name, const iString &file, const iImage *icon, const iDataType &type)
{
	mEntry.Id = (1+mId)*1000 + id;
	mEntry.Name = name;
	mEntry.File = file;
	mEntry.Icon = icon;
	mEntry.Type = &type;
}


void iggAbstractExtension::PopulateFileMenu()
{
	if(mEntry.IsValid())
	{
		iggMenuHelper<iViewModule,int> mh(this->GetShell()->GetViewModuleHandle(),&iViewModule::GetCloneOfWindow,-1);
		this->GetMainWindow()->GetSubject()->AddMenuItem(mEntry.Id,"Open "+mEntry.Name+" file",mEntry.Icon,"",false,false,&mh);
	}
}


void iggAbstractExtension::PopulateFileToolBar()
{
	if(mEntry.IsValid())
	{
		iggMenuHelper<iViewModule,int> mh(this->GetShell()->GetViewModuleHandle(),&iViewModule::GetCloneOfWindow,-1);
		this->GetMainWindow()->GetSubject()->AddToolBarButton(mEntry.Id,"Open "+mEntry.Name.Substitute("&","")+" file",0,false,&mh);
	}
}


bool iggAbstractExtension::OnMenuBody(int id, bool on)
{
	if(id==mEntry.Id && mEntry.IsValid())
	{
		iDataSubject *subject = this->GetShell()->GetViewModule()->GetReader()->GetSubject(*mEntry.Type);
		if(subject==0 || subject->GetLoader()==0)
		{
			IBUG_ERROR("Unable to find a FileLoader to load the data. This most likely indicates a bug.");
		}
		else
		{
			iString ws = subject->GetLoader()->GetLastFileName();
			if(ws.IsEmpty()) ws = mEntry.Type->GetEnvironment(this->GetShell());
			iString fname = this->GetMainWindow()->GetFileName("Choose a file",ws,mEntry.File);
			if(!fname.IsEmpty())
			{
				this->GetShell()->GetViewModule()->GetReader()->LoadFile(*mEntry.Type,fname);
			}
		}
		return true;
	}
	return false;
}


//
//  Empty inheritable functions
//
void iggAbstractExtension::PopulateLocalDialogMenu()
{
}


void iggAbstractExtension::PopulateGlobalDialogMenu()
{
}


void iggAbstractExtension::PopulateShowToolBar()
{
}


void iggAbstractExtension::UpdateOnPick()
{
}


void iggAbstractExtension::UpdateParticleWidgets(const iImage * )
{
}


void iggAbstractExtension::AddReloadingDataTypes(iggPageData * )
{
}


const iImage* iggAbstractExtension::GetSpecialParticleIcon(const iDataType &type) const
{
	return 0;
}


void iggAbstractExtension::CompleteInitializationBody()
{
}


iggExtensionCreator::iggExtensionCreator(int n) : mId(iAbstractExtension::DefineToId(n))
{
	iggExtensionWindow::AttachCreator(this);
}

#endif
