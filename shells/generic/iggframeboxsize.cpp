/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggframeboxsize.h"


#include "ierror.h"
#include "iviewmodule.h"

#include "igghandle.h"
#include "iggshell.h"
#include "iggwidgetmisc.h"
#include "iggwidgetpropertycontrolbutton.h"
#include "iggwidgetpropertycontrollineedit.h"

#include "ibgwidgetbuttonsubject.h"

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"


using namespace iParameter;


namespace iggFrameBoxSize_Private
{
	
	class LineEdit : public iggWidgetPropertyControlFloatLineEdit
	{
		
	public:
		
		LineEdit(iggWidgetSimpleCheckBox *cb, const iString &label, const iggPropertyHandle &ph, iggFrameBoxSize *parent) : iggWidgetPropertyControlFloatLineEdit(label,ph,parent)
		{
			mRealParent = parent;
			mCheckBox = cb;
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			if(mCheckBox->IsChecked())
			{
				this->Enable(false);
			}
			else
			{
				this->Enable(true);
				this->iggWidgetPropertyControlFloatLineEdit::UpdateWidgetBody();
			}
		}

		virtual void OnVoid1Body()
		{
			iggWidgetPropertyControlFloatLineEdit::OnVoid1Body();
			if(mRealParent != 0)
			{
				mRealParent->UpdatePositions();
			}
		}

	private:

		iggFrameBoxSize *mRealParent;
		iggWidgetSimpleCheckBox *mCheckBox;
	};
	

	class CheckBox : public iggWidgetSimpleCheckBox
	{
		
	public:
		
		CheckBox(const char* text, iggFrameBoxSize *parent) : iggWidgetSimpleCheckBox(text,parent)
		{
			mRealParent = parent;

			this->SetBaloonHelp("Specify whether to use OpenGL coordinates (box axes go from -1 to 1) or to set the size of the box (axes go from 0 to box size)");
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			mSubject->SetDown(this->GetShell()->GetViewModule()->InOpenGLCoordinates());
		}

		virtual void OnChecked(bool s)
		{
			this->GetShell()->GetViewModule()->SetBoxSize(s?0.0:1.0);
			if(mRealParent != 0)
			{
				mRealParent->UpdatePositions();
			}
			this->Render();
		}

	private:

		iggFrameBoxSize *mRealParent;
	};
};


using namespace iggFrameBoxSize_Private;


iggFrameBoxSize::iggFrameBoxSize(iggFrame *parent) : iggFrame("Coordinates",parent,1)
{
	CheckBox *cb = new CheckBox("Use OpenGL coordinates",this);
	LineEdit *le = new LineEdit(cb,"Box size",iggPropertyHandle("BoxSize",this->GetShell()->GetViewModuleHandle()),this);
	cb->AddDependent(le);

	this->AddLine(cb);
	this->AddLine(le);
}


void iggFrameBoxSize::UpdatePositions()
{
	int i;
	iLookupArray<iggWidgetPropertyControlBase*> &list(iggWidgetPropertyControlBase::PositionList());

	for(i=0; i<list.Size(); i++)
	{
		list[i]->UpdateWidget();
	}
}

#endif
