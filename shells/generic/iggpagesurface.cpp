/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggpagesurface.h"


#include "isurfaceviewsubject.h"
#include "iviewmodule.h"
#include "iviewobject.h"

#include "iggframedatavariablelist.h"
#include "iggframeobjectcontrols.h"
#include "iggframepaintingcontrols.h"
#include "igghandle.h"
#include "iggimagefactory.h"
#include "iggmainwindow.h"
#include "iggshell.h"
#include "iggwidgetarea.h"
#include "iggwidgetmisc.h"
#include "iggwidgetotherbutton.h"
#include "iggwidgetpropertycontrolbutton.h"
#include "iggwidgetpropertycontrolcolorselection.h"
#include "iggwidgetpropertycontrolselectionbox.h"

#include "ibgframesubject.h"
#include "ibgwidgetareasubject.h"
#include "ibgwidgetbuttonsubject.h"
#include "ibgwidgethelper.h"
#include "ibgwidgetselectionboxsubject.h"

#include "iggsubjectfactory.h"

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"
#include "iggwidgetpropertycontrolslider.tlh"


using namespace iParameter;


namespace iggPageSurface_Private
{
	//
	//  Helper classes
	//
	class MethodRadioBox : public iggWidgetPropertyControlRadioBox
	{
		
	public:
		
		MethodRadioBox(const iggPropertyHandle& ph, iggFrame *parent) : iggWidgetPropertyControlRadioBox(1,"Method",0,ph,parent)
		{
			mFlipBuddy = 0;
			mObjectBuddy = 0;

			this->InsertItem("Isosurface");
			this->InsertItem("Sphere");
			this->InsertItem("Plane");
		}

		void SetFlipBuddy(iggFrameFlip *b)
		{
			mFlipBuddy = b;
		}

		void SetObjectBuddy(iggFrameObjectControls *b)
		{
			mObjectBuddy = b;
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			this->iggWidgetPropertyControlRadioBox::UpdateWidgetBody();
			this->UpdateBuddy();
		}
		
		virtual void OnInt1Body(int i)
		{
			iggWidgetPropertyControlRadioBox::OnInt1Body(i);
			this->UpdateBuddy();
		}

		virtual void UpdateBuddy()
		{
			int i = mSubject->GetValue();

			if(mFlipBuddy != 0)
			{
				if(i == 0)
				{
					mFlipBuddy->ShowLayer(0);
				}
				else
				{
					mFlipBuddy->ShowLayer(1);
				}
			}

			if(mObjectBuddy == 0) return;

			switch(i)
			{
			case 0:
				{
					break;
				}
			case 1:
				{
					mObjectBuddy->EnableDirection(false);
					mObjectBuddy->EnableSize(true);
					break;
				}
			case 2:
				{
					mObjectBuddy->EnableDirection(true);
					mObjectBuddy->EnableSize(false);
					break;
				}
			default:
				{
					IBUG_WARN("Incorrect button in MethodRadioBox.");
				}
			}
		}

		iggFrameFlip *mFlipBuddy;
		iggFrameObjectControls *mObjectBuddy;
	};
	

	class BatchCreateButton: public iggWidgetSimpleButton
	{

	public:

		BatchCreateButton(iggPageObject *owner, iggWidgetSimpleSpinBox *spinBox1, iggWidgetSimpleSpinBox *spinBox2, iggWidgetSimpleSpinBox *spinBox3, iggFrame *parent) : iggWidgetSimpleButton("Create",parent)
		{
			mOwner = owner;
			mSpinBox1 = spinBox1;
			mSpinBox2 = spinBox2;
			mSpinBox3 = spinBox3;

			this->SetBaloonHelp("Create the instances.","Create several new instances frm the values specified in spin boxes.");
		}

	protected:

		virtual void Execute()
		{
			iSurfaceViewSubject *subject = iRequiredCast<iSurfaceViewSubject>(INFO,mOwner->GetViewSubject(-1));

			if(!subject->InterpolateBetweenTwoInstances(mSpinBox1->GetValue()-1,mSpinBox2->GetValue()-1,mSpinBox3->GetValue()))
			{
				mOwner->GetShell()->PopupWindow("Unable to complete batch creating - perhaps, ran out of memory.",MessageType::Error);
				this->Enable(false);
				return;
			}

			this->Render();
		}

		virtual void UpdateWidgetBody()
		{
			int n = mOwner->GetViewSubject(-1)->GetNumberOfInstances();

			if(mSpinBox1->Count() != n) mSpinBox1->SetRange(1,n);
			if(mSpinBox2->Count() != n) mSpinBox2->SetRange(1,n);

			this->Enable(this->AreTwoInstancesConsistent());
		}
		
		bool AreTwoInstancesConsistent() const
		{
			//
			//  Split one if for debugging
			//
			if(mSpinBox1->GetValue() == mSpinBox2->GetValue()) return false;

			iSurfaceViewSubject *subject = iRequiredCast<iSurfaceViewSubject>(INFO,mOwner->GetViewSubject(-1));
			return subject->InterpolateBetweenTwoInstances(mSpinBox1->GetValue()-1,mSpinBox2->GetValue()-1,1,true);
		}

		iggPageObject *mOwner;
		iggWidgetSimpleSpinBox *mSpinBox1, *mSpinBox2, *mSpinBox3;
	};


	class BatchCreateFrame : public iggFrame
	{
		
	public:

		BatchCreateFrame(iggPageObject *owner, iggFrame *parent) : iggFrame("Interpolate between instances",parent,2)
		{
			mOwner = owner;

			mSpinBox1 = new iggWidgetSimpleSpinBox(1,1,1,"",this);
			mSpinBox1->SetBaloonHelp("Choose the 1st instance for interpolation");
			this->AddLine(new iggWidgetTextLabel("1st instance",this),mSpinBox1);

			mSpinBox2 = new iggWidgetSimpleSpinBox(1,1,1,"",this);
			mSpinBox2->SetBaloonHelp("Choose the 2nd instance for interpolation");
			this->AddLine(new iggWidgetTextLabel("2nd instance",this),mSpinBox2);

			mSpinBox3 = new iggWidgetSimpleSpinBox(1,100,1,"",this);
			mSpinBox3->SetBaloonHelp("Choose the number of instances to create between the two limits");
			this->AddLine(new iggWidgetTextLabel("Number to create",this),mSpinBox3);

			mButton = new BatchCreateButton(mOwner,mSpinBox1,mSpinBox2,mSpinBox3,this);
			this->AddLine(0,mButton);

			mSpinBox1->AddDependent(mButton);
			mSpinBox2->AddDependent(mButton);
			mSpinBox3->AddDependent(mButton);

			this->SetColStretch(0,10);

			this->SetBaloonHelp("Creates multiple surfaces by interpolating between the two other instances. Press Shift+F1 for more help.","This control allows you to create several (up to 100) surfaces at once, by interpolating between two existing different instances. The two instances must be compatible with each other: they must use the same method (for example, both being isosurfaces of the same scalar field variable, or both being spheres, etc) and must both use the same data channel. During the interpolation, surface opacities, isosurface leveles, colors, sphere and plane positions, sphere sizes, and plane directions are all interpolated linear between the two specified instances. The batch creation may be useful for, for example, creating a set of semi-transparent isosurface as a surrogate for volume rendering.");
		}

	protected:

		iggPageObject *mOwner;
		iggWidgetSimpleSpinBox *mSpinBox1, *mSpinBox2, *mSpinBox3;
		BatchCreateButton *mButton;
	};


	class PrintInfoButton : public iggWidgetSimpleButton
	{

	public:

		PrintInfoButton(iggPageObject *owner, iggFrame *parent) : iggWidgetSimpleButton("Print info",parent)
		{
			mOwner = owner;

			this->SetBaloonHelp("Print some information about the current surface");
		}

	protected:

		virtual void Execute()
		{
			this->GetMainWindow()->BLOCK(true);

			iSurfaceViewSubject *subject = iRequiredCast<iSurfaceViewSubject>(INFO,mOwner->GetViewSubject(-1));
			
			if (subject->GetNumberOfInstances() > mOwner->GetActiveInstance())
			{
				iString s;
				subject->OutputProperties(mOwner->GetActiveInstance(), s);
				this->GetShell()->PopupWindow(s,MessageType::Information);
			}

			this->GetMainWindow()->BLOCK(false);
		}

		iggPageObject *mOwner;
	};


	//
	//  Main page
	// ************************************************
	//
	class MainPage : public iggMainPage2
	{

	public:

		MainPage(iggPageObject *owner, iggFrameBase *parent) : iggMainPage2(owner,parent,true)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		virtual void InsertSectionA(iggFrame *page)
		{
			//
			//  Top half-page sub-frame
			//
			iggFrame *page0top = new iggFrame(page,4);
			page->AddLine(page0top,3);
			//
			//  Method & flip normals
			//
			iggFrame *mf = new iggFrame(page0top,2);
			MethodRadioBox *mb = new MethodRadioBox(mOwner->CreatePropertyHandle("Method"),mf);
			iggFrame *mf2 = new iggFrame(mf,1);
			mf2->AddSpace(1);
			mf2->AddLine(new iggWidgetPropertyControlColorSelection(mOwner->CreatePropertyHandle("Color"),mf2));
			mf2->AddLine(new PrintInfoButton(mOwner,mf2));
			mf2->AddSpace(3);
			mf2->AddLine(new iggWidgetPropertyControlCheckBox("Flip normals",mOwner->CreatePropertyHandle("NormalsFlipped"),mf2));
			mf->AddLine(mb,mf2);
			page0top->AddLine(mf,2);
			page0top->AddSpace(2);
			//
			//  Opacity
			//
			iggWidgetPropertyControlOpacitySlider *os = new iggWidgetPropertyControlOpacitySlider(mOwner->CreatePropertyHandle("Opacity"),page0top);
			page0top->AddLine(os,3);
			iggWidgetAllInstancesCheckBox *li = new iggWidgetAllInstancesCheckBox(page0top);
			li->AddDependent(os);
			page0top->AddLine(li,2);
			page0top->AddSpace(10);

			page0top->SetColStretch(2,3);

			page->AddSpace(10);

			//
			//  Lower half-page: various method-dependent settings in a headless mBook
			//
			iggFrame *page0bot = new iggFrame(page,1);
			page->AddLine(page0bot,3);

			iggFrameFlip *msflip = new iggFrameFlip(page0bot);
			page0bot->AddLine(msflip);
			page0bot->SetColStretch(0,10);
			//	page0bot->SetColStretch(1,1);
			mb->SetFlipBuddy(msflip);

			//
			//  Isosurface settings
			//
			iggFrame *mspage0 = new iggFrame(msflip,4);
			msflip->AddLayer(mspage0);
			mspage0->SetColStretch(0,0);
			mspage0->SetColStretch(1,0);
			mspage0->SetColStretch(2,0);
			mspage0->SetColStretch(3,10);

			iggPropertyHandle ph(mOwner->CreatePropertyHandle("IsoVar"));
			iggWidget *il = new iggWidgetPropertyControlVariableLimitsSlider(0,5,"Level",mOwner->GetDataSubjectHandle(),mOwner->CreatePropertyHandle("IsoLevel"),mspage0,&ph);
			mspage0->AddLine(il,4);

			iggFrame *iv = new iggFrame(mspage0,1);
			iggFrameDataVariableList *io = new iggFrameDataVariableList("Isosurface of...",mOwner->GetDataSubjectHandle(),mOwner->CreatePropertyHandle("IsoVar"),iv);
			io->Complete();
			io->AddDependent(il);
			iv->AddLine(io);
			iv->AddLine(new iggWidgetPropertyControlCheckBox("Optimize",mOwner->CreatePropertyHandle("IsoOptimization"),iv));

			iggWidgetPropertyControlRadioBox *id = new iggWidgetPropertyControlRadioBox(1,"Reduction",0,mOwner->CreatePropertyHandle("IsoReduction"),mspage0);
			id->InsertItem("None");
			id->InsertItem("Light");
			id->InsertItem("Medium");
			id->InsertItem("Heavy");

			iggWidgetPropertyControlRadioBox *is = new iggWidgetPropertyControlRadioBox(1,"Smoothing",0,mOwner->CreatePropertyHandle("IsoSmoothing"),mspage0);
			is->InsertItem("None");
			is->InsertItem("Light");
			is->InsertItem("Medium");
			is->InsertItem("Heavy");

			mspage0->AddLine(iv,id,is);
			mspage0->AddLine(new iggWidgetPropertyControlCheckBox("Use alternative reduction method",mOwner->CreatePropertyHandle("AlternativeReductionMethod"),mspage0),3);

			mspage0->AddSpace(10);
			mspage0->SetColStretch(3,10);

			//
			//  Sphere & plane settings
			//
			iggFrame *mspage1 = new iggFrame(msflip);
			msflip->AddLayer(mspage1);

			iggFrameObjectControls *sc= new iggFrameObjectControls("",mOwner->CreatePropertyHandle("Position"),mOwner->CreatePropertyHandle("PlaneDirection"),mOwner->CreatePropertyHandle("SphereSize"),mspage1);
			mb->SetObjectBuddy(sc);
			mspage1->AddLine(sc);

			mspage1->AddSpace(10);
			mspage0->SetColStretch(0,10);
		}
	};


	//
	//  Paint page
	// ************************************************
	//
	class PaintPage : public iggPaintPage2
	{

	public:

		PaintPage(iggPageObject *owner, iggFrameBase *parent) : iggPaintPage2(owner,parent,iggPaintPage2::WithBrightness|iggPaintPage2::WithShading|iggPaintPage2::WithAllInstances)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		virtual void InsertSectionA(iggFrame *page)
		{
			iggFramePaintingControls *pl = new iggFramePaintingControls("Paint with...",true,mOwner->GetDataSubjectHandle(),mOwner->CreatePropertyHandle("PaintVar"),page);

			page->AddLine(pl,2);
			page->AddSpace(10);
		}
	};

	//
	//  Batch page
	// ************************************************
	//
	class BatchPage : public iggPage2
	{

	public:

		BatchPage(iggPageObject *owner, iggFrameBase *parent) : iggPage2(owner,parent,2)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		virtual void CompleteConstructionBody()
		{
			this->AddLine(new BatchCreateFrame(mOwner,this));
			this->AddSpace(10);
			this->SetColStretch(1,10);
		}
	};
};


using namespace iggPageSurface_Private;


iggPageSurface::iggPageSurface(iggFrameBase *parent) : iggPageObject("Instance",parent,"s",iggImageFactory::FindIcon("surf.png"))
{
	//
	//  Main page
	// ************************************************
	//
	mBook->AddPage("Main",this->Icon(),new MainPage(this,mBook));

	//
	//  Paint page
	// ************************************************
	//
	mBook->AddPage("Paint",this->Icon(),new PaintPage(this,mBook));

	//
	//  Instances page
	// ************************************************
	//
	mBook->AddPage("Batch creation",this->Icon(),new BatchPage(this,mBook));

	//
	//  Replicate page
	// ************************************************
	//
	mBook->AddPage("Replicate",this->Icon(),new iggReplicatePage2(this,mBook,true));
}

#endif
