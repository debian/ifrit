/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggwidgetrendermodebutton.h"


#include "imarker.h"
#include "ishell.h"
#include "iviewmodule.h"

#include "iggimagefactory.h"
#include "iggwidgetpropertycontrol.h"

#include "ibgwidgetbuttonsubject.h"

#include "iggsubjectfactory.h"

//
//  Template
//
#include "iarray.tlh"


using namespace iParameter;


//
//  RenderMode button
//
iggWidgetRenderModeButton::iggWidgetRenderModeButton(iggWidgetPropertyControlBase *parent, bool registered) : iggWidget(parent==0?0:parent->GetParent())
{
	mParent = parent; IASSERT(mParent)

	mDelayIcon = iggImageFactory::FindIcon("delay.png");
	mReadyIcon = iggImageFactory::FindIcon("ready.png");
	if(mDelayIcon==0 || mReadyIcon==0)
	{
		IBUG_WARN("Icon images are not found.");
	}
	if(registered) List().AddUnique(this);

	mSubject = iggSubjectFactory::CreateWidgetButtonSubject(this,ButtonType::ToolButton,"",1);
	mSubject->SetFlat(true);
	mSubject->SetSize(18,18);

	//
	//  Since the subject belongs to something else, detach it
	//
	mSubjectOwner = false;

	this->SetBaloonHelp("Toggles the rendering mode between Delayed/Immediate. Press Shift+F1 for more help.","Widgets that require continuing interaction (such as sliders or line edits) support two modes: in the Delayed mode rendering of the visualization scene is performed only after the widget is released; in the Immediate mode every change of the widget state causes rendering of the scene. If the scene is complex and the rendering process is slow, the widget will feel \"jerky\" in the Immediate mode.");
}


iggWidgetRenderModeButton::~iggWidgetRenderModeButton()
{
	List().Remove(this);
}


void iggWidgetRenderModeButton::UpdateWidgetBody()
{
	switch(mParent->GetRenderMode())
	{
	case RenderMode::Delayed:
		{
			if(mDelayIcon != 0) mSubject->SetIcon(*mDelayIcon);
			break;
		}
	case RenderMode::Immediate:
		{
			if(mReadyIcon != 0) mSubject->SetIcon(*mReadyIcon);
			break;
		}
	default:
		{
			IBUG_WARN("iggWidgetRenderModeButton should only be used with rendering widgets.");
		}
	}
}


void iggWidgetRenderModeButton::OnVoid1Body()
{
	switch(mParent->GetRenderMode())
	{
	case RenderMode::Delayed:
		{
			mParent->SetRenderMode(RenderMode::Immediate);
			break;
		}
	case RenderMode::Immediate:
		{
			mParent->SetRenderMode(RenderMode::Delayed);
			break;
		}
	default:
		{
			IBUG_WARN("iggWidgetRenderModeButton should only be used with rendering widgets.");
		}
	}
	this->UpdateWidget();
}


void iggWidgetRenderModeButton::ShowAll(bool s)
{
	int i;
	iLookupArray<iggWidgetRenderModeButton*> &list(List());

	for(i=0; i<list.Size(); i++)
	{
		list[i]->Show(s);
	}
}


void iggWidgetRenderModeButton::UpdateAll()
{
	int i;
	iLookupArray<iggWidgetRenderModeButton*> &list(List());

	for(i=0; i<list.Size(); i++)
	{
		list[i]->UpdateWidget();
	}
}


iLookupArray<iggWidgetRenderModeButton*>& iggWidgetRenderModeButton::List()
{
	static iLookupArray<iggWidgetRenderModeButton*> list(100);
	return list;
}


void iggWidgetRenderModeButton::ClearLaidOutFlag(const ibgWidgetEntrySubject *e)
{
	if(e != 0) mWasLaidOut = true;
}


void iggWidgetRenderModeButton::ClearLaidOutFlag(const ibgWidgetTrackBallSubject *t)
{
	if(t != 0) mWasLaidOut = true;
}

#endif
