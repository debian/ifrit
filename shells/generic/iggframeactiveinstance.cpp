/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggframeactiveinstance.h"


#include "ierror.h"
#include "imath.h"

#include "iggimagefactory.h"
#include "iggmainwindow.h"
#include "iggsubjectfactory.h"
#include "iggwidgetotherbutton.h"

#include "ibgwidgetbuttonsubject.h"
#include "ibgwidgetselectionboxsubject.h"


namespace iggFrameActiveInstance_Private
{
	//
	//  Special widgets that update their parents on execution
	//
	class InstanceSpinBox : public iggWidget
	{

	public:

		InstanceSpinBox(const iString &title, iggFrameActiveInstance *parent) : iggWidget(parent)
		{
			mParent = parent; IASSERT(mParent);

			mSubject = iggSubjectFactory::CreateWidgetSpinBoxSubject(this,1,1,title,1);
		
			this->SetBaloonHelp("Set the current instance.","This control allows a user to specify which of several existing instances of this object is controlled by the GUI page.");
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			int n = mParent->GetNumberOfInstances();

			if(n > 0)
			{
				this->Enable(true);
				mSubject->SetFirstEntryText("");
				if(mSubject->Count() != n)
				{
					//
					//  Update the selection box
					//
					mSubject->SetRange(1,mParent->GetNumberOfInstances());
				}
				mSubject->SetValue(1+mParent->GetActiveInstance());
			}
			else
			{
				mSubject->SetFirstEntryText("none");
				this->Enable(false);
			}
		}

		virtual void OnInt1Body(int v)
		{
			mParent->SetActiveInstance(v-1);
		}

		iggFrameActiveInstance *mParent;
		ibgWidgetSpinBoxSubject *mSubject;
	};
	
	class InstanceComboBox : public iggWidget
	{

	public:

		InstanceComboBox(const iString &title, const iString &item, iggFrameActiveInstance *parent) : iggWidget(parent), mItemText(item)
		{
			mParent = parent; IASSERT(mParent);

			mSubject = iggSubjectFactory::CreateWidgetComboBoxSubject(this,title);

			this->SetBaloonHelp("Set the current instance.","This control allows a user to specify which of several existing instances of this object is controlled by the GUI page.");
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			int n = mParent->GetNumberOfInstances();

			if(n > 0)
			{
				this->Enable(true);
				if(mSubject->Count() != n)
				{
					int i;

					mSubject->Clear();
					for(i=0; i<mParent->GetNumberOfInstances(); i++) mSubject->InsertItem((mItemText+"#"+iString::FromNumber(i+1)).ToCharPointer());
				}
				mSubject->SetValue(1+mParent->GetActiveInstance());
			}
			else
			{
				mSubject->Clear();
				this->Enable(false);
			}
		}

		virtual void OnInt1Body(int v)
		{
			mParent->SetActiveInstance(v-1);
		}

		const iString mItemText;
		iggFrameActiveInstance *mParent;
		ibgWidgetComboBoxSubject *mSubject;
	};

	class CreateDeleteButton : public iggWidgetSimpleButton
	{

	public:

		CreateDeleteButton(bool create, iggFrameActiveInstance *parent) : iggWidgetSimpleButton("",parent,true)
		{
			mIsCreate = create;
			mParent = parent; IASSERT(mParent);

			if(mIsCreate)
			{
				mSubject->SetIcon(*iggImageFactory::FindIcon("create2.png"));
			}
			else
			{
				mSubject->SetIcon(*iggImageFactory::FindIcon("delete2.png"));
			}

			this->SetBaloonHelp("Creates/Deletes an instance of the object.");
		}

	protected:

		virtual void Execute()
		{
			this->GetMainWindow()->BLOCK(true);

			if(mIsCreate)
			{
				if(mParent->CreateInstance())
				{
					this->UpdateWidget();
				}
			}
			else
			{
				if(mParent->DeleteInstance(mParent->GetActiveInstance()))
				{
					this->UpdateWidget();
				}
			}

			this->GetMainWindow()->BLOCK(false);
			this->Render();
		}

		virtual void UpdateWidgetBody()
		{
			if(mIsCreate)
			{
				this->Enable(mParent->GetNumberOfInstances() < mParent->GetMaxNumberOfInstances());
			}
			else
			{
				this->Enable(mParent->GetNumberOfInstances() > mParent->GetMinNumberOfInstances());
			}
		}

		bool mIsCreate;
		iggFrameActiveInstance *mParent;
	};
};


using namespace iggFrameActiveInstance_Private;


iggFrameActiveInstance::iggFrameActiveInstance(bool withCreateDelete, const iString &title, const iString &item, iggFrame *parent, bool spinbox) : iggFrame(parent,3)
{
	iggWidget *w;

	if(spinbox)
	{
		w = new InstanceSpinBox(title,this);
	}
	else
	{
		w = new InstanceComboBox(title,item,this);
	}

	if(withCreateDelete)
	{
		this->AddLine(w,new CreateDeleteButton(true,this),new CreateDeleteButton(false,this));
	}
	else
	{
		this->AddLine(w);
	}
}


int iggFrameActiveInstance::GetMinNumberOfInstances() const
{
	return 0;
}


int iggFrameActiveInstance::GetMaxNumberOfInstances() const
{
	return INT_MAX;
}

#endif
