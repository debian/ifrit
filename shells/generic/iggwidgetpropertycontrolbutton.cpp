/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggwidgetpropertycontrolbutton.h"


#include "ishell.h"

#include "iggframe.h"
#include "iggmainwindow.h"

#include "ibgwidgetbuttonsubject.h"

#include "iggsubjectfactory.h"

//
//  Templates
//
#include "iarray.tlh"
#include "iggwidgetpropertycontrol.tlh"


using namespace iParameter;


//
//******************************************
//
//  Generic button
//
//******************************************
//
iggWidgetPropertyControlButton::iggWidgetPropertyControlButton(int type, const iString &text, const iggPropertyHandle& ph, iggFrame *parent) : iggWidgetPropertyControl<iType::Bool>(ph,parent,RenderMode::Immediate)
{
	mSubject = iggSubjectFactory::CreateWidgetButtonSubject(this,type,text,1);
	mReverse = false;
}


void iggWidgetPropertyControlButton::SetReverse(bool s)
{
	mReverse = s;
}


void iggWidgetPropertyControlButton::QueryValue(bool &val) const
{
	val = mSubject->IsDown();
	if(mReverse) val = !val;
}


void iggWidgetPropertyControlButton::UpdateValue(bool val)
{
	if(mReverse) val = !val;
	mSubject->SetDown(val);
}


void iggWidgetPropertyControlButton::OnVoid1Body()
{
	this->ExecuteControl(true);
}


//
//******************************************
//
//  CheckBox
//
//******************************************
//
iggWidgetPropertyControlCheckBox::iggWidgetPropertyControlCheckBox(const iString &text, const iggPropertyHandle& ph, iggFrame *parent) : iggWidgetPropertyControlButton(ButtonType::CheckBox,text,ph,parent)
{
}


//
//******************************************
//
//  PushButtons for 'action' keys
//
//******************************************
//
iggWidgetPropertyControlExecButton::iggWidgetPropertyControlExecButton(const iString &text, const iggPropertyHandle& ph, iggFrame *parent) : iggWidgetPropertyControlButton(ButtonType::PushButton,text,ph,parent)
{
}


void iggWidgetPropertyControlExecButton::UpdateWidgetBody()
{
	//
	//  Nothing to update
	//
}


void iggWidgetPropertyControlExecButton::QueryValue(bool &val) const
{
	val = true;
}


iggWidgetActionControlExecButton::iggWidgetActionControlExecButton(const iString &text, const iggPropertyHandle& ph, iggFrame *parent) : iggWidgetPropertyControlExecButton(text,ph,parent)
{
}


bool iggWidgetActionControlExecButton::ExecuteControl(bool final, bool)
{
	if(iDynamicCast<const iPropertyAction >(INFO,this->Handle()->Function())->CallAction())
	{
		this->SetRenderTarget((this->GetExecuteFlags() & ExecuteFlag::Module::Mask));
		if(this->GetRenderMode()==RenderMode::Immediate || (final && this->GetRenderMode()!=RenderMode::DoNotRender)) this->Render();
		return true;
	}
	else return false;
}


//
//******************************************
//
//  ToolButton (a small PushButton) for 'action' keys
//
//******************************************
//
iggWidgetPropertyControlToolButton::iggWidgetPropertyControlToolButton(const iString &text, const iggPropertyHandle& ph, iggFrame *parent) : iggWidgetPropertyControlButton(ButtonType::ToolButton,text,ph,parent)
{
}


void iggWidgetPropertyControlToolButton::UpdateWidgetBody()
{
	//
	//  Nothing to update
	//
}


void iggWidgetPropertyControlToolButton::QueryValue(bool &val) const
{
	val = true;
}


//
//******************************************
//
//  Toggle (tool) button
//
//******************************************
//
iggWidgetPropertyControlToggleButton::iggWidgetPropertyControlToggleButton(const iImage *icon, const iString &text, const iggPropertyHandle& ph, iggFrame *parent) : iggWidgetPropertyControlButton(ButtonType::ToggleButton,text,ph,parent), mIcon(icon)
{
	if(icon != 0) mSubject->SetIcon(*icon);
}

//
//******************************************
//
//  Special PushButton for loading file names
//
//******************************************
//
iggWidgetPropertyControlFileNameButton::iggWidgetPropertyControlFileNameButton(const iString &text, const iString &header, const iString &selection, const iggPropertyHandle& ph, iggFrame *parent) : iggWidgetPropertyControl<iType::String>(ph,parent,RenderMode::Immediate)
{
	mSubject = iggSubjectFactory::CreateWidgetButtonSubject(this,ButtonType::ToolButton,text,1);

	mHeader = header;
	mSelection = selection;
	mDefault = this->GetMainWindow()->GetShell()->GetEnvironment(Environment::Base);
}


void iggWidgetPropertyControlFileNameButton::SetDefaut(const iString &s)
{
	mDefault = s;
}


void iggWidgetPropertyControlFileNameButton::UpdateWidgetBody()
{
	//
	//  Nothing to update
	//
}


void iggWidgetPropertyControlFileNameButton::QueryValue(iString &val) const
{
	val = this->GetMainWindow()->GetFileName(mHeader,this->GetMainWindow()->GetShell()->GetEnvironment(Environment::Base),"Images (*.jpg *.jpeg *.pnm *.bmp *.png *.tif *.tiff)");
}


void iggWidgetPropertyControlFileNameButton::UpdateValue(const iString&)
{
}


void iggWidgetPropertyControlFileNameButton::OnVoid1Body()
{
	this->ExecuteControl(true);
}


//
//******************************************
//
//  Exec button for an int-valued key
//
//******************************************
//
iggWidgetPropertyControlExecIntButton::iggWidgetPropertyControlExecIntButton(int value, const iString &text, const iggPropertyHandle& ph, iggFrame *parent) : iggWidgetPropertyControl<iType::Int>(ph,parent,RenderMode::Immediate)
{
	mSubject = iggSubjectFactory::CreateWidgetButtonSubject(this,ButtonType::PushButton,text,1);
	mValue = value;
}


void iggWidgetPropertyControlExecIntButton::QueryValue(int &val) const
{
	val = mValue;
}


void iggWidgetPropertyControlExecIntButton::UpdateWidgetBody()
{
	//
	//  Nothing to update
	//
}


void iggWidgetPropertyControlExecIntButton::UpdateValue(int)
{
	//
	//  Nothing to update
	//
}


void iggWidgetPropertyControlExecIntButton::OnVoid1Body()
{
	this->ExecuteControl(true);
}

#endif
