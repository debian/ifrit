/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IGGWIDGETPROPERTYCONTROLLINEEDIT_H
#define IGGWIDGETPROPERTYCONTROLLINEEDIT_H

//
//  Non-abstract classes defined in this file
//
// iggWidgetPropertyControlTextLineEdit;
// iggWidgetPropertyControlFloatLineEdit;
// iggWidgetPropertyControlPositionLineEdit;
// iggWidgetPropertyControlSizeLineEdit;
// iggWidgetPropertyControlFileNameLineEdit;
//
#include "iggwidgetpropertycontrol.h"


#include "istring.h"

class ibgWidgetButtonSubject;
class ibgWidgetEntrySubject;


template<iType::TypeId id> 
class iggWidgetPropertyControlLineEdit : public iggWidgetPropertyControl<id>
{

public:

	typedef typename iType::Traits<id>::Type			ArgT;
	typedef typename iType::Traits<id>::ContainerType	ValT;

	iggWidgetPropertyControlLineEdit(bool numeric, bool readonly, const iString &label, const iggPropertyHandle& ph, iggFrame *parent, int rm);

	virtual void Enable(bool s);
	void SetText(const iString &text);

	virtual void QueryValue(ValT &val) const;

protected:

	virtual void UpdateValue(ArgT val);

	virtual void OnVoid1Body();

	virtual void ConvertToString(ArgT val, iString &s) const = 0;
	virtual bool ConvertFromString(const iString &s, ValT &val) const = 0;

	ibgWidgetEntrySubject *mSubject;
};


class iggWidgetPropertyControlTextLineEdit : public iggWidgetPropertyControlLineEdit<iType::String>
{

public:

	iggWidgetPropertyControlTextLineEdit(bool readonly, const iString &label, const iggPropertyHandle& ph, iggFrame *parent, int rm = iParameter::RenderMode::Delayed);

protected:

	virtual void ConvertToString(const iString& val, iString &s) const;
	virtual bool ConvertFromString(const iString &s, iString &val) const;
};


class iggWidgetPropertyControlFloatLineEdit : public iggWidgetPropertyControlLineEdit<iType::Float>
{

public:

	iggWidgetPropertyControlFloatLineEdit(const iString &label, const iggPropertyHandle& ph, iggFrame *parent, int rm = iParameter::RenderMode::Delayed);

protected:

	virtual void ConvertToString(float val, iString &s) const;
	virtual bool ConvertFromString(const iString &s, float &val) const;
};


class iggWidgetPropertyControlGenericPositionLineEdit : public iggWidgetPropertyControlLineEdit<iType::Double>
{

public:

	iggWidgetPropertyControlGenericPositionLineEdit(const iString &label, const iggPropertyHandle& ph, iggFrame *parent, int rm);
	virtual ~iggWidgetPropertyControlGenericPositionLineEdit();
};


class iggWidgetPropertyControlPositionLineEdit : public iggWidgetPropertyControlGenericPositionLineEdit
{

public:

	iggWidgetPropertyControlPositionLineEdit(const iString &label, const iggPropertyHandle& ph, iggFrame *parent, int rm = iParameter::RenderMode::Delayed);

protected:

	virtual void ConvertToString(double val, iString &s) const;
	virtual bool ConvertFromString(const iString &s, double &val) const;
};


class iggWidgetPropertyControlSizeLineEdit : public iggWidgetPropertyControlGenericPositionLineEdit
{

public:

	iggWidgetPropertyControlSizeLineEdit(const iString &label, const iggPropertyHandle& ph, iggFrame *parent, int rm = iParameter::RenderMode::Delayed);

protected:

	virtual void ConvertToString(double val, iString &s) const;
	virtual bool ConvertFromString(const iString &s, double &val) const;
};


class iggWidgetPropertyControlFileNameLineEdit : public iggWidgetPropertyControlTextLineEdit
{

public:

	iggWidgetPropertyControlFileNameLineEdit(bool render, const iString &header, const iString &location, const iString &selection, bool exists, const iString &label, bool unloadable, const iggPropertyHandle& ph, iggFrame *parent);

	void OpenLoadFileDialog();
	void UnloadFile();

protected:

	virtual void OnVoid1Body();
	virtual void UpdateWidgetBody();

	bool mFileExists, mUnloadable, mRender;
	iString mHeader, mLocation, mSelection;
};

#endif  // IGGWIDGETPROPERTYCONTROLLINEEDIT_H

