/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "igghandle.h"


#include "idata.h"
#include "ierror.h"
#include "iviewmodule.h"
#include "iviewobject.h"
#include "iviewsubject.h"

#include "iggpageobject.h"
#include "iggshell.h"

//
//  Templates
//
#include "iarray.tlh"


//
//  Object handles
//  *****************************************
//
//  Base class
//
iggObjectHandle::iggObjectHandle(iggShell *s)
{
	mShell = s; IASSERT(mShell);
}


iggObjectHandle::iggObjectHandle(iggElement *el)
{
	IASSERT(el);
	mShell = el->GetShell(); IASSERT(mShell);
}


//
//  Handle for passing index values
//  *****************************************
//
//  Base class
//
iggIndexHandle::iggIndexHandle()
{
}


//
//  trivial handle that returns a fixed index value
//
iggFixedIndexHandle::iggFixedIndexHandle(int index) : mIndex(index)
{
}


int iggFixedIndexHandle::Index() const
{
	return mIndex;
}


//
//  Property handle
//  *****************************************
//
iggPropertyHandle::iggPropertyHandle(const iString &name, const iggObjectHandle* oh, int index) : mName(name), mObjectHandle(oh), mIndex(index), mIndexHandle(0)
{
	IASSERT(oh);

	this->Define(0);
}


iggPropertyHandle::iggPropertyHandle(const iString &name, const iggObjectHandle* oh, const iggIndexHandle* ih) : mName(name), mObjectHandle(oh), mIndex(0), mIndexHandle(ih)
{
	IASSERT(oh);

	this->Define(0);
}


iggPropertyHandle::iggPropertyHandle(const iString &name, const iggObjectHandle* oh, const iggPropertyHandle& ih) : mName(name), mObjectHandle(oh), mIndex(0), mIndexHandle(0)
{
	IASSERT(oh);

	this->Define(&ih);
}


iggPropertyHandle::iggPropertyHandle(const iggPropertyHandle& other) : mName(other.mName), mObjectHandle(other.mObjectHandle), mIndex(other.mIndex), mIndexHandle(other.mIndexHandle)
{
	this->Define(other.mIndexHandle2);
}


iggPropertyHandle::iggPropertyHandle(const iggPropertyHandle& other, const iString &name) : mName(name), mObjectHandle(other.mObjectHandle), mIndex(other.mIndex), mIndexHandle(other.mIndexHandle)
{
	this->Define(other.mIndexHandle2);
}


void iggPropertyHandle::Define(const iggPropertyHandle *ih)
{
	if(mName.IsEmpty())
	{
		IBUG_FATAL("Property name cannot be empty");
	}

	if(ih != 0)
	{
		if(ih->Variable()->GetType() != iType::Int)
		{
			IBUG_FATAL("Incorrectly configured iggPropertyHandle with a property index");
			mIndexHandle2 = 0;
		}
		mIndexHandle2 = new iggPropertyHandle(*ih); IERROR_CHECK_MEMORY(mIndexHandle2);
	}
	else
	{
		mIndexHandle2 = 0;
	}

	mObject = 0;
	mProperty = 0;
	mFunctionProperty = 0;
	mVariableProperty = 0;
}


iggPropertyHandle::~iggPropertyHandle()
{
	if(mIndexHandle2 != 0) delete mIndexHandle2;
}


int iggPropertyHandle::Index() const
{
	if(mIndexHandle2 == 0)
	{
		if(mIndexHandle == 0)
		{
			return mIndex;
		}
		else
		{
			return mIndexHandle->Index();
		}
	}
	else
	{
		const iPropertyDataVariable<iType::Int> *prop = dynamic_cast<const iPropertyDataVariable<iType::Int>*>(mIndexHandle2->Variable()); IASSERT(prop);
		return prop->GetValue(0);
	}
}


const iProperty* iggPropertyHandle::Property(int mod) const
{
	if(mObject != mObjectHandle->Object(mod))
	{
		mObject = mObjectHandle->Object(mod); IASSERT(mObject);
		mProperty = mObject->GetLocalProperty(mName);
		if(mProperty == 0)
		{
			IBUG_FATAL("Unable to find property <"+mName+"> for object <"+mObject->LongName()+">.");
		}
	}
	return mProperty;
}


const iPropertyFunction* iggPropertyHandle::Function(int mod) const
{
	if(mFunctionProperty != this->Property())
	{
		mFunctionProperty = dynamic_cast<const iPropertyFunction*>(this->Property());
		if(mFunctionProperty == 0)
		{
			IBUG_FATAL("Unable to cast function property <"+mName+"> for object <"+mObject->LongName()+">.");
		}
	}
	return mFunctionProperty;
}


const iPropertyVariable* iggPropertyHandle::Variable(int mod) const
{
	if(mVariableProperty != this->Property(mod))
	{
		mVariableProperty = dynamic_cast<const iPropertyVariable*>(this->Property(mod));
		if(mVariableProperty == 0)
		{
			IBUG_FATAL("Unable to cast variable property <"+mName+"> for object <"+mObject->LongName()+">.");
		}
	}
	return mVariableProperty;
}


const iObject* iggPropertyHandle::Object(int mod) const
{
	return mObjectHandle->Object(mod);
}


//
//  Abstract DataType handle
//
iggDataTypeHandle::iggDataTypeHandle()
{
	List().Add(this);
}


iggDataTypeHandle::~iggDataTypeHandle()
{
	int i = List().Find(this);
	if(i != -1)
	{
		List().iArray<iggDataTypeHandle*>::Remove(i);
	}
#ifdef I_CHECK
	else
	{
		IBUG_ERROR("Deleting an unregistered data handle.");
	}
#endif
}


const iDataType& iggDataTypeHandle::GetActiveDataType() const
{
	return this->GetDataInfo().Type(this->GetActiveDataTypeIndex());
}


void iggDataTypeHandle::SetActiveDataType(const iDataType &type)
{
	int i = this->GetDataInfo().Index(type);
	if(i != -1)
	{
		this->OnActiveDataTypeChanged(i);
	}
}


void iggDataTypeHandle::SetActiveDataTypeIndex(int v)
{
	if(v>=0 && v<this->GetDataInfo().Size())
	{
		this->OnActiveDataTypeChanged(v);
	}
}


iLookupArray<iggDataTypeHandle*>& iggDataTypeHandle::List()
{
	static iLookupArray<iggDataTypeHandle*> list(100);
	return list;
}


void iggDataTypeHandle::AfterFileLoad(const iDataInfo &info)
{
	int i;
	iLookupArray<iggDataTypeHandle*> &list(List());

	//
	//  Forward the request to each handle
	//
	for(i=0; i<list.Size(); i++)
	{
		list[i]->AfterFileLoadBody(info);
	}
}

#endif
