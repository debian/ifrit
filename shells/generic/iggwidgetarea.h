/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IGGWIDGETAREA_H
#define IGGWIDGETAREA_H


#include "iggwidget.h"


#include "icolor.h"
#include "iimage.h"

class iHistogram;

class iggFrameHistogramControls;

class ibgWidgetDrawAreaSubject;


class iggWidgetDrawArea : public iggWidget
{

public:

	void DrawBackground();
	void DrawForeground();

	virtual void OnMousePress(int /*x*/, int /*y*/, int /*b*/){}
	virtual void OnMouseRelease(int /*x*/, int /*y*/, int /*b*/){}
	virtual void OnMouseMove(int /*x*/, int /*y*/, int /*b*/){}

protected:

	iggWidgetDrawArea(iggFrame *parent, bool interactive);

	virtual void DrawBackgroundBody() = 0;
	virtual void DrawForegroundBody() = 0;

	virtual void UpdateWidgetBody();

	ibgWidgetDrawAreaSubject *mSubject;
};


class iggWidgetImageArea : public iggWidgetDrawArea
{

public:

	iggWidgetImageArea(iggFrame *parent);
	iggWidgetImageArea(const iImage& image, bool scaled, iggFrame *parent);

	void SetImage(const iImage& image, bool scaled);
	void SetFixedSize(int w, int h);

	const iImage& GetImage() const { return mImage; }

protected:

	virtual void DrawBackgroundBody();
	virtual void DrawForegroundBody(){}

	iImage mImage;
	bool mScaled;
};


//
//  An interactive drawing area with the histogram plotted as a background
//
class iggWidgetHistogramArea : public iggWidgetDrawArea
{

public:

	iggWidgetHistogramArea(iggFrameHistogramControls *controls, iggFrame *parent);

protected:

	void DrawHistogram(const iHistogram& h, const iColor &color = iColor(160,160,160));
	void DrawHistogram(const iHistogram& h, float range[2], const iColor &color = iColor(160,160,160));

	iggFrameHistogramControls *mControls;
};

#endif  // IGGWIDGETAREA_H

