/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggdialogparallelcontroller.h"


#include "ibuffer.h"
#include "ierror.h"
#include "imath.h"
#include "iparallelmanager.h"

#include "iggframe.h"
#include "iggframedisplay.h"
#include "iggimagefactory.h"
#include "iggmainwindow.h"
#include "iggshell.h"
#include "iggwidgetarea.h"
#include "iggwidgetmisc.h"

#include "ibgwidgetareasubject.h"
#include "ibgwidgetselectionboxsubject.h"

#include "iggsubjectfactory.h"

//
//  Templates
//
#include "ibuffer.tlh"


namespace iggDialogParallelController_Private
{
	//
	//  Display the # of processors
	//
	class NumProcs : public iggWidget
	{

	public:

		NumProcs(iggFrame *parent) : iggWidget(parent)
		{
			mSubject = iggSubjectFactory::CreateWidgetSpinBoxSubject(this,1,1,"Number of Processors",1);
			this->SetBaloonHelp("Set the number of processors");
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			iParallelManager *p = this->GetShell()->GetParallelManager();
			mSubject->SetRange(p->GetMinNumberOfProcessors(),p->GetMaxNumberOfProcessors());
			mSubject->SetValue(p->GetNumberOfProcessors());
		}

		virtual void OnInt1Body(int n)
		{
			this->GetShell()->GetParallelManager()->SetNumberOfProcessors(n);
		}

		ibgWidgetSpinBoxSubject *mSubject;
	};

	//
	// Grapical display
	//
	class PerformanceDisplay : public iggWidgetDrawArea
	{

	public:

		PerformanceDisplay(iggFrameNumberDisplay *wc, iggFrameNumberDisplay *li, iggFrameNumberDisplay *st, iggFrameNumberDisplay *sf, iggFrameNumberDisplay *ft, iggFrameNumberDisplay *ff, iggFrame *parent) : iggWidgetDrawArea(parent,false)
		{
			mWallClockTimeDisplay = wc;
			mLoadImbalanceDisplay = li;
			mSlowTimeDisplay = st;
			mSlowFracDisplay = sf;
			mFastTimeDisplay = ft;
			mFastFracDisplay = ff;

			mNeedsBaloonHelp = false;
		}

	protected:

		virtual void DrawBackgroundBody()
		{
		}

		virtual void DrawForegroundBody()
		{
			static iBuffer<double> t;
			int i, j, n, k1, k2, iMin, iMax;

			//
			//  Cache for speed
			//
			iParallelManager *man = this->GetShell()->GetParallelManager();
			n = man->GetNumberOfProcessors();
			double tWall = man->GetWallClockExecutionTime();
			for(i=0; i<n; i++) t[i] = man->GetProcessorExecutionTime(i);

			mWallClockTimeDisplay->Display(tWall,"%.1lg");

			double tMax, tMin, imb = 0.0;
			tMax = tMin = t[0];
			iMin = iMax = 0;
			for(i=0; i<n; i++)
			{
				imb += t[i];
				if(t[i] < tMin) { tMin = t[i]; iMin = i; }
				if(t[i] > tMax) { tMax = t[i]; iMax = i; }
			}

			if(tWall > 0.0) imb = 1.0 - imb/(n*tWall); else imb = 0.0;
			mLoadImbalanceDisplay->Display(iMath::Round2Int(100.0*imb));

			if(iMin == iMax) iMin = iMax = -1;

			mFastTimeDisplay->Display(tMin,"%.1lg");
			mFastFracDisplay->Display(iMath::Round2Int(100.0*tMin/tWall));

			mSlowTimeDisplay->Display(tMax,"%.1lg");
			mSlowFracDisplay->Display(iMath::Round2Int(100.0*tMax/tWall));

			int w = mSubject->Width();
			int h = mSubject->Height();

			iColor c;

			for(i=0; i<n; i++)
			{
				k1 = iMath::Round2Int((w*(i+0.2))/n);
				k2 = iMath::Round2Int((w*(i+0.8))/n);
				if(i == iMin)
				{
					c = iColor(0,255,0);
				}
				else if(i == iMax)
				{
					c = iColor(255,0,0);
				}
				else
				{
					c = iColor(0,0,255);
				}
				j = iMath::Round2Int(h*t[i]/tWall);
				mSubject->DrawRectangle(k1,0,k2,j,c,c);
			}
		}

		iggFrameNumberDisplay *mWallClockTimeDisplay, *mLoadImbalanceDisplay;
		iggFrameNumberDisplay *mSlowTimeDisplay, *mSlowFracDisplay;
		iggFrameNumberDisplay *mFastTimeDisplay, *mFastFracDisplay;
	};
};


using namespace iggDialogParallelController_Private;


iggDialogParallelController::iggDialogParallelController(iggMainWindow *mw) : iggDialog(mw,0U,iggImageFactory::FindIcon("parallel.png"),"Parallel Controller","sr.gg.dpc",3)
{
	if(this->ImmediateConstruction()) this->CompleteConstruction();
}


void iggDialogParallelController::CompleteConstructionBody()
{
	iggFrame *q = new iggFrame(mFrame,2);
	q->AddLine(new NumProcs(q));
	q->SetColStretch(0,0);
	q->SetColStretch(1,10);
	
	iggFrame *b1 = new iggFrame("",mFrame,4);
	iggFrameNumberDisplay *wc = new iggFrameNumberDisplay("",b1);
    iggFrameNumberDisplay *ib = new iggFrameNumberDisplay("sec;    Load imbalance",b1);
	b1->AddLine(new iggWidgetTextLabel("Wall clock time",b1),wc,ib,new iggWidgetTextLabel("%",b1));

	iggFrame *b2 = new iggFrame("",mFrame,4);
	iggFrameNumberDisplay *st = new iggFrameNumberDisplay("",b2);
	iggFrameNumberDisplay *sf = new iggFrameNumberDisplay("sec;   ",b2);
	b2->AddLine(new iggWidgetTextLabel("Slowest processor",b2),st,sf,new iggWidgetTextLabel("% wall clock time",b2));
	iggFrameNumberDisplay *ft = new iggFrameNumberDisplay("",b2);
	iggFrameNumberDisplay *ff = new iggFrameNumberDisplay("sec;   ",b2);
	b2->AddLine(new iggWidgetTextLabel("Fastest processor",b2),ft,ff,new iggWidgetTextLabel("% wall clock time",b2));

	mDisplay = new PerformanceDisplay(wc,ib,st,sf,ft,ff,mFrame);

	mFrame->AddLine(q,2);
	mFrame->AddLine(mDisplay,3);
	mFrame->SetRowStretch(1,10);
	mFrame->AddLine(b1,2);	
	mFrame->AddLine(b2,2);	
	mFrame->SetColStretch(1,1);
	mFrame->SetColStretch(2,10);

	this->ResizeContents(200,300);
}


void iggDialogParallelController::UpdateDisplay()
{
	this->CompleteConstruction();
	iDynamicCast<PerformanceDisplay,iggWidget>(INFO,mDisplay)->UpdateWidget();
}

#endif
