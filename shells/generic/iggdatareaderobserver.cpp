/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggdatareaderobserver.h"


#include "ierror.h"

#include "iggdialogloadfile.h"
#include "igghandle.h"
#include "iggmainwindow.h"
#include "iggshell.h"

#include "ibgmenuwindowsubject.h"


using namespace iParameter;


//
//  iggDataReaderObserver class
//
iggDataReaderObserver::iggDataReaderObserver(iggShell *s) : iDataReaderObserver(s)
{
	mMainWindow = 0;
	mDoNotOfferToReload = mStarted = false;
	mShell = s; IASSERT(s);
}


bool iggDataReaderObserver::AllowReload()
{
	if(mMainWindow == 0)
	{
		mMainWindow = mShell->GetMainWindow();
	}

	if(mMainWindow != 0)
	{
		if(!mDoNotOfferToReload)
		{
			int ret = iRequiredCast<iggShell>(INFO,this->GetShell())->PopupWindow("This control only takes effect when the data set is reloaded.",MessageType::Information,"Reload now","Later","Do not ask again");
			if(ret == 2) mDoNotOfferToReload = true;
			return (ret == 0);
		}
	}

	return false;
}


void iggDataReaderObserver::OnSetStart()
{
	if(mMainWindow == 0)
	{
		mMainWindow = mShell->GetMainWindow();
	}

	IASSERT(mMainWindow);

	mInfo.Clear();

	if(mMainWindow->GetSubject()->IsVisible())
	{
		mMainWindow->GetDialogLoadFile()->Start();
		mStarted = true; // just in case the MainWindow visibilty changes while we are loading file
	}
}


void iggDataReaderObserver::OnFileStart(const iString &fname, const iDataType &type)
{
	IASSERT(mMainWindow);

	if(mStarted)
	{
		mMainWindow->GetDialogLoadFile()->OnFileStart(fname,type);
	}
}


void iggDataReaderObserver::OnFileFinish(const iString &fname, const iDataInfo &info)
{
	IASSERT(mMainWindow);

	if(mStarted)
	{
		mMainWindow->GetDialogLoadFile()->OnFileFinish(fname,info);
	}

	mInfo += info;
}


void iggDataReaderObserver::OnSetFinish(bool error)
{
	IASSERT(mMainWindow);

	if(mStarted)
	{
		mStarted = false;
		mMainWindow->GetDialogLoadFile()->Finish();
	}

	if(!error)
	{
		iggDataTypeHandle::AfterFileLoad(mInfo);
	}
}

#endif
