/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggwidgetotherbutton.h"


#include "ierror.h"
#include "ihelpfactory.h"
#include "ishell.h"
#include "iviewsubject.h"

#include "iggdialog.h"
#include "iggframe.h"
#include "iggimagefactory.h"
#include "iggmainwindow.h"
#include "igghandle.h"
#include "iggpageobject.h"
#include "iggwidgetpropertycontrol.h"
#include "iggwidgetpropertycontrolslider.h"

#include "ibgwidgetbuttonsubject.h"

#include "iggsubjectfactory.h"

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"
#include "iggwidgetpropertycontrol.tlh"
#include "iggwidgetpropertycontrolslider.tlh"


using namespace iParameter;


//
//  Show button
//
iggWidgetShowButton::iggWidgetShowButton(iggPageObject *page, iggFrame *parent) : iggWidget(parent)
{
	IASSERT(page);
	mPage = page;

	mSubject = iggSubjectFactory::CreateWidgetButtonSubject(this,ButtonType::CheckBox,"Show",1,25);

	iString tt = "Shows/hides the "+mPage->GetObjectHandle()->Object()->LongName()+" object. Press Shift+F1 for more help.";
	const iHelpDataBuffer &buf = iHelpFactory::FindData("or.-vo.vis",iHelpFactory::_ObjectReference);
	this->SetBaloonHelp(tt,buf.GetHTML());
}


void iggWidgetShowButton::OnVoid1Body()
{
	mPage->Show(mSubject->IsDown());
}


//
//  query the control module about the proper state for this widget
//
void iggWidgetShowButton::UpdateWidgetBody()
{
	const iViewSubject *sub = iDynamicCast<const iViewSubject>(INFO,mPage->GetSubjectHandle()->Object());

	this->Enable(sub->IsThereData());
	mSubject->SetDown(sub->Visible.GetValue(0));
}


//
//  Dialog launch button
//
iggWidgetLaunchButton::iggWidgetLaunchButton(iggDialog *dialog, const iString &text, iggFrame *parent, bool destructive) : iggWidget(parent)
{
	mSubject = iggSubjectFactory::CreateWidgetButtonSubject(this,ButtonType::PushButton,text,1);

	mDialog = dialog; if(dialog == 0) this->Enable(false);
	mWindow = 0;
	this->Define(text,destructive);
}


iggWidgetLaunchButton::iggWidgetLaunchButton(iggMenuWindow *window, const iString &text, iggFrame *parent, bool destructive) : iggWidget(parent)
{
	mSubject = iggSubjectFactory::CreateWidgetButtonSubject(this,ButtonType::PushButton,text,1);

	mDialog = 0;
	mWindow = window; if(window == 0) this->Enable(false);
	this->Define(text,destructive);
}


void iggWidgetLaunchButton::Define(const iString &text, bool destructive)
{
	mDestructive = destructive;
	this->SetBaloonHelp("Opens a dialog window");
}


iggWidgetLaunchButton::~iggWidgetLaunchButton()
{
	if(mDestructive)
	{
		if(mDialog != 0) delete mDialog;
		if(mWindow != 0) delete mWindow;
	}
}


void iggWidgetLaunchButton::UpdateWidgetBody()
{
	//
	//  Nothing to do here
	//
}


void iggWidgetLaunchButton::OnVoid1Body()
{
	if(mDialog != 0) mDialog->Show(true);
	if(mWindow != 0) mWindow->Show(true);
}


//
//  Lock for all instances check box
//
iggWidgetAllInstancesCheckBox::iggWidgetAllInstancesCheckBox(iggFrame *parent) : iggWidget(parent)
{
	mSubject = iggSubjectFactory::CreateWidgetButtonSubject(this,ButtonType::CheckBox,"Lock for all instances",1);
	
	this->SetBaloonHelp("Toggles whether sliders affects all instances or the current one only.","Properties of all instances can be changed together if this checkbox is checked. This is convenient when a large number of instances are present that are used in a similar fashion.");
}


void iggWidgetAllInstancesCheckBox::UpdateWidgetBody()
{
	//
	//  Nothing to update
	//
}


void iggWidgetAllInstancesCheckBox::OnVoid1Body()
{
	int i, ef;

	ef = ExecuteFlag::Default;
	if(mSubject->IsDown())
	{
		//
		//  Unset the index flag
		//
		ef &= (~ExecuteFlag::Index::Mask);
		ef |= ExecuteFlag::Index::All;
	}

	iggWidgetPropertyControlBase *kh;
	for(i=0; i<mDependents.Size(); i++)
	{
		kh = dynamic_cast<iggWidgetPropertyControlBase*>(mDependents[i]);
		if(kh != 0) kh->SetExecuteFlags(ef);
	}
}


//
//  iggWidgetSimpleButton class
//
iggWidgetSimpleButton::iggWidgetSimpleButton(const iString &text, iggFrame *parent, bool tool, int minsize) : iggWidget(parent)
{
	int type = tool ? ButtonType::ToolButton : ButtonType::PushButton;
	mSubject = iggSubjectFactory::CreateWidgetButtonSubject(this,type,text,1,minsize);
}


void iggWidgetSimpleButton::UpdateWidgetBody()
{
}


void iggWidgetSimpleButton::OnVoid1Body()
{
	this->Execute();
}


//
//  iggWidgetDialogCloseButton class
//
iggWidgetDialogCloseButton::iggWidgetDialogCloseButton(iggDialog *dialog, iggFrame *parent, const char *text) : iggWidgetSimpleButton(text,parent,false)
{
	mDialog = dialog;

	this->SetBaloonHelp("Close dialog","This button closes the current dialog window.");
}


void iggWidgetDialogCloseButton::Execute()
{
	if(mDialog != 0) mDialog->Close();
}

#endif
