/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggwidgetarea.h"


#include "ierror.h"
#include "ihistogram.h"
#include "istretch.h"

#include "iggframehistogramcontrols.h"
#include "iggmainwindow.h"

#include "ibgwidgetareasubject.h"

#include "iggsubjectfactory.h"

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"


//
//  DrawArea widget - abstract base class for all drawing widgets
//
iggWidgetDrawArea::iggWidgetDrawArea(iggFrame *parent, bool interactive) : iggWidget(parent)
{
	mSubject = iggSubjectFactory::CreateWidgetDrawAreaSubject(this,interactive);
}


void iggWidgetDrawArea::UpdateWidgetBody()
{
	mSubject->RequestPainting();
}


void iggWidgetDrawArea::DrawBackground()
{
	this->DrawBackgroundBody();
}


void iggWidgetDrawArea::DrawForeground()
{
	this->DrawForegroundBody();
}


//
//  ImageArea widget
//
iggWidgetImageArea::iggWidgetImageArea(iggFrame *parent) : iggWidgetDrawArea(parent,false)
{
	mNeedsBaloonHelp = false;
}


iggWidgetImageArea::iggWidgetImageArea(const iImage &image, bool scaled, iggFrame *parent) : iggWidgetDrawArea(parent,false)
{
	mNeedsBaloonHelp = false;

	this->SetImage(image,scaled);
}


void iggWidgetImageArea::DrawBackgroundBody()
{
	if(mScaled)
	{
		mSubject->DrawImage(0,0,mSubject->Width()-1,mSubject->Height()-1,mImage,iImage::_Both);
	}
	else
	{
		mSubject->DrawImage(0,0,mSubject->Width()-1,mSubject->Height()-1,mImage,iImage::_FitMin);
	}
}


void iggWidgetImageArea::SetImage(const iImage &image, bool scaled)
{
	mImage = image;
	mScaled = scaled;
}


void iggWidgetImageArea::SetFixedSize(int w, int h)
{
	mSubject->SetFixedSize(w,h);
}


//
//  HistogramArea widget (with a histogram as a background)
//
iggWidgetHistogramArea::iggWidgetHistogramArea(iggFrameHistogramControls *controls, iggFrame *parent) : iggWidgetDrawArea(parent,true)
{
	mControls = controls;  // controls can be 0

	if(mControls != 0)
	{
		mControls->Attach(this);
	}
}


void iggWidgetHistogramArea::DrawHistogram(const iHistogram &h, const iColor &color)
{
	float range[2] = { 0, 1 };

	this->DrawHistogram(h,range,color);
}


void iggWidgetHistogramArea::DrawHistogram(const iHistogram &h, float range[2], const iColor &color)
{
	static float topOffset = 0.95f;
	static const iColor tickColor = iColor(60,60,60);    // tick mark color
	static int tickWidth = 3;

	if(h.NumBins() < 1)
	{
		return;
	}

	int i;
	float ymax = h.Value(0);
	for(i=1; i<h.NumBins(); i++)
	{
		if(ymax < h.Value(i)) ymax = h.Value(i);
	}
	if(ymax < 1.0e-35) return;

	bool inlog = false;
	if(mControls != 0)
	{
		inlog = mControls->GetYLogScale();
		mControls->DisplayRange(h.GetMin(),h.GetMax());
	}
	float xmin = iStretch::Apply(h.GetMin(),h.StretchId(),false);
	float xmax = iStretch::Apply(h.GetMax(),h.StretchId(),true);
	if(xmax-xmin < 1.0e-35) return;

	int ixmin = iMath::Round2Int(range[0]*(mSubject->Width()-1));
	int iymin = 0;
	int ixmax = iMath::Round2Int(range[1]*(mSubject->Width()-1));
	int iymax = mSubject->Height() - 1;

	float xnorm = float(ixmax-ixmin);
	float ynorm = float(1.0/log(1+ymax));
	float ys,yoffset = topOffset*iymax;
	int iytick = iMath::Round2Int(0.05*iymax);

	int iy1,ix1,ix2 = ixmin;
	for(i=0; i<h.NumBins(); i++)
	{
		ix1 = ix2;
		ix2 = ixmin + iMath::Round2Int(xnorm*(iStretch::Apply(h.GetBinMax(i),h.StretchId(),true)-xmin)/(xmax-xmin));
		if(ix2 > ix1)
		{
			if(inlog)
			{
				ys = ynorm*float(log(1+h.Value(i)));
				if(ys < 0.0) ys = 0.0;
			}
			else
			{
				ys = h.Value(i)/ymax;
			}
			iy1 = iMath::Round2Int(yoffset*ys);
			if(ix2 > ix1+1)
			{
				mSubject->DrawRectangle(ix1,0,ix2-1,iy1,color,color);
			}
			else
			{
				mSubject->DrawLine(ix1,0,ix1,iy1,color);
			}
			if(h.NumBins() < 20)
			{
				mSubject->DrawLine((ix1+ix2)/2,0,(ix1+ix2)/2,iytick,tickColor,tickWidth);
			}
		}
	}
}

#endif
