/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggdialogfilesetexplorer.h"


#include "idata.h"
#include "idatareader.h"
#include "idirectory.h"
#include "ifile.h"
#include "ihelpfactory.h"
#include "imonitor.h"
#include "iviewmodule.h"

#include "iggframe.h"
#include "iggimagefactory.h"
#include "iggmainwindow.h"
#include "iggshell.h"
#include "iggwidgetotherbutton.h"
#include "iggwidgettext.h"

#include "ibgwidgetbuttonsubject.h"
#include "ibgwidgettexteditorsubject.h"

#include "iggsubjectfactory.h"

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"


using namespace iParameter;


namespace iggDialogFileSetExplorer_Private
{
	class View : public iggWidgetListView
	{

	public:

		View(iggDialogFileSetExplorer *dialog, iggFrame *parent) : iggWidgetListView(false,parent)
		{
			mDialog = dialog;
			mCurrentEntry = -1;
			mBlockUpdates = false;
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			static const iColor current = iColor(0,0,255);

			if(mBlockUpdates) return;

			iDataReader *dr = this->GetShell()->GetViewModule()->GetReader();

			if(dr->IsSet())
			{
				int i, j, n;
				if(!mDir.Open(dr->GetLastFileSetName()))
				{
					this->GetShell()->PopupWindow(mDialog,"Unable to open directory containing "+dr->GetLastFileSetName()+" file.",MessageType::Error);
					this->Clear();
					mDialog->MustBeClosed();
					return;
				}

				bool redo = (mCurrentEntry==-1 || mDir.GetFile(mCurrentEntry,true)!=dr->GetLastFileSetName());
				n = mDir.GetNumberOfFiles();
				iString ws;
				for(i=j=0; !redo && i<n; i++)
				{
					if(dr->IsAnotherSetLeader(mDir.GetFile(i,true)))
					{
						ws = this->GetItem(j);
						ws.ReduceWhiteSpace();
						if(mDir.GetFile(i,false) != ws)
						{
							redo = true;
						}
						j++;
					}
				}

				if(redo)
				{
					mCurrentEntry = -1;
					this->Clear();
					for(i=j=0; i<n; i++)
					{
						if(dr->IsAnotherSetLeader(mDir.GetFile(i,true)))
						{
							if(mDir.GetFile(i,true) == dr->GetLastFileSetName())
							{
								mCurrentEntry = j;
								//mSubject->SetBoldText(true);
								this->InsertItem(mDir.GetFile(i,false),current);
								//mSubject->SetBoldText(false);
							}
							else
							{
								this->InsertItem(mDir.GetFile(i,false));
							}
							j++;
						}
					}
					if(mCurrentEntry >= 0)
					{
						this->Select(mCurrentEntry,mCurrentEntry);
					}
				}
			}
			else this->Clear();

			this->iggWidgetListView::UpdateWidgetBody();
		}

		virtual void OnReturnPressed()
		{
			int i, imin = mSelectedRange[0], imax = mSelectedRange[1];
			iString ws;

			mBlockUpdates = true;
			for(i=imin; i<=imax; i++)
			{
				ws = mDir.GetDirectoryPrefix() + mSubject->GetLine(i);
				ws.ReduceWhiteSpace(); // needed for some bizarre reason
				mDialog->LoadData(ws);
			}
			mBlockUpdates = false;
			this->UpdateWidget();
		}

		iggDialogFileSetExplorer *mDialog;
		iDirectory mDir;
		int mCurrentEntry;
		bool mBlockUpdates;
	};

	//
	//  Load all/find match
	//
	class CheckBox : public iggWidget
	{

	public:

		CheckBox(bool all, iggDialogFileSetExplorer *dialog, iggFrame *parent) : iggWidget(parent)
		{
			mIsAll = all;
			mDialog = dialog;
			mButton = iggSubjectFactory::CreateWidgetButtonSubject(this,ButtonType::CheckBox,all?"Load data for all windows":"Match similar names",1);

			if(all)
			{
				this->SetBaloonHelp("Load data for all visualization windows");
			}
			else
			{
				this->SetBaloonHelp("Load similarly named file if there is no exact match");
			}
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			mButton->SetDown(mIsAll?mDialog->GetLoadAll():mDialog->GetMatchSimilarNames());
		}

		virtual void OnVoid1Body()
		{
			if(mIsAll)
			{
				mDialog->SetLoadAll(mButton->IsDown());
			}
			else
			{
				mDialog->SetMatchSimilarNames(mButton->IsDown());
			}
		}

		bool mIsAll;
		ibgWidgetButtonSubject *mButton;
		iggDialogFileSetExplorer *mDialog;
	};


	class RefreshButton : public iggWidgetSimpleButton
	{

	public:

		RefreshButton(iggDialogFileSetExplorer *dialog, iggFrame *parent) : iggWidgetSimpleButton("Refresh",parent)
		{
			mDialog = dialog;
		
			this->SetBaloonHelp("Refresh file list","Refresh the list of files in case they changed on disk.");
		}

	protected:

		virtual void Execute()
		{
			mDialog->UpdateDialog();
		}

		iggDialogFileSetExplorer *mDialog;
	};
};


using namespace iggDialogFileSetExplorer_Private;


iggDialogFileSetExplorer::iggDialogFileSetExplorer(iggMainWindow *parent) : iggDialog(parent,0U,iggImageFactory::FindIcon("setexp.png"),"File Set Explorer","sr.gg.dfe",3)
{
	mMustBeClosed = false;
	mLoadAll = mMatchSimilarNames = true;

	if(this->ImmediateConstruction()) this->CompleteConstruction();
}


void iggDialogFileSetExplorer::CompleteConstructionBody()
{
	mView = new View(this,mFrame);
	mFrame->AddLine(mView,3);
	mFrame->AddLine(new CheckBox(true,this,mFrame));
	mFrame->AddLine(new CheckBox(false,this,mFrame),(iggWidget *)0,new RefreshButton(this,mFrame));

	this->ResizeContents(300,400);
}


void iggDialogFileSetExplorer::UpdateDialog()
{
	if(!this->GetShell()->GetViewModule()->GetReader()->IsSet())
	{
		this->Show(false);
	}
	else
	{
		iggDialog::UpdateDialog();
	}
}


void iggDialogFileSetExplorer::ShowBody(bool s)
{
	mMustBeClosed = false;
	iggDialog::ShowBody(s);
	if(mMustBeClosed) iggDialog::ShowBody(false);
}


void iggDialogFileSetExplorer::MustBeClosed()
{
	mMustBeClosed = true;
}


void iggDialogFileSetExplorer::LoadData(const iString &fname)
{
	//
	//  Load current first
	//
	iDataReader *dr = this->GetShell()->GetViewModule()->GetReader();

	iMonitor h(this);
	dr->LoadFile(dr->GetFileSetDataType(),fname);

	if(mLoadAll && !h.IsStopped())
	{
		int i, n = this->GetShell()->GetNumberOfViewModules();
		int rec = dr->GetRecordNumber();
		for(i=0; i<n; i++) if(i != this->GetShell()->GetActiveViewModuleIndex())
		{
			iDataReader *odr = this->GetShell()->GetViewModule(i)->GetReader();
			if(dr == odr) continue;

			iString ofname = odr->GetFileSetName(rec);
			if(!ofname.IsEmpty())
			{
				iString fn;

				if(iFile::IsReadable(ofname))
				{
					fn = ofname;
				}
				else if(mMatchSimilarNames)
				{
					int i;
					iString s;
					for(i=1; i<10; i++)
					{
						s = odr->GetFileSetName(rec+i);
						if(iFile::IsReadable(s))
						{
							fn = s;
							break;
						}
						if(rec > i)
						{
							s = odr->GetFileSetName(rec-i);
							if(iFile::IsReadable(s))
							{
								fn = s;
								break;
							}
						}
					}
				}

				if(fn.IsEmpty())
				{
					this->OutputText(MessageType::Warning,"File "+ofname+" is mising or not readable.");
				}
				else
				{
					odr->LoadFile(odr->GetFileSetDataType(),fn);
				}
			}
		}
	}

	this->Render(mLoadAll?ViewModuleSelection::All:ViewModuleSelection::One);
}

#endif
