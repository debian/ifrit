/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggframeposition.h"


#include "ierror.h"
#include "imarker.h"
#include "iposition.h"
#include "iviewsubject.h"
#include "iviewmodule.h"

#include "iggframedoublebutton.h"
#include "igghandle.h"
#include "iggshell.h"
#include "iggwidgetpropertycontrollineedit.h"
#include "iggwidgetpropertycontrolselectionbox.h"
#include "iggwidgetpropertycontrolslider.h"
#include "iggwidgetotherbutton.h"

#include "iggsubjectfactory.h"

#include "ibgwidgetselectionboxsubject.h"


using namespace iParameter;


namespace iggFramePosition_Private
{
	class MoveFocalPointFrame : public iggFrameMoveFocalPointButton
	{
		
	public:
		
		MoveFocalPointFrame(const iggPropertyHandle& ph, iggFrame *parent) : iggFrameMoveFocalPointButton(parent,false,true), mHandle(ph)
		{
		}

		virtual iVector3D GetPosition() const
		{
			return iDynamicCast<const iPropertyDataVariable<iType::Vector> >(INFO,mHandle.Variable())->GetValue(0);
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			this->Enable(mHandle.Index() >= 0);

			this->iggFrameMoveFocalPointButton::UpdateWidgetBody();
		}

		const iggPropertyHandle mHandle;
	};

	class MoveToWidget : public iggWidget
	{

	public:

		MoveToWidget(bool withRender, const iggPropertyHandle& ph, iggFrame *parent) : iggWidget(parent), mHandle(ph)
		{
			mSubject = iggSubjectFactory::CreateWidgetComboBoxSubject(this,"");

			mWithRender = withRender;

			this->SetBaloonHelp("Move position to a special location. Press Shift+F1 for more help.","Using the drop-down list, the current object position can be moved to a special location in th scene, such as a focal point, the last picked point, a specific marker, etc.");
		}

		virtual void OnInt1Body(int val)
		{
			const iViewSubject *sub = iDynamicCast<const iViewSubject>(INFO,mHandle.Object());

			iPosition pos(sub->GetViewModule());
			pos.SetToSpecialLocation(val-1+SpecialLocation::FIRST__);

			iDynamicCast<const iPropertyDataVariable<iType::Vector> >(INFO,mHandle.Variable())->SetValue(0,pos.OpenGLValue());

			mSubject->SetValue(0);
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			int i;

			mSubject->Clear();

			if(mHandle.Index() >= 0)
			{
				this->Enable(true);

				mSubject->InsertItem("Move to...");
				mSubject->InsertItem("  Picked point");
				mSubject->InsertItem("  Focal point");
				mSubject->InsertItem("  Box Center");

				for(i=0; i<this->GetShell()->GetViewModule()->GetNumberOfMarkers(); i++)
				{
					mSubject->InsertItem("  Marker #"+iString::FromNumber(i+1));
				}

				mSubject->SetValue(0);
			}
			else
			{
				this->Enable(false);
			}
		}

		bool mWithRender;
		const iggPropertyHandle mHandle;
		ibgWidgetComboBoxSubject *mSubject;
	};
};


using namespace iggFramePosition_Private;


iggFramePosition::iggFramePosition(const iString &title, const iggPropertyHandle& ph, iggFrame *parent, bool withFocalPointMove, bool withRender) : iggFrame(title,parent,1)
{
#ifndef I_NO_CHECK
	const iPropertyDataVariable<iType::Vector> *prop = dynamic_cast<const iPropertyDataVariable<iType::Vector>*>(ph.Variable());
	if(prop == 0)
	{
		IBUG_FATAL("iggFramePosition is configured incorrectly.");
	}
#endif

	this->AddLine(new iggWidgetPropertyControlPositionSlider(8,"%b%+X",ph,0,this,withRender?RenderMode::Delayed:RenderMode::DoNotRender));
	this->AddLine(new iggWidgetPropertyControlPositionSlider(8,"%b%+Y",ph,1,this,withRender?RenderMode::Delayed:RenderMode::DoNotRender));
	this->AddLine(new iggWidgetPropertyControlPositionSlider(8,"%b%+Z",ph,2,this,withRender?RenderMode::Delayed:RenderMode::DoNotRender));

	this->AddLine(new MoveToWidget(withRender,ph,this));

	if(withFocalPointMove)
	{
		this->AddLine(new MoveFocalPointFrame(ph,this));
	}
}

#endif
