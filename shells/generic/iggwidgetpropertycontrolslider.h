/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IGGWIDGETPROPERTYCONTROLSLIDER_H
#define IGGWIDGETPROPERTYCONTROLSLIDER_H

//
//  Non-abstract classes defined in this file
//
//  iggWidgetPropertyControlIntSlider;
//  iggWidgetPropertyControlLargeIntSlider;
//  iggWidgetPropertyControlFloatSlider;
//  iggWidgetPropertyControlDoubleSlider;
//  iggWidgetPropertyControlRelativeFloatSlider;
//  iggWidgetPropertyControlPositionSlider;
//  iggWidgetPropertyControlSizeSlider;
//  iggWidgetPropertyControlVariableLimitsSlider;
//
#include "iggwidgetpropertycontrol.h"


template<class T> class iLookupArray;

class iggDataTypeProvider;
class iggObjectHandle;
class iggPropertyHandle;
class iggWidgetRenderModeButton;

class ibgWidgetEntrySubject;


//
//  Main termplate class
//
template<iType::TypeId id>
class iggWidgetPropertyControlSlider : public iggWidgetPropertyControl<id>
{

public:

	typedef typename iType::Traits<id>::Type			ArgT;
	typedef typename iType::Traits<id>::ContainerType	ValT;

	iggWidgetPropertyControlSlider(int numdig, const iString &label, const iggPropertyHandle& ph, iggFrame *parent, int rm);
	virtual ~iggWidgetPropertyControlSlider();

	void SetRange(int smin, int smax, int pagestep = 0);
	void SetStretch(int label, int slider, int number = 0);
	void SetNumberOfTicks(int n);

	virtual void Enable(bool s);

protected:

	virtual void QueryValue(ValT &val) const;
	virtual void UpdateValue(ArgT val);

	virtual void ConvertToSliderInt(ArgT val, int &sint) const = 0;
	virtual void ConvertFromSliderInt(int sint, ValT &val) const = 0;
	virtual void OnEdge(int edge);

	virtual void OnInt1Body(int);
	virtual void OnVoid1Body();
	virtual void OnVoid2Body();

	iggWidgetRenderModeButton *mRenderButton;
	ibgWidgetEntrySubject *mSubject;
	bool mIsTextValue;
};


template<iType::TypeId id>
class iggWidgetPropertyControlGenericNumberSlider : public iggWidgetPropertyControlSlider<id>
{

public:

	typedef typename iType::Traits<id>::Type			ArgT;
	typedef typename iType::Traits<id>::ContainerType	ValT;

	iggWidgetPropertyControlGenericNumberSlider(ArgT min, ArgT max, int res, int sid, ArgT tiny, int numdig, bool exdown, bool exup, const iString &label, const iggPropertyHandle& ph, iggFrame *parent, int rm);

protected:

	virtual void ConvertToSliderInt(ArgT val, int &sint) const;
	virtual void ConvertFromSliderInt(int sint, ValT &val) const;
	virtual void OnEdge(int edge);

	const ValT mTiny;
	ValT mMin, mMax;
	int mResolution;
	int mStretchId;
	bool mIsExpandableDown, mIsExpandableUp, mInOnEdgeUpdate;
};


class iggWidgetPropertyControlIntSlider : public iggWidgetPropertyControlSlider<iType::Int>
{

public:

	iggWidgetPropertyControlIntSlider(int min, int max, int numdig, const iString &label, const iggPropertyHandle& ph, iggFrame *parent, int rm = iParameter::RenderMode::Delayed);

protected:

	virtual void ConvertToSliderInt(int val, int &sint) const;
	virtual void ConvertFromSliderInt(int sint, int &val) const;
};


class iggWidgetPropertyControlLargeIntSlider : public iggWidgetPropertyControlIntSlider
{

public:

	iggWidgetPropertyControlLargeIntSlider(int min, int max, int numdig, const iString &label, const iggPropertyHandle& ph, iggFrame *parent, int rm = iParameter::RenderMode::Delayed);

protected:

	virtual void ConvertToSliderInt(int val, int &sint) const;
	virtual void ConvertFromSliderInt(int sint, int &val) const;
};


class iggWidgetPropertyControlFloatSlider : public iggWidgetPropertyControlGenericNumberSlider<iType::Float>
{

public:

	iggWidgetPropertyControlFloatSlider(float min, float max, int res, int sid, int numdig, bool exdown, bool exup, const iString &label, const iggPropertyHandle& ph, iggFrame *parent, int rm = iParameter::RenderMode::Delayed);
};


class iggWidgetPropertyControlDoubleSlider : public iggWidgetPropertyControlGenericNumberSlider<iType::Double>
{

public:

	iggWidgetPropertyControlDoubleSlider(double min, double max, int res, int sid, int numdig, bool exdown, bool exup, const iString &label, const iggPropertyHandle& ph, iggFrame *parent, int rm = iParameter::RenderMode::Delayed);
};


class iggWidgetPropertyControlPositionSlider : public iggWidgetPropertyControlSlider<iType::Double>
{

public:

	iggWidgetPropertyControlPositionSlider(int numdig, const iString &label, const iggPropertyHandle& ph, int dir, iggFrame *parent, int rm = iParameter::RenderMode::Delayed);
	virtual ~iggWidgetPropertyControlPositionSlider();

protected:

	//
	//  Fine-grain customization
	//
	virtual bool SetPropertyValue(const iProperty *prop, int i, double val) const;
	virtual double GetPropertyValue(const iProperty *prop, int i) const;

	virtual void ConvertToSliderInt(double val, int &sint) const;
	virtual void ConvertFromSliderInt(int sint, double &val) const;

	virtual void OnBool1Body(bool);

	int mResolution, mDirection;
	//
	//  Registry functionality
	//
	static iLookupArray<iggWidgetPropertyControlPositionSlider*> &List();
};


class iggWidgetPropertyControlSizeSlider : public iggWidgetPropertyControlGenericNumberSlider<iType::Double>
{

public:

	iggWidgetPropertyControlSizeSlider(double min, int numdig, const iString &label, const iggPropertyHandle& ph, iggFrame *parent, int rm = iParameter::RenderMode::Delayed);
	virtual ~iggWidgetPropertyControlSizeSlider();

protected:

	virtual void ConvertToSliderInt(double val, int &sint) const;
	virtual void ConvertFromSliderInt(int sint, double &val) const;
};


class iggWidgetPropertyControlOpacitySlider : public iggWidgetPropertyControlFloatSlider
{

public:

	iggWidgetPropertyControlOpacitySlider(const iggPropertyHandle& ph, iggFrame *parent, int rm = iParameter::RenderMode::Delayed);
	iggWidgetPropertyControlOpacitySlider(const iString& title, const iggPropertyHandle& ph, iggFrame *parent, int rm = iParameter::RenderMode::Delayed);
};


class iggWidgetPropertyControlVariableLimitsSlider : public iggWidgetPropertyControlFloatSlider
{

public:

	//
	//  mode +-1 for min/max of a range, mode=0 for a plain float value
	//
	iggWidgetPropertyControlVariableLimitsSlider(int mode, int numdig, const iString &label, const iggObjectHandle *dsh, const iggPropertyHandle& ph, iggFrame *parent, int var = 0, int rm = iParameter::RenderMode::Delayed);
	iggWidgetPropertyControlVariableLimitsSlider(int mode, int numdig, const iString &label, const iggObjectHandle *dsh, const iggPropertyHandle& ph, iggFrame *parent, const iggPropertyHandle *vh, const iggPropertyHandle *sh = 0, int rm = iParameter::RenderMode::Delayed);
	virtual ~iggWidgetPropertyControlVariableLimitsSlider();

	static int Method;

protected:

	void Define(int mode, const iggObjectHandle *dsh);

	virtual bool SetPropertyValue(const iProperty *prop, int i, float val) const;
	virtual float GetPropertyValue(const iProperty *prop, int i) const;

	bool PrepareForUpdate(iPair &range);

	virtual void UpdateWidgetBody();
	virtual void OnInt1Body(int);
	virtual void OnVoid1Body();
	virtual void OnVoid2Body();

	bool mRangeSet;
	int mMode, mVar;
	const iggObjectHandle *mDataSubjectHandle;
	const iggPropertyHandle *mVarHandle, *mStretchHandle;
};

#endif  // IGGWIDGETPROPERTYCONTROLSLIDER_H

