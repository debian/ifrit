/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggframepushcontrols.h"


#include "ierror.h"
#include "ivector3D.h"
#include "iviewmodule.h"

#include "iggframeposition.h"
#include "igghandle.h"
#include "iggimagefactory.h"
#include "iggshell.h"
#include "iggmainwindow.h"
#include "iggwidgetpropertycontrol.h"
#include "iggwidgetotherbutton.h"

#include "ibgwidgetbuttonsubject.h"

#include <vtkCamera.h>
#include <vtkMath.h>
#include <vtkRenderer.h>


namespace iggFramePushControls_Private
{
	class PushButton : public iggWidgetSimpleButton
	{

	public:

		PushButton(float step, int dir, const iggPropertyHandle& posph, const iggPropertyHandle& dirph, iggFrame *parent) : iggWidgetSimpleButton("",parent,true), mPositionHandle(posph), mDirectionHandle(dirph)
		{
			mStep = step;
			mNeedsBaloonHelp = false;
			switch(dir)
			{
			case -1:
				{
					mSubject->SetIcon(*iggImageFactory::FindIcon("moveleft.png"));
					break;
				}
			case 1:
				{
					mSubject->SetIcon(*iggImageFactory::FindIcon("moveright.png"));
					break;
				}
			case -2:
				{
					mSubject->SetIcon(*iggImageFactory::FindIcon("moveleft2.png"));
					break;
				}
			case 2:
				{
					mSubject->SetIcon(*iggImageFactory::FindIcon("moveright2.png"));
					break;
				}
			default:
				{
					IBUG_WARN("Invalid direction.");
				}
			}
		}

	protected:

		virtual void Execute()
		{
			const int ind[4][3] = { {0,2,4}, {0,3,4}, {1,2,4}, {1,3,4} };
			int i, j, k;

			iVector3D x = iDynamicCast<const iPropertyDataVariable<iType::Vector> >(INFO,mPositionHandle.Variable())->GetValue(0);
			iVector3D n = iDynamicCast<const iPropertyDataVariable<iType::Vector> >(INFO,mDirectionHandle.Variable())->GetValue(0);

			//
			//  get camera field of view (planes are ordered according to their normals: -X, +X, -Y, ...
			//
			double planes[24];
			double aspect[2];
			vtkCamera *cam = this->GetShell()->GetViewModule()->GetRenderer()->GetActiveCamera();
			this->GetShell()->GetViewModule()->GetRenderer()->GetAspect(aspect);
			cam->GetFrustumPlanes(aspect[0]/aspect[1],planes);
			//
			//  Find the plane that goes through the camera focal plane, and is perpendicular to the camera
			//  direction of projection.
			//
			cam->GetDirectionOfProjection(planes+16);
			double *fp = cam->GetFocalPoint();
			planes[16+3] = -(planes[16+0]*fp[0]+planes[16+1]*fp[1]+planes[16+2]*fp[2]);
			//
			//  Find all intersection points
			//
			double as[9], *a[3], b[4][3], c[3], d = 0.0;
			a[0] = as + 0;
			a[1] = as + 3;
			a[2] = as + 6;
			for(k=0; k<4; k++)
			{
				for(i=0; i<3; i++)
				{
					for(j=0; j<3; j++) a[i][j] = planes[4*ind[k][i]+j];
					b[k][i] = -planes[4*ind[k][i]+3];
				}
				vtkMath::SolveLinearSystem(a,b[k],3);
			}
			//
			//  Center-of-mass
			//
			for(j=0; j<3; j++) c[j] = 0.0;
			for(k=0; k<4; k++)
			{
				for(j=0; j<3; j++) c[j] += b[k][j];
			}
			for(j=0; j<3; j++) c[j] *= 0.25;
			//
			//  Average distance to COM
			//
			for(k=0; k<4; k++)
			{
				d += vtkMath::Distance2BetweenPoints(b[k],c);
			}
			d = 0.5*sqrt(0.25*d);  //  0.5 is because the box is about 1/2 of the view field

			for(i=0; i<3; i++) x[i] += mStep*d*n[i];

			iDynamicCast<const iPropertyDataVariable<iType::Vector> >(INFO,mPositionHandle.Variable())->SetValue(0,x);

			this->Render();
		}

		const iggPropertyHandle mPositionHandle, mDirectionHandle;
		float mStep;
	};

};


using namespace iggFramePushControls_Private;


iggFramePushControls::iggFramePushControls(iggFramePosition *buddy, const iString &title, const iggPropertyHandle& posph, const iggPropertyHandle& dirph, iggFrame *parent) : iggFrame(title,parent,4)
{
	this->AddLine(
        new PushButton(-0.10f,-2,posph,dirph,this),
		new PushButton(-0.01f,-1,posph,dirph,this),
		new PushButton( 0.01f,1,posph,dirph,this),
		new PushButton( 0.10f,2,posph,dirph,this));

	this->AddDependent(buddy);

	mNeedsBaloonHelp = true;
	this->SetBaloonHelp("Pushes the plane along its direction. Press Shift+F1 for more help.","Four push plane buttons move the plane along its direction axis forward (>) or backward (<). The \"long\" push (>>) moves the plane by about 1/10 of the current view size, while a short push (>) moves it by about 1/100 of the view size."); 
}

#endif
