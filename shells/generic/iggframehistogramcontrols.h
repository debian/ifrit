/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A frame that displays controls for histogram widgets
//

#ifndef IGGFRAMEHISTOGRAMCONTROLS_H
#define IGGFRAMEHISTOGRAMCONTROLS_H


#include "iggframe.h"


#include "icolor.h"

class iHistogram;

class iggWidgetHistogramArea;
class iggWidgetNumberLabel;


class iggFrameHistogramControls : public iggFrame
{

public:

	iggFrameHistogramControls(bool withfull, bool withstretch, iggFrame *parent);

	inline int GetStretchId() const { return mStretchId; }
	void SetStretchId(int s);

	inline bool GetYLogScale() const { return mYLogScale; }
	void SetYLogScale(bool s);

	inline bool GetFullResolution() const { return mFullResolution; }
	void SetFullResolution(bool s);

	void DisplayRange(float min, float max);
	void Attach(iggWidgetHistogramArea *area);
	void UpdateArea();

protected:

	int mStretchId;
	bool mYLogScale, mFullResolution;
	iggWidgetNumberLabel *mDisplayMin, *mDisplayMax;
	iggWidgetHistogramArea *mHistogramArea;
};

#endif  // IGGFRAMEHISTOGRAMCONTROLS_H

