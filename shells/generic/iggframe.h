/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A generic (toolkit-independent) containers.
//

#ifndef IGGFRAME_H
#define IGGFRAME_H


#include "iggwidget.h"


#include "iarray.h"
#include "istring.h"

class iImage;

class iggFrameBook;
class iggFrameFlip;
class iggFrameScroll;
class iggRenderWindowObserver;

class ibgFrameSubject;
class ibgFrameBookSubject;
class ibgFrameFlipSubject;
class ibgFrameScrollSubject;
class ibgFrameSplitSubject;
class ibgFrameHelper;


namespace iParameter
{
	//
	//  Book widget modes
	//
	namespace BookTab
	{
		const int TitleOnly = 0;
		const int ImageOnly = 1;
		const int TitleAndImage = 2;
	};
};


//
//  Base Frame class
//
class iggFrameBase : public iggWidget
{

public:

	virtual ~iggFrameBase();

	virtual bool CheckBaloonHelpStatus();

	void RegisterChild(iggWidget *child);
	void UnRegisterChild(iggWidget *child);

	void RegisterRenderWindowObserver(iggRenderWindowObserver *rwo);
	void UnRegisterRenderWindowObserver(iggRenderWindowObserver *rwo);

	virtual void AddDependent(iggWidget *w);
	virtual void RemoveDependent(iggWidget *w);

	virtual void FlipThroughAllChildren() = 0;

protected:

	iggFrameBase(iggFrameBase *parent);
	iggFrameBase(iggShell *shell);

	virtual void UpdateWidgetBody();
	//
	//  Children are updated when this frame is updated
	//
	virtual void UpdateChildren();

	virtual void OnInt1Body(int);
	virtual void OnInt2Body(int);
	virtual void OnVoid1Body();
	virtual void OnVoid2Body();
	virtual void OnVoid3Body();
	virtual void OnBool1Body(bool);

	void OnChildFlipped();

	bool mNeedsUpdate;
	iLookupArray<iggWidget*> mChildren;
	iLookupArray<iggRenderWindowObserver*> mRenderWindowObservers;

private:

	void Define();
};


//
//  Plain frame with an optional border and title
//
class iggFrame : public iggFrameBase
{

	friend class ibgFrameSubject;
	friend class iggFrameBook;

public:

	iggFrame(const iString& title, iggFrameBase *parent, int cols = 1);
	iggFrame(iggFrameBase *parent, int cols = 1);
	iggFrame(iggShell *shell, int cols = 1, bool withsubject = true);

	inline ibgFrameSubject* GetSubject() const { return mSubject; }

	void SetPadding(bool s);
	void SetColStretch(int col, int s);
	void SetRowStretch(int row, int s);

	void AddLine(iggWidget *child1, iggWidget *child2 = 0, iggWidget *child3 = 0, iggWidget *child4 = 0, iggWidget *child5 = 0, iggWidget *child6 = 0, iggWidget *child7 = 0);
	void AddLine(iggWidget *child1, int nc1, iggWidget *child2 = 0, int nc2 = 1, iggWidget *child3 = 0, int nc3 = 1, iggWidget *child4 = 0, int nc4 = 1, iggWidget *child5 = 0, int nc5 = 1, iggWidget *child6 = 0, int nc6 = 1, iggWidget *child7 = 0, int nc7 = 1);
	void AddSpace(int space);

	virtual void GetFrameGeometry(int wg[4]) const;

	void SetTitle(const iString &title);
	void ShowFrame(bool s);

	virtual void FlipThroughAllChildren();

	//
	//  Allow selected widgets to clear the laid-out flag
	//
	void ClearLaidOutFlag(const iggFrameBook *book); 
	void ClearLaidOutFlag(const iggFrameFlip *flip); 
	void ClearLaidOutFlag(const iggFrameScroll *scroll); 

protected:

	virtual bool NeedsRedoLayout();

	int mCurCol, mCurRow;
	ibgFrameSubject *mSubject;

private:

	void Define(int cols, bool withsubject);
	void InsertWidget(int &c, iggWidget *child, int nc);
};


//
//  A frame with delayed construction.
//
class iggFrameDC : public iggFrame
{

public:

	iggFrameDC(iggFrameBase *parent, int cols = 1);

protected:

	virtual bool NeedsRedoLayout();

	bool ImmediateConstruction() const; 
	void CompleteConstruction();
	virtual void CompleteConstructionBody() = 0;

	bool mCompleteConstructionCalled;
};


//
//  A generic Book frame (TabBook, TabWidget).
//
class iggFrameBook : public iggFrameBase
{

public:

	iggFrameBook(iggFrameBase *parent, bool withFeedback = false, int orientation = 0);
	
	void OpenPage(int i);
	void AddPage(const iString &title, const iImage *image, iggFrame *frame, int index = -1);
	void SetTabMode(int m);
	int GetTabMode() const;
	void SetOrientation(int v);
	int GetOrientation() const;

	void ChangeIcon(int page, const iImage *image);

	void EnablePage(int n, bool s);
	iggFrame* GetPage(int i) const;
	const iggFrame* GetLastPage() const;

	virtual void FlipThroughAllChildren();

protected:

	struct Page
	{
		iString Title;
		const iImage *Image;
		iggFrame *Frame;
		int Index;
		Page() { Title = ""; Image = 0; Frame = 0; Index = -1; }
	};

	iArray<Page> mPages;

	ibgFrameBookSubject *mSubject;
};


//
//  A frame that has several layers and shows a specific one on demand.
//
class iggFrameFlip : public iggFrameBase
{

public:

	iggFrameFlip(iggFrameBase *parent, bool expanding = true);

	int Count() const;

	inline int GetActiveLayer() const { return mActiveLayer; }
	void ShowLayer(int i);
	void AddLayer(iggFrame *layer);

	const iggFrame* GetLastLayer() const { return mLayers.Last(); }

	virtual void FlipThroughAllChildren();

protected:

	int mActiveLayer;
	iArray<iggFrame*> mLayers;
	ibgFrameFlipSubject *mSubject;
};


//
//  A frame that allows its contents to scroll up/down and left/right
//
class iggFrameScroll : public iggFrameBase
{

public:

	iggFrameScroll(iggFrameBase *parent, bool withHor = true, int numcol = 1, bool withVer = true);

	inline iggFrame* GetContents() const { return mContents; }

	virtual void FlipThroughAllChildren();

protected:

	iggFrame *mContents;
	ibgFrameScrollSubject *mSubject;
};


//
//  A frame that splits into several adjustable-size segments
//
class iggFrameSplit : public iggFrameBase
{

public:

	iggFrameSplit(iggFrameBase *parent, bool hor);

	void AddSegment(iggFrame *frame);

	virtual void FlipThroughAllChildren();

protected:

	iArray<iggFrame*> mSegments;
	ibgFrameSplitSubject *mSubject;
};

#endif  // IGGFRAME_H

