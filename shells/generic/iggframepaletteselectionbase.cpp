/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggframepaletteselectionbase.h"


#include "ierror.h"
#include "imath.h"
#include "ipalette.h"
#include "ipalettecollection.h"
#include "ishell.h"

#include "iggdialogpaletteeditor.h"
#include "iggmainwindow.h"
#include "iggwidgetarea.h"
#include "iggwidgetmisc.h"
#include "iggwidgetotherbutton.h"

#include "ibgwidgetselectionboxsubject.h"

#include "iggsubjectfactory.h"

//
//  Templates
//
#include "iarray.tlh"


namespace iggFramePaletteSelectionBase_Private
{
	class PaletteView : public iggWidgetImageArea
	{

	public:

		PaletteView(iggFramePaletteViewFrame *parent) : iggWidgetImageArea(parent)
		{
			mFrame = parent;
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			static const iImage dummy;
			if(mFrame->GetActivePalette() != 0)
			{
				this->SetImage(*mFrame->GetActivePalette()->GetImage(),true);
			}
			else
			{
				this->SetImage(dummy,true);
			}
			this->iggWidgetImageArea::UpdateWidgetBody();
		}

		iggFramePaletteViewFrame *mFrame;
	};

	
	class UseBrightnessCheckBox : public iggWidgetSimpleCheckBox
	{

	public:

		UseBrightnessCheckBox(iggFramePaletteSelectionBase *parent) : iggWidgetSimpleCheckBox("Use object brightness",parent)
		{
			mFrame = parent;
			this->SetBaloonHelp("Use object brightness as the current palette","This check box replaces the current palette with a custom palette made of the same color as the color of the object, but with varied brightness.");
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			this->SetChecked(mFrame->GetBrightnessPalette());
		}

		virtual void OnChecked(bool s)
		{
			mFrame->SetBrightnessPalette(s);
		}

		iggFramePaletteSelectionBase *mFrame;
	};


	class UseReversedCheckBox : public iggWidgetSimpleCheckBox
	{

	public:

		UseReversedCheckBox(iggFramePaletteSelectionBase *parent) : iggWidgetSimpleCheckBox("Reverse",parent)
		{
			mFrame = parent;
			this->SetBaloonHelp("Reverse the current palette","Use this check box to flip the order of colors in the current palette.");
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			if(!mFrame->GetBrightnessPalette())
			{
				this->Enable(true);
				this->SetChecked(mFrame->GetReversedPalette());
			}
			else this->Enable(false);
		}

		virtual void OnChecked(bool s)
		{
			mFrame->SetReversedPalette(s);
		}

		iggFramePaletteSelectionBase *mFrame;
	};


	class PaletteList : public iggWidget
	{

	public:

		PaletteList(iggFramePaletteSelectionBase *parent) : iggWidget(parent)
		{
			mSubject = iggSubjectFactory::CreateWidgetComboBoxSubject(this,"Palette");
			mFrame = parent;

			this->SetBaloonHelp("Choose palette from the list","Choose a palette from the list of all available palettes. Use Palette Editor from the dialog menus to create new or change existing palettes.");
		}

		virtual ~PaletteList()
		{
		}

		int GetValue() const
		{
			return mSubject->GetValue();
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			if(!mFrame->GetBrightnessPalette())
			{
				this->Enable(true);

				int i, np = iPaletteCollection::Global()->GetNumber();

				mSubject->Clear();
				for(i=0; i<np; i++)
				{
					mSubject->InsertItem(iPaletteCollection::Global()->GetPalette(i)->GetName());
				}

				mSubject->SetValue(mFrame->GetActivePaletteIndex());
			}
			else this->Enable(false);
		}

		virtual void OnInt1Body(int n)
		{
			mFrame->SetActivePaletteIndex(n);
		}

		iggFramePaletteSelectionBase *mFrame;
		ibgWidgetComboBoxSubject *mSubject;
	};


	class LaunchButton : public iggWidgetLaunchButton
	{

	public:

		LaunchButton(PaletteList *list, iggFrame *parent) : iggWidgetLaunchButton(parent->GetMainWindow()->GetDialogPaletteEditor(),"Launch palette editor",parent)
		{
			mList = list;
		}

	protected:

		virtual void OnVoid1Body()
		{
			if(mDialog!=0 && mList!=0)
			{
				iDynamicCast<iggDialogPaletteEditor,iggDialog>(INFO,mDialog)->SetActivePaletteIndex(mList->GetValue());
			}
			iggWidgetLaunchButton::OnVoid1Body();
		}

		PaletteList *mList;
	};
};


using namespace iggFramePaletteSelectionBase_Private;


iggFramePaletteViewFrame::iggFramePaletteViewFrame(const iString &title, iggFrame *parent) : iggFrame(title,parent,3)
{
	mPaletteView = new PaletteView(this);

	this->AddLine(mPaletteView,3);
	this->SetColStretch(1,10);
}


iggFramePaletteSelectionBase::iggFramePaletteSelectionBase(bool witheditor, bool withbrightness, bool withreverse, iggFrame *parent) : iggFramePaletteViewFrame("Current palette",parent), mWithBrightness(withbrightness)
{
	PaletteList *list = new PaletteList(this);
	UseReversedCheckBox *rb = 0;

	if(withreverse)
	{
		rb = new UseReversedCheckBox(this);
	}

	if(withbrightness)
	{
		UseBrightnessCheckBox *bb = new UseBrightnessCheckBox(this);
		bb->AddDependent(mPaletteView);
		bb->AddDependent(list);
		bb->AddDependent(rb);
		this->AddLine(bb,3);
	}

	this->AddLine(list,2,rb,1);

	if(witheditor)
	{
		this->AddLine(new LaunchButton(list,this));
	}

	list->AddDependent(mPaletteView);
	List().AddUnique(this);

	mOldPaletteId = 1;
}


iggFramePaletteSelectionBase::~iggFramePaletteSelectionBase()
{
	List().Remove(this);
}


void iggFramePaletteSelectionBase::SetReversedPalette(bool s)
{
	int id = this->GetPaletteId();

	if(s && id>0)
	{
		this->SetPaletteId(-id);
	}

	if(!s && id<0)
	{
		this->SetPaletteId(-id);
	}
}


bool iggFramePaletteSelectionBase::GetReversedPalette() const
{
	return (this->GetPaletteId() < 0);
}


void iggFramePaletteSelectionBase::SetBrightnessPalette(bool s)
{
	int id = this->GetPaletteId();

	if(s && id!=0)
	{
		mOldPaletteId = id;
		this->SetPaletteId(0);
	}

	if(!s && id==0)
	{
		this->SetPaletteId(mOldPaletteId);
	}
}


bool iggFramePaletteSelectionBase::GetBrightnessPalette() const
{
	if(mWithBrightness) return (this->GetPaletteId() == 0); else return false;
}


void iggFramePaletteSelectionBase::SetActivePaletteIndex(int n)
{
	int id = this->GetPaletteId();

	if(n+1 != iMath::Abs(id))
	{
		if(n>=0 && n<iPaletteCollection::Global()->GetNumber())
		{
			this->SetPaletteId((1+n)*(id<0?-1:1));
		}
		else IBUG_WARN("Incorrect palette number.");
	}
}


int iggFramePaletteSelectionBase::GetActivePaletteIndex() const
{
	int id = this->GetPaletteId();
	
	if(id == 0) return iMath::Abs(mOldPaletteId)-1; else return iMath::Abs(id)-1;
}


void iggFramePaletteSelectionBase::UpdateAll()
{
	iLookupArray<iggFramePaletteSelectionBase*> &list(List());
	int i, n = list.Size();

	for(i=0; i<n; i++) list[i]->UpdateWidget();
}


iLookupArray<iggFramePaletteSelectionBase*>& iggFramePaletteSelectionBase::List()
{
	static iLookupArray<iggFramePaletteSelectionBase*> list;
	return list;
}


iPalette* iggFramePaletteSelectionBase::GetActivePalette() const
{
	return iPaletteCollection::Global()->GetPalette(this->GetActivePaletteIndex());
}

#endif
