/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggdialogpaletteeditor.h"


#include "ierror.h"
#include "ihistogram.h"
#include "ifile.h"
#include "ipalette.h"
#include "ipalettecollection.h"
#include "ipiecewisefunction.h"

#include "iggframepaletteselectionbase.h"
#include "iggframepiecewisefunctioncontrols.h"
#include "iggimagefactory.h"
#include "iggmainwindow.h"
#include "iggshell.h"
#include "iggwidgetarea.h"
#include "iggwidgetmisc.h"
#include "iggwidgetotherbutton.h"

#include "ibgwidgetentrysubject.h"

#include "iggsubjectfactory.h"

//
//  Templates
//
#include "iarray.tlh"


using namespace iParameter;


namespace iggDialogPaletteEditor_Private
{
	static const iString temp("temp");

	class Palette : public iPalette
	{

	public:

		Palette(iShell *shell) : iPalette(0,temp,temp)
		{
		}
	};


	//
	//  Load and save palettes to files
	//
	class FileLoadButton : public iggWidgetSimpleButton
	{

	public:

		FileLoadButton(iggDialogPaletteEditor *dialog, iggFrame *parent) : iggWidgetSimpleButton("Load from file",parent)
		{
			mDialog = dialog;
			this->SetBaloonHelp("Load palette from file","Load a palette from a file. The file must have been previously created by IFrIT Palette Editor.");
		}

	protected:

		virtual void Execute()
		{
			iString fn;
			if(mOldFileName.IsEmpty()) mOldFileName = this->GetShell()->GetEnvironment(Environment::Palette);
			fn = this->GetMainWindow()->GetFileName("Open palette file",mOldFileName,"Palette file (*.ipf)");
			if(!fn.IsEmpty() && fn.Part(-4).Lower()!=".ipf") fn += ".ipf";

			iFile f(fn);
			if(!f.Open(iFile::_Read,iFile::_Text))
			{
				this->GetShell()->PopupWindow(mDialog,"Failed to open a palette file.",MessageType::Error);
				return;
			}

			iString line, ws;
			while(f.ReadLine(ws)) line += ws + "\n";
			f.Close();

			if(line.Contains("/Palette/") > 0)
			{
				line.Replace("/Palette","");
				line.Replace("/0,1;","\n");
				line.Replace("/","\n");
			}

			if(line.Contains("/Red/0,1;") > 0)
			{
				line.Replace("/Red/0,1;","\n");
				line.Replace("/ Green/0,1;","\n");
				line.Replace("/ Blue/0,1;","\n");
				line.Replace("/","\n");
			}

			ws = line.Section("\n",0,0);
			mDialog->GetActivePalette()->SetName(ws);
			line = line.Section("\n",1);

			int j;
			for(j=0; j<3; j++)
			{
				bool ok;
				int i, n;
				iPiecewiseFunction *fun = mDialog->GetActivePalette()->GetComponent(j);

				ws = line.Section("\n",j,j);
				n = ws.Contains(';') + 1;
				if(n<2 || n>256)
				{
					this->GetShell()->PopupWindow(mDialog,"Palette file is corrupted.",MessageType::Error);
					return;
				}

				while(fun->N() > 2) fun->RemovePoint(1);

				for(i=0; i<n; i++)
				{
					float x, y;
					iString s;

					s = ws.Section(";",i,i);

					x = s.Section(",",0,0).ToFloat(ok);
					if(!ok)
					{
						this->GetShell()->PopupWindow(mDialog,"Palette file is corrupted.",MessageType::Error);
						return;
					}

					y = s.Section(",",1).ToFloat(ok);
					if(!ok)
					{
						this->GetShell()->PopupWindow(mDialog,"Palette file is corrupted.",MessageType::Error);
						return;
					}

					if(x<1.0e-5 || x>0.99999)
					{
						fun->MovePoint(i,x,y);
					}
					else
					{
						fun->AddPoint(x,y);
					}
				}
			}

			mOldFileName = fn;
			mDialog->SetChanged(true);
			mDialog->GetActivePalette()->Update();
			mDialog->GetFrame()->UpdateWidget();
		}

		iString mOldFileName;
		iggDialogPaletteEditor *mDialog;
	};


	class FileSaveButton : public iggWidgetSimpleButton
	{

	public:

		FileSaveButton(iggDialogPaletteEditor *dialog, iggFrame *parent) : iggWidgetSimpleButton("Save to file",parent)
		{
			mDialog = dialog;
			this->SetBaloonHelp("Save palette to file","Save the current palette to a file. If the file exists, it will be overwritten and all its contents erased.");
		}

	protected:

		virtual void Execute()
		{
			iString fn;
			if(mOldFileName.IsEmpty()) mOldFileName = this->GetShell()->GetEnvironment(Environment::Palette);
			fn = this->GetMainWindow()->GetFileName("Save palette file",mOldFileName,"Palette file (*.ipf)",false);
			if(!fn.IsEmpty() && fn.Part(-4).Lower()!=".ipf") fn += ".ipf";

			iFile f(fn);
			if(!f.Open(iFile::_Write,iFile::_Text))
			{
				this->GetShell()->PopupWindow(mDialog,"Failed to create a palette file.",MessageType::Error);
				return;
			}

			iString ws;
			ws += mDialog->GetActivePalette()->GetName() + "\n";
			int j;
			for(j=0; j<3; j++)
			{
				int i;
				iPiecewiseFunction *fun = mDialog->GetActivePalette()->GetComponent(j);
				for(i=0; i<fun->N(); i++)
				{
					if(i > 0) ws += ";";
					ws += iString::FromNumber(fun->X(i)) + "," + iString::FromNumber(fun->Y(i));
				}
				ws += "\n";
			}
			if(!f.WriteLine(ws))
			{
				this->GetShell()->PopupWindow(mDialog,"Unable to write into a palette file.",MessageType::Error);
			}
			else
			{
				mOldFileName = fn;
				mDialog->UpdateDialog();
			}

			f.Close();
		}

		iString mOldFileName;
		iggDialogPaletteEditor *mDialog;
	};


	class PaletteSelection : public iggFramePaletteSelectionBase
	{

	public:

		PaletteSelection(iggDialogPaletteEditor *editor, iggFrame *parent) : iggFramePaletteSelectionBase(false,false,false,parent)
		{
			mEditor = editor;
		}

	protected:

		virtual int GetPaletteId() const
		{
			return mEditor->GetActivePaletteIndex() + 1;
		}

		virtual void SetPaletteId(int n)
		{
			mEditor->SetActivePaletteIndex(n-1);
		}

		iggDialogPaletteEditor *mEditor;
	};


	class ColorComponentControls : public iggFramePiecewiseFunctionControls
	{

	public:

		ColorComponentControls(int type, iggDialogPaletteEditor *editor, iggFrame *parent): iggFramePiecewiseFunctionControls(false,false,parent)
		{
			mType = type;
			mEditor = editor;

			this->SetBaloonHelp("Interactively controls palette components. Press Shift+F1 for more help.","Each of the three palette component can be represented as a piecewise function. The piecewise function can be controlled interactively by clicking on a control point and dragging it around with the mouse. Buttons below the interactive area allow you to add or remove a control point and to reset the function to the default one.");
		}

		virtual const iHistogram& GetHistogram(int, bool) const
		{
			//
			//  No background histogram
			//
			return iHistogram::Null();
		}

		virtual iPiecewiseFunction* GetFunction()
		{
			return mEditor->GetActivePalette()->GetComponent(mType);
		}

		virtual void OnRender()
		{
			//
			//  No rendering
			//
		}

		virtual void OnChange()
		{
			mEditor->SetChanged(true);
			mEditor->GetActivePalette()->Update();
			this->UpdateDependents();
		}

	protected:

		int mType;
		iggDialogPaletteEditor *mEditor;
	};
	//
	//  Palette operations
	//
	class ApplyButton : public iggWidgetSimpleButton
	{

	public:

		ApplyButton(iggDialogPaletteEditor *dialog, iggFrame *parent) : iggWidgetSimpleButton("Apply modifications",parent)
		{
			mDialog = dialog;
			this->SetBaloonHelp("Apply the changes","Apply all the changes made to palettes and render all visualization scenes.");
		}

	protected:

		virtual void Execute()
		{
			mDialog->Apply(false);
			this->Enable(false);
		}

		iggDialogPaletteEditor *mDialog;
	};


	class NameLineEdit : public iggWidget
	{

	public:

		NameLineEdit(iggDialogPaletteEditor *dialog, iggFrame *parent) : iggWidget(parent)
		{
			mDialog = dialog;
			mSubject = iggSubjectFactory::CreateWidgetEntrySubject(this,false,0,"Palette");
			this->SetBaloonHelp("Set the name","Change the name of the current palette. The new name may contain white spaces but should not contain symbols '*'.");
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			mSubject->SetText(mDialog->GetActivePalette()->GetName());
		}

		virtual void OnVoid1Body()
		{
			iString s = mSubject->GetText();

			if(s.Find("*") >= 0)
			{
				this->GetShell()->PopupWindow(mDialog,"A palette name may not contain symbols '*'",MessageType::Error);
				mSubject->SetText(mDialog->GetActivePalette()->GetName());
			}
			else
			{
				mDialog->GetActivePalette()->SetName(s);
				mDialog->SetChanged(true);
			}
		}

		ibgWidgetEntrySubject *mSubject;
		iggDialogPaletteEditor *mDialog;
	};


	class NewPaletteDisplay : public iggFramePaletteViewFrame
	{

	public:

		NewPaletteDisplay(iggDialogPaletteEditor *dialog, iggFrame *parent) : iggFramePaletteViewFrame("Modified palette",parent)
		{
			mDialog = dialog;
			
			this->AddLine(new NameLineEdit(dialog,this),2);
		}

		virtual iPalette* GetActivePalette() const
		{
			return mDialog->GetActivePalette();
		}

	private:

		iggDialogPaletteEditor *mDialog;
	};


	class CreateNewButton : public iggWidgetSimpleButton
	{

	public:

		CreateNewButton(iggDialogPaletteEditor *dialog, iggFrame *parent) : iggWidgetSimpleButton("Create new palette",parent)
		{
			mDialog = dialog;
			this->SetBaloonHelp("Create new palette","Add a new palette to the global palette list.");
		}

	protected:

		virtual void Execute()
		{
			mDialog->Apply();
			if(!iPaletteCollection::Global()->Create.CallAction())
			{
				this->GetShell()->PopupWindow(mDialog,"Unable to create a new palette.\nPerhaps, there is not enough free memory.",MessageType::Error);
			}
			else
			{
				mDialog->SetActivePaletteIndex(iPaletteCollection::Global()->GetNumber()-1);
			}
		}

		iggDialogPaletteEditor *mDialog;
	};
};


using namespace iggDialogPaletteEditor_Private;


iggDialogPaletteEditor::iggDialogPaletteEditor(iggMainWindow *parent) : iggDialog(parent,0U,iggImageFactory::FindIcon("paled.png"),"Palette Editor","sr.gg.dpe",2)
{
	this->SetRenderTarget(ViewModuleSelection::All);

	mActivePalette = new Palette(this->GetShell()); IERROR_CHECK_MEMORY(mActivePalette);
	mActivePalette->CopyState(iPaletteCollection::Global()->GetPalette(0));
	mActivePaletteIndex = 0;

	if(this->ImmediateConstruction()) this->CompleteConstruction();
}


void iggDialogPaletteEditor::CompleteConstructionBody()
{
	iggFrame *cpb = new iggFrame(mFrame,4);
	PaletteSelection *ps = new PaletteSelection(this,cpb);
	NewPaletteDisplay *pd = new NewPaletteDisplay(this,cpb);
	ps->AddDependent(pd);

	iggFrame *po = new iggFrame("Palette operations",cpb);
	po->AddLine(new CreateNewButton(this,po));
	mApplyButton = new ApplyButton(this,po);
	mApplyButton->Enable(false);
	po->AddLine(mApplyButton);

	iggFrame *fo = new iggFrame("File operations",cpb);
	fo->AddLine(new FileLoadButton(this,fo));
	fo->AddLine(new FileSaveButton(this,fo));

	cpb->AddLine(ps,pd,po,fo);
	cpb->SetColStretch(0,10);
	cpb->SetColStretch(1,10);

	mFrame->AddLine(cpb);

	iggFrame *spb = new iggFrame("Sculpt Palette",mFrame,3);
	spb->AddLine(new iggWidgetTextLabel("%bRed",spb),new iggWidgetTextLabel("%bGreen",spb),new iggWidgetTextLabel("%bBlue",spb));

	iggWidget *pf1, *pf2, *pf3;
	pf1 = new ColorComponentControls(0,this,spb);
	ps->AddDependent(pf1);
	pf1->AddDependent(pd);
	pf2 = new ColorComponentControls(1,this,spb);
	ps->AddDependent(pf2);
	pf2->AddDependent(pd);
	pf3 = new ColorComponentControls(2,this,spb);
	ps->AddDependent(pf3);
	pf3->AddDependent(pd);
	spb->AddLine(pf1,pf2,pf3);

	spb->SetRowStretch(0,0);
	spb->SetRowStretch(1,10);

	mFrame->AddLine(spb);
	mFrame->SetColStretch(0,10);
	mFrame->SetColStretch(1,0);
}


iggDialogPaletteEditor::~iggDialogPaletteEditor()
{
	if(mActivePalette != 0) mActivePalette->Delete();
}


const iString& iggDialogPaletteEditor::GetToolTip() const
{
	static const iString tmp = "Create new palettes and edit existing ones.";
	return tmp;
}


void iggDialogPaletteEditor::SetChanged(bool s)
{
	this->CompleteConstruction();
	mApplyButton->Enable(s);
}


void iggDialogPaletteEditor::SetActivePaletteIndex(int n)
{
	if(n>=0 && n<iPaletteCollection::Global()->GetNumber())
	{
		this->CompleteConstruction();
		if(mApplyButton->IsEnabled()) this->Apply();
		mActivePalette->CopyState(iPaletteCollection::Global()->GetPalette(n));
		mActivePaletteIndex = n;
		this->UpdateDialog();
	}
}


void iggDialogPaletteEditor::Apply(bool ask)
{
	this->CompleteConstruction();
	if(mApplyButton->IsEnabled() && (!ask || this->GetShell()->PopupWindow(this,"Do you want to apply changes?",MessageType::Information,"Apply","Cancel")==0))
	{
		iPaletteCollection::Global()->GetPalette(mActivePaletteIndex)->CopyState(mActivePalette);
		iggFramePaletteSelectionBase::UpdateAll();
		this->Render();
	}
	mApplyButton->Enable(false);
}


bool iggDialogPaletteEditor::CanBeClosed()
{
	this->Apply();
	return true;
}

#endif
