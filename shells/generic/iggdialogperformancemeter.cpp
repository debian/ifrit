/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggdialogperformancemeter.h"


#include "isystem.h"
#include "iviewmodule.h"
#include "iviewmoduleeventobservers.h"

#include "iggframe.h"
#include "iggframedisplay.h"
#include "iggimagefactory.h"
#include "iggmainwindow.h"
#include "iggshell.h"
#include "iggwidgetarea.h"
#include "iggwidgetmisc.h"

#include "ibgwidgetareasubject.h"
#include "ibgwidgethelper.h"


using namespace iParameter;


namespace iggDialogPerformanceMeter_Private
{
	class ValueDisplay : public iggWidgetNumberLabel
	{

	public:
		
		ValueDisplay(iggDialogPerformanceMeter *dialog, bool max, int mode, iggFrame *parent) : iggWidgetNumberLabel(parent)
		{
			mDialog = dialog;
			mIsMax = max;
			mMode = mode;
			mValue = 0.0f;

			mWarning = 0;

			this->SetAlignment(1);
			mSubject->ShowFrame(true);
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			float v;

			mWidgetHelper->SetBackgroundColor(iColor::IFrIT());

			switch(mMode)
			{
			case 0:
				{
					v = this->GetShell()->GetViewModule()->GetRenderEventObserver()->GetRenderTime();
					break;
				}
			case 1:
				{
					v = this->GetShell()->GetViewModule()->GetUpdateTime();
					break;
				}
			case 2:
				{
					v = 0.001f*this->GetShell()->GetMemorySize();
					//
					//  50% threshold
					//
					if(v > 0.5f*iSystem::GetMemoryLimit())
					{
						mWidgetHelper->SetBackgroundColor(iColor(200,200,0));
						if(mWarning < 1)
						{
							mWarning = 1;
							if(mDialog != 0) mDialog->Show(true);
						}
					}
					if(v > 0.9f*iSystem::GetMemoryLimit())
					{
						mWidgetHelper->SetBackgroundColor(iColor(255,0,0));
						if(mWarning < 2)
						{
							mWarning = 2;
							if(mDialog != 0) mDialog->Show(true);
						}
					}
					break;
				}
			}

			float prev = mValue;

			if(mIsMax)
			{
				if(mValue < v) mValue = v;  
			}
			else
			{
				mValue = v;
			}

			if(fabs(mValue-prev) > 0.005) this->SetNumber(mValue,"%.2lg");
		}

		int mWarning;
		bool mIsMax;
		int mMode;
		float mValue;
		iggDialogPerformanceMeter *mDialog;
	};
};


using namespace iggDialogPerformanceMeter_Private;


iggDialogPerformanceMeter::iggDialogPerformanceMeter(iggMainWindow *parent) : iggDialog(parent,0U,iggImageFactory::FindIcon("perf.png"),"Performance Meter",0,7)
{
	if(this->ImmediateConstruction()) this->CompleteConstruction();
}


void iggDialogPerformanceMeter::CompleteConstructionBody()
{
	iggWidgetNumberLabel *tm = new iggWidgetNumberLabel(mFrame);
	tm->SetAlignment(1);
	tm->SetNumber(iSystem::GetMemoryLimit());

	mFrame->AddLine(static_cast<iggWidget*>(0),1,new iggWidgetTextLabel("Render time",mFrame),2,new iggWidgetTextLabel("Update time",mFrame),2,new iggWidgetTextLabel("Memory used",mFrame),2);
	mFrame->AddLine(new iggWidgetTextLabel("Current",mFrame),new ValueDisplay(this,false,0,mFrame),new iggWidgetTextLabel("sec",mFrame),new ValueDisplay(this,false,1,mFrame),new iggWidgetTextLabel("sec",mFrame),new ValueDisplay(this,false,2,mFrame),new iggWidgetTextLabel("MB",mFrame));
	mFrame->AddLine(new iggWidgetTextLabel("Maximum",mFrame),static_cast<iggWidget*>(0),static_cast<iggWidget*>(0),new ValueDisplay(this,true,1,mFrame),new iggWidgetTextLabel("sec",mFrame),new ValueDisplay(this,true,2,mFrame),new iggWidgetTextLabel("MB",mFrame));
	mFrame->AddLine(new iggWidgetTextLabel("Available",mFrame),1,static_cast<iggWidget*>(0),4,tm,1,new iggWidgetTextLabel("MB",mFrame),1);
	mFrame->AddSpace(10);

	mFrame->SetColStretch(0,10);
}


const iString& iggDialogPerformanceMeter::GetToolTip() const
{
	static iString tmp = "Report the amount of used memory and rendering times";
	return tmp;
}

#endif
