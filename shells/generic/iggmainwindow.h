/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Toolkit-independent MainWindow class
//

#ifndef IGGMAINWINDOW_H
#define IGGMAINWINDOW_H


#include "iobject.h"
#include "iggmenuwindow.h"


#include "iarray.h"
#include "istring.h"


class iDataInfo;
class iImage;
class iViewModule;

class iggDialog;
class iggDialogAbout;
class iggDialogAnimatingProgress;
class iggDialogAuto;
class iggDialogBase;
class iggDialogDataExplorer;
class iggDialogFileSetExplorer;
class iggDialogHelp;
class iggDialogImageComposer;
class iggDialogLoadFile;
class iggDialogPaletteEditor;
class iggDialogParallelController;
class iggDialogPickerWindow;
class iggDialogRenderingProgress;
class iggExtensionWindow;
class iggFrame;
class iggFrameBook;
class iggFrameFlip;
class iggPageData;
class iggPageObject;
class iggPageParticles;
class iggPageView;
class iggRenderWindow;
class iggScriptWindow;
class iggWidget;
class iggWidgetProgressBar;
class iggWidgetTextBrowser;

class ibgMainWindowSubject;
class ibgWindowSubject;


class iggMainWindow : public iObject, public iggMenuWindow
{

	friend class iggShell;

public:

	vtkTypeMacro(iggMainWindow,iObject);

	//
	//  Register dependent windows
	//
	void Register(ibgWindowSubject *ws);
	void UnRegister(ibgWindowSubject *ws);

	void RegisterAutoDialog(iggDialogAuto *d);
	void UnRegisterAutoDialog(iggDialogAuto *d);
	void PlaceAutoDialogs();

	//
	//  Access components
	//
	inline ibgMainWindowSubject* GetMainSubject() const { return mMainSubject; }

	inline iggExtensionWindow* GetExtensionWindow() const { return mExtensionWindow; }

	inline iggWidgetProgressBar* GetProgressBar() const { return mProgressBar; }
	inline iggFrameFlip* GetDataTypeFrame() const { return mDataTypeFrame; }

	inline iggDialog* GetDialogDataExplorer() const { return mDialogDataExplorer; }
	inline iggDialog* GetDialogPaletteEditor() const { return mDialogPaletteEditor; };
	inline iggDialog* GetDialogPerformanceMeter() const { return mDialogPerformanceMeter; };

	inline iggDialogAnimatingProgress* GetDialogAnimatingProgress() const { return mDialogAnimatingProgress; };
	inline iggDialogHelp* GetDialogHelp() const { return mDialogHelp; };
	inline iggDialogImageComposer* GetDialogImageComposer() const { return mDialogImageComposer; };
	inline iggDialogLoadFile* GetDialogLoadFile() const { return mDialogLoadFile; }
	inline iggDialogParallelController* GetDialogParallelController() const { return mDialogParallelController; };
	inline iggDialogPickerWindow* GetDialogPickerWindow() const { return mDialogPickerWindow; };
	inline iggDialogRenderingProgress* GetDialogRenderingProgress() const { return mDialogRenderingProgress; };
	inline iggScriptWindow* GetScriptWindow() const { return mScriptWindow; };

	inline iggPageObject* GetPage(int n) const { if(n>0 && n<7) return mPages[n-1]; else return 0; }

	void WriteToLog(const iString &prefix, const iString &message, const iColor &c = iColor(0,0,0));
	void WriteToLog(const iString &message);
#ifdef I_DEBUG
	void WriteToDebugLog(const iString &message);
#endif
	void ClearLog();
	void ShowLog(bool s);
	bool IsLogVisible() const;

	bool AskForConfirmation(const iString &message, const char *action);

	void ProcessEvents(bool sync = false) const;
	void AddTheme(const iString &name);

	//
	//  Data handling
	//
	void AfterDataChanged(); // data can change not just from loading a file - for example, is some are erased

	//
	//  Update components
	//
	void UpdateOnPick();
	void UpdateMarkerWidgets();
	void UpdateParticleWidgets(const iImage *icon = 0);

	void ShowAll(bool s);
	void Exit();
#ifndef I_NO_CHECK
	void Block(bool s, int line, const char *file);
#define BLOCK(s) Block(s,__LINE__,__FILE__)
#else
	void Block(bool s);
#define BLOCK(s) Block(s)
#endif

	//
	//  Main book manipulation
	//
	void OpenBookPage(int n);

	//
	//  Render window events
	//
	void OnRenderWindowMove(int wn);
	void OnRenderWindowResize(int wn);
	void OnRenderWindowFocusIn(int wn);
	void OnRenderWindowFocusOut(int wn);
	void OnRenderWindowEnter(int wn);
	void OnRenderWindowLeave(int wn);

	//
	//  Attached windows manipulation
	//
	void DisplayWindowsAsIcons();
	void DisplayWindowsAsWindows();
	void MoveWindows(int pos[2]);

	void ShowInteractorHelp(bool s, iViewModule *vm = 0);

private:

	iggMainWindow(iggShell *shell);
	virtual ~iggMainWindow();

	void StartInitialization();
	void PreShowInitialization();
	void PostShowInitialization();

	void StartTimer();

	virtual void UpdateContents();

	//
	//  Menu work
	//
	virtual void OnMenuBody(int id, bool on);
	void BuildMenus();
	const iString ListBlockRegistry();

	//
	//  Misc functions
	//
	void DockWindows(bool s, bool show);
	bool IsExitAllowed();

	//
	//  Subject
	//
	ibgMainWindowSubject *mMainSubject;

	//
	//  Extension
	//
	iggExtensionWindow *mExtensionWindow;

	//
	//  Dialogs
	//
	iggDialog *mDialogAbout;
	iggDialog *mDialogAxesLabels;
	iggDialog *mDialogDataExplorer;
	iggDialog *mDialogDocking;
	iggDialog *mDialogFileSetExplorer;
	iggDialog *mDialogPaletteEditor;
	iggDialog *mDialogPerformanceMeter;

	iggDialogAnimatingProgress *mDialogAnimatingProgress;
	iggDialogHelp *mDialogHelp;
	iggDialogImageComposer *mDialogImageComposer;
	iggDialogLoadFile *mDialogLoadFile;
	iggDialogParallelController *mDialogParallelController;
	iggDialogPickerWindow *mDialogPickerWindow;
	iggDialogRenderingProgress *mDialogRenderingProgress;
	iggScriptWindow *mScriptWindow;

	iLookupArray<iggDialogAuto*> mAutoDialogList;

#if defined(I_DEBUG)
	iggDialog *mDialogDebugHelper;
#endif
	//
	//  Widget components
	//
	iggFrameTopParent *mBusyIndicatorFrame, *mVisitedFileListFrame;
	iggFrameFlip *mBaseFrame, *mDataTypeFrame;
	iggWidget *mBusyIndicator, *mVisitedFileList, *mRenderButton, *mRenderAllButton;
	iggFrameBook *mBook;
	iggPageView *mViewPage;
	iggPageParticles *mParticlesPage;
	iggPageData *mDataPage;
	iggWidgetTextBrowser *mLog;

	iggPageObject *mPages[6];

	//
	//  Helper variables
	//
	iArray<iString> mThemeList;
	int mBlockLevel;
	iLookupArray<ibgWindowSubject*> mWindowList;
	iggWidgetProgressBar *mProgressBar;
	bool mMoveTogether;
	bool mInitialized, mInMove, mTwoSubjectsAreTheSame;
	iString mStateFileName, mExportFileName;
	int mInitialGeometry[4], mPrevPos[2], mNextPos[2];

	iggMainWindow(const iggMainWindow&); // Not implemented.
	void operator=(const iggMainWindow&);  // Not implemented.

	iObjectPropertyMacro2V(Geometry,Int);
	iObjectPropertyMacro2V(ExtensionGeometry,Int);

	iObjectPropertyMacro1S(TabMode,Int);
	iObjectPropertyMacro1S(Theme,String);
	iObjectPropertyMacro1S(BookOrientation,Int);

	iObjectPropertyMacro1S(Docked,Bool);
	iObjectPropertyMacro1S(ToolTips,Bool);
	iObjectPropertyMacro1S(InteractorHelp,Bool);
	iObjectPropertyMacro1S(IsIdiosyncratic,Bool);
	iObjectPropertyMacro1S(OptionsAreGlobal,Bool);
	iObjectPropertyMacro1S(AllowPrePopulateToolBar,Bool);
	iObjectPropertyMacro1S(WindowUnderFocusActive,Bool);
};

//
//  Useful Macros
//
#define FOR_EVERY_RENDER_WINDOW(window) \
vtkRenderWindowCollection *c_; iggRenderWindow *window; int k_, n_ = this->GetShell()->GetNumberOfViewModules(); \
for(k_=0;  k_<n_ && (c_=this->GetShell()->GetViewModule(k_)->GetRenderWindowCollection(true))!=0; k_++) while((window=iRequiredCast<iggRenderWindow>(INFO,c_->GetNextItem())) != 0)

#endif  // IGGMAINWINDOW_H

