/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggpagedata.h"


#include "icoredatasubjects.h"
#include "idata.h"
#include "idatalimits.h"
#include "idatareader.h"
#include "ierror.h"
#include "iparallelmanager.h"
#include "istretch.h"
#include "iviewmodule.h"

#include "iggdialog.h"
#include "iggframeboxsize.h"
#include "iggframedatatypeselector.h"
#include "iggframedatatypewidgetlist.h"
#include "iggframedatavariablelimits.h"
#include "iggframedatavariablelist.h"
#include "igghandle.h"
#include "iggimagefactory.h"
#include "iggmainwindow.h"
#include "iggshell.h"
#include "iggwidgetarea.h"
#include "iggwidgetmisc.h"
#include "iggwidgetpropertycontrolbutton.h"
#include "iggwidgetpropertycontrollineedit.h"
#include "iggwidgetpropertycontrolselectionbox.h"
#include "iggwidgetpropertycontrolslider.h"
#include "iggwidgetotherbutton.h"

#include "ibgwidgetbuttonsubject.h"
#include "ibgwidgethelper.h"
#include "ibgwidgetselectionboxsubject.h"


//
//  Templates
//
#include "iarray.tlh"
#include "iggwidgetpropertycontrollineedit.tlh"
#include "iggwidgetpropertycontrolslider.tlh"


using namespace iParameter;


namespace iggPageData_Private
{
	//
	//  Controls the DataType of this page
	//
	class DataTypeHandle : public iggDataTypeHandle
	{

	public:

		DataTypeHandle(iggPageData *owner)
		{
			mOwner = owner;
			mPage = 0;

			iDataType::FindTypesByKeywords(mDataInfo[1],"scalars");
			iDataType::FindTypesByKeywords(mDataInfo[2],"particles");

			int j;
			for(j=0; j<3; j++)
			{
				mIndex[j] = new iggIndex(mOwner->GetShell()); IASSERT(mIndex[j]);
			}
		}

		~DataTypeHandle()
		{
			int j;
			for(j=0; j<3; j++) delete mIndex[j];
		}

		void SetPage(int i)
		{
			if(i>=0 && i<3)
			{
				mPage = i;
			}
		}

		virtual const iDataType& GetDataType(int page) const
		{
			return mDataInfo[page].Type(*(mIndex[page]));
		}

		virtual const iDataInfo& GetDataInfo() const
		{
			return mDataInfo[mPage];
		}

		virtual int GetActiveDataTypeIndex() const
		{
			return *(mIndex[mPage]);
		}

	protected:

		virtual void OnActiveDataTypeChanged(int v)
		{
			if(v>=0 && v<mDataInfo[mPage].Size())
			{
				*(mIndex[mPage]) = v;
				mOwner->UpdateWidget();
			}
		}

		virtual void AfterFileLoadBody(const iDataInfo &info)
		{
			int j, p;

			//
			//  Try each loaded type in order, until succeeded, but only if it has data
			//  (info lists all types, but only some of them may actually have data)
			//
			for(j=0; j<info.Size(); j++)
			{
				if(mOwner->GetShell()->GetViewModule()->GetReader()->IsThereData(info.Type(j)))
				{
					for(p=1; p<3; p++)
					{
						if(mDataInfo[p].Includes(info.Type(j))) this->SetActiveDataType(info.Type(j));
					}
				}
			}
		}

	private:

		int mPage;
		iggIndex* mIndex[3];
		iDataInfo mDataInfo[3];
		iggPageData *mOwner;
	};

	class ScalarSubjectHandle : public iggObjectHandle
	{

	public:

		ScalarSubjectHandle(iggPageData *owner) : iggObjectHandle(owner)
		{
			mOwner = owner; IASSERT(mOwner);
		}

		virtual const iObject* Object(int mod = -1) const
		{
			return mOwner->GetScalarSubject(mod);
		}

	private:

		iggPageData *mOwner;
	};

	class ParticleSubjectHandle : public iggObjectHandle
	{

	public:

		ParticleSubjectHandle(iggPageData *owner) : iggObjectHandle(owner)
		{
			mOwner = owner; IASSERT(mOwner);
		}

		virtual const iObject* Object(int mod = -1) const
		{
			return mOwner->GetParticleSubject(mod);
		}

	private:

		iggPageData *mOwner;
	};

	//
	//  Helper widgets
	//
	class LimitsFrameSet : public iggFrameScroll
	{
		
	public:
		
		LimitsFrameSet(const iggObjectHandle *dsh, iggFrame *parent) : iggFrameScroll(parent,true)
		{
			mDataSubjectHandle = dsh;

			mNeedsBaloonHelp = false;
		}

		virtual void UpdateChildren()
		{
			//
			//  This is expensive to update, make sure it does not update needlessly
			//
			if(!mNeedsUpdate) return;

			const iDataSubject *sub = dynamic_cast<const iDataSubject*>(mDataSubjectHandle->Object());
			if(sub == 0)
			{
				IBUG_ERROR("iggPageData::LimitsFrameSet is incorrectly configured.");
				return;
			}
			iDataLimits *lim = sub->GetLimits();
			if(lim == 0)
			{
				IBUG_ERROR("iggPageData::LimitsFrameSet is incorrectly configured.");
				return;
			}

			int i, n = lim->GetNumVars();
			if(n > mBoxes.Size())
			{
				for(i=mBoxes.Size(); i<n; i++)
				{
					mBoxes.Add(new iggFrameDataVariableLimits(i,mDataSubjectHandle,this->GetContents()));
					this->GetContents()->AddLine(mBoxes[i]);
					this->GetContents()->AddSpace(5);
					mBoxes[i]->UpdateWidget();
				}
			}
			//for(i=0; i<n; i++) mBoxes[i]->Show(true);
			iggFrameScroll::UpdateChildren();
		}

	protected:

		const iggObjectHandle *mDataSubjectHandle;
		iggIndexHandle *mIndexHandle;
		iArray<iggFrameDataVariableLimits*> mBoxes;
	};


	//
	//  Shift slider
	//
	class ShiftingSlider : public iggWidgetPropertyControlDoubleSlider
	{

	public:

		ShiftingSlider(const iString &title, iggFrame *parent, int index) : iggWidgetPropertyControlDoubleSlider(-1.0,1.0,20,0,5,false,false,title,iggPropertyHandle("Shift",parent->GetShell()->GetDataReaderHandle(),index),parent)
		{
			this->SetRenderTarget(ViewModuleSelection::Clones);
		}

	protected:

		void OnInt1Body(int v)
		{
			if(this->GetRenderMode() == RenderMode::Immediate)
			{
				this->iggWidgetPropertyControlDoubleSlider::OnInt1Body(v);
			}
			else
			{
				//
				//  Do not execute - DataReader::SetShift actually does the shift
				//
				double val;
				this->ConvertFromSliderInt(this->mSubject->GetValue(),val);
				this->mSubject->SetValue(this->mSubject->GetValue(),double(val));
				mIsTextValue = false;
			}
		}
	};


	class EnableShiftingCheckBox : public iggWidgetSimpleCheckBox
	{

	public:

		EnableShiftingCheckBox(iggFrame *target, iggFrame *parent) : iggWidgetSimpleCheckBox("Enable shifting",parent)
		{
			mTarget = target;
			this->SetChecked(false);
			mNeedsBaloonHelp = false;
		}

	protected:

		virtual void OnChecked(bool s)
		{
			mTarget->Enable(s);
		}

		iggFrame *mTarget;
	};


	//
	//  Erase widgets
	//
	class ErasePushButton : public iggWidgetSimpleButton
	{

	public:

		ErasePushButton(const iString &title, iggFrame *parent) : iggWidgetSimpleButton(title,parent)
		{
			iString tt = "Click this button to erase selected data.";
			this->SetBaloonHelp(tt);
		}

		void AddEraseDataType(const iDataType &type)
		{
			mEraseDataInfo += type;
		}


		void RemoveEraseDataType(const iDataType &type)
		{
			mEraseDataInfo -= type;
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			this->Enable(mEraseDataInfo.Size()>0 && this->GetShell()->GetViewModule()->GetReader()->IsThereData(mEraseDataInfo));
		}

		virtual void Execute()
		{
			if(this->GetMainWindow()->AskForConfirmation("Are you sure you want to erase data?","Erase"))
			{
				int i;
				for(i=0; i<mEraseDataInfo.Size(); i++)
				{
					this->GetShell()->GetViewModule()->GetReader()->EraseData(mEraseDataInfo.Type(i));
				}

				this->GetMainWindow()->AfterDataChanged();

				mEraseDataInfo.Clear();

				this->Enable(false);
			}
		}

		iDataInfo mEraseDataInfo;
	};


	class EraseCheckBox : public iggWidget
	{

	public:

		EraseCheckBox(const iDataType &type, const iString &title, ErasePushButton *button, iggFrame *parent) : iggWidget(parent), mType(type)
		{
			this->SetRenderTarget(ViewModuleSelection::Clones);

			mButton = button;

			mSubject = iggSubjectFactory::CreateWidgetButtonSubject(this,ButtonType::CheckBox,title,1);

			iString tt = "Check this box if you would like to erase these data.";
			this->SetBaloonHelp(tt);
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			mSubject->SetDown(false);
			mWidgetHelper->Enable(this->GetShell()->GetViewModule()->GetReader()->IsThereData(mType));
		}

		virtual void OnVoid1Body()
		{
			if(mSubject->IsDown())
			{
				mButton->AddEraseDataType(mType);
			}
			else
			{
				mButton->RemoveEraseDataType(mType);
			}
		}

		ErasePushButton *mButton;
		ibgWidgetButtonSubject *mSubject;
		const iDataType &mType;
	};


	class EraseFrame : public iggFrameDataTypeWidgetList
	{

	public:

		EraseFrame(ErasePushButton *button, iggFrame *parent) : iggFrameDataTypeWidgetList("Select the data to erase:",parent)
		{
			mButton = button;
			this->UpdateList();
		}

	protected:

		virtual iggWidget* CreateEntry(const iDataType &type) const
		{
			iggWidget *w = new EraseCheckBox(type,type.TextName(),mButton,this->EntryParent());
			w->AddBuddy(mButton);
			return w;
		}

		ErasePushButton *mButton;
	};


	/*=
	//
	//  Function calculator
	//
	class CalculatorLineEdit : public iggWidget
	{

	public:

		CalculatorLineEdit(iggFrame *parent) : iggWidget(parent)
		{
			this->SetRenderTarget(ViewModuleSelection::Clones);

			mSubject = iggSubjectFactory::CreateWidgetEntrySubject(this,false,0,"%bFunction:");
			mSubject->SetEditable(false);

			mNeedsBaloonHelp = false;
		}

		void SelectText(int start, int len = -1)
		{
			mSubject->SelectText(start,len);
		}

		const iString GetText() const
		{
			return mSubject->GetText();
		}

		void SetText(const iString &text)
		{
			mSubject->SetText(text);
		}

	protected:

		virtual void UpdateWidgetBody()
		{
		}

		ibgWidgetEntrySubject *mSubject;
	};
	=*/

	class SmoothScalarDataRadioBox : public iggWidgetSimpleSpinBox
	{

	public:

		SmoothScalarDataRadioBox(const iString &title, iggFrame *parent) : iggWidgetSimpleSpinBox(1,10,1,title,parent)
		{
			iString tt = "Set the scale (in cell sizes) over which to smooth the scalar data.";
			this->SetBaloonHelp(tt);
		}
	};


	class SmoothScalarDataButton : public iggWidgetSimpleButton
	{

	public:

		SmoothScalarDataButton(iggPageData *owner, iggFrameDataVariableList *varBox, SmoothScalarDataRadioBox *valueBox, const iString &title, iggFrame *parent) : iggWidgetSimpleButton(title,parent)
		{
			mOwner = owner;
			mVarBox = varBox;
			mValueBox = valueBox;

			this->SetRenderTarget(ViewModuleSelection::Clones);

			iString tt = "Click this button to smooth scalar data.";
			this->SetBaloonHelp(tt);
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			this->Enable(mOwner->GetScalarSubject(-1)->GetDataType()==iCoreData::ScalarDataSubject::DataType() && mOwner->GetScalarSubject(-1)->IsThereData());
		}

		virtual void Execute()
		{
			iCoreData::ScalarDataSubject *sub = iDynamicCast<iCoreData::ScalarDataSubject>(INFO,mOwner->GetScalarSubject(-1));
			int var = mVarBox->GetIndex();
			if(var>=0 && var<sub->GetLimits()->GetNumVars())
			{
				sub->CallSmooth(var,mValueBox->GetValue());
				this->Render();
			}
		}

		iggFrameDataVariableList *mVarBox;
		SmoothScalarDataRadioBox *mValueBox;
		iggPageData *mOwner;
	};

	class ReloadingPushButton : public iggWidgetSimpleButton
	{

	public:

		ReloadingPushButton(const iString &title, iggFrame *parent) : iggWidgetSimpleButton(title,parent)
		{
			this->SetRenderTarget(ViewModuleSelection::Clones);

			iString tt = "Click this button to reload the current data set.";
			this->SetBaloonHelp(tt);
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			this->Enable(this->GetShell()->GetViewModule()->GetReader()->HasDataToReload());
		}

		virtual void Execute()
		{
			this->GetShell()->GetViewModule()->GetReader()->CallReload();
			this->Enable(false);
		}
	};


	class ParticleExtraPage : public iggFrame
	{

	public:

		ParticleExtraPage(iggPageData *owner, iggFrameBook *parent) : iggFrame(parent,2)
		{
			mOwner = owner; IASSERT(mOwner);
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			this->Enable(mOwner->GetParticleSubject(-1)->GetUseExtras());
			this->iggFrame::UpdateWidgetBody();
		}

		iggPageData *mOwner;
	};


	class ParallelExecutionBox : public iggWidgetSimpleRadioBox
	{

	public:

		ParallelExecutionBox(iggFrame *parent) : iggWidgetSimpleRadioBox(1,"Execution",parent)
		{
			this->SetBaloonHelp("Specify serial or parallel execution mode","Use this control to quickly switch from a serial (single core) to parallel (multiple cores/processors). More elaborate control of parallel execution is provided by the Parallel Controller dialog.");
			this->InsertItem("Serial");
			this->InsertItem("Parallel");
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			mSubject->SetValue((this->GetShell()->GetParallelManager()->GetNumberOfProcessors()==1)?0:1);
		}

		virtual void OnInt1Body(int i)
		{
			this->GetShell()->GetParallelManager()->SetNumberOfProcessors((i==0)?this->GetShell()->GetParallelManager()->GetMinNumberOfProcessors():this->GetShell()->GetParallelManager()->GetMaxNumberOfProcessors());
		}
	};


	//
	//  Shift page
	//
	class CommonShiftPage : public iggFrameDC
	{

	public:
	
		CommonShiftPage(iggFrameBase *parent) : iggFrameDC(parent,2)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		virtual void CompleteConstructionBody()
		{
			iggFrame *sh = new iggFrame("",this);
			sh->Enable(false);
			sh->AddLine(new ShiftingSlider("%bX",sh,0));
			sh->AddLine(new ShiftingSlider("%bY",sh,1));
			sh->AddLine(new ShiftingSlider("%bZ",sh,2));
			this->AddLine(new EnableShiftingCheckBox(sh,this));
			this->AddLine(new iggWidgetTextLabel("%bShifting rearranges all the data, and may be slow.",this));
			this->AddLine(new iggWidgetTextLabel("%bIt cannot be interrupted, to avoid data corruption.",this));
			this->AddLine(sh);
			this->AddSpace(1);
			this->AddSpace(10);
			this->SetColStretch(0,10);
			this->SetColStretch(1,3);
		}
	};


	//
	//  Erase page
	//
	class CommonErasePage : public iggFrameDC
	{

	public:

		CommonErasePage(iggFrameBase *parent) : iggFrameDC(parent,3)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		virtual void CompleteConstructionBody()
		{
			ErasePushButton *eb = new ErasePushButton("Erase selected data",this);
			this->AddLine(new EraseFrame(eb,this),2);
			this->AddLine(eb);
			this->AddSpace(10);
			this->SetRowStretch(0,0);
			this->SetColStretch(1,3);
			this->SetColStretch(2,5);
		}
	};


	//
	//  Common page
	// ************************************************
	//
	class CommonPage : public iggFrameDC
	{

	public:
	
		CommonPage(iggFrameBase *parent) : iggFrameDC(parent,1)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		virtual void CompleteConstructionBody()
		{
			const iImage *icon = iggImageFactory::FindIcon("data.png");

			//
			//  Book
			//
			iggFrameBook *sb = new iggFrameBook(this);
			this->AddLine(sb);
			//
			//  Global page
			//
			iggFrame *sbpage0 = new iggFrame(sb,3);
			sb->AddPage("Global",icon,sbpage0);
			{
				if(this->GetShell()->GetParallelManager()->GetMaxNumberOfProcessors() > 1)
				{
					sbpage0->AddLine(new iggFrameBoxSize(sbpage0),new ParallelExecutionBox(sbpage0));
				}
				else
				{
					sbpage0->AddLine(new iggFrameBoxSize(sbpage0));
				}
				sbpage0->AddSpace(2);

				iggFrame *f = new iggFrame(sbpage0,1);
				iggWidgetPropertyControlRadioBox *bc = new iggWidgetPropertyControlRadioBox(1,"Boundary conditions",0,iggPropertyHandle("BoundaryConditions",this->GetShell()->GetDataReaderHandle()),f);
				bc->InsertItem("None");
				bc->InsertItem("Periodic");
				f->AddLine(bc);
				f->AddLine(new iggWidgetTextLabel("%b(May require reloading of data)",f));

				iggWidgetPropertyControlRadioBox *orb = new iggWidgetPropertyControlRadioBox(1,"Optimize for...",0,iggPropertyHandle("OptimizationMode",this->GetShell()->GetShellHandle()),sbpage0);
				orb->InsertItem("Speed");
				orb->InsertItem("Memory");
				orb->InsertItem("Quality");

				sbpage0->AddLine(f,orb);
				sbpage0->AddSpace(2);

				ReloadingPushButton *rb = new ReloadingPushButton("Reload data",sbpage0);
				rb->AddDependent(bc);
				sbpage0->AddLine(rb);
				sbpage0->AddSpace(10);
				sbpage0->SetColStretch(2,10);
			}
			//
			//  Shift page
			//
			CommonShiftPage *sbpage1 = new CommonShiftPage(sb);
			sb->AddPage("Shift data",icon,sbpage1);

			//
			//  Erase page
			//
			CommonErasePage *sbpage2 = new CommonErasePage(sb);
			sb->AddPage("Erase",icon,sbpage2);
		}
	};


	//
	//  Scalar data page
	// ************************************************
	//
	class ScalarDataPage : public iggFrameDC
	{

	public:
	
		ScalarDataPage(iggPageData *owner, iggFrameBase *parent) : iggFrameDC(parent,1), mOwner(owner)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		iggPageData *mOwner;

		virtual void CompleteConstructionBody()
		{
			const iImage *icon = iggImageFactory::FindIcon("data.png");

			//
			//  Book
			//
			iggFrameBook *fb = new iggFrameBook(this);
			this->AddLine(fb);
			//
			//  Placement page
			//
			iggFrame *fbpage2 = new iggFrame(fb,2);
			fb->AddPage("Placement",icon,fbpage2);
			{
				iggWidgetPropertyControlRadioBox *vl = new iggWidgetPropertyControlRadioBox(1,"Voxel location",0,iggPropertyHandle("VoxelLocation",this->GetShell()->GetDataReaderHandle()),fbpage2);
				vl->InsertItem("Vertex of a cell (\"point data\")");
				vl->InsertItem("Center of a cell (\"cell data\")");
				fbpage2->AddLine(vl);

				iggWidgetTextLabel *tmp = new iggWidgetTextLabel("  Dimension to fit",fbpage2);
				tmp->SetAlignment(-1);
				fbpage2->AddLine(tmp);
				iggWidgetPropertyControlRadioBox *bb = new iggWidgetPropertyControlRadioBox(1,"into bounding box",2,iggPropertyHandle("ScaledDimension",this->GetShell()->GetDataReaderHandle()),fbpage2);
				bb->InsertItem("Longest");
				bb->InsertItem("Shortest");
				bb->InsertItem("X");
				bb->InsertItem("Y");
				bb->InsertItem("Z");
				fbpage2->AddLine(bb);
				fbpage2->AddSpace(10);

				fbpage2->SetColStretch(1,10);
			}
			//
			//  Limits page
			//
			iggFrame *fbpage1 = new iggFrame(fb,1);
			fb->AddPage("Limits",icon,fbpage1);
			{
				iggPropertyHandle ph("ResetOnLoad",mOwner->GetScalarSubjectHandle());
				fbpage1->AddLine(new iggWidgetPropertyControlCheckBox("Reset on file load",ph,fbpage1));
				fbpage1->AddLine(new LimitsFrameSet(mOwner->GetScalarSubjectHandle(),fbpage1));
			}
			//
			//  Operate page
			//
			iggFrame *fbpage3 = new iggFrame(fb,2);
			fb->AddPage("Operate",icon,fbpage3);
			{
				/*=
				iggFrameDataVariableList *ol = new iggFrameDataVariableList("Assign result to...",this->GetShell()->GetObjectHandle("Data.Scalars"),iggPropertyHandle("CalculatorOutput",this->GetShell()->GetObjectHandle("Data.Scalars")),fbpage3);
				ol->Complete();
				iggFrame *of = new iggFrame(fbpage3,1);
				of->AddSpace(10);
				iggWidgetPropertyControlTextLineEdit *oe = new iggWidgetPropertyControlTextLineEdit(true,"= ",iggPropertyHandle("CalculatorFunction",this->GetShell()->GetObjectHandle("Data.Scalars")),of);
				of->AddLine(oe);
				of->AddSpace(10);
				fbpage3->AddLine(ol,of);
				fbpage3->AddLine(new iggWidgetTextLabel("%b(May require reloading of data)",fbpage3));
				fbpage3->AddSpace(2);

				CalculatorDialog *cd = new CalculatorDialog(oe,this->GetMainWindow());
				fbpage3->AddLine(new iggWidgetLaunchButton(cd,"Launch calculator",fbpage3,true));
				fbpage3->AddLine(new iggWidgetTextLabel("%b(May require reloading of data)",fbpage3));
				=*/

				iggFrame *sf = new iggFrame("Smooth data",fbpage3,2);
				iggFrameDataVariableList *sl = new iggFrameDataVariableList("Variable",this->GetShell()->GetObjectHandle("Data.Scalars"),sf);
				sl->Complete();
				sf->AddLine(sl,2);
				SmoothScalarDataRadioBox *vb = new SmoothScalarDataRadioBox("Scale in cell sizes",sf);
				sf->AddLine(vb,new SmoothScalarDataButton(mOwner,sl,vb,"Smooth",sf));
				fbpage3->AddLine(sf);

				fbpage3->AddSpace(10);
				fbpage3->SetColStretch(1,10);
				fbpage3->SetColStretch(2,3);
			}
		}
	};


	//
	//  Particle data page
	// ************************************************
	//
	class ParticleDataPage : public iggFrameDC
	{

	public:
	
		ParticleDataPage(iggPageData *owner, iggFrameBase *parent) : iggFrameDC(parent,1), mOwner(owner)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		iggPageData *mOwner;

		virtual void CompleteConstructionBody()
		{
			const iImage *icon = iggImageFactory::FindIcon("data.png");

			//
			//  Book
			//
			iggFrameBook *pb = new iggFrameBook(this);
			this->AddLine(pb);
			//
			//  Downsample page
			//
			iggFrame *pbpage0 = new iggFrame(pb,3);
			pb->AddPage("Downsample",icon,pbpage0);
			{
				iggWidgetPropertyControlSpinBox *dl = new iggWidgetPropertyControlSpinBox(1,100,"Downsample factor",0,iggPropertyHandle("DownsampleFactor",mOwner->GetParticleSubjectHandle()),pbpage0);

				pbpage0->AddLine(dl);

				iggWidgetPropertyControlRadioBox *db = new iggWidgetPropertyControlRadioBox(1,"Downsample mask",0,iggPropertyHandle("DownsampleMode",mOwner->GetParticleSubjectHandle()),pbpage0);
				db->InsertItem("Regular");
				db->InsertItem("Regular 2D");
				db->InsertItem("Regular 3D");
				db->InsertItem("Random");
				db->InsertItem("Head of file");
				db->InsertItem("Tail of file");
				pbpage0->AddLine(db);
				pbpage0->AddLine(new iggWidgetTextLabel("%b(May require reloading of data)",pbpage0),2);

				pbpage0->AddSpace(10);
				pbpage0->SetColStretch(1,3);
				pbpage0->SetColStretch(2,10);
			}
			//
			//  Operate page
			//
			iggFrame *pbpage1 = new ParticleExtraPage(mOwner,pb);
			pb->AddPage("Operate",icon,pbpage1);
			{
				iggWidgetPropertyControlCheckBox *oa = new iggWidgetPropertyControlCheckBox("Include the particle order in the file as a new variable",iggPropertyHandle("AddOrderAsVar",mOwner->GetParticleSubjectHandle()),pbpage1);
				pbpage1->AddLine(oa);
				pbpage1->AddLine(new iggWidgetTextLabel("%b(May require reloading of data)",pbpage1));

				pbpage1->AddSpace(2);

				iggFrameDataVariableList *al = new iggFrameDataVariableList("Compute density of...",mOwner->GetParticleSubjectHandle(),iggPropertyHandle("DensityVar",mOwner->GetParticleSubjectHandle()),pbpage1);
				al->InsertItem("Do not compute");
				al->InsertItem("Particle number");
				al->Complete();

				pbpage1->AddLine(al);
				pbpage1->AddLine(new iggWidgetTextLabel("%b(May require reloading of data)",pbpage1));

				pbpage1->AddSpace(10);
				pbpage1->SetColStretch(1,10);
			}
		}
	};
};


using namespace iggPageData_Private;


iggPageData::iggPageData(iggFrameBase *parent) : iggPageMain(parent,iggPage::WithBook|iggPage::WithFeedback)
{
	const iImage *icon = iggImageFactory::FindIcon("data.png");

	mDataTypeHandle = new DataTypeHandle(this);
	mScalarSubjectHandle = new ScalarSubjectHandle(this);
	mParticleSubjectHandle = new ParticleSubjectHandle(this);

	//
	//  Common page
	// ************************************************
	//
	CommonPage *page0 = new CommonPage(mBook);
	mBook->AddPage("Common",icon,page0);

	//
	//  Field data page
	// ************************************************
	//
	ScalarDataPage *page1 = new ScalarDataPage(this,mBook);
	mBook->AddPage("Scalar data",icon,page1);

	//
	//  Particle data page
	// ************************************************
	//
	ParticleDataPage *page2 = new ParticleDataPage(this,mBook);
	mBook->AddPage("Particle data",icon,page2);
}


iggPageData::~iggPageData()
{
	delete mParticleSubjectHandle;
	delete mScalarSubjectHandle;
	delete mDataTypeHandle;
}


iggFrame* iggPageData::CreateFlipFrame(iggFrameBase *parent)
{
	return new iggFrameDataTypeSelector(mDataTypeHandle,"Active data",parent);
}


void iggPageData::OnInt1Body(int i)
{
	iDynamicCast<DataTypeHandle>(INFO,mDataTypeHandle)->SetPage(i);
	if(mFlipFrame != 0) mFlipFrame->UpdateWidget();
}


iDataSubject* iggPageData::GetScalarSubject(int mod) const
{
	return this->GetShell()->GetViewModule(mod)->GetReader()->GetSubject(iDynamicCast<DataTypeHandle>(INFO,mDataTypeHandle)->GetDataType(1));
}


iParticleDataSubject* iggPageData::GetParticleSubject(int mod) const
{
	return iDynamicCast<iParticleDataSubject>(INFO,this->GetShell()->GetViewModule(mod)->GetReader()->GetSubject(iDynamicCast<DataTypeHandle>(INFO,mDataTypeHandle)->GetDataType(2)));
}

#endif
