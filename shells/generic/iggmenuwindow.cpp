/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggmenuwindow.h"


#include "idirectory.h"
#include "ifile.h"
#include "ierror.h"

#include "iggframetopparent.h"
#include "iggmainwindow.h"
#include "iggshell.h"

#include "ibgmenuwindowsubject.h"

#include "iggsubjectfactory.h"


using namespace iParameter;

iggMenuWindow::iggMenuWindow(iggShell *shell) : iggRenderingElement(shell)
{
	mMainWindow = 0;
	mMenuSubject = 0;
	mGlobalFrame = 0;
}


iggMenuWindow::iggMenuWindow(iggMainWindow *mainWindow) : iggRenderingElement(mainWindow?mainWindow->GetShell():0)
{
	IASSERT(mainWindow);
	mMainWindow = mainWindow;
	mMenuSubject = 0;
	mGlobalFrame = 0;
}


iggMenuWindow::~iggMenuWindow()
{
	if(mMenuSubject != 0) mMenuSubject->Delete();
}


void iggMenuWindow::Show(bool s)
{
	IASSERT(mMenuSubject);

	if(s)
	{
		this->UpdateAll();
		mMenuSubject->Show(true);
	}
	else mMenuSubject->Show(false);
}


bool iggMenuWindow::CanBeClosed()
{
	return true;
}


iString iggMenuWindow::GetFileName(const iString &header, const iString &file, const iString &selection, bool reading)
{
	iString res(mMenuSubject->GetFileName(header,file,selection,reading));
	if(!res.IsEmpty())
	{
		iDirectory::ExpandFileName(res);
		if(mMainWindow != 0)
		{
			if(reading)
			{
				if(!iFile::IsReadable(res))
				{
					this->GetShell()->PopupWindow("The file is not readable.",MessageType::Warning);
					res.Clear();
				}
			}
			else
			{
				if(iFile::IsReadable(res))
				{
					if(this->GetShell()->PopupWindow("Do you want to overwrite the existing file?",MessageType::Warning,"Ok","Cancel") == 1) res.Clear();
				}
			}
		}
	}
	return res;
}


void iggMenuWindow::UpdateAll()
{
	this->UpdateMenus();
	this->UpdateContents();
}


void iggMenuWindow::UpdateMenus()
{
	IASSERT(mMenuSubject);
	mMenuSubject->UpdateMenus();
}


void iggMenuWindow::AttachSubject(ibgMenuWindowSubject *subject, int cols)
{
	IASSERT(subject);
	mMenuSubject = subject; 

	mGlobalFrame = new iggFrameTopParent(this->GetShell());
	mMenuSubject->SetGlobalFrame(mGlobalFrame,cols);
	IASSERT(mGlobalFrame->GetSubject());
}


void iggMenuWindow::OnMenu(int id, bool on)
{
	IASSERT(mMainWindow);

	mMainWindow->BLOCK(true);
	this->OnMenuBody(id,on);
	mMainWindow->BLOCK(false);
}

#endif
