/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/
  

#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggrenderwindowinteractor.h"


#include "ierror.h"
#include "iviewmodule.h"

#include "iggelement.h"
#include "iggrenderwindow.h"

#include "ibgrenderwindowsubject.h"

#include <vtkInteractorStyle.h>
#include <vtkCommand.h>


using namespace iParameter;


iggRenderWindowInteractor::iggRenderWindowInteractor(iViewModule *vm) : iViewModuleComponent(vm)
{
	this->Enabled = 0;
}


iggRenderWindowInteractor::~iggRenderWindowInteractor()
{
}


//
// We never allow the iggRenderWindowInteractor to control 
// the event loop. The application always has the control. 
//
void iggRenderWindowInteractor::Initialize() 
{
	//
	// If the render window has zero size, then set it to a default 
	// value of 128x128.
	// 
	int *size = this->RenderWindow->GetSize();
	size[0] = ((size[0] > 0) ? size[0] : 128);
	size[1] = ((size[1] > 0) ? size[1] : 128);
	this->RenderWindow->SetSize(size);

	//
	// Enable the interactor. 
	//
	this->Enable();
	
	//
	// Set our size to that of the render window. 
	//
	this->Size[0] = size[0];
	this->Size[1] = size[1];
	
	//
	// The interactor has been initialized.
	//
	this->Initialized = 1;
}


void iggRenderWindowInteractor::Start() 
{
	//
	// We do not allow this interactor to control the 
	// event loop. Only the QtApplication objects are
	// allowed to do that. 
	//
	vtkErrorMacro(<<"iggRenderWindowInteractor::Start() not allowed to start event loop.");
	return;
}


int iggRenderWindowInteractor::InternalCreateTimer(int timerId, int timerType, unsigned long duration)
{
	return iRequiredCast<iggRenderWindow>(INFO,this->RenderWindow)->GetSubject()->InternalCreateTimer(timerId,timerType,duration);
}


int iggRenderWindowInteractor::InternalDestroyTimer(int platformTimerId)
{
	return iRequiredCast<iggRenderWindow>(INFO,this->RenderWindow)->GetSubject()->InternalDestroyTimer(platformTimerId);
}


void iggRenderWindowInteractor::OnTimer() 
{
	if(!this->Enabled) return;

	iRequiredCast<vtkInteractorStyle>(INFO,this->InteractorStyle)->OnTimer();
	this->RenderWindow->Render();
}


void iggRenderWindowInteractor::OnEnter() 
{
	if(!this->Enabled) return;

	iRequiredCast<vtkInteractorStyle>(INFO,this->InteractorStyle)->OnEnter();
	this->RenderWindow->Render();
}


void iggRenderWindowInteractor::OnLeave() 
{
	if(!this->Enabled) return;

	iRequiredCast<vtkInteractorStyle>(INFO,this->InteractorStyle)->OnLeave();
	this->RenderWindow->Render();
}


void iggRenderWindowInteractor::OnExpose() 
{
	if(!this->Enabled) return;

	iRequiredCast<vtkInteractorStyle>(INFO,this->InteractorStyle)->OnExpose();
	this->RenderWindow->Render();
}


void iggRenderWindowInteractor::OnMouseMove(int x, int y, int b) 
{
	if(!this->Enabled || b==0) return;  //  No interaction without a button pressed
		
	this->SetControlKey(b & MouseButton::ControlKey);
	this->SetShiftKey(b & MouseButton::ShiftKey);
	this->SetEventPosition(x,y);
	this->SetKeySym("0");
	this->InvokeEvent(vtkCommand::MouseMoveEvent,0);
}


void iggRenderWindowInteractor::OnMousePress(int x, int y, int b) 
{
	if(!this->Enabled) return;
	
	this->SetControlKey(b & MouseButton::ControlKey);
	this->SetShiftKey(b & MouseButton::ShiftKey);
	this->SetEventPosition(x,y);
	this->SetKeySym("0");

	switch(b & MouseButton::AnyButton)
	{
	case MouseButton::LeftButton:
		{
			this->InvokeEvent(vtkCommand::LeftButtonPressEvent,0);
			break;
		}
	case MouseButton::MiddleButton:
		{
			this->InvokeEvent(vtkCommand::MiddleButtonPressEvent,0);
			break;
		}
	case MouseButton::RightButton:
		{
			this->InvokeEvent(vtkCommand::RightButtonPressEvent,0);
			break;
		}
	}
}


void iggRenderWindowInteractor::OnMouseRelease(int x, int y, int b) 
{
	if(!this->Enabled) return;
	
	this->SetControlKey(b & MouseButton::ControlKey);
	this->SetShiftKey(b & MouseButton::ShiftKey);
	this->SetEventPosition(x,y);
	this->SetKeySym("0");

	switch(b & MouseButton::AnyButton)
	{
	case MouseButton::LeftButton:
		{
			this->InvokeEvent(vtkCommand::LeftButtonReleaseEvent,0);
			break;
		}
	case MouseButton::MiddleButton:
		{
			this->InvokeEvent(vtkCommand::MiddleButtonReleaseEvent,0);
			break;
		}
	case MouseButton::RightButton:
		{
			this->InvokeEvent(vtkCommand::RightButtonReleaseEvent,0);
			break;
		}
	}
}


void iggRenderWindowInteractor::OnKeyPress(char key, int b) 
{
	const char keyEscape = char(27);

	if(!this->Enabled) return;
	
	if(key=='F' || key=='f' || (key==keyEscape && RenderWindow->GetFullScreen()!=0))
	{
		RenderWindow->SetFullScreen(!RenderWindow->GetFullScreen());
	}
#ifdef I_DEBUG
	else if(key=='B' || key=='b')
	{
		RenderWindow->SetBorders(!RenderWindow->GetBorders());
	}
#endif
	else
	{
		this->SetControlKey(b & MouseButton::ControlKey);
		this->SetShiftKey(b & MouseButton::ShiftKey);
		this->SetKeyCode(key);
		this->SetKeySym("0");
		this->InvokeEvent(vtkCommand::CharEvent,0);
	}		
}

//
//  User callback function for window interactor
//
void iggRenderWindowInteractor::UserCallback()
{
	iRequiredCast<iggRenderWindow>(INFO,RenderWindow)->GetViewModule()->WriteImages(ImageType::Image);
}

#endif
