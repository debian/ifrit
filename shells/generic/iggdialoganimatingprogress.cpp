/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggdialoganimatingprogress.h"


#include "ianimator.h"
#include "idatareader.h"
#include "ierror.h"
#include "ieventobserver.h"
#include "imath.h"
#include "isystem.h"
#include "iviewmodule.h"
#include "iwriter.h"

#include "iggframe.h"
#include "iggimagefactory.h"
#include "iggmainwindow.h"
#include "iggshell.h"
#include "iggwidgetmisc.h"
#include "iggwidgetotherbutton.h"

#include "ibgwidgetbuttonsubject.h"

#include "iggsubjectfactory.h"


using namespace iParameter;


namespace iggDialogAnimatingProgress_Private
{
	class CancelButton : public iggWidgetSimpleButton
	{

	public:

		CancelButton(iggDialogAnimatingProgress *dialog, iggFrame *parent) : iggWidgetSimpleButton("Cancel",parent)
		{
			mDialog = dialog;

			this->SetBaloonHelp("Cancel the animation");
		}

	protected:

		virtual void Execute()
		{
			mDialog->Cancel();
		}

		iggDialogAnimatingProgress *mDialog;
	};


	class PauseResumeButton : public iggWidgetSimpleButton
	{

	public:

		PauseResumeButton(iggDialogAnimatingProgress *dialog, iggFrame *parent) : iggWidgetSimpleButton("Pause",parent)
		{
			mDialog = dialog;

			this->SetBaloonHelp("Pause or resume the animation");
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			if(mDialog->IsPaused()) mSubject->SetText("Resume"); else mSubject->SetText("Pause");
		}

		virtual void Execute()
		{
			mDialog->Pause(!mDialog->IsPaused());
		}

		iggDialogAnimatingProgress *mDialog;
	};
};


using namespace iggDialogAnimatingProgress_Private;


iggDialogAnimatingProgress::iggDialogAnimatingProgress(iggMainWindow *parent) : iggDialog(parent,DialogFlag::Blocking|DialogFlag::NoTitleBar,0,"Animating...",0,4,0)
{
	if(this->ImmediateConstruction()) this->CompleteConstruction();
}


void iggDialogAnimatingProgress::CompleteConstructionBody()
{
	mFrame->AddLine(new iggWidgetTextLabel("%bAnimating the scene...",mFrame),4);
	
	mFrame->AddSpace(10);
	mFrame->AddLine(0,new PauseResumeButton(this,mFrame),new CancelButton(this,mFrame));
	mFrame->AddSpace(10);

	mInfoFrame = new iggFrame(mFrame,2);
	mCurrentRecord = new iggWidgetNumberLabel(mInfoFrame);
	mCurrentRecord->SetAlignment(-1);
	mInfoFrame->AddLine(new iggWidgetTextLabel("Current record: ",mInfoFrame),mCurrentRecord);
	mCurrentFrame = new iggWidgetNumberLabel(mInfoFrame);
	mCurrentFrame->SetAlignment(-1);
	mInfoFrame->AddLine(new iggWidgetTextLabel("Current frame: ",mInfoFrame),mCurrentFrame);
	mFrame->AddLine(mInfoFrame,4);

	mWarningFrame = new iggFrame(mFrame);
	mWarningFrame->AddLine(new iggWidgetTextLabel("%b%+Animation is paused",mWarningFrame));
	mFrame->AddLine(mWarningFrame,4);

	mCancelled = mPaused = false;
}


const iString& iggDialogAnimatingProgress::GetToolTip() const
{
	static iString tmp = "Pause or cancel an animation.";
	return tmp;
}

	
void iggDialogAnimatingProgress::Animate()
{
	this->CompleteConstruction();

	mCancelled = false;
	this->Pause(false);

	mLastRecord = mLastFrame = -1;

	iAnimator *anim = this->GetShell()->GetViewModule()->GetAnimator();

	this->Show(true);

	while(anim->CallContinue() && !this->IsCancelled())
	{
		bool nr;
		int cr, cf;
		iString s;
	
		anim->GetInfo(nr,cr,cf);

		if(cr != mLastRecord)
		{
			mLastRecord = cr;
			mCurrentRecord->SetNumber(cr,"%4d");
		}
		if(cf != mLastFrame)
		{
			mLastFrame = cf;
			mCurrentFrame->SetNumber(cf,"%4d");
		}

		this->GetMainWindow()->ProcessEvents();
		while(this->IsPaused() && !this->IsCancelled())
		{
			iSystem::Sleep(50);
			this->GetMainWindow()->ProcessEvents();
		}
	}

	mCancelled = mPaused = false;
	this->Show(false);
}


void iggDialogAnimatingProgress::Cancel()
{
    mCancelled = true;
}


void iggDialogAnimatingProgress::Pause(bool s)
{
	mPaused = s;

	this->CompleteConstruction();
	
	mInfoFrame->Show(!s);
	mWarningFrame->Show(s);

	mFrame->UpdateWidget();
}


bool iggDialogAnimatingProgress::CanBeClosed()
{
	return false;  // make it non-closeable
}

#endif
