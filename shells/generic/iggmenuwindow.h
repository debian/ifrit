/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A window with menus and toolsbars
//

#ifndef IGGMENUWINDOW_H
#define IGGMENUWINDOW_H


#include "iggrenderingelement.h"


#include "istring.h"

class iggFrameTopParent;
class iggMainWindow;

class ibgMenuWindowSubject;


class iggMenuWindow : public iggRenderingElement
{

	friend class iggSubjectFactory;

public:

	virtual ~iggMenuWindow();

	virtual void Show(bool s);
	ibgMenuWindowSubject* GetSubject() const { return mMenuSubject; }
	inline iggMainWindow* GetMainWindow() const { return mMainWindow; }

	iString GetFileName(const iString &header, const iString &file, const iString &selection, bool reading = true);

	inline const iggFrameTopParent* GetGlobalFrame() const { return mGlobalFrame; }
	virtual bool CanBeClosed();

	//
	//  Update components
	//
	void UpdateAll();
	void UpdateMenus();

	//
	//  Do all menu work here
	//
	void OnMenu(int id, bool on);

protected:

	iggMenuWindow(iggShell *shell);
	iggMenuWindow(iggMainWindow *mainWindow);

	void AttachSubject(ibgMenuWindowSubject *subject, int cols);

	virtual void OnMenuBody(int id, bool on) = 0;
	virtual void UpdateContents() = 0;

	iggFrameTopParent *mGlobalFrame;

	iggMainWindow *mMainWindow;

private: 

	//
	//  MenuSubject must be attached using AttachSubject call.
	//
	ibgMenuWindowSubject *mMenuSubject;
};

#endif  // IGGMENUWINDOW_H

