/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A decorator for the ComboBox or RadioBox which contains data variables
//

#ifndef IGGFRAMEDATAVARIABLELIST_H
#define IGGFRAMEDATAVARIABLELIST_H


#include "iggframe.h"


class iggDataTypeHandle;
class iggObjectHandle;
class iggPropertyHandle;
class iggWidgetSimpleItemizedSelectionBox;


class iggFrameDataVariableList : public iggFrameFlip
{

public:

	iggFrameDataVariableList(const iString &title, const iggObjectHandle *dsh, iggFrame *parent, const iggPropertyHandle* eh = 0);
	iggFrameDataVariableList(const iString &title, const iggObjectHandle *dsh, const iggPropertyHandle& ph, iggFrame *parent, const iggPropertyHandle* eh = 0);
	iggFrameDataVariableList(const iString &title, const iggDataTypeHandle *dth, const iggPropertyHandle& ph, iggFrame *parent, const iggPropertyHandle* eh = 0);
	virtual ~iggFrameDataVariableList();

	//
	//  Decorator functions
	//
	virtual void InsertItem(const iString &text);
	virtual void Complete();

	int GetIndex() const;

protected:

	void UpdateList();
	virtual void UpdateChildren();

	void Define(const iString &title, const iggObjectHandle *dsh, const iggDataTypeHandle *dth, const iggPropertyHandle *ph, iggFrame *parent, const iggPropertyHandle* eh);

	int mOffset;
	bool mCompleted;
	const iggObjectHandle *mDataSubjectHandle;
	const iggDataTypeHandle *mDataTypeHandle;
	const iggPropertyHandle *mEnablingHandle;
	iggFrame *mLayers[2];
};

#endif  // IGGFRAMEDATAVARIABLELIST_H

