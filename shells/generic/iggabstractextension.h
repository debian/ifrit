/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Generic abstract extension
//

#ifndef IGGABSTRACTEXTENSION_H
#define IGGABSTRACTEXTENSION_H


#include "iggframe.h"


#include "iimage.h"
#include "istring.h"

class iDataType;

class iggMainWindow;
class iggPageData;


//
//  Helper class
//
class iggExtensionFrameBook : public iggFrameBook
{

public:

	iggExtensionFrameBook(iggFrameBase *parent);

	void OpenPageByIndex(int index);
	void Detach();

	void Block(bool s);

protected:

	virtual void OnInt1Body(int i);

	bool mBlocked;
};


class iggAbstractExtension : public iggFrame
{

	friend class iggExtensionWindow;

public:

	virtual ~iggAbstractExtension();

	virtual void OpenBookPageByIndex(int index);
	virtual void SetTabMode(int m);

	inline const iImage* GetIcon() const { return mIcon; }
	inline const iString& GetTitle() const { return mTitle; }

	//
	//  Menu
	//
	virtual void PopulateFileMenu();
	virtual void PopulateLocalDialogMenu();
	virtual void PopulateGlobalDialogMenu();
	virtual void PopulateFileToolBar();
	virtual void PopulateShowToolBar();

	virtual bool OnMenuBody(int id, bool on);

	virtual void UpdateOnPick();
	virtual void UpdateParticleWidgets(const iImage *icon = 0);
	virtual void AddReloadingDataTypes(iggPageData *page);  // base data has been already added
	virtual const iImage* GetSpecialParticleIcon(const iDataType &type) const;

	virtual bool CanPrePopulateToolBar() const { return false; }

protected:

	iggAbstractExtension(iggMainWindow *mw, int id, const iImage *icon, const iString &title, bool withbook);

	iggFrame* CreateFrontPage(const iImage *image, const iString &line2 = "");
	void ConfigureDefaultOpenFileMenu(int id, const iString &name, const iString &file, const iImage *icon, const iDataType &type);

	void CompleteInitialization();
	virtual void CompleteInitializationBody();

	inline iggExtensionFrameBook* GetBook() const { return mBook; }

	virtual bool IsUsingData(const iDataType &type) const = 0;

private:

	const int mId;
	const iImage *mIcon;
	const iString mTitle;

	bool mWithBook;
	iggExtensionFrameBook *mBook;

	//
	//  Menu configuration parameters for default behaviour
	//
	struct MenuFileOpenEntry
	{
		int Id;
		iString Name;
		iString File;
		const iImage *Icon;
		const iDataType *Type;
		MenuFileOpenEntry(){ Id = -1; Icon = 0; Type = 0; }
		bool IsValid() const { return (Id>0 && Icon!=0 && Type!=0); }
	}
	mEntry;
};


//
//  Helper class - a creator class needed for the auto-build mechanism to work;
//  GUI extensions need the MainWindow class, so they cannot be created at
//  start-up.
//
class iggExtensionCreator
{

public:

	inline int GetId() const { return mId; }

	virtual iggAbstractExtension* Create(iggMainWindow *mw) const = 0;

protected:

	iggExtensionCreator(int id);

	const int mId;
};

//
//  Useful macros
//
#define iggExtensionDeclareMacro(_class_) \
	private: \
		_class_(iggMainWindow *mw, int id); \
		static const int mAutoInstall; \
		static const iggExtensionCreator* Creator(); \
		class ThisCreator : public iggExtensionCreator \
		{ \
		public: \
			ThisCreator(int id) : iggExtensionCreator(id){} \
			virtual iggAbstractExtension* Create(iggMainWindow *mw) const; \
		}

#define iggExtensionDefineMacro(_class_,_name_,_icon_,_book_) \
	const iggExtensionCreator* _class_::Creator() \
	{ \
		static iggExtensionCreator* self = new _class_::ThisCreator(IEXTENSION_##_name_); \
		return self; \
	} \
	iggAbstractExtension* _class_::ThisCreator::Create(iggMainWindow *mw) const { return new _class_(mw,mId); } \
	_class_::_class_(iggMainWindow *mw, int id) : iggAbstractExtension(mw,id,iggImageFactory::FindIcon(_icon_),#_name_,_book_){}

#define iggExtensionDeclareClassMacro(_prefix_) \
	class _prefix_##ggExtension : public iggAbstractExtension \
	{ \
		iggExtensionDeclareMacro(_prefix_##ggExtension); \
	protected: \
		void StandardInitialization(); \
		virtual void CompleteInitializationBody(); \
		virtual bool IsUsingData(const iDataType &type) const; \
	}

#define iggExtensionDefineClassMacro(_prefix_,_name_,_icon_,_book_,_wildcard_,_subject_) \
	iggExtensionDefineMacro(_prefix_##ggExtension,_name_,_icon_".png",_book_); \
	void _prefix_##ggExtension::StandardInitialization() \
	{ \
		this->CreateFrontPage(iggImageFactory::FindIcon(_icon_"large.png")); \
		this->ConfigureDefaultOpenFileMenu(1,"&"#_name_,#_name_" File ("_wildcard_")",iggImageFactory::FindIcon(_icon_"fileopen.png"),_subject_::DataType()); \
	} \
	bool _prefix_##ggExtension::IsUsingData(const iDataType &type) const \
	{ \
		return type.MatchesKeyword(_icon_); \
	}

#endif  // IGGEXTENSION_H

