/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggmainwindow.h"


#include "iboundingbox.h"
#include "icoredatasubjects.h"
#include "icrosssectionviewsubject.h"
#include "idata.h"
#include "idatareader.h"
#include "ierror.h"
#include "ifile.h"
#include "iparallelmanager.h"
#include "iparticleviewsubject.h"
#include "irendertool.h"
#include "iscript.h"
#include "isurfaceviewsubject.h"
#include "itensorfieldviewsubject.h"
#include "ivectorfieldviewsubject.h"
#include "iviewmodule.h"
#include "iviewobject.h"
#include "ivolumeviewsubject.h"
#include "iwriter.h"

#include "iggdialogauto.h"
#include "iggdialoghelp.h"
#include "iggdialogimagecomposer.h"
#include "iggdialogpaletteeditor.h"
#include "iggdialogparallelcontroller.h"
#include "iggdialogpickerwindow.h"
#include "iggextensionwindow.h"
#include "iggframetopparent.h"
#include "igghandle.h"
#include "iggimagefactory.h"
#include "iggmenuhelper.h"
#include "iggpageobject.h"
#include "iggpageview.h"
#include "iggscriptwindow.h"
#include "iggshell.h"
#include "iggwidgetpropertycontrol.h"
#include "iggwidgetpropertycontrolslider.h"
#include "iggwidgetrendermodebutton.h"

#include "ibgmenuwindowsubject.h"
#include "ibgwindowhelper.h"

//
//  templates
//
#include "iarray.tlh"
#include "iggmenuhelper.tlh"


using namespace iParameter;


namespace iggMainWindow_Private
{
	//
	//  Menu ids
	//
	namespace Menu
	{
		namespace File
		{
			const int OpenScalars = 1;
			const int OpenParticles = 2;
			const int OpenVectors = 3;
			const int OpenTensors = 4;
			const int ExportScene = 80;
			const int SaveState = 90;
			const int SaveStateAs = 91;
			const int RestoreState = 92;
			const int Exit = 99;
			const int Max = 100;
		};

		namespace Dialog
		{
			const int PaletteEditor = 102;
			const int PickerWindow = 103;
			const int ParallelController = 105;
			const int FileSetExplorer = 106;
			const int DataExplorer = 107;
			const int ImageComposer = 108;
			const int ScriptWindow = 109;
			const int PerformanceMeter = 110;
			const int AutoBlock = 120;
			const int AutoBegin = 121;
			const int AutoEnd = 200;
			const int Max = 200;
		};

		namespace Style
		{
			const int DockWindow = 201;
			const int InteractorHelp = 202;
			const int ToolTips = 203;
			const int TabText = 211;
			const int TabIcon = 212;
			const int TabBoth = 213;
			const int IsIdiosyncratic = 225;
			const int AutoRender = 226;
			const int Theme = 230;
			const int RotateBook = 240;
			const int Max = 300;
		};

		namespace Option
		{
			const int PointAntialiasing  = 301;
			const int LineAntialiasing   = 302;
			const int PolyAntialiasing   = 303;
			const int TrueRenderingOff   = 304;
			const int TrueRenderingLow   = 305;
			const int TrueRenderingHigh  = 306;
			const int TrueRenderingExact = 307;
			const int IfritBox = 311;
			const int ClassicBox = 312;
			const int HairBox = 313;
			const int AxesBox = 314;
			const int FontTypeVector = 322;
			const int FontTypeArial = 323;
			const int FontTypeCourier = 324;
			const int FontTypeTimes = 325;
			const int FontSizem5 = 331;
			const int FontSizem4 = 332;
			const int FontSizem3 = 333;
			const int FontSizem2 = 334;
			const int FontSizem1 = 335;
			const int FontSizec0 = 336;
			const int FontSizep1 = 337;
			const int FontSizep2 = 338;
			const int FontSizep3 = 339;
			const int FontSizep4 = 340;
			const int FontSizep5 = 341;
			const int ImageFormatPNG = 351;
			const int ImageFormatJPG = 352;
			const int ImageFormatPPM = 353;
			const int ImageFormatBMP = 354;
			const int ImageFormatTIF = 355;
			const int ImageFormatEPS = 356;
			const int ImageZoom001 = 361;
			const int ImageZoom002 = 362;
			const int ImageZoom003 = 363;
			const int ImageZoom004 = 364;
			const int ImageZoom005 = 365;
			const int ImageZoom006 = 366;
			const int ImageZoom008 = 367;
			const int ImageZoom010 = 368;
			const int ImageZoom015 = 369;
			const int ImageZoom020 = 370;
			const int ImageZoom030 = 371;
			const int ImageZoom040 = 372;
			const int ImageZoom050 = 373;
			const int ImageZoom060 = 374;
			const int ImageZoom080 = 375;
			const int ImageZoom100 = 376;
			const int PostScriptPaperFormatA0 = 401;
			const int PostScriptPaperFormatA1 = 402;
			const int PostScriptPaperFormatA2 = 403;
			const int PostScriptPaperFormatA3 = 404;
			const int PostScriptPaperFormatA4 = 405;
			const int PostScriptPaperFormatA5 = 406;
			const int PostScriptPaperFormatA6 = 407;
			const int PostScriptPaperFormatA7 = 408;
			const int PostScriptPaperFormatA8 = 409;
			const int PostScriptPaperFormatL1 = 410;
			const int PostScriptPaperFormatL2 = 411;
			const int PostScriptOrientationPortrait = 421;
			const int PostScriptOrientationLandscape = 422;
			const int BackgroundImageFixedAspect = 432;
			const int SettingsGlobal = 433;
			const int RefreshGUI = 434;
			const int Max = 500;
		};

		namespace Help
		{
			const int Contents = 501;
			const int About = 502;
			const int Max = 599;
		};
	};
	
	namespace ToolBar
	{
		const int OpenWindowsPage = 801;
		const int MinimizeWindows = 802;
		const int MaximizeWindows = 803;
		const int MoveAllTogether = 804;

		const int ShowSurface = 901;
		const int ShowCrossSection = 902;
		const int ShowVolume = 903;
		const int ShowParticles = 904;
		const int ShowVectorField = 905;
		const int ShowTensorField = 906;
		const int Max = 999;
	};
};


using namespace iggMainWindow_Private;


//
//  **************************************************************************************
//
//      MENU INTERACTION
//
//  **************************************************************************************
//
void iggMainWindow::BuildMenus()
{
	//
	//  Cache for simpler code
	//
	iggObjectHandle *vmh = this->GetShell()->GetViewModuleHandle();

	//
	//  1. File menu
	//
	this->GetSubject()->BeginMenu("&File",false);
	{
		iggMenuHelper<iViewModule,int> mh(vmh,&iViewModule::GetCloneOfWindow,-1);

		this->GetSubject()->AddMenuItem(Menu::File::OpenScalars,"Open &Scalar data file",iggImageFactory::FindIcon("fileopenmesh.png"),"",false,false,&mh);
		this->GetSubject()->AddMenuItem(Menu::File::OpenParticles,"Open &Particle data file",iggImageFactory::FindIcon("fileopenpart.png"),"",false,false,&mh);
		this->GetSubject()->AddMenuItem(Menu::File::OpenVectors,"Open &Vector data file",iggImageFactory::FindIcon("fileopenvect.png"),"",false,false,&mh);
		this->GetSubject()->AddMenuItem(Menu::File::OpenTensors,"Open &Tensor data file",iggImageFactory::FindIcon("fileopentens.png"),"",false,false,&mh);

		mExtensionWindow->PopulateFileMenu();

		this->GetSubject()->AddMenuSeparator();

		this->GetSubject()->AddMenuItem(Menu::File::ExportScene,"Export scene...",0,"",false,false);

		this->GetSubject()->AddMenuSeparator();

		this->GetSubject()->AddMenuItem(Menu::File::SaveState,"Save state",0,"",false,false);
		this->GetSubject()->AddMenuItem(Menu::File::SaveStateAs,"Save state as...",0,"",false,false);
		this->GetSubject()->AddMenuItem(Menu::File::RestoreState,"Restore state from...",0,"",false,false);

		this->GetSubject()->AddMenuSeparator();
		this->GetSubject()->AddMenuItem(Menu::File::Exit,"&Exit",0,"",false,false);
	}
	this->GetSubject()->EndMenu();

	//
	//  2. Dialog menu
	//
	this->GetSubject()->BeginMenu("&Tools",false);
	{
		this->GetSubject()->AddMenuItem(Menu::Dialog::PaletteEditor,"Palette &Editor",iggImageFactory::FindIcon("paled.png"),"Ctrl+E",false,false);
		this->GetSubject()->AddMenuItem(Menu::Dialog::PickerWindow,"&Picker Window",iggImageFactory::FindIcon("picks.png"),"Ctrl+P",false,false);
		iggMenuHelper<iDataReader,bool> mh(this->GetShell()->GetDataReaderHandle(),&iDataReader::IsSet,true);
		this->GetSubject()->AddMenuItem(Menu::Dialog::FileSetExplorer,"&File Set Explorer",iggImageFactory::FindIcon("setexp.png"),"Ctrl+F",false,false,&mh);
		mExtensionWindow->PopulateLocalDialogMenu();
	
		this->GetSubject()->BeginMenu("Automatic Dialogs",false);
		{
			this->GetSubject()->AddMenuItem(Menu::Dialog::AutoBlock,"Do not launch automatically",0,"",true,iggDialogAuto::mBlockShowing);
			int i;
			iString ws;
			for(i=0; i<mAutoDialogList.Size() && i<Menu::Dialog::AutoEnd-Menu::Dialog::AutoBegin; i++)
			{
				this->GetSubject()->AddMenuItem(Menu::Dialog::AutoBegin+i,"  "+mAutoDialogList[i]->GetTitle(),0,"",false,false);
			}
		}
		this->GetSubject()->EndMenu();

		this->GetSubject()->AddMenuSeparator();
		this->GetSubject()->AddMenuItem(Menu::Dialog::PerformanceMeter,"Performance &Meter",iggImageFactory::FindIcon("perf.png"),"Ctrl+M",false,false);
		if(this->GetSubject()->GetShell()->GetParallelManager()->GetMaxNumberOfProcessors() > 1) this->GetSubject()->AddMenuItem(Menu::Dialog::ParallelController,"Parallel &Controller",iggImageFactory::FindIcon("parallel.png"),"Ctrl+C",false,false);
		this->GetSubject()->AddMenuItem(Menu::Dialog::DataExplorer,"&Data Explorer",iggImageFactory::FindIcon("dataexp.png"),"Ctrl+D",false,false);
		this->GetSubject()->AddMenuItem(Menu::Dialog::ImageComposer,"&Image Composer",iggImageFactory::FindIcon("imcomp.png"),"Ctrl+I",false,false);

		if(!this->GetShell()->IsThreaded())
		{
			this->GetSubject()->AddMenuItem(Menu::Dialog::ScriptWindow,"&Script Window",iggImageFactory::FindIcon("comline.png"),"Ctrl+S",false,false);
		}

		mExtensionWindow->PopulateGlobalDialogMenu();
	}
	this->GetSubject()->EndMenu();

	//
	//  3. Style menu
	//
	this->GetSubject()->BeginMenu("&Style",false);
	{
		this->GetSubject()->AddMenuItem(Menu::Style::InteractorHelp,"Show &interactor help",0,"",true,mInteractorHelp);
		this->GetSubject()->AddMenuItem(Menu::Style::ToolTips,"Show t&ooltips",0,"",true,mToolTips);

		this->GetSubject()->BeginMenu("&Tab style",true);
		{
			this->GetSubject()->AddMenuItem(Menu::Style::TabText,"&Text only",0,"",true,mBook->GetTabMode()==BookTab::TitleOnly);
			this->GetSubject()->AddMenuItem(Menu::Style::TabIcon,"&Icon only",0,"",true,mBook->GetTabMode()==BookTab::ImageOnly);
			this->GetSubject()->AddMenuItem(Menu::Style::TabBoth,"Icon &and text",0,"",true,mBook->GetTabMode()==BookTab::TitleAndImage);
		}
		this->GetSubject()->EndMenu();

		this->GetSubject()->AddMenuSeparator();
		this->GetSubject()->BeginMenu("&Advanced",false);
		{
			this->GetSubject()->AddMenuItem(Menu::Style::DockWindow,"&Dock windows",0,"",true,mDocked);
			this->GetSubject()->AddMenuItem(Menu::Style::IsIdiosyncratic,"&Ask for confirmations",0,"",true,mIsIdiosyncratic);

			iggMenuHelper<iShell,bool> h(this->GetShell()->GetShellHandle(),&iShell::GetAutoRender,this->GetShell()->GetAutoRender());
			this->GetSubject()->AddMenuItem(Menu::Style::AutoRender,"&Render automatically",0,"",true,this->GetShell()->GetAutoRender(),0,&h);
		}
		this->GetSubject()->EndMenu();
		this->GetSubject()->BeginMenu("The&me",true);
		{
			int i;
			for(i=0; i<mThemeList.Size(); i++) this->GetSubject()->AddMenuItem(Menu::Style::Theme+i,mThemeList[i],0,"",true,false);
		}
		this->GetSubject()->EndMenu();

		this->GetSubject()->AddMenuItem(Menu::Style::RotateBook,"&Rotate the book widget",0,"",false,false);
	}
	this->GetSubject()->EndMenu();

	//
	//  4. Options menu
	//
	this->GetSubject()->BeginMenu("&Options",false);
	{
		this->GetSubject()->BeginMenu("&OpenGL options",false);
		{
			this->GetSubject()->BeginMenu("&Antialiasing",false);
			{
				iggMenuHelper<iViewModule,bool> h1(vmh,&iViewModule::GetPointAntialising,true);
				iggMenuHelper<iViewModule,bool> h2(vmh,&iViewModule::GetLineAntialising,true);
				iggMenuHelper<iViewModule,bool> h3(vmh,&iViewModule::GetPolyAntialising,true);
				this->GetSubject()->AddMenuItem(Menu::Option::PointAntialiasing,"&Points",0,"",true,false,0,&h1);
				this->GetSubject()->AddMenuItem(Menu::Option::LineAntialiasing,"&Lines",0,"",true,true,0,&h2);
				this->GetSubject()->AddMenuItem(Menu::Option::PolyAntialiasing,"Po&lygons",0,"",true,true,0,&h3);
			}
			this->GetSubject()->EndMenu();
			this->GetSubject()->BeginMenu("&True rendering",true);
			{
				iggMenuHelper<iViewModule,int> h0(vmh,&iViewModule::GetTrueRendering,0);
				iggMenuHelper<iViewModule,int> h1(vmh,&iViewModule::GetTrueRendering,1);
				iggMenuHelper<iViewModule,int> h2(vmh,&iViewModule::GetTrueRendering,2);
				iggMenuHelper<iViewModule,int> h3(vmh,&iViewModule::GetTrueRendering,3);
				this->GetSubject()->AddMenuItem(Menu::Option::TrueRenderingOff,"&Off",0,"",true,false,0,&h0);
				this->GetSubject()->AddMenuItem(Menu::Option::TrueRenderingLow,"&Low quality",0,"",true,false,0,&h1);
				this->GetSubject()->AddMenuItem(Menu::Option::TrueRenderingHigh,"&High quality",0,"",true,false,0,&h2);
				this->GetSubject()->AddMenuItem(Menu::Option::TrueRenderingExact,"&Exact (very slow)",0,"",true,false,0,&h3);
			}
			this->GetSubject()->EndMenu();
		}
		this->GetSubject()->EndMenu();

		this->GetSubject()->BeginMenu("&Bounding box style",true);
		{
			iggObjectHandle *bbh = this->GetShell()->GetObjectHandle("BoundingBox");

			iggMenuHelper<iBoundingBox,int> h1(bbh,&iBoundingBox::GetType,BoundingBoxType::Default);
			iggMenuHelper<iBoundingBox,int> h2(bbh,&iBoundingBox::GetType,BoundingBoxType::Classic);
			iggMenuHelper<iBoundingBox,int> h3(bbh,&iBoundingBox::GetType,BoundingBoxType::HairThin);
			iggMenuHelper<iBoundingBox,int> h4(bbh,&iBoundingBox::GetType,BoundingBoxType::Axes);
			this->GetSubject()->AddMenuItem(Menu::Option::IfritBox,"&Ifrit style",0,"",true,false,0,&h1);
			this->GetSubject()->AddMenuItem(Menu::Option::ClassicBox,"&Classic style",0,"",true,false,0,&h2);
			this->GetSubject()->AddMenuItem(Menu::Option::HairBox,"&Hair-thin style",0,"",true,false,0,&h3);
			this->GetSubject()->AddMenuItem(Menu::Option::AxesBox,"&Axes style",0,"",true,false,0,&h4);
		}
		this->GetSubject()->EndMenu();

		this->GetSubject()->BeginMenu("&Fonts",false);
		{
			this->GetSubject()->BeginMenu("Font &type",true);
			{
				iggMenuHelper<iViewModule,int> h1(vmh,&iViewModule::GetFontType,TextType::Arial);
				iggMenuHelper<iViewModule,int> h2(vmh,&iViewModule::GetFontType,TextType::Courier);
				iggMenuHelper<iViewModule,int> h3(vmh,&iViewModule::GetFontType,TextType::Times);
				this->GetSubject()->AddMenuItem(Menu::Option::FontTypeArial,"&Arial",0,"",true,false,0,&h1);
				this->GetSubject()->AddMenuItem(Menu::Option::FontTypeCourier,"&Courier",0,"",true,false,0,&h2);
				this->GetSubject()->AddMenuItem(Menu::Option::FontTypeTimes,"&Times",0,"",true,false,0,&h3);
			}
			this->GetSubject()->EndMenu();

			this->GetSubject()->BeginMenu("Font &size",true);
			{
				iggMenuHelper<iViewModule,int> hm5(vmh,&iViewModule::GetFontScale,-5);
				iggMenuHelper<iViewModule,int> hm4(vmh,&iViewModule::GetFontScale,-4);
				iggMenuHelper<iViewModule,int> hm3(vmh,&iViewModule::GetFontScale,-3);
				iggMenuHelper<iViewModule,int> hm2(vmh,&iViewModule::GetFontScale,-2);
				iggMenuHelper<iViewModule,int> hm1(vmh,&iViewModule::GetFontScale,-1);
				iggMenuHelper<iViewModule,int> hc0(vmh,&iViewModule::GetFontScale,0);
				iggMenuHelper<iViewModule,int> hp1(vmh,&iViewModule::GetFontScale,+1);
				iggMenuHelper<iViewModule,int> hp2(vmh,&iViewModule::GetFontScale,+2);
				iggMenuHelper<iViewModule,int> hp3(vmh,&iViewModule::GetFontScale,+3);
				iggMenuHelper<iViewModule,int> hp4(vmh,&iViewModule::GetFontScale,+4);
				iggMenuHelper<iViewModule,int> hp5(vmh,&iViewModule::GetFontScale,+5);

				this->GetSubject()->AddMenuItem(Menu::Option::FontSizem5," 20%",0,"",true,false,0,&hm5);
				this->GetSubject()->AddMenuItem(Menu::Option::FontSizem4," 30%",0,"",true,false,0,&hm4);
				this->GetSubject()->AddMenuItem(Menu::Option::FontSizem3," 40%",0,"",true,false,0,&hm3);
				this->GetSubject()->AddMenuItem(Menu::Option::FontSizem2," 60%",0,"",true,false,0,&hm2);
				this->GetSubject()->AddMenuItem(Menu::Option::FontSizem1," 75%",0,"",true,false,0,&hm1);
				this->GetSubject()->AddMenuItem(Menu::Option::FontSizec0,"100%",0,"",true,false,0,&hc0);
				this->GetSubject()->AddMenuItem(Menu::Option::FontSizep1,"130%",0,"",true,false,0,&hp1);
				this->GetSubject()->AddMenuItem(Menu::Option::FontSizep2,"180%",0,"",true,false,0,&hp2);
				this->GetSubject()->AddMenuItem(Menu::Option::FontSizep3,"250%",0,"",true,false,0,&hp3);
				this->GetSubject()->AddMenuItem(Menu::Option::FontSizep4,"330%",0,"",true,false,0,&hp4);
				this->GetSubject()->AddMenuItem(Menu::Option::FontSizep5,"450%",0,"",true,false,0,&hp5);
			}
			this->GetSubject()->EndMenu();
		}
		this->GetSubject()->EndMenu();

		this->GetSubject()->BeginMenu("&Images",false);
		{
			this->GetSubject()->BeginMenu("Image &format",true);
			{
				iggObjectHandle *iwh = this->GetShell()->GetObjectHandle("ImageWriter");

				iggMenuHelper<iWriter,int> h1(iwh,&iWriter::GetImageFormat,ImageFormat::PNG);
				iggMenuHelper<iWriter,int> h2(iwh,&iWriter::GetImageFormat,ImageFormat::JPG);
				iggMenuHelper<iWriter,int> h3(iwh,&iWriter::GetImageFormat,ImageFormat::PNM);
				iggMenuHelper<iWriter,int> h4(iwh,&iWriter::GetImageFormat,ImageFormat::BMP);
				iggMenuHelper<iWriter,int> h5(iwh,&iWriter::GetImageFormat,ImageFormat::TIF);
				iggMenuHelper<iWriter,int> h6(iwh,&iWriter::GetImageFormat,ImageFormat::EPS);

				this->GetSubject()->AddMenuItem(Menu::Option::ImageFormatPNG,"PNG (&Portable Network Graphics)",0,"",true,false,0,&h1);
				this->GetSubject()->AddMenuItem(Menu::Option::ImageFormatJPG,"JPG (&Joint Photographic Experts Group)",0,"",true,false,0,&h2);
				this->GetSubject()->AddMenuItem(Menu::Option::ImageFormatPPM,"PPM (Portable Pi&xmap)",0,"",true,false,0,&h3);
				this->GetSubject()->AddMenuItem(Menu::Option::ImageFormatBMP,"BMP (Window &Bitmap)",0,"",true,false,0,&h4);
				this->GetSubject()->AddMenuItem(Menu::Option::ImageFormatTIF,"TIF (&Tag Image File Format)",0,"",true,false,0,&h5);
				this->GetSubject()->AddMenuItem(Menu::Option::ImageFormatEPS,"EPS (&Encapsulated PostScript)",0,"",true,false,0,&h6);

				this->GetSubject()->BeginMenu("  Postscript paper format",true);
				{
					iggMenuHelper<iWriter,int> ha(iwh,&iWriter::GetPostScriptPaperFormat,0);
					iggMenuHelper<iWriter,int> hb(iwh,&iWriter::GetPostScriptPaperFormat,1);
					iggMenuHelper<iWriter,int> hc(iwh,&iWriter::GetPostScriptPaperFormat,2);
					iggMenuHelper<iWriter,int> hd(iwh,&iWriter::GetPostScriptPaperFormat,3);
					iggMenuHelper<iWriter,int> he(iwh,&iWriter::GetPostScriptPaperFormat,4);
					iggMenuHelper<iWriter,int> hf(iwh,&iWriter::GetPostScriptPaperFormat,5);
					iggMenuHelper<iWriter,int> hg(iwh,&iWriter::GetPostScriptPaperFormat,6);
					iggMenuHelper<iWriter,int> hh(iwh,&iWriter::GetPostScriptPaperFormat,7);
					iggMenuHelper<iWriter,int> hi(iwh,&iWriter::GetPostScriptPaperFormat,8);
					iggMenuHelper<iWriter,int> hj(iwh,&iWriter::GetPostScriptPaperFormat,9);
					iggMenuHelper<iWriter,int> hk(iwh,&iWriter::GetPostScriptPaperFormat,10);

					this->GetSubject()->AddMenuItem(Menu::Option::PostScriptPaperFormatA0,"A0",0,"",true,false,0,&ha);
					this->GetSubject()->AddMenuItem(Menu::Option::PostScriptPaperFormatA1,"A1",0,"",true,false,0,&hb);
					this->GetSubject()->AddMenuItem(Menu::Option::PostScriptPaperFormatA2,"A2",0,"",true,false,0,&hc);
					this->GetSubject()->AddMenuItem(Menu::Option::PostScriptPaperFormatA3,"A3",0,"",true,false,0,&hd);
					this->GetSubject()->AddMenuItem(Menu::Option::PostScriptPaperFormatA4,"A4",0,"",true,false,0,&he);
					this->GetSubject()->AddMenuItem(Menu::Option::PostScriptPaperFormatA5,"A5",0,"",true,false,0,&hf);
					this->GetSubject()->AddMenuItem(Menu::Option::PostScriptPaperFormatA6,"A6",0,"",true,false,0,&hg);
					this->GetSubject()->AddMenuItem(Menu::Option::PostScriptPaperFormatA7,"A7",0,"",true,false,0,&hh);
					this->GetSubject()->AddMenuItem(Menu::Option::PostScriptPaperFormatA8,"A8",0,"",true,false,0,&hi);
					this->GetSubject()->AddMenuItem(Menu::Option::PostScriptPaperFormatL1,"Letter",0,"",true,false,0,&hj);
					this->GetSubject()->AddMenuItem(Menu::Option::PostScriptPaperFormatL2,"10x17",0,"",true,false,0,&hk);
				}
				this->GetSubject()->EndMenu();

				this->GetSubject()->BeginMenu("  Postscript orientation",true);
				{
					iggMenuHelper<iWriter,int> hp(iwh,&iWriter::GetPostScriptOrientation,0);
					iggMenuHelper<iWriter,int> hl(iwh,&iWriter::GetPostScriptOrientation,1);

					this->GetSubject()->AddMenuItem(Menu::Option::PostScriptOrientationPortrait,"Portrait",0,"",true,false,0,&hp);
					this->GetSubject()->AddMenuItem(Menu::Option::PostScriptOrientationLandscape,"Landscape",0,"",true,false,0,&hl);
				}
				this->GetSubject()->EndMenu();
			}
			this->GetSubject()->EndMenu();

			this->GetSubject()->BeginMenu("Image &zoom",true);
			{
				iggMenuHelper<iViewModule,int> ha(vmh,&iViewModule::GetImageMagnification,1);
				iggMenuHelper<iViewModule,int> hb(vmh,&iViewModule::GetImageMagnification,2);
				iggMenuHelper<iViewModule,int> hc(vmh,&iViewModule::GetImageMagnification,3);
				iggMenuHelper<iViewModule,int> hd(vmh,&iViewModule::GetImageMagnification,4);
				iggMenuHelper<iViewModule,int> he(vmh,&iViewModule::GetImageMagnification,5);
				iggMenuHelper<iViewModule,int> hf(vmh,&iViewModule::GetImageMagnification,6);
				iggMenuHelper<iViewModule,int> hg(vmh,&iViewModule::GetImageMagnification,8);
				iggMenuHelper<iViewModule,int> hh(vmh,&iViewModule::GetImageMagnification,10);
				iggMenuHelper<iViewModule,int> hi(vmh,&iViewModule::GetImageMagnification,15);
				iggMenuHelper<iViewModule,int> hj(vmh,&iViewModule::GetImageMagnification,20);
				iggMenuHelper<iViewModule,int> hk(vmh,&iViewModule::GetImageMagnification,30);
				iggMenuHelper<iViewModule,int> hl(vmh,&iViewModule::GetImageMagnification,40);
				iggMenuHelper<iViewModule,int> hm(vmh,&iViewModule::GetImageMagnification,50);
				iggMenuHelper<iViewModule,int> hn(vmh,&iViewModule::GetImageMagnification,60);
				iggMenuHelper<iViewModule,int> ho(vmh,&iViewModule::GetImageMagnification,80);
				iggMenuHelper<iViewModule,int> hp(vmh,&iViewModule::GetImageMagnification,100);

				this->GetSubject()->AddMenuItem(Menu::Option::ImageZoom001,"x 1",0,"",true,false,0,&ha);
				this->GetSubject()->AddMenuItem(Menu::Option::ImageZoom002,"x 2",0,"",true,false,0,&hb);
				this->GetSubject()->AddMenuItem(Menu::Option::ImageZoom003,"x 3",0,"",true,false,0,&hc);
				this->GetSubject()->AddMenuItem(Menu::Option::ImageZoom004,"x 4",0,"",true,false,0,&hd);
				this->GetSubject()->AddMenuItem(Menu::Option::ImageZoom005,"x 5",0,"",true,false,0,&he);
				this->GetSubject()->AddMenuItem(Menu::Option::ImageZoom006,"x 6",0,"",true,false,0,&hf);
				this->GetSubject()->AddMenuItem(Menu::Option::ImageZoom008,"x 8",0,"",true,false,0,&hg);
				this->GetSubject()->AddMenuItem(Menu::Option::ImageZoom010,"x 10",0,"",true,false,0,&hh);
				this->GetSubject()->AddMenuItem(Menu::Option::ImageZoom015,"x 15",0,"",true,false,0,&hi);
				this->GetSubject()->AddMenuItem(Menu::Option::ImageZoom020,"x 20",0,"",true,false,0,&hj);
				this->GetSubject()->AddMenuItem(Menu::Option::ImageZoom030,"x 30",0,"",true,false,0,&hk);
				this->GetSubject()->AddMenuItem(Menu::Option::ImageZoom040,"x 40",0,"",true,false,0,&hl);
				this->GetSubject()->AddMenuItem(Menu::Option::ImageZoom050,"x 50",0,"",true,false,0,&hm);
				this->GetSubject()->AddMenuItem(Menu::Option::ImageZoom060,"x 60",0,"",true,false,0,&hn);
				this->GetSubject()->AddMenuItem(Menu::Option::ImageZoom080,"x 80",0,"",true,false,0,&ho);
				this->GetSubject()->AddMenuItem(Menu::Option::ImageZoom100,"x 100",0,"",true,false,0,&hp);
			}
			this->GetSubject()->EndMenu();
		}
		this->GetSubject()->EndMenu();

		iggMenuHelper<iViewModule,bool> mh(vmh,&iViewModule::GetBackgroundImageFixedAspect,true);
		this->GetSubject()->AddMenuItem(Menu::Option::BackgroundImageFixedAspect,"&Preserve aspect ratio of background images",0,"",true,true,0,&mh);
		
		this->GetSubject()->AddMenuItem(Menu::Option::SettingsGlobal,"Options apply to all windows",0,"",true,mOptionsAreGlobal);
		this->GetSubject()->AddMenuItem(Menu::Option::RefreshGUI,"Check GUI",0,"",false,false);
	}
	this->GetSubject()->EndMenu();
	//
	//  5. Help menu
	//
	this->GetSubject()->BeginMenu("&Help",false);
	{
		this->GetSubject()->AddMenuItem(Menu::Help::Contents,"Contents",0,"F1",false,false);
#if defined(I_DEBUG)
		this->GetSubject()->AddMenuSeparator();
		this->GetSubject()->AddMenuItem(Menu::Help::Max,"Debug Helper",0,"",false,false);
#endif
		this->GetSubject()->AddMenuSeparator();
		this->GetSubject()->AddMenuItem(Menu::Help::About,"About IFrIT",0,"",false,false);
	}
	this->GetSubject()->EndMenu();
	this->GetSubject()->CompleteMenu();

	//
	//  Build a tool bar
	//
	if(mAllowPrePopulateToolBar)
	{
		mExtensionWindow->PrePopulateFileToolBar();
		this->GetSubject()->AddToolBarSeparator();
	}
	this->GetSubject()->AddToolBarButton(Menu::File::OpenScalars,"Open Scalar data file",0,false);
	this->GetSubject()->AddToolBarButton(Menu::File::OpenParticles,"Open Particle data file",0,false);
	this->GetSubject()->AddToolBarButton(Menu::File::OpenVectors,"Open Vector data file",0,false);
	this->GetSubject()->AddToolBarButton(Menu::File::OpenTensors,"Open Tensor data file",0,false);
	mExtensionWindow->PopulateFileToolBar(mAllowPrePopulateToolBar);
	
	this->GetSubject()->AddToolBarSeparator();
	
	iggMenuHelper<iSurfaceViewSubject,bool> d1(mPages[0]->GetSubjectHandle(),&iSurfaceViewSubject::IsThereData,true);
	iggMenuHelper<iSurfaceViewSubject,bool> v1(mPages[0]->GetSubjectHandle(),&iSurfaceViewSubject::IsVisible,true);
	iggMenuHelper<iCrossSectionViewSubject,bool> d2(mPages[1]->GetSubjectHandle(),&iCrossSectionViewSubject::IsThereData,true);
	iggMenuHelper<iCrossSectionViewSubject,bool> v2(mPages[1]->GetSubjectHandle(),&iCrossSectionViewSubject::IsVisible,true);
	iggMenuHelper<iVolumeViewSubject,bool> d3(mPages[2]->GetSubjectHandle(),&iVolumeViewSubject::IsThereData,true);
	iggMenuHelper<iVolumeViewSubject,bool> v3(mPages[2]->GetSubjectHandle(),&iVolumeViewSubject::IsVisible,true);
	iggMenuHelper<iParticleViewSubject,bool> d4(mPages[3]->GetSubjectHandle(),&iParticleViewSubject::IsThereData,true);
	iggMenuHelper<iParticleViewSubject,bool> v4(mPages[3]->GetSubjectHandle(),&iParticleViewSubject::IsVisible,true);
	iggMenuHelper<iVectorFieldViewSubject,bool> d5(mPages[4]->GetSubjectHandle(),&iVectorFieldViewSubject::IsThereData,true);
	iggMenuHelper<iVectorFieldViewSubject,bool> v5(mPages[4]->GetSubjectHandle(),&iVectorFieldViewSubject::IsVisible,true);
	iggMenuHelper<iTensorFieldViewSubject,bool> d6(mPages[5]->GetSubjectHandle(),&iTensorFieldViewSubject::IsThereData,true);
	iggMenuHelper<iTensorFieldViewSubject,bool> v6(mPages[5]->GetSubjectHandle(),&iTensorFieldViewSubject::IsVisible,true);

	this->GetSubject()->AddToolBarButton(ToolBar::ShowSurface,"Show surface",iggImageFactory::FindIcon("surf.png"),true,&d1,&v1);
	this->GetSubject()->AddToolBarButton(ToolBar::ShowCrossSection,"Show cross section",iggImageFactory::FindIcon("xsec.png"),true,&d2,&v2);
	this->GetSubject()->AddToolBarButton(ToolBar::ShowVolume,"Show volume",iggImageFactory::FindIcon("volv.png"),true,&d3,&v3);
	this->GetSubject()->AddToolBarButton(ToolBar::ShowParticles,"Show particles",iggImageFactory::FindIcon("part.png"),true,&d4,&v4);
	this->GetSubject()->AddToolBarButton(ToolBar::ShowVectorField,"Show vector field",iggImageFactory::FindIcon("vect.png"),true,&d5,&v5);
	this->GetSubject()->AddToolBarButton(ToolBar::ShowTensorField,"Show tensor field",iggImageFactory::FindIcon("tens.png"),true,&d6,&v6);
	mExtensionWindow->PopulateShowToolBar();

	this->GetSubject()->AddToolBarSeparator();

	this->GetSubject()->AddToolBarButton(ToolBar::OpenWindowsPage,"Switch to page with multiple windows controls",iggImageFactory::FindIcon("wins.png"),false);
	this->GetSubject()->AddToolBarButton(ToolBar::MinimizeWindows,"Minimize all windows",iggImageFactory::FindIcon("minimize.png"),false);
	this->GetSubject()->AddToolBarButton(ToolBar::MaximizeWindows,"Restore all windows to normal size",iggImageFactory::FindIcon("maximize.png"),false);
	this->GetSubject()->AddToolBarButton(ToolBar::MoveAllTogether,"Move all windows together",iggImageFactory::FindIcon("together.png"),true);
}


void iggMainWindow::OnMenuBody(int id, bool on)
{
	iString fname;

	if(id < 0)
	{
		IBUG_WARN("Invalid menu item id.");
	}

	if(id <= Menu::File::Max)
	{
		switch(id)
		{
		case Menu::File::OpenScalars:
			{
				iString ws = this->GetShell()->GetViewModule()->GetReader()->GetLastFileName(iCoreData::ScalarDataSubject::DataType());
				if(ws.IsEmpty()) ws = iCoreData::ScalarDataSubject::DataType().GetEnvironment(this->GetShell());
				fname = this->GetFileName("Choose a file",ws,"Scalar data files (*.bin *.txt)");
				if(!fname.IsEmpty())
				{
					this->GetShell()->GetViewModule()->GetReader()->LoadFile(iCoreData::ScalarDataSubject::DataType(),fname);
				}
				break;
			}
		case Menu::File::OpenVectors:
			{
				iString ws = this->GetShell()->GetViewModule()->GetReader()->GetLastFileName(iCoreData::VectorDataSubject::DataType());
				if(ws.IsEmpty()) ws = iCoreData::VectorDataSubject::DataType().GetEnvironment(this->GetShell());
				fname = this->GetFileName("Choose a file",ws,"Vector data files (*.bin *.txt)");
				if(!fname.IsEmpty())
				{
					this->GetShell()->GetViewModule()->GetReader()->LoadFile(iCoreData::VectorDataSubject::DataType(),fname);
				}
				break;
			}
		case Menu::File::OpenTensors:
			{
				iString ws = this->GetShell()->GetViewModule()->GetReader()->GetLastFileName(iCoreData::TensorDataSubject::DataType());
				if(ws.IsEmpty()) ws = iCoreData::TensorDataSubject::DataType().GetEnvironment(this->GetShell());
				fname = this->GetFileName("Choose a file",ws,"Tensor data files (*.bin *.txt)");
				if(!fname.IsEmpty())
				{
					this->GetShell()->GetViewModule()->GetReader()->LoadFile(iCoreData::TensorDataSubject::DataType(),fname);
				}
				break;
			}
		case Menu::File::OpenParticles:
			{
				iString ws = this->GetShell()->GetViewModule()->GetReader()->GetLastFileName(iCoreData::ParticleDataSubject::DataType());
				if(ws.IsEmpty()) ws = iCoreData::ParticleDataSubject::DataType().GetEnvironment(this->GetShell());
				fname = this->GetFileName("Choose a file",ws,"Particle data files (*.bin *.txt)");
				if(!fname.IsEmpty())
				{
					this->GetShell()->GetViewModule()->GetReader()->LoadFile(iCoreData::ParticleDataSubject::DataType(),fname);
				}
				break;
			}
		case Menu::File::ExportScene:
			{
				fname = this->GetFileName("Choose a file",mExportFileName,"OBJ file (*.obj);;X3D file (*.x3d);;VRML file (*.vrml)",false);
				if(!fname.IsEmpty())
				{
					if(this->GetShell()->GetViewModule()->CallExport(fname))
					{
						mExportFileName = fname;
					}
					else
					{
						this->GetShell()->PopupWindow("Unable to export the scene.",MessageType::Error);
					}
				}
				return;
			}
		case Menu::File::SaveState:
			{
				if(mStateFileName.IsEmpty()) mStateFileName = this->GetFileName("Choose a file",this->GetShell()->GetEnvironment(Environment::Base),"IFrIT state files (*.ini)",false);
				if(!mStateFileName.IsEmpty())
				{
					bool ok = true;
					if(iFile::IsReadable(mStateFileName))
					{
						if(this->GetShell()->PopupWindow("Do you want to overwrite the existing file?",MessageType::Warning,"Ok","Cancel") == 1) ok = false;
					}
					if(ok)
					{
						if(!this->GetShell()->SaveStateToFile(mStateFileName)) this->GetShell()->PopupWindow("Saving options failed for unknown reason.\n The current state will not be saved",MessageType::Error);
					}
				}
				return;
			}
		case Menu::File::SaveStateAs:
			{
				fname = this->GetFileName("Choose a file",mStateFileName,"IFrIT state files (*.ini)",false);
				if(!fname.IsEmpty())
				{
					if(this->GetShell()->SaveStateToFile(fname))
					{
						mStateFileName = fname;
					}
					else
					{
						this->GetShell()->PopupWindow("Saving options failed for unknown reason.\n The current state will not be saved.",MessageType::Error);
					}
				}
				return;
			}
		case Menu::File::RestoreState:
			{
				fname = this->GetFileName("Choose a file",mStateFileName,"IFrIT state files (*.ini)");
				if(!fname.IsEmpty())
				{
					if(this->GetShell()->RestoreStateFromFile(fname))
					{
						this->UpdateAll();
						this->Render(ViewModuleSelection::All);
					}
				}
				return;
			}
		case Menu::File::Exit:
			{
				if(this->IsExitAllowed()) this->Exit();
				break;
			}
		default:
			{
				IBUG_WARN("Invalid menu item id.");
			}
		}
		return;
	}

	if(id <= Menu::Dialog::Max)
	{
		switch(id)
		{
		case Menu::Dialog::PaletteEditor:
			{
				mDialogPaletteEditor->Show(true);
				break;
			}
		case Menu::Dialog::PickerWindow:
			{
				mDialogPickerWindow->Show(true);
				break;
			}
		case Menu::Dialog::ParallelController:
			{
				mDialogParallelController->Show(true);
				break;
			}
		case Menu::Dialog::PerformanceMeter:
			{
				mDialogPerformanceMeter->Show(true);
				break;
			}
		case Menu::Dialog::FileSetExplorer:
			{
				mDialogFileSetExplorer->Show(true);
				break;
			}
		case Menu::Dialog::DataExplorer:
			{
				mDialogDataExplorer->Show(true);
				break;
			}
		case Menu::Dialog::ImageComposer:
			{
				mDialogImageComposer->Show(true);
				break;
			}
		case Menu::Dialog::ScriptWindow:
			{
				if(this->GetShell() != 0) mScriptWindow->Show(true);
				break;
			}
		case Menu::Dialog::AutoBlock:
			{
				iggDialogAuto::mBlockShowing = on;
				break;
			}
		default:
			{
				if(id>=Menu::Dialog::AutoBegin && id<Menu::Dialog::AutoBegin+mAutoDialogList.Size())
				{
					mAutoDialogList[id-Menu::Dialog::AutoBegin]->ForceShow(true);
				}
				else IBUG_WARN("Invalid menu item id.");
			}
		}
		return;
	}

	if(id <= Menu::Style::Max)
	{
		switch(id)
		{
		case Menu::Style::DockWindow:
			{
				this->DockWindows(on,true);
				break;
			}
		case Menu::Style::InteractorHelp:
			{
				mInteractorHelp = on;
				break;
			}
		case Menu::Style::ToolTips:
			{
				this->SetToolTips(on);
				break;
			}
		case Menu::Style::TabText:
			{
				if(on) this->SetTabMode(BookTab::TitleOnly);
				break;
			}
		case Menu::Style::TabIcon:
			{
				if(on) this->SetTabMode(BookTab::ImageOnly);
				break;
			}
		case Menu::Style::TabBoth:
			{
				if(on) this->SetTabMode(BookTab::TitleAndImage);
				break;
			}
		case Menu::Style::IsIdiosyncratic:
			{
				mIsIdiosyncratic = on;
				break;
			}
		case Menu::Style::AutoRender:
			{
				this->GetShell()->SetAutoRender(on);
				mRenderButton->Show(!on);
				mRenderAllButton->Show(!on);
				break;
			}
		case Menu::Style::RotateBook:
			{
				mBook->SetOrientation(mBook->GetOrientation()+1);
				break;
			}
		default:
			{
				if(id>=Menu::Style::Theme && id<Menu::Style::Theme+mThemeList.Size())
				{
					if(on) this->SetTheme(mThemeList[id-Menu::Style::Theme]);
				}
				else IBUG_WARN("Invalid menu item id.");
			}
		}
		return;
	}

	if(id <= Menu::Option::Max)
	{
		int i;
		iArray<iViewModule*> arr;
		this->GetShell()->SelectViewModules(arr,mOptionsAreGlobal?ViewModuleSelection::All:ViewModuleSelection::One);

		switch(id)
		{
		case Menu::Option::TrueRenderingOff:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetTrueRendering(0);
				break;
			}
		case Menu::Option::TrueRenderingLow:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetTrueRendering(1);
				break;
			}
		case Menu::Option::TrueRenderingHigh:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetTrueRendering(2);
				break;
			}
		case Menu::Option::TrueRenderingExact:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetTrueRendering(3);
				break;
			}
		case Menu::Option::PointAntialiasing:
			{
				for(i=0; i<arr.Size(); i++) arr[i]->SetAntialiasing(0,on);
				break;
			}
		case Menu::Option::LineAntialiasing:
			{
				for(i=0; i<arr.Size(); i++) arr[i]->SetAntialiasing(1,on);
				break;
			}
		case Menu::Option::PolyAntialiasing:
			{
				for(i=0; i<arr.Size(); i++) arr[i]->SetAntialiasing(2,on);
				break;
			}
		case Menu::Option::IfritBox:
			{
				if(on)
				{
					for(i=0; i<arr.Size(); i++) arr[i]->GetBoundingBox()->SetType(BoundingBoxType::Default);
					if(mDialogAxesLabels != 0) mDialogAxesLabels->Show(false);
				}
				break;
			}
		case Menu::Option::ClassicBox:
			{
				if(on)
				{
					for(i=0; i<arr.Size(); i++) arr[i]->GetBoundingBox()->SetType(BoundingBoxType::Classic);
					if(mDialogAxesLabels != 0) mDialogAxesLabels->Show(false);
				}
				break;
			}
		case Menu::Option::HairBox:
			{
				if(on)
				{
					for(i=0; i<arr.Size(); i++) arr[i]->GetBoundingBox()->SetType(BoundingBoxType::HairThin);
					if(mDialogAxesLabels != 0) mDialogAxesLabels->Show(false);
				}
				break;
			}
		case Menu::Option::AxesBox:
			{
				if(on)
				{
					for(i=0; i<arr.Size(); i++) arr[i]->GetBoundingBox()->SetType(BoundingBoxType::Axes);
					if(mDialogAxesLabels != 0) mDialogAxesLabels->Show(true);
				}
				break;
			}
		case Menu::Option::FontTypeArial:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetFontType(TextType::Arial);
				break;
			}
		case Menu::Option::FontTypeCourier:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetFontType(TextType::Courier);
				break;
			}
		case Menu::Option::FontTypeTimes:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetFontType(TextType::Times);
				break;
			}
		case Menu::Option::FontSizem5:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetFontScale(-5);
				break;
			}
		case Menu::Option::FontSizem4:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetFontScale(-4);
				break;
			}
		case Menu::Option::FontSizem3:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetFontScale(-3);
				break;
			}
		case Menu::Option::FontSizem2:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetFontScale(-2);
				break;
			}
		case Menu::Option::FontSizem1:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetFontScale(-1);
				break;
			}
		case Menu::Option::FontSizec0:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetFontScale(0);
				break;
			}
		case Menu::Option::FontSizep1:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetFontScale(1);
				break;
			}
		case Menu::Option::FontSizep2:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetFontScale(2);
				break;
			}
		case Menu::Option::FontSizep3:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetFontScale(3);
				break;
			}
		case Menu::Option::FontSizep4:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetFontScale(4);
				break;
			}
		case Menu::Option::FontSizep5:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetFontScale(5);
				break;
			}
		case Menu::Option::ImageFormatPNG:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->GetImageWriter()->SetImageFormat(ImageFormat::PNG);
				break;
			}
		case Menu::Option::ImageFormatJPG:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->GetImageWriter()->SetImageFormat(ImageFormat::JPG);
				break;
			}
		case Menu::Option::ImageFormatPPM:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->GetImageWriter()->SetImageFormat(ImageFormat::PNM);
				break;
			}
		case Menu::Option::ImageFormatBMP:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->GetImageWriter()->SetImageFormat(ImageFormat::BMP);
				break;
			}
		case Menu::Option::ImageFormatTIF:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->GetImageWriter()->SetImageFormat(ImageFormat::TIF);
				break;
			}
		case Menu::Option::ImageFormatEPS:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->GetImageWriter()->SetImageFormat(ImageFormat::EPS);
				break;
			}
		case Menu::Option::PostScriptPaperFormatA0:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->GetImageWriter()->SetPostScriptPaperFormat(0);
				break;
			}
		case Menu::Option::PostScriptPaperFormatA1:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->GetImageWriter()->SetPostScriptPaperFormat(1);
				break;
			}
		case Menu::Option::PostScriptPaperFormatA2:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->GetImageWriter()->SetPostScriptPaperFormat(2);
				break;
			}
		case Menu::Option::PostScriptPaperFormatA3:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->GetImageWriter()->SetPostScriptPaperFormat(3);
				break;
			}
		case Menu::Option::PostScriptPaperFormatA4:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->GetImageWriter()->SetPostScriptPaperFormat(4);
				break;
			}
		case Menu::Option::PostScriptPaperFormatA5:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->GetImageWriter()->SetPostScriptPaperFormat(5);
				break;
			}
		case Menu::Option::PostScriptPaperFormatA6:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->GetImageWriter()->SetPostScriptPaperFormat(6);
				break;
			}
		case Menu::Option::PostScriptPaperFormatA7:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->GetImageWriter()->SetPostScriptPaperFormat(7);
				break;
			}
		case Menu::Option::PostScriptPaperFormatA8:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->GetImageWriter()->SetPostScriptPaperFormat(8);
				break;
			}
		case Menu::Option::PostScriptPaperFormatL1:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->GetImageWriter()->SetPostScriptPaperFormat(9);
				break;
			}
		case Menu::Option::PostScriptPaperFormatL2:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->GetImageWriter()->SetPostScriptPaperFormat(10);
				break;
			}
		case Menu::Option::PostScriptOrientationPortrait:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->GetImageWriter()->SetPostScriptOrientation(0);
				break;
			}
		case Menu::Option::PostScriptOrientationLandscape:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->GetImageWriter()->SetPostScriptOrientation(1);
				break;
			}
		case Menu::Option::ImageZoom001:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetImageMagnification(1);
				break;
			}
		case Menu::Option::ImageZoom002:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetImageMagnification(2);
				break;
			}
		case Menu::Option::ImageZoom003:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetImageMagnification(3);
				break;
			}
		case Menu::Option::ImageZoom004:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetImageMagnification(4);
				break;
			}
		case Menu::Option::ImageZoom005:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetImageMagnification(5);
				break;
			}
		case Menu::Option::ImageZoom006:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetImageMagnification(6);
				break;
			}
		case Menu::Option::ImageZoom008:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetImageMagnification(8);
				break;
			}
		case Menu::Option::ImageZoom010:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetImageMagnification(10);
				break;
			}
		case Menu::Option::ImageZoom015:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetImageMagnification(15);
				break;
			}
		case Menu::Option::ImageZoom020:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetImageMagnification(20);
				break;
			}
		case Menu::Option::ImageZoom030:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetImageMagnification(30);
				break;
			}
		case Menu::Option::ImageZoom040:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetImageMagnification(40);
				break;
			}
		case Menu::Option::ImageZoom050:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetImageMagnification(50);
				break;
			}
		case Menu::Option::ImageZoom060:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetImageMagnification(60);
				break;
			}
		case Menu::Option::ImageZoom080:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetImageMagnification(80);
				break;
			}
		case Menu::Option::ImageZoom100:
			{
				if(on) for(i=0; i<arr.Size(); i++) arr[i]->SetImageMagnification(100);
				break;
			}
		case Menu::Option::BackgroundImageFixedAspect:
			{
				for(i=0; i<arr.Size(); i++) arr[i]->SetBackgroundImageFixedAspect(on);
				break;
			}
		case Menu::Option::SettingsGlobal:
			{
				mOptionsAreGlobal = on;
				return;
			}
		case Menu::Option::RefreshGUI:
			{
				if(mBlockLevel != 1)
				{
					this->GetShell()->PopupWindow("GUI was in a non-ready state, needs refreshing.",MessageType::Error);
					if(!this->ListBlockRegistry().IsEmpty())
					{
						int i, n = this->ListBlockRegistry().Contains("\n");
						this->OutputText(MessageType::Information,"Block registry contents:");
						for(i=0; i<n; i++)
						{
							this->OutputText(MessageType::Information,"  "+this->ListBlockRegistry().Section("\n",i,i));
						}
					}
					mBlockLevel = 1;
				}
				break;
			}
		default:
			{
				IBUG_WARN("Invalid menu item id.");
			}
		}
		this->Render(mOptionsAreGlobal?ViewModuleSelection::All:ViewModuleSelection::One);
		return;
	}

	if(id <= Menu::Help::Max)
	{
		switch(id)
		{
		case Menu::Help::Contents:
			{
				if(mDialogHelp != 0) mDialogHelp->Show(true);
				break;
			}
		case Menu::Help::About:
			{
				if(mDialogAbout != 0) mDialogAbout->Show(true);
				break;
			}
#if defined(I_DEBUG)
		case Menu::Help::Max:
			{
				if(mDialogDebugHelper != 0) mDialogDebugHelper->Show(true);
				break;
			}
#endif
		default:
			{
				IBUG_WARN("Invalid menu item id.");
			}
		}
		return;
	}

	if(id <= ToolBar::Max)
	{
		switch(id)
		{
		case ToolBar::OpenWindowsPage:
			{
				mBook->OpenPage(0);
				mViewPage->ShowPage(4);
				mViewPage->GetWindowListDialog()->Show(true);
				break;
			}
		case ToolBar::MinimizeWindows:
			{
				this->DisplayWindowsAsIcons();
				this->GetSubject()->GetHelper()->ShowAsIcon();
				break;
			}
		case ToolBar::MaximizeWindows:
			{
				this->DisplayWindowsAsWindows();
				break;
			}
		case ToolBar::MoveAllTogether:
			{
				mMoveTogether = on;
				break;
			}
		case ToolBar::ShowSurface:
			{
				mPages[0]->Show(on);
				if(on)
				{
					mBook->OpenPage(1);
					mExtensionWindow->OpenBookPageByIndex(1);
					mBook->GetPage(1)->UpdateWidget();
				}
				break;
			}
		case ToolBar::ShowCrossSection:
			{
				mPages[1]->Show(on);
				if(on)
				{
					mBook->OpenPage(2);
					mExtensionWindow->OpenBookPageByIndex(2);
					mBook->GetPage(2)->UpdateWidget();
				}
				break;
			}
		case ToolBar::ShowVolume:
			{
				mPages[2]->Show(on);
				if(on)
				{
					mBook->OpenPage(3);
					mExtensionWindow->OpenBookPageByIndex(3);
					mBook->GetPage(3)->UpdateWidget();
				}
				break;
			}
		case ToolBar::ShowParticles:
			{
				mPages[3]->Show(on);
				if(on)
				{
					mBook->OpenPage(4);
					mExtensionWindow->OpenBookPageByIndex(4);
					mBook->GetPage(4)->UpdateWidget();
				}
				break;
			}
		case ToolBar::ShowVectorField:
			{
				mPages[4]->Show(on);
				if(on)
				{
					mBook->OpenPage(5);
					mExtensionWindow->OpenBookPageByIndex(5);
					mBook->GetPage(5)->UpdateWidget();
				}
				break;
			}
		case ToolBar::ShowTensorField:
			{
				mPages[5]->Show(on);
				if(on)
				{
					mBook->OpenPage(6);
					mExtensionWindow->OpenBookPageByIndex(6);
					mBook->GetPage(6)->UpdateWidget();
				}
				break;
			}
		default:
			{
				IBUG_WARN("Invalid menu item id.");
			}
		}
		return;
	}

	if(!mExtensionWindow->OnMenuBody(id,on))
	{
		IBUG_WARN("Invalid menu item id.");
	}
}

#endif
