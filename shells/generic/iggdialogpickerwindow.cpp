/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggdialogpickerwindow.h"


#include "idataformatter.h"
#include "imarker.h"
#include "ipicker.h"
#include "iviewmodule.h"
#include "ishell.h"
#include "istretch.h"

#include "iggframe.h"
#include "iggframedoublebutton.h"
#include "igghandle.h"
#include "iggimagefactory.h"
#include "iggmainwindow.h"
#include "iggshell.h"
#include "iggwidgetmisc.h"
#include "iggwidgetpropertycontrolbutton.h"
#include "iggwidgetpropertycontrolselectionbox.h"
#include "iggwidgetpropertycontrolslider.h"
#include "iggwidgetotherbutton.h"
#include "iggwidgettext.h"

#include "ibgwidgetentrysubject.h"

#include "iggsubjectfactory.h"

//
//  Templates
//
#include "iggwidgetpropertycontrolslider.tlh"


using namespace iParameter;


namespace iggDialogPickerWindow_Private
{
	//
	//  Returns the current Picker
	//
	class PickerHandle : public iggObjectHandle
	{

	public:

		PickerHandle(iggDialogPickerWindow *owner) : iggObjectHandle(owner)
		{
		}

		virtual const iObject* Object(int mod = -1) const
		{
			return this->GetShell()->GetViewModule(mod)->GetPicker();
		}
	};

	class MoveFocalPointFrame : public iggFrameMoveFocalPointButton
	{
		
	public:
		
		MoveFocalPointFrame(iggFrame *parent) : iggFrameMoveFocalPointButton("Picked position",parent,true)
		{
		}

		virtual iVector3D GetPosition() const
		{
			return this->GetShell()->GetViewModule()->GetPicker()->GetPosition().OpenGLValue();
		}
	};

	//
	//  For the display to update when the box size changes, we need to have
	//  a dummy invisible position slider
	//
	class DummyPositionSlider : public iggWidgetPropertyControlPositionSlider
	{

	public:

		DummyPositionSlider(iggDialogPickerWindow *dialog, iggFrame *parent) : iggWidgetPropertyControlPositionSlider(0,"",iggPropertyHandle("Position",dialog->GetPickerHandle()),0,parent,RenderMode::DoNotRender)
		{
			mDialog = dialog;
			this->Show(false);
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			if(mDialog != 0) mDialog->DisplayData(this->GetShell()->GetViewModule()->GetPicker());
		}

		iggDialogPickerWindow *mDialog;
	};
};


using namespace iggDialogPickerWindow_Private;


iggDialogPickerWindow::iggDialogPickerWindow(iggMainWindow *parent) : iggDialog(parent,0U,iggImageFactory::FindIcon("picks.png"),"Picker Window","sr.gg.dpi",1)
{
	mPickerHandle = new PickerHandle(this); IASSERT(mPickerHandle);

	if(this->ImmediateConstruction()) this->CompleteConstruction();
}


iggDialogPickerWindow::~iggDialogPickerWindow()
{
	delete mPickerHandle;
}


void iggDialogPickerWindow::CompleteConstructionBody()
{
	mWaitFrame = new iggFrame(mFrame);
	mWaitFrame->AddLine(new iggWidgetTextLabel("Please wait, the data are being gathered...",mWaitFrame));
	mWaitFrame->Show(false);
	mFrame->AddLine(mWaitFrame);

	mInfoFrame = new iggFrame(mFrame,2);
	mInfo = new iggWidgetTextBrowser(false,false,mInfoFrame);
	mInfoFrame->AddLine(mInfo,2);
	iggWidgetPropertyControlRadioBox *pm = new iggWidgetPropertyControlRadioBox(1,"Method",0,iggPropertyHandle("PickMethod",this->GetPickerHandle()),mInfoFrame);
	pm->InsertItem("Cell picker");
	pm->InsertItem("Point picker");
	pm->InsertItem("Object picker");
	mInfoFrame->AddLine(pm,new MoveFocalPointFrame(mInfoFrame));
	mInfoFrame->AddLine(new iggWidgetPropertyControlFloatSlider(0.001,0.1,20,iStretch::Log,0,false,false,"Pick accuracy",iggPropertyHandle("Accuracy",this->GetPickerHandle()),mInfoFrame),2);
	mInfoFrame->AddLine(new iggWidgetPropertyControlFloatSlider(0.01,100.0,40,iStretch::Log,0,false,false,"Marker size",iggPropertyHandle("PointSize",this->GetPickerHandle()),mInfoFrame),2);
	mInfoFrame->SetColStretch(1,10);

	//
	//  Dummy position slider
	//
	mInfoFrame->AddLine(new DummyPositionSlider(this,mInfoFrame));
	
	mFrame->AddLine(mInfoFrame);
	mFrame->SetRowStretch(1,10);

	this->ResizeContents(300,500);
}


void iggDialogPickerWindow::ShowBody(bool s)
{
	int i;
	for(i=0; i<this->GetShell()->GetNumberOfViewModules(); i++)
	{
		this->GetShell()->GetViewModule(i)->GetPicker()->ShowPickedPoint(s);
	}
	iggDialog::ShowBody(s);
}


void iggDialogPickerWindow::CollectData(bool s)
{
	if(!mCompleteConstructionCalled) this->CompleteConstruction();

	mWaitFrame->Show(s);
	mInfoFrame->Show(!s);
}


void iggDialogPickerWindow::DisplayData(const iPicker *picker)
{
	if(!mCompleteConstructionCalled) this->CompleteConstruction();

	mInfo->Clear();

	if(picker!=0 && !picker->GetObjectName().IsEmpty())
	{
		mInfo->AppendTextLine("Object",picker->GetObjectName());
		mInfo->AppendTextLine("X",iString::FromNumber(picker->GetPosition().BoxValue(0)));
		mInfo->AppendTextLine("Y",iString::FromNumber(picker->GetPosition().BoxValue(1)));
		mInfo->AppendTextLine("Z",iString::FromNumber(picker->GetPosition().BoxValue(2)));

		int i, lnmax;
		iString name, valu, unit, s;

		lnmax = 0;
		for(i=0; i<picker->GetDataFormatter()->GetNumReportLines(); i++)
		{
			picker->GetDataFormatter()->GetReport(i,name,valu,unit);
			if(lnmax < name.Length()) lnmax = name.Length();
		}
		for(i=0; i<lnmax; i++) s += ".";

		for(i=0; i<picker->GetDataFormatter()->GetNumReportLines(); i++)
		{
			picker->GetDataFormatter()->GetReport(i,name,valu,unit);
			mInfo->AppendTextLine(name+s.Part(0,lnmax-name.Length()),valu+(unit.IsEmpty()?"":"  ["+unit+"]"));
		}

		this->GetMainWindow()->UpdateOnPick();
	}
	else
	{
		mInfo->AppendTextLine("Nothing is picked.");
	}
}

#endif
