/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Window with extension pages (extension manager)
//

#ifndef IGGEXTENSIONWINDOW_H
#define IGGEXTENSIONWINDOW_H


#include "iggelement.h"


#include "iarray.h"

class iDataType;
class iImage;
class iShell;
class iString;

class iggAbstractExtension;
class iggExtensionCreator;
class iggExtensionCreatorPointer;
class iggExtensionWindowHelper;
class iggFrame;
class iggFrameFlip;
class iggMainWindow;
class iggPageData;

class ibgExtensionWindowSubject;


class iggExtensionWindow : public iggElement
{

	friend class iggAbstractExtension;
	friend class iggExtensionCreator;
	friend class iggMainWindow;

public:

	virtual ~iggExtensionWindow();

	//
	//  Broadcast to all extensions
	//
	void OpenBookPageByIndex(int index);
	void SetTabMode(int m);

	void PopulateFileMenu();
	void PopulateLocalDialogMenu();
	void PopulateGlobalDialogMenu();
	void PopulateFileToolBar(bool postonly);
	void PrePopulateFileToolBar();
	void PopulateShowToolBar();

	bool OnMenuBody(int id, bool on);

	void UpdateOnPick();
	void UpdateParticleWidgets(const iImage *icon = 0);
	void AddReloadingDataTypes(iggPageData *page);
	const iImage* GetSpecialParticleIcon(const iDataType &type) const;

	void UpdateOnDataType(const iDataType &type);

	//
	//  Special functions
	//
	bool IsEmpty() const;
	void SetPadding(bool s);
	void OnCloseAttempt();

	//
	//  Access components
	//
	inline ibgExtensionWindowSubject* GetSubject() const { return mSubject; }
	inline iggMainWindow* GetMainWindow() const { return mMainWindow; }

private:

	iggExtensionWindow(iggMainWindow *mw);
	void CompleteInitialization();

	void UpdateAll();

	static void AttachCreator(iggExtensionCreator *c);
	static iArray<iggExtensionCreatorPointer>& Creators();

	iggFrame *mFrames[3];
	iggFrameFlip *mMainPanel;
	iggMainWindow *mMainWindow;
	ibgExtensionWindowSubject *mSubject;
	iggExtensionWindowHelper *mHelper;
	iArray<iggAbstractExtension*> mExt;
};

#endif  // IGGEXTENSIONWINDOW_H

