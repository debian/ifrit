/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IGGDIALOGFILESETEXPLORER_H
#define IGGDIALOGFILESETEXPLORER_H


#include "iggdialog.h"


class iggWidgetListView;


class iggDialogFileSetExplorer : public iggDialog
{

public:

	iggDialogFileSetExplorer(iggMainWindow *parent);

	virtual void UpdateDialog();

	void SetLoadAll(bool s){ mLoadAll = s; }
	inline bool GetLoadAll() const { return mLoadAll; }

	void SetMatchSimilarNames(bool s){ mMatchSimilarNames = s; }
	inline bool GetMatchSimilarNames() const { return mMatchSimilarNames; }

	void MustBeClosed();

	void LoadData(const iString &fname);

protected:

	virtual void CompleteConstructionBody();
	virtual void ShowBody(bool s);

	bool mMustBeClosed, mLoadAll, mMatchSimilarNames;
	iggWidgetListView *mView;
};

#endif  //  IGGDIALOGFILESETEXPLORER_H
