/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggpageparticles.h"


#include "idata.h"
#include "idatalimits.h"
#include "idatasubject.h"
#include "ihistogrammaker.h"
#include "iparticleviewsubject.h"
#include "ipointglyph.h"
#include "iviewmodule.h"
#include "iviewobject.h"

#include "iggextensionwindow.h"
#include "iggframedatatypeselector.h"
#include "iggframedatavariablelist.h"
#include "iggframepaintingcontrols.h"
#include "iggframerangecontrols.h"
#include "igghandle.h"
#include "iggimagefactory.h"
#include "iggmainwindow.h"
#include "iggshell.h"
#include "iggwidgetmisc.h"
#include "iggwidgetpropertycontrolbutton.h"
#include "iggwidgetpropertycontrolcolorselection.h"
#include "iggwidgetpropertycontrollineedit.h"
#include "iggwidgetpropertycontrolselectionbox.h"
#include "iggwidgetotherbutton.h"

#include "ibgwidgetbuttonsubject.h"

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"
#include "iggwidgetpropertycontrolslider.tlh"


using namespace iParameter;


namespace iggPageParticles_Private
{
	//
	//  Helper class
	//
	class RangeControls : public iggFrameRangeControls
	{
		
	public:
		
		RangeControls(iggPageParticles *owner, iggFrame *dependent, const iggPropertyHandle& rh, const iggPropertyHandle& th, iggFrame *parent) : iggFrameRangeControls(rh,th,parent)
		{
			mOwner = owner;
			mDependent = dependent;
			mCachedSubject = 0;
		}

		virtual bool IsActive() const
		{
			return (this->Subject()->GetSplitVar() > -1);
		}

		virtual iPair GetGlobalRange() const
		{
			return this->Subject()->GetLimits()->GetDataRange(this->Subject()->GetSplitVar());
		}

		virtual int GetActiveRangeIndex() const
		{
			return mOwner->GetActiveInstance();
		}

		virtual void SetActiveRangeIndex(int c)
		{
			mOwner->SetActiveInstance(c);
		}

		virtual const iHistogram& GetHistogram(int sid, bool full) const
		{
			int var = this->Subject()->GetSplitVar();
			if(var != -1)
			{
				return this->Subject()->GetSubject()->GetHistogramMaker()->GetHistogram(var,sid,full);
			}
			else return iHistogram::Null();
		}

		virtual void SetNumRanges(int n)
		{
		    if(this->Subject()->ResizeSplitRanges(n))
			{
				if(mDependent != 0) mDependent->UpdateWidget();
			}
		}

	protected:

		iParticleViewSubject* Subject() const
		{
			if(mCachedSubject != mOwner->GetViewSubject(-1))
			{
				mCachedSubject = iRequiredCast<iParticleViewSubject>(INFO,mOwner->GetViewSubject(-1));
			}
			return mCachedSubject;
		}

		mutable iParticleViewSubject *mCachedSubject;
		iggPageParticles *mOwner;
		iggFrame *mDependent;
	};


	class SplitFrame : public iggFrame
	{

	public:

		SplitFrame(iggPageParticles *owner, iggFrameBase *parent) : iggFrame(parent,1)
		{
			mOwner = owner;
			mHandle = new iggPropertyHandle("RenderSortVar",mOwner->GetSubjectHandle());
		}

		virtual ~SplitFrame()
		{
			delete mHandle;
		}

	protected:

		iggPageParticles *mOwner;
		const iggPropertyHandle *mHandle;

		virtual void UpdateWidgetBody()
		{
			const iPropertyDataVariable<iType::Int> *prop = iDynamicCast<const iPropertyDataVariable<iType::Int> >(INFO,mHandle->Variable());
			if(prop != 0)
			{
				this->Enable(prop->GetValue(0) == -1);
			}
			else this->Enable(false);
		}
	};


	//
	//  Main page
	// ************************************************
	//
	class MainPage : public iggMainPage2
	{

	public:

		MainPage(iggPageObject *owner, iggFrameBase *parent) : iggMainPage2(owner,parent,false)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		virtual void InsertSectionA(iggFrame *page)
		{
			//
			//  Method & color normals
			//
			iggFrame *tf = new iggFrame(this,2);
			iggWidgetPropertyControlComboBox *tb = new iggWidgetPropertyControlComboBox("Type",0,mOwner->CreatePropertyHandle("Type"),tf);
			int i;
			for(i=0; i<PointGlyphType::SIZE; i++)
			{
				tb->InsertItem(iPointGlyph::GetName(i));
			}
			tf->AddLine(tb,new iggWidgetPropertyControlColorSelection(mOwner->CreatePropertyHandle("Color"),tf));

			this->AddLine(tf);
			this->AddSpace(10);
			//
			//  Opacity
			//
			iggWidgetPropertyControlFloatSlider *os = new iggWidgetPropertyControlFloatSlider(1.0e-4,1.0,40,iStretch::Log,4,true,false,"Opacity",mOwner->CreatePropertyHandle("Opacity"),this);
			os->SetStretch(2,10);
			this->AddLine(os,3);
			iggWidgetPropertyControlFloatSlider *ss = new iggWidgetPropertyControlFloatSlider(1.0e-2,1.0e2,40,iStretch::Log,4,true,false,"Size",mOwner->CreatePropertyHandle("FixedSize"),this);
			ss->SetStretch(2,10);
			this->AddLine(ss,3);
			this->AddLine(new iggWidgetPropertyControlCheckBox("Scale particles automatically",mOwner->CreatePropertyHandle("AutoScaled"),this));
			this->AddSpace(10);

			this->SetColStretch(1,10); 
		}
	};


	//
	//  Paint page
	// ************************************************
	//
	class PaintPage : public iggPaintPage2
	{

	public:
	
		PaintPage(iggPageObject *owner, iggFrameBase *parent) : iggPaintPage2(owner,parent,iggPaintPage2::WithBrightness|iggPaintPage2::WithShading)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		virtual void InsertSectionA(iggFrame *page)
		{
			iggFramePaintingControls *pl = new iggFramePaintingControls("Paint with...",true,mOwner->GetDataSubjectHandle(),mOwner->CreatePropertyHandle("PaintVar"),page);

			page->AddLine(pl,2);
			page->AddSpace(10);
		}
	};


	//
	//  Size page
	// ************************************************
	//
	class SizePage : public iggPage2
	{

	public:

		SizePage(iggPageObject *owner, iggFrameBase *parent) : iggPage2(owner,parent,3)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		virtual void CompleteConstructionBody()
		{
			iggPropertyHandle vh(mOwner->CreatePropertyHandle("ScaleVar"));

			iggFrameDataVariableList *sl = new iggFrameDataVariableList("Size with...",mOwner->GetDataSubjectHandle(),vh,this);
			sl->InsertItem("None");
			sl->Complete();

			this->AddLine(sl);
			this->AddLine(new iggWidgetPropertyControlFloatLineEdit("Scale by",mOwner->CreatePropertyHandle("ScaleFactor"),this),2);

			this->AddSpace(10);

			this->SetColStretch(1,0);
			this->SetColStretch(2,10);
		}
	};
	

	//
	//  Advanced page
	// ************************************************
	//
	class AdvancedPage : public iggPage2
	{

	public:

		AdvancedPage(iggPageParticles *owner, iggFrameBase *parent) : iggPage2(owner,parent,1)
		{
			mRealOwner = owner;

			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		iggPageParticles *mRealOwner;

		virtual void CompleteConstructionBody()
		{
			//
			//  Book
			//
			iggFrameBook *ab = new iggFrameBook(this);
			this->AddLine(ab);
			//
			//  Split page
			//
			SplitFrame *abpage0 = new SplitFrame(mRealOwner,ab);
			ab->AddPage("Split into groups",mOwner->Icon(),abpage0);
			{
				//
				//  Book
				//
				iggFrameBook *db = new iggFrameBook(abpage0);
				abpage0->AddLine(db);
				iggFrameDataVariableList *av;
				//
				//  Main page
				//
				iggFrame *dbpage0 = new iggFrame(db,2);
				db->AddPage("Main",mOwner->Icon(),dbpage0);
				{
					av = new iggFrameDataVariableList("Split by...",mOwner->GetDataSubjectHandle(),mRealOwner->GetSplitVarHandle(),dbpage0);
					av->InsertItem("Do not split");
					av->Complete(); // splitter's variables start with -1
					dbpage0->AddLine(av);
					dbpage0->AddSpace(10);
					dbpage0->SetColStretch(1,3);
				}
				//
				//  FunctionMapping widget
				//
				iggFrame *dbpage1 = new iggFrame(db,1);
				db->AddPage("Ranges",mOwner->Icon(),dbpage1);
				{
					iggPropertyHandle rh("SplitRanges",mOwner->GetSubjectHandle(),0);
					iggPropertyHandle th("SplitRangesTiled",mOwner->GetSubjectHandle(),0);
					RangeControls *rm = new RangeControls(mRealOwner,this,rh,th,dbpage1);
					av->AddDependent(rm);
					dbpage1->AddLine(rm);
				}
			}
			//
			//  Connect page
			//
			iggFrame *abpage1 = new iggFrame(ab,3);
			ab->AddPage("Connect with lines",mOwner->Icon(),abpage1);
			{
				iggFrameDataVariableList *cv = new iggFrameDataVariableList("Connect points by...",mOwner->GetDataSubjectHandle(),mOwner->CreatePropertyHandle("ConnectVar"),abpage1);
				cv->InsertItem("Do not connect");
				cv->Complete(); // connector's variables start with -1
				iggFrameDataVariableList *sv = new iggFrameDataVariableList("Break lines by...",mOwner->GetDataSubjectHandle(),mOwner->CreatePropertyHandle("ConnectionBreakVar"),abpage1);
				sv->InsertItem("None");
				sv->Complete(); // connector's variables start with -1
				abpage1->AddLine(cv,sv);

				abpage1->AddLine(new iggWidgetPropertyControlSpinBox(1,100,"Line width",0,mOwner->CreatePropertyHandle("LineWidth"),abpage1));

				abpage1->AddSpace(10);
				abpage1->SetColStretch(2,10);
			}
			//
			//  Render sort page
			//
			iggFrame *abpage2 = new iggFrame(ab,3);
			ab->AddPage("Render sort",mOwner->Icon(),abpage2);
			{
				abpage2->AddLine(new iggWidgetTextLabel("%b This feature disables splitting particles into groups.",abpage2));
				abpage2->AddLine(new iggWidgetTextLabel("%b It may also result in incompatible rendering modes.",abpage2));

				iggFrameDataVariableList *cv = new iggFrameDataVariableList("Order rendering by...",mOwner->GetDataSubjectHandle(),mOwner->CreatePropertyHandle("RenderSortVar"),abpage2);
				cv->InsertItem("Use true rendering");
				cv->Complete(); // connector's variables start with -1
				abpage2->AddLine(cv);

				abpage2->AddSpace(10);
				abpage2->SetColStretch(2,10);
			}
		}
	};
};


using namespace iggPageParticles_Private;


iggPageParticles::iggPageParticles(iggFrameBase *parent) : iggPageObject("Group",parent,"p",iggImageFactory::FindIcon("part.png"))
{
	mCurrentParticles = mDataTypeHandle->GetActiveDataTypeIndex();
	
	mSplitVarHandle = new iggPropertyHandle("SplitVar",this->GetSubjectHandle());

	//
	//  Main page
	// ************************************************
	//
	mBook->AddPage("Main",this->Icon(),new MainPage(this,mBook));

	//
	//  Paint page
	// ************************************************
	//
	mBook->AddPage("Paint",this->Icon(),new PaintPage(this,mBook));

	//
	//  Size page
	// ************************************************
	//
	mBook->AddPage("Size",this->Icon(),new SizePage(this,mBook));

	//
	//  Advanced page
	// ************************************************
	//
	mBook->AddPage("Advanced",this->Icon(),new AdvancedPage(this,mBook));

	//
	//  Replicate page
	// ************************************************
	//
	mBook->AddPage("Replicate",this->Icon(),new iggReplicatePage2(this,mBook,false));
}


iggPageParticles::~iggPageParticles()
{
	delete mSplitVarHandle;
}


void iggPageParticles::UpdateIcons()
{
	if(mIcons.Size() == 0)
	{
		mIcons.Add(this->Icon());

		const iDataInfo &di(mDataTypeHandle->GetDataInfo());
		int i;
		for(i=1; i<di.Size(); i++)
		{
			const iImage *image = this->GetMainWindow()->GetExtensionWindow()->GetSpecialParticleIcon(di.Type(i));
			if(image == 0) mIcons.Add(this->Icon()); else mIcons.Add(image);
		}
	}
}


void iggPageParticles::SetCurrentParticles(int n)
{
	if(n>=0 && n<this->GetViewObject(-1)->GetNumberOfSubjects())
	{
		mDataTypeHandle->SetActiveDataTypeIndex(n);
		mCurrentParticles = mDataTypeHandle->GetActiveDataTypeIndex();

#ifndef I_NO_CHECK
		if(mIcons.Size() != this->GetViewObject(-1)->GetNumberOfSubjects())
		{
			IBUG_WARN("Bug in iggPageParticles: incompating mIcons array.");
		}
#endif

		if(n>=0 && n<mIcons.Size())
		{
			this->GetMainWindow()->UpdateParticleWidgets(mIcons[n]);
		}
		else
		{
			this->GetMainWindow()->UpdateParticleWidgets(mIcons[0]);
		}
	}
}


void iggPageParticles::AddIcon(const iImage *image)
{
	mIcons.Add(image);
}


void iggPageParticles::UpdateWidgetBody()
{
	if(mCurrentParticles != mDataTypeHandle->GetActiveDataTypeIndex())
	{
		this->SetCurrentParticles(mDataTypeHandle->GetActiveDataTypeIndex());
		this->GetMainWindow()->GetExtensionWindow()->UpdateOnDataType(mDataTypeHandle->GetActiveDataType());
		this->GetMainWindow()->UpdateAll();
	}
	//this->GetMainWindow()->UpdateParticleWidgets(mIcons[mCurrentParticles]);
	this->iggPage::UpdateWidgetBody();
}

#endif
