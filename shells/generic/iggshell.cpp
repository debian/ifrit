/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggshell.h"


#include "idatareader.h"
#include "iedition.h"
#include "ierror.h"
#include "iexception.h"
#include "ifile.h"
#include "ioutputchannel.h"
#include "iscript.h"
#include "iversion.h"
#include "iviewmodule.h"
#include "iviewmodulecollection.h"

#include "iggdatareaderobserver.h"
#include "iggdialog.h"
#include "iggextensionwindow.h"
#include "iggframe.h"
#include "igghandle.h"
#include "iggimagefactory.h"
#include "iggmainwindow.h"
#include "iggrenderwindow.h"
#include "iggrenderwindowinteractor.h"
#include "iggshelleventobservers.h"
#include "iggviewmoduleeventobservers.h"
#include "iggwidgetarea.h"
#include "iggwidgetmisc.h"
#include "iggwidgetprogressbar.h"

#include "ibgdialogsubject.h"
#include "ibgextensionwindowsubject.h"
#include "ibgmainwindowsubject.h"
#include "ibgmenuwindowsubject.h"
#include "ibgrenderwindowsubject.h"
#include "ibgshellsubject.h"
#include "ibgwindowhelper.h"

#if defined(I_DEBUG)
#include <vtkTimerLog.h>
#endif

#include "iggsubjectfactory.h"

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"


using namespace iParameter;


class iggDialogFlashWindow : public iggDialog
{

public:

	iggDialogFlashWindow(iggShell *shell) : iggDialog(shell,DialogFlag::Unattached|DialogFlag::Blocking|DialogFlag::NoTitleBar,0,"",0,3,0)
	{
		this->CompleteConstruction();
	}

	void CompleteConstructionBody()
	{
		mFrame->AddLine(new iggWidgetTextLabel("%b%+   IFrIT - Ionization FRont Interactive Tool   ",mFrame),3);
		mFrame->AddLine(new iggWidgetTextLabel("Version "+iVersion::GetVersion(),mFrame),3);
		mFrame->AddLine(new iggWidgetTextLabel(iEdition::GetEditionName(),mFrame),3);

		mFlipper = new iggWidgetLogoFlipper(mFrame);
		mFrame->AddLine(0,mFlipper,0);

		mMessage = new iggWidgetTextLabel("Starting...",mFrame);
		mFrame->AddLine(mMessage,3);

		this->ResizeContents(100,100);
	}

	virtual void ShowBody(bool s)
	{
		mFlipper->Reset();
		iggDialog::ShowBody(s);
	}

	void OnInitAtom()
	{
		mFlipper->Advance();
	}

	void PlaceOnDesktop(int dw, int dh)
	{
		int wg[4];
		mSubject->GetWindowGeometry(wg,true);
		wg[0] = (dw-wg[2]-20)/2;
		wg[1] = (dh-wg[3]-20)/2;
		mSubject->SetWindowGeometry(wg);
	}

	void DisplayMessage(const iString &text)
	{
		mMessage->SetText(text);
	}

protected:

	virtual bool CanBeClosed()
	{
		return false; // cannot be closed
	}

	iggWidgetTextLabel *mMessage;
	iggWidgetLogoFlipper *mFlipper;
};


namespace iggShell_Private
{
	class OutputChannel: public iOutputChannel
	{

	public:

		OutputChannel(iggShell *shell): iOutputChannel(shell)
		{
			IASSERT(shell);
			mTrueShell = shell;
		}

	protected:

		virtual void DisplayBody(int type,const iString &message)
		{
			switch(type)
			{
			case MessageType::Error:
				{
					mTrueShell->PopupWindow(message,MessageType::Error);
					break;
				}
			case MessageType::Warning:
				{
					if(mTrueShell->GetMainWindow() != 0)
					{
						mTrueShell->GetMainWindow()->WriteToLog("",message,iColor(255, 0, 0));
					}
					else mTrueShell->PopupWindow(message,MessageType::Warning);
					break;
				}
			case MessageType::Information:
				{
					if(mTrueShell->GetMainWindow() != 0)
					{
						if(message.Contains(':'))
						{
							iString prefix = message.Section(": ",0,0);
							iString body = message.Section(": ",1);
							mTrueShell->GetMainWindow()->WriteToLog(prefix,body);
						}
						else
						{
							mTrueShell->GetMainWindow()->WriteToLog("",message);
						}
					}
					else mTrueShell->PopupWindow(message,MessageType::Information);
					break;
				}
#ifdef I_DEBUG
			case MessageType::DebugMessage:
				{
					if(mTrueShell->GetMainWindow() != 0)
					{
						mTrueShell->GetMainWindow()->WriteToDebugLog(message);
					}
					else
					{
						cout << message.ToCharPointer() << endl;
					}
					break;
				}
#endif
			}
		}

	private:

		iggShell *mTrueShell;
	};


	//
	//  Returns self
	//
	class ShellHandle: public iggObjectHandle
	{

	public:

		ShellHandle(iggShell *s) : iggObjectHandle(s)
		{
		}

		virtual const iObject* Object(int mod = -1) const
		{
			return this->GetShell();
		}
	};

	//
	//  Returns the current VM
	//
	class ViewModuleHandle : public iggObjectHandle
	{

	public:

		ViewModuleHandle(iggShell *s) : iggObjectHandle(s)
		{
		}

		virtual const iObject* Object(int mod = -1) const
		{
			return this->GetShell()->GetViewModule(mod);
		}
	};

	//
	//  Returns the current DR
	//
	class DataReaderHandle : public iggObjectHandle
	{

	public:

		DataReaderHandle(iggShell *s) : iggObjectHandle(s)
		{
		}

		virtual const iObject* Object(int mod = -1) const
		{
			return this->GetShell()->GetViewModule(mod)->GetReader();
		}
	};


	//
	//  Returns the current whatever
	//
	class GenericObjectHandle : public iggObjectHandle
	{

	public:

		GenericObjectHandle(const iString &name, iggShell *s) : iggObjectHandle(s), mName(name)
		{
		}

		virtual const iObject* Object(int mod = -1) const
		{
			const iObject *obj = this->GetShell()->GetViewModule(mod)->FindByName(mName);
			if(obj == 0)
			{
				IBUG_FATAL("Object with name <"+mName+"> does not exist.");
			}
			return obj;
		}

	private:

		const iString mName;
	};

	const iString& HandleLookup(iggObjectHandle* const &ptr)
	{
		static const iString null("unknown");
		if(ptr == 0) return null; else return ptr->Object()->LongName();
	}

};


using namespace iggShell_Private;


iggShell::iggShell(iRunner *runner, bool threaded, const iString &type, int argc, const char **argv) : iShell(runner,threaded,type,argc,argv), mHandles(HandleLookup,20)
{
	try
	{
		//
		// Make sure Image factory is up to date
		//
		iggImageFactory::FindIcon("ok.png");

		//
		//  Default settings
		//
		mShowFlashWindow = true;
		mStartDocked = false;

		mConditionFlags = 0;
		mSubject = 0;
		mMainWindow = 0; // very important!!! This tells other components that the main window does not exist yet.
		mFlashWindow = 0;

		mShellHandle = new ShellHandle(this); IERROR_CHECK_MEMORY(mShellHandle);
		mViewModuleHandle = new ViewModuleHandle(this); IERROR_CHECK_MEMORY(mViewModuleHandle);
		mDataReaderHandle = new DataReaderHandle(this); IERROR_CHECK_MEMORY(mDataReaderHandle);

		mActiveViewModuleIndex = 0;

		//
		//  Define generic GUI-specific command-line options
		//
		this->AddCommandLineOption("d","docked","start IFrIT with windows docked",false);
		this->AddCommandLineOption("fs","font-size","increase/decrease window font size",true);
		this->AddCommandLineOption("nf","no-flash-window","do not show the splash window at start-up",false);

		this->AddCommandLineOption("om","old-style-window-manager","use old-style window manager",false);
		this->AddCommandLineOption("sd","small-desktop","assume the desktop size is small",false);
		this->AddCommandLineOption("sc","slow-connection","minimize updates for running on a slow remote connection",false);
		this->AddCommandLineOption("ddi","disable-delayed-initialization","disable delayed initialization",false);
	}
	catch(iException::GlobalExit)
	{
		mIsFailed = true;
	}
}


iggShell::~iggShell()
{
	delete mDataReaderHandle;
	delete mViewModuleHandle;
	delete mShellHandle;

	while(mHandles.Size() > 0) delete mHandles.RemoveLast();
}


bool iggShell::StartConstructor()
{
	mSubject = iggSubjectFactory::CreateShellSubject(this);
	if(mSubject != 0)
	{
		iEdition::ApplySettings(this);
		return true;
	}
	else return false;
}


void iggShell::OnInitStart()
{
#ifdef I_DEBUG
	iOutput::DisplayDebugMessage("Starting...");
#endif
	//
	//  Create and show message window
	//
	if(mShowFlashWindow)
	{
		mFlashWindow = new iggDialogFlashWindow(this); IERROR_CHECK_MEMORY(mFlashWindow);

		//
		//  calculate the position of the flash window
		//
		int dw, dh;
		mSubject->GetDesktopDimensions(dw,dh);
		//
		//  Allow for the two-monitor desktop
		//
		if(dw > 2*dh)
		{
			dw /= 2;
		}
		mSubject->ProcessEvents(true);
		mFlashWindow->PlaceOnDesktop(dw,dh);
		mFlashWindow->Show(true);
		mSubject->ProcessEvents(true);
		mFlashWindow->DisplayMessage("Creating visualization window...");
	}
}


void iggShell::OnInitState()
{
	if(mShowFlashWindow)
	{
		mFlashWindow->DisplayMessage("Loading the state file...");
		mSubject->ProcessEvents(false);
	}
}


void iggShell::OnInitFinish()
{
	const int topOffset0 = 64;
	const int frameXoffDefault = 13;
	const int frameYoffDefault = 25;
	int frameXoff = 1, frameYoff = 1;
	int topOffset, leftOffset = 0;

	if(mShowFlashWindow)
	{
		mFlashWindow->DisplayMessage("Showing windows...");
		mSubject->ProcessEvents(false);
	}
	//
	//  Show visualization windows
	//
	if(!this->IsDesktopSmall(false) && !mLoadedState)
	{
		//
		// Calculate the left offset of the visualization window (it is the only one)
		//
		int dw, dh;
		mSubject->GetDesktopDimensions(dw,dh);
		//
		//  Allow for the two-monitor desktop
		//
		if(dw > 2*dh)
		{
			dw /= 2;
		}

		int mwg[4], vwg[4];
		iggRenderWindow *visWindow = iRequiredCast<iggRenderWindow>(INFO,this->GetViewModule()->GetRenderWindow());

		//
		//  squeeze windows to the minimum possible size
		//
		mwg[0] = mwg[1] = mwg[2] = mwg[3] = 1;
		mMainWindow->GetSubject()->SetWindowGeometry(mwg);
		mMainWindow->GetExtensionWindow()->GetSubject()->SetWindowGeometry(mwg);
		mSubject->ProcessEvents(false);

		topOffset = this->IsDesktopSmall(true) ? 0 : topOffset0;
		//
		//  Show all windows
		//
		if(mStartDocked)
		{
			//
			//  If we start in the docked mode, trust the window manager
			//
			mMainWindow->DockWindows(true,false);
			mMainWindow->GetSubject()->GetWindowGeometry(mwg);

			bool redo = false;
			if(mwg[2] < (4*mwg[3]/3))
			{
				mwg[2] = (4*mwg[3]/3);
				redo = true;
			}

			leftOffset = (dw-mwg[2]-20)/2;
			if(leftOffset < 0) leftOffset = 0;
			if(mwg[0]<leftOffset || mwg[1]<topOffset)
			{
				mwg[0] = leftOffset;
				mwg[1] = topOffset;
				redo = true;
			}
			if(redo) mMainWindow->GetSubject()->SetWindowGeometry(mwg);

			mMainWindow->ShowAll(true);
		}
		else
		{
			//
			//  Place the windows on the screen
			//
			mMainWindow->GetSubject()->GetWindowGeometry(mwg,true);
			visWindow->GetSubject()->GetWindowGeometry(vwg,true);

			leftOffset = (dw-vwg[2]-mwg[2]-20)/2;
			if(leftOffset < 0) leftOffset = 0;
			//
			//  Move the view module window
			//
			vwg[0] = leftOffset;
			vwg[1] = topOffset;
			visWindow->GetSubject()->SetWindowGeometry(vwg);

			//
			//  Show the visualization window and give window manager a chance to decorate it.
			//
			int i;
			for(i=0; i<mViewModules->Size(); i++)
			{
				this->RequestRender(mViewModules->GetViewModule(i),true);
			}
			mSubject->ProcessEvents(false);

			//
			//  Get the frame geometry
			//
			visWindow->GetSubject()->GetFrameSize(frameXoff,frameYoff);
			if(frameXoff == 0) frameXoff = frameXoffDefault; // X11 failed to assign a proper frame
			if(frameYoff == 0) frameYoff = frameYoffDefault; // X11 failed to assign a proper frame

			mwg[0] = vwg[0] + vwg[2] + frameXoff;
			mwg[1] = topOffset;
			mMainWindow->GetSubject()->SetWindowGeometry(mwg,true);
	
			int ewg[4], ewg2[4];
			ewg[0] = leftOffset;
			ewg[1] = vwg[1] + vwg[3] + frameYoff;
			ewg[2] = vwg[2];
			ewg[3] = mwg[3] - vwg[3] - frameYoff;
			mMainWindow->GetExtensionWindow()->GetSubject()->SetWindowGeometry(ewg,true);
			mMainWindow->GetExtensionWindow()->GetSubject()->GetWindowGeometry(ewg2,true);
			//
			//  Stretch main window to match the extension window, but only if the desktop tall enough.
			//
			if(ewg2[3]>ewg[3] && !this->IsDesktopSmall(true))
			{
				mMainWindow->GetSubject()->GetWindowGeometry(mwg,true);
				mwg[3] += (ewg2[3]-ewg[3]);
				mMainWindow->GetSubject()->SetWindowGeometry(mwg,true);
			}

			//
			//  Show the main window and give a chance to the Window Manager to decorate the window
			//			     
			mMainWindow->GetSubject()->Show(true);
			mSubject->ProcessEvents(false);

			mMainWindow->GetExtensionWindow()->GetSubject()->Show(true);
			mSubject->ProcessEvents(false);
		}
	}
	else
	{
		int i;
		for(i=0; i<mViewModules->Size(); i++)
		{
			this->RequestRender(mViewModules->GetViewModule(i),true);
		}
		mMainWindow->GetSubject()->Show(true);
		mMainWindow->GetExtensionWindow()->GetSubject()->Show(true);
	}
	
	mSubject->CompleteStartUp();

	if(mShowFlashWindow)
	{
		//
		//  Remove the flash window
		//
		mFlashWindow->Show(false);
		delete mFlashWindow;
		mFlashWindow = 0;
	}

	mSubject->ProcessEvents(false);
	mMainWindow->PostShowInitialization();
}


bool iggShell::ConstructorBody()
{
	if(mShowFlashWindow)
	{
		mFlashWindow->DisplayMessage("Creating widgets...");
		mSubject->ProcessEvents(false);
	}

	mMainWindow = new iggMainWindow(this); IERROR_CHECK_MEMORY(mMainWindow);

	mMainWindow->StartInitialization();
	if(mStartDocked) mMainWindow->mDocked = true;
	mMainWindow->PreShowInitialization();
	if(mStartDocked) mMainWindow->mDocked = false;
	mSubject->ProcessEvents(false);
		
	return true;
}


void iggShell::DestructorBody()
{
	mMainWindow->Delete();
	mMainWindow = 0;  // tell other objects that the MainWindow does not exist any more
}


void iggShell::FinishDestructor()
{
	delete mSubject;
	mSubject = 0;
}


void iggShell::StartBody()
{
	if(this->IsThreaded())
	{
		mMainWindow->StartTimer();
	}
	mSubject->EnterEventLoop();
}


void iggShell::StopBody()
{
	if(!this->IsThreaded())
	{
		//
		//  Hide windows to avoid extra updates
		//
		mMainWindow->ShowAll(false);
		mMainWindow->BLOCK(true); // block all events
		mSubject->Exit();
	}
}


void iggShell::OnTimerEvent()
{
	if(this->HasPostedRequests())
	{
		this->PushAllRequests_();
	}

	if(this->IsStopped())
	{
		//
		//  Hide windows to avoid extra updates
		//
		mMainWindow->ShowAll(false);
		mMainWindow->BLOCK(true); // block all events
		mSubject->Exit();
	}
}


iOutputChannel* iggShell::CreateOutputChannel()
{
	return new OutputChannel(this);
}


void iggShell::GetDesktopDimensions(int &w, int &h) const
{
	mSubject->GetDesktopDimensions(w,h);
}


bool iggShell::OnCommandLineOption(const iString &sname, const iString& value)
{
	if(sname == "fs")
	{
		int s;
		bool ok;
		if((value[0]=='+' || value[0]=='-') && (s=value.ToInt(ok))!=0 && ok)
		{
			ibgWindowHelper::ChangeFontSize(s);
			return true;
		}
		else
		{
			this->OutputText(MessageType::Error,"Invalid font size specification; should be in the form +<number> or -<number>. Abort.");
			return false;
		}
	} 

	if(sname == "d")
	{
		mStartDocked = true;
		return true;
	} 

	if(sname == "nf")
	{
		mShowFlashWindow = false;
		return true;
	} 

	//
	//  behaviour settings
	//
	if(sname == "om")
	{
		mConditionFlags |= Condition::OldWindowManager;
		return true;
	}

	if(sname == "sd")
	{
		mConditionFlags |= Condition::SmallDesktopSize;
		return true;
	} 

	if(sname == "sc")
	{
		mConditionFlags |= Condition::SlowRemoteConnection;
		return true;
	}
	
	if(sname == "ddi")
	{
		mConditionFlags |= Condition::DisableDelayedInitialization;
		return true;
	}

	return this->iShell::OnCommandLineOption(sname,value);
}


//
//  Running conditions
//
bool iggShell::IsDesktopSmall(bool vert) const
{
	if(this->CheckCondition(Condition::SmallDesktopSize)) return true;

	int w, h;
	mSubject->GetDesktopDimensions(w,h);
	if(vert) return (h < 900); else return (w < 1200);
}


bool iggShell::CheckCondition(int flag) const
{
	return ((mConditionFlags & flag) != 0);
}


void iggShell::SetCondition(int flag)
{
	mConditionFlags |= flag;
}


//
//  State saving
//
void iggShell::ProcessEvents(bool sync)
{
	mSubject->ProcessEvents(sync);
}


void iggShell::OnInitAtomBody(bool timed)
{
	if(timed && mFlashWindow!=0) mFlashWindow->OnInitAtom();
}


void iggShell::OnLoadStateAtomBody(int prog)
{
	if(mMainWindow!=0 && mMainWindow->GetProgressBar()!=0)
	{
		if(prog == 0) mMainWindow->GetProgressBar()->Reset();
		mMainWindow->GetProgressBar()->SetProgress(prog);
		if(prog == 100) mMainWindow->GetProgressBar()->Reset();
	}
}


iggObjectHandle* iggShell::GetObjectHandle(const iString& name)
{
	int id = mHandles.FindByKey(name);
	if(id > -1)
	{
		return mHandles[id];
	}
	else
	{
		iggObjectHandle *oh = new GenericObjectHandle(name,this); IERROR_CHECK_MEMORY(oh);
		mHandles.Add(oh);
		return oh;
	}
}


//
//  Factory methods
//
iEventObserver* iggShell::CreateShellEventObserver(ShellObserver::Type type)
{
	switch (type)
	{
		case ShellObserver::Execution:
		{
			return new iggExecutionEventObserver(this);
		}
		case ShellObserver::ParallelUpdate:
		{
			return new iggParallelUpdateEventObserver(this);
		}
		default:
		{
			IBUG_FATAL("Invalid ShellObserver type.");
			return 0;
		}
	}
}


iEventObserver* iggShell::CreateViewModuleEventObserver(ViewModuleObserver::Type type, iViewModule *vm) const
{
	IASSERT(vm);

	switch(type)
	{
		case ViewModuleObserver::Render:
		{
			return new iggRenderEventObserver(vm);
		}
		case ViewModuleObserver::Pick:
		{
			return new iggPickEventObserver(vm);
		}
		default:
		{
			IBUG_FATAL("Invalid ViewModuleObserver type.");
			return 0;
		}
	}
}


iDataReaderObserver* iggShell::CreateDataReaderObserver()
{
	return new iggDataReaderObserver(this);
}


vtkRenderWindow* iggShell::CreateRenderWindow(iViewModule *vm, bool stereo) const
{
	IASSERT(vm);
	iggRenderWindow *win = new iggRenderWindow(vm);
	if(win != 0)
	{
		win->SetStereoCapableWindow(stereo?1:0);
	}
	return win;
}


vtkRenderWindowInteractor* iggShell::CreateRenderWindowInteractor(iViewModule *vm) const
{
	IASSERT(vm);
	return new iggRenderWindowInteractor(vm);
}


iViewModule* iggShell::GetViewModule(int mod) const
{
	if(mod == -1) mod = this->GetActiveViewModuleIndex();
	return iShell::GetViewModule(mod);
}


bool iggShell::SetActiveViewModuleIndex(int v)
{
	if(v>=0 && v<mViewModules->Size() && mActiveViewModuleIndex!=v)
	{
		mActiveViewModuleIndex = v;
		this->OnActiveWindowChange();
		return true;
	}
	else return false;
}


int iggShell::GetActiveViewModuleIndex() const
{
	if(mActiveViewModuleIndex >= mViewModules->Size())
	{
		mActiveViewModuleIndex = mViewModules->Size() - 1;
		this->OnActiveWindowChange();
	}

	return mActiveViewModuleIndex;
}


void iggShell::SelectViewModules(iArray<iViewModule*> &arr, int selection)
{
	int j;

	arr.Clear();

	switch(selection)
	{
	case ViewModuleSelection::One:
		{
			arr.Add(this->GetViewModule());
			break;
		}
	case ViewModuleSelection::All:
		{
			for(j=0; j<mViewModules->Size(); j++) arr.Add(mViewModules->GetViewModule(j));
			break;
		}
	case ViewModuleSelection::Clones:
		{
			for(j=0; j<mViewModules->Size(); j++) if(this->GetViewModule()->GetReader() == mViewModules->GetViewModule(j)->GetReader()) arr.Add(mViewModules->GetViewModule(j));
			break;
		}
	}
}


//
//  Use as a callback when the active window changes
//
void iggShell::OnActiveWindowChange() const
{
	int j;

	for(j=0; j<mViewModules->Size(); j++) iDynamicCast<iggRenderWindow>(INFO,mViewModules->GetViewModule(j)->GetRenderWindow())->UpdateActiveStatus(mViewModules->Size()==1);
}


//
//  User notification
//
int iggShell::PopupWindow(const iString &text, int type, const char *button0, const char *button1, const char *button2) const
{
	if(mMainWindow == 0)
	{
		return mSubject->PopupWindow(0,text,type,button0,button1,button2);
	}
	else
	{
		return mSubject->PopupWindow(mMainWindow->GetSubject(),text,type,button0,button1,button2);
	}
}


int iggShell::PopupWindow(const iggDialog *parent, const iString &text, int type, const char *button0, const char *button1, const char *button2) const
{
	return mSubject->PopupWindow(parent->GetSubject(),text,type,button0,button1,button2);
}


int iggShell::PopupWindow(const iggMenuWindow *parent, const iString &text, int type, const char *button0, const char *button1, const char *button2) const
{
	return mSubject->PopupWindow(parent->GetSubject(),text,type,button0,button1,button2);
}


int iggShell::PopupWindow(const iggRenderWindow *parent, const iString &text, int type, const char *button0, const char *button1, const char *button2) const
{
	return mSubject->PopupWindow(parent->GetSubject(),text,type,button0,button1,button2);
}


int iggShell::PopupWindow(const iggExtensionWindow *parent, const iString &text, int type, const char *button0, const char *button1, const char *button2) const
{
	return mSubject->PopupWindow(parent->GetSubject(),text,type,button0,button1,button2);
}

#endif
