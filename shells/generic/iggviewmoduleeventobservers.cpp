/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggviewmoduleeventobservers.h"


#include "ierror.h"
#include "iviewmodule.h"

#include "iggdialogpickerwindow.h"
#include "iggdialogrenderingprogress.h"
#include "iggmainwindow.h"
#include "iggshell.h"


using namespace iParameter;


//
//  iggRenderEventObserver class
//
bool iggRenderEventObserver::CheckAbort()
{
	iggShell *s = iRequiredCast<iggShell>(INFO,this->GetViewModule()->GetShell());
	if(s!=0 && s->GetMainWindow()!=0 && s->GetMainWindow()->GetDialogRenderingProgress()!=0) 
	{
		s->GetMainWindow()->GetDialogRenderingProgress()->KeepWorking(); 
		return s->GetMainWindow()->GetDialogRenderingProgress()->IsCancelled();
	}
	else return false;
}


void iggRenderEventObserver::OnStart()
{ 
	iggShell *s = iRequiredCast<iggShell>(INFO,this->GetViewModule()->GetShell());
	if(s!=0 && s->GetMainWindow()!=0 && s->GetMainWindow()->GetDialogRenderingProgress()!=0) 
	{
		s->GetMainWindow()->GetDialogRenderingProgress()->Start(this->IsInteractive()); 
	}
}


void iggRenderEventObserver::OnFinish()
{ 
	iggShell *s = iRequiredCast<iggShell>(INFO,this->GetViewModule()->GetShell());
	if(s!=0 && s->GetMainWindow()!=0) 
	{
		if(s->GetMainWindow()->GetDialogRenderingProgress() != 0) s->GetMainWindow()->GetDialogRenderingProgress()->Finish();
		if(s->GetMainWindow()->GetDialogPerformanceMeter() != 0) s->GetMainWindow()->GetDialogPerformanceMeter()->UpdateDialog();
	}
}


void iggRenderEventObserver::PostFinished()
{ 
	iggShell *s = iRequiredCast<iggShell>(INFO,this->GetViewModule()->GetShell());
	if(s!=0 && s->GetMainWindow()!=0 && s->GetMainWindow()->GetDialogPerformanceMeter()!=0) 
	{
		s->GetMainWindow()->GetDialogPerformanceMeter()->UpdateDialog();
	}
}


//
//  iggPickEventObserver class
//
void iggPickEventObserver::OnStart()
{
	iggShell *s = iRequiredCast<iggShell>(INFO,this->GetViewModule()->GetShell());
	if(s!=0 && s->GetMainWindow()!=0 && s->GetMainWindow()->GetDialogPickerWindow()!=0) 
	{
		s->GetMainWindow()->GetDialogPickerWindow()->CollectData(true);
		s->GetMainWindow()->GetDialogPickerWindow()->Show(true);
	}
}


void iggPickEventObserver::OnFinish()
{
	iggShell *s = iRequiredCast<iggShell>(INFO,this->GetViewModule()->GetShell());
	if(s!=0 && s->GetMainWindow()!=0 && s->GetMainWindow()->GetDialogPickerWindow()!=0) 
	{
		s->GetMainWindow()->GetDialogPickerWindow()->CollectData(false);
		s->GetMainWindow()->GetDialogPickerWindow()->DisplayData(this->GetViewModule()->GetPicker());
	}
}

#endif
