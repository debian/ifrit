/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggextensionwindow.h"


#include "idata.h"
#include "ierror.h"

#include "iggabstractextension.h"
#include "iggimagefactory.h"
#include "iggmainwindow.h"
#include "iggshell.h"
#include "iggwidgetmisc.h"
#include "iggwidgetotherbutton.h"

#include "ibgextensionwindowsubject.h"
#include "ibgframesubject.h"
#include "ibgwidgetbuttonsubject.h"
#include "ibgwidgethelper.h"
#include "ibgwidgetselectionboxsubject.h"
#include "ibgwindowhelper.h"

#include "iggextensionfactory.h"
#include "iggsubjectfactory.h"

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"


using namespace iParameter;


class iggExtensionWindowHelper
{

public:

	iggExtensionWindowHelper(ibgWindowHelper *window)
	{
		mWindow = window;
		mIndex = 2;
	}

	void AddWidget(iggWidget *w)
	{
		mWidgets.Add(w);
	}

	void RemoveWidget(iggWidget *w)
	{
		mWidgets.Remove(w);
	}

	void Rotate()
	{
		int oldIndex = mIndex++;
		if(mIndex == mWidgets.Size()) mIndex = 0;
		mWidgets[oldIndex]->Show(false);
		if(mWidgets[mIndex]->GetParent() != mWidgets[oldIndex])
		{
			mWidgets[oldIndex]->GetParent()->Show(false);
			mWidgets[mIndex]->GetParent()->Show(true);
		}
		mWidgets[mIndex]->Show(true);

		int wg[4];
		mWindow->GetWindowGeometry(wg,true);
		wg[2] = wg[3] = 0;
		mWindow->SetWindowGeometry(wg,true);
	}

private:

	int mIndex;
	ibgWindowHelper *mWindow;
	iLookupArray<iggWidget*> mWidgets;
};


namespace iggExtensionWindow_Private
{
	class RotateButton : public iggWidgetSimpleButton
	{

	public:

		RotateButton(iggExtensionWindowHelper *helper, iggFrame *parent) : iggWidgetSimpleButton("",parent,true)
		{
			mHelper = helper;
			mSubject->SetIcon(*iggImageFactory::FindIcon("cycle.png"));
			this->SetBaloonHelp("Try out a different window layout");
		}

	protected:
		
		virtual void Execute()
		{
			mHelper->Rotate();
		}

		iggExtensionWindowHelper *mHelper;
	};


	class BookControl : public iggFrameBook
	{

	public:

		BookControl(iggExtensionWindowHelper *helper, iggFrameFlip *panel, iggFrame *parent) : iggFrameBook(parent,true,1), mPanel(panel)
		{
			helper->AddWidget(this);
		}

		~BookControl()
		{
		}

		void AddExtension(iggAbstractExtension *ext)
		{
			iggFrame *tmp = new iggFrame(this);
			this->AddPage(ext->GetTitle(),ext->GetIcon(),tmp);
			tmp->SetPadding(false);
		}

	protected:

		virtual void OnInt1Body(int n)
		{
			mPanel->ShowLayer(n);
		}

		virtual void UpdateWidgetBody()
		{
			this->OpenPage(mPanel->GetActiveLayer());
		}

	private:

		iggFrameFlip *mPanel;
	};


	class SelectionControlButton : public iggWidget
	{

	public:

		SelectionControlButton(int index, iggWidgetTextLabel *title, iggAbstractExtension *ext, iggFrameFlip *panel, iggFrame *parent) : iggWidget(parent), mIndex(index), mPanel(panel)
		{
			mTitle = title;
			mSubject = iggSubjectFactory::CreateWidgetButtonSubject(this,ButtonType::ToggleButton,"",1);
			if(ext->GetIcon() != 0) mSubject->SetIcon(*ext->GetIcon());
			this->AddDependent(parent);
			this->SetBaloonHelp("Select current extension");
	}

	protected:

		virtual void OnVoid1Body()
		{
			mPanel->ShowLayer(mIndex);
		}

		virtual void UpdateWidgetBody()
		{
			static const iColor c(246,144,0);

			if(mIndex == mPanel->GetActiveLayer())
			{
				mTitle->GetHelper()->SetBackgroundColor(c);
				mSubject->SetDown(true);
			}
			else
			{
				mTitle->GetHelper()->UnSetBackgroundColor();
				mSubject->SetDown(false);
			}
		}

	private:

		const int mIndex;
		iggFrameFlip *mPanel;
		iggWidgetTextLabel *mTitle;
		ibgWidgetButtonSubject *mSubject;
	};


	class SelectionControlRadioBox : public iggWidget
	{

	public:

		SelectionControlRadioBox(iggFrameFlip *panel, iggFrame *parent) : iggWidget(parent), mPanel(panel)
		{
			mSubject = iggSubjectFactory::CreateWidgetRadioBoxSubject(this,1,"");
			this->SetBaloonHelp("Select current extension");
			this->AddDependent(parent);
		}

		void AddExtension(iggAbstractExtension *ext)
		{
			if(ext->GetIcon() != 0)
			{
				iImage im(*ext->GetIcon());
				im.Scale(22,22,iImage::_Both,iImage::_Smooth);
				mSubject->InsertItem(im,ext->GetTitle());
			}
			else mSubject->InsertItem(ext->GetTitle());
		}

	protected:

		virtual void OnInt1Body(int n)
		{
			mPanel->ShowLayer(n);
		}

		virtual void UpdateWidgetBody()
		{
			mSubject->SetValue(mPanel->GetActiveLayer());
		}

	private:

		iggFrameFlip *mPanel;
		ibgWidgetRadioBoxSubject *mSubject;
	};


	class SelectionControlComboBox : public iggWidget
	{

	public:

		SelectionControlComboBox(iggFrameFlip *panel, iggFrame *parent) : iggWidget(parent), mPanel(panel)
		{
			mSubject = iggSubjectFactory::CreateWidgetComboBoxSubject(this,"");
			this->SetBaloonHelp("Select current extension");
			this->AddDependent(parent);
		}

		void AddExtension(iggAbstractExtension *ext)
		{
			if(ext->GetIcon() != 0)
			{
				iImage im(*ext->GetIcon());
				im.Scale(22,22);
				mSubject->InsertItem(im,ext->GetTitle());
			}
			else mSubject->InsertItem(ext->GetTitle());
		}

	protected:

		virtual void OnInt1Body(int n)
		{
			mPanel->ShowLayer(n);
		}

		virtual void UpdateWidgetBody()
		{
			mSubject->SetValue(mPanel->GetActiveLayer());
		}

	private:

		iggFrameFlip *mPanel;
		ibgWidgetComboBoxSubject *mSubject;
	};


	class SelectionControlEntryBox : public iggFrame
	{

	public:

		SelectionControlEntryBox(iggFrameFlip *panel, iggFrame *parent) : iggFrame(parent,2), mPanel(panel)
		{
			mIndex = 0;
			this->AddDependent(parent);
		}

		inline int Count() const { return mIndex; }

		void AddExtension(iggAbstractExtension *ext)
		{
			iggWidgetTextLabel *t = new iggWidgetTextLabel(" "+ext->GetTitle(),this);
			t->SetAlignment(-1);
			this->AddLine(new SelectionControlButton(mIndex++,t,ext,mPanel,this),t);
		}

	private:

		int mIndex;
		iggFrameFlip *mPanel;
	};


	class LeftPanel : public iggFrame
	{

	public:

		LeftPanel(iggExtensionWindowHelper *helper, iggFrameFlip *panel) : iggFrame(panel->GetShell(),2)
		{
			mTitle = new iggWidgetTextLabel("%b%+Select extension",this);
			mTitle->SetAlignment(-1);
			mComboBox = new SelectionControlComboBox(panel,this);
			mRadioBox = new SelectionControlRadioBox(panel,this);
			mEntryBox = new SelectionControlEntryBox(panel,this);

			this->AddLine(mTitle);
			this->AddLine(mComboBox);
			this->AddLine(mRadioBox);
			this->AddLine(mEntryBox);
			this->AddSpace(10);

			iggFrame *tmp = new iggFrame(this,2);
			tmp->AddLine(new RotateButton(helper,tmp));
			tmp->SetColStretch(1,10);

			this->AddLine(tmp);

			mHelper = helper;
			mHelper->AddWidget(mComboBox);
			mHelper->AddWidget(mRadioBox);
			mHelper->AddWidget(mEntryBox);

			mComboBox->Show(false);
			mRadioBox->Show(false);
		}

		void AddExtension(iggAbstractExtension *ext)
		{
			mComboBox->AddExtension(ext);
			mRadioBox->AddExtension(ext);
			mEntryBox->AddExtension(ext);

			if(mEntryBox->Count() == 5)
			{
				mHelper->RemoveWidget(mRadioBox);
				mHelper->RemoveWidget(mEntryBox);
				mComboBox->Show(true);
				mRadioBox->Show(false);
				mEntryBox->Show(false);
			}
		}

	private:

		iggWidgetTextLabel *mTitle;
		iggExtensionWindowHelper *mHelper;
		SelectionControlComboBox *mComboBox;
		SelectionControlRadioBox *mRadioBox;
		SelectionControlEntryBox *mEntryBox;
	};
};


using namespace iggExtensionWindow_Private;


//
//  Helper class
//
class iggExtensionCreatorPointer : public iPointer::Ordered<iggExtensionCreator>
{

public:

	iggExtensionCreatorPointer(iggExtensionCreator *ptr = 0) : iPointer::Ordered<iggExtensionCreator>(ptr){}
};


//
//  Main class
//
iArray<iggExtensionCreatorPointer>& iggExtensionWindow::Creators()
{
	static iOrderedArray<iggExtensionCreatorPointer> arr;
	return arr;
}


void iggExtensionWindow::AttachCreator(iggExtensionCreator *c)
{
	if(c != 0) Creators().Add(c);
}


iggExtensionWindow::iggExtensionWindow(iggMainWindow *mw) : iggElement(mw?mw->GetShell():0)
{
	IASSERT(mw);
	mMainWindow = mw; 

	//
	//  Create subject
	//
	mSubject = iggSubjectFactory::CreateExtensionWindowSubject(this);
	mFrames[0] = new iggFrame(this->GetShell()); IERROR_CHECK_MEMORY(mFrames[0]);
	mFrames[0]->SetPadding(false);

	mMainPanel = new iggFrameFlip(mFrames[0]); IERROR_CHECK_MEMORY(mMainPanel);
	mFrames[0]->AddLine(mMainPanel);

	mHelper = new iggExtensionWindowHelper(mSubject->GetHelper()); IERROR_CHECK_MEMORY(mHelper);
}


void iggExtensionWindow::CompleteInitialization()
{
	//
	//  Query creators for all extensions, in order of their ids
	//
	int i;
	for(i=0; i<Creators().Size(); i++)
	{
		mExt.Add(Creators()[i]->Create(this->GetMainWindow()));
	}

	//
	//  Layout widgets
	//
	LeftPanel *leftPanel = new LeftPanel(mHelper,mMainPanel);
	iggFrame *bottomPanel = new iggFrame(this->GetShell(),2);
	BookControl *bookControl = new BookControl(mHelper,mMainPanel,bottomPanel);

	iggFrame *tmp = new iggFrame(bottomPanel);
	tmp->AddSpace(1);
	tmp->AddLine(new RotateButton(mHelper,tmp));

	bottomPanel->AddLine(tmp,bookControl);
	bottomPanel->SetColStretch(1,10);

	mSubject->AttachPanels(mFrames[0],bottomPanel,leftPanel);

	leftPanel->AddBuddy(bookControl);

	//
	//  Attach extensions
	//
	if(mExt.Size() > 0)
	{
		for(i=0; i<mExt.Size(); i++)
		{
			mMainPanel->AddLayer(mExt[i]);
			leftPanel->AddExtension(mExt[i]);
			bookControl->AddExtension(mExt[i]);
			mExt[i]->SetPadding(false);
			mExt[i]->CompleteInitialization();
		}
		mMainPanel->ShowLayer(0);
	}

	bottomPanel->Show(false);
	if(mExt.Size() == 1)
	{
		leftPanel->Show(false);
	}

	mFrames[1] = leftPanel;
	mFrames[2] = bottomPanel;
}

	
iggExtensionWindow::~iggExtensionWindow()
{
	while(mExt.Size() > 0) delete mExt.RemoveLast();
	delete mHelper;
	delete mFrames[2];
	delete mFrames[1];
	delete mFrames[0];
	mSubject->Delete();
}


//
//  Special functions
//
void iggExtensionWindow::UpdateAll()
{
	int i;
	for(i=0; i<mExt.Size(); i++) mExt[i]->UpdateWidget();
	mFrames[0]->UpdateWidget();
	mFrames[1]->UpdateWidget();
	mFrames[2]->UpdateWidget();
}


bool iggExtensionWindow::IsEmpty() const
{
	return mExt.Size() == 0;
}


void iggExtensionWindow::SetPadding(bool s)
{
	mFrames[0]->SetPadding(s);
}


void iggExtensionWindow::OpenBookPageByIndex(int index)
{
	int i;
	for(i=0; i<mExt.Size(); i++) mExt[i]->OpenBookPageByIndex(index);
}


void iggExtensionWindow::SetTabMode(int m)
{
	int i;
	for(i=0; i<mExt.Size(); i++) mExt[i]->SetTabMode(m);
}


void iggExtensionWindow::PopulateFileMenu()
{
	int i;
	for(i=0; i<mExt.Size(); i++) mExt[i]->PopulateFileMenu();
}


void iggExtensionWindow::PopulateLocalDialogMenu()
{
	int i;
	for(i=0; i<mExt.Size(); i++) mExt[i]->PopulateLocalDialogMenu();
}


void iggExtensionWindow::PopulateGlobalDialogMenu()
{
	int i;
	for(i=0; i<mExt.Size(); i++) mExt[i]->PopulateGlobalDialogMenu();
}


void iggExtensionWindow::PopulateFileToolBar(bool postonly)
{
	int i;
	for(i=0; i<mExt.Size(); i++) if(!postonly || !mExt[i]->CanPrePopulateToolBar())
	{
		mExt[i]->PopulateFileToolBar();
	}
}


void iggExtensionWindow::PrePopulateFileToolBar()
{
	int i;
	for(i=0; i<mExt.Size(); i++) if(mExt[i]->CanPrePopulateToolBar())
	{
		mExt[i]->PopulateFileToolBar();
	}
}


void iggExtensionWindow::PopulateShowToolBar()
{
	int i;
	for(i=0; i<mExt.Size(); i++) mExt[i]->PopulateShowToolBar();
}


bool iggExtensionWindow::OnMenuBody(int id, bool on)
{
	int i;
	for(i=0; i<mExt.Size(); i++) if(mExt[i]->OnMenuBody(id,on)) return true;
	return false;
}


void iggExtensionWindow::UpdateOnPick()
{
	int i;
	for(i=0; i<mExt.Size(); i++) mExt[i]->UpdateOnPick();
}


void iggExtensionWindow::UpdateParticleWidgets(const iImage *icon)
{
	int i;
	for(i=0; i<mExt.Size(); i++) mExt[i]->UpdateParticleWidgets(icon);
}


void iggExtensionWindow::AddReloadingDataTypes(iggPageData *page)
{
	int i;
	for(i=0; i<mExt.Size(); i++) mExt[i]->AddReloadingDataTypes(page);
}


const iImage* iggExtensionWindow::GetSpecialParticleIcon(const iDataType &type) const
{
	int i;
	const iImage* image = 0;

	for(i=0; i<mExt.Size(); i++)
	{
		image = mExt[i]->GetSpecialParticleIcon(type);
		if(image != 0) return image;
	}
	return image;
}


void iggExtensionWindow::OnCloseAttempt()
{
	this->GetShell()->PopupWindow(this,"Extension Window can not be closed. If it is on the way, it can be minimized.",MessageType::Warning);
}


void iggExtensionWindow::UpdateOnDataType(const iDataType &type)
{
	int i;
	
	for(i=0; i<mExt.Size(); i++)
	{
		if(mExt[i]->IsUsingData(type))
		{
			mExt[i]->GetBook()->Block(true);
			mMainPanel->ShowLayer(i);
			this->UpdateAll();
			mExt[i]->GetBook()->Block(false);
			break;
		}
	}
}

#endif
