/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggpageobjectextension.h"


#include "idata.h"
#include "ierror.h"
#include "iviewobject.h"
#include "iviewsubject.h"

#include "igghandle.h"
#include "iggmainwindow.h"
#include "iggshell.h"
#include "iggpageobject.h"


namespace iggPageObjectExtension_Private
{
	//
	//  Returns the current VS
	//
	class ViewSubjectHandle : public iggObjectHandle
	{

	public:

		ViewSubjectHandle(iggPageObjectExtension *owner) : iggObjectHandle(owner)
		{
			mOwner = owner; IASSERT(mOwner);
		}

		virtual const iObject* Object(int mod = -1) const
		{
			return mOwner->GetViewSubject(mod);
		}

	private:

		iggPageObjectExtension *mOwner;
	};
};


using namespace iggPageObjectExtension_Private;


iggPageObjectExtension::iggPageObjectExtension(iggFrameBase *parent, int pageId, const iDataType &type, int flags) : iggPage(parent,flags), mPageId(pageId), mDataType(type)
{
	mOwner = this->GetMainWindow()->GetPage(mPageId); IASSERT(mOwner);

	mSubjectHandle = new ViewSubjectHandle(this);
}


iggPageObjectExtension::~iggPageObjectExtension()
{
	delete mSubjectHandle;
}


const iggPropertyHandle iggPageObjectExtension::CreatePropertyHandle(const iString &name) const
{
	return iggPropertyHandle(name,mSubjectHandle,mOwner->GetInstanceHandle());
}


iViewSubject* iggPageObjectExtension::GetViewSubject(int mod) const
{
	iViewSubject *sub = mOwner->GetViewSubject(mod);

	if(mDataType == sub->GetDataType())
	{
		return sub;
	}
	else
	{
		return mOwner->GetViewObject(mod)->GetSubject(mDataType);
	}
}


int iggPageObjectExtension::GetActiveInstance() const
{
	return mOwner->GetActiveInstance();
}


const iImage* iggPageObjectExtension::Icon() const
{
	return mOwner->Icon();
}


void iggPageObjectExtension::UpdateWidgetBody()
{
	if(this->GetViewSubject(-1) == mOwner->GetViewSubject(-1))
	{
		this->Enable(true);
		this->iggPage::UpdateWidgetBody();
	}
	else this->Enable(false);
}

#endif
