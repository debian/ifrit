/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggframe.h"


#include "iimage.h"
#include "isystem.h"

#include "iggmainwindow.h"
#include "iggrenderwindowobserver.h"
#include "iggshell.h"
#include "iggwidget.h"

#include "ibgframesubject.h"

#include "iggsubjectfactory.h"

//
//  Templates
//
#include "iarray.tlh"


using namespace iParameter;


//
//  Base Frame class
//
iggFrameBase::iggFrameBase(iggFrameBase *parent) : iggWidget(parent)
{
	this->Define();
}


iggFrameBase::iggFrameBase(iggShell *shell) : iggWidget(shell)
{
	mWasLaidOut = true;
	this->Define();
}


void iggFrameBase::Define()
{
	mNeedsBaloonHelp = false;
	mNeedsUpdate = true;
}


iggFrameBase::~iggFrameBase()
{
	//
	//  Make sure our help status is up-to-date
	//
	this->CheckBaloonHelpStatus();
	//
	//  Delete all children (they unregisted themselves)
	//
	while(mChildren.Size() > 0)
	{
		delete mChildren.Last();
	}
}


bool iggFrameBase::CheckBaloonHelpStatus()
{
	if(mNeedsBaloonHelp)
	{
		int i;
		bool ok = true;
		for(i=0; ok && i<mChildren.Size(); i++)
		{
			if(!mChildren[i]->CheckBaloonHelpStatus())
			{
				ok = false;
#ifndef I_NO_CHECK
				mChildren[i]->CheckBaloonHelpStatus();
#endif
			}
		}
		if(ok) mNeedsBaloonHelp = false; // all children have their own help
	}
	return this->iggWidget::CheckBaloonHelpStatus();
}


void iggFrameBase::RegisterChild(iggWidget *child)
{
	int i;

	if(child != 0)
	{
		mChildren.AddUnique(child);
		//
		//  Add already set dependencies to this child
		//
		for(i=0; i<mDependents.Size(); i++)
		{
			child->AddDependent(mDependents[i]);
		}
	}
}


void iggFrameBase::UnRegisterChild(iggWidget *child)
{
	if(child != 0) mChildren.Remove(child);
}


void iggFrameBase::RegisterRenderWindowObserver(iggRenderWindowObserver *rwo)
{
	if(rwo != 0) mRenderWindowObservers.AddUnique(rwo);
}


void iggFrameBase::UnRegisterRenderWindowObserver(iggRenderWindowObserver *rwo)
{
	if(rwo != 0) mRenderWindowObservers.Remove(rwo);
}


//
//  Frames do not have buddies - their widgets do
//
void iggFrameBase::AddDependent(iggWidget *w)
{
	int i;
	for(i=0; i<mChildren.Size(); i++)
	{
		mChildren[i]->AddDependent(w); // when buddy changes, we update through the parent frame
	}
	iggWidget::AddDependent(w);
}


void iggFrameBase::RemoveDependent(iggWidget *w)
{
	int i;
	for(i=0; i<mChildren.Size(); i++)
	{
		mChildren[i]->RemoveDependent(w);
	}
	iggWidget::RemoveDependent(w);
}


void iggFrameBase::UpdateWidgetBody()
{
	mNeedsUpdate = true;
	if(this->IsVisible()) this->UpdateChildren();
}


//
//  Update all children
//
void iggFrameBase::UpdateChildren()
{
	if(mNeedsUpdate)
	{
		int i;
		mNeedsUpdate = false;
		for(i=0; i<mChildren.Size(); i++)
		{
			mChildren[i]->UpdateWidget();
		}
		for(i=0; i<mRenderWindowObservers.Size(); i++)
		{
			mRenderWindowObservers[i]->UpdateConnection();
		}
	}
}


//
//  Pass slot calls to parent
//
void iggFrameBase::OnInt1Body(int v)
{
	if(mParent != 0) mParent->OnInt1Body(v);
}


void iggFrameBase::OnInt2Body(int v)
{
	if(mParent != 0) mParent->OnInt2Body(v);
}


void iggFrameBase::OnVoid1Body()
{
	if(mParent != 0) mParent->OnVoid1Body();
}


void iggFrameBase::OnVoid2Body()
{
	if(mParent != 0) mParent->OnVoid2Body();
}


void iggFrameBase::OnVoid3Body()
{
	if(mParent != 0) mParent->OnVoid3Body();
}


void iggFrameBase::OnBool1Body(bool v)
{
	if(mParent != 0) mParent->OnBool1Body(v);
}


void iggFrameBase::OnChildFlipped()
{
	iSystem::Sleep(30);
	this->GetMainWindow()->ProcessEvents();
}


//
//  Plain Frame class with optional border and title
//
iggFrame::iggFrame(const iString& title, iggFrameBase *parent, int cols) : iggFrameBase(parent)
{
	this->Define(cols,true);
	this->ShowFrame(true);
	this->SetPadding(true);
	this->SetTitle(title);
}


iggFrame::iggFrame(iggFrameBase *parent, int cols) : iggFrameBase(parent)
{
	this->Define(cols,true);
}


iggFrame::iggFrame(iggShell *shell, int cols, bool withsubject) : iggFrameBase(shell)
{
	mWasLaidOut = true;
	this->Define(cols,withsubject);
}


void iggFrame::Define(int cols, bool withsubject)
{
	if(withsubject)
	{
		mSubject = iggSubjectFactory::CreateFrameSubject(this,cols);
	}
	else
	{
		mSubject = 0;
	}

	mCurCol = mCurRow = 0;
}


bool iggFrame::NeedsRedoLayout()
{
	return false;
}


void iggFrame::GetFrameGeometry(int wg[4]) const
{
	mSubject->GetFrameGeometry(wg);
}


void iggFrame::SetTitle(const iString &title)
{
	mSubject->SetTitle(title);
}


void iggFrame::ShowFrame(bool s)
{
	mSubject->ShowFrame(s);
}


//
//  Layout helpers
//
void iggFrame::SetPadding(bool s)
{
	mSubject->SetPadding(s);
}


void iggFrame::SetColStretch(int col, int s)
{
	mSubject->SetColStretch(col,s);
}


void iggFrame::SetRowStretch(int row, int s)
{
	mSubject->SetRowStretch(row,s);
}


void iggFrame::AddLine(iggWidget *child1, iggWidget *child2, iggWidget *child3, iggWidget *child4, iggWidget *child5, iggWidget *child6, iggWidget *child7)
{
	this->AddLine(child1,1,child2,1,child3,1,child4,1,child5,1,child6,1,child7,1);
}


void iggFrame::AddLine(iggWidget *child1, int nc1, iggWidget *child2, int nc2, iggWidget *child3, int nc3, iggWidget *child4, int nc4, iggWidget *child5, int nc5, iggWidget *child6, int nc6, iggWidget *child7, int nc7)
{
	if((child1!=0 && child1->GetParent()!=this) || (child2!=0 && child2->GetParent()!=this) || (child3!=0 && child3->GetParent()!=this) || (child4!=0 && child4->GetParent()!=this) || (child5!=0 && child5->GetParent()!=this) || (child6!=0 && child6->GetParent()!=this) || (child7!=0 && child7->GetParent()!=this))
	{
		IBUG_WARN("Incorrect parenting in iggFrame class,");
		return;
	}
	
	int c = 0;
	this->InsertWidget(c,child1,nc1);
	this->InsertWidget(c,child2,nc2);
	this->InsertWidget(c,child3,nc3);
	this->InsertWidget(c,child4,nc4);
	this->InsertWidget(c,child5,nc5);
	this->InsertWidget(c,child6,nc6);
	this->InsertWidget(c,child7,nc7);

	mCurRow++;
	mCurCol = 0;
}


void iggFrame::InsertWidget(int &c, iggWidget *child, int nc)
{
	if(nc > 0)
	{
		if(child != 0)
		{
			mSubject->PlaceWidget(c,mCurRow,child->mWidgetSubject,nc,true);
			child->mWasLaidOut = true;
		}
		c += nc;
	}
}


void iggFrame::AddSpace(int space)
{
	mSubject->PlaceWidget(0,mCurRow,0,1,true);
	mSubject->SetRowStretch(mCurRow++,space);
	mCurCol = 0;
}


//
//  Allow selected widgets to clear the laid-out flag
//
void iggFrame::ClearLaidOutFlag(const iggFrameBook *book)
{
	if(book!=0 && book->GetLastPage()==this) mWasLaidOut = true;
}


void iggFrame::ClearLaidOutFlag(const iggFrameFlip *flip)
{
	if(flip!=0 && flip->GetLastLayer()==this) mWasLaidOut = true;
}


void iggFrame::ClearLaidOutFlag(const iggFrameScroll *scroll)
{
	if(scroll!=0 && scroll->GetContents()==this) mWasLaidOut = true;
}


void iggFrame::FlipThroughAllChildren()
{
	int i;
	iggFrameBase *c;
    bool v = this->IsVisible();
	
	if(!v) this->Show(true);
	
	this->OnChildFlipped();
	for(i=0; i<mChildren.Size(); i++)
	{
		c = dynamic_cast<iggFrameBase*>(mChildren[i]);
		if(c != 0) c->FlipThroughAllChildren();
	}

	if(!v) this->Show(false);
}


//
//  A frame with delayed construction.
//
iggFrameDC::iggFrameDC(iggFrameBase *parent, int cols) : iggFrame(parent,cols)
{
	mCompleteConstructionCalled = false;
}


bool iggFrameDC::ImmediateConstruction() const
{
	return this->GetShell()->CheckCondition(iParameter::Condition::DisableDelayedInitialization);
}


bool iggFrameDC::NeedsRedoLayout()
{
	if(mCompleteConstructionCalled)
	{
		return false;
	}
	else
	{
		this->CompleteConstruction();
		return true;
	}
}


void iggFrameDC::CompleteConstruction()
{
	if(mCompleteConstructionCalled) return;

	this->CompleteConstructionBody();
	mCompleteConstructionCalled = true;
#ifdef I_DEBUG
	iOutput::DisplayDebugMessage(iString("Completing delayed construction for element #")+iString::FromNumber(this->GetId()));
#endif
}


//
//  FrameBook class
//
iggFrameBook::iggFrameBook(iggFrameBase *parent, bool withFeedback, int orientation) : iggFrameBase(parent)
{
	mSubject = iggSubjectFactory::CreateFrameBookSubject(this,withFeedback);
	mSubject->SetOrientation(orientation);

	mNeedsBaloonHelp = false;
}


iggFrame* iggFrameBook::GetPage(int i) const
{
	if(i>=0 && i<mPages.Size()) return mPages[i].Frame; else return 0;
}


const iggFrame* iggFrameBook::GetLastPage() const
{
	return mPages.Last().Frame;
}


void iggFrameBook::OpenPage(int i)
{
	if(i>=0 && i<mPages.Size())
	{
		mSubject->OpenPage(i);
	}
	else
	{
		IBUG_WARN("Trying to open an non-existent page.");
	}
}


void iggFrameBook::AddPage(const iString &title, const iImage *image, iggFrame *frame, int index)
{
	Page tmp;

	if(frame->GetParent() != this)
	{
		IBUG_FATAL("Icorrect parenting in iggFrameBook.");
	}

	frame->SetPadding(true);

	mSubject->AddPage(title,image,frame);

	tmp.Title = title;
	tmp.Image = image;
	tmp.Frame = frame;
	tmp.Index = index;
	mPages.Add(tmp);

	frame->ClearLaidOutFlag(this);
}


void iggFrameBook::ChangeIcon(int page, const iImage *image)
{
	int n;

	if(page>=0 && page<mPages.Size())
	{
		mPages[page].Image = image;
		if(image != 0) mSubject->ChangeIcon(page,*image);
	}
	else
	{
		for(n=0; n<mPages.Size(); n++)
		{
			mPages[n].Image = image;
			if(image != 0) mSubject->ChangeIcon(n,*image);
		}
	}
}


void iggFrameBook::SetTabMode(int m)
{
	int n;

	if(m<0 || m>2) IBUG_FATAL("Invalid Book tab mode.");

	for(n=0; n<mPages.Size(); n++)
	{
		mSubject->SetTabMode(n,m,mPages[n].Title,mPages[n].Image);
	}
}


int iggFrameBook::GetTabMode() const
{
	return mSubject->GetTabMode();
}


void iggFrameBook::EnablePage(int n, bool s)
{
	if(n>=0 && n<mPages.Size()) mPages[n].Frame->Enable(s);
}


void iggFrameBook::FlipThroughAllChildren()
{
	int i;

	for(i=0; i<mPages.Size(); i++)
	{
		this->OpenPage(i);
		mPages[i].Frame->FlipThroughAllChildren();	
	}
}


void iggFrameBook::SetOrientation(int v)
{
	int i, j;

	for(i=0; i<mPages.Size(); i++)
	{
		for(j=0; j<mPages[i].Frame->mChildren.Size(); j++)
		{
			iggFrameBook *b = dynamic_cast<iggFrameBook*>(mPages[i].Frame->mChildren[j]);
			if(b != 0) b->SetOrientation(v);
		}
	}

	mSubject->SetOrientation(v);
}


int iggFrameBook::GetOrientation() const
{
	return mSubject->GetOrientation();
}

//
//  FrameFlip class 
//
iggFrameFlip::iggFrameFlip(iggFrameBase *parent, bool expanding) : iggFrameBase(parent)
{
	mSubject = iggSubjectFactory::CreateFrameFlipSubject(this,expanding);
	mNeedsBaloonHelp = false;

	mActiveLayer = -1;
}

int iggFrameFlip::Count() const
{
	return mLayers.Size();
}


void iggFrameFlip::ShowLayer(int i)
{
	if(i>=0 && i<mLayers.Size())
	{
		mSubject->ShowLayer(i);
		mActiveLayer = i;
	}
}


void iggFrameFlip::AddLayer(iggFrame *page)
{
	if(page->GetParent() != this)
	{
		IBUG_FATAL("Icorrect parenting in iggFrameFlip.");
	}

	mLayers.Add(page);
	mSubject->AddLayer(page);
	page->ClearLaidOutFlag(this);
}


void iggFrameFlip::FlipThroughAllChildren()
{
	int i;

	for(i=0; i<mLayers.Size(); i++)
	{
		this->ShowLayer(i);
		mLayers[i]->FlipThroughAllChildren();
	}
}


//
//  FrameScroll class
//
iggFrameScroll::iggFrameScroll(iggFrameBase *parent, bool withHor, int numcol, bool withVer) : iggFrameBase(parent)
{
	mSubject = iggSubjectFactory::CreateFrameScrollSubject(this,withHor,withVer);
	mContents = new iggFrame(this,numcol);
	mSubject->AttachContents(mContents);
	mContents->ClearLaidOutFlag(this);

	mNeedsBaloonHelp = false;
}


void iggFrameScroll::FlipThroughAllChildren()
{
	mContents->FlipThroughAllChildren();
}


//
//  FrameSplit class
//
iggFrameSplit::iggFrameSplit(iggFrameBase *parent, bool hor) : iggFrameBase(parent)
{
	mSubject = iggSubjectFactory::CreateFrameSplitSubject(this,hor);

	mNeedsBaloonHelp = false;
}


void iggFrameSplit::AddSegment(iggFrame *frame)
{
	if(frame != 0)
	{
		mSegments.Add(frame);
		mSubject->AddSegment(frame);
	}
}


void iggFrameSplit::FlipThroughAllChildren()
{
	int i;

	for(i=0; i<mSegments.Size(); i++)
	{
		mSegments[i]->FlipThroughAllChildren();
	}
}

#endif
