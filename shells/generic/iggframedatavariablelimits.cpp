/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggframedatavariablelimits.h"


#include "idatalimits.h"
#include "idatasubject.h"
#include "ierror.h"
#include "istretch.h"

#include "iggframedatavariablelist.h"
#include "igghandle.h"
#include "iggshell.h"
#include "iggwidgetmisc.h"
#include "iggwidgetpropertycontrollineedit.h"
#include "iggwidgetpropertycontrolselectionbox.h"
#include "iggwidgetpropertycontrolslider.h"

//
//  Templates
//
#include "iarray.tlh"
#include "iggwidgetpropertycontrollineedit.tlh"
#include "iggwidgetpropertycontrolslider.tlh"


using namespace iParameter;


namespace iggFrameDataVariableLimits_Private
{
	class LimitsStretchComboBox : public iggWidgetPropertyControlStretchComboBox
	{
		
	public:
		
		LimitsStretchComboBox(const iggPropertyHandle &ph, iggFrame *parent, bool title = true) : iggWidgetPropertyControlStretchComboBox(ph,parent,RenderMode::Immediate,title)
		{
			this->SetRenderTarget(ViewModuleSelection::Clones);
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			this->iggWidgetPropertyControlStretchComboBox::UpdateWidgetBody();

			const iDataSubject *sub = dynamic_cast<const iDataSubject*>(this->Handle()->Object());
			if(sub == 0)
			{
				IBUG_ERROR("iggFrameDataVariableLimits::LimitsStretchComboBox is incorrectly configured.");
				return;
			}
			iDataLimits *lim = sub->GetLimits();

			if(lim != 0) this->Enable(!lim->GetStretchFixed(this->Handle()->Index())); else this->Show(false);
		}

		virtual void OnInt1Body(int i)
		{
			iggWidgetPropertyControlStretchComboBox::OnInt1Body(i);
			mParent->UpdateWidget();
		}
	};


	class LimitsNameEdit : public iggWidgetPropertyControlTextLineEdit
	{
		
	public:
		
		LimitsNameEdit(iggFrameDataVariableLimits *owner, const iggPropertyHandle &ph, iggFrame *parent, bool title = true) : iggWidgetPropertyControlTextLineEdit(false,title?"Name":"",ph,parent)
		{
			IASSERT(owner);
			mOwner = owner;
			this->SetRenderTarget(ViewModuleSelection::Clones);
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			this->iggWidgetPropertyControlTextLineEdit::UpdateWidgetBody();

			const iDataSubject *sub = dynamic_cast<const iDataSubject*>(this->Handle()->Object());
			if(sub == 0)
			{
				IBUG_ERROR("iggFrameDataVariableLimits::LimitsNameEdit is incorrectly configured.");
				return;
			}
			iDataLimits *lim = sub->GetLimits();

			if(lim != 0) this->Show(!lim->IsFrozen()); else this->Show(false);
		}

		virtual void OnVoid1Body()
		{
			iggWidgetPropertyControlTextLineEdit::OnVoid1Body();
			mOwner->UpdateWidget();
		}

		iggFrameDataVariableLimits *mOwner;
	};


	class LimitsRangeSlider : public iggWidgetPropertyControlVariableLimitsSlider
	{
		
	public:
		
		LimitsRangeSlider(bool min, const iggObjectHandle *dsh, const iggPropertyHandle &ph, iggFrame *parent) : iggWidgetPropertyControlVariableLimitsSlider(min?-1:+1,3,min?"Min":"Max",dsh,ph,parent)
		{
		}
	};


	class ListIndexHandle: public iggIndexHandle
	{

	public:

		ListIndexHandle(const iggPropertyHandle& ph) : mHandle(ph)
		{
		}

		virtual int Index() const
		{
			const iPropertyDataVariable<iType::Int> *var = iDynamicCast<const iPropertyDataVariable<iType::Int> >(INFO,mHandle.Variable());
			int idx = mHandle.Index();
			if(var==0 || idx<0 || idx>=var->Size())
			{
				//
				//  This should not happen if we are enabled
				//
				IBUG_ERROR("Invalid index value for property "+mHandle.Name());
				return -1;
			}
			
			return var->GetValue(idx);
		}

	private:

		const iggPropertyHandle mHandle;
	};
};


using namespace iggFrameDataVariableLimits_Private;


iggFrameDataVariableLimits::iggFrameDataVariableLimits(int index, const iggObjectHandle *dsh, iggFrameBase *parent, const iggPropertyHandle* eh, bool narrow) : iggFrame("",parent)
{
	mIndexHandle = new iggFixedIndexHandle(index); IERROR_CHECK_MEMORY(mIndexHandle);

	this->Define(dsh,eh,narrow);
}


iggFrameDataVariableLimits::iggFrameDataVariableLimits(const iggPropertyHandle& ph, const iggObjectHandle *dsh, iggFrameBase *parent, const iggPropertyHandle* eh, bool narrow) : iggFrame("",parent)
{
	mIndexHandle = new ListIndexHandle(ph); IERROR_CHECK_MEMORY(mIndexHandle);

	this->Define(dsh,eh,narrow);
}

	
iggFrameDataVariableLimits::~iggFrameDataVariableLimits()
{
	if(mEnablingHandle != 0) delete mEnablingHandle;
	delete mIndexHandle;
}

	
void iggFrameDataVariableLimits::Define(const iggObjectHandle *dsh, const iggPropertyHandle *eh, bool narrow)
{
	mDataSubjectHandle = dsh;
	if(eh != 0)
	{
		mEnablingHandle = new iggPropertyHandle(*eh);
	}
	else
	{
		mEnablingHandle = 0;
	}

	iggPropertyHandle rh("Range",dsh,mIndexHandle);
	LimitsRangeSlider *vls1 = new LimitsRangeSlider(true,dsh,rh,this);
	LimitsRangeSlider *vls2 = new LimitsRangeSlider(false,dsh,rh,this);
	vls1->AddBuddy(vls2);

	this->AddLine(vls1);
	this->AddLine(vls2);

	iggFrame *f = new iggFrame(this,4);

	iggPropertyHandle nh("Name",dsh,mIndexHandle);
	iggPropertyHandle sh("Stretch",dsh,mIndexHandle);
	LimitsStretchComboBox *sc = new LimitsStretchComboBox(sh,f,!narrow);
	sc->AddDependent(vls1);
	sc->AddDependent(vls2);
	if(narrow)
	{
		f->AddLine(new iggWidgetTextLabel("Name",f),1,new LimitsNameEdit(this,nh,f,false),2);
		f->AddLine(new iggWidgetTextLabel("Stretch",f),sc);
		f->SetColStretch(1,0);
		f->SetColStretch(2,10);
	}
	else
	{
		f->AddLine(new LimitsNameEdit(this,nh,f),2,sc,1);
		f->SetColStretch(1,10);
		f->SetColStretch(2,0);
	}
	f->SetColStretch(0,0);
	f->SetColStretch(3,1);
	this->AddLine(f);
}


void iggFrameDataVariableLimits::UpdateWidgetBody()
{
	bool en = true;
	if(mEnablingHandle != 0)
	{
		const iPropertyQuery<iType::Bool> *prop = dynamic_cast<const iPropertyQuery<iType::Bool>*>(mEnablingHandle->Function());
		if(prop == 0)
		{
			IBUG_WARN("FrameDataVariableList is configured incorrectly (unable to cast EnablingHandle).");
			return;
		}
		en = prop->GetQuery(); 
	}
	
	if(!en)
	{
		//
		//  Needs to be first - if we are not visible, the children would not update
		//
		this->iggFrame::UpdateWidgetBody();
		this->Show(false);
		return;
	}

	const iDataSubject *sub = dynamic_cast<const iDataSubject*>(mDataSubjectHandle->Object());
	if(sub == 0)
	{
		IBUG_ERROR("iggFrameDataVariableLimits is incorrectly configured.");
		return this->UpdateFailed();
	}

	iDataLimits *lim = sub->GetLimits();
	if(lim == 0)
	{
		IBUG_ERROR("iggFrameDataVariableLimits is incorrectly configured.");
		return this->UpdateFailed();
	}

	if(mIndexHandle->Index()>=0 && mIndexHandle->Index()<lim->GetNumVars())
	{
		this->SetTitle(lim->GetName(mIndexHandle->Index()));
		this->Show(true);
	}
	else
	{
		this->Show(false);
	}

	//
	//  Needs to be last - if we are not visible, the children would not update
	//
	this->iggFrame::UpdateWidgetBody();
}

#endif
