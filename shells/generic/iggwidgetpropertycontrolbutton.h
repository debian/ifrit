/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IGGWIDGETPROPERTYCONTROLBUTTON_H
#define IGGWIDGETPROPERTYCONTROLBUTTON_H

//
//  Non-abstract classes defined in this file
//
// iggWidgetPropertyControlCheckBox;
// iggWidgetPropertyControlExecButton;
// iggWidgetPropertyControlToggleButton;
// iggWidgetPropertyControlFileNameButton;
// iggWidgetPropertyControlIntExecButton;
//
#include "iggwidgetpropertycontrol.h"


#include "istring.h"

class iImage;

class ibgWidgetButtonSubject;


class iggWidgetPropertyControlButton : public iggWidgetPropertyControl<iType::Bool>
{

public:

	iggWidgetPropertyControlButton(int type, const iString &text, const iggPropertyHandle& ph, iggFrame *parent);

	void SetReverse(bool s);

	virtual void QueryValue(bool &val) const;

protected:

	virtual void UpdateValue(bool val);

	virtual void OnVoid1Body();

	bool mReverse;
	ibgWidgetButtonSubject *mSubject;
};


class iggWidgetPropertyControlCheckBox : public iggWidgetPropertyControlButton
{

public:

	iggWidgetPropertyControlCheckBox(const iString &text, const iggPropertyHandle& ph, iggFrame *parent);
};


class iggWidgetPropertyControlExecButton : public iggWidgetPropertyControlButton
{

public:

	iggWidgetPropertyControlExecButton(const iString &text, const iggPropertyHandle& ph, iggFrame *parent);

	virtual void QueryValue(bool &val) const;

protected:

	virtual void UpdateWidgetBody();
};


class iggWidgetActionControlExecButton : public iggWidgetPropertyControlExecButton
{

public:

	iggWidgetActionControlExecButton(const iString &text, const iggPropertyHandle& ph, iggFrame *parent);

	virtual bool ExecuteControl(bool final, bool val);
};


class iggWidgetPropertyControlToolButton : public iggWidgetPropertyControlButton
{

public:

	iggWidgetPropertyControlToolButton(const iString &text, const iggPropertyHandle& ph, iggFrame *parent);

	virtual void QueryValue(bool &val) const;

protected:

	virtual void UpdateWidgetBody();
};


class iggWidgetPropertyControlToggleButton : public iggWidgetPropertyControlButton
{

public:

	iggWidgetPropertyControlToggleButton(const iImage *icon, const iString &text, const iggPropertyHandle& ph, iggFrame *parent);

protected:

	const iImage *mIcon;
};


class iggWidgetPropertyControlFileNameButton : public iggWidgetPropertyControl<iType::String>
{

public:

	iggWidgetPropertyControlFileNameButton(const iString &text, const iString &header, const iString &selection, const iggPropertyHandle& ph, iggFrame *parent);

	void SetDefaut(const iString &s);

	virtual void QueryValue(iString &val) const;

protected:

	virtual void UpdateWidgetBody();
	virtual void UpdateValue(const iString& val);

	virtual void OnVoid1Body();

	ibgWidgetButtonSubject *mSubject;
	iString mHeader, mSelection, mDefault;
};


class iggWidgetPropertyControlExecIntButton : public iggWidgetPropertyControl<iType::Int>
{

public:

	iggWidgetPropertyControlExecIntButton(int value, const iString &text, const iggPropertyHandle& ph, iggFrame *parent);

	virtual void QueryValue(int &val) const;

protected:

	virtual void UpdateWidgetBody();
	virtual void UpdateValue(int val);

	virtual void OnVoid1Body();

	int mValue;
	ibgWidgetButtonSubject *mSubject;
};

#endif  // IGGWIDGETPROPERTYCONTROLBUTTON_H

