/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggrenderwindowobserver.h"


#include "ierror.h"
#include "iviewmodule.h"

#include "iggframe.h"
#include "iggshell.h"

#include <vtkRenderWindow.h>


//
//  Main class
//
iggRenderWindowObserver::iggRenderWindowObserver(iggFrame *parent, bool disabled)
{
	IASSERT(parent);
	mParent = parent;

	mParent->RegisterRenderWindowObserver(this);

	if(disabled)
	{
		mHelper = 0;
	}
	else
	{
		mHelper = new iggRenderWindowObserverHelper(this,parent); IERROR_CHECK_MEMORY(mHelper);
	}
}


iggRenderWindowObserver::~iggRenderWindowObserver()
{
	if(mHelper != 0)
	{
		mHelper->Detach();
		mHelper->Delete();
	}

	mParent->UnRegisterRenderWindowObserver(this);
}

void iggRenderWindowObserver::UpdateConnection()
{
	if(mHelper != 0) mHelper->UpdateConnection();
}


//
//  Helper class
//
iggRenderWindowObserverHelper::iggRenderWindowObserverHelper(iggRenderWindowObserver *owner, iggFrame *frame) : iEventObserver()
{
	IASSERT(frame);
	IASSERT(owner);

	mFrame = frame;
	mOwner = owner;
	mWindow = 0;
}


iggRenderWindowObserverHelper::~iggRenderWindowObserverHelper()
{
}


void iggRenderWindowObserverHelper::Detach()
{
	if(mWindow != 0) mWindow->RemoveObserver(this);
	mWindow = 0;
}


void iggRenderWindowObserverHelper::UpdateConnection()
{
	vtkRenderWindow *win = mFrame->GetShell()->GetViewModule()->GetRenderWindow();
	if(win!=0 && win!=mWindow)
	{
		this->Detach();
		mWindow = win;
		mWindow->AddObserver(EndEvent,this);
		mWindow->AddObserver(DeleteEvent,this);
	}
}


void iggRenderWindowObserverHelper::ExecuteBody(vtkObject *caller, unsigned long event, void *)
{
#ifndef I_NO_CHECK
	if(caller != mWindow)
	{
		IBUG_WARN("Internal check failed.");
	}
#endif
	if(caller == mWindow)
	{
		switch(event)
		{
		case EndEvent:
			{
				if(mFrame->IsVisible())
				{
					mOwner->OnRenderWindowModified();
				}
				break;
			}
		case DeleteEvent:
			{
				this->Detach();
				break;
			}
		}
	}
}

#endif
