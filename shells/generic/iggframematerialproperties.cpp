/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggframematerialproperties.h"


#include "ierror.h"
#include "imaterial.h"
#include "imaterialobject.h"
#include "iviewsubject.h"

#include "igghandle.h"
#include "iggwidgetpropertycontrolselectionbox.h"
#include "iggwidgetpropertycontrolslider.h"
#include "iggwidgetotherbutton.h"

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"
#include "iggwidgetpropertycontrolslider.tlh"


using namespace iParameter;

namespace iggFrameMaterialProperties_Private
{
	class MaterialHandle : public iggObjectHandle
	{

	public:

		MaterialHandle(const iggObjectHandle *parent) : iggObjectHandle(parent->GetShell())
		{
			mParent = parent;
		}

		virtual const iObject* Object(int mod = -1) const
		{
			iMaterialObject *obj = iRequiredCast<iMaterialObject>(INFO,const_cast<iObject*>(mParent->Object(mod)));
			return obj->GetMaterial();
		}

	private:

		const iggObjectHandle *mParent;
	};
};


using namespace iggFrameMaterialProperties_Private;


iggFrameMaterialProperties::iggFrameMaterialProperties(const iggObjectHandle *handle, iggFrame *parent) : iggFrame("Material properties",parent,2)
{
	IASSERT(handle);
	const iMaterialObject *sub = dynamic_cast<const iMaterialObject*>(handle->Object());
	if(sub == 0)
	{
		IBUG_FATAL("Widget iggFrameMaterialProperties is congifured incorrectly.");
	}

	mMaterialHandle = new MaterialHandle(handle); IERROR_CHECK_MEMORY(mMaterialHandle);

	iggWidgetPropertyControlRadioBox *sm = new iggWidgetPropertyControlRadioBox(3,"Shading mode",1,iggPropertyHandle("ShadingMode",mMaterialHandle),this);
	sm->InsertItem("Off");
	sm->InsertItem("Auto");
	sm->InsertItem("On");
	this->AddLine(sm);
	
	iggWidgetPropertyControlFloatSlider *slider;
	slider = new iggWidgetPropertyControlFloatSlider(0.0,1.0,20,0,3,false,false,"Ambient",iggPropertyHandle("Ambient",mMaterialHandle),this);
	slider->SetStretch(10,15);
	this->AddLine(slider,2);

	slider = new iggWidgetPropertyControlFloatSlider(0.0,1.0,20,0,3,false,false,"Diffuse",iggPropertyHandle("Diffuse",mMaterialHandle),this);
	slider->SetStretch(10,15);
	this->AddLine(slider,2);

	slider = new iggWidgetPropertyControlFloatSlider(0.0,1.0,20,0,3,false,false,"Specular",iggPropertyHandle("Specular",mMaterialHandle),this);
	slider->SetStretch(10,15);
	this->AddLine(slider,2);

	slider = new iggWidgetPropertyControlFloatSlider(0.0,1.0,20,0,3,false,false,"Specular power",iggPropertyHandle("SpecularPower",mMaterialHandle),this);
	slider->SetStretch(10,15);
	this->AddLine(slider,2);
}


iggFrameMaterialProperties::~iggFrameMaterialProperties()
{
	delete mMaterialHandle;
}

#endif
