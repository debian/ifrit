/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  generic GUI interactor
//

#ifndef IGGRENDERWINDOWINTERACTOR_H
#define IGGRENDERWINDOWINTERACTOR_H


#include <vtkRenderWindowInteractor.h>
#include "iviewmodulecomponent.h"


#include "ipointermacro.h"
#include "ivtk.h"


class iggRenderWindowInteractor : public vtkRenderWindowInteractor, public iViewModuleComponent
{

	friend class iggShell;
	friend class ibgRenderWindowSubject;

public:

	vtkTypeMacro(iggRenderWindowInteractor,vtkRenderWindowInteractor);

	// Description:
	// Initializes the event handlers without an XtAppContext.  This is
	// good for when you don't have a user interface, but you still
	// want to have mouse interaction.
	virtual void Initialize();

	// Description:
	// This will start up the X event loop and never return. If you
	// call this method it will loop processing X events until the
	// application is exited.
	virtual void Start();

	// Description:
	// This function is called on 'q','e' keypress if exitmethod is not
	// specified and should be overidden by platform dependent subclasses
	// to provide a termination procedure if one is required.
	virtual void TerminateApp(void) { /* empty */ }

	virtual void UserCallback();

	// Description:
	// These methods correspond to the the Exit, User and Pick
	// callbacks. They allow for the Style to invoke them.
	//virtual void ExitCallback();
	//virtual void UserCallback();
	//virtual void StartPickCallback();
	//virtual void EndPickCallback();

protected:

	iggRenderWindowInteractor(iViewModule *vm);
	virtual ~iggRenderWindowInteractor();

	virtual int InternalCreateTimer(int timerId, int timerType, unsigned long duration);
	virtual int InternalDestroyTimer(int platformTimerId);

//
	//  Mouse and keyboard interaction
	//
	void OnMouseMove(int x, int y, int b);
	void OnMousePress(int x, int y, int b);
	void OnMouseRelease(int x, int y, int b);
	void OnKeyPress(char k, int b);
	void OnTimer();
	void OnEnter();
	void OnLeave();
	void OnExpose();
};

#endif //IGGRENDERWINDOWINTERACTOR_H
