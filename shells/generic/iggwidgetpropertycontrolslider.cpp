/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggwidgetpropertycontrolslider.h"


#include "idatasubject.h"
#include "imath.h"
#include "iposition.h"
#include "istretch.h"

#include "iggframe.h"
#include "igghandle.h"
#include "iggshell.h"

#include "ibgwidgetentrysubject.h"

#include "iggsubjectfactory.h"

//
//  Template
//
#include "iarray.tlh"
#include "iggwidgetpropertycontrol.tlh"
#include "iggwidgetpropertycontrolslider.tlh"


using namespace iParameter;


//
//******************************************
//
//  Int slider
//
//******************************************
//
iggWidgetPropertyControlIntSlider::iggWidgetPropertyControlIntSlider(int min, int max, int numdig, const iString &label, const iggPropertyHandle& ph, iggFrame *parent, int rm) : iggWidgetPropertyControlSlider<iType::Int>(numdig,label,ph,parent,rm)
{
	int smin, smax;

	this->ConvertToSliderInt(min,smin);
	this->ConvertToSliderInt(max,smax);

	if(smin < smax)
	{
		this->SetRange(smin,smax);
	}
	else IBUG_WARN("Invalid slider range.");
}


void iggWidgetPropertyControlIntSlider::ConvertToSliderInt(int val, int &sint) const
{
	sint = val;
}


void iggWidgetPropertyControlIntSlider::ConvertFromSliderInt(int sint, int &val) const
{
	val = sint;
}


//
//******************************************
//
//  LargeInt slider
//
//******************************************
//
iggWidgetPropertyControlLargeIntSlider::iggWidgetPropertyControlLargeIntSlider(int min, int max, int numdig, const iString &label, const iggPropertyHandle& ph, iggFrame *parent, int rm) : iggWidgetPropertyControlIntSlider(min,max,numdig,label,ph,parent,rm)
{
}


void iggWidgetPropertyControlLargeIntSlider::ConvertToSliderInt(int val, int &sint) const
{
	const int ndex = 10 + 15 + 10;
	const int ddex[] = { 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95 };

	if(val < 10)
	{
		sint = val;
		return;
	}
	int i, dex = int(iMath::Log10((double)val));
	sint = 10 + ndex*(dex-1);
	val /= iMath::Round2Int(iMath::Pow10(double(dex-1)));
	for(i=0; i<ndex && ddex[i]<val; i++);
	sint += i;
}


void iggWidgetPropertyControlLargeIntSlider::ConvertFromSliderInt(int sint, int &val) const
{
	const int ndex = 10 + 15 + 10;
	const int ddex[] = { 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95 };

	if(sint < 10)
	{
		val = sint;
		return;
	}
	sint -= 10;
	int dex = sint/ndex;
	sint -= ndex*dex;
	val = iMath::Round2Int(iMath::Pow10(double(dex))*ddex[sint]);

#ifdef I_CHECK
	int sint2;
	this->ConvertToSliderInt(val,sint2);
	if(sint+10+ndex*dex != sint2)
	{
		this->ConvertToSliderInt(val,sint2);
		IBUG_FATAL("Bug in iggWidgetPropertyControlLargeIntSlider::ConvertFromSliderIn");
	}
#endif
}


//
//******************************************
//
//  template Number slider
//
//******************************************
//
template<iType::TypeId id>
iggWidgetPropertyControlGenericNumberSlider<id>::iggWidgetPropertyControlGenericNumberSlider(ArgT min, ArgT max, int res, int sid, ArgT tiny, int numdig, bool exdown, bool exup, const iString &label, const iggPropertyHandle& ph, iggFrame *parent, int rm) : iggWidgetPropertyControlSlider<id>(numdig,label,ph,parent,rm), mTiny(tiny)
{
	this->mResolution = res;
	if(this->mResolution < 1) this->mResolution = 1;
	this->mStretchId = sid;
	this->mMin = min;
	this->mMax = max;

	this->mIsExpandableDown = exdown;
	this->mIsExpandableUp = exup;

	this->mInOnEdgeUpdate = false;
	this->SetRange(0,mResolution);
}


template<iType::TypeId id>
void iggWidgetPropertyControlGenericNumberSlider<id>::ConvertToSliderInt(ArgT val, int &sint) const
{
	val = iStretch::Apply(val,this->mStretchId,false);
	ValT min = iStretch::Apply(this->mMin,this->mStretchId,false);
	ValT max = iStretch::Apply(this->mMax,this->mStretchId,true);
	ValT stp = (max-min)/this->mResolution;
	if(fabs(stp) < this->mTiny) stp = 1.0;

	if(val < min) val = min;
	if(val > max) val = max;

	sint = iMath::Round2Int((val-min)/stp);
	if(sint < 0) sint = 0;
	if(sint > this->mResolution) sint = this->mResolution; 
}


template<iType::TypeId id>
void iggWidgetPropertyControlGenericNumberSlider<id>::ConvertFromSliderInt(int sint, ValT &val) const
{
	ValT min = iStretch::Apply(this->mMin,this->mStretchId,false);
	ValT max = iStretch::Apply(this->mMax,this->mStretchId,true);

	if(sint < 0) sint = 0;
	if(sint > this->mResolution) sint = this->mResolution; 

	val = min + (max-min)*sint/this->mResolution;
	val = iStretch::Reset(val,this->mStretchId);
}



template<iType::TypeId id>
void iggWidgetPropertyControlGenericNumberSlider<id>::OnEdge(int edge)
{
	if(this->mInOnEdgeUpdate || edge==0 || (edge<0 && !this->mIsExpandableDown) || (edge>0 && !this->mIsExpandableUp)) return;
	this->mInOnEdgeUpdate = true;

	float min = iStretch::Apply(this->mMin,this->mStretchId,false);
	float max = iStretch::Apply(this->mMax,this->mStretchId,true);
	float step = (max-min)/this->mResolution;

	if(edge < 0)
	{
		min -= step;
		this->mMin = iStretch::Reset(min,this->mStretchId);
	}
	else if(edge > 0)
	{
		max += step;
		this->mMax = iStretch::Reset(max,this->mStretchId);
	}
	else
	{
		IBUG_WARN("Should never come here");
	}

	this->mResolution++;
	this->SetRange(0,this->mResolution);
	this->UpdateWidget();

	this->mInOnEdgeUpdate = false;
}


//
//******************************************
//
//  Float slider
//
//******************************************
//
iggWidgetPropertyControlFloatSlider::iggWidgetPropertyControlFloatSlider(float min, float max, int res, int sid, int numdig, bool exdown, bool exup, const iString &label, const iggPropertyHandle& ph, iggFrame *parent, int rm) : iggWidgetPropertyControlGenericNumberSlider<iType::Float>(min,max,res,sid,iMath::_FloatMin,numdig,exdown,exup,label,ph,parent,rm)
{
}


//
//******************************************
//
//  Double slider
//
//******************************************
//
iggWidgetPropertyControlDoubleSlider::iggWidgetPropertyControlDoubleSlider(double min, double max, int res, int sid, int numdig, bool exdown, bool exup, const iString &label, const iggPropertyHandle& ph, iggFrame *parent, int rm) : iggWidgetPropertyControlGenericNumberSlider<iType::Double>(min,max,res,sid,iMath::_DoubleMin,numdig,exdown,exup,label,ph,parent,rm)
{
}


//
//******************************************
//
//  Position slider
//
//******************************************
//
iggWidgetPropertyControlPositionSlider::iggWidgetPropertyControlPositionSlider(int numdig, const iString &label, const iggPropertyHandle& ph, int dir, iggFrame *parent, int rm) : iggWidgetPropertyControlSlider<iType::Double>(numdig,label,ph,parent,rm)
{
	if(dir<-1 || dir>2)
	{
		IBUG_FATAL("WidgetPropertyControlPositionSlider is configured incorrectly.");
	}

	mDirection = dir;
	mResolution = 100;

	this->SetRange(-mResolution,mResolution);
	mSubject->SetEditable(true);

	iggWidgetPropertyControlBase::PositionList().AddUnique(this);
	List().AddUnique(this);
}


iggWidgetPropertyControlPositionSlider::~iggWidgetPropertyControlPositionSlider()
{
	List().Remove(this);
	iggWidgetPropertyControlBase::PositionList().Remove(this);
}


void iggWidgetPropertyControlPositionSlider::ConvertToSliderInt(double val, int &sint) const
{
	iCoordinate d(this->GetShell()->GetViewModule());
	d.SetBoxValue(val);
	sint = iMath::Round2Int(mResolution*d);
	if(sint < -mResolution) sint = -mResolution;
	if(sint >  mResolution) sint =  mResolution; 
}


void iggWidgetPropertyControlPositionSlider::ConvertFromSliderInt(int sint, double &val) const
{
	if(sint < -mResolution) sint = -mResolution;
	if(sint >  mResolution) sint =  mResolution; 
	iCoordinate d(this->GetShell()->GetViewModule());
	d = (double)sint/mResolution;
	val = d.BoxValue();
}


void iggWidgetPropertyControlPositionSlider::OnBool1Body(bool s)
{
	int i;
	iLookupArray<iggWidgetPropertyControlPositionSlider*> &list(List());

	for(i=0; i<list.Size(); i++)
	{
		list[i]->mSubject->IncrementNumDigits(s?1:-1);
		list[i]->UpdateWidget();
	}
}


bool iggWidgetPropertyControlPositionSlider::SetPropertyValue(const iProperty *prop, int i, double val) const
{
	if(mDirection == -1)
	{
		return iggWidgetPropertyControlSlider<iType::Double>::SetPropertyValue(prop,i,val);
	}
	else
	{
		const iPropertyDataVariable<iType::Vector> *vprop = iDynamicCast<const iPropertyDataVariable<iType::Vector> >(INFO,prop);
		iVector3D v = vprop->GetValue(i);
		v[mDirection] = val;
		return vprop->SetValue(i,v);
	}
}


double iggWidgetPropertyControlPositionSlider::GetPropertyValue(const iProperty *prop, int i) const
{
	if(mDirection == -1)
	{
		return iggWidgetPropertyControlSlider<iType::Double>::GetPropertyValue(prop,i);
	}
	else
	{
		return iDynamicCast<const iPropertyDataVariable<iType::Vector> >(INFO,prop)->GetValue(i)[mDirection];
	}
}


iLookupArray<iggWidgetPropertyControlPositionSlider*>& iggWidgetPropertyControlPositionSlider::List()
{
	static iLookupArray<iggWidgetPropertyControlPositionSlider*> list(20);
	return list;
}


//
//******************************************
//
//  Size slider
//
//******************************************
//
iggWidgetPropertyControlSizeSlider::iggWidgetPropertyControlSizeSlider(double min, int numdig, const iString &label, const iggPropertyHandle& ph, iggFrame *parent, int rm) : iggWidgetPropertyControlGenericNumberSlider<iType::Double>((min<iMath::_DoubleMin)?iMath::_DoubleMin:min,1.0,100,(min>iMath::_DoubleMin)?1:0,iMath::_DoubleMin,numdig,true,false,label,ph,parent,rm)
{
	iggWidgetPropertyControlBase::PositionList().AddUnique(this);
}


iggWidgetPropertyControlSizeSlider::~iggWidgetPropertyControlSizeSlider()
{
	iggWidgetPropertyControlBase::PositionList().Remove(this);
}


void iggWidgetPropertyControlSizeSlider::ConvertToSliderInt(double val, int &sint) const
{
	iCoordinate d(this->GetShell()->GetViewModule());
	d.SetBoxValue(val);
	this->iggWidgetPropertyControlGenericNumberSlider<iType::Double>::ConvertToSliderInt(d,sint);
}


void iggWidgetPropertyControlSizeSlider::ConvertFromSliderInt(int sint, double &val) const
{
	double dv;
	this->iggWidgetPropertyControlGenericNumberSlider<iType::Double>::ConvertFromSliderInt(sint,dv);
	iCoordinate d(this->GetShell()->GetViewModule());
	d = dv;
	val = d.BoxValue();
}


//
//******************************************
//
//  Opacity slider
//
//******************************************
//
iggWidgetPropertyControlOpacitySlider::iggWidgetPropertyControlOpacitySlider(const iggPropertyHandle& ph, iggFrame *parent, int rm) : iggWidgetPropertyControlFloatSlider(0.0,1.0,20,0,4,false,false,"Opacity",ph,parent,rm)
{
}


iggWidgetPropertyControlOpacitySlider::iggWidgetPropertyControlOpacitySlider(const iString& title, const iggPropertyHandle& ph, iggFrame *parent, int rm) : iggWidgetPropertyControlFloatSlider(0.0,1.0,20,0,4,false,false,title,ph,parent,rm)
{
}


//
//******************************************
//
//  VariableLimits slider
//
//******************************************
//
int iggWidgetPropertyControlVariableLimitsSlider::Method = 1;

iggWidgetPropertyControlVariableLimitsSlider::iggWidgetPropertyControlVariableLimitsSlider(int mode, int numdig, const iString &label, const iggObjectHandle *dsh, const iggPropertyHandle& ph, iggFrame *parent, int var, int rm) : iggWidgetPropertyControlFloatSlider(0.0,1.0,100,0,numdig,(mode != 0),(mode != 0),label,ph,parent,rm)
{
	mVar = var;
	mVarHandle = 0;
	mStretchHandle = 0;

	this->Define(mode,dsh);
}


iggWidgetPropertyControlVariableLimitsSlider::iggWidgetPropertyControlVariableLimitsSlider(int mode, int numdig, const iString &label, const iggObjectHandle *dsh, const iggPropertyHandle& ph, iggFrame *parent, const iggPropertyHandle *vh, const iggPropertyHandle *sh, int rm) : iggWidgetPropertyControlFloatSlider(0.0,1.0,100,0,numdig,(mode != 0),(mode != 0),label,ph,parent,rm)
{
	mVar = -1;

	if(vh == 0)
	{
		mVarHandle = 0;
	}
	else
	{
		mVarHandle = new iggPropertyHandle(*vh); IERROR_CHECK_MEMORY(mVarHandle);
	}

	if(sh == 0)
	{
		mStretchHandle = 0;
	}
	else
	{
		mStretchHandle = new iggPropertyHandle(*sh); IERROR_CHECK_MEMORY(mStretchHandle);
	}

	this->Define(mode,dsh);

	this->SetRenderTarget(ViewModuleSelection::Clones);
}


void iggWidgetPropertyControlVariableLimitsSlider::Define(int mode, const iggObjectHandle *dsh)
{
	mDataSubjectHandle = dsh; IASSERT(mDataSubjectHandle);

	iggWidgetPropertyControlBase::VariableLimitsList().AddUnique(this);

	mMode = mode;

	mVar = -1;
	mMin = 1;
	mMax = 1;
	mRangeSet = false;
}


iggWidgetPropertyControlVariableLimitsSlider::~iggWidgetPropertyControlVariableLimitsSlider()
{
	iggWidgetPropertyControlBase::VariableLimitsList().Remove(this);

	if(mVarHandle != 0) delete mVarHandle;
}


bool iggWidgetPropertyControlVariableLimitsSlider::SetPropertyValue(const iProperty *prop, int i, float val) const
{
	if(mMode != 0)
	{
		const iPropertyDataVariable<iType::Pair> *p = iDynamicCast<const iPropertyDataVariable<iType::Pair> >(INFO,prop);
		if(p != 0)
		{
			iPair d = p->GetValue(i);
			if(mMode > 0) d.Max = val; else d.Min = val;
			return p->SetValue(i,d);
		}
		else return false;
	}
	else return this->iggWidgetPropertyControlFloatSlider::SetPropertyValue(prop,i,val);
}


float iggWidgetPropertyControlVariableLimitsSlider::GetPropertyValue(const iProperty *prop, int i) const
{
	if(mMode != 0)
	{
		const iPropertyDataVariable<iType::Pair> *p = iDynamicCast<const iPropertyDataVariable<iType::Pair> >(INFO,prop);
		if(p != 0)
		{
			iPair d = p->GetValue(i);
			return (mMode > 0) ? d.Max : d.Min;
		}
		else return (mMode > 0) ? iMath::_LargeFloat : -iMath::_LargeFloat;
	}
	else return this->iggWidgetPropertyControlFloatSlider::GetPropertyValue(prop,i);
}


bool iggWidgetPropertyControlVariableLimitsSlider::PrepareForUpdate(iPair &range)
{
	//
	//  Adjust to the parent page if it is set
	//
	const iDataSubject *obj = dynamic_cast<const iDataSubject*>(mDataSubjectHandle->Object());
	if(obj == 0)
	{
		IBUG_ERROR("iggWidgetPropertyControlVariableLimitsSlider is connected to a wrong object!");
		this->Enable(false);
		return false;
	}

	int var;
	if(mVarHandle != 0)
	{
		const iPropertyDataVariable<iType::Int> *prop = dynamic_cast<const iPropertyDataVariable<iType::Int>*>(mVarHandle->Variable());
		if(prop == 0)
		{
			IBUG_ERROR("iggWidgetPropertyControlVariableLimitsSlider::VarProperty is not an int-valued property!");
			this->Enable(false);
			return false;
		}
		var = prop->GetValue(this->Handle()->Index());
	}
	else
	{
		var = this->Handle()->Index();
	}

	if(var<0 || var>=obj->GetLimits()->GetNumVars())
	{
		//
		//  Inactive value
		//
		this->Enable(false);
		return false;
	}
	else this->Enable(true);

	if(var != mVar)
	{
		mRangeSet = false;
		mVar = var;
	}

	if(mStretchHandle != 0)
	{
		const iPropertyDataVariable<iType::String> *prop = dynamic_cast<const iPropertyDataVariable<iType::String>*>(mStretchHandle->Variable());
		if(prop == 0)
		{
			IBUG_ERROR("iggWidgetPropertyControlVariableLimitsSlider::StretchProperty is not a string-valued property!");
			this->Enable(false);
			return false;
		}
		mStretchId = iStretch::GetId(prop->GetValue(this->Handle()->Index()));
	}
	else
	{
		mStretchId = obj->GetLimits()->GetStretchId(mVar);
	}

	if(!mRangeSet)
	{
		iPair r = obj->GetLimits()->GetRange(mVar);
		mMin = r.Min;
		mMax = r.Max;
		mRangeSet = true;
	}

	float step;
	float min = iStretch::Apply(mMin,mStretchId,false);
	float max = iStretch::Apply(mMax,mStretchId,true);

	switch(mStretchId)
	{
	case iStretch::Log:
		{
			if(max > min+1.0f) step = 0.1f; else if(max > min+0.1f) step = 0.01f; else step = 0.01f*(max-min);
			break;
		}
	default:
		{
			step = 0.01f*(max-min);
			break;
		}
	}
	if(step < 1.0e-36f) step = 1.0e-36f;

	mResolution = 2*iMath::Round2Int(0.5*(max-min)/step);  // make it even
	if(mResolution < 2) mResolution = 2;
	this->SetRange(0,mResolution);

	if(mMode == 0)
	{
		//
		//  This is isosurface level, just set the range to data range
		//
		range = obj->GetLimits()->GetDataRange(mVar);
	}
	else
	{
		range = obj->GetLimits()->GetRange(mVar);
	}

	return true;
}


void iggWidgetPropertyControlVariableLimitsSlider::UpdateWidgetBody()
{
	iPair range;
	if(!this->PrepareForUpdate(range)) return;
	
	if(mMode == 0)
	{
		//
		//  This is isosurface level, just the range to data range
		//
		mMin = range.Min;
		mMax = range.Max;
	}
	else
	{
		if(Method == 0)
		{
			//
			//  Simple expandable slides, just make sure that the values would fit
			//
			mIsExpandableDown = mIsExpandableUp = true;
			if(mMin > range.Min) mMin = range.Min;
			if(mMax < range.Max) mMax = range.Max;
		}
		else
		{
			//
			//  Always-centered slider
			//
			mIsExpandableDown = mIsExpandableUp = false;
			if(mMode > 0)
			{
				mMin = range.Min;
				mMax = iStretch::Reset(2*iStretch::Apply(range.Max,mStretchId,true)-iStretch::Apply(range.Min,mStretchId,false),mStretchId);
			}
			else
			{
				mMax = range.Max;
				mMin = iStretch::Reset(2*iStretch::Apply(range.Min,mStretchId,true)-iStretch::Apply(range.Max,mStretchId,false),mStretchId);
			}
		}
	}

	this->iggWidgetPropertyControlFloatSlider::UpdateWidgetBody();
}


void iggWidgetPropertyControlVariableLimitsSlider::OnVoid1Body()
{
	mDisableDepedentUpdate = false;
	this->iggWidgetPropertyControlFloatSlider::OnVoid1Body();
	this->UpdateWidget();
}


void iggWidgetPropertyControlVariableLimitsSlider::OnVoid2Body()
{
	mDisableDepedentUpdate = false;
	this->iggWidgetPropertyControlFloatSlider::OnVoid2Body();
	this->UpdateWidget();
}


void iggWidgetPropertyControlVariableLimitsSlider::OnInt1Body(int v)
{
	mDisableDepedentUpdate = true;
	this->iggWidgetPropertyControlFloatSlider::OnInt1Body(v);
}

#endif
