/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IGGWIDGETPROPERTYCONTROLSELECTIONBOX_H
#define IGGWIDGETPROPERTYCONTROLSELECTIONBOX_H

//
//  Non-abstract classes defined in this file
//
//  iggWidgetPropertyControlSpinBox;
//  iggWidgetPropertyControlComboBox;
//  iggWidgetPropertyControlRadioBox;
//  iggWidgetPropertyControlTextComboBox;
//  iggWidgetPropertyControlStretchComboBox;
//
#include "iggwidgetpropertycontrol.h"


class ibgWidgetComboBoxSubject;
class ibgWidgetRadioBoxSubject;
class ibgWidgetSpinBoxSubject;


//
//  Base class for selection lists
//
class iggWidgetPropertyControlSelectionBox : public iggWidgetPropertyControl<iType::Int>
{

public:

	iggWidgetPropertyControlSelectionBox(int offset, const iggPropertyHandle& ph, iggFrame *parent, int rm);

	void SetOffset(int);
	inline int GetOffset() const { return mOffset; }

	virtual int Count() = 0;

protected:

	virtual void OnInt1Body(int);

	int mOffset;
};


//
//  Itemized selection box (like combo box or radio button box)
//
class iggWidgetPropertyControlItemizedSelectionBox : public iggWidgetPropertyControlSelectionBox
{

public:

	iggWidgetPropertyControlItemizedSelectionBox(int offset, const iggPropertyHandle& ph, iggFrame *parent, int rm);

	//
	//  Decorator functions
	//
	virtual void InsertItem(const iString &text, int index = -1) = 0;
	virtual void SetItem(const iString &text, int index, bool vis) = 0;
	virtual void RemoveItem(int index) = 0;
};


//
//  Spin box
//
class iggWidgetPropertyControlSpinBox : public iggWidgetPropertyControlSelectionBox
{

public:

	iggWidgetPropertyControlSpinBox(int min, int max, const iString &title, int offset, const iggPropertyHandle& ph, iggFrame *parent, int rm = iParameter::RenderMode::Immediate);

	void SetStretch(int title, int box);

	//
	//  Decorator functions
	//
	void SetFirstEntryText(const iString &text);
	void SetRange(int min, int max);
	virtual int Count();

protected:

	virtual void QueryValue(int &val) const;
	virtual void UpdateValue(int val);

	ibgWidgetSpinBoxSubject *mSubject;
};


//
//  Plain combo box
//
class iggWidgetPropertyControlComboBox : public iggWidgetPropertyControlItemizedSelectionBox
{

public:

	iggWidgetPropertyControlComboBox(const iString &title, int offset, const iggPropertyHandle& ph, iggFrame *parent, int rm = iParameter::RenderMode::Immediate);

	void SetStretch(int title, int box);

	//
	//  Decorator functions
	//
	virtual void InsertItem(const iString &text, int index = -1);
	virtual void SetItem(const iString &text, int index, bool vis);
	virtual void RemoveItem(int index);
	virtual int Count();
	void Clear();

protected:

	virtual void QueryValue(int &val) const;
	virtual void UpdateValue(int val);

	ibgWidgetComboBoxSubject *mSubject;
};


//
//  Radio button box
//
class iggWidgetPropertyControlRadioBox : public iggWidgetPropertyControlItemizedSelectionBox
{

public:

	iggWidgetPropertyControlRadioBox(int cols, const iString &title, int offset, const iggPropertyHandle& ph, iggFrame *parent, int rm = iParameter::RenderMode::Immediate);

	//
	//  Decorator functions
	//
	virtual void InsertItem(const iString &text, int index = -1);
	virtual void SetItem(const iString &text, int index, bool vis);
	virtual void RemoveItem(int index);
	virtual int Count();

protected:

	virtual void QueryValue(int &val) const;
	virtual void UpdateValue(int val);

	ibgWidgetRadioBoxSubject *mSubject;
};


//
//  Text combo box
//
class iggWidgetPropertyControlTextComboBox : public iggWidgetPropertyControlComboBox
{

public:

	iggWidgetPropertyControlTextComboBox(int section, const iString &title, int offset, const iggPropertyHandle& ph, iggFrame *parent, int rm = iParameter::RenderMode::Immediate);

	void SetInvalidValue(int v);

protected:

	virtual void QueryValue(int &val) const;
	virtual void UpdateValue(int val);

	//
	//  This function specifies how the combo box text entry is transformed into an integer number.
	//  By default, the first white-space separated section is assumed to be a number.
	//
	virtual bool ConvertFromText(const iString &text, int &val) const;

	int mSection, mInvalidValue;
};


//
//  Stretch combo box
//
class iggWidgetPropertyControlStretchComboBox : public iggWidgetPropertyControl<iType::String>
{

public:

	iggWidgetPropertyControlStretchComboBox(const iggPropertyHandle& ph, iggFrame *parent, int rm = iParameter::RenderMode::Immediate, bool title = true);

protected:

	virtual void QueryValue(iString &val) const;
	virtual void UpdateValue(const iString& val);

	virtual void OnInt1Body(int);

	ibgWidgetComboBoxSubject *mSubject;
};

#endif  // IGGWIDGETPROPERTYCONTROLSELECTIONBOX_H

