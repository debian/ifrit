/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggwidgetpropertycontroltrackball.h"


#include "ierror.h"
#include "imath.h"
#include "imonitor.h"
#include "ishell.h"
#include "iviewmodule.h"
#include "ivtk.h"

#include "iggdialog.h"
#include "iggframe.h"
#include "igghandle.h"
#include "iggmainwindow.h"
#include "iggrenderwindowobserver.h"
#include "iggwidgetotherbutton.h"
#include "iggwidgetrendermodebutton.h"

#include "ibgwidgetentrysubject.h"
#include "ibgwidgettrackballsubject.h"

#include <vtkMath.h>

#include "iggsubjectfactory.h"

//
//  templates
//
//#include "iggwidgetpropertycontrol.tlh"


using namespace iParameter;


namespace iggWidgetPropertyControlTrackBall_Private
{
	class ValuesDialog;

	class AxisAlignButton : public iggWidgetSimpleButton
	{

	public:

		AxisAlignButton(int dir, double *vec, const iString &text, ValuesDialog *owner, iggFrame *parent) : iggWidgetSimpleButton(text,parent,true)
		{
			mDir = dir;
			mVec = vec;
			mOwner = owner;

			this->SetBaloonHelp("Align the controlled direction with a coordinate axis.");
		}
	
	protected:

		virtual void Execute();

		int mDir;
		double *mVec;
		ValuesDialog *mOwner;
	};

	class ValuesDialog : public iggDialog
	{

	public:

		ValuesDialog(iggWidgetPropertyControlTrackBall *parent) : iggDialog(parent->GetMainWindow(),0U,0,"Direction",0,4,0)
		{
			mParent = parent; IASSERT(mParent);
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

		void CompleteConstructionBody();

		virtual void OnValueChanged(bool isdir)
		{
			if(isdir)
			{
				double len = vtkMath::Norm(mDir);
				if(len > 0)
				{
					int i;
					for(i=0; i<3; i++)
					{
						mDir[i] /= len;
						if(fabs(mDir[i]) < 1.0e-13) mDir[i] = 0;
					}
				}
				else
				{
					mDir[0] = mDir[1] = 0;
					mDir[2] = 0;
				}
				this->SetAngles();
			}
			else
			{
				double theta = vtkMath::RadiansFromDegrees(mAngles[0]); 
				double phi = vtkMath::RadiansFromDegrees(mAngles[1]);

				mDir[0] = cos(theta)*cos(phi);
				mDir[1] = cos(theta)*sin(phi);
				mDir[2] = sin(theta);
			}

			mParent->SetDirection(mDir);

			this->UpdateDialog();
			mParent->UpdateWidget();
		}

	protected:

		virtual void SetAngles()
		{
			mAngles[0] = vtkMath::DegreesFromRadians(atan2(mDir[2],sqrt(mDir[0]*mDir[0]+mDir[1]*mDir[1]))); 
			mAngles[1] = vtkMath::DegreesFromRadians(atan2(mDir[1],mDir[0]));

			int i;
			for(i=0; i<2; i++)
			{
				if(fabs(mAngles[i]) < 1.0e-13) mAngles[i] = 0;
			}
		}

		virtual void UpdateDialog()
		{
			mParent->GetDirection(mDir);
			this->SetAngles();
			this->iggDialog::UpdateDialog();
		}

		double mAngles[2], mDir[3];
		iggWidgetPropertyControlTrackBall *mParent;
	};

	class ValueEntry : public iggWidget
	{

	public:

		ValueEntry(ValuesDialog *owner, const iString &label, bool isdir, double &value, iggFrame *parent) : iggWidget(parent), mValue(value)
		{
			mOwner = owner; IASSERT(mOwner);
			mIsDir = isdir;

			mSubject = iggSubjectFactory::CreateWidgetEntrySubject(this,false,6,label); IERROR_CHECK_MEMORY(mSubject);
			mSubject->SetEditable(true);

			mNeedsBaloonHelp = false;
		}

	protected:

		virtual void OnVoid1Body();

		virtual void UpdateWidgetBody()
		{
			mSubject->SetText(iString::FromNumber(mValue));
		}

		bool mIsDir;
		double &mValue;
		ValuesDialog *mOwner;
		ibgWidgetEntrySubject *mSubject;
	};

	void ValuesDialog::CompleteConstructionBody()
	{
		mFrame->AddLine(new ValueEntry(this,"Theta",false,mAngles[0],mFrame),1,new ValueEntry(this,"Phi",false,mAngles[1],mFrame),3);
		mFrame->AddLine(new ValueEntry(this,"X-component",true,mDir[0],mFrame),2,new AxisAlignButton(1,mDir,"+X",this,mFrame),1,new AxisAlignButton(-1,mDir,"-X",this,mFrame),1);
		mFrame->AddLine(new ValueEntry(this,"Y-component",true,mDir[1],mFrame),2,new AxisAlignButton(2,mDir,"+Y",this,mFrame),1,new AxisAlignButton(-2,mDir,"-Y",this,mFrame),1);
		mFrame->AddLine(new ValueEntry(this,"Z-component",true,mDir[2],mFrame),2,new AxisAlignButton(3,mDir,"+Z",this,mFrame),1,new AxisAlignButton(-3,mDir,"-Z",this,mFrame),1);
		mFrame->SetColStretch(0,10);
		mFrame->SetColStretch(1,10);
	}

	void ValueEntry::OnVoid1Body()
	{
		bool ok;
		double v = mSubject->GetText().ToDouble(ok);
		if(ok)
		{
			mValue = v;
			mOwner->OnValueChanged(mIsDir);
		}
	}

	void AxisAlignButton::Execute()
	{
		mVec[0] = mVec[1] = mVec[2] = 0;
		if(mDir > 0)
		{
			mVec[mDir-1] = 1;
		}
		else
		{
			mVec[-mDir-1] = -1;
		}
		mOwner->OnValueChanged(true);
	}
};


using namespace iggWidgetPropertyControlTrackBall_Private;


//
// Constructs a TrackBall.
//
iggWidgetPropertyControlTrackBall::iggWidgetPropertyControlTrackBall(bool followCamera, const iggPropertyHandle& ph, iggFrame *parent, int rm) : iggWidgetPropertyControlBase(ph,parent,rm), iggRenderWindowObserver(parent,followCamera)
{
	if(this->Handle()->Variable()->GetType() != iType::Vector)
	{
		IBUG_ERROR("Widget iggWidgetPropertyControlTrackBall is congifured incorrectly.");
		this->Enable(false);
		return;
	}

	mSubject = iggSubjectFactory::CreateWidgetTrackBallSubject(this,followCamera);

	if(rm != RenderMode::DoNotRender)
	{
		iggWidgetRenderModeButton *rm = new iggWidgetRenderModeButton(this); IERROR_CHECK_MEMORY(rm);
		rm->ClearLaidOutFlag(mSubject);
		mSubject->AddButton(rm->GetSubject());
	}

	this->SetRenderMode(rm);

	mValuesDialog = new ValuesDialog(this); IERROR_CHECK_MEMORY(mValuesDialog);
}


iggWidgetPropertyControlTrackBall::~iggWidgetPropertyControlTrackBall()
{
	delete mValuesDialog;
}


void iggWidgetPropertyControlTrackBall::UpdateWidgetBody()
{
	this->UpdateHelp();

	if(this->Handle()->Index() >= 0)
	{
		this->Enable(true);
		const iPropertyDataVariable<iType::Vector> *var = iDynamicCast<const iPropertyDataVariable<iType::Vector> >(INFO,this->Handle()->Variable());
		const iVector3D &v(var->GetValue(0));
		mSubject->SetDirection(v);
	}
	else
	{
		this->Enable(false);
	}
}


void iggWidgetPropertyControlTrackBall::OnRenderWindowModified()
{
	mSubject->Repaint();
}


void iggWidgetPropertyControlTrackBall::OnBool1Body(bool final)
{
	iString ws;
	double dir[3];

	mSubject->GetDirection(dir);

	const iPropertyDataVariable<iType::Vector> *var = iDynamicCast<const iPropertyDataVariable<iType::Vector> >(INFO,this->Handle()->Variable());
	if(!var->SetValue(0,iVector3D(dir[0],dir[1],dir[2])))
	{
		iMonitor h(this);
		h.PostError("Unable to set the property value");
		this->Enable(false);
		return;
	}
	else this->Enable(true);

	mValuesDialog->UpdateDialog();

	if(this->GetRenderMode()==RenderMode::Immediate || (final && this->GetRenderMode()!=RenderMode::DoNotRender)) this->Render();
}


void iggWidgetPropertyControlTrackBall::SetDirection(const double dir[3])
{
	mSubject->SetDirection(dir);
	this->OnBool1Body(true);
}


void iggWidgetPropertyControlTrackBall::GetDirection(double dir[3]) const
{
	mSubject->GetDirection(dir);
}


void iggWidgetPropertyControlTrackBall::OnVoid1Body()
{
	mValuesDialog->Show(true);
}

#endif
