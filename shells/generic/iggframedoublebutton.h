/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IGGFRAMEDOUBLEBUTTON_H
#define IGGFRAMEDOUBLEBUTTON_H


#include "iggframe.h"


#include "ivector3D.h"

class iString;

class iggPropertyHandle;


class iggFrameDoubleButton : public iggFrame
{

public:

	virtual void OnButton1() = 0;
	virtual void OnButton2() = 0;

protected:

	iggFrameDoubleButton(iggFrame *parent, int cols);
	iggFrameDoubleButton(const iString &title, iggFrame *parent, int cols);
};


class iggFrameMoveFocalPointButton : public iggFrameDoubleButton
{

public:

	iggFrameMoveFocalPointButton(iggFrame *parent, bool withmarker = false, bool stretching = false);
	iggFrameMoveFocalPointButton(const iString &title, iggFrame *parent, bool withmarker = false, bool stretching = false);

	virtual iVector3D GetPosition() const = 0;
	virtual int GetWindowNumber() const { return -1; }

	void SetMoveInstantly(bool s);
	inline bool GetMoveInstantly() const { return mMoveInstantly; }

protected:

	virtual void OnButton1();
	virtual void OnButton2();

	bool mMoveInstantly;

private:

	void Define(bool withmarker, bool stretching);
};

#endif  // IGGFRAMEDOUBLEBUTTON_H

