/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggdialog.h"


#include "ierror.h"
#include "isystem.h"

#include "ihelpfactory.h"

#include "iggframetopparent.h"
#include "iggmainwindow.h"
#include "iggmenuwindow.h"
#include "iggshell.h"
#include "iggwidgetotherbutton.h"

#include "ibgdialogsubject.h"
#include "ibgmenuwindowsubject.h"

#include "iggsubjectfactory.h"

//
//  Templates
//
#include "iarray.tlh"


//
//  Main class
//
iggDialog::iggDialog(iggShell *shell, unsigned int mode, const iImage *icon, const iString &title, const char *helpkey, int cols, const char *closetext) : iggRenderingElement(shell)
{
	mMainWindow = 0;
	this->Define(0,mode,icon,title,helpkey,cols,closetext);
}


iggDialog::iggDialog(const iggMenuWindow *mw, unsigned int mode, const iImage *icon, const iString &title, const char *helpkey, int cols, const char *closetext) : iggRenderingElement(mw==0?0:mw->GetShell())
{
	IASSERT(mw);
	mMainWindow = mw->GetMainWindow();
	this->Define(mw->GetSubject(),mode,icon,title,helpkey,cols,closetext);
}


iggDialog::iggDialog(const iggDialog *pd, unsigned int mode, const iImage *icon, const iString &title, const char *helpkey, int cols, const char *closetext) : iggRenderingElement(pd==0?0:pd->GetShell())
{
	IASSERT(pd);
	mMainWindow = pd->GetMainWindow();
	this->Define(pd->GetSubject(),mode,icon,title,helpkey,cols,closetext);
}


void iggDialog::Define(const ibgWindowSubject *base, unsigned int mode, const iImage *icon, const iString &title, const char *helpkey, int cols, const char *closetext)
{
	mSubject = iggSubjectFactory::CreateDialogSubject(this,base,mode,icon,title.IsEmpty()?title:("IFrIT - "+title)); IERROR_CHECK_MEMORY(mSubject);
	if(mMainWindow != 0) mMainWindow->Register(mSubject);

	if(cols < 1)
	{
		cols = 1;
		IBUG_WARN("Incorrect number of columns in iggDialog frame.");
	}

	mMode = mode;

	if(closetext!=0 && (closetext[0]!=0 || iDynamicCast<iggShell,iShell>(INFO,this->GetShell())->CheckCondition(iParameter::Condition::OldWindowManager)))
	{
		iggFrame *tmp = new iggFrame(this->GetShell(),3); IERROR_CHECK_MEMORY(tmp);
		mSubject->SetFrame(tmp->GetSubject());
		mFrame = new iggFrame(tmp,cols); IERROR_CHECK_MEMORY(mFrame);
		tmp->AddLine(mFrame,3);
		iggFrame *tmp1 = new iggFrame("",tmp);
		tmp1->AddLine(new iggWidgetDialogCloseButton(this,tmp1,(closetext[0]==0)?"Close":closetext));
		tmp->AddLine(static_cast<iggWidget*>(0),tmp1,static_cast<iggWidget*>(0));
		tmp->SetColStretch(0,10);
		tmp->SetColStretch(2,10);
		if((mMode & iParameter::DialogFlag::NoTitleBar) != 0)
		{
			tmp->SetPadding(true);
		}
	}
	else
	{
		mFrame = new iggFrame(this->GetShell(),cols); IERROR_CHECK_MEMORY(mFrame);
		mSubject->SetFrame(mFrame->GetSubject());
	}
	mFrame->SetPadding(true);

	if(helpkey == 0)
	{
		mHelp = 0;
	}
	else
	{
		mHelp = new iHelpDataBuffer(iHelpFactory::FindData(helpkey,iHelpFactory::_ShellReference));
	}
	mFrame->SetBaloonHelp(this->GetToolTip(),this->GetBaloonHelp());

	mCompleteConstructionCalled = false;
	List().Add(this);
}


iggDialog::~iggDialog()
{
	mSubject->Show(false);

	List().Remove(this);

	delete mFrame;
	if(mHelp != 0) delete mHelp;
	if(mMainWindow != 0) mMainWindow->UnRegister(mSubject);
}


iLookupArray<iggDialog*>& iggDialog::List()
{
	static iLookupArray<iggDialog*> list(100);
	return list;
}


void iggDialog::AttachSubject(ibgDialogSubject* /*subject*/)
{
	//
	//  The subject has been created in the constructor. This funcion is for SubjectFactory interface only
	//
}


const iString& iggDialog::GetToolTip() const
{
	static const iString none;
	//
	//  By default dialogs have no tooltips, as they are quite distracting - only specific widgets do
	//
	return none;
}


iString iggDialog::GetBaloonHelp() const
{
	static const iString none;

	if(mHelp != 0) return mHelp->GetHTML(); else return none;
}


void iggDialog::Show(bool s)
{
	if(mFrame == 0)
	{
		IBUG_WARN("No frame was set for this dialog.");
		return;
	}

	if(!mCompleteConstructionCalled) this->CompleteConstruction();

	this->ShowBody(s);
}


void iggDialog::ShowBody(bool s)
{
	if(s)
	{
		if(mSubject->IsVisible()) return;

		this->UpdateDialog();
		mSubject->Show(true);
	}
	else mSubject->Show(false);
}

	
bool iggDialog::IsVisible() const
{
	return mSubject->IsVisible();
}


void iggDialog::UpdateDialog()
{
	if(mFrame == 0)
	{
		IBUG_WARN("No frame was set for this dialog.");
		return;
	}
	mFrame->UpdateWidget();
}


void iggDialog::ResizeContents(int w, int h)
{
	int wg[4];
	mSubject->GetWindowGeometry(wg);
	wg[2] = w;
	wg[3] = h;
	mSubject->SetWindowGeometry(wg);
}


bool iggDialog::CanBeClosed()
{
	return true; // closeable
}


void iggDialog::Close()
{
	if(this->CanBeClosed()) this->Show(false);
}


bool iggDialog::ImmediateConstruction() const
{
	return this->GetShell()->CheckCondition(iParameter::Condition::DisableDelayedInitialization);
}


void iggDialog::CompleteConstruction()
{
	if(mCompleteConstructionCalled) return;

	this->CompleteConstructionBody();
	mCompleteConstructionCalled = true;
}


#ifdef I_DEBUG
void iggDialog::FlashAllDialogs()
{
	int i;
	iLookupArray<iggDialog*>& list(List());
	for(i=0; i<list.Size(); i++)
	{
		list[i]->Show(true);
		iSystem::Sleep(300);
		list[i]->Show(false);
	}
}
#endif

#endif
