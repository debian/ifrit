/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A base class for all generic (toolkit-independent) widgets.
//
//  The GUI hierarchy tree is build in the following manner. All igg... classes are
//  completely toolkit-independent and comprise their own independent hierarchy. They all
//  derive from this class. Those of igg... classes that need toolkit-dependent capabilities
//  have subject ibg...Subject (the same name) classes as members, and decorate them. The ibg... 
//  classes form a usual system, with toolkit-independent parents and toolkit-dependent children, 
//  like vtkRenderWindow, etc classes. 
//
//  In addition, ibg... classes use helpers to implement common functionality. For example,
//  both ibgWidgetSubject and ibgFrameSubject use Show(bool) function. If this function is 
//  implemented as pure virtual, and inherited in specific Qt-based chidren iqtWidgetSubject 
//  and iqtFrameSubject, then the same Show(bool){ if(s) this->show; ... } function will 
//  appear in both classes, causing code replication. Instead, both ibg... classes have an 
//  abstract ibgWidgetHelper member, which does Show(bool) for them. It is inherited by 
//  iqtWidgetHelper that actually implements Show(bool){...} only once.
//

#ifndef IGGWIDGET_H
#define IGGWIDGET_H


#include "iggrenderingelement.h"


#include "iarray.h"

class iColor;
class iString;

class iggFrame;
class iggFrameBase;
class iggMainWindow;

class ibgWidgetHelper;
class ibgWidgetSubject;


namespace iParameter
{
	namespace ButtonType
	{
		const int PushButton = 0;
		const int ToolButton = 1;
		const int SwitchButton = 2;
		const int ToggleButton = 3;
		const int CheckBox = 4;
	};
};


class iggWidget : public iggRenderingElement
{

	friend class iggFrame;
	friend class iggSubjectFactory;
	friend class ibgWidgetHelper;

public:

	virtual ~iggWidget();

	void AttachSubject(ibgWidgetSubject *subject);

	inline iggMainWindow* GetMainWindow() const { return mMainWindow; }
	inline iggFrameBase* GetParent() const { return mParent; }
	inline ibgWidgetHelper* GetHelper() const { return mWidgetHelper; }

	void SetBaloonHelp(const iString &tooltip, const iString &whatsthis);
	void SetBaloonHelp(const iString &help);
	virtual bool CheckBaloonHelpStatus();

	virtual void AddDependent(iggWidget *w);
	virtual void RemoveDependent(iggWidget *w);
	void AddBuddy(iggWidget *buddy);     //  Buddies are mutual dependents
	void RemoveBuddy(iggWidget *buddy);

	virtual void Show(bool s);
	bool IsVisible() const;

	virtual void Enable(bool s);
	bool IsEnabled() const;

	void Emphasize(bool s, bool random = false);

	void UpdateWidget();
	
#ifdef I_DEBUG
	static void EmphasizeLayouts(bool s);
#endif

protected:

	iggWidget(iggFrameBase *parent);
	iggWidget(iggShell *shell);

	virtual void UpdateWidgetBody() = 0;
	//
	//  Dependencies are updated when this widget is executed
	//
	virtual void UpdateDependents();
	void AddBuddyBody(iggWidget *buddy);
	void RemoveBuddyBody(iggWidget *buddy);

	void UpdateFailed() const;

	//
	//  generic slots
	//
	virtual void OnInt1Body(int){}
	virtual void OnInt2Body(int){}
	virtual void OnVoid1Body(){}
	virtual void OnVoid2Body(){}
	virtual void OnVoid3Body(){}
///	virtual void OnVoid4Body(){}
	virtual void OnBool1Body(bool){}

	iggFrameBase *mParent;
	iLookupArray<iggWidget*> mBuddies;
	iLookupArray<iggWidget*> mDependents;
	bool mNeedsBaloonHelp, mWasLaidOut, mSubjectOwner, mInitialized, mHelpIsSet, mInUpdate, mDisableDepedentUpdate;
	iggMainWindow *mMainWindow;
	ibgWidgetHelper *mWidgetHelper;
	ibgWidgetSubject *mWidgetSubject;

private:

	void Define();

	//
	//  Registry
	//
	static iLookupArray<iggWidget*>& List();

	iggWidget(const iggWidget&); // Not implemented.
	void operator=(const iggWidget&);  // Not implemented.
};

#endif  // IGGWIDGET_H

