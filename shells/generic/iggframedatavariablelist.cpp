/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggframedatavariablelist.h"


#include "idata.h"
#include "idatalimits.h"
#include "idatareader.h"
#include "idatasubject.h"
#include "ierror.h"
#include "iviewmodule.h"

#include "igghandle.h"
#include "iggshell.h"
#include "iggwidgetarea.h"
#include "iggwidgetmisc.h"
#include "iggwidgetpropertycontrolselectionbox.h"

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"


namespace iggFrameDataVariableList_Private
{
	class LayerBase: public iggFrame
	{

	public:

		LayerBase(const iString& title, iggFrameBase *parent) : iggFrame(title,parent)
		{
		}

		LayerBase(iggFrameBase *parent) : iggFrame(parent)
		{
		}

		//
		//  Decorator functions
		//
		virtual void InsertItem(const iString &text, int index = -1) = 0;
		virtual void SetItem(const iString &text, int index, bool vis) = 0;
		virtual void RemoveItem(int index) = 0;
		virtual void SetOffset(int offset) = 0;
		virtual int GetValue() const = 0;
		virtual int Count() const = 0;
	};

	template<class T>
	class Layer : public LayerBase
	{

	private:

		T *mWidget;

	public:

		Layer(const iString& title, iggFrameBase *parent);
		Layer(const iString& title, const iggPropertyHandle& ph, iggFrameBase *parent);

		virtual void InsertItem(const iString &text, int index = -1)
		{
			mWidget->InsertItem(text,index);
		}

		virtual void SetItem(const iString &text, int index, bool vis)
		{
			mWidget->SetItem(text,index,vis);
		}

		virtual void RemoveItem(int index)
		{
			mWidget->RemoveItem(index);
		}

		virtual void SetOffset(int offset)
		{
		}

		virtual int GetValue() const
		{
			return mWidget->GetValue();
		}

		virtual int Count() const
		{
			return mWidget->Count();
		}
	};

	template<> Layer<iggWidgetSimpleComboBox>::Layer(const iString& title, iggFrameBase *parent);
	template<> Layer<iggWidgetSimpleRadioBox>::Layer(const iString& title, iggFrameBase *parent);
	template<> Layer<iggWidgetPropertyControlComboBox>::Layer(const iString& title, const iggPropertyHandle& ph, iggFrameBase *parent);
	template<> Layer<iggWidgetPropertyControlRadioBox>::Layer(const iString& title, const iggPropertyHandle& ph, iggFrameBase *parent);

	template<>
	void Layer<iggWidgetPropertyControlComboBox>::SetOffset(int offset)
	{
		mWidget->SetOffset(offset);
	}

	template<>
	void Layer<iggWidgetPropertyControlRadioBox>::SetOffset(int offset)
	{
		mWidget->SetOffset(offset);
	}
};


using namespace iggFrameDataVariableList_Private;


iggFrameDataVariableList::iggFrameDataVariableList(const iString &title, const iggObjectHandle *dsh, iggFrame *parent, const iggPropertyHandle *eh) : iggFrameFlip(parent)
{
	this->Define(title,dsh,0,0,parent,eh);
}


iggFrameDataVariableList::iggFrameDataVariableList(const iString &title, const iggObjectHandle *dsh, const iggPropertyHandle& ph, iggFrame *parent, const iggPropertyHandle *eh) : iggFrameFlip(parent)
{
	this->Define(title,dsh,0,&ph,parent,eh);
}


iggFrameDataVariableList::iggFrameDataVariableList(const iString &title, const iggDataTypeHandle *dth, const iggPropertyHandle& ph, iggFrame *parent, const iggPropertyHandle *eh) : iggFrameFlip(parent)
{
	this->Define(title,0,dth,&ph,parent,eh);
}


void iggFrameDataVariableList::Define(const iString &title, const iggObjectHandle *dsh, const iggDataTypeHandle *dth, const iggPropertyHandle *ph, iggFrame *parent, const iggPropertyHandle* eh)
{
	if(dsh==0 && dth==0)
	{
		IBUG_FATAL("iggFrameDataVariableList: either an ObjectHandle or a DataTypeHandle must be set.");
	}

	mDataSubjectHandle = dsh;
	mDataTypeHandle = dth;
	
	if(ph == 0)
	{
		mLayers[0] = new Layer<iggWidgetSimpleComboBox>(title,this);
		mLayers[1] = new Layer<iggWidgetSimpleRadioBox>(title,this);
	}
	else
	{
		mLayers[0] = new Layer<iggWidgetPropertyControlComboBox>(title,*ph,this);
		mLayers[1] = new Layer<iggWidgetPropertyControlRadioBox>(title,*ph,this);
	}

	this->AddLayer(mLayers[0]);
	this->AddLayer(mLayers[1]);

	if(eh != 0)
	{
		mEnablingHandle = new iggPropertyHandle(*eh);
	}
	else
	{
		mEnablingHandle = 0;
	}

	mCompleted = false;
	mOffset = 0;
}


iggFrameDataVariableList::~iggFrameDataVariableList()
{
	if(mEnablingHandle != 0) delete mEnablingHandle;
}


void iggFrameDataVariableList::Complete()
{
	if(!mCompleted)
	{
		mCompleted = true;

		LayerBase *layer0 = dynamic_cast<LayerBase*>(mLayers[0]); IASSERT(layer0);
		LayerBase *layer1 = dynamic_cast<LayerBase*>(mLayers[1]); IASSERT(layer1);

		mOffset = layer0->Count();
		layer0->SetOffset(mOffset);
		layer1->SetOffset(mOffset);

		this->UpdateList();
	}
	else 
	{
		IBUG_WARN("Cannot complete FrameDataVariableList twice.");
	}
}


void iggFrameDataVariableList::UpdateList()
{
	if(!mCompleted)
	{
		IBUG_WARN("FrameDataVariableList must be completed before the first update.");
		return;
	}

	bool en = true;
	if(mEnablingHandle != 0)
	{
		const iPropertyQuery<iType::Bool> *prop = dynamic_cast<const iPropertyQuery<iType::Bool>*>(mEnablingHandle->Function());
		if(prop == 0)
		{
			IBUG_WARN("FrameDataVariableList is configured incorrectly (unable to cast EnablingHandle).");
			return;
		}
		en = prop->GetQuery(); 
	}

	//
	//  We need to find our subject
	//
	const iDataSubject *ds;
	if(mDataSubjectHandle != 0)
	{
		ds = dynamic_cast<const iDataSubject*>(mDataSubjectHandle->Object());
	}
	else
	{
		ds= this->GetShell()->GetViewModule()->GetReader()->GetSubject(mDataTypeHandle->GetActiveDataType());
	}
	if(ds == 0)
	{
		IBUG_WARN("FrameDataVariableList is configured incorrectly (unable to cast DataSubject).");
		return;
	}

	LayerBase *layer0 = dynamic_cast<LayerBase*>(mLayers[0]); IASSERT(layer0);
	LayerBase *layer1 = dynamic_cast<LayerBase*>(mLayers[1]); IASSERT(layer1);

	int n = ds->GetLimits()->GetNumVars();
	int na = en ? n : 0;
	int np = layer0->Count() - mOffset;

	int i;
	for(i=0; i<n; i++)
	{
		layer0->SetItem(ds->GetLimits()->GetName(i),mOffset+i,i<na);
		if(i < 3)
		{
			layer1->SetItem(ds->GetLimits()->GetName(i),mOffset+i,i<na);
		}
	}
	for(i=n; i<3; i++)
	{
		layer1->SetItem(" ",mOffset+i,false);
	}
	for(i=n; i<np; i++)
	{
		layer0->RemoveItem(mOffset+n); // this deletes an item, so they shift down
	}
}


void iggFrameDataVariableList::UpdateChildren()
{
	if(!mCompleted)
	{
		IBUG_WARN("FrameDataVariableList must be completed before the first update.");
		return;
	}

	this->UpdateList();

	LayerBase *layer0 = dynamic_cast<LayerBase*>(mLayers[0]); IASSERT(layer0);
	LayerBase *layer1 = dynamic_cast<LayerBase*>(mLayers[1]); IASSERT(layer1);

	int np = layer0->Count();
	if(np > 3+mOffset) // RadioBox cannot be too large
	{
		this->ShowLayer(0);
	}
	else
	{
		this->ShowLayer(1);
	}

	this->iggFrameFlip::UpdateChildren();
}


void iggFrameDataVariableList::InsertItem(const iString &text)
{
	if(!mCompleted)
	{
		LayerBase *layer0 = dynamic_cast<LayerBase*>(mLayers[0]); IASSERT(layer0);
		LayerBase *layer1 = dynamic_cast<LayerBase*>(mLayers[1]); IASSERT(layer1);

		layer0->InsertItem(text);
		layer1->InsertItem(text);
	}
	else
	{
		IBUG_WARN("Cannot insert items in the completed FrameDataVariableList.");
	}
}


int iggFrameDataVariableList::GetIndex() const
{
	LayerBase *layer0 = dynamic_cast<LayerBase*>(mLayers[0]); IASSERT(layer0);

	return layer0->GetValue();
}


namespace iggFrameDataVariableList_Private
{
	template<>
	Layer<iggWidgetSimpleComboBox>::Layer(const iString& title, iggFrameBase *parent) : LayerBase(title,parent)
	{
		mWidget = new iggWidgetSimpleComboBox("",this);
		mWidget->SetBaloonHelp("Choose a variable");
		this->AddLine(mWidget);
		this->AddSpace(1);
	}

	template<>
	Layer<iggWidgetSimpleRadioBox>::Layer(const iString& title, iggFrameBase *parent) : LayerBase(parent)
	{
		mWidget = new iggWidgetSimpleRadioBox(1,title,this);
		mWidget->SetBaloonHelp("Choose a variable");
		this->AddLine(mWidget);
		//this->AddSpace(1);
	}

	template<>
	Layer<iggWidgetPropertyControlComboBox>::Layer(const iString& title, const iggPropertyHandle& ph, iggFrameBase *parent) : LayerBase(title,parent)
	{
		mWidget = new iggWidgetPropertyControlComboBox("",0,ph,this);
		this->AddLine(mWidget);
		this->AddSpace(1);
	}

	template<>
	Layer<iggWidgetPropertyControlRadioBox>::Layer(const iString& title, const iggPropertyHandle& ph, iggFrameBase *parent) : LayerBase(parent)
	{
		mWidget = new iggWidgetPropertyControlRadioBox(1,title,0,ph,this);
		this->AddLine(mWidget);
		//this->AddSpace(1);
	}
};

#endif
