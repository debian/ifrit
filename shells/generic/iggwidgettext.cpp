/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggwidgettext.h"


#include "ierror.h"
#include "ihelpfactory.h"
#include "iobject.h"
#include "istring.h"

#include "iggframe.h"
#include "iggimagefactory.h"
#include "iggshell.h"

#include "ibgwidgethelpbrowsersubject.h"
#include "ibgwidgettexteditorsubject.h"

#include "iggsubjectfactory.h"

//
//  Templates
//
#include "iarray.tlh"


using namespace iParameter;


//
//  iggWidgetTextEditor class
//
iggWidgetTextEditor::iggWidgetTextEditor(unsigned int mode, iggFrame *parent) : iggWidget(parent)
{
	mSubject = iggSubjectFactory::CreateWidgetTextEditorSubject(this,mode);
}


iggWidgetTextEditor::~iggWidgetTextEditor()
{
}


void iggWidgetTextEditor::UpdateWidgetBody()
{
}


void iggWidgetTextEditor::Clear()
{
	mSubject->Clear();
}


void iggWidgetTextEditor::SetModified(bool s)
{
	mSubject->SetModified(s);
}


bool iggWidgetTextEditor::SupportsHTML() const
{
	return mSubject->SupportsHTML();
}


bool iggWidgetTextEditor::TextModified() const
{
	return mSubject->TextModified();
}


void iggWidgetTextEditor::SetText(const iString &text)
{
	mSubject->SetText(text);
}


iString iggWidgetTextEditor::GetText() const
{
	return mSubject->GetText();
}


iString iggWidgetTextEditor::GetLine(int n) const
{
	return mSubject->GetLine(n);
}


int iggWidgetTextEditor::GetNumberOfLines() const
{
	return mSubject->GetNumberOfLines();
}


void iggWidgetTextEditor::RemoveLine(int n)
{
	mSubject->RemoveLine(n);
}


void iggWidgetTextEditor::AdjustFontSize(int val)
{
	mSubject->AdjustFontSize(val);
}


//
//  Selection
//
/*void iggWidgetTextEditor::Select(int lineFrom, int indexFrom, int lineTo, int indexTo, int selNum)
{
	mSubject->Select(lineFrom,indexFrom,lineTo,indexTo,false,selNum);
}*/


//
//  Editing functions
//
bool iggWidgetTextEditor::HasEditingFunction(int type) const
{
	return mSubject->HasEditingFunction(type);
}


void iggWidgetTextEditor::UseEditingFunction(int type)
{
	mSubject->UseEditingFunction(type);
}


//
//  iggWidgetTextBrowser class
//
iggWidgetTextBrowser::iggWidgetTextBrowser(bool wrapping, bool blockselection, iggFrame *parent, bool withHTML) : iggWidgetTextEditor(TextEditorFlag::ReadOnly|(withHTML?TextEditorFlag::WithHTML:0)|(wrapping?TextEditorFlag::Wrapping:0U),parent)
{
	mBlockSelection = blockselection;
	mNeedsBaloonHelp = false;
}


void iggWidgetTextBrowser::AppendTextLine(const iString &text, const iColor &textcolor)
{
	static const iString none;

	this->AppendTextLine(none,text,textcolor);
}


void iggWidgetTextBrowser::AppendTextLine(const iString &prefix, const iString &text, const iColor &textcolor, const iColor &prefixcolor, bool prefixbold)
{
	if(mSubject->SupportsHTML())
	{
		iString ws;
		if(!mSubject->AppendBreaksLine()) ws = "<br>";
		if(!prefix.IsEmpty() && prefix!="\t")
		{
			ws += "<font color=\"#" + iString::FromNumber(prefixcolor.Red(),"%02X") + iString::FromNumber(prefixcolor.Green(),"%02X") + iString::FromNumber(prefixcolor.Blue(),"%02X") + "\">";
			if(prefixbold) ws += "<b>";
			ws += prefix + ": ";
			if(prefixbold) ws += "</b>";
			if(prefixcolor != textcolor)
			{
				ws += "</font><font color=\"#" + iString::FromNumber(textcolor.Red(),"%02X") + iString::FromNumber(textcolor.Green(),"%02X") + iString::FromNumber(textcolor.Blue(),"%02X") + "\">";
			}
		}
		else
		{
			if(prefix == "\t") ws += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"; 
			ws += "<font color=\"#" + iString::FromNumber(textcolor.Red(),"%02X") + iString::FromNumber(textcolor.Green(),"%02X") + iString::FromNumber(textcolor.Blue(),"%02X") + "\">";
		}
		ws += text + "</font>";
		mSubject->AppendText(ws);
	}
	else
	{
		if(prefix.IsEmpty() || prefix=="\t")
		{
			iString ws;
			if(prefix == "\t") ws += "        ";
			ws += text;
			mSubject->SetTextColor(textcolor);
			if(mSubject->AppendBreaksLine())
			{
				mSubject->AppendText(ws);
			}
			else
			{
				mSubject->AppendText(ws+"\n");
			}
		}
		else
		{
			if(mSubject->AppendBreaksLine())
			{
				mSubject->SetTextColor(textcolor);
				mSubject->AppendText(prefix+": "+text);
			}
			else
			{
				mSubject->SetTextColor(prefixcolor);
				mSubject->SetBoldText(prefixbold);
				mSubject->AppendText(prefix+": ");
				mSubject->SetBoldText(false);
				mSubject->SetTextColor(textcolor);
				mSubject->AppendText(text+"\n");
			}
		}
	}
}


void iggWidgetTextBrowser::OnSelectionChanged(int /*lineFrom*/, int /*indexFrom*/, int /*lineTo*/, int /*indexTo*/)
{
	//
	//  remove selection if it is present
	//
	if(mBlockSelection) mSubject->Select(0,0,0,0);
}


//
//  iggWidgetListView class
//
iggWidgetListView::iggWidgetListView(bool withHTML, iggFrame *parent) : iggWidgetTextEditor(withHTML?(TextEditorFlag::ReadOnly|TextEditorFlag::WithHTML):TextEditorFlag::ReadOnly,parent)
{
	mSelectedRange[0] = 0;
	mSelectedRange[1] = -1;

	mNeedsBaloonHelp = false;
	mNumItems = 0;
}


void iggWidgetListView::UpdateWidgetBody()
{
	if(this->HasSelection())
	{
		this->Select(mSelectedRange[0],mSelectedRange[1]);
	}
	this->iggWidgetTextEditor::UpdateWidgetBody();
}


void iggWidgetListView::InsertItem(const iString &text, const iColor &color)
{
	mSubject->SetTextColor(color);
	if(!mSubject->AppendBreaksLine())
	{
		if(mSubject->SupportsHTML()) mSubject->AppendText("<p>"+text,false); else mSubject->AppendText(text+"\n",false);
	}
	else
	{
		mSubject->AppendText(text,false);
	}
	mNumItems++;
	mSelectedRange[0] = 0;
	mSelectedRange[1] = -1;
}


iString iggWidgetListView::GetItem(int n) const
{
	return mSubject->GetLine(n);
}


void iggWidgetListView::Clear()
{
	mSelectedRange[0] = 0;
	mSelectedRange[1] = -1;
	mNumItems = 0;
	mSubject->Clear();
}


void iggWidgetListView::Select(int lineFrom, int lineTo)
{
	//
	//  Select full lines only
	//
	if(lineFrom>=0 && lineTo>=0 && lineFrom<this->GetNumberOfLines() && lineTo<this->GetNumberOfLines())
	{
		mSubject->Select(lineFrom,0,lineTo,mSubject->GetLine(lineTo).Length(),true); // justify left
		mSelectedRange[0] = lineFrom;
		mSelectedRange[1] = lineTo;
	}
}


void iggWidgetListView::OnSelectionChanged(int lineFrom, int /*indexFrom*/, int lineTo, int /*indexTo*/)
{
	this->Select(lineFrom,lineTo);
}


void iggWidgetListView::OnCursorPositionChanged(int line, int /*index*/)
{
	this->Select(line,line);
}


//
//  iggWidgetHelpBrowser class
//
iggWidgetHelpBrowser::iggWidgetHelpBrowser(iggFrame *parent) : iggWidgetTextBrowser(true,false,parent)
{
	mHTMLSubject = iggSubjectFactory::CreateWidgetHelpBrowserSubject(this);
	
	if(mHTMLSubject != 0)
	{
		delete mSubject;
		mSubject = 0;
	}

	mCurrentTag = -1;
}


iString iggWidgetHelpBrowser::GetContentsText() const
{
	if(mHTMLSubject != 0)
	{
		return mHTMLSubject->GetText();
	}
	else
	{
		return mSubject->GetText();
	}
}


void iggWidgetHelpBrowser::SetContentsText(const iString &text)
{
	if(mHTMLSubject != 0)
	{
		//
		//  Ask subject to remember the images
		//
		const iImage *im;
		iString name, tmp = text;
		int i = tmp.Find("\"images::");
		while(i >= 0)
		{
			tmp = tmp.Part(i+1);
			name = tmp.Section("\"",0,0);
			im = iggImageFactory::FindHelpChart(name.Part(8));
			if(im != 0) 
			{
				mHTMLSubject->AddImage(name,*im);
			}
			else
			{
				IBUG_WARN(("Missing image "+name).ToCharPointer());
			}
			i = tmp.Find("\"images::");
		}
		
		tmp = text;
		tmp.RemoveFormatting("<pdfonly>","</pdfonly>");

		mHTMLSubject->SetText(tmp);
	}
	else
	{
		if(this->SupportsHTML()) this->SetText(text); else
		{
			iString tmp = text;
			tmp.ReformatHTMLToText(80);
			this->SetText(tmp);
		}
	}
}


void iggWidgetHelpBrowser::SetContents(const char *tag, bool resetHistory)
{
	const iHelpDataBuffer &buf = iHelpFactory::FindData(tag,iHelpFactory::_All,true);

	if(!buf.IsNull())
	{
		iString text = buf.GetHTML();

		this->ModifyText(tag,text);
		this->SetContentsText(text);

		if(resetHistory)
		{
			mCurrentTag++;
			while(mVisitedTags.Size() > mCurrentTag) mVisitedTags.RemoveLast();
			if(mCurrentTag > mVisitedTags.MaxIndex()) mVisitedTags.Add(iString(tag));
		}
	
		this->UpdateDependents();
	}
	else this->OnNullHelpBuffer();
}


//
//  HTML Navigation
//
void iggWidgetHelpBrowser::Home()
{
	if(mVisitedTags.Size() > 0)
	{ 
		mCurrentTag = 0;
		this->SetContents(mVisitedTags[mCurrentTag].ToCharPointer(),false);
	}
}


void iggWidgetHelpBrowser::Forward()
{
	if(this->CanGoForward())
	{ 
		mCurrentTag++;
		this->SetContents(mVisitedTags[mCurrentTag].ToCharPointer(),false);
	}
}


void iggWidgetHelpBrowser::Backward()
{
	if(this->CanGoBackward())
	{ 
		mCurrentTag--;
		this->SetContents(mVisitedTags[mCurrentTag].ToCharPointer(),false);
	}
}
		

void iggWidgetHelpBrowser::AddDependent(iggWidget *c)
{
	iggWidgetTextBrowser::AddDependent(c);
}


void iggWidgetHelpBrowser::OnLinkFollowed(const char *tag)
{
	if(tag != 0) this->SetContents(tag);
}


void iggWidgetHelpBrowser::OnNullHelpBuffer()
{
}


void iggWidgetHelpBrowser::ModifyText(const char *tag, iString &text)
{
	if(tag[0]=='o' && tag[1]=='r' && tag[2]=='.')
	{
		//
		//  This a tag to an object reference item. Is it the description of an object?
		//
		iString tmp = (tag+3);
		if(tmp.Contains('.') == 0)
		{
			text = iHelpFactory::FormObjectDescription(tmp,false);
#ifndef I_NO_CHECK
			if(text.IsEmpty())
			{
				IBUG_ERROR("Undefined reference tag: "+iString(tmp));
			}
#endif
		}
		else
		{
			text = iHelpFactory::FormPropertyDescription(tmp,false,true);
#ifndef I_NO_CHECK
			if(text.IsEmpty())
			{
				IBUG_ERROR("Undefined reference tag: "+iString(tmp));
			}
#endif
		}
	}

	iHelpFactory::InterpretExtraElements(text);
}


//void iggWidgetHelpBrowser::OnMousePress(int line, int index)
//{
//}


//void iggWidgetHelpBrowser::OnMouseRelease(int line, int index)
//{
//}

#endif
