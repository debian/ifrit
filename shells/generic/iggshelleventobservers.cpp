/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggshelleventobservers.h"


#include "ierror.h"
#include "iviewmodule.h"

#include "iggdialogparallelcontroller.h"
#include "iggmainwindow.h"
#include "iggshell.h"
#include "iggwidgetprogressbar.h"

#include "ibgmenuwindowsubject.h"


using namespace iParameter;


//
//  iggExecutionEventObserver class
//
iggExecutionEventObserver::iggExecutionEventObserver(iggShell *s) : iExecutionEventObserver(s)
{
	mCurrentProgressBar = 0;
	mShell = s; IASSERT(s);
}


void iggExecutionEventObserver::OnStart()
{
	if(mCurrentProgressBar==0 && mShell->GetMainWindow()!=0 && mShell->GetMainWindow()->GetProgressBar()!=0) 
	{
		mCurrentProgressBar = mShell->GetMainWindow()->GetProgressBar();
	}
	if(mCurrentProgressBar != 0)
	{
		mCurrentProgressBar->Reset();
	}
}


void iggExecutionEventObserver::OnSetLabel(const iString &label)
{
	if(mCurrentProgressBar != 0)
	{
		mCurrentProgressBar->SetLabel(label);
	}
}


void iggExecutionEventObserver::OnProgress(double f, bool &abort)
{
	if(mCurrentProgressBar != 0)
	{
		mCurrentProgressBar->SetProgress(iMath::Round2Int(f*100.0));
		abort = mCurrentProgressBar->IsAborted();
	}
	else
	{
		abort = false;
	}
}


void iggExecutionEventObserver::OnFinish()
{
	if(mCurrentProgressBar != 0)
	{
		mCurrentProgressBar->Reset();
		mCurrentProgressBar = 0;
	}
}


void iggExecutionEventObserver::SetProgressBar(iggWidgetProgressBar *pb)
{
	mCurrentProgressBar = pb;
}


//
//  iggParallelUpdateEventObserver class
//
iggParallelUpdateEventObserver::iggParallelUpdateEventObserver(iggShell *s) : iParallelUpdateEventObserver(s)
{
	mShell = s; IASSERT(s);
}


void iggParallelUpdateEventObserver::UpdateInformation()
{
	if(mShell->GetMainWindow()!=0 && mShell->GetMainWindow()->GetDialogParallelController()!=0) 
	{
		mShell->GetMainWindow()->GetDialogParallelController()->UpdateDisplay();
	}
}

#endif
