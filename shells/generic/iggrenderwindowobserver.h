/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A widget that updates its parent when RenderWindow is modified
//
#ifndef IGGRENDERWINDOWOBSERVER_H
#define IGGRENDERWINDOWOBSERVER_H


#include "ieventobserver.h"


class iggFrame;
class iggRenderWindowObserverHelper;

class vtkRenderWindow;


class iggRenderWindowObserver
{

	friend class iggFrameBase;
	friend class iggRenderWindowObserverHelper;

public:

	virtual ~iggRenderWindowObserver();

protected:

	iggRenderWindowObserver(iggFrame *parent, bool disabled = false);

	void UpdateConnection();
	virtual void OnRenderWindowModified() = 0;

	iggFrame *mParent;
	iggRenderWindowObserverHelper *mHelper;
};


class iggRenderWindowObserverHelper : public iEventObserver
{

	friend class iggRenderWindowObserver;

public:
	
	vtkTypeMacro(iggRenderWindowObserverHelper,iEventObserver);

protected:

	iggRenderWindowObserverHelper(iggRenderWindowObserver *owner, iggFrame *frame);
	virtual ~iggRenderWindowObserverHelper();

	virtual void ExecuteBody(vtkObject *caller, unsigned long event, void *data);

	void UpdateConnection();
	void Detach();

private:

	iggFrame *mFrame;
	iggRenderWindowObserver *mOwner;
	vtkRenderWindow *mWindow;
};

#endif
