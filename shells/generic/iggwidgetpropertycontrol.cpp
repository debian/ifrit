/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggwidgetpropertycontrol.h"


#include "ierror.h"
#include "ihelpfactory.h"
#include "iproperty.h"

#include "iggframe.h"
#include "igghandle.h"
#include "iggshell.h"

//
//  templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


using namespace iParameter;


int iggWidgetPropertyControlBase::mGlobalExecuteFlags = 0;


//
//  Base class
//
iggWidgetPropertyControlBase::iggWidgetPropertyControlBase(const iggPropertyHandle& ph, iggFrame *parent, int rm) : iggWidget(parent) //, mProvider(0)
{
	mHandle = new iggPropertyHandle(ph); IERROR_CHECK_MEMORY(mHandle);

	this->Define(rm);
}


void iggWidgetPropertyControlBase::Define(int rm)
{
#ifndef I_NO_CHECK
	if(
		ExecuteFlag::Module::One!=ViewModuleSelection::One ||
		ExecuteFlag::Module::All!=ViewModuleSelection::All ||
		ExecuteFlag::Module::Clones!=ViewModuleSelection::Clones)
	{
		IBUG_FATAL("Incorrect ExecuteFlag::Module flags.");
	};
#endif

	if(mParent == 0) IBUG_FATAL("iggWidgetPropertyControlBase must have a parent.");

	this->SetRenderMode(rm);
	mExecuteFlags = ExecuteFlag::Default;

	//
	//  Find all my buddies (same object, same property)
	//
	int i;
	const iString &objname(mHandle->Object()->ShortName());
	iLookupArray<iggWidgetPropertyControlBase*> &list = iggWidgetPropertyControlBase::List();
	for(i=0; i<list.Size(); i++)
	{
		if(list[i]->Handle()->Name()==mHandle->Name() && list[i]->Handle()->Object()->ShortName()==objname) list[i]->AddBuddy(this);
	}
	//
	//  Add myself to the list
	//
	list.AddUnique(this);

	mHelpIsSet = false;
}


iggWidgetPropertyControlBase::~iggWidgetPropertyControlBase()
{
	iggWidgetPropertyControlBase::List().Remove(this);

	if(mNeedsBaloonHelp && !mHelpIsSet)
	{
		//
		//  We were never updated, so the help has never been set. Then we do not need it at all.
		//
		mNeedsBaloonHelp = false;
	}

	delete mHandle;
}


void iggWidgetPropertyControlBase::UpdateHelp()
{
	if(mNeedsBaloonHelp && !mHelpIsSet)
	{
		mHelpIsSet = true;

		iString s = "or." + mHandle->Variable()->HelpTag();
		const iHelpDataBuffer &buf = iHelpFactory::FindData(s.ToCharPointer(),iHelpFactory::_ObjectReference);
		this->SetBaloonHelp("Controls the "+mHandle->Name()+" property. Press Shift+F1 for more help.",(buf.IsNull())?"No help is set for this property.":buf.GetHTML());
	}
}


iLookupArray<iggWidgetPropertyControlBase*>& iggWidgetPropertyControlBase::List()
{
	static iLookupArray<iggWidgetPropertyControlBase*> list(100);
	return list;
}


iLookupArray<iggWidgetPropertyControlBase*>& iggWidgetPropertyControlBase::PositionList()
{
	static iLookupArray<iggWidgetPropertyControlBase*> list;
	return list;
}


iLookupArray<iggWidgetPropertyControlBase*>& iggWidgetPropertyControlBase::VariableLimitsList()
{
	static iLookupArray<iggWidgetPropertyControlBase*> list;
	return list;
}


bool iggWidgetPropertyControlBase::CheckBaloonHelpStatus()
{
	return !mHelpIsSet || this->iggWidget::CheckBaloonHelpStatus();
}


int iggWidgetPropertyControlBase::GetRenderMode() const
{
	return mRenderMode;
}


void iggWidgetPropertyControlBase::SetRenderMode(int m)
{
	mRenderMode = m;
}


void iggWidgetPropertyControlBase::SetExecuteFlags(int f)
{
	mExecuteFlags = f;

#ifndef I_NO_CHECK
	int idxOption = f & ExecuteFlag::Index::Mask;
	int modOption = f & ExecuteFlag::Module::Mask;

	//
	//  Check that modes are valid
	//
	if(
		(idxOption!=ExecuteFlag::Index::One && idxOption!=ExecuteFlag::Index::All) ||
		(modOption!=ExecuteFlag::Module::One && modOption!=ExecuteFlag::Module::All && modOption!=ExecuteFlag::Module::Clones) )
	{
		IBUG_WARN("Invalid option.");
	}
#endif
}


void iggWidgetPropertyControlBase::SetGlobalExecuteFlags(int f)
{
	mGlobalExecuteFlags = f;
}


int iggWidgetPropertyControlBase::GetExecuteFlags() const
{
	if(mGlobalExecuteFlags == 0) return mExecuteFlags;

	int flags = 0;
	if((mGlobalExecuteFlags & ExecuteFlag::Index::Mask) != 0) flags = flags | (mGlobalExecuteFlags & ExecuteFlag::Index::Mask); else flags = flags | (mExecuteFlags & ExecuteFlag::Index::Mask); 
	if((mGlobalExecuteFlags & ExecuteFlag::Module::Mask) != 0) flags = flags | (mGlobalExecuteFlags & ExecuteFlag::Module::Mask); else flags = flags | (mExecuteFlags & ExecuteFlag::Module::Mask); 
	return flags;
}

#endif
