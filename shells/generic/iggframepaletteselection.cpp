/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggframepaletteselection.h"


#include "ipalette.h"
#include "ishell.h"

#include "iggwidgetpropertycontrol.h"

//
//  Templates
//
#include "iarray.tlh"
#include "iggwidgetpropertycontrol.tlh"


using namespace iParameter;


iggFramePaletteSelection::iggFramePaletteSelection(bool withbrightness, const iggPropertyHandle& ph, iggFrame *parent) : iggFramePaletteSelectionBase(true,withbrightness,true,parent)
{
	//
	//  This class combines the functionality of iggWidgetPropertyControl and iggFramePaletteSelectionBase.
	//  Since we cannot inherit both of them (that would create two instances of toolkit-dependent
	//  classes, we create a dummy invisible KeyHandler and use its functionality.
	//
	mDummy = new iggWidgetPropertyControlDummy<iType::Int>(ph,this,RenderMode::Immediate);
}


iggFramePaletteSelection::~iggFramePaletteSelection()
{
}


void iggFramePaletteSelection::SetPaletteId(int n)
{
	mDummy->SetValue(n);
}


int iggFramePaletteSelection::GetPaletteId() const
{
	mDummy->UpdateWidget();
	return mDummy->GetValue();
}

#endif
