/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A class for providing Objects and Properties for GUI elements
//
#ifndef IGGHANDLE_H
#define IGGHANDLE_H


#include "iarray.h"
#include "istring.h"

class iObject;
class iProperty;
class iPropertyFunction;
class iPropertyVariable;
class iString;

class iggElement;
class iggPageObject;
class iggShell;


//
//  Object handles
//  *****************************************
//
//  Base class
//
class iggObjectHandle
{

public:

	virtual const iObject* Object(int mod = -1) const = 0;

	inline iggShell* GetShell() const { return mShell; }

protected:

	iggObjectHandle(iggShell *s);
	iggObjectHandle(iggElement *el);

private:

	iggShell *mShell;
};


//
//  Handle for passing index values
//  *****************************************
//
//  Base class
//
class iggIndexHandle
{

public:

	virtual int Index() const = 0;

protected:

	iggIndexHandle();
};


//
//  trivial handle that returns a fixed index value
//
class iggFixedIndexHandle : public iggIndexHandle
{

public:

	iggFixedIndexHandle(int index);

	virtual int Index() const;

private:

	int mIndex;
};


//
//  Property handle (uses both)
//  *****************************************
//
class iggPropertyHandle
{

public:

	iggPropertyHandle(const iString &name, const iggObjectHandle* oh, int index = 0);
	iggPropertyHandle(const iString &name, const iggObjectHandle* oh, const iggIndexHandle* ih);
	iggPropertyHandle(const iString &name, const iggObjectHandle* oh, const iggPropertyHandle& ih);
	iggPropertyHandle(const iggPropertyHandle& other);
	iggPropertyHandle(const iggPropertyHandle& other, const iString &name);
	~iggPropertyHandle();

	inline const iString& Name() const { return mName; }

	const iPropertyFunction* Function(int mod = -1) const;
	const iPropertyVariable* Variable(int mod = -1) const;
	const iObject* Object(int mod = -1) const;

	int Index() const;

private:

	void Define(const iggPropertyHandle *ih);

	const iString mName;
	const iggObjectHandle* mObjectHandle;

	const int mIndex;
	const iggIndexHandle* mIndexHandle;
	const iggPropertyHandle* mIndexHandle2;

	const iProperty* Property(int mod = -1) const;

	mutable const iObject *mObject;
	mutable const iProperty *mProperty;
	mutable const iPropertyFunction *mFunctionProperty;
	mutable const iPropertyVariable *mVariableProperty;

	void operator=(const iggPropertyHandle& other);  // not implemented
};


//
//  Handle for providing data types
//
class iDataInfo;
class iDataType;
class iViewModule;


class iggDataTypeHandle
{

public:

	iggDataTypeHandle();
	virtual ~iggDataTypeHandle();

	virtual const iDataInfo& GetDataInfo() const = 0;

	const iDataType& GetActiveDataType() const;
	void SetActiveDataType(const iDataType &type);

	virtual int GetActiveDataTypeIndex() const = 0;
	virtual void SetActiveDataTypeIndex(int v);

	static void AfterFileLoad(const iDataInfo &info);

protected:

	virtual void OnActiveDataTypeChanged(int v) = 0;
	virtual void AfterFileLoadBody(const iDataInfo &info) = 0;

private:

	static iLookupArray<iggDataTypeHandle*>& List();
};


//
//  Handle for providing histograms: we separate drawing histograms
//  from figuring out where to get them
//
class iHistogram;
class iHistogramMaker;


class iggHistogramHandle
{

public:

	iggHistogramHandle();
	virtual ~iggHistogramHandle();

	const iHistogram& GetHistogram(float fscale = 1.0) const;
};

#endif  // IGGHANDLE_H

