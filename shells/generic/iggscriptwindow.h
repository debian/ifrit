/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IGGSCRIPTWINDOW_H
#define IGGSCRIPTWINDOW_H


#include "iggmenuwindow.h"


#include "istring.h"

class iScript;

class iggDialog;
class iggFrame;
class iggWidgetScriptEditor;
class iggWidgetSimpleButton;
class iggWidgetTextBrowser;


class iggScriptWindow : public iggMenuWindow
{

public:

	iggScriptWindow(iggMainWindow *parent);
	virtual ~iggScriptWindow();

	virtual void Show(bool s);
	virtual bool CanBeClosed();

	inline int GetRunState() const { return mRunState; }

	void DebugLine(int line, const iString& file);
	inline bool IsDebugging() const { return mIsDebugging; }

protected:

	bool ImmediateConstruction() const; 
	void CompleteConstruction();
	virtual void CompleteConstructionBody();

	virtual void UpdateContents();
	virtual void OnMenuBody(int id, bool on);

	void BuildMenus();
	void LoadFromFile(const iString &fn);
	bool LoadFromFileBody(const iString &fn, bool silent);
	void SaveToFile(const iString &fn);
	void SetContents(const iString &text);
	void CheckModified();

	iScript *mScript;
	iggFrame *mDebugFrame;
	iggDialog *mFindDialog;
	iggWidgetScriptEditor *mEditor;
	iggWidgetTextBrowser *mOutput;

	int mRunState;
	bool mCompleteConstructionCalled, mFirstShow, mIsDebugging;
	iString mCurrentFileName, mHistory, mLastExecutedSegment, mDebuggedFile;
};

#endif  //  IGGSCRIPTWINDOW_H
