/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A frame with palette control. This class is abstract to allow for
//  various ways to specify the palette.
//

#ifndef IGGFRAMEPALETTESELECTIONBASE_H
#define IGGFRAMEPALETTESELECTIONBASE_H


#include "iggframe.h"


#include "iarray.h"

class iPalette;

class iggWidget;


class iggFramePaletteViewFrame : public iggFrame
{

public:

	iggFramePaletteViewFrame(const iString &title, iggFrame *parent);

	virtual iPalette* GetActivePalette() const = 0;

protected:

	iggWidget *mPaletteView;
};


class iggFramePaletteSelectionBase : public iggFramePaletteViewFrame
{

public:

	iggFramePaletteSelectionBase(bool witheditor, bool withbrightness, bool withreverse, iggFrame *parent);
	virtual ~iggFramePaletteSelectionBase();

	virtual iPalette* GetActivePalette() const;

	void SetReversedPalette(bool s);
	bool GetReversedPalette() const;

	void SetBrightnessPalette(bool s);
	bool GetBrightnessPalette() const;

	void SetActivePaletteIndex(int n);
	int GetActivePaletteIndex() const;

	static void UpdateAll();

protected:

	virtual int GetPaletteId() const = 0;
	virtual void SetPaletteId(int n) = 0;

	int mOldPaletteId;
	const bool mWithBrightness;

	static iLookupArray<iggFramePaletteSelectionBase*>& List();
};

#endif  // IGGFRAMEPALETTESELECTIONBASE_H

