/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggframeobjectcontrols.h"


#include "istring.h"
#include "iviewsubject.h"

#include "iggframeposition.h"
#include "iggframepushcontrols.h"
#include "iggwidgetpropertycontrolslider.h"
#include "iggwidgetpropertycontroltrackball.h"
#include "iggwidgetotherbutton.h"


using namespace iParameter;


iggFrameObjectControls::iggFrameObjectControls(const iString &name, const iggPropertyHandle& posph, const iggPropertyHandle& dirph, const iggPropertyHandle& sizph, iggFrame *parent) : iggFrame(parent,2)
{
	this->Define(name,posph,dirph,&sizph,parent);
}


iggFrameObjectControls::iggFrameObjectControls(const iString &name, const iggPropertyHandle& posph, const iggPropertyHandle& dirph, iggFrame *parent) : iggFrame(parent,2)
{
	this->Define(name,posph,dirph,0,parent);
}


void iggFrameObjectControls::Define(const iString &name, const iggPropertyHandle& posph, const iggPropertyHandle& dirph, const iggPropertyHandle* sizph, iggFrame *parent)
{
	iString ws;

	if(name.IsEmpty()) ws = "Position"; else ws = name+" position";
	mOP = new iggFramePosition(ws,posph,this);

	if(name.IsEmpty()) ws = "Direction"; else ws = name+" direction";
	mOD = new iggFrame(ws,this);
	mOD->AddLine(new iggWidgetPropertyControlTrackBall(false,dirph,mOD));
	this->AddLine(mOP,mOD);

	if(sizph != 0)
	{
		if(name.IsEmpty()) ws = "Size"; else ws = name+" size";
		mOS = new iggFrame(ws,this);
		iggWidgetPropertyControlSizeSlider *s = new iggWidgetPropertyControlSizeSlider(1.0e-4,5,"",*sizph,mOS);
		mOS->AddLine(s);
	}
	else
	{
		mOS = 0;
	}

	mPO = new iggFramePushControls(mOP,"Push "+name.Lower(),posph,dirph,this);
	this->AddLine(mOS,mPO);
	this->AddSpace(5);

	this->SetColStretch(0,5);
}


void iggFrameObjectControls::EnablePosition(bool s)
{
	mOP->Enable(s);
}


void iggFrameObjectControls::EnableDirection(bool s)
{
	mOD->Enable(s);
	mPO->Enable(s);
}


void iggFrameObjectControls::EnableSize(bool s)
{
	if(mOS != 0) mOS->Enable(s);
}

#endif
