/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Some miscenallous widgets
//

#ifndef IGGWIDGETMISC_H
#define IGGWIDGETMISC_H


#include "iggwidget.h"


class ibgWidgetButtonSubject;
class ibgWidgetDisplayAreaSubject;
class ibgWidgetEntrySubject;
class ibgWidgetMultiImageDisplayAreaSubject;
class ibgWidgetRadioBoxSubject;
class ibgWidgetComboBoxSubject;
class ibgWidgetSpinBoxSubject;


class iggWidgetImageFlipper : public iggWidget
{

public:

	iggWidgetImageFlipper(iggFrame *parent);

	//
	//  Manual operation
	//
	void Advance();
	void Reset();

	//
	//  Automatic operation
	//
	void Start();
	void Abort();

protected:

	virtual void UpdateWidgetBody();
	virtual void OnVoid1Body();

	ibgWidgetMultiImageDisplayAreaSubject *mSubject;
};


class iggWidgetLogoFlipper : public iggWidgetImageFlipper
{

public:

	iggWidgetLogoFlipper(iggFrame *parent);
};


class iggWidgetNumberLabel : public iggWidget
{

public:

	iggWidgetNumberLabel(iggFrame *parent, int alignment = 0);

	void SetNumber(double v, const char *format = "%lg");
	void SetNumber(int v, const char *format = "%d");
	void SetAlignment(int s);

protected:

	virtual void UpdateWidgetBody();

	ibgWidgetDisplayAreaSubject *mSubject;
};


class iggWidgetTextLabel : public iggWidget
{

public:

	iggWidgetTextLabel(const iString &text, iggFrame *parent, int alignment = 0);

	void SetText(const iString &text);
	void SetAlignment(int s);

protected:

	virtual void UpdateWidgetBody();

	ibgWidgetDisplayAreaSubject *mSubject;
};


class iggWidgetSimpleItemizedSelectionBox : public iggWidget
{

public:

	iggWidgetSimpleItemizedSelectionBox(iggFrame *parent);

	//
	//  Decorator functions
	//
	virtual void InsertItem(const iString &text, int index = -1) = 0;
	virtual void SetItem(const iString &text, int index, bool vis) = 0;
	virtual void RemoveItem(int index) = 0;
	virtual int Count() const = 0;

	virtual int GetValue() const = 0;
};


class iggWidgetSimpleRadioBox : public iggWidgetSimpleItemizedSelectionBox
{

public:

	iggWidgetSimpleRadioBox(int cols, const iString &text, iggFrame *parent);

	virtual void InsertItem(const iString &text, int index = -1);
	virtual void SetItem(const iString &text, int index, bool vis);
	virtual void RemoveItem(int index);
	virtual int Count() const;

	virtual int GetValue() const;
	void SetValue(int v);

protected:

	virtual void UpdateWidgetBody();

	ibgWidgetRadioBoxSubject *mSubject;
};


class iggWidgetSimpleComboBox : public iggWidgetSimpleItemizedSelectionBox
{

public:

	iggWidgetSimpleComboBox(const iString &text, iggFrame *parent);

	virtual void InsertItem(const iString &text, int index = -1);
	virtual void SetItem(const iString &text, int index, bool vis);
	virtual void RemoveItem(int index);
	virtual int Count() const;

	virtual int GetValue() const;
	void SetValue(int v);

protected:

	virtual void UpdateWidgetBody();

	ibgWidgetComboBoxSubject *mSubject;
};


class iggWidgetSimpleSpinBox : public iggWidget
{

public:

	iggWidgetSimpleSpinBox(int min, int max, int step, const iString &text, iggFrame *parent);

	void SetValue(int v);
	int GetValue() const;

	void SetRange(int min, int max);
	void SetStep(int step);
	int Count();

protected:

	virtual void UpdateWidgetBody();

	ibgWidgetSpinBoxSubject *mSubject;
};


class iggWidgetSimpleCheckBox : public iggWidget
{

public:

	iggWidgetSimpleCheckBox(const iString &title, iggFrame *parent);

	void SetChecked(bool s);
	bool IsChecked() const;

protected:

	virtual void UpdateWidgetBody();
	virtual void OnVoid1Body();
	virtual void OnChecked(bool s) = 0;

	ibgWidgetButtonSubject *mSubject;
};

#endif  //  IGGWIDGETMISC_H
