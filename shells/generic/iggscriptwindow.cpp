/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggscriptwindow.h"


#include "ierror.h"
#include "ifile.h"
#include "iscript.h"
#include "isystem.h"
#include "iviewmodule.h"

#include "iggdialog.h"
#include "iggdialoghelp.h"
#include "iggframetopparent.h"
#include "iggimagefactory.h"
#include "iggmainwindow.h"
#include "iggmenuhelper.h"
#include "iggshell.h"
#include "iggwidgetmisc.h"
#include "iggwidgetotherbutton.h"
#include "iggwidgettext.h"

#include "ibgmenuwindowsubject.h"
#include "ibgwidgetbuttonsubject.h"
#include "ibgwidgetentrysubject.h"
#include "ibgwidgettexteditorsubject.h"

#include "iggsubjectfactory.h"

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"


using namespace iParameter;


//
//  Helper class
//
class iggWidgetScriptEditor : public iggWidgetTextEditor
{

public:

	iggWidgetScriptEditor(iggScriptWindow *w, iggFrame *parent) : iggWidgetTextEditor(0U,parent)
	{
		mWindow = w; 
		mBreakPointColor = iColor(210,180,120);

		//
		//  Search capabilities
		//
		mFindLoc[0] = mFindLoc[1] = 0;
	}

	void SaveRestore(bool save)
	{
		mSubject->SetReadOnly(save);

		if(save)
		{
			mSavedState.Text = this->GetText();
			mSavedState.Modified = this->TextModified();
			mSubject->GetCursorPosition(mSavedState.CursorLocation[0],mSavedState.CursorLocation[1]);
		}
		else
		{
			if(mSavedState.Text != this->GetText())
			{
				this->SetText(mSavedState.Text);
				this->SetModified(mSavedState.Modified);
			}
			mSubject->SetCursorPosition(mSavedState.CursorLocation[0],mSavedState.CursorLocation[1]);
		}
	}

	void FindNext(const iString &text, bool cs, bool wo, bool forward)
	{
		if(!mSubject->Find(text,cs,wo,forward,mFindLoc[0],mFindLoc[1]))
		{
			mFindLoc[0] = mFindLoc[1] = 0;
		}
		else mFindLoc[1]++;
	}

	virtual void SetText(const iString &text)
	{
		this->iggWidgetTextEditor::SetText(text);
		this->OnTextChanged();
	}

	void MarkLine(int line)
	{
		if(line > -1)
		{
			mSubject->Select(line,0,line,mSubject->GetLine(line).Length());
		}
		else
		{
            this->ClearSelection();
		}
	}

	void ClearSelection()
	{
		mSubject->Select(-1,0,0,0);
	}

	void SetBreakPoint(bool s)
	{
		int line, index;
		mSubject->GetCursorPosition(line,index);
		iString ws = mSubject->GetLine(line);
		ws.ReduceWhiteSpace();

		bool mod = this->TextModified();
		if(s && ws.Length()>0 && ws[0]!='#') mSubject->SetLineColor(line,mBreakPointColor); else mSubject->UnSetLineColor(line);
		this->SetModified(mod);

		this->OnCursorPositionChanged(line,index);
	}

	bool IsBreakPoint(int line = -1) const
	{
		if(line < 0)
		{
			int index;
			mSubject->GetCursorPosition(line,index);
		}
		return mSubject->GetLineColor(line) == mBreakPointColor;
	}

protected:

	virtual void OnCursorPositionChanged(int line, int index)
	{
	}

	struct State
	{
		iString Text;
		bool Modified;
		int CursorLocation[2];
		State(){ Modified = false; CursorLocation[0] = CursorLocation[1] = 0; }
	};

	bool mMarkBefore;
	State mSavedState;
	iColor mBreakPointColor;
	iggScriptWindow *mWindow;
	int mFindLoc[2];
};


namespace iggScriptWindow_Private
{
	//
	//  Menu ids
	//
	namespace Menu
	{
		namespace File
		{
			const int New = 1;
			const int NewTemplate = 2;
			const int Open = 3;
			const int Save = 4;
			const int SaveAs = 5;
			const int Close = 6;
			const int Max = 100;
		};

		namespace Edit
		{
			const int Undo = 101;
			const int Redo = 102;
			const int Cut = 103;
			const int Copy = 104;
			const int Paste = 105;
			const int Find = 106;
			const int FontUp = 107;
			const int FontDn = 108;
			const int Max = 200;
		};

		namespace Run
		{
			const int Debug = 201;
			const int Exec = 202;
			const int Step = 203;
			const int Stop = 204;
			const int ToggleBreakPoint = 205;
			const int Max = 300;
		};

		namespace History
		{
			const int Show = 301;
			const int Clear = 302;
			const int Max = 400;
		};

		namespace Output
		{
			const int Clear = 401;
			const int Max = 500;
		};

		namespace Help
		{
			const int Script = 501;
			const int Max = 600;
		};
	};

	namespace RunState
	{
		const int Exec = 1;
		const int Stop = 2;
		const int Wait = 3;
		const int Step = 4;
	};

	//
	//  Find dialog and its classes
	//
	class FindText : public iggWidget
	{

	public:

		FindText(iggFrame *parent) : iggWidget(parent)
		{
			mSubject = iggSubjectFactory::CreateWidgetEntrySubject(this,false,0,"Find:");
			this->SetBaloonHelp("Type a string to find");
		}

		const iString GetText() const
		{
			return mSubject->GetText();
		}

	protected:

		virtual void UpdateWidgetBody()
		{
		}

		ibgWidgetEntrySubject *mSubject;
	};


	class FindCheckBox : public iggWidgetSimpleCheckBox
	{

	public:

		FindCheckBox(const iString &title, iggFrame *parent) : iggWidgetSimpleCheckBox(title,parent)
		{
			mNeedsBaloonHelp = false;
		}

	protected:

		virtual void OnChecked(bool)
		{
		}
	};


	class FindButton : public iggWidgetSimpleButton
	{

	public:

		FindButton(iggWidgetScriptEditor *ed, FindText *t, FindCheckBox *cs, FindCheckBox *wo, FindCheckBox *bs, iggFrame *parent) : iggWidgetSimpleButton("Next",parent)
		{
			mEditor = ed;
			mText = t;
			mCaseCheckBox = cs;
			mWordCheckBox = wo;
			mBackCheckBox = bs;
			this->SetBaloonHelp("Find the next occurence of the highlighted string");
		}

	protected:

		virtual void Execute()
		{
			mEditor->FindNext(mText->GetText(),mCaseCheckBox->IsChecked(),mWordCheckBox->IsChecked(),!mBackCheckBox->IsChecked());
		}

		iggWidgetScriptEditor *mEditor;
		FindText *mText;
		FindCheckBox *mCaseCheckBox, *mWordCheckBox, *mBackCheckBox;
	};


	class FindDialog : public iggDialog
	{
		
	public:

		FindDialog(iggWidgetScriptEditor *ed, iggMenuWindow *mw) : iggDialog(mw,DialogFlag::Modal,iggImageFactory::FindIcon("searchfind.png"),"Find",0,2)
		{
			FindText *t = new FindText(mFrame);
			FindCheckBox *cs = new FindCheckBox("Case sensitive",mFrame);
			FindCheckBox *wo = new FindCheckBox("Whole word only",mFrame);
			FindCheckBox *bs = new FindCheckBox("Search backwards",mFrame);
			mFrame->AddLine(t,new FindButton(ed,t,cs,wo,bs,mFrame));
			mFrame->AddLine(cs);
			mFrame->AddLine(wo);
			mFrame->AddLine(bs);

			this->CompleteConstruction();
		}

	protected:

		virtual void CompleteConstructionBody()
		{
		}
	};


	//
	//  Single line command editor
	//
	class CommandLineEditor : public iggWidget
	{

	public:

		CommandLineEditor(iScript *script, iggFrame *parent) : iggWidget(parent)
		{
			mScript = script;
			mCurHistoryLine = -1;

			mSubject = iggSubjectFactory::CreateWidgetEntrySubject(this,false,0,"Debugger command line");
			this->SetBaloonHelp("Type a script command");
		}

	protected:

		virtual void UpdateWidgetBody()
		{
		}

		virtual void OnVoid1Body()
		{
			mHistory.Add(mSubject->GetText());

			if(mScript->ExecuteSegment(mSubject->GetText()))
			{
				mSubject->SetText("");
				mCurHistoryLine = -1;
			}
		}

		virtual void OnBool1Body(bool up)
		{
			if(up)
			{
				if(mCurHistoryLine == -1)
				{
					mCurLine = mSubject->GetText();
				}
				if(mCurHistoryLine < mHistory.MaxIndex())
				{
					mCurHistoryLine++;
					mSubject->SetText(mHistory[mHistory.MaxIndex()-mCurHistoryLine]);
				}
			}
			else
			{
				if(mCurHistoryLine >= 0)
				{
					mCurHistoryLine--;
					if(mCurHistoryLine == -1)
					{
						mSubject->SetText(mCurLine);
					}
					else
					{
						mSubject->SetText(mHistory[mHistory.MaxIndex()-mCurHistoryLine]);
					}
				}
			}
		}

		int mCurHistoryLine;
		iString mCurLine;
		iArray<iString> mHistory;
		ibgWidgetEntrySubject *mSubject;
		iScript *mScript;
	};


	class MenuHelper : public iggMenuHelperBase
	{

	public:

		MenuHelper(iggScriptWindow *w, int type)
		{
			mWindow = w;
			mType = type;
		}

		virtual iggMenuHelperBase* Clone() const
		{
			iggMenuHelperBase *ret = new MenuHelper(mWindow,mType); IERROR_CHECK_MEMORY(ret);
			return ret;
		}

		virtual bool IsSet() const
		{
			int rs = mWindow->GetRunState();

			switch(mType)
			{
			case RunState::Exec:
				{
					return (rs==0 || rs==RunState::Wait);
				}
			case RunState::Step:
				{
					return (rs == RunState::Wait) && mWindow->IsDebugging();
				}
			case RunState::Stop:
				{
					return (rs==RunState::Exec || rs==RunState::Wait) && mWindow->IsDebugging();
				}
			default:
				{
					IBUG_FATAL("Invalid menu item");
					return false;
				}
			}
		}

	protected:

		iggScriptWindow *mWindow;
		int mType;
	};


	class ScriptIO : public iScriptIOHelper
	{

	public:

		ScriptIO(iggWidgetTextBrowser *output)
		{
			mOutput = output;
		}

		virtual iString Read()
		{
			static const iString none;
			return none;
		}

		virtual int Write(const iString &str, bool err)
		{
			static const iColor cerr(255,0,0);
			static const iColor cout(0,0,0);

			mBuffer += str;
			if(str[str.Length()-1] == '\n')
			{
				mOutput->AppendTextLine(mBuffer,err?cerr:cout);
				mBuffer.Clear();
			}

			return str.Length();
		}

	protected:

		iString mBuffer;
		iggWidgetTextBrowser *mOutput;
	};


	class Observer : public iScriptObserver
	{

	public:

		Observer(iggScriptWindow *w)
		{
			mWindow = w;
		}

	protected:

		virtual void OnStartBody()
		{
		}
		
		virtual void OnStopBody(bool aborted)
		{
		}

		virtual void OnBeginLineBody(int line, const iString &file)
		{
			mWindow->DebugLine(line,file);
		}

		virtual void OnEndLineBody(int line, const iString &file)
		{
		}

		virtual bool IsTerminatedBody()
		{
			return (mWindow->GetRunState() == RunState::Stop);
		}

		iggScriptWindow *mWindow;
	};
};


using namespace iggScriptWindow_Private;


//
//  Main class
//
iggScriptWindow::iggScriptWindow(iggMainWindow *parent) : iggMenuWindow(parent)
{
	//
	//  Create MenuSubject
	//
	this->AttachSubject(iggSubjectFactory::CreateMenuWindowSubject(this,iggImageFactory::FindIcon("debug.png"),"Ionization Front Interactive Tool"),1);

	mScript = 0;

	mFirstShow =  mIsDebugging = true;
	mFindDialog = 0;
	mEditor = 0;

	mRunState = 0;
	mCompleteConstructionCalled = false;

	if(this->ImmediateConstruction()) this->CompleteConstruction();
}


void iggScriptWindow::CompleteConstructionBody()
{
	//
	//  Top frame
	//
	mGlobalFrame->SetPadding(true);

	iggFrameSplit *split = new iggFrameSplit(mGlobalFrame,false);

	iggFrame *tmp = new iggFrame(split);
	mEditor = new iggWidgetScriptEditor(this,tmp); IERROR_CHECK_MEMORY(mEditor);
	tmp->AddLine(mEditor);

	tmp = new iggFrame(split);
	mOutput = new iggWidgetTextBrowser(false,true,tmp,false); IERROR_CHECK_MEMORY(mOutput);
	tmp->AddLine(mOutput);

	mGlobalFrame->AddLine(split);

	mScript = iScript::New(this->GetShell());

	if(mScript != 0)
	{
		ScriptIO *ioh = new ScriptIO(mOutput); IERROR_CHECK_MEMORY(ioh);
		mScript->InstallIOHelper(ioh);

		if(mIsDebugging)
		{
			Observer *obs = new Observer(this); IERROR_CHECK_MEMORY(obs);
			mScript->InstallObserver(obs);
		}
	}
	mOutput->SetText("Using scripting language: "+mScript->Name());


	mDebugFrame = new iggFrame(mGlobalFrame);
	mDebugFrame->AddLine(new CommandLineEditor(mScript,mDebugFrame));
	mGlobalFrame->AddLine(mDebugFrame);
	mDebugFrame->Show(false);

	//
	//  Menus
	//
	this->BuildMenus();

	//
	//  Helper dialogs
	//
	mFindDialog = new FindDialog(mEditor,this); IERROR_CHECK_MEMORY(mFindDialog);

	//
	//  Resize window
	//
	int wg[4];
	this->GetSubject()->GetWindowGeometry(wg);
	wg[2] = 600;
	wg[3] = 600;
	this->GetSubject()->SetWindowGeometry(wg);
}


iggScriptWindow::~iggScriptWindow()
{
	if(mFindDialog != 0) delete mFindDialog;
	if(mScript != 0) mScript->Delete();
}


void iggScriptWindow::Show(bool s)
{
	if(s && mFirstShow)
	{
		mFirstShow = false;

		if(!mCompleteConstructionCalled)
		{
			this->CompleteConstruction();
		}

		int wg1[4], wg2[4];
		this->GetMainWindow()->GetSubject()->GetWindowGeometry(wg1);
		this->GetSubject()->GetWindowGeometry(wg2);
		wg2[0] = wg1[0] + 50;
		wg2[1] = wg1[1] + 100;
		this->GetSubject()->SetWindowGeometry(wg2);

		if(mScript == 0)
		{
			this->GetShell()->PopupWindow("No script is currently available",MessageType::Warning);
			return;
		}
	}


	if(!s) this->CheckModified();
	this->iggMenuWindow::Show(s);
}


bool iggScriptWindow::CanBeClosed()
{
	this->CheckModified();
	return true;
}


void iggScriptWindow::UpdateContents()
{
	if(!mCompleteConstructionCalled) return;

	mGlobalFrame->UpdateWidget();
}


void iggScriptWindow::CheckModified()
{
	if(mEditor!=0 && mEditor->TextModified())
	{
		switch(this->GetShell()->PopupWindow(this,"The current script is not saved.",MessageType::Warning,"Save","Ignore")) 
		{
		case 0: // The user clicked the Save button or pressed Enter
			{ 
				this->SaveToFile(mCurrentFileName);
				break;
			}
		case 1: // The user clicked the Ignore or pressed Escape
			{
				mEditor->SetModified(false);
				break;
			}
		}
	}
}


void iggScriptWindow::SetContents(const iString &text)
{
	this->CheckModified();

	mEditor->SetText(text);
	mEditor->SetModified(false);
}


void iggScriptWindow::LoadFromFile(const iString &filename)
{
	iString fn;
	iString ext;
	if(mScript != 0)
	{
		ext = "." + mScript->GetScriptFileSuffix();
	}
	else ext = ".*";

	if(filename.IsEmpty())
	{
		if(mCurrentFileName.IsEmpty())
		{
			fn = this->GetFileName("Open script file",this->GetShell()->GetEnvironment(Environment::Script),"Script file (*"+ext+")");
		}
		else
		{
			fn = this->GetFileName("Open script file",mCurrentFileName,"Script file (*"+ext+")");
		}
	}
	else
	{
		fn = filename;
	}

	if(fn.IsEmpty()) return; 
	if(!fn.EndsWith(ext)) fn += ext;

	if(this->LoadFromFileBody(fn,false))
	{
		mCurrentFileName = fn;
	}
}


bool iggScriptWindow::LoadFromFileBody(const iString &fn, bool silent)
{
	iFile f(fn);
	if(!f.Open(iFile::_Read,iFile::_Text))
	{
		if(!silent) this->GetShell()->PopupWindow(this,"Failed to open a script file.",MessageType::Error);
		return false;
	}

	iString text, line;
	while(f.ReadLine(line))
	{
		text += line +"\n";
	}
	f.Close();

	this->SetContents(text);

	return true;
}


void iggScriptWindow::SaveToFile(const iString &filename)
{
	iString fn;
	iString ext;
	if(mScript != 0)
	{
		ext = "." + mScript->GetScriptFileSuffix();
	}
	else ext = ".*";

	if(filename.IsEmpty())
	{
		if(mCurrentFileName.IsEmpty())
		{
			fn = this->GetFileName("Save script file",this->GetShell()->GetEnvironment(Environment::Script),"Script file (*"+ext+")",false);
		}
		else
		{
			fn = this->GetFileName("Save script file",mCurrentFileName,"Script file (*"+ext+")",false);
		}
	}
	else
	{
		fn = filename;
	}

	if(fn.IsEmpty()) return; 
	if(!fn.EndsWith(ext)) fn += ext;

	iFile f(fn);
	if(!f.Open(iFile::_Write,iFile::_Text))
	{
		this->GetShell()->PopupWindow(this,"Failed to open a script file.",MessageType::Error);
		return;
	}

	iString line;
	int i, n = mEditor->GetNumberOfLines();
	for(i=0; i<n; i++)
	{
		line = mEditor->GetLine(i);
		if(!line.IsEmpty() && line.IsWhiteSpace(line.Length()-1)) line = line.Part(0,line.Length()-1); // weird Qt bug
		if(i<n-1 || !line.IsEmpty()) //  Qt bug
		{
			if(!f.WriteLine(line))
			{
				this->GetShell()->PopupWindow(this,"Error in writing a script file.",MessageType::Error);
				f.Close();
				return;
			}
		}
	}
	f.Close();

	mEditor->SetModified(false);
	mCurrentFileName = fn;
}


void iggScriptWindow::BuildMenus()
{
	//
	//  1. File menu
	//
	this->GetSubject()->BeginMenu("&File",false);
	{
		this->GetSubject()->AddMenuItem(Menu::File::New,"&New",iggImageFactory::FindIcon("filenew.png"),"Ctrl+N",false,false);
		this->GetSubject()->AddMenuItem(Menu::File::NewTemplate,"New &Template",0,"Ctrl+T",false,false);
		this->GetSubject()->AddMenuItem(Menu::File::Open,"&Open...",iggImageFactory::FindIcon("fileopen.png"),"Ctrl+O",false,false);
		this->GetSubject()->AddMenuItem(Menu::File::Save,"&Save",iggImageFactory::FindIcon("filesave.png"),"Ctrl+S",false,false);
		this->GetSubject()->AddMenuItem(Menu::File::SaveAs,"Save &As...",0,"",false,false);
		this->GetSubject()->AddMenuSeparator();
		this->GetSubject()->AddMenuItem(Menu::File::Close,"&Close",0,"Ctrl+C",false,false);
	}
	this->GetSubject()->EndMenu();

	//
	//  2. Edit menu
	//
	this->GetSubject()->BeginMenu("&Edit",false);
	{
		if(mEditor->HasEditingFunction(TextEditorFunction::Undo)) this->GetSubject()->AddMenuItem(Menu::Edit::Undo,"&Undo",iggImageFactory::FindIcon("undo.png"),"Ctrl+U",false,false);
		if(mEditor->HasEditingFunction(TextEditorFunction::Redo)) this->GetSubject()->AddMenuItem(Menu::Edit::Redo,"&Redo",iggImageFactory::FindIcon("redo.png"),"Ctrl+R",false,false);
		if(mEditor->HasEditingFunction(TextEditorFunction::Cut)) this->GetSubject()->AddMenuItem(Menu::Edit::Cut,"Cu&t",iggImageFactory::FindIcon("editcut.png"),"Ctrl+X",false,false);
		if(mEditor->HasEditingFunction(TextEditorFunction::Copy)) this->GetSubject()->AddMenuItem(Menu::Edit::Copy,"&Copy",iggImageFactory::FindIcon("editcopy.png"),"Ctrl+C",false,false);
		if(mEditor->HasEditingFunction(TextEditorFunction::Paste)) this->GetSubject()->AddMenuItem(Menu::Edit::Paste,"&Paste",iggImageFactory::FindIcon("editpaste.png"),"Ctrl+V",false,false);
		this->GetSubject()->AddMenuSeparator();
		this->GetSubject()->AddMenuItem(Menu::Edit::Find,"&Find",iggImageFactory::FindIcon("searchfind.png"),"Ctrl+F",false,false);
	}
	this->GetSubject()->EndMenu();

	//
	//  3. Run menu
	//
	MenuHelper mhe(this,RunState::Exec);
	MenuHelper mhs(this,RunState::Step);
	MenuHelper mht(this,RunState::Stop);
	if(mScript != 0)
	{
		this->GetSubject()->BeginMenu("&Run",false);
		{
			if(mScript->IsTraceable()) this->GetSubject()->AddMenuItem(Menu::Run::Debug,"&Debug script",0,"",true,mIsDebugging);
			this->GetSubject()->AddMenuItem(Menu::Run::Exec,"&Run script",iggImageFactory::FindIcon("script_run.png"),"F5",false,false,&mhe);
			if(mScript->IsTraceable())
			{
				this->GetSubject()->AddMenuItem(Menu::Run::Step,"Step one &line",iggImageFactory::FindIcon("script_step.png"),"F10",false,false,&mhs);
				this->GetSubject()->AddMenuItem(Menu::Run::Stop,"&Stop script",iggImageFactory::FindIcon("script_stop.png"),"Esc",false,false,&mht);
				this->GetSubject()->AddMenuSeparator();
				this->GetSubject()->AddMenuItem(Menu::Run::ToggleBreakPoint,"Toggle &breakpoint",iggImageFactory::FindIcon("script_break.png"),"Ctrl+B",false,false);
			}
		}
		this->GetSubject()->EndMenu();
	}

	//
	//  4. History menu
	//
	this->GetSubject()->BeginMenu("H&istory",false);
	{
		this->GetSubject()->AddMenuItem(Menu::History::Show,"&Show history",0,"",false,false);
		this->GetSubject()->AddMenuItem(Menu::History::Clear,"&Clear history",0,"",false,false);
	}
	this->GetSubject()->EndMenu();

	//
	//  5. History menu
	//
	this->GetSubject()->BeginMenu("&Output",false);
	{
		this->GetSubject()->AddMenuItem(Menu::Output::Clear,"&Clear output window",0,"",false,false);
	}
	this->GetSubject()->EndMenu();

	//
	//  6. Help menu
	//
	this->GetSubject()->BeginMenu("&Help",false);
	{
		this->GetSubject()->AddMenuItem(Menu::Help::Script,"&Using script",0,"",false,false);
	}
	this->GetSubject()->EndMenu();

	//
	//  Build a tool bar
	//
	this->GetSubject()->AddToolBarButton(Menu::File::New,"Create a new script",iggImageFactory::FindIcon("filenew.png"),false);
	this->GetSubject()->AddToolBarButton(Menu::File::Open,"Open an existing script from a file",iggImageFactory::FindIcon("fileopen.png"),false);
	this->GetSubject()->AddToolBarButton(Menu::File::Save,"Save the current script into a file",iggImageFactory::FindIcon("filesave.png"),false);
	this->GetSubject()->AddToolBarSeparator();
	if(mEditor->HasEditingFunction(TextEditorFunction::Undo)) this->GetSubject()->AddToolBarButton(Menu::Edit::Undo,"Undo",iggImageFactory::FindIcon("undo.png"),false);
	if(mEditor->HasEditingFunction(TextEditorFunction::Redo)) this->GetSubject()->AddToolBarButton(Menu::Edit::Redo,"Redo",iggImageFactory::FindIcon("redo.png"),false);
	if(mEditor->HasEditingFunction(TextEditorFunction::Cut)) this->GetSubject()->AddToolBarButton(Menu::Edit::Cut,"Cut the current selection",iggImageFactory::FindIcon("editcut.png"),false);
	if(mEditor->HasEditingFunction(TextEditorFunction::Copy)) this->GetSubject()->AddToolBarButton(Menu::Edit::Copy,"Copy the current selection to the clipboard",iggImageFactory::FindIcon("editcopy.png"),false);
	if(mEditor->HasEditingFunction(TextEditorFunction::Paste)) this->GetSubject()->AddToolBarButton(Menu::Edit::Paste,"Paste the contents of the clipboard",iggImageFactory::FindIcon("editpaste.png"),false);
	this->GetSubject()->AddToolBarButton(Menu::Edit::Find,"Search the script text",iggImageFactory::FindIcon("searchfind.png"),false);
	this->GetSubject()->AddToolBarButton(Menu::Edit::FontUp,"Increase font size",iggImageFactory::FindIcon("fontup.png"),false);
	this->GetSubject()->AddToolBarButton(Menu::Edit::FontDn,"Decrease font size",iggImageFactory::FindIcon("fontdn.png"),false);

	if(mScript != 0)
	{
		this->GetSubject()->AddToolBarSeparator();
		this->GetSubject()->AddToolBarButton(Menu::Run::Exec,"Run script",iggImageFactory::FindIcon("script_run.png"),false,&mhe);
		if(mScript->IsTraceable())
		{
			this->GetSubject()->AddToolBarButton(Menu::Run::Step,"Step one line",iggImageFactory::FindIcon("script_step.png"),false,&mhs);
			this->GetSubject()->AddToolBarButton(Menu::Run::Stop,"Stop script",iggImageFactory::FindIcon("script_stop.png"),false,&mht);
			this->GetSubject()->AddToolBarSeparator();
			this->GetSubject()->AddToolBarButton(Menu::Run::ToggleBreakPoint,"Toggle a breakpoint",iggImageFactory::FindIcon("script_break.png"),false);
		}
	}
}


void iggScriptWindow::OnMenuBody(int id, bool on)
{
	static const iString none;
	iString fname;

	if(id < 0)
	{
		IBUG_WARN("Invalid menu item id.");
	}

	if(id <= Menu::File::Max)
	{
		switch(id)
		{
		case Menu::File::New:
			{
				this->SetContents(none);
			 	mCurrentFileName.Clear();
				break;
			}
		case Menu::File::NewTemplate:
			{
				if(mScript != 0)
				{
					this->SetContents(mScript->CreateTemplate());
				}
				else
				{
					this->SetContents(none);
				}
			 	mCurrentFileName.Clear();
				break;
			}
		case Menu::File::Open:
			{
				this->LoadFromFile(none);
				break;
			}
		case Menu::File::Save:
			{
				this->SaveToFile(mCurrentFileName);
				break;
			}
		case Menu::File::SaveAs:
			{
				this->SaveToFile(none);
				break;
			}
		case Menu::File::Close:
			{
				this->CheckModified();
				this->Show(false);
				break;
			}
		default:
			{
				IBUG_WARN("Invalid menu item id.");
			}
		}
		return;
	}

	if(id <= Menu::Edit::Max)
	{
		switch(id)
		{
		case Menu::Edit::Undo:
		case Menu::Edit::Redo:
		case Menu::Edit::Cut:
		case Menu::Edit::Copy:
		case Menu::Edit::Paste:
			{
				mEditor->UseEditingFunction(id-Menu::Edit::Undo+TextEditorFunction::Undo);
				break;
			}
		case Menu::Edit::Find:
			{
				mFindDialog->Show(true);	
				break;
			}
		case Menu::Edit::FontUp:
			{
				mEditor->AdjustFontSize(1);
				mOutput->AdjustFontSize(1);
				break;
			}
		case Menu::Edit::FontDn:
			{
				mEditor->AdjustFontSize(-1);	
				mOutput->AdjustFontSize(-1);	
				break;
			}
		default:
			{
				IBUG_WARN("Invalid menu item id.");
			}
		}
		return;
	}

	if(id<=Menu::Run::Max && mScript!=0)
	{
		switch(id)
		{
		case Menu::Run::Debug:
			{
				mIsDebugging = on;
				if(mIsDebugging)
				{
					Observer *obs = new Observer(this); IERROR_CHECK_MEMORY(obs);
					mScript->InstallObserver(obs);
				}
				else
				{
					mScript->InstallObserver(0);
				}

				this->UpdateMenus();

				break;
			}
		case Menu::Run::Exec:
			{
				if(mRunState == 0)
				{
					mDebuggedFile.Clear();
	
					mRunState = RunState::Exec;
					this->UpdateMenus();
					this->GetMainWindow()->ProcessEvents();
					mEditor->SaveRestore(true);
					mDebugFrame->Show(true);

					mScript->ExecuteSegment(mEditor->GetText());

					mRunState = 0;
					this->UpdateMenus();
					mEditor->ClearSelection();
					mEditor->SaveRestore(false);
					mDebugFrame->Show(false);

					this->GetMainWindow()->UpdateAll();

					if(mLastExecutedSegment != mEditor->GetText())
					{
						mLastExecutedSegment = mEditor->GetText();
						mHistory += mLastExecutedSegment;
					}
				}
				else
				{
					mRunState = RunState::Exec;
				}

				break;
			}
		case Menu::Run::Step:
			{
				mRunState = RunState::Step;
				break;
			}
		case Menu::Run::Stop:
			{
				mRunState = RunState::Stop;
				break;
			}
		case Menu::Run::ToggleBreakPoint:
			{
				mEditor->SetBreakPoint(!mEditor->IsBreakPoint());
				break;
			}
		default:
			{
				IBUG_WARN("Invalid menu item id.");
			}
		}
		return;
	}

	if(id <= Menu::History::Max)
	{
		switch(id)
		{
		case Menu::History::Show:
			{
				this->SetContents(mHistory);
				break;
			}
		case Menu::History::Clear:
			{
				mHistory.Clear();
				mLastExecutedSegment.Clear();
				break;
			}
		default:
			{
				IBUG_WARN("Invalid menu item id.");
			}
		}
		return;
	}

	if(id <= Menu::Output::Max)
	{
		switch(id)
		{
		case Menu::Output::Clear:
			{
				mOutput->Clear();
				mOutput->SetText("Using scripting language: "+mScript->Name());
				break;
			}
		default:
			{
				IBUG_WARN("Invalid menu item id.");
			}
		}
		return;
	}

	if(id <= Menu::Help::Max)
	{
		switch(id)
		{
		case Menu::Help::Script:
			{
				mMainWindow->GetDialogHelp()->Open("sr.gg.dsw");	
				break;
			}
		default:
			{
				IBUG_WARN("Invalid menu item id.");
			}
		}
		return;
	}

	IBUG_WARN("Invalid menu item id.");
}


bool iggScriptWindow::ImmediateConstruction() const
{
	return this->GetShell()->CheckCondition(iParameter::Condition::DisableDelayedInitialization);
}


void iggScriptWindow::CompleteConstruction()
{
	if(mCompleteConstructionCalled) return;

	this->CompleteConstructionBody();
	mCompleteConstructionCalled = true;
#ifdef I_DEBUG
	iOutput::DisplayDebugMessage(iString("Completing delayed construction for element #")+iString::FromNumber(this->GetId()));
#endif
}


void iggScriptWindow::DebugLine(int line, const iString& file)
{
	//
	//  Need to change file?
	//
	if(mDebuggedFile.IsEmpty())
	{
		mDebuggedFile = file;
	}
	if(file != mDebuggedFile)
	{
		//
		//  Debug just 1 file for now.
		//
		return;
	}

	mEditor->MarkLine(line);
	this->GetMainWindow()->ProcessEvents();
	
	if(mRunState==RunState::Exec && mEditor->IsBreakPoint(line))
	{
		mRunState = RunState::Wait;
		this->UpdateMenus();
	}

	while(mRunState == RunState::Wait)
	{
		iSystem::Sleep(100);
		this->GetMainWindow()->ProcessEvents();
	}

	switch(mRunState)
	{
	case RunState::Exec:
	case RunState::Stop:
		{
			return;
		}
	case RunState::Step:
		{
			mRunState = RunState::Wait;
			return;
		}
	}
}

#endif
