/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggdialogdataexplorer.h"


#include "idata.h"
#include "idatalimits.h"
#include "idatareader.h"
#include "idatasubject.h"
#include "ierror.h"
#include "ihistogram.h"
#include "ihistogrammaker.h"
#include "iviewmodule.h"

#include "iggframedatatypeselector.h"
#include "iggframedoublebutton.h"
#include "iggframehistogramcontrols.h"
#include "igghandle.h"
#include "iggimagefactory.h"
#include "iggmainwindow.h"
#include "iggshell.h"
#include "iggwidgetarea.h"
#include "iggwidgetmisc.h"
#include "iggwidgetotherbutton.h"
#include "iggwidgettext.h"

#include "ibgwidgetareasubject.h"
#include "ibgwidgetbuttonsubject.h"
#include "ibgwidgetcolorselectionsubject.h"
#include "ibgwidgetentrysubject.h"
#include "ibgwidgetselectionboxsubject.h"
#include "ibgwidgettexteditorsubject.h"

#include "iggsubjectfactory.h"

#include <limits.h>

//
//  templates
//
#include "iarray.tlh"


using namespace iParameter;


namespace iggDialogDataExplorer_Private
{
	//
	//  Controls the DataType of this page
	//
	class DataTypeHandle : public iggDataTypeHandle
	{

	public:

		DataTypeHandle(iggDialogDataExplorer *owner)
		{
			mOwner = owner;
			mIndex = 0;
		}

		virtual const iDataInfo& GetDataInfo() const
		{
			mDataInfo.Clear();

			int i;
			const iDataInfo &di(iDataInfo::Any());
			for(i=0; i<di.Size(); i++)
			{
				if(mOwner->GetShell()->GetViewModule()->GetReader()->IsThereData(di.Type(i)))
				{
					mDataInfo += di.Type(i);
				}
			}

			if(mIndex >= di.Size()) mIndex = di.Size() - 1;

			return mDataInfo;
		}

		virtual int GetActiveDataTypeIndex() const
		{
			return mIndex;
		}

	protected:

		virtual void OnActiveDataTypeChanged(int v)
		{
			if(v>=0 && v<mDataInfo.Size())
			{
				mIndex = v;
				mOwner->UpdateDialog();
			}
		}

		virtual void AfterFileLoadBody(const iDataInfo &info)
		{
		}

	private:

		mutable int mIndex;
		mutable iDataInfo mDataInfo;
		iggDialogDataExplorer *mOwner;
	};

	struct Variable
	{
		int Var;
		iString Name;
		iDataInfo Data;
		iViewModule *View;
		iHistogramMaker *Maker;
		iColor Color;
		float Opacity;
		Variable()
		{
			View = 0;
			Maker = 0;
			Opacity = 0.0f;
		}
	};

	//
	//  List of explored variables - does most of work
	//
	class ExploredVariablesList : public iggWidgetListView
	{

	public:

		ExploredVariablesList(iggDialogDataExplorer *de, iggFrame *parent) : iggWidgetListView(true,parent)
		{
			mDataExplorer = de;
			mMaxNameLength = -1;

			//
			//  Set colors for first levels like in a pseudo-raibow palette
			//
			mDefaultColor[0] = iColor(255,  0,  0);
			mDefaultColor[1] = iColor(  0,  0,255);
			mDefaultColor[2] = iColor(  0,255,  0);
			mDefaultColor[3] = iColor(255,255,  0);
			mDefaultColor[4] = iColor(255,  0,255);
			mDefaultColor[5] = iColor(  0,255,255);
		}

		~ExploredVariablesList()
		{
            while(mVariables.Size() > 0) this->RemoveVariable(0);
		}

		inline iggDialogDataExplorer* DataExplorer() const { return mDataExplorer; }
		inline const iArray<Variable>& GetVariables() const { return mVariables; }

		void AddVariable(int n)
		{
			iDataSubject *subject = this->GetShell()->GetViewModule()->GetReader()->GetSubject(mDataExplorer->GetActiveDataType()); IASSERT(subject);
			iDataLimits *lims = subject->GetLimits(); IASSERT(lims);

			if(n>=0 && n<lims->GetNumVars())
			{
				Variable v;

				v.Var = n;
				v.Name = lims->GetName(n);
				v.Data = lims->GetDataType();
				v.View = this->GetShell()->GetViewModule();

				//
				//  Check for duplicates
				//
				int i;
				for(i=0; i<mVariables.Size(); i++)
				{
					if(mVariables[i].Name==v.Name && mVariables[i].View==v.View && mVariables[i].Data.Type(0)==v.Data.Type(0))
					{
						if(this->GetShell()->PopupWindow(mDataExplorer,"This variables is already on the list!",MessageType::Information,"Cancel","Add anyway") == 0) return; else break;
					}
				}

				v.Maker = subject->GetHistogramMaker();

				if(mVariables.Size() < 6)
				{
					v.Color = mDefaultColor[mVariables.Size()];
				}
				else
				{
					v.Color = mDefaultColor[0];
				}
				v.Opacity = 0.5;

				mVariables.Add(v);
				if(v.Name.Length() > mMaxNameLength)
				{
					this->UpdateWidget();
				}
				else
				{
					this->InsertFormattedEntry(mVariables.MaxIndex());
				}
				this->UpdateDependents();
			}
		}

		void RemoveVariable(int n)
		{
			if(n>=0 && n<mVariables.Size())
			{
				mVariables.Remove(n);
			}
		}

		virtual void Select(int lineFrom, int lineTo)
		{
			iggWidgetListView::Select(lineFrom,lineTo);
			this->UpdateDependents();
		}

		void Modify(int type)
		{
			static Variable *buffer = 0;
			static int length = 0;
			int i;
			Variable v;

			if(!this->HasSelection()) return;

			switch(type)
			{
			case 0:
				{
					if(mSelectedRange[0] == 0) return; // already there

					int n = mSelectedRange[1] - mSelectedRange[0] + 1;
					if(n > length)
					{
						//
						//  Extend the buffer
						//
						Variable *tmp = new Variable[n];
						if(length > 0)
						{
							memcpy(tmp,buffer,length*sizeof(Variable));
							delete [] buffer;
						}
						buffer = tmp;
						length = n;
					}
					for(i=0; i<n; i++) buffer[i] = mVariables[mSelectedRange[0]+i];
					for(i=mSelectedRange[0]-1; i>=0; i--) mVariables[i+n] = mVariables[i];
					for(i=0; i<n; i++) mVariables[i] = buffer[i];
					break;
				}
			case 1:
				{
					if(mSelectedRange[0] == 0) return; // already there

					v = mVariables[mSelectedRange[0]-1];
					for(i=mSelectedRange[0]; i<=mSelectedRange[1]; i++)
					{
						mVariables[i-1] = mVariables[i];
					}
					mVariables[mSelectedRange[1]] = v;
					break;
				}
			case 2:
				{
					if(mSelectedRange[1] == mVariables.MaxIndex()) return; // already there

					v = mVariables[mSelectedRange[1]+1];
					for(i=mSelectedRange[1]; i>=mSelectedRange[0]; i--)
					{
						mVariables[i+1] = mVariables[i];
					}
					mVariables[mSelectedRange[0]] = v;
					break;
				}
			case 3:
				{
					int n = mSelectedRange[1] - mSelectedRange[0] + 1;
					for(i=0; i<n; i++) this->RemoveVariable(mSelectedRange[0]);
					break;
				}
			}
			this->UpdateWidget();
		}

		Variable& GetSelectedVar()
		{
			static Variable null;
			if(this->HasSelection())
			{
				return mVariables[mSelectedRange[0]];
			}
			else return null;
		}

		void UpdateVariableColor()
		{
			if(this->HasSelection())
			{
				int s = mSelectedRange[0];
				this->UpdateWidget();
				this->Select(s,s);
			}
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			bool ok;
			int i, j, wn, n = mVariables.Size();
			iggShell *s = this->GetShell();

			mMaxNameLength = -1;
			bool redo = true;
			if(n == this->Count())
			{
				redo = false;
				iString ws;
				for(i=0; !redo && i<n; i++)
				{
					ws = this->GetItem(i);
					ws.ReduceWhiteSpace();
					if(mVariables[i].Name != ws.Section(" ",0,0))
					{
						redo = true;
					}

					j = ws.Find("window#");
					wn = -1;
					if(j > 0)
					{
						ws = ws.Part(j+7);
						if(mSubject->SupportsHTML())
						{
							//
							//  remove all tags
							//
							ws.RemoveFormatting("<",">");
						}
						ws.ReduceWhiteSpace();
						wn = ws.Section(" ",0,0).ToInt(ok) - 1;
#ifndef I_NO_CHECK
						if(!ok)
						{
							IBUG_WARN("Internal check failed.");
						}
#endif
					}
#ifndef I_NO_CHECK
					else
					{
						IBUG_WARN("Internal check failed.");
					}
#endif
					if(wn<0 || s->GetViewModule(wn)!=mVariables[i].View)
					{
						redo = true;
					}
				}
			}

			if(redo)
			{
				//
				//  remove non-existent VMs
				//
				n = s->GetNumberOfViewModules();
				for(i=0; i<mVariables.Size(); i++) // the upper limit is variable!!!
				{
					ok = false;
					for(j=0; !ok && j<n; j++) if(s->GetViewModule(j) == mVariables[i].View) ok = true;
					if(!ok)
					{
						this->RemoveVariable(i--);
					}
				}

				this->Clear();
				n = mVariables.Size();
				for(i=0; i<n; i++)
				{
					this->InsertFormattedEntry(i);
				}
				this->UpdateDependents();
			}

			this->iggWidgetListView::UpdateWidgetBody();
		}

		void InsertFormattedEntry(int n)
		{
			if(!mSubject->SupportsHTML()) mSubject->SetTextColor(mVariables[n].Color);
			this->InsertItem(this->GetFormattedEntry(n));
		}

		iString GetFormattedEntry(int n)
		{
			static iString dummy;

			if(mMaxNameLength < 0)
			{
				int i, n = mVariables.Size();
				for(i=0; i<n; i++)
				{
					if(mVariables[i].Name.Length() > mMaxNameLength) mMaxNameLength = mVariables[i].Name.Length();
				}
				while(dummy.Length() < mMaxNameLength+3) dummy += ".........";
			}

			iString ws;
			if(mSubject->SupportsHTML())
			{
				ws += "<b><font color=#" + iString::FromNumber(mVariables[n].Color.Red(),"%02x") + iString::FromNumber(mVariables[n].Color.Green(),"%02x") + iString::FromNumber(mVariables[n].Color.Blue(),"%02x") + ">";
			}
			ws += mVariables[n].Name;
			if(mSubject->SupportsHTML()) ws += "</font></b>";
			ws += dummy.Part(0,3+mMaxNameLength-mVariables[n].Name.Length());
			ws += " window#";
			if(mSubject->SupportsHTML()) ws += "<b>";
			ws += iString::FromNumber(mVariables[n].View->GetWindowNumber()+1,"%2d");
			if(mSubject->SupportsHTML()) ws += "</b>";
			ws += " data: ";
			if(mSubject->SupportsHTML()) ws += "<b>";
			ws += mVariables[n].Data.Type(0).TextName();
			if(mSubject->SupportsHTML()) ws += "</b>";
			return ws;
		}

		int mMaxNameLength;
		iColor mDefaultColor[9];
		iArray<Variable> mVariables;  // not pointers for cache coherence
		iggDialogDataExplorer *mDataExplorer;
	};
	//
	//  Widgets for setting variable's color and opacity
	//
	class VariablePropertyOpacitySlider : public iggWidget
	{

	public:

		VariablePropertyOpacitySlider(ExploredVariablesList *list, iggFrame *parent) : iggWidget(parent)
		{
			mList = list;
			mSubject = iggSubjectFactory::CreateWidgetEntrySubject(this,true,0,"Opacity");
			mNeedsBaloonHelp = false;
		}

	protected:

		void UpdateWidgetBody()
		{
			this->Enable(mList->HasSelection());
			mSubject->SetValue(iMath::Round2Int(20.0*mList->GetSelectedVar().Opacity));
		}

		void OnInt1Body(int)
		{
			mList->GetSelectedVar().Opacity = 0.05*mSubject->GetValue();
		}

		ExploredVariablesList *mList;
		ibgWidgetEntrySubject *mSubject;
	};

	class VariablePropertyColorSelection : public iggWidget
	{

	public:

		VariablePropertyColorSelection(ExploredVariablesList *list, iggFrame *parent) : iggWidget(parent)
		{
			mList = list;
			mSubject = iggSubjectFactory::CreateWidgetColorSelectionSubject(this);
			mNeedsBaloonHelp = false;
		}

	protected:

		void UpdateWidgetBody()
		{
			this->Enable(mList->HasSelection());
			mSubject->SetColor(mList->GetSelectedVar().Color);
		}

		void OnVoid1Body()
		{
			mList->GetSelectedVar().Color = mSubject->GetColor();
			mList->UpdateVariableColor();
		}

		ExploredVariablesList *mList;
		ibgWidgetColorSelectionSubject *mSubject;
	};

	//
	//  Fraction info list
	//
	class FractionInfoList : public iggFrameFlip
	{

	public:

		FractionInfoList(ExploredVariablesList *list, iggFrameHistogramControls *controls, iggFrame *parent) : iggFrameFlip(parent)
		{
			mList = list;
			mControls = controls;
			mValue = 0.0;
			
			//
			//  Use double buffer
			//
			iggFrame *tmp;
			tmp = new iggFrame(this);
			mView0 = new iggWidgetTextBrowser(false,false,tmp);
			tmp->AddLine(mView0);
			this->AddLayer(tmp);
			tmp = new iggFrame(this);
			mView1 = new iggWidgetTextBrowser(false,false,tmp);
			tmp->AddLine(mView1);
			this->AddLayer(tmp);
			mView = mView1;
			this->ShowLayer(0);
		}

		void SetValue(float v)
		{
			mValue = v;
			this->UpdateWidget();
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			static iString dummy;
			//
			//  Cache a few things
			//
			const iArray<Variable> &vars = mList->GetVariables();

			int i, maxlength = 0;
			for(i=0; i<vars.Size(); i++)
			{
				if(vars[i].Name.Length() > maxlength) maxlength = vars[i].Name.Length();
			}
			while(dummy.Length() < maxlength+3) dummy += ".........";

			mView->Clear();
			mView->AppendTextLine("Value",iString::FromNumber(mValue));
			int sid = mControls->GetStretchId();
			for(i=0; i<vars.Size(); i++)
			{
				const iHistogram &h(vars[i].Maker->GetHistogram(vars[i].Var,sid,true));
				double sum1, sum2;
				if(mValue < h.GetMin())
				{
					sum1 = 0.0;
					sum2 = 1.0;
				}
				else if(mValue > h.GetMax())
				{
					sum1 = 1.0;
					sum2 = 0.0;
				}
				else
				{
					sum1 = sum2 = 0.0;
					int j;
					for(j=0; j<h.NumBins(); j++)
					{
						if(mValue < h.GetBinMax(j)) sum2 += h.Value(j); else sum1 += h.Value(j); 
					}
					if(sum1+sum2 < 1.0e-30) sum1 = sum2 = 1.0;
				}
				mView->AppendTextLine(vars[i].Name+dummy.Part(0,3+maxlength-vars[i].Name.Length()),"below "+iString::FromNumber(sum1/(sum1+sum2))+", above "+iString::FromNumber(sum2/(sum1+sum2)),vars[i].Color);
			}
			if(mView == mView1)
			{
				mView = mView0;
				this->ShowLayer(1);
			}
			else
			{
				mView = mView1;
				this->ShowLayer(0);
			}

			this->iggFrameFlip::UpdateWidgetBody();
		}

		float mValue;
		ExploredVariablesList *mList;
		iggFrameHistogramControls *mControls;
		iggWidgetTextBrowser *mView, *mView0, *mView1;
	};
	//
	//  Draw area
	//
	class Area : public iggWidgetHistogramArea
	{

	public:

		Area(ExploredVariablesList *list, iggFrameHistogramControls *controls, iggFrame *parent) : iggWidgetHistogramArea(controls,parent)
		{
			mList = list;
		
			mGlobalMin = 0.0;
			mGlobalMax = 1.0;

			list->AddDependent(this);

			mUnderInteraction = false;
			mInfoList = 0;

			mNeedsBaloonHelp = false;
		}

		void AttachInfoList(FractionInfoList *l)
		{
			mInfoList = l;
		}

		virtual void OnMousePress(int x, int y, int b)
		{
			if(b != MouseButton::LeftButton) return;

			if(!mUnderInteraction && mInfoList!=0)
			{
				mUnderInteraction = true;
				mPosX = x;
				mPosY = y;
				this->UpdateInfoList();
				mList->DataExplorer()->ShowFractionInfo(true);
				mSubject->RequestPainting(DrawMode::Foreground);
			}
		}

		virtual void OnMouseRelease(int x, int y, int b)
		{
			if(b != MouseButton::LeftButton) return;

			if(mUnderInteraction)
			{
				mUnderInteraction = false;
				mList->DataExplorer()->ShowFractionInfo(false);
				mSubject->RequestPainting(DrawMode::Foreground);
			}
		}

		virtual void OnMouseMove(int x, int y, int b)
		{
			if(b != MouseButton::LeftButton) return;

			if(mUnderInteraction)
			{
				mPosX = x;
				mPosY = y;
				this->UpdateInfoList();
				mSubject->RequestPainting(DrawMode::Foreground);
			}
		}

	protected:

		void UpdateInfoList()
		{
			float min = iStretch::Apply(mGlobalMin,mControls->GetStretchId(),false);
			float max = iStretch::Apply(mGlobalMax,mControls->GetStretchId(),true);
			float f = float(mPosX)/float(mSubject->Width()-1);
			if(f < 0.0f) f = 0.0f;
			if(f > 1.0f) f = 1.0f;
			mInfoList->SetValue(iStretch::Reset(min+(max-min)*f,mControls->GetStretchId()));
		}

		virtual void DrawBackgroundBody()
		{
			//
			//  Cache a few things
			//
			const iArray<Variable> &vars = mList->GetVariables();
			this->GetMainWindow()->BLOCK(true);

			//
			//  Compute the global range first
			//
			int i;
			if(vars.Size() > 0)
			{
				vars[0].Maker->GetRange(vars[0].Var,mGlobalMin,mGlobalMax);
				for(i=1; i<vars.Size(); i++)
				{
					float min, max;
					vars[i].Maker->GetRange(vars[i].Var,min,max);
					if(mGlobalMin > min) mGlobalMin = min;
					if(mGlobalMax < max) mGlobalMax = max;
				}
			}

			//
			//  We use the background to accumulate the image
			//  Paint in the reverse order, so that the variable on top in the
			//  list is on top in the image too
			//
			int sid = mControls->GetStretchId();
			float min = iStretch::Apply(mGlobalMin,sid,false);
			float max = iStretch::Apply(mGlobalMax,sid,true);
			for(i=vars.Size()-1; i>=0; i--)
			{
				if(i < vars.Size()-1)
				{
					mSubject->Begin(DrawMode::Foreground,true);
				}
				float range[2];
				vars[i].Maker->GetRange(vars[i].Var,range);
				range[0] = (iStretch::Apply(range[0],sid,false)-min)/(max-min+1.0e-36f);
				range[1] = (iStretch::Apply(range[1],sid,true)-min)/(max-min+1.0e-36f);

				this->DrawHistogram(vars[i].Maker->GetHistogram(vars[i].Var,sid,true),range,vars[i].Color);

				if(i > 0) mSubject->End();
				//
				//  Blend images
				//
				if(i < vars.Size()-1) mSubject->BlendForegroundOntoBackground(vars[i].Opacity);
			}

			mControls->DisplayRange(mGlobalMin,mGlobalMax);

			this->GetMainWindow()->BLOCK(false);
		}

		virtual void DrawForegroundBody()
		{
			static const iColor markerColor(0,0,0);

			//
			// draw a marker line if under interaction
			//
			if(mUnderInteraction)
			{
				mSubject->DrawLine(mPosX,0,mPosX,mSubject->Height()-1,markerColor,5);
			}
		}

		virtual void GetHistogramRange(float &min, float &max) const
		{
			min = mGlobalMin;
			max = mGlobalMax;
		}

		int mPosX, mPosY;
		float mGlobalMin, mGlobalMax;
		bool mUnderInteraction;
		ExploredVariablesList *mList;
		FractionInfoList *mInfoList;
	};
	//
	//  List of available variables
	//
	class AvailableVariablesList : public iggWidgetListView
	{

	public:

		AvailableVariablesList(iggDialogDataExplorer *de, ExploredVariablesList *list, iggFrameDataTypeSelector *dts, iggFrame *parent) : iggWidgetListView(false,parent)
		{
			mSelector = dts;
			mDataExplorer = de;
			mList = list;
			mAddButton = 0;
		}

		void SetButton(iggWidget *addbutton)
		{
			mAddButton = addbutton;
		}

		virtual void Select(int lineFrom, int lineTo)
		{
			iggWidgetListView::Select(lineFrom,lineTo);
			mAddButton->Enable(this->HasSelection());
		}

		void AddSelectedVariables()
		{
			int i;
			for(i=mSelectedRange[0]; i<=mSelectedRange[1] && i<this->GetNumberOfLines(); i++)
			{
				mList->AddVariable(i);
			}
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			mSelector->UpdateWidget();

			iDataReader *dr = this->GetShell()->GetViewModule()->GetReader();
			iDataLimits *l = dr->GetLimits(mDataExplorer->GetActiveDataType());

			if(l!=0 && dr->IsThereData(l->GetDataType()))
			{
				int i, n = l->GetNumVars();

				bool redo = true;
				if(n == this->Count())
				{
					redo = false;
					iString ws;
					for(i=0; !redo && i<n; i++)
					{
						ws = this->GetItem(i);
						ws.ReduceWhiteSpace();
						if(l->GetName(i) != ws)
						{
							redo = true;
						}
					}
				}

				if(redo)
				{
					this->Clear();
					for(i=0; i<n; i++)
					{
						this->InsertItem(l->GetName(i));
					}
				}
			}
			else this->Clear();

			this->iggWidgetListView::UpdateWidgetBody();
		}

		virtual void OnReturnPressed()
		{
			this->AddSelectedVariables();
		}

		iggWidget *mAddButton;
		iggFrameDataTypeSelector *mSelector;
		ExploredVariablesList *mList;
		iggDialogDataExplorer *mDataExplorer;
	};


	class AddVariableButton : public iggWidgetSimpleButton
	{

	public:

		AddVariableButton(AvailableVariablesList *list, iggFrame *parent) : iggWidgetSimpleButton("",parent,true)
		{
			mList = list;
			mSubject->SetIcon(*iggImageFactory::FindIcon("moveleft.png"));

			this->SetBaloonHelp("Show selected variables");
		}

		~AddVariableButton()
		{
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			this->Enable(mList->HasSelection());
		}

		virtual void Execute()
		{
			mList->AddSelectedVariables();
		}

		AvailableVariablesList *mList;
	};


	class ControlButton : public iggWidgetSimpleButton
	{

	public:

		ControlButton(int type, ExploredVariablesList *list, iggFrame *parent) : iggWidgetSimpleButton("",parent,true)
		{
			mType = type;
			mList = list;
			switch(type)
			{
			case 0:
				{
					mSubject->SetIcon(*iggImageFactory::FindIcon("movetop.png"));
					this->SetBaloonHelp("Move the current variables to the top");
					break;
				}
			case 1:
				{
					mSubject->SetIcon(*iggImageFactory::FindIcon("moveup.png"));
					this->SetBaloonHelp("Move the current variables up");
					break;
				}
			case 2:
				{
					mSubject->SetIcon(*iggImageFactory::FindIcon("movedown.png"));
					this->SetBaloonHelp("Move the current variables down");
					break;
				}
			case 3:
				{
					mSubject->SetIcon(*iggImageFactory::FindIcon("delete.png"));
					this->SetBaloonHelp("Remove selected variables");
					break;
				}
			default:
				{
					IBUG_FATAL("Invalid type");
				}
			}
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			this->Enable(mList->HasSelection());
		}

		virtual void Execute()
		{
			mList->Modify(mType);
		}

		int mType;
		ExploredVariablesList *mList;
	};


	class DataComboBox : public iggWidget
	{

	public:

		DataComboBox(bool ismin, ExploredVariablesList *list, iggFrame *parent) : iggWidget(parent)
		{
			mList = list;
			mIsMin = ismin;

			mSubject = iggSubjectFactory::CreateWidgetComboBoxSubject(this,"");
			mNeedsBaloonHelp = false;
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			mSubject->Clear();
			
			if(mList->GetVariables().Size() > 0)
			{
				const Variable &v(mList->GetVariables()[0]);
				const iHistogramMaker::PointInfo &p(mIsMin?v.Maker->GetMin(v.Var):v.Maker->GetMax(v.Var));
				mSubject->InsertItem(iString::FromNumber(p.Value));
				mSubject->InsertItem("Cell: "+iString::FromNumber(p.Cell));
				mSubject->InsertItem("at X: "+iString::FromNumber(p.Position[0]));
				mSubject->InsertItem("at Y: "+iString::FromNumber(p.Position[1]));
				mSubject->InsertItem("at Z: "+iString::FromNumber(p.Position[2]));
			}
		}

		bool mIsMin;
		ibgWidgetComboBoxSubject *mSubject;
		ExploredVariablesList *mList;
	};


	class MoveFocalPointFrame : public iggFrameMoveFocalPointButton
	{
		
	public:
		
		MoveFocalPointFrame(bool min, ExploredVariablesList *list, iggFrame *parent) : iggFrameMoveFocalPointButton(min?"Minimum":"Maximum",parent,true)
		{
			mIsMin = min;
			mList = list;

			this->SetBaloonHelp("Move the camera focal point or place a marker at the location of the maximum or minimum value");
		}

		virtual int GetWindowNumber() const 
		{ 
			if(mList->GetVariables().Size() > 0)
			{
				return mList->GetVariables()[0].View->GetWindowNumber();
			}
			else return -1;
		}

		virtual iVector3D GetPosition() const
		{
			if(mList->GetVariables().Size() > 0)
			{
				const Variable &v = mList->GetVariables()[0];
				if(mIsMin)
				{
					return v.Maker->GetMin(v.Var).Position.BoxValue();
				}
				else
				{
					return v.Maker->GetMax(v.Var).Position.BoxValue();
				}
			}
			return iVector3D();
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			this->Enable(mList->GetVariables().Size() > 0);
			this->iggFrameMoveFocalPointButton::UpdateWidgetBody();
		}

		bool mIsMin;
		ExploredVariablesList *mList;
	};
};


using namespace iggDialogDataExplorer_Private;


iggDialogDataExplorer::iggDialogDataExplorer(iggMainWindow *parent) : iggDialog(parent,0U,iggImageFactory::FindIcon("dataexp.png"),"Data Explorer","sr.gg.dde",2)
{
	mHandle = new DataTypeHandle(this); IERROR_CHECK_MEMORY(mHandle);

	if(this->ImmediateConstruction()) this->CompleteConstruction();
}


void iggDialogDataExplorer::CompleteConstructionBody()
{
	iggFrame *box1, *box2, *tmp1, *tmp2;

	mFlashFrame = new iggFrameFlip(mFrame);
	tmp1 = new iggFrame(mFlashFrame,2);
	mFlashFrame->AddLayer(tmp1);

	box1 = new iggFrame(tmp1,2);
	box2 = new iggFrame(tmp1,2);

	iggFrameDataTypeSelector *dts = new iggFrameDataTypeSelector(mHandle,"Use data",box2,ListDataTypes::WithVariables);
	ExploredVariablesList *el = new ExploredVariablesList(this,box1);
	AvailableVariablesList *al = new AvailableVariablesList(this,el,dts,box2);
	tmp1->AddLine(new iggWidgetTextLabel("   Shown variables",tmp1),new iggWidgetTextLabel("   All variables",tmp1));
	tmp1->AddLine(box1,box2);

	tmp1 = new iggFrame(mFrame,1);
	iggFrameHistogramControls *controls = new iggFrameHistogramControls(false,true,tmp1);
	controls->SetYLogScale(true);
	controls->SetStretchId(iStretch::Log);
	Area *area = new Area(el,controls,tmp1);
	tmp1->AddLine(area);
	tmp1->AddLine(controls);
	tmp1->SetRowStretch(0,10);

	tmp2 = new iggFrame(mFrame);
	iggFrame *b = new iggFrame("Data for top variable",tmp2,2);
	iggWidget *w;
	w = new DataComboBox(0,el,b);
	el->AddDependent(w);
	b->AddLine(new iggWidgetTextLabel("Maximum",b),w);
	w = new DataComboBox(1,el,b);
	el->AddDependent(w);
	b->AddLine(new iggWidgetTextLabel("Minimum",b),w);
	tmp2->AddLine(b);
	w = new MoveFocalPointFrame(false,el,tmp2);
	el->AddDependent(w);
	tmp2->AddLine(w);
	w = new MoveFocalPointFrame(true,el,tmp2);
	el->AddDependent(w);
	tmp2->AddLine(w);
	iggFrame *tmp3 = new iggFrame("Shown variable",tmp2,2);
	w = new VariablePropertyColorSelection(el,tmp3);
	w->AddDependent(area);
	tmp3->AddLine(w);
	w = new VariablePropertyOpacitySlider(el,tmp3);
	w->AddDependent(area);
	tmp3->AddLine(w,2);
	el->AddDependent(tmp3);
	tmp2->AddLine(tmp3);
	tmp2->AddSpace(10);

	mFrame->AddLine(tmp1,tmp2);

//	box1->AddLine(0,new iggWidgetTextLabel("Shown variables",box1));
	tmp1 = new iggFrame(box1);
	ControlButton *b2;
	b2 = new ControlButton(0,el,tmp1);
	el->AddDependent(b2);
	tmp1->AddLine(b2);
	b2 = new ControlButton(1,el,tmp1);
	el->AddDependent(b2);
	tmp1->AddLine(b2);
	b2 = new ControlButton(2,el,tmp1);
	el->AddDependent(b2);
	tmp1->AddLine(b2);
	b2 = new ControlButton(3,el,tmp1);
	el->AddDependent(b2);
	tmp1->AddLine(b2);
	tmp1->AddSpace(10);
	box1->AddLine(tmp1,el);

//	box2->AddLine(0,new iggWidgetTextLabel("All variables",box2));
	tmp1 = new iggFrame(box2);
	AddVariableButton *b1 = new AddVariableButton(al,tmp1);
	al->SetButton(b1);
	tmp1->AddSpace(10);
	tmp1->AddLine(b1);
	tmp1->AddSpace(10);
	box2->AddLine(tmp1,al);
	box2->AddLine(static_cast<iggWidget*>(0),dts);

//	mFrame->AddLine(box1,box2);
	mFrame->AddLine(mFlashFrame,2);

	tmp1 = new iggFrame("",mFlashFrame);
	tmp1->SetPadding(false);
	FractionInfoList *fi = new FractionInfoList(el,controls,tmp1);
	area->AttachInfoList(fi);
	tmp1->AddLine(fi);
	mFlashFrame->AddLayer(tmp1);
	mFlashFrame->ShowLayer(0);

	mFrame->SetRowStretch(0,10);
	mFrame->SetRowStretch(1,5);
	mFrame->SetColStretch(0,10);

	this->ResizeContents(700,500);
}


iggDialogDataExplorer::~iggDialogDataExplorer()
{
	delete mHandle;
}


const iDataType& iggDialogDataExplorer::GetActiveDataType() const
{
	return mHandle->GetActiveDataType();
}


void iggDialogDataExplorer::ShowFractionInfo(bool s)
{
	mFlashFrame->ShowLayer(s?1:0);
}

#endif
