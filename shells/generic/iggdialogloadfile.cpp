/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggdialogloadfile.h"


#include "idata.h"
#include "idatareader.h"
#include "ierror.h"
#include "isystem.h"
#include "iviewmodule.h"

#include "iggframe.h"
#include "igghandle.h"
#include "iggmainwindow.h"
#include "iggshell.h"
#include "iggshelleventobservers.h"
#include "iggwidgetmisc.h"
#include "iggwidgetprogressbar.h"


using namespace iParameter;


const bool iggDialogLoadFile::mIsMultiThread = false;


namespace iggDialogLoadFile_Private
{
	class ProgressBar : public iggWidgetProgressBar
	{

	public:

		ProgressBar(iggFrame *parent) : iggWidgetProgressBar(parent)
		{
		}

	protected:

		virtual void OnSetProgress()
		{
			this->GetMainWindow()->ProcessEvents();
		}
	};
};


using namespace iggDialogLoadFile_Private;


//
//  Main class
//
iggDialogLoadFile::iggDialogLoadFile(iggMainWindow *parent) : iggDialog(parent,mIsMultiThread?0U:DialogFlag::Blocking,0,"Loading file...",0,2,"Abort")
{
	if(this->ImmediateConstruction()) this->CompleteConstruction();
}


void iggDialogLoadFile::CompleteConstructionBody()
{
	mProgressBar = new ProgressBar(mFrame);
	mLabel = new iggWidgetTextLabel("",mFrame);

	mFrame->AddLine(new iggWidgetTextLabel("%bLoading file:",mFrame));
	mFrame->AddLine(mLabel,2);
	mFrame->AddLine(mProgressBar,2);
	if(mIsMultiThread)
	{
		mFrame->AddLine(new iggWidgetTextLabel("You can work with other windows while file is being loaded.",mFrame),2);
	}
	else
	{
		mFrame->AddLine(new iggWidgetTextLabel("Please wait while file is being loaded.",mFrame),2);
	}

	mFrame->SetColStretch(1,10);

	this->ResizeContents(300,200);
}


iggDialogLoadFile::~iggDialogLoadFile()
{
}


void iggDialogLoadFile::Start()
{
	if(!mCompleteConstructionCalled) this->CompleteConstruction();

	iRequiredCast<iggExecutionEventObserver>(INFO,this->GetShell()->GetExecutionEventObserver())->SetProgressBar(mProgressBar);

	iggDialog::Show(true);
	this->GetMainWindow()->BLOCK(true);

	mLabel->SetText("...");
}


void iggDialogLoadFile::OnFileStart(const iString &fname, const iDataType &type)
{
	mLabel->SetText(fname);
}


void iggDialogLoadFile::OnFileFinish(const iString &fname, const iDataInfo &info)
{
}


void iggDialogLoadFile::Finish()
{
	this->GetMainWindow()->AfterDataChanged();

	this->GetMainWindow()->BLOCK(false);
	iggDialog::Show(false);
}


bool iggDialogLoadFile::CanBeClosed()
{
	mProgressBar->Abort();
	return true;
}

#endif
