/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggpagemain.h"


#include "ierror.h"

#include "iggmainwindow.h"
#include "iggshell.h"

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"


iggPageMain::iggPageMain(iggFrameBase *parent, int flags) : iggPage(parent,flags)
{
	mFlipFrame = 0;
	mFlipIndex = -1;
}


iggPageMain::~iggPageMain()
{
}


void iggPageMain::Polish()
{
	if(mFlipFrame == 0)
	{
		//
		//  Attach a flip frame
		//
		mFlipFrame = this->CreateFlipFrame(this->GetMainWindow()->GetDataTypeFrame());
		mFlipIndex = this->GetMainWindow()->GetDataTypeFrame()->Count();
		this->GetMainWindow()->GetDataTypeFrame()->AddLayer(mFlipFrame);
	}
}


void iggPageMain::UpdateChildren()
{
	this->Polish();

	mFlipFrame->UpdateWidget();
	iggPage::UpdateChildren();
	if(this->IsVisible()) this->GetMainWindow()->GetDataTypeFrame()->ShowLayer(mFlipIndex);
}


//
//  Helper class for window-dependent indicies (like active instances and data types)
//
iggIndex::iggIndex(iggShell *s)
{
	IASSERT(s);
	mShell = s;
}


iggIndex::RegistryItem& iggIndex::Item() const
{
	int i,n = mShell->GetActiveViewModuleIndex();
	iViewModule *vm;

	for(i = 0; i <= n; i++)
	{
		vm = mShell->GetViewModule(i);

		//
		//  Update the registry, but only up to the window we need
		//
		while(i < this->mRegistry.Size() && this->mRegistry[i].vm != vm)
		{
			this->mRegistry.Remove(i);
		}
		if(i == this->mRegistry.Size())
		{
			RegistryItem w;
			w.vm = vm;
			w.idx = 0;
			this->mRegistry.Add(w);
		}
	}

	return this->mRegistry[n];
}

#endif
