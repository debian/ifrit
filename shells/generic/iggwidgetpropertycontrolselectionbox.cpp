/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggwidgetpropertycontrolselectionbox.h"


#include "istretch.h"
#include "istring.h"

#include "iggframe.h"

#include "ibgwidgetselectionboxsubject.h"

#include <limits.h>

#include "iggsubjectfactory.h"

//
//  Templates
//
#include "iarray.tlh"
#include "iggwidgetpropertycontrol.tlh"


using namespace iParameter;


//
//******************************************
//
//  SelectionBox class
//
//******************************************
//
iggWidgetPropertyControlSelectionBox::iggWidgetPropertyControlSelectionBox(int offset, const iggPropertyHandle& ph, iggFrame *parent,int rm) : iggWidgetPropertyControl<iType::Int>(ph,parent,rm)
{
	mOffset = offset;
}


void iggWidgetPropertyControlSelectionBox::OnInt1Body(int)
{
	this->ExecuteControl(true);
}


void iggWidgetPropertyControlSelectionBox::SetOffset(int o)
{
	mOffset = o;
}


//
//******************************************
//
//  ItemizedSelectionBox
//
//******************************************
//
iggWidgetPropertyControlItemizedSelectionBox::iggWidgetPropertyControlItemizedSelectionBox(int offset, const iggPropertyHandle& ph, iggFrame *parent, int rm) : iggWidgetPropertyControlSelectionBox(offset,ph,parent,rm)
{
}


//
//******************************************
//
//  SpinBox
//
//******************************************
//
iggWidgetPropertyControlSpinBox::iggWidgetPropertyControlSpinBox(int min, int max, const iString &title, int offset, const iggPropertyHandle& ph, iggFrame *parent, int rm) : iggWidgetPropertyControlSelectionBox(offset,ph,parent,rm)
{
	mSubject = iggSubjectFactory::CreateWidgetSpinBoxSubject(this,min,max,title,1);
}


void iggWidgetPropertyControlSpinBox::QueryValue(int &v) const
{
	v = mSubject->GetValue() - mOffset;
}


void iggWidgetPropertyControlSpinBox::UpdateValue(int v)
{
	mSubject->SetValue(v+mOffset);
}


void iggWidgetPropertyControlSpinBox::SetFirstEntryText(const iString &text)
{
	mSubject->SetFirstEntryText(text);
}


void iggWidgetPropertyControlSpinBox::SetRange(int min, int max)
{
	mSubject->SetRange(min,max);
}


void iggWidgetPropertyControlSpinBox::SetStretch(int title, int box)
{
	mSubject->SetStretch(title,box);
}


int iggWidgetPropertyControlSpinBox::Count()
{
	return mSubject->Count();
}

//
//******************************************
//
//  ComboBox
//
//******************************************
//
iggWidgetPropertyControlComboBox::iggWidgetPropertyControlComboBox(const iString &title, int offset, const iggPropertyHandle& ph, iggFrame *parent, int rm) : iggWidgetPropertyControlItemizedSelectionBox(offset,ph,parent,rm)
{
	mSubject = iggSubjectFactory::CreateWidgetComboBoxSubject(this,title);
}


void iggWidgetPropertyControlComboBox::QueryValue(int &v) const
{
	v = mSubject->GetValue() - mOffset;
}


void iggWidgetPropertyControlComboBox::UpdateValue(int v)
{
	mSubject->SetValue(v+mOffset);
}


void iggWidgetPropertyControlComboBox::InsertItem(const iString &text, int ind)
{
	mSubject->InsertItem(text,ind);
}


void iggWidgetPropertyControlComboBox::SetItem(const iString &text, int ind, bool vis)
{
	mSubject->SetItem(text,ind,vis);
}


void iggWidgetPropertyControlComboBox::RemoveItem(int index)
{
	if(index>=0 && index<this->Count())	mSubject->RemoveItem(index);
}


int iggWidgetPropertyControlComboBox::Count()
{
	return mSubject->Count();
}


void iggWidgetPropertyControlComboBox::Clear()
{
	mSubject->Clear(); 
}


void iggWidgetPropertyControlComboBox::SetStretch(int title, int box)
{
	mSubject->SetStretch(title,box);
}


//
//******************************************
//
//  RadioBox
//
//******************************************
//
iggWidgetPropertyControlRadioBox::iggWidgetPropertyControlRadioBox(int cols, const iString &title, int offset, const iggPropertyHandle& ph, iggFrame *parent, int rm) : iggWidgetPropertyControlItemizedSelectionBox(offset,ph,parent,rm)
{
	mSubject = iggSubjectFactory::CreateWidgetRadioBoxSubject(this,cols,title);
}


void iggWidgetPropertyControlRadioBox::QueryValue(int &v) const
{
	v = mSubject->GetValue() - mOffset; 
}


void iggWidgetPropertyControlRadioBox::UpdateValue(int v)
{
	mSubject->SetValue(v+mOffset);
}


void iggWidgetPropertyControlRadioBox::InsertItem(const iString &text, int index)
{
	mSubject->InsertItem(text,index);
}


void iggWidgetPropertyControlRadioBox::SetItem(const iString &text, int index, bool vis)
{
	mSubject->SetItem(text,index,vis);
}


void iggWidgetPropertyControlRadioBox::RemoveItem(int index)
{
	//if(index>=0 && index<this->Count())	mSubject->RemoveItem(index);
	mSubject->RemoveItem(index);
}


int iggWidgetPropertyControlRadioBox::Count()
{
	return mSubject->Count();
}


//
//******************************************
//
//  TextComboBox
//
//******************************************
//
iggWidgetPropertyControlTextComboBox::iggWidgetPropertyControlTextComboBox(int section, const iString &title, int offset, const iggPropertyHandle& ph, iggFrame *parent, int rm) : iggWidgetPropertyControlComboBox(title,offset,ph,parent,rm)
{
	if(section < 0)
	{
		IBUG_WARN("Negative sections do not exist.");
		section = 0;
	}
	mSection = section;
	mInvalidValue = iMath::_IntMax;
}


void iggWidgetPropertyControlTextComboBox::SetInvalidValue(int v)
{
	mInvalidValue = v;
}


void iggWidgetPropertyControlTextComboBox::QueryValue(int &v) const
{
	if(!this->ConvertFromText(mSubject->GetText(),v)) v = 0;
}


void iggWidgetPropertyControlTextComboBox::UpdateValue(int v)
{
	int i, k;

	for(i=0; i<mSubject->Count(); i++)
	{
		if(this->ConvertFromText(mSubject->GetText(i),k) && v==k) break;
	}
	if(i < mSubject->Count())
	{
		mSubject->SetValue(i);
	}
	else
	{
		mSubject->SetValue(0);
		IBUG_WARN("Invalid value in ComboBox widget.");
	}
}


bool iggWidgetPropertyControlTextComboBox::ConvertFromText(const iString &text, int &val) const
{
	bool ok;
	iString ws = text;

	ws.ReduceWhiteSpace();
	val = ws.Section(" ",mSection,mSection).ToInt(ok);
	if(!ok && mInvalidValue<iMath::_IntMax)
	{
		val = mInvalidValue;
		ok = true;
	}
	return ok;
}


//
//******************************************
//
//  StretchComboBox
//
//******************************************
//
iggWidgetPropertyControlStretchComboBox::iggWidgetPropertyControlStretchComboBox(const iggPropertyHandle& ph, iggFrame *parent, int rm, bool title) : iggWidgetPropertyControl<iType::String>(ph,parent,rm)
{
	if(this->Handle()->Variable()->GetType() != iType::String)
	{
		IBUG_FATAL("Invalid property in iggWidgetPropertyControlStretchComboBox.");
	}

	mSubject = iggSubjectFactory::CreateWidgetComboBoxSubject(this,title?"Stretch":"");

	int i = 0;
	while(!iStretch::GetName(i).IsEmpty())
	{
		mSubject->InsertItem(iStretch::GetName(i++));
	}
}


void iggWidgetPropertyControlStretchComboBox::QueryValue(iString &val) const
{
	val = iStretch::GetName(mSubject->GetValue());
}


void iggWidgetPropertyControlStretchComboBox::UpdateValue(const iString& val)
{
	int v = iStretch::GetId(val);
	if(v<0 || v>=mSubject->Count())
	{
		IBUG_ERROR("Invalid stretch name");
	}
	else
	{
		mSubject->SetValue(v);
	}
}


void iggWidgetPropertyControlStretchComboBox::OnInt1Body(int)
{
	this->ExecuteControl(true);
}

#endif
