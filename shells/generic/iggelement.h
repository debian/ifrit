/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A base class for all generic (toolkit-independent) GUI elements.
//  This is really a dummy to unite all gg... classes into a single hierarchy.
//

#ifndef IGGELEMENT_H
#define IGGELEMENT_H


#include "ishellcomponent.h"


class iggShell;


namespace iParameter
{
	namespace MouseButton
	{
		//
		//  Mouse interaction buttons
		//
		const int NoButton = 0;
		const int LeftButton = 1;
		const int MiddleButton = 2;
		const int RightButton = 4;
		const int AnyButton = 7;
		const int ControlKey = 8;
		const int ShiftKey = 16;
	};

	namespace Condition
	{
		//
		//  Running conditions
		//
		const int OldWindowManager =				1;
		const int SmallDesktopSize =				2;
		const int SlowRemoteConnection =			4;
		const int DisableDelayedInitialization =	8;
	};

};


class iggElement : public iShellComponent
{

public:

	virtual ~iggElement();

	int GetId() const { return mId; }

	inline iggShell* GetShell() const { return mTrueShell; } // to avoid extra casting

protected:

	iggElement(iggShell *shell);

private:

	int mId;
	static int mIdCounter;

	iggShell *mTrueShell;

	iggElement(const iggElement&);		// Not implemented.
	void operator=(const iggElement&);	// Not implemented.
};

#endif  // IGGELEMENT_H

