/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A base class for pages of the main window
//
#ifndef IGGPAGEMAIN_H
#define IGGPAGEMAIN_H


#include "iggpage.h"


class iggPageMain : public iggPage
{

public:

	virtual ~iggPageMain();

	void Polish();

protected:

	iggPageMain(iggFrameBase *parent, int flags);

	virtual void UpdateChildren();
	virtual iggFrame* CreateFlipFrame(iggFrameBase *parent) = 0;

	iggFrame *mFlipFrame;
	int mFlipIndex;
};


//
//  Helper class for window-dependent indicies (like active instances and data types)
//
class iViewModule;

class iggIndex
{

public:

	iggIndex(iggShell *s);

	inline void operator=(int v)
	{
		this->Item().idx = v;
	}

	inline operator int() const
	{
		return this->Item().idx;
	}

private:

	struct RegistryItem
	{
		iViewModule *vm;
		int idx;
	};

	RegistryItem& Item() const;

	mutable iArray<RegistryItem> mRegistry;
	iggShell *mShell;
};


#endif  // IGGPAGEMAIN_H

