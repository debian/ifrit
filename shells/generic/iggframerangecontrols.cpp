/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggframerangecontrols.h"


#include "ierror.h"
#include "imath.h"
#include "ihistogram.h"
#include "ihistogrammaker.h"
#include "iproperty.h"
#include "ishell.h"
#include "istretch.h"

#include "iggframehistogramcontrols.h"
#include "igghandle.h"
#include "iggwidgetarea.h"
#include "iggwidgetmisc.h"
#include "iggwidgetotherbutton.h"
#include "iggwidgetpropertycontrolbutton.h"
#include "iggwidgetpropertycontrolselectionbox.h"

#include "ibgwidgetareasubject.h"
#include "ibgwidgetentrysubject.h"

#include "iggsubjectfactory.h"

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"


using namespace iParameter;


namespace iggFrameRangeControls_Private
{
	//
	//  Helper classes
	//
	class RenderButton;

	class Area : public iggWidgetHistogramArea
	{

	public:

		Area(const iggPropertyHandle& ranges, iggFrameHistogramControls *controls, iggFrameRangeControls *parent) : iggWidgetHistogramArea(controls,parent), mRanges(ranges)
		{
			mParent = parent;
			mActiveRange = 0;
			mRenderButton = 0;

			if(controls != 0) controls->SetYLogScale(true);

			//mNeedsBaloonHelp = false;
		}

		virtual ~Area()
		{
		}

		void SetRenderButton(RenderButton *b)
		{
			mRenderButton = b;
		}

		void OnSetNumRanges(int n)
		{
			mParent->SetNumRanges(n);
			mSubject->RequestPainting(DrawMode::Foreground);
			this->OnRender();
		}

		void OnRender();

		int GetNumRanges() const { return mRanges.Variable()->Size(); }

	protected:

		virtual void DrawBackgroundBody()
		{
			if(mControls != 0)
			{
				this->DrawHistogram(mParent->GetHistogram(mControls->GetStretchId(),mControls->GetFullResolution()));
			}
		}

		virtual void DrawForegroundBody()
		{
			static const int lineWidth = 3;
			static const iColor regColor = iColor(0,0,0);
			static const iColor curColor = iColor(255,0,0);
			float dx;
			int i, i1, ixmax, iymax, ix1, iy1, ix2;

			if(!mParent->IsActive()) return;

			mActiveRange = mParent->GetActiveRangeIndex();

			ixmax = mSubject->Width() - 1;
			iymax = mSubject->Height() - 1;

			float vMin, vMax;
			int npic = this->GetNumRanges();
			float totMin = iStretch::Apply(mParent->GetGlobalRange().Min,mControls->GetStretchId(),false);
			float totMax = iStretch::Apply(mParent->GetGlobalRange().Max,mControls->GetStretchId(),true);

			dx = ixmax/(totMax-totMin);

			for(i=0; i<=npic; i++)
			{
				if(i == npic)
				{
					i1 = mActiveRange;
				}
				else
				{
					i1 = i;
				}
				vMin = iStretch::Apply(this->Ranges()->GetValue(i1).Min,mControls->GetStretchId(),false);
				vMax = iStretch::Apply(this->Ranges()->GetValue(i1).Max,mControls->GetStretchId(),true);

				ix1 = iMath::Round2Int(dx*(vMin-totMin));
				if(ix1>=0 && ix1<lineWidth/2) ix1 = lineWidth/2;
				ix2 = iMath::Round2Int(dx*(vMax-totMin));
				if(ix2<=ixmax && ix2>ixmax-lineWidth/2) ix2 = ixmax-lineWidth/2;
				iy1 = iMath::Round2Int(iymax*(0.95-0.4*i1/npic));

				if(i == npic)
				{
					mSubject->DrawLine(ix1,0,ix1,iymax,curColor,lineWidth);
					mSubject->DrawLine(ix2,0,ix2,iymax,curColor,lineWidth);
					mSubject->DrawLine(ix1,iy1,ix2,iy1,curColor,lineWidth);
				}
				else
				{
					mSubject->DrawLine(ix1,0,ix1,iymax,regColor,lineWidth);
					mSubject->DrawLine(ix2,0,ix2,iymax,regColor,lineWidth);
					mSubject->DrawLine(ix1,iy1,ix2,iy1,regColor,lineWidth);
				}
			}					
		}
		
		void OnMousePress(int x, int y, int b)
		{
			this->OnMouseEvent(x,y,b,true);
		}

		void OnMouseMove(int x, int y, int b)
		{
			this->OnMouseEvent(x,y,b,false);
		}

		void OnMouseEvent(int x, int y, int b, bool click); // defined below

		const iPropertyDataVariable<iType::Pair>* Ranges() const
		{
			const iPropertyDataVariable<iType::Pair> *var = dynamic_cast<const iPropertyDataVariable<iType::Pair>*>(mRanges.Variable()); IASSERT(var);
			return var;
		}

		iggFrameRangeControls *mParent;
		int mActiveRange;
		RenderButton *mRenderButton;
		iggPropertyHandle mRanges;
	};	


	//
	//  SetNumRangesButton
	//
	class SetNumRangesSpinBox : public iggWidgetSimpleSpinBox
	{

	public:

		SetNumRangesSpinBox(Area *area, iggFrame *parent) : iggWidgetSimpleSpinBox(1,999,1,"Number of groups",parent)
		{
			mArea = area;
			mNeedsBaloonHelp = false;
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			this->SetValue(mArea->GetNumRanges());
		}

		virtual void OnInt1Body(int n)
		{
			mArea->OnSetNumRanges(n);
		}

		Area *mArea;
	};

	//
	//  RenderButton
	//
	class RenderButton : public iggWidgetSimpleButton
	{

	public:

		RenderButton(Area *area, iggFrame *parent) : iggWidgetSimpleButton("Render",parent)
		{
			mArea = area;
			mNeedsBaloonHelp = false;

			this->Enable(false);
		}

	protected:

		virtual void Execute()
		{
			mArea->OnRender();
		}

		Area *mArea;
	};


	//
	//  Area functions
	//
	void Area::OnMouseEvent(int x, int, int b, bool click)
	{
		static const int lineWidth = 3;
		static int tol = 15;
	    float dx;
	    int i, idx, idxmin, indmin, ixmax;
	    int ix1, ix2;
		static bool isMax = false;

		if(!mParent->IsActive()) return;

		mActiveRange = mParent->GetActiveRangeIndex();

		ixmax = mSubject->Width() - 1;

		float aMin, aMax, a;
		int npic = this->GetNumRanges();
		float attMin = iStretch::Apply(mParent->GetGlobalRange().Min,mControls->GetStretchId(),false);
		float attMax = iStretch::Apply(mParent->GetGlobalRange().Max,mControls->GetStretchId(),true);

		int oldCurRange = mActiveRange;

		dx = ixmax/(attMax-attMin);

		if(click)
		{
			idxmin = ixmax;
			indmin = -1;
			for(i=0; i<npic; i++)
			{
				aMin = iStretch::Apply(this->Ranges()->GetValue(i).Min,mControls->GetStretchId(),false);
				aMax = iStretch::Apply(this->Ranges()->GetValue(i).Max,mControls->GetStretchId(),true);

				ix1 = iMath::Round2Int(dx*(aMin-attMin));
				if(ix1>=0 && ix1<lineWidth/2) ix1 = lineWidth/2;
				ix2 = iMath::Round2Int(dx*(aMax-attMin));
				if(ix2<=ixmax && ix2>ixmax-lineWidth/2) ix2 = ixmax-lineWidth/2;

				idx = iMath::Abs(x-ix1);
				if(idxmin > idx)
				{
					indmin = i;
					idxmin = idx;
					isMax = false;
				}
				idx = iMath::Abs(x-ix2);
				if(idxmin > idx)
				{
					indmin = i;
					idxmin = idx;
					isMax = true;
				}
			}	

			if(click && indmin>=0 && idxmin<tol)
			{
				mActiveRange = indmin;
			}
		}					

		a = x/dx + attMin;
		if(a < attMin) a = attMin;
		if(a > attMax) a = attMax;
		aMin = iStretch::Apply(this->Ranges()->GetValue(mActiveRange).Min,mControls->GetStretchId(),false);
		aMax = iStretch::Apply(this->Ranges()->GetValue(mActiveRange).Max,mControls->GetStretchId(),true);
		if(isMax) aMax = a; else aMin = a;
		aMin = iStretch::Reset(aMin,mControls->GetStretchId());
		aMax = iStretch::Reset(aMax,mControls->GetStretchId());
		this->Ranges()->SetValue(mActiveRange,iPair(aMin,aMax));

		if(mActiveRange != oldCurRange)
		{
			mParent->SetActiveRangeIndex(mActiveRange);
		}

		if(mControls != 0)
		{
			mControls->DisplayRange(this->Ranges()->GetValue(mActiveRange).Min,this->Ranges()->GetValue(mActiveRange).Max);
		}

		mSubject->RequestPainting(DrawMode::Foreground);

		if(b == MouseButton::RightButton) this->OnRender(); else if(!mRenderButton->IsEnabled()) mRenderButton->Enable(true);
	}

	void Area::OnRender()
	{
		if(mControls != 0) mControls->UpdateWidget();
		this->Render();
		mRenderButton->Enable(false);
	}


	class TileCheckBox : public iggWidgetPropertyControlCheckBox
	{

	public:

		TileCheckBox(RenderButton *rb, const iggPropertyHandle& th, iggFrame *parent) : iggWidgetPropertyControlCheckBox("Tile",th,parent)
		{
			mRenderButton = rb;
		}

	protected:

		virtual bool ExecuteControl(bool final, bool val)
		{
			iggWidgetPropertyControlCheckBox::ExecuteControl(final,val);
			mRenderButton->Enable(true);
		}

		RenderButton *mRenderButton;
	};
};


using namespace iggFrameRangeControls_Private;


iggFrameRangeControls::iggFrameRangeControls(const iggPropertyHandle& rh, const iggPropertyHandle& th, iggFrame *parent) : iggFrame(parent)
{
	iggFrameHistogramControls *controls = new iggFrameHistogramControls(true,true,this);
	Area *a = new Area(rh,controls,this);
	this->AddLine(a);
	this->SetRowStretch(0,10);
	this->AddLine(controls);
	this->AddSpace(1);

	iggFrame *tmp = new iggFrame(this,3);
	iggWidgetPropertyControlCheckBox *tb = new iggWidgetPropertyControlCheckBox("Tile",th,tmp);
	tb->AddDependent(a);
	tmp->AddLine(new SetNumRangesSpinBox(a,tmp),tb);
	RenderButton *rb = new RenderButton(a,tmp);
	tmp->AddLine(rb);
	tmp->SetColStretch(0,0);
	tmp->SetColStretch(1,0);
	tmp->SetColStretch(2,10);
	this->AddLine(tmp);

	a->SetRenderButton(rb);

//	mNeedsBaloonHelp = false;
	iString tooltip = "Interactively controls the collection of ranges. Press Shift+F1 for more help.";
	iString whatsthis = "The range collection can be controlled interactively by clicking on vertical line that represents the range boundary and dragging it around with the mouse. Controls below the interactive area allow you to add or remove a new range (group), to change the stretch of the horizontal axis, to tile the ranges together, and to render the scene. The latter button is important because while you are dragging a range boundary with the left mouse button pressed, the scene is actually not being rendered, even if the range collection is being changed - this is done to maintain the interactive performance. Because the scene may be complex and slow to render, the interactive area will appear \"jumpy\" if the scene is rendered all the time. If you would like to render the scene continously, drag the range boundary with the right mouse button pressed.";
	this->SetBaloonHelp(tooltip,whatsthis);
	a->SetBaloonHelp(tooltip,whatsthis);
}


iggFrameRangeControls::~iggFrameRangeControls()
{
}

#endif
