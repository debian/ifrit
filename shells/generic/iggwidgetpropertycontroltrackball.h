/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
// A widget that provides control to change a direction
//

#ifndef IGGWIDGETPROPERTYCONTROLTRACKBALL_H
#define IGGWIDGETPROPERTYCONTROLTRACKBALL_H


#include "iggwidgetpropertycontrol.h"
#include "iggrenderwindowobserver.h"


class iObjectKey;

class iggDialog;

class ibgWidgetTrackBallSubject;


class iggWidgetPropertyControlTrackBall : public iggWidgetPropertyControlBase, public iggRenderWindowObserver
{

public:

    iggWidgetPropertyControlTrackBall(bool followCamera, const iggPropertyHandle& ph, iggFrame *parent, int rm = iParameter::RenderMode::Delayed);
	virtual ~iggWidgetPropertyControlTrackBall();

	void SetDirection(const double dir[3]);
	void GetDirection(double dir[3]) const;

protected:

	virtual void UpdateWidgetBody();
	virtual void OnVoid1Body();
	virtual void OnBool1Body(bool final);
	virtual void OnRenderWindowModified();

	ibgWidgetTrackBallSubject *mSubject;
	iggDialog *mValuesDialog;
};

#endif // IGGWIDGETPROPERTYCONTROLTRACKBALL_H
