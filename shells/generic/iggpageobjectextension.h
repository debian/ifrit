/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A base class for pages for object extensions
//
#ifndef IGGPAGEOBJECTEXTENSION_H
#define IGGPAGEOBJECTEXTENSION_H


#include "iggpage.h"


class iDataType;
class iImage;
class iViewSubject;

class iggObjectHandle;
class iggPageObject;
class iggPropertyHandle;


class iggPageObjectExtension : public iggPage
{

public:

	const iDataType& GetDataType() const { return mDataType; }

	iViewSubject* GetViewSubject(int mod) const;
	const iggObjectHandle* GetSubjectHandle() const { return mSubjectHandle; }
	const iggPropertyHandle CreatePropertyHandle(const iString &name) const;

	int GetActiveInstance() const;
	const iImage* Icon() const;

protected:

	iggPageObjectExtension(iggFrameBase *parent, int pageId, const iDataType &type, int flags);
	virtual ~iggPageObjectExtension();

	virtual void UpdateWidgetBody();

private:

	const int mPageId;
	const iDataType& mDataType;
	const iggPageObject *mOwner;
	iggObjectHandle *mSubjectHandle;
};

#endif  // IGGPAGEOBJECTEXTENSION_H

