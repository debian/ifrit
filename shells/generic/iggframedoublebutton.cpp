/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggframedoublebutton.h"


#include "ierror.h"
#include "ihelpfactory.h"
#include "imarker.h"
#include "iviewmodule.h"

#include "igghandle.h"
#include "iggimagefactory.h"
#include "iggmainwindow.h"
#include "iggpage.h"
#include "iggshell.h"
#include "iggwidgetpropertycontrol.h"
#include "iggwidgetotherbutton.h"

#include "ibgwidgetbuttonsubject.h"

#include "iggsubjectfactory.h"

//
//  Templates
//
#include "iarray.tlh"


using namespace iParameter;


namespace iggFrameDoubleButton_Private
{
	class Button : public iggWidget
	{
		
	public:
		
		Button(const iString &text, iggFrameDoubleButton *parent, int slot) : iggWidget(parent)
		{
			mConcreteParent = parent;
			mSubject = iggSubjectFactory::CreateWidgetButtonSubject(this,ButtonType::PushButton,text,slot);
			mNeedsBaloonHelp = false;
		}

	protected:

		virtual void UpdateWidgetBody()
		{
		}

		virtual void OnVoid1Body()
		{
			mConcreteParent->OnButton1();
		}

		virtual void OnVoid2Body()
		{
			mConcreteParent->OnButton2();
		}

		ibgWidgetButtonSubject *mSubject;
		iggFrameDoubleButton *mConcreteParent;
	};


	class AtOnceButton : public iggWidget
	{
		
	public:
		
		AtOnceButton(iggFrameMoveFocalPointButton *parent) : iggWidget(parent)
		{
			mConcreteParent = parent;

			mDelayIcon = iggImageFactory::FindIcon("delay.png");
			mReadyIcon = iggImageFactory::FindIcon("ready.png");
			if(mDelayIcon==0 || mReadyIcon==0)
			{
				IBUG_WARN("Icon images are not found.");
			}

			mSubject = iggSubjectFactory::CreateWidgetButtonSubject(this,ButtonType::ToggleButton,"",1);
			mSubject->SetFlat(true);
			mSubject->SetSize(22,22);
			
			mNeedsBaloonHelp = false;
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			if(mConcreteParent->GetMoveInstantly())
			{
				if(mReadyIcon != 0) mSubject->SetIcon(*mReadyIcon);
				mSubject->SetDown(true);
			}
			else
			{
				if(mDelayIcon != 0) mSubject->SetIcon(*mDelayIcon);
				mSubject->SetDown(false);
			}
		}

		virtual void OnVoid1Body()
		{
			mConcreteParent->SetMoveInstantly(mSubject->IsDown());
			this->UpdateWidget();
		}

		const iImage *mDelayIcon, *mReadyIcon;
		ibgWidgetButtonSubject *mSubject;
		iggFrameMoveFocalPointButton *mConcreteParent;
	};


	class PlaceMarkerButton : public iggWidgetSimpleButton
	{
		
	public:
		
		PlaceMarkerButton(iggFrameMoveFocalPointButton *parent) : iggWidgetSimpleButton("Place a marker here",parent)
		{
			mConcreteParent = parent;
			this->SetBaloonHelp("Place a marker at this position","This button puts a marker object at this poistion. Markers can be used to attach various visualization objects to them.");
		}

	protected:

		virtual void Execute()
		{
			iVector3D x = mConcreteParent->GetPosition();

			iMarkerObject *ms = this->GetShell()->GetViewModule(mConcreteParent->GetWindowNumber())->GetMarkerObject();
			if(ms->Create.CallAction())
			{
				ms->Position.SetValue(ms->GetNumber()-1,x);
				this->GetMainWindow()->UpdateAll();
				this->Render();
			}
		}

		iggFrameMoveFocalPointButton *mConcreteParent;
	};
};


using namespace iggFrameDoubleButton_Private;


//
//  Abstract base
//
iggFrameDoubleButton::iggFrameDoubleButton(iggFrame *parent, int cols) : iggFrame(parent,cols)
{
}


iggFrameDoubleButton::iggFrameDoubleButton(const iString &title, iggFrame *parent, int cols) : iggFrame(title,parent,cols)
{
}


//
//  Move the focal point to position of a given object type.
//
iggFrameMoveFocalPointButton::iggFrameMoveFocalPointButton(iggFrame *parent, bool withmarker, bool stretching) : iggFrameDoubleButton(parent,3)
{
	this->Define(withmarker,stretching);
}


iggFrameMoveFocalPointButton::iggFrameMoveFocalPointButton(const iString &title, iggFrame *parent, bool withmarker, bool stretching) : iggFrameDoubleButton(title,parent,3)
{
	this->Define(withmarker,stretching);
}


void iggFrameMoveFocalPointButton::Define(bool withmarker, bool stretching)
{
	this->AddLine(new Button("Make it a focal point",this,1),new AtOnceButton(this));
	if(withmarker) this->AddLine(new PlaceMarkerButton(this),2);
	if(stretching) this->SetColStretch(0,10); else this->SetColStretch(2,10);

	mMoveInstantly = false;
}


void iggFrameMoveFocalPointButton::SetMoveInstantly(bool s)
{
	mMoveInstantly = s;
}


void iggFrameMoveFocalPointButton::OnButton1()
{
	iVector3D x = this->GetPosition();
	this->GetShell()->GetViewModule(this->GetWindowNumber())->FlyTo(x,mMoveInstantly?1.0e-20f:3.0f);
}


void iggFrameMoveFocalPointButton::OnButton2()
{
}

#endif
