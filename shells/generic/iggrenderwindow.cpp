/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/
  

#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggrenderwindow.h"


#include "irendertool.h"
#include "ishelleventobservers.h"
#include "iviewmodule.h"

#include "iggmainwindow.h"
#include "iggshell.h"

#include "ibgmainwindowsubject.h"
#include "ibgrenderwindowsubject.h"
#include "ibgwindowhelper.h"

#include <vtkMapper.h>

#include "iggsubjectfactory.h"

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"


using namespace iParameter;


iggRenderWindow::iggRenderWindow(iViewModule *vm) : iViewModuleComponent(vm)
{
	IASSERT(vm);

	mSubject = iggSubjectFactory::CreateRenderWindowSubject(this); IERROR_CHECK_MEMORY(mSubject);

	mInitialized = mResizing = mMoving = false;

	mMaxStep = mCurStep = 0;

	this->DoubleBufferOn();

	//
	//  Set intitial position to something off, so that the window is not considered placed into any
	//  specific point
	//
	this->Position[0] = this->Position[1] = 999999999;

	//
	//  Connect to Main Window
	//
	mMainWindow = 0;
	mTrueShell = iRequiredCast<iggShell>(INFO,this->GetViewModule()->GetShell());
	if(mTrueShell->GetMainWindow() !=0 ) this->AttachToMainWindow(mTrueShell->GetMainWindow());
}


iggRenderWindow::~iggRenderWindow()
{
	if(mMainWindow != 0)
	{
		mMainWindow->UnRegister(mSubject);
	}

	mSubject->Delete();
}


void iggRenderWindow::AttachToMainWindow(iggMainWindow *mw)
{
	if(mMainWindow==0 && mw!=0)
	{
		mMainWindow = mw;
		mMainWindow->Register(mSubject);
		mMainWindow->GetMainSubject()->AttachRenderWindow(this);
	}
}


void iggRenderWindow::Initialize()
{
	if(!mInitialized) 
	{
		mInitialized = true;

		this->MakeCurrent();
		this->OpenGLInit();

		mSubject->Initialize(this->GetOffScreenRendering()==0);

#ifdef I_NO_STEREO
		this->StereoCapableWindow = 0;
#else
#ifndef I_PORT_DEBIAN
		//
		//  Check if we have stereo
		//
		GLboolean ucTest;
		glGetBooleanv(GL_STEREO,&ucTest);
		this->StereoCapableWindow = (ucTest != 0) ? 1 : 0;
#else
		this->StereoCapableWindow = 1;
#endif
#endif
		this->Mapped = 1;
	}
}


void iggRenderWindow::Finalize()
{
	mSubject->Finalize();
}

//
//  Rendering process
//
void iggRenderWindow::Start() 
{
	// 
	// Initialize the QGLWidget part of the widget if it has not
	// been initialized so far. 
	//
	if(!mInitialized) this->Initialize();
}


void iggRenderWindow::Render()  
{
	if(!mInitialized) this->Initialize();

	if(this->GetOffScreenRendering() != 0)
	{
		this->RenderIntoMemory();
		return;
	}

	if(this->GetViewModule()->IsRenderingImage())
	{	
		//
		//  This way the magnified image creation do not show in actual window
		//
		if(!mSubject->IsReadyForDumpingImage()) return;
		this->vtkOpenGLRenderWindow::Render();
		if(mCurStep == 0) 
		{
			this->GetShell()->GetExecutionEventObserver()->Start();
			this->GetShell()->GetExecutionEventObserver()->SetLabel("Rendering");
			this->GetShell()->GetExecutionEventObserver()->SetInterval(0.0,1.0);
			mMaxStep = this->GetViewModule()->GetImageMagnification();
			mMaxStep *= mMaxStep;
		}
		this->GetShell()->GetExecutionEventObserver()->SetProgress(double(mCurStep)/mMaxStep);
		mCurStep++;
		if(mCurStep == mMaxStep)
		{
			this->GetShell()->GetExecutionEventObserver()->Finish();
			mCurStep = 0;
		}
	}
	else
	{
		this->vtkOpenGLRenderWindow::Render();
	}
}


void iggRenderWindow::Frame() 
{
	if(!this->AbortRender && this->DoubleBuffer && this->SwapBuffers) // VTK wants us to swap buffers
	{
		mSubject->SwapBuffers();
	}
}


void iggRenderWindow::MakeCurrent()
{
	mSubject->MakeCurrent();
}


//
//  Size and position
//
int* iggRenderWindow::GetPosition()
{
	//
	//  Update VTK positions
	//
	int wg[4];
	mSubject->GetWindowGeometry(wg);
	if(Position[0]!=wg[0] || Position[1]!=wg[1])
	{
		Position[0] = wg[0];
		Position[1] = wg[1];
		this->MTime.Modified(); // do not invoke observers
	}
	return this->vtkOpenGLRenderWindow::GetPosition();
}
	

int* iggRenderWindow::GetSize()
{
	//
	//  Update VTK size
	//
	int wg[4];
	mSubject->GetWindowGeometry(wg);
	if(Size[0]!=wg[2] || Size[1]!=wg[3])
	{
		Size[0] = wg[2];
		Size[1] = wg[3];
		this->MTime.Modified(); // do not invoke observers
	}
	return this->vtkOpenGLRenderWindow::GetSize();
}


void iggRenderWindow::SetPosition(int x, int y)
{
	if(mMoving) return;

	if(this->Position[0]!=x || this->Position[1]!=y)
	{
		int wg[4];
		mSubject->GetWindowGeometry(wg);
		wg[0] = x;
		wg[1] = y;
		mMoving = true;
		mSubject->SetWindowGeometry(wg);
		mMoving = false;
//		this->vtkOpenGLRenderWindow::SetPosition(x,y); in vtkWindow::SetPosition(...) this->Modified() is in the wrong place!!!
		this->Position[0] = x;
		this->Position[1] = y;
		this->Modified();
	}
}
	

void iggRenderWindow::SetSize(int w, int h)
{
	if(mResizing) return;

	if(this->Size[0]!=w || this->Size[1]!=h)
    {
		int wg[4];
		mSubject->GetWindowGeometry(wg);
		wg[2] = w;
		wg[3] = h;
		mResizing = true;
		mSubject->SetWindowGeometry(wg);
	    this->Size[0] = w;
		this->Size[1] = h;
		this->Modified();
		mResizing = false;
	}
}


void iggRenderWindow::SetBorders(int arg) 
{
	if(this->Borders==arg || !mInitialized) return;
	
	mSubject->SetBorder(arg != 0);
	if(arg != 0)
	{
		mSubject->GetHelper()->RestoreDecoration();
	}

	this->Borders = arg;
	this->Modified();
}


void iggRenderWindow::SetFullScreen(int arg) 
{
	//
	// We do not need to do anything if the FullScreen mode
	// is already set to the specified value. 
	//
	if(this->FullScreen==arg || !mInitialized) return;
	
	if(this->GetViewModule()->GetRenderTool()->GetFullScreenMode() > 0)
	{
		if(arg != 0) mSubject->GetHelper()->SaveWindowGeometry();
		mSubject->SetFullScreen(arg != 0);
		if(arg == 0)
		{
			mSubject->GetHelper()->RestoreWindowGeometry();
			mSubject->GetHelper()->RestoreDecoration();
		}

		this->FullScreen = arg;
		this->Modified();

		if(arg!=0 && this->GetViewModule()->GetRenderTool()->GetFullScreenMode()==2)
		{
			if(iRequiredCast<iggShell>(INFO,this->GetShell())->PopupWindow(this,"Is the window placement correct?",MessageType::Information,"Ok","Cancel") == 1)
			{
				mSubject->SetFullScreen(0);
				mSubject->GetHelper()->RestoreWindowGeometry();
				mSubject->GetHelper()->RestoreDecoration();
				this->FullScreen = 0;
				this->Modified();
			}
		}
	}
	else
	{
		iRequiredCast<iggShell>(INFO,this->GetShell())->PopupWindow(this,"Full screen mode is not permitted in this set-up.",MessageType::Error);
	}
}


int* iggRenderWindow::GetScreenSize()
{
	mTrueShell->GetDesktopDimensions(mScreenSize[0],mScreenSize[1]);
	return mScreenSize;
}


void* iggRenderWindow::GetGenericDisplayId()
{
	return mSubject->GetDisplay();
}


void* iggRenderWindow::GetGenericWindowId()
{
	return mSubject->GetWindowId();
}


void iggRenderWindow::SetWindowName(const char *s)
{
	if(mTrueShell->CheckCondition(Condition::SlowRemoteConnection))
	{
		iString ws(s);
		mSubject->SetWindowName(ws+" (click to render)");
	}
	else
	{
		mSubject->SetWindowName(s);
	}
}


void iggRenderWindow::RenderIntoMemory()
{
	//
	// This does not work under magnification yet. Or does it?
	//
	const int *size = this->GetSize();

	//
	//  QGLWidget::renderPixmap does nor render textures under Unix - force polygonal mode for xsections
	//
	this->GetViewModule()->ForceCrossSectionToUsePolygonalMethod(true);

	//
	//  Need to reset all existing display lists
	//
	int gim = vtkMapper::GetGlobalImmediateModeRendering();
	vtkMapper::GlobalImmediateModeRenderingOn();

	//
	//  render unto a pixmap
	//
	int db = this->DoubleBuffer;
	this->DoubleBuffer = 0; // Render into the front buffer

	iImage image;
	image.Scale(size[0],size[1]);

	mSubject->RenderIntoMemory(image);

	//
	//  Put them back so that Magnifier could read them
	//
	switch(image.Depth())
	{
	case 3:
		{
			this->SetPixelData(0,0,size[0]-1,size[1]-1,image.DataPointer(),!db);
			break;
		}
	case 4:
		{
			this->SetRGBACharPixelData(0,0,size[0]-1,size[1]-1,image.DataPointer(),!db);
			break;
		}
	default:
		{
			IBUG_ERROR("Unable to render off-screen for unknown reason.");
		}
	}
	this->DoubleBuffer = db;

	vtkMapper::SetGlobalImmediateModeRendering(gim);
	this->GetViewModule()->ForceCrossSectionToUsePolygonalMethod(false);
}


void iggRenderWindow::OnCloseAttempt()
{
	iRequiredCast<iggShell>(INFO,this->GetShell())->PopupWindow(this,"Render Window can not be closed. Use window controls in the main window to delete Render Windows.",MessageType::Warning);
}


bool iggRenderWindow::IsCurrent()
{
	return mSubject->IsCurrent();
}


int iggRenderWindow::IsDirect()
{
	return mSubject->IsDirect() ? 1 : 0;
}


void iggRenderWindow::SetStereoCapableWindow(int capable)
{
	if(!mInitialized)
	{
		mSubject->RequestStereo(capable != 0);
	}
	else
	{
		vtkWarningMacro(<< "Requesting a StereoCapableWindow must be performed before the window is realized, i.e. before a render.");
    }
}


void iggRenderWindow::UpdateActiveStatus(bool last)
{
	mSubject->UpdateActiveStatus(last);
}


bool iggRenderWindow::IsActiveWindow() const
{
	//return (mTrueShell->GetNumberOfViewModules()>1 && this->GetViewModule()->GetWindowNumber()==mTrueShell->GetActiveViewModuleIndex());
	return (this->GetViewModule()->GetWindowNumber() == mTrueShell->GetActiveViewModuleIndex());
}


//
//  Unused pure virtual overwrites
//
#define ERR \
	IBUG_FATAL("Misconfiguration error.\nYou are seeing this message because IFrIT was never tested on your window system.\nPlease send this message and the line number listed below to ifrit.bugs@gmail.com.")

void iggRenderWindow::SetDisplayId(void *)
{
	ERR;
}
void iggRenderWindow::SetWindowId(void *)
{
	ERR;
}
void iggRenderWindow::SetParentId(void *)
{
	ERR;
}
void* iggRenderWindow::GetGenericParentId()
{
	ERR;
	return 0;
}
void* iggRenderWindow::GetGenericContext()
{
	ERR;
	return 0;
}
void* iggRenderWindow::GetGenericDrawable()
{
	ERR;
	return 0;
}
void iggRenderWindow::SetWindowInfo(char *)
{
	ERR;
}
void iggRenderWindow::SetParentInfo(char *)
{
	ERR;
}
void iggRenderWindow::HideCursor()
{
	ERR;
}
void iggRenderWindow::ShowCursor()
{
	ERR;
}
void iggRenderWindow::WindowRemap()
{
	ERR;
}
int iggRenderWindow::GetEventPending()
{
	ERR;
	return 0;
}
void iggRenderWindow::SetNextWindowId(void *)
{
	ERR;
}
void iggRenderWindow::SetNextWindowInfo(char *)
{
	ERR;
}

void iggRenderWindow::CreateAWindow()
{
	ERR;
}
void iggRenderWindow::DestroyWindow()
{
	ERR;
}

#endif
