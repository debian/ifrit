/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggframedatatypewidgetlist.h"


#include "idata.h"
#include "idatareader.h"
#include "idatasubject.h"
#include "ishell.h"
#include "iviewmodule.h"

#include "iggwidget.h"
#include "iggwidgetarea.h"

//
//  Templates
//
#include "iarray.tlh"


iggFrameDataTypeWidgetList::iggFrameDataTypeWidgetList(const iString &title, iggFrame *parent) : iggFrame(title,parent)
{
	mBox = new iggFrameScroll(this);
	this->AddLine(mBox);
}


iggFrame* iggFrameDataTypeWidgetList::EntryParent() const
{
	return mBox->GetContents();
}


void iggFrameDataTypeWidgetList::UpdateList()
{
	int i;

	//
	//  This is expensive to update, make sure it does not update needlessly
	//
	if(!mNeedsUpdate) return;

	//
	//  This frame assumes that all readers have the same set of included DataType(s)
	//
	const iDataInfo &all(iDataInfo::Any());
	for(i=mEntries.Size(); i<all.Size(); i++)
	{
		mEntries.Add(this->CreateEntry(all.Type(i)));
		mBox->GetContents()->AddLine(mEntries.Last());
	}
}


void iggFrameDataTypeWidgetList::UpdateChildren()
{
	this->UpdateList();
	iggFrame::UpdateChildren();
}

#endif
