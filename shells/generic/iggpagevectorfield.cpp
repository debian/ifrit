/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggpagevectorfield.h"


#include "istretch.h"
#include "ivectorfieldviewsubject.h"

#include "iggframepaintingcontrols.h"
#include "iggframeobjectcontrols.h"
#include "igghandle.h"
#include "iggimagefactory.h"
#include "iggwidgetmisc.h"
#include "iggwidgetpropertycontrolbutton.h"
#include "iggwidgetpropertycontrolcolorselection.h"
#include "iggwidgetpropertycontrolselectionbox.h"

#include "ibgwidgetselectionboxsubject.h"

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"
#include "iggwidgetpropertycontrolslider.tlh"


using namespace iParameter;


namespace iggPageVectorField_Private
{
	class MethodRadioBox : public iggWidgetPropertyControlRadioBox
	{
		
	public:
		
		MethodRadioBox(const iggPropertyHandle& ph, iggFrame *parent) : iggWidgetPropertyControlRadioBox(1,"Streamline source",0,ph,parent)
		{
			mObjectBuddy = 0;

			this->InsertItem("Disk");
			this->InsertItem("Plane");
			this->InsertItem("Sphere");
			this->InsertItem("Markers");
		}

		void SetObjectBuddy(iggFrameObjectControls *b)
		{
			mObjectBuddy = b;
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			this->iggWidgetPropertyControlRadioBox::UpdateWidgetBody();
			this->UpdateBuddy();
		}
		
		virtual void OnInt1Body(int i)
		{
			this->UpdateBuddy();
			this->iggWidgetPropertyControlRadioBox::OnInt1Body(i);
		}

		virtual void UpdateBuddy()
		{
			if(mObjectBuddy == 0) return;

			switch(mSubject->GetValue())
			{
			case StreamLine::Source::Disk:
				{
					mObjectBuddy->EnablePosition(true);
					mObjectBuddy->EnableDirection(true);
					mObjectBuddy->EnableSize(true);
					break;
				}
			case StreamLine::Source::Plane:
				{
					mObjectBuddy->EnablePosition(true);
					mObjectBuddy->EnableDirection(true);
					mObjectBuddy->EnableSize(false);
					break;
				}
			case StreamLine::Source::Sphere:
				{
					mObjectBuddy->EnablePosition(true);
					mObjectBuddy->EnableDirection(false);
					mObjectBuddy->EnableSize(true);
					break;
				}
			case StreamLine::Source::Marker:
				{
					mObjectBuddy->EnablePosition(false);
					mObjectBuddy->EnableDirection(false);
					mObjectBuddy->EnableSize(false);
					break;
				}
			default:
				{
					IBUG_WARN("Incorrect button in MethodRadioBox.");
				}
			}
		}

		iggFrameObjectControls *mObjectBuddy;
	};


	//
	//  Main page
	// ************************************************
	//
	class MainPage : public iggMainPage2
	{

	public:

		MainPage(iggPageObject *owner, iggFrameBase *parent) : iggMainPage2(owner,parent,false)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		virtual void InsertSectionA(iggFrame *page)
		{
			//
			//  Method & color
			//
			iggFrame *mc = new iggFrame(page,2);
			iggWidgetPropertyControlRadioBox *mb = new iggWidgetPropertyControlRadioBox(1,"Method",0,mOwner->CreatePropertyHandle("Method"),mc);
			mb->InsertItem("Glyph");
			mb->InsertItem("Stream lines");
			mb->InsertItem("Stream tubes");
			mb->InsertItem("Strean bands");
			iggFrame *cf = new iggFrame(mc,2);
			cf->AddSpace(1);
			cf->AddLine(new iggWidgetPropertyControlColorSelection(mOwner->CreatePropertyHandle("Color"),cf));
			cf->AddSpace(10);
			cf->SetColStretch(1,1);
			mc->AddLine(mb,cf);
			mc->SetColStretch(1,3);

			page->AddLine(mc);
			page->AddSpace(10);

			//
			//  Glyph controls
			//
			mc = new iggFrame(page,2);
			mc->AddLine(new iggWidgetTextLabel("Glyph size",mc,-1),new iggWidgetPropertyControlFloatSlider(1.0e-3,10.0,40,iStretch::Log,0,true,false,"",mOwner->CreatePropertyHandle("GlyphSize"),mc));
			mc->AddLine(new iggWidgetTextLabel("Glyph opacity",mc,-1),new iggWidgetPropertyControlOpacitySlider("",mOwner->CreatePropertyHandle("Opacity"),mc));
			mc->SetColStretch(1,10);
			page->AddLine(mc,3);

			mc = new iggFrame(page,3);
			mc->AddLine(new iggWidgetTextLabel("Sample rate",mc,-1),new iggWidgetPropertyControlSpinBox(1,100,"",0,mOwner->CreatePropertyHandle("GlyphSampleRate"),mc));
			mc->AddLine(new iggWidgetTextLabel("Line width",mc,-1),new iggWidgetPropertyControlSpinBox(1,100,"",0,mOwner->CreatePropertyHandle("LineWidth"),mc));
			mc->AddLine(new iggWidgetTextLabel("Glyph base size",mc,-1),new iggWidgetPropertyControlSpinBox(1,100,"",0,mOwner->CreatePropertyHandle("GlyphBaseSize"),mc));
			mc->SetColStretch(2,3);
			page->AddLine(mc);
			page->AddSpace(10);

			page->SetColStretch(1,10);
			page->SetColStretch(2,3);
		}
	};


	//
	//  Paint page
	// ************************************************
	//
	class PaintPage : public iggPaintPage2
	{

	public:

		PaintPage(iggPageObject *owner, iggFrameBase *parent) : iggPaintPage2(owner,parent,iggPaintPage2::WithBrightness)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		virtual void InsertSectionA(iggFrame *page)
		{
			iggPropertyHandle ph(mOwner->CreatePropertyHandle("IsConnectedToScalars"));
			iggFramePaintingControls *pl = new iggFramePaintingControls("Paint with...",true,mOwner->GetScalarDataSubjectHandle(),mOwner->CreatePropertyHandle("PaintVar"),page,&ph);

			page->AddLine(pl);
			page->AddSpace(10);
		}
	};


	//
	//  Streamlines page
	// ************************************************
	//
	class StreamlinesPage : public iggPage2
	{

	public:

		StreamlinesPage(iggPageObject *owner, iggFrameBase *parent) : iggPage2(owner,parent,1)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		virtual void CompleteConstructionBody()
		{
			//
			//  Book
			//
			iggFrameBook *sb = new iggFrameBook(this);
			this->AddLine(sb);
			//
			//  Main page
			//
			iggFrame *sbpage0 = new iggFrame(sb,4);
			sb->AddPage("Main",mOwner->Icon(),sbpage0);
			MethodRadioBox *lt;
			{
				sbpage0->AddLine(new iggWidgetPropertyControlSpinBox(1,10000,"Number of streamlines",0,mOwner->CreatePropertyHandle("NumberOfStreamLines"),sbpage0));

				lt = new MethodRadioBox(mOwner->CreatePropertyHandle("SourceType"),sbpage0);

				iggWidgetPropertyControlRadioBox *ld = new iggWidgetPropertyControlRadioBox(2,"Streamline direction",0,mOwner->CreatePropertyHandle("LineDirection"),sbpage0);
				ld->InsertItem("Up stream");
				ld->InsertItem("Down stream");
				ld->InsertItem("Forward");
				ld->InsertItem("Backward");
				ld->InsertItem("Both ways");
				sbpage0->AddLine(lt,ld);
				sbpage0->AddSpace(5);

				iggWidgetPropertyControlFloatSlider *ll = new iggWidgetPropertyControlFloatSlider(1.0e-3,1.0,30,iStretch::Log,0,true,true,"Length",mOwner->CreatePropertyHandle("LineLength"),sbpage0);
				ll->SetStretch(3,10);
				iggWidgetPropertyControlIntSlider *lw = new iggWidgetPropertyControlIntSlider(1,10,0,"Width",mOwner->CreatePropertyHandle("TubeSize"),sbpage0);
				lw->SetStretch(3,10);
				sbpage0->AddLine(ll,3);
				sbpage0->AddLine(lw,3);
				sbpage0->AddSpace(10);

				iggFrame *ab = new iggFrame("Adjustments",sbpage0,1);
				iggWidgetPropertyControlIntSlider *lq = new iggWidgetPropertyControlIntSlider(1,7,0,"Line quality",mOwner->CreatePropertyHandle("LineQuality"),ab);
				lq->SetStretch(5,10);
				iggWidgetPropertyControlFloatSlider *tr = new iggWidgetPropertyControlFloatSlider(1.0,100.0,20,iStretch::Log,0,false,false,"Tube range",mOwner->CreatePropertyHandle("TubeRangeFactor"),ab);
				tr->SetStretch(5,10);
				iggWidgetPropertyControlFloatSlider *tv = new iggWidgetPropertyControlFloatSlider(1.0e-3,1.0,30,iStretch::Log,0,true,false,"Tube variation",mOwner->CreatePropertyHandle("TubeVariationFactor"),ab);
				tv->SetStretch(5,10);
				ab->AddLine(lq);
				ab->AddLine(tr);
				ab->AddLine(tv);
				sbpage0->AddLine(ab,3);

				sbpage0->AddSpace(20);
				sbpage0->SetColStretch(2,10);
				sbpage0->SetColStretch(3,3);
			}
			//
			//  Source page
			//
			iggFrame *sbpage1 = new iggFrame(sb,2);
			sb->AddPage("Source",mOwner->Icon(),sbpage1);
			{
				iggFrameObjectControls *sc = new iggFrameObjectControls("Source",mOwner->CreatePropertyHandle("SourcePosition"),mOwner->CreatePropertyHandle("SourceDirection"),mOwner->CreatePropertyHandle("SourceSize"),sbpage1);
				lt->SetObjectBuddy(sc);

				sbpage1->AddLine(sc);

				sbpage1->AddSpace(5);

				iggFrame *of = new iggFrame("Source object",sbpage1,1);
				iggWidgetPropertyControlCheckBox *sv = new iggWidgetPropertyControlCheckBox("Show",mOwner->CreatePropertyHandle("ShowSourceObject"),of);
				iggWidgetPropertyControlOpacitySlider *so = new iggWidgetPropertyControlOpacitySlider(mOwner->CreatePropertyHandle("SourceOpacity"),of);
				so->SetStretch(3,10);
				of->AddLine(sv);
				of->AddLine(so);
				sbpage1->AddLine(of);

				sbpage1->AddSpace(10);

				sbpage1->SetColStretch(0,10);
				sbpage1->SetColStretch(1,3);
			}
		}
	};
};


using namespace iggPageVectorField_Private;


iggPageVectorField::iggPageVectorField(iggFrameBase *parent) : iggPageObject(parent,"u",iggImageFactory::FindIcon("vect.png"))
{
	//
	//  Main page
	// ************************************************
	//
	mBook->AddPage("Main",this->Icon(),new MainPage(this,mBook));

	//
	//  Paint page
	// ************************************************
	//
	mBook->AddPage("Paint",this->Icon(),new PaintPage(this,mBook));

	//
	//  Streamlines page
	// ************************************************
	//
	mBook->AddPage("Streamlines",this->Icon(),new StreamlinesPage(this,mBook));

	//
	//  Replicate page
	// ************************************************
	//
	mBook->AddPage("Replicate",this->Icon(),new iggReplicatePage2(this,mBook,true));
}

#endif
