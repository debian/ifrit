/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggpagetensorfield.h"


#include "istretch.h"
#include "itensorfieldviewsubject.h"

#include "iggframepaintingcontrols.h"
#include "igghandle.h"
#include "iggimagefactory.h"
#include "iggwidgetmisc.h"
#include "iggwidgetpropertycontrolcolorselection.h"
#include "iggwidgetpropertycontrolselectionbox.h"


//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"
#include "iggwidgetpropertycontrolslider.tlh"


using namespace iParameter;


namespace iggPageTensorField_Private
{
	//
	//  Main page
	// ************************************************
	//
	class MainPage : public iggMainPage2
	{

	public:

		MainPage(iggPageObject *owner, iggFrameBase *parent) : iggMainPage2(owner,parent,false)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		virtual void InsertSectionA(iggFrame *page)
		{
			//
			//  Method & color
			//
			iggFrame *mc = new iggFrame(page,2);
			iggWidgetPropertyControlRadioBox *mb = new iggWidgetPropertyControlRadioBox(1,"Method",0,mOwner->CreatePropertyHandle("Method"),mc);
			mb->InsertItem("Glyph");
			iggFrame *cf = new iggFrame(mc,2);
			cf->AddSpace(1);
			cf->AddLine(new iggWidgetPropertyControlColorSelection(mOwner->CreatePropertyHandle("Color"),cf));
			cf->AddSpace(2);
			cf->SetColStretch(1,1);
			mc->AddLine(mb,cf);
			mb = new iggWidgetPropertyControlRadioBox(1,"Glyph type",0,mOwner->CreatePropertyHandle("GlyphType"),mc);
			mb->InsertItem("Cube");
			mb->InsertItem("Sphere (slow)");
			mc->AddLine(mb);
			mc->SetColStretch(1,3);

			page->AddLine(mc);
			page->AddSpace(10);

			//
			//  Glyph controls
			//
			mc = new iggFrame(page,2);
			mc->AddLine(new iggWidgetTextLabel("Glyph size",mc,-1),new iggWidgetPropertyControlFloatSlider(1.0e-3,10.0,40,iStretch::Log,0,true,false,"",mOwner->CreatePropertyHandle("GlyphSize"),mc));
			mc->AddLine(new iggWidgetTextLabel("Glyph opacity",mc,-1),new iggWidgetPropertyControlOpacitySlider("",mOwner->CreatePropertyHandle("Opacity"),mc));
			mc->SetColStretch(1,10);
			page->AddLine(mc,3);

			mc = new iggFrame(page,3);
			mc->AddLine(new iggWidgetTextLabel("Sample rate",mc,-1),new iggWidgetPropertyControlSpinBox(1,100,"",0,mOwner->CreatePropertyHandle("GlyphSampleRate"),mc));
			mc->SetColStretch(2,3);
			page->AddLine(mc);
			page->AddSpace(10);

			page->SetColStretch(1,10);
			page->SetColStretch(2,3);
		}
	};


	//
	//  Paint page
	// ************************************************
	//
	class PaintPage : public iggPaintPage2
	{

	public:

		PaintPage(iggPageObject *owner, iggFrameBase *parent) : iggPaintPage2(owner,parent,iggPaintPage2::WithBrightness)
		{
			if(this->ImmediateConstruction()) this->CompleteConstruction();
		}

	protected:

		virtual void InsertSectionA(iggFrame *page)
		{
			iggPropertyHandle ph(mOwner->CreatePropertyHandle("IsConnectedToScalars"));
			iggFramePaintingControls *pl = new iggFramePaintingControls("Paint with...",true,mOwner->GetScalarDataSubjectHandle(),mOwner->CreatePropertyHandle("PaintVar"),page,&ph);

			page->AddLine(pl,3);
			page->AddSpace(10);
		}
	};
};


using namespace iggPageTensorField_Private;

iggPageTensorField::iggPageTensorField(iggFrameBase *parent) : iggPageObject(parent,"t",iggImageFactory::FindIcon("tens.png"))
{
	//
	//  Main page
	// ************************************************
	//
	mBook->AddPage("Main",this->Icon(),new MainPage(this,mBook));

	//
	//  Paint page
	// ************************************************
	//
	mBook->AddPage("Paint",this->Icon(),new PaintPage(this,mBook));

	//
	//  Replicate page
	// ************************************************
	//
	mBook->AddPage("Replicate",this->Icon(),new iggReplicatePage2(this,mBook,true));
}

#endif
