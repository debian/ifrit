/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggdialogrenderingprogress.h"


#include "ierror.h"
#include "imath.h"

#include "iggframe.h"
#include "iggimagefactory.h"
#include "iggmainwindow.h"
#include "iggrenderwindow.h"
#include "iggwidgetarea.h"
#include "iggwidgetmisc.h"
#include "iggwidgetotherbutton.h"

#include <vtkTimerLog.h>

#include "iggsubjectfactory.h"


using namespace iParameter;


namespace iggDialogRenderingProgress_Private
{
	class CancelButton : public iggWidgetSimpleButton
	{

	public:

		CancelButton(iggDialogRenderingProgress *dialog, iggFrame *parent) : iggWidgetSimpleButton("Interrupt",parent)
		{
			mDialog = dialog;

			this->SetBaloonHelp("Interrupt rendering");
		}

	protected:

		virtual void Execute()
		{
			mDialog->Cancel();
		}

		iggDialogRenderingProgress *mDialog;
	};
};


using namespace iggDialogRenderingProgress_Private;


iggDialogRenderingProgress::iggDialogRenderingProgress(iggMainWindow *parent) : iggDialog(parent,DialogFlag::Blocking|DialogFlag::NoTitleBar,0,"Rendering...",0,1)
{
	mBlockParent = true;  // no explicit blocking required, since it is a semi-modal dialog
	mIsParentBlocked = false;

	mCancelled = true;
	mPreviousTime = 0.0;
	mTimer = vtkTimerLog::New(); IERROR_CHECK_MEMORY(mTimer);

	if(this->ImmediateConstruction()) this->CompleteConstruction();
}


void iggDialogRenderingProgress::CompleteConstructionBody()
{
	iggFrame*tmp;

	mFrame->AddLine(new iggWidgetTextLabel("%bRendering the scene...",mFrame));
	
	tmp = new iggFrame(mFrame,3);
	mElapsedTime = new iggWidgetTextLabel("0",tmp);
	tmp->AddLine(new iggWidgetTextLabel("Time elapsed: ",tmp),mElapsedTime,new iggWidgetTextLabel(" seconds",tmp));
	mFrame->AddLine(tmp);

	tmp = new iggFrame(mFrame,3);
	mFlipper = new iggWidgetLogoFlipper(tmp);
	tmp->AddLine(0,mFlipper);
	mFrame->AddLine(tmp);
	
	mButtonFrame = new iggFrame(mFrame,3);
	mButtonFrame->AddLine(0,new iggDialogRenderingProgress_Private::CancelButton(this,mButtonFrame));
	mFrame->AddLine(mButtonFrame);

	mRemarkFrame = new iggFrame(mFrame);
	mRemarkFrame->AddLine(new iggWidgetTextLabel("Interrupting all processes...",mRemarkFrame));
	mFrame->AddLine(mRemarkFrame);
}
	

iggDialogRenderingProgress::~iggDialogRenderingProgress()
{
	mTimer->Delete();
}


void iggDialogRenderingProgress::Start(bool interactive)
{
	if(!mCompleteConstructionCalled) this->CompleteConstruction();

	mTimer->StartTimer();
	mCancelled = false;
	mPreviousTime = 0.0;

	mInteractive = interactive;

	mElapsedTime->SetText("  0.0");
	mFlipper->Reset();
	mButtonFrame->Show(true);
	mRemarkFrame->Show(false);
}


void iggDialogRenderingProgress::KeepWorking()
{
	if(mCancelled) return;

	mTimer->StopTimer();
	double t = mTimer->GetElapsedTime();
	double dt = t - mPreviousTime;

	if(t > 0.1)
	{
		if(mBlockParent && !mIsParentBlocked)
		{
			mMainWindow->BLOCK(true);
			mIsParentBlocked = true;
		}
	}

	if(t>1.0 && dt>0.1 && !mInteractive)
    {
		if(!this->IsVisible()) this->Show(true);

		mFlipper->Advance();
		mElapsedTime->SetText(iString::FromNumber(t,"%6.1lf"));
		mPreviousTime = t;
	    mMainWindow->ProcessEvents();
    }
}


void iggDialogRenderingProgress::Cancel()
{
    mCancelled = true;

	mButtonFrame->Show(false);
	mRemarkFrame->Show(true);
}


void iggDialogRenderingProgress::Finish()
{
	mCancelled = false;
	this->Show(false);
	if(mBlockParent && mIsParentBlocked)
	{
		mMainWindow->BLOCK(false);
		mIsParentBlocked = false;
	}
}

bool iggDialogRenderingProgress::CanBeClosed()
{
	return false;  // make it non-closeable
}

#endif
