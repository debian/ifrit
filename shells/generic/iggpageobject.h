/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A page containing volume widgets
//

#ifndef IGGPAGEOBJECT_H
#define IGGPAGEOBJECT_H


#include "iggpagemain.h"


class iViewModule;
class iViewObject;
class iViewSubject;

class iggDataTypeHandle;
class iggIndexHandle;
class iggObjectHandle;
class iggPropertyHandle;


class iggPageObject : public iggPageMain
{

public:

	iggPageObject(iggFrameBase *parent, const iString &name, const iImage *icon);
	iggPageObject(const iString &instanceName, iggFrameBase *parent, const iString &name, const iImage *icon);
	virtual ~iggPageObject();

	void Show(bool s);
	virtual void SetActiveInstance(int n);
	int GetActiveInstance() const;

	iViewModule* GetViewModule(int mod) const;
	iViewObject* GetViewObject(int mod) const;
	iViewSubject* GetViewSubject(int mod) const;

	const iggObjectHandle* GetObjectHandle() const { return mObjectHandle; }
	const iggObjectHandle* GetSubjectHandle() const { return mSubjectHandle; }
	const iggObjectHandle* GetDataSubjectHandle() const { return mDataSubjectHandle; }
	const iggObjectHandle* GetScalarDataSubjectHandle() const { return mScalarDataSubjectHandle; }

	const iggIndexHandle* GetInstanceHandle() const { return mInstanceHandle; }

	const iggPropertyHandle CreatePropertyHandle(const iString &name) const;

	const iImage* Icon() const { return mIcon; }

	const iString& InstanceName() const { return mInstanceName; }

protected:

	virtual iggFrame* CreateFlipFrame(iggFrameBase *parent);

	iggObjectHandle *mObjectHandle, *mSubjectHandle, *mDataSubjectHandle, *mScalarDataSubjectHandle;
	iggIndexHandle *mInstanceHandle;
	iggDataTypeHandle *mDataTypeHandle;

private:

	void Define(const iString &name);

	const iImage *mIcon;
	int mObjectIndex;
	iString mInstanceName;
};


//
//  Second level pages - primarily to avoid code replication
//
class iggPage2 : public iggFrameDC
{

public:

	iggPage2(iggPageObject *owner, iggFrameBase *parent, int ncols);

protected:

	iggPageObject *mOwner;
};


class iggMainPage2 : public iggPage2
{

public:

	iggMainPage2(iggPageObject *owner, iggFrameBase *parent, bool withCreateDelete);

protected:

	virtual void CompleteConstructionBody();

	virtual void InsertSectionA(iggFrame *page) = 0;

	bool mWithCreateDelete;
};


class iggPaintPage2 : public iggPage2
{

public:

	const static int WithBrightness   = 1;
	const static int WithShading      = 2;
	const static int WithAllInstances = 4;
	iggPaintPage2(iggPageObject *owner, iggFrameBase *parent, int flags);

protected:

	virtual void CompleteConstructionBody();

	virtual void InsertSectionA(iggFrame *page) = 0;

	int mFlags;
};


class iggReplicatePage2 : public iggPage2
{

public:

	iggReplicatePage2(iggPageObject *owner, iggFrameBase *parent, bool voxelBox);

protected:

	virtual void CompleteConstructionBody();

	bool mWithVoxelBox;
};

#endif  // IGGPAGEOBJECT_H

