/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggwidgetmisc.h"


#include "imath.h"

#include "iggframe.h"
#include "iggimagefactory.h"

#include "ibgwidgetareasubject.h"
#include "ibgwidgetbuttonsubject.h"
#include "ibgwidgetentrysubject.h"
#include "ibgwidgetselectionboxsubject.h"

#include "iggsubjectfactory.h"


using namespace iParameter;


//
//  iggWidgetImageFlipper class
//
iggWidgetImageFlipper::iggWidgetImageFlipper(iggFrame *parent) : iggWidget(parent)
{
	mSubject = iggSubjectFactory::CreateWidgetMultiImageDisplayAreaSubject(this);
}


void iggWidgetImageFlipper::Advance()
{
	mSubject->Advance();
}


void iggWidgetImageFlipper::Reset()
{
	mSubject->ShowImage(0);
}


void iggWidgetImageFlipper::Start()
{
	mSubject->Start();
}


void iggWidgetImageFlipper::Abort()
{
	mSubject->Abort();
}


void iggWidgetImageFlipper::OnVoid1Body()
{
	mSubject->Advance();
}


void iggWidgetImageFlipper::UpdateWidgetBody()
{
}


//
//  iggWidgetLogoFlipper class
//
iggWidgetLogoFlipper::iggWidgetLogoFlipper(iggFrame *parent) : iggWidgetImageFlipper(parent)
{
	mSubject->AddImage(*iggImageFactory::FindIcon("genie1_frame_000.png"),false);
	mSubject->AddImage(*iggImageFactory::FindIcon("genie1_frame_001.png"),false);
	mSubject->AddImage(*iggImageFactory::FindIcon("genie1_frame_002.png"),false);
	mSubject->AddImage(*iggImageFactory::FindIcon("genie1_frame_003.png"),false);
	mSubject->AddImage(*iggImageFactory::FindIcon("genie1_frame_004.png"),false);
	mSubject->AddImage(*iggImageFactory::FindIcon("genie1_frame_005.png"),false);
	mSubject->AddImage(*iggImageFactory::FindIcon("genie1_frame_006.png"),false);
	mSubject->AddImage(*iggImageFactory::FindIcon("genie1_frame_007.png"),false);
	mSubject->AddImage(*iggImageFactory::FindIcon("genie1_frame_008.png"),false);
	mSubject->AddImage(*iggImageFactory::FindIcon("genie1_frame_009.png"),false);
	mSubject->AddImage(*iggImageFactory::FindIcon("genie1_frame_010.png"),false);
	mSubject->AddImage(*iggImageFactory::FindIcon("genie1_frame_011.png"),false);
	mSubject->AddImage(*iggImageFactory::FindIcon("genie1_frame_012.png"),false);
	mSubject->AddImage(*iggImageFactory::FindIcon("genie1_frame_013.png"),false);
	mSubject->AddImage(*iggImageFactory::FindIcon("genie1_frame_014.png"),false);

	mNeedsBaloonHelp = false;
}


//
//  NumberLabel widget
//
iggWidgetNumberLabel::iggWidgetNumberLabel(iggFrame *parent, int alignment) : iggWidget(parent)
{
	mSubject = iggSubjectFactory::CreateWidgetDisplayAreaSubject(this,"");
	this->SetAlignment(alignment);

	mNeedsBaloonHelp = false;
}


void iggWidgetNumberLabel::SetNumber(double v, const char *format)
{
	mSubject->SetText(iString::FromNumber(v,format));
}


void iggWidgetNumberLabel::SetNumber(int v, const char *format)
{
	mSubject->SetText(iString::FromNumber(v,format));
}


void iggWidgetNumberLabel::SetAlignment(int s)
{
	mSubject->SetAlignment(s);
}


void iggWidgetNumberLabel::UpdateWidgetBody()
{
	//
	//  Nothing to do here
	//
}


//
//  TextLabel widget
//
iggWidgetTextLabel::iggWidgetTextLabel(const iString &text, iggFrame *parent, int alignment) : iggWidget(parent)
{
	mSubject = iggSubjectFactory::CreateWidgetDisplayAreaSubject(this,text);
	this->SetAlignment(alignment);

	mNeedsBaloonHelp = false;
}


void iggWidgetTextLabel::SetText(const iString &text)
{
	mSubject->SetText(text);
}


void iggWidgetTextLabel::SetAlignment(int s)
{
	mSubject->SetAlignment(s);
}


void iggWidgetTextLabel::UpdateWidgetBody()
{
	//
	//  Nothing to do here
	//
}


//
//******************************************
//
//  ItemizedSelectionBox
//
//******************************************
//
iggWidgetSimpleItemizedSelectionBox::iggWidgetSimpleItemizedSelectionBox(iggFrame *parent) : iggWidget(parent)
{
}


//
//  iggWidgetSimpleRadioBox class
//
iggWidgetSimpleRadioBox::iggWidgetSimpleRadioBox(int cols, const iString &text, iggFrame *parent) : iggWidgetSimpleItemizedSelectionBox(parent)
{
	mSubject = iggSubjectFactory::CreateWidgetRadioBoxSubject(this,cols,text);
}


void iggWidgetSimpleRadioBox::UpdateWidgetBody()
{
}


int iggWidgetSimpleRadioBox::GetValue() const
{
	return mSubject->GetValue();
}


void iggWidgetSimpleRadioBox::SetValue(int v)
{
	mSubject->SetValue(v);
}


void iggWidgetSimpleRadioBox::InsertItem(const iString &text, int index)
{
	mSubject->InsertItem(text,index);
	mSubject->SetValue(index>=0?index:0);
}

void iggWidgetSimpleRadioBox::SetItem(const iString &text, int index, bool vis)
{
	mSubject->SetItem(text,index,vis);
}


void iggWidgetSimpleRadioBox::RemoveItem(int index)
{
	if(index>=0 && index<this->Count())	mSubject->RemoveItem(index);
}

int iggWidgetSimpleRadioBox::Count() const
{
	return mSubject->Count();
}


//
//  iggWidgetSimpleComboBox class
//
iggWidgetSimpleComboBox::iggWidgetSimpleComboBox(const iString &text, iggFrame *parent) : iggWidgetSimpleItemizedSelectionBox(parent)
{
	mSubject = iggSubjectFactory::CreateWidgetComboBoxSubject(this,text);
}


void iggWidgetSimpleComboBox::UpdateWidgetBody()
{
}


int iggWidgetSimpleComboBox::GetValue() const
{
	return mSubject->GetValue();
}


void iggWidgetSimpleComboBox::SetValue(int v)
{
	mSubject->SetValue(v);
}


void iggWidgetSimpleComboBox::InsertItem(const iString &text, int index)
{
	mSubject->InsertItem(text,index);
	mSubject->SetValue(index>=0?index:0);
}

void iggWidgetSimpleComboBox::SetItem(const iString &text, int index, bool vis)
{
	mSubject->SetItem(text,index,vis);
}


void iggWidgetSimpleComboBox::RemoveItem(int index)
{
	if(index>=0 && index<this->Count())	mSubject->RemoveItem(index);
}

int iggWidgetSimpleComboBox::Count() const
{
	return mSubject->Count();
}


//
//  iggWidgetSimpleSpinBox class
//
iggWidgetSimpleSpinBox::iggWidgetSimpleSpinBox(int min, int max, int step, const iString &text, iggFrame *parent) : iggWidget(parent)
{
	mSubject = iggSubjectFactory::CreateWidgetSpinBoxSubject(this,min,max,text,step);
}


void iggWidgetSimpleSpinBox::UpdateWidgetBody()
{
}


void iggWidgetSimpleSpinBox::SetValue(int v)
{
	mSubject->SetValue(v);
}


int iggWidgetSimpleSpinBox::GetValue() const
{
	return mSubject->GetValue();
}


void iggWidgetSimpleSpinBox::SetRange(int min,int max)
{
	mSubject->SetRange(min,max);
}

void iggWidgetSimpleSpinBox::SetStep(int step)
{
	mSubject->SetStep(step);
}


int iggWidgetSimpleSpinBox::Count()
{
	return mSubject->Count();
}


//
//  iggWidgetSimpleCheckBox class
//
iggWidgetSimpleCheckBox::iggWidgetSimpleCheckBox(const iString &title, iggFrame *parent) : iggWidget(parent)
{
	mSubject = iggSubjectFactory::CreateWidgetButtonSubject(this,ButtonType::CheckBox,title,1);
}


void iggWidgetSimpleCheckBox::SetChecked(bool s)
{
	mSubject->SetDown(s);
}


bool iggWidgetSimpleCheckBox::IsChecked() const
{
	return mSubject->IsDown();
}


void iggWidgetSimpleCheckBox::UpdateWidgetBody()
{
}


void iggWidgetSimpleCheckBox::OnVoid1Body()
{
	this->OnChecked(mSubject->IsDown());
}

#endif
