/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A base class for generic dialogs (toolkit-independent)
//

#ifndef IGGDIALOG_H
#define IGGDIALOG_H


#include "iggrenderingelement.h"


#include "iarray.h"


class iImage;
class iString;

struct iHelpData;
class iHelpDataBuffer;

class iggFrame;
class iggMainWindow;
class iggMenuWindow;

class ibgDialogSubject;
class ibgWindowSubject;


namespace iParameter
{
	//
	//  Dialog modes masks
	//
	namespace DialogFlag
	{
		const unsigned int Blocking = 1U;
		const unsigned int WithEventLoop = 2U;
		const unsigned int Modal = 3U;
		const unsigned int NoTitleBar = 4U;
		const unsigned int Unattached = 8U;
	};
};


class iggDialog : public iggRenderingElement
{

	friend class iggSubjectFactory;

public:

	virtual ~iggDialog();

	inline ibgDialogSubject* GetSubject() const { return mSubject; }
	inline iggFrame* GetFrame() const { return mFrame; }

	void Show(bool s);
	bool IsVisible() const;
	void ResizeContents(int w, int h);

	void Close();
	virtual void UpdateDialog();

	iggMainWindow* GetMainWindow() const { return mMainWindow; }

	//
	//  Provides help for this dialog
	//
	virtual const iString& GetToolTip() const;
	iString GetBaloonHelp() const;

#ifdef I_DEBUG
	static void FlashAllDialogs();
#endif

protected:

	iggDialog(iggShell *shell, unsigned int mode, const iImage *icon, const iString &title, const char *helpkey, int cols, const char *closetext = "");
	iggDialog(const iggMenuWindow *mw, unsigned int mode, const iImage *icon, const iString &title, const char *helpkey, int cols, const char *closetext = "");
	iggDialog(const iggDialog *pd, unsigned int mode, const iImage *icon, const iString &title, const char *helpkey, int cols, const char *closetext = "");

	virtual void ShowBody(bool s);
	virtual bool CanBeClosed();
	void AttachSubject(ibgDialogSubject *subject); //  this is a dummy for factory use

	bool ImmediateConstruction() const; 
	void CompleteConstruction();
	virtual void CompleteConstructionBody() = 0;

	bool mCompleteConstructionCalled;
	ibgDialogSubject *mSubject;
	iggMainWindow *mMainWindow;
	iggFrame *mFrame;
	unsigned int mMode;
	iHelpDataBuffer *mHelp;

private:

	void Define(const ibgWindowSubject *base, unsigned int mode, const iImage *icon, const iString &title, const char *helpkey, int cols, const char *closetext);

	//
	//  Registry
	//
	static iLookupArray<iggDialog*>& List();

	iggDialog(const iggDialog&); // Not implemented.
	void operator=(const iggDialog&);  // Not implemented.
};

#endif  // IGGDIALOG_H

