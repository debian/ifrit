/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggwidget.h"


#include "icolor.h"
#include "ierror.h"
#include "imath.h"

#include "iggframe.h"
#include "iggshell.h"

#include "ibgwidgetsubject.h"
#include "ibgwidgethelper.h"

#include <vtkMath.h>

//
//  Templates
//
#include "iarray.tlh"


using namespace iParameter;


//
//  Main class
//
iggWidget::iggWidget(iggFrameBase *parent) : iggRenderingElement(parent?parent->GetShell():0)
{
	IASSERT(parent);
	mParent = parent;

	//
	//  Register with the parent
	//
	mParent->RegisterChild(this);

	this->Define();
}


iggWidget::iggWidget(iggShell *shell) : iggRenderingElement(shell)
{
	mParent = 0;

	this->Define();
}


void iggWidget::Define()
{
	mWasLaidOut = mHelpIsSet = mInUpdate = false;
	mNeedsBaloonHelp = true;  //  used for checking internal consistency
	mSubjectOwner = true;  // owns its subject
	mMainWindow = this->GetShell()->GetMainWindow();

	mWidgetSubject = 0;
	mWidgetHelper = 0;

	mInitialized = mDisableDepedentUpdate = false;

	List().Add(this);
}


iggWidget::~iggWidget()
{
	List().Remove(this);

	if(!this->CheckBaloonHelpStatus())
	{
		IBUG_WARN("Context-sensitive help was not set.");
	}

	if(!mWasLaidOut)
	{
		IBUG_WARN("This widget was not added to a layout.");
	}

	while(mBuddies.Size() > 0) this->RemoveBuddy(mBuddies.Last());  // remove buddies first
	while(mDependents.Size() > 0) this->RemoveDependent(mDependents.Last());  // remove other dependents
	if(mParent != 0) mParent->UnRegisterChild(this);

	if(mSubjectOwner)
	{
		IASSERT(mWidgetSubject);
		IASSERT(mWidgetHelper);
		delete mWidgetSubject;
	}
}


void iggWidget::UpdateWidget()
{
	//
	//  Avoid cyclic updates
	//
	if(mInUpdate) return;
	mInUpdate = true;
	this->UpdateWidgetBody();
	mInUpdate = false;
}


iLookupArray<iggWidget*>& iggWidget::List()
{
	static iLookupArray<iggWidget*> list(100);
	return list;
}


void iggWidget::AttachSubject(ibgWidgetSubject *subject)
{
	IASSERT(subject);
	mWidgetSubject = subject;
	mWidgetHelper = mWidgetSubject->mWidgetHelper;
	IASSERT(mWidgetHelper);
}


void iggWidget::AddBuddy(iggWidget *buddy)
{
	if(buddy!=0 && mBuddies.Find(buddy)==-1)
	{
		this->AddBuddyBody(buddy); // this is to avoid infinite loop
		buddy->AddBuddyBody(this);
	}
}


void iggWidget::RemoveBuddy(iggWidget *buddy)
{
	if(buddy!=0 && mBuddies.Find(buddy)>-1)
	{
		this->RemoveBuddyBody(buddy); // this is to avoid infinite loop
		buddy->RemoveBuddyBody(this);
	}
}


void iggWidget::AddBuddyBody(iggWidget *buddy)
{
	mBuddies.Add(buddy);
	this->AddDependent(buddy);
}


void iggWidget::RemoveBuddyBody(iggWidget *buddy)
{
	mBuddies.Remove(buddy);
	this->RemoveDependent(buddy);
}


void iggWidget::AddDependent(iggWidget *w)
{
	mDependents.AddUnique(w);
}


void iggWidget::RemoveDependent(iggWidget *w)
{
	mDependents.Remove(w);
}


//
//  Update all buddies
//
void iggWidget::UpdateDependents()
{
	if(mDisableDepedentUpdate) return;

	int i;
	for(i=0; i<mDependents.Size(); i++) //if(mDependents[i]->IsVisible())
	{
		mDependents[i]->UpdateWidget();
	}
}


void iggWidget::UpdateFailed() const
{
	//
	//  This is for debugging purposes only
	//
#ifdef I_DEBUG
	IBUG_WARN("Update failed.");
#endif
}


void iggWidget::SetBaloonHelp(const iString &help)
{
	this->SetBaloonHelp(help,help+".");
}


void iggWidget::SetBaloonHelp(const iString &tooltip, const iString &whatsthis)
{
	if(mWidgetHelper == 0) IBUG_FATAL("WidgetHelper has not been set.");

	if(mWidgetHelper->SupportsHTMLHelp())
	{
		mWidgetHelper->SetBaloonHelp(tooltip,whatsthis);
	}
	else
	{
		iString ws = whatsthis;
		ws.ReformatHTMLToText();
		mWidgetHelper->SetBaloonHelp(tooltip,ws);
	}
	mHelpIsSet = true;
}


bool iggWidget::CheckBaloonHelpStatus()
{
	if(mWidgetHelper == 0) IBUG_FATAL("WidgetHelper has not been set.");

	return !mNeedsBaloonHelp || !mSubjectOwner || mWidgetHelper->HasBaloonHelp();
}


void iggWidget::Show(bool s)
{
	if(mWidgetHelper == 0) IBUG_FATAL("WidgetHelper has not been set.");

	mWidgetHelper->Show(s);
}


bool iggWidget::IsVisible() const
{
	if(mWidgetHelper == 0) IBUG_FATAL("WidgetHelper has not been set.");

	return mWidgetHelper->IsVisible();
}


void iggWidget::Enable(bool s)
{
	if(mWidgetHelper == 0) IBUG_FATAL("WidgetHelper has not been set.");

	mWidgetHelper->Enable(s);
}


bool iggWidget::IsEnabled() const
{
	if(mWidgetHelper == 0) IBUG_FATAL("WidgetHelper has not been set.");

	return mWidgetHelper->IsEnabled();
}


void iggWidget::Emphasize(bool s, bool random)
{
	static const iColor emp(255,50,50);

	if(mWidgetHelper == 0) IBUG_FATAL("WidgetHelper has not been set.");

	if(s)
	{
		mWidgetHelper->SetBackgroundColor(random?iColor(iMath::Round2Int(127+128*vtkMath::Random()),iMath::Round2Int(127+128*vtkMath::Random()),iMath::Round2Int(127+128*vtkMath::Random())):emp);
	}
	else
	{
		mWidgetHelper->UnSetBackgroundColor();
	}
}


#ifdef I_DEBUG
void iggWidget::EmphasizeLayouts(bool s)
{
	int i;
	iLookupArray<iggWidget*>& list(List());
	for(i=0; i<list.Size(); i++)
	{
		list[i]->Emphasize(s,true);
	}
}
#endif

#endif
