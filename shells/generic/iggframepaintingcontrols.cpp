/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggframepaintingcontrols.h"


#include "ierror.h"

#include "iggframedatavariablelimits.h"
#include "iggframedatavariablelist.h"
#include "igghandle.h"


#ifdef I_DEBUG
#include "iggwidgetotherbutton.h"
namespace iggFramePaintingControls_Private
{
	class Toggle: public iggWidgetSimpleButton
	{

	public:

		Toggle(iggFrameDataVariableList *owner, iggFrame *parent) : iggWidgetSimpleButton("",parent,true)
		{
			mOwner = owner;
		}

	protected:

		virtual void Execute()
		{
			mOwner->ShowLayer((1+mOwner->GetActiveLayer())%2);
		}

		iggFrameDataVariableList *mOwner;
	};
};

using namespace iggFramePaintingControls_Private;
#endif

iggFramePaintingControls::iggFramePaintingControls(const iString &title, bool withnone, const iggObjectHandle *dsh, const iggPropertyHandle& ph, iggFrame *parent, const iggPropertyHandle* eh) : iggFrame(parent,2)
{
	IASSERT(dsh);

	iggFrameDataVariableList *list = new iggFrameDataVariableList(title,dsh,ph,this,eh);
	if(withnone) list->InsertItem("None");
	list->Complete();

	iggFrameDataVariableLimits *lims = new iggFrameDataVariableLimits(ph,dsh,this,eh,true);
	list->AddDependent(lims);
	this->AddLine(list,lims);
#ifdef I_DEBUG
	//this->AddLine(new Toggle(list,this));
#endif
	lims->Show(false);

	this->SetColStretch(0,0);
	this->SetColStretch(1,10);
}

#endif
