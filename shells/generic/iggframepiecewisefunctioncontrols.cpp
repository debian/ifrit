/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggframepiecewisefunctioncontrols.h"


#include "ierror.h"
#include "imath.h"
#include "ipiecewisefunction.h"

#include "iggframehistogramcontrols.h"
#include "iggwidgetarea.h"
#include "iggwidgetotherbutton.h"

#include "ibgwidgetareasubject.h"

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"


using namespace iParameter;


namespace iggFramePiecewiseFunctionControls_Private
{
	//
	//  Helper classes
	//
	class RenderButton;

	class Area : public iggWidgetHistogramArea
	{

		friend class AddPointButton;
		friend class RemovePointButton;
		friend class ResetButton;
		friend class RenderButton;

	public:

		Area(iggFrameHistogramControls *controls, iggFramePiecewiseFunctionControls *parent) : iggWidgetHistogramArea(controls,parent)
		{
			mFrame = parent;

			mCurPoint = 0;
			mRenderButton = 0;
			mDefaultFunction = 0;

			mNeedsBaloonHelp = false;
			mSubject->SetMinimumSize(200,200);
		}

		~Area()
		{
			if(mDefaultFunction != 0) mDefaultFunction->Delete();
		}

		void SetRenderButton(RenderButton *b)
		{
			mRenderButton = b;
		}

	protected:

		virtual void UpdateWidgetBody()
		{
			if(mDefaultFunction == 0) 
			{
				mDefaultFunction = iPiecewiseFunction::New(); IERROR_CHECK_MEMORY(mDefaultFunction);
				mDefaultFunction->Copy(mFrame->GetFunction());
			}

			if(mFrame->GetFunction() == 0) this->UpdateFailed();

			this->iggWidgetHistogramArea::UpdateWidgetBody();
		}

		virtual void DrawBackgroundBody()
		{
			if(mControls != 0)
			{
				this->DrawHistogram(mFrame->GetHistogram(mControls->GetStretchId(),mControls->GetFullResolution()));
			}
		}

		virtual void DrawForegroundBody()
		{
			const int sym = 10;
			static const iColor regColor = iColor(0,0,0);
			static const iColor curColor = iColor(255,0,0);
			int i, ix, iy, ixo, iyo, ixmax, iymax;

			iPiecewiseFunction *fun = mFrame->GetFunction();
			if(fun != 0) 
			{
				ixmax = mSubject->Width() - 1;
				iymax = mSubject->Height() - 1;
				
				if(mCurPoint >= fun->N()) mCurPoint = fun->N() - 1;

				ixo = iyo = -1;
				for(i=0; i<fun->N(); i++)  
				{
					ix = iMath::Round2Int(ixmax*fun->X(i));
					iy = iMath::Round2Int(iymax*fun->Y(i));
					if(ixo>=0 && iyo >=0) mSubject->DrawLine(ixo,iyo,ix,iy,regColor);
					if(i != mCurPoint) mSubject->DrawEllipse(ix-sym/2,iy-sym/2,ix+sym/2,iy+sym/2,regColor,regColor);
					ixo = ix; iyo = iy;
				}
				
				ix = iMath::Round2Int(ixmax*fun->X(mCurPoint));
				iy = iMath::Round2Int(iymax*fun->Y(mCurPoint));
				mSubject->DrawEllipse(ix-sym/2,iy-sym/2,ix+sym/2,iy+sym/2,regColor,curColor);
			}
			else
			{
				IBUG_WARN("Unable to query PiecewiseFunctionControls.");
			}
		}
		
		void OnAddPoint()
		{
			iPiecewiseFunction *fun = mFrame->GetFunction();
			if(fun != 0) 
			{
				fun->AddPoint(mCurPoint);
				if(mCurPoint < fun->N()-1) mCurPoint++;
				mSubject->RequestPainting(DrawMode::Foreground);
				mFrame->OnChange();
			}
			this->OnRender();
		}

		void OnRemovePoint()
		{
			iPiecewiseFunction *fun = mFrame->GetFunction();
			if(fun != 0) 
			{
				fun->RemovePoint(mCurPoint);
				if(mCurPoint > 0) mCurPoint--;
				mSubject->RequestPainting(DrawMode::Foreground);
				mFrame->OnChange();
			}
			this->OnRender();
		}

		void OnRender();

		void OnMousePress(int x, int y, int b)
		{
			this->OnMouseMove(x,y,b);
		}

		void OnMouseMove(int x, int y, int b); // defined below

		int mCurPoint;
		RenderButton *mRenderButton;
		iPiecewiseFunction *mDefaultFunction;
		iggFramePiecewiseFunctionControls *mFrame;
	};	

	//
	//  AddPointButton
	//
	class AddPointButton : public iggWidgetSimpleButton
	{

	public:

		AddPointButton(Area *area, iggFrame *parent) : iggWidgetSimpleButton("Add point",parent)
		{
			mArea= area;
			this->SetBaloonHelp("Add new control point");
		}

	protected:

		virtual void Execute()
		{
			mArea->OnAddPoint();
		}

		Area *mArea;
	};

	//
	//  RemovePointButton
	//
	class RemovePointButton : public iggWidgetSimpleButton
	{

	public:

		RemovePointButton(Area *area, iggFrame *parent) : iggWidgetSimpleButton("Remove point",parent)
		{
			mArea= area;
			this->SetBaloonHelp("Remove current (red) control point");
		}

	protected:

		virtual void Execute()
		{
			mArea->OnRemovePoint();
		}

		Area *mArea;
	};

	//
	//  RenderButton
	//
	class RenderButton : public iggWidgetSimpleButton
	{

	public:

		RenderButton(Area *area, iggFrame *parent) : iggWidgetSimpleButton("Render",parent)
		{
			mArea= area;
			this->SetBaloonHelp("Render the scene");
			this->Enable(false);
		}

	protected:

		virtual void Execute()
		{
			mArea->OnRender();
		}

		Area *mArea;
	};

	void Area::OnMouseMove(int x, int y, int b)
	{
		const int sym = 16;
		int i;
		float d;
		iPair p;

		iPiecewiseFunction *fun = mFrame->GetFunction();
		if(fun != 0) 
		{
			p.X = float(x)/mSubject->Width();
			p.Y = float(y)/mSubject->Height();
			
			if(b==MouseButton::LeftButton || b==MouseButton::RightButton) 
			{
				i = fun->FindPoint(p,d);
				if(iMath::Round2Int(d*mSubject->Width()) < 5*sym)
				{
					mCurPoint = i;
					fun->MovePoint(mCurPoint,p);
					mSubject->RequestPainting(DrawMode::Foreground);
					if(b == MouseButton::RightButton) this->OnRender(); else if(mRenderButton!=0 && !mRenderButton->IsEnabled()) mRenderButton->Enable(true);
					mFrame->OnChange();
				}
			}
		}
	}

	void Area::OnRender()
	{
		mFrame->Render();
		if(mRenderButton != 0)
		{
			mRenderButton->Enable(false);
		}
	}
};


using namespace iggFramePiecewiseFunctionControls_Private;


iggFramePiecewiseFunctionControls::iggFramePiecewiseFunctionControls(bool withRender, bool withHistogram, iggFrame *parent) : iggFrame(parent,3)
{
	Area *a;
	if(withHistogram)
	{
		iggFrameHistogramControls *controls = new iggFrameHistogramControls(true,false,this);
		a = new Area(controls,this);
		this->AddLine(a,3);
		this->AddLine(controls,3);
		this->AddSpace(1);
	}
	else
	{
		a = new Area(0,this);
		this->AddLine(a,3);
	}
	this->SetRowStretch(0,10);

	RenderButton *rb = 0;
	if(withRender)
	{
		rb = new RenderButton(a,this);
		a->SetRenderButton(rb);
	}

	this->AddLine(new AddPointButton(a,this),(iggWidget*)0,rb);
	this->AddLine(new RemovePointButton(a,this));
	this->AddSpace(10);
}

#endif
