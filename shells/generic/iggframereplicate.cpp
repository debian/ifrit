/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "iggframereplicate.h"


#include "idatalimits.h"
#include "idatasubject.h"
#include "ierror.h"
#include "iviewsubject.h"
#include "ishell.h"

#include "igghandle.h"
#include "iggwidgetmisc.h"
#include "iggwidgetpropertycontrolselectionbox.h"


iggFrameReplicate::iggFrameReplicate(const iggObjectHandle *oh, iggFrame *parent) : iggFrame("Periodic extensions",parent,2), mObjectHandle(oh)
{
	iggWidgetPropertyControlSpinBox *box1, *box2;

	iggFrame *tmp = new iggFrame(this,2);
	tmp->AddLine(new iggWidgetTextLabel("       Down",tmp),new iggWidgetTextLabel("Up",tmp));
	this->AddLine(tmp);

	mDirs[0] = new iggFrame(this,3);
	box1 = new iggWidgetPropertyControlSpinBox(0,2,"",0,iggPropertyHandle("ReplicationFactors",oh,0),mDirs[0]);
	box2 = new iggWidgetPropertyControlSpinBox(0,2,"",0,iggPropertyHandle("ReplicationFactors",oh,1),mDirs[0]);
	mDirs[0]->AddLine(new iggWidgetTextLabel("%b%+X",mDirs[0]),box1,box2);
	this->AddLine(mDirs[0]);

	mDirs[1] = new iggFrame(this,3);
	box1 = new iggWidgetPropertyControlSpinBox(0,2,"",0,iggPropertyHandle("ReplicationFactors",oh,2),mDirs[1]);
	box2 = new iggWidgetPropertyControlSpinBox(0,2,"",0,iggPropertyHandle("ReplicationFactors",oh,3),mDirs[1]);
	mDirs[1]->AddLine(new iggWidgetTextLabel("%b%+Y",mDirs[1]),box1,box2);
	this->AddLine(mDirs[1]);

	mDirs[2] = new iggFrame(this,3);
	box1 = new iggWidgetPropertyControlSpinBox(0,2,"",0,iggPropertyHandle("ReplicationFactors",oh,4),mDirs[2]);
	box2 = new iggWidgetPropertyControlSpinBox(0,2,"",0,iggPropertyHandle("ReplicationFactors",oh,5),mDirs[2]);
	mDirs[2]->AddLine(new iggWidgetTextLabel("%b%+Z",mDirs[2]),box1,box2);
	this->AddLine(mDirs[2]);

	this->SetColStretch(1,10);
}


void iggFrameReplicate::UpdateChildren()
{
	this->iggFrame::UpdateChildren();
	
	const iViewSubject *sub = iDynamicCast<const iViewSubject>(INFO,mObjectHandle->Object());
	
	int i;
	for(i=0; i<3; i++)
	{
		mDirs[i]->Enable(sub->GetSubject()->IsDirectionPeriodic(i));
	}
}

#endif
