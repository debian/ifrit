/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A GUI widget that controls a single property
//

#ifndef IGGWIDGETPROPERTYCONTROL_H
#define IGGWIDGETPROPERTYCONTROL_H


#include "iggwidget.h"


#include "itype.h"

template<class T> class iLookupArray;
class iObject;
class iProperty;
class iPropertyVariable;

class iggFrame;
class iggPropertyHandle;


namespace iParameter
{
	namespace RenderMode
	{
		const int Ignore = 0;
		const int Delayed = 1;
		const int Immediate = 2;
		const int DoNotRender = 3;
	};

	namespace ExecuteFlag
	{
	    namespace Index
        {
                const int Mask          = 0xF0;
                const int One           = 0x10;
                const int All           = 0x20;
        };

		namespace Module
        {
                const int Mask          = 0x0F;
                const int One           = 0x01;
                const int All           = 0x02;
                const int Clones        = 0x03;
        };

        const int Default = Index::One | Module::One;
	};
};

//
//  Non-template base class
//
class iggWidgetPropertyControlBase : public iggWidget
{

	friend class iggFrame;
	friend class iggFrameBoxSize;

public:

	virtual bool CheckBaloonHelpStatus();

	int GetRenderMode() const;
	virtual void SetRenderMode(int m);

	int GetExecuteFlags() const;
	virtual void SetExecuteFlags(int f);
	static int GetGlobalExecuteFlags(){ return mGlobalExecuteFlags; }
	static void SetGlobalExecuteFlags(int f);

protected:

	iggWidgetPropertyControlBase(const iggPropertyHandle& ph, iggFrame *parent, int rm);
	virtual ~iggWidgetPropertyControlBase();

	inline const iggPropertyHandle* Handle() const { return mHandle; }

	void UpdateHelp();

	//
	//  Registry functionality
	//
	static iLookupArray<iggWidgetPropertyControlBase*>& VariableLimitsList();
	static iLookupArray<iggWidgetPropertyControlBase*>& PositionList();
	static iLookupArray<iggWidgetPropertyControlBase*>& List();

private:

	void Define(int rm);

	const iggPropertyHandle* mHandle;

	int mRenderMode;     // RenderMode should be accessed via GetRenderMode always
	int mExecuteFlags; // ExecureFlags should be accessed via GetExecuteFlags always
	static int mGlobalExecuteFlags;
};


template <iType::TypeId id>
class iggWidgetPropertyControl : public iggWidgetPropertyControlBase
{

public:

	typedef typename iType::Traits<id>::Type			ArgT;
	typedef typename iType::Traits<id>::ContainerType	ValT;

	inline ArgT GetValue() const { return mValue; }

	virtual bool ExecuteControl(bool final);
	virtual bool ExecuteControl(bool final, ArgT val);

protected:

	iggWidgetPropertyControl(const iggPropertyHandle& ph, iggFrame *parent, int rm);

	//
	//  Fine-grain customization
	//
	virtual bool SetPropertyValue(const iProperty *prop, int i, ArgT val) const;
	virtual ArgT GetPropertyValue(const iProperty *prop, int i) const;

	virtual void UpdateWidgetBody();
	virtual void QueryValue(ValT &v) const = 0;
	virtual void UpdateValue(ArgT v) = 0;

	ValT mValue;
};

//
//  This is a dummy invisible widget, which is controlled by SetValue and GetValue functions
//
template <iType::TypeId id>
class iggWidgetPropertyControlDummy : public iggWidgetPropertyControl<id>
{

public:

	typedef typename iType::Traits<id>::Type			ArgT;
	typedef typename iType::Traits<id>::ContainerType	ValT;

	iggWidgetPropertyControlDummy(const iggPropertyHandle& ph, iggFrame *parent, int rm);

	void SetValue(ArgT v);

protected:

	virtual void QueryValue(ValT &v) const;
	virtual void UpdateValue(ArgT v);
};

#endif  // IGGWIDGETPROPERTYCONTROL_H

