/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_CL)


#include "iclviewmoduleeventobservers.h"


#include "idataformatter.h"
#include "ierror.h"
#include "ipicker.h"
#include "iviewmodule.h"

#include "iclshell.h"

#include <vtkTimerLog.h>


//
//  iclRenderEventObserver class
//
iclRenderEventObserver::iclRenderEventObserver(iViewModule *vm) : iRenderEventObserver(vm)
{
	mStarted = false;
}


iclRenderEventObserver::~iclRenderEventObserver()
{
}


bool iclRenderEventObserver::CheckAbort()
{
	if(this->GetViewModule()->GetShell()->IsRunning())
	{
		this->Timer()->StopTimer();
		if(this->Timer()->GetElapsedTime() > 2.0) mStarted = true;
	}

	if(mStarted)
	{
		cout << "\rRendering...  ";
		switch(wSymbol)
		{
		case 0:
			{
				cout << "|";
				break;
			}
		case 1:
			{
				cout << "/";
				break;
			}
		case 2:
			{
				cout << "-";
				break;
			}
		case 3:
			{
				cout << "\\";
				break;
			}
		}
		wSymbol++; if(wSymbol > 3) wSymbol = 0;
		return false;
	}
	else return false;
}


void iclRenderEventObserver::OnStart()
{
	wSymbol = 0;
	mStarted = false;
}


void iclRenderEventObserver::OnFinish()
{
	if(mStarted)
	{
		mStarted = false;
		cout << "\bdone." << endl;
	}

	iclShell *shell = iRequiredCast<iclShell>(INFO,this->GetViewModule()->GetShell());
	if(shell->GetTrace())
	{
		iString str;

		if(shell->GetNumberOfViewModules() > 1)
		{
			str += "window #" + iString::FromNumber(this->GetViewModule()->GetWindowNumber()) + ": ";
		}
		str += "render time: " + iString::FromNumber(this->GetRenderTime()) + " secs.";

		cout << str.ToCharPointer() << endl;
	}
}


void iclRenderEventObserver::PostFinished()
{
	this->OnFinish();
}

//
//  iclPickEventObserver class
//
void iclPickEventObserver::OnStart()
{
}


void iclPickEventObserver::OnFinish()
{
	iPicker *picker = this->GetViewModule()->GetPicker();
	if(picker->GetObjectName().IsEmpty())
	{
		//
		//  Nothing is picked.
		//
		return;
	}

	cout << "Picker info:" << endl;

	cout << "Object: " << picker->GetObjectName().ToCharPointer() << endl;

	cout << "Location: " << 
		picker->GetPosition().BoxValue(0) << "," <<
		picker->GetPosition().BoxValue(1) << "," <<
		picker->GetPosition().BoxValue(2) << endl;

	int i, lnmax, lvmax, lumax, lmax;
	iString name, valu, unit, s;

	lnmax = lvmax = lumax = 0;
	for(i=0; i<picker->GetDataFormatter()->GetNumReportLines(); i++)
	{
		picker->GetDataFormatter()->GetReport(i,name,valu,unit);
		if(lnmax < name.Length()) lnmax = name.Length();
		if(lvmax < valu.Length()) lvmax = valu.Length();
		if(lumax < unit.Length()) lumax = unit.Length();
	}
	
	lmax = lnmax;
	if(lmax < lvmax) lmax = lvmax;
	if(lmax < lumax) lmax = lumax;
	for(i=0; i<lmax; i++) s += " ";
	
	for(i=0; i<picker->GetDataFormatter()->GetNumReportLines(); i++)
	{
		picker->GetDataFormatter()->GetReport(i,name,valu,unit);
		cout << 
			name.ToCharPointer() << s.Part(0,lnmax-name.Length()).ToCharPointer() << "  " <<
			valu.ToCharPointer() << s.Part(0,lvmax-valu.Length()).ToCharPointer() << "  " <<
			unit.ToCharPointer() << s.Part(0,lnmax-unit.Length()).ToCharPointer() << endl;
	}
	cout << "(Pick nothing to hide the picked point marker.)" << endl;
}

#endif
