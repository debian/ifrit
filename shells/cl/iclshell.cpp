/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_CL)


#include "iclshell.h"


#include "ierror.h"
#include "ioutputchannel.h"
#include "iversion.h"
#include "iviewmodule.h"
#include "iviewmodulecollection.h"

#include "icldatareaderobserver.h"
#include "iclshelleventobservers.h"
#include "iclviewmoduleeventobservers.h"

#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkTimerLog.h>


//
//  Templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


using namespace iParameter;


namespace iclShell_Private
{
	class OutputChannel : public iOutputChannel
	{

	public:

		OutputChannel(iShell *s) : iOutputChannel(s)
		{
		}

	protected:

		virtual void DisplayBody(int type, const iString &text)
		{
			this->iOutputChannel::DisplayBody(type,text);
		}
	};


	class TimerObserver : public vtkCommand
	{

	public:

		static TimerObserver* New(iclShell *s)
		{
			return new TimerObserver(s);
		}

		virtual void Execute(vtkObject *obj, unsigned long id, void *)
		{
			if(mWorking) return;
			mWorking = true;

			switch(id)
			{
			case vtkCommand::TimerEvent:
				{
					vtkRenderWindowInteractor *rwi = iRequiredCast<vtkRenderWindowInteractor>(INFO,obj);
					rwi->SetLastEventPosition(rwi->GetEventPosition());

					if(mShell->IsInterrupted() || mShell->IsStopped())
					{
						rwi->TerminateApp();
					}
					break;
				}
			}
			
			mWorking = false;
		}

	private:

		TimerObserver(iclShell *s)
		{
			IASSERT(s);
			mShell = s;
			mWorking = false;

			this->PassiveObserverOn();
		}

		iclShell *mShell;
		bool mWorking;
	};
};


using namespace iParameter;
using namespace iclShell_Private;


iclShell::iclShell(iRunner *runner, bool threaded, const iString &type, int argc, const char **argv) : iShell(runner,threaded,type,argc,argv),
	iObjectConstructPropertyMacroS(Bool,iclShell,Trace,tr)
{
	Trace.AddFlag(iProperty::_OptionalInState);

	mTimerId = -1;
	mDriver = 0;

	mTrace = false;
}


iclShell::~iclShell()
{
}


bool iclShell::ConstructorBody()
{
	if(this->GetViewModule(0)->GetRenderWindow()->IsA("vtkCocoaRenderWindow") != 0)
	{
		cerr << "Command-line shell does not work with Cocoa rendering system." << endl;
		return false;
	}

	cout << "IFrIT Command-line shell, version " << iVersion::GetVersion().ToCharPointer() << endl;

	return true;
}


void iclShell::DestructorBody()
{
}


void iclShell::StartBody()
{
	int i;
	for(i=0; i<mViewModules->Size(); i++)
	{
		this->RequestRender(mViewModules->GetViewModule(i),true);
	}

	mDriver = 0;
	TimerObserver *obs = TimerObserver::New(this); IERROR_CHECK_MEMORY(obs);

	do
	{
		if(mDriver != this->GetViewModule(0)->GetRenderWindowInteractor())
		{
			mDriver = this->GetViewModule(0)->GetRenderWindowInteractor(); IASSERT(mDriver);
			mTimerId = mDriver->CreateRepeatingTimer(10);
			mDriver->AddObserver(vtkCommand::TimerEvent,obs);
		}

		this->DisplayPrompt();

		mDriver->Start();

		if(this->IsStopped()) break;

		this->OnInterrupt();
	}
	while(true);

	if(mDriver != 0) mDriver->DestroyTimer(mTimerId);
	mDriver = 0;

	obs->Delete();
}


void iclShell::StopBody()
{
	//if(mDriver != 0) mDriver->TerminateApp();
}


bool iclShell::SetTrace(bool s)
{
	mTrace = s;
	return true;
}


void iclShell::OnLoadStateAtomBody(int prog)
{
	cout << "\rLoading a state file...  ";

	if(prog < 0)
	{
		cout << "done" << endl;
		mWorkTimer->StopTimer();
	}
	else if(prog == 0)
	{
		mWorkTimer->StartTimer();
		wCount = 0;
	}
	else
	{
		mWorkTimer->StopTimer();
		if(mWorkTimer->GetElapsedTime() > 0.2)
		{
			switch(wCount % 4)
			{
			case 0:
				{
					cout << "|";
					break;
				}
			case 1:
				{
					cout << "/";
					break;
				}
			case 2:
				{
					cout << "-";
					break;
				}
			case 3:
				{
					cout << "\\";
				}
			}

			wCount++;

			mWorkTimer->StartTimer();
		}
	}
}


//
//  Base implementation works as extending shell
//
bool iclShell::IsInterrupted() const
{
	return this->HasPostedRequests();
}


void iclShell::DisplayPrompt() const
{
	//
	//  Prompt is displayed by the script interpreter
	//
}


void iclShell::OnInterrupt()
{
	//
	//  Render as needed
	//
	this->PushAllRequests_();
}


//
//  Factory methods
//
iDataReaderObserver* iclShell::CreateDataReaderObserver()
{
	return new iclDataReaderObserver(this);
}


iEventObserver* iclShell::CreateShellEventObserver(ShellObserver::Type type)
{
	switch (type)
	{
		case ShellObserver::Execution:
		{
			return new iclExecutionEventObserver(this);
		}
		case ShellObserver::ParallelUpdate:
		{
			return new iclParallelUpdateEventObserver(this);
		}
		default:
		{
			IBUG_FATAL("Invalid ShellObserver type.");
			return 0;
		}
	}
}


iOutputChannel* iclShell::CreateOutputChannel()
{
	return new OutputChannel(this);
}


iEventObserver* iclShell::CreateViewModuleEventObserver(ViewModuleObserver::Type type, iViewModule *vm) const
{
	IASSERT(vm);

	switch(type)
	{
		case ViewModuleObserver::Render:
		{
			return new iclRenderEventObserver(vm);
		}
		case ViewModuleObserver::Pick:
		{
			return new iclPickEventObserver(vm);
		}
		default:
		{
			IBUG_FATAL("Invalid ViewModuleObserver type.");
			return 0;
		}
	}
}


vtkRenderWindow* iclShell::CreateRenderWindow(iViewModule *vm, bool stereo) const
{
	IASSERT(vm);
	vtkRenderWindow *win = vtkRenderWindow::New();
	if(win != 0)
	{
		win->SetStereoCapableWindow(stereo?1:0);
	}
	return win;
}


vtkRenderWindowInteractor* iclShell::CreateRenderWindowInteractor(iViewModule *vm) const
{
	IASSERT(vm);
	return vtkRenderWindowInteractor::New();
}

#endif
