/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef ICLSHELL_H
#define ICLSHELL_H

//
//  A shell based on command line interface
//
#include "ishell.h"


class vtkRenderWindowInteractor;
class vtkTimerLog;


class iclShell : public iShell
{

	friend class iShell;

public:

	vtkTypeMacro(iclShell,iShell);

	iObjectPropertyMacro1S(Trace,Bool);

	//
	//  Factory methods
	//
	virtual vtkRenderWindow* CreateRenderWindow(iViewModule *vm, bool stereo) const;
	virtual vtkRenderWindowInteractor* CreateRenderWindowInteractor(iViewModule *vm) const;
	virtual iEventObserver* CreateViewModuleEventObserver(iParameter::ViewModuleObserver::Type type, iViewModule *vm) const;

	virtual bool IsInterrupted() const;

protected:

	iclShell(iRunner *runner, bool threaded, const iString &type, int argc, const char **argv);
	virtual ~iclShell();

	virtual void DisplayPrompt() const;
	virtual void OnInterrupt();

	virtual bool ConstructorBody();  
	virtual void DestructorBody();  
	virtual void StartBody();
	virtual void StopBody();

	virtual void OnLoadStateAtomBody(int prog);

	virtual iDataReaderObserver* CreateDataReaderObserver();
	virtual iEventObserver* CreateShellEventObserver(iParameter::ShellObserver::Type type);
	virtual iOutputChannel* CreateOutputChannel();

	int mTimerId;
	vtkRenderWindowInteractor *mDriver;

	int wCount;
};

#endif  // ICLSHELL_H
