/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Command line implementation of view module observers
//

#ifndef ICLVIEWMODULEEVENTOBSERVERS_H
#define ICLVIEWMODULEEVENTOBSERVERS_H


#include "iviewmoduleeventobservers.h"


class iPicker;


class iclRenderEventObserver : public iRenderEventObserver
{
	
	friend class iclShell;

public:
	
	vtkTypeMacro(iclRenderEventObserver,iRenderEventObserver);

protected:

	iclRenderEventObserver(iViewModule *vm);
	virtual ~iclRenderEventObserver();

	virtual void OnStart();
	virtual void OnFinish();
	virtual bool CheckAbort();
	virtual void PostFinished();

	bool mStarted;
	int wSymbol;
};


class iclPickEventObserver : public iPickEventObserver
{
	
	friend class iclShell;

public:
	
	vtkTypeMacro(iclPickEventObserver,iPickEventObserver);

protected:

	iclPickEventObserver(iViewModule *vm) : iPickEventObserver(vm){}

	virtual void OnStart();
	virtual void OnFinish();
};

#endif // ICLVIEWMODULEEVENTOBSERVERS_H

