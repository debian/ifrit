/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "ibgwidgetareasubject.h"


#include "iggwidgetarea.h"

//
//  Templates
//
#include "iarray.tlh"


using namespace iParameter;


//
//  WidgetDisplayAreaSubject class
//
ibgWidgetDisplayAreaSubject::ibgWidgetDisplayAreaSubject(iggWidget *owner, const iString& /*text*/) : ibgWidgetSubject(owner)
{
};


ibgWidgetDisplayAreaSubject::~ibgWidgetDisplayAreaSubject()
{
}


//
//  WidgetMultiImageDisplayAreaSubject class
//
ibgWidgetMultiImageDisplayAreaSubject::ibgWidgetMultiImageDisplayAreaSubject(iggWidget *owner) : ibgWidgetSubject(owner)
{
	mCurrentImage = 0;
};


ibgWidgetMultiImageDisplayAreaSubject::~ibgWidgetMultiImageDisplayAreaSubject()
{
	while(mImages.Size() > 0) delete mImages.RemoveLast();
}


void ibgWidgetMultiImageDisplayAreaSubject::AddImage(const iImage &image, bool withmask)
{
	mImages.Add(this->AddImageBody(image,withmask,mImages.Size()==0));
}


void ibgWidgetMultiImageDisplayAreaSubject::ShowImage(int n)
{
	if(n>=0 && n<mImages.Size())
	{
		mCurrentImage = n;
		this->ShowImageBody(n);
	}
}


bool ibgWidgetMultiImageDisplayAreaSubject::Advance()
{
	bool ret = false;

	if(mImages.Size() == 0) return true;

	mCurrentImage++;
	if(mCurrentImage == mImages.Size())
	{
		mCurrentImage = 0;
		ret = true;
	}
	this->ShowImage(mCurrentImage);
	return ret;
}


//
//  WidgetDrawAreaSubject class
//
ibgWidgetDrawAreaSubject::ibgWidgetDrawAreaSubject(iggWidgetDrawArea *owner, bool interactive) : ibgWidgetSubject(owner)
{
	mBackgroundColor = iColor(255,255,255);
	mCurrentMode = mPaintMode = DrawMode::All;

	mInPaint = false;
	mDrawOwner = owner;
	mIsInteractive = interactive;
}
	

ibgWidgetDrawAreaSubject::~ibgWidgetDrawAreaSubject()
{
}


void ibgWidgetDrawAreaSubject::SetBackgroundColor(const iColor &c)
{
	mBackgroundColor = c;
}


void ibgWidgetDrawAreaSubject::OnMousePress(int x, int y, int b)
{
	if(mIsInteractive) mDrawOwner->OnMousePress(x,y,b);
}

void ibgWidgetDrawAreaSubject::OnMouseRelease(int x, int y, int b)
{
	if(mIsInteractive) mDrawOwner->OnMouseRelease(x,y,b);
}

void ibgWidgetDrawAreaSubject::OnMouseMove(int x, int y, int b)
{
	if(mIsInteractive) mDrawOwner->OnMouseMove(x,y,b);
}


void ibgWidgetDrawAreaSubject::RequestPainting(int pm)
{
	if(DrawMode::IsValid(pm))
	{
		mPaintMode = pm;
		this->RequestPaintingBody();
	}
}


void ibgWidgetDrawAreaSubject::Paint()
{
	if(mInPaint) return;
	mInPaint = true;

	if(mPaintMode==DrawMode::All || mPaintMode==DrawMode::Background)
	{
		this->Begin(DrawMode::Background,true);
		mDrawOwner->DrawBackground();
		this->End();
	}

	if(mPaintMode==DrawMode::All || mPaintMode==DrawMode::Foreground)
	{
		this->Begin(DrawMode::Foreground,false);
		mDrawOwner->DrawForeground();
		this->End();
	}

	this->Frame();

	mInPaint = false;
}

#endif
