/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A base class for toolkit-dependent frame implementor
//

#ifndef IBGFRAMESUBJECT_H
#define IBGFRAMESUBJECT_H


#include "ibgwidgetsubject.h"


class iImage;
class iString;

class iggFrame;
class iggFrameBook;
class iggFrameFlip;
class iggFrameScroll;
class iggFrameSplit;


class ibgFrameSubject : public ibgWidgetSubject
{

	friend class ibgShell;
	friend class ibgWindowHelper;

public:

	inline iggFrame* GetFrame() const { return mFrameOwner; }

	virtual void SetTitle(const iString &title) = 0;
	virtual void ShowFrame(bool s) = 0;

	virtual void SetPadding(bool s) = 0;
	virtual void SetColStretch(int col, int s) = 0;
	virtual void SetRowStretch(int row, int s) = 0;
	virtual void PlaceWidget(int col, int row, ibgWidgetSubject *child, int nc, bool expand) = 0;

	virtual void GetFrameGeometry(int wg[4]) const = 0;

	inline static int GetPaddingWidth(){ return mPaddingWidth; }

protected:

	ibgFrameSubject(iggFrame *owner, int cols);

	virtual bool BeforeShow();

	iggFrame *mFrameOwner;
	static int mPaddingWidth;
};


class ibgFrameBookSubject : public ibgWidgetSubject
{

public:

	virtual void OpenPage(int i) = 0;
	virtual void AddPage(const iString &title, const iImage *image, iggFrame *frame) = 0;
	virtual void SetTabMode(int n, int m, const iString &title, const iImage *image) = 0;
	virtual int GetTabMode() = 0;
	virtual void ChangeIcon(int n, const iImage &image) = 0;
	virtual void SetOrientation(int v) = 0;
	virtual int GetOrientation() = 0;

protected:

	ibgFrameBookSubject(iggFrameBook *owner, bool withFeedback);
};


class ibgFrameFlipSubject : public ibgWidgetSubject
{

public:

	inline int Count() const { return mCount; }

	virtual void ShowLayer(int i) = 0;
	void AddLayer(iggFrame *layer);

protected:

	ibgFrameFlipSubject(iggFrameFlip *owner, bool expanding);

	virtual void AddLayerBody(iggFrame *layer) = 0;

	int mCount;
};


class ibgFrameScrollSubject : public ibgWidgetSubject
{

public:

	virtual void AttachContents(iggFrame *contents) = 0;

protected:

	ibgFrameScrollSubject(iggFrameScroll *owner, bool withHor);
};


class ibgFrameSplitSubject : public ibgWidgetSubject
{

public:

	virtual void AddSegment(iggFrame *frame) = 0;

protected:

	ibgFrameSplitSubject(iggFrameSplit *owner, bool hor);
};

#endif  // IBGFRAMESUBJECT_H

