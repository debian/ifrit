/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Toolkit-independent subject for window subjects
//

#ifndef IBGWINDOWSUBJECT_H
#define IBGWINDOWSUBJECT_H


#include "ibgelement.h"


#include "ipointermacro.h"

class iImage;
class iString;

class iggShell;

class ibgWindowHelper;


class ibgWindowSubject : public ibgElement
{

public:

	void Delete();
	
	virtual void SetWindowGeometry(int wg[4], bool force = false);
	virtual void GetWindowGeometry(int wg[4], bool force = false) const;

	virtual void GetFrameSize(int &w, int &h) const;

	virtual void Show(bool s);
	virtual bool IsVisible() const;

	inline ibgWindowHelper* GetHelper() const { return mWindowHelper; }

	static void Block(bool s);

	inline iggShell* GetShell() const { return mShell; }

protected:

	ibgWindowSubject(iggShell *shell);
	virtual ~ibgWindowSubject();

	virtual bool IsBlocked() const;

	ibgWindowHelper *mWindowHelper;

private:

	iggShell *mShell;

	bool mIsBeingDeleted;
	static bool mIsBlocked;
};

#endif  // IBGWINDOWSUBJECT_H

