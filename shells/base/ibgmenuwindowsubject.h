/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Toolkit-independent base for a subject window class that has menus and toolbars
//

#ifndef IBGMENUWINDOWSUBJECT_H
#define IBGMENUWINDOWSUBJECT_H


#include "ibgwindowsubject.h"


#include "imath.h"

class iggFrameTopParent;
class iggMenuHelperBase;
class iggMenuWindow;


class ibgMenuWindowSubject : public ibgWindowSubject
{

	friend class iggMenuWindow;

public:

	//
	//  Menu interaction (public to allow extensions to modify the menu)
	//
	virtual void BeginMenu(const iString &text, bool exclusive) = 0;
	virtual void EndMenu() = 0;
	virtual void AddMenuItem(int id, const iString &text, const iImage *icon, const iString &accel, bool toggle, bool on, const iggMenuHelperBase *enabler = 0, const iggMenuHelperBase *checker = 0) = 0;
	virtual void AddMenuSeparator() = 0;
	virtual void AddToolBarButton(int id, const iString &tooltip, const iImage *icon, bool toggle, const iggMenuHelperBase *enabler = 0, const iggMenuHelperBase *checker = 0) = 0;
	virtual void AddToolBarSeparator() = 0;
	virtual void CompleteMenu() = 0;

	virtual void SetToolBarIcon(int id, const iImage &icon) = 0;
	virtual void UpdateMenus() = 0;

	virtual void SetGlobalFrame(iggFrameTopParent *globalFrame, int cols) = 0;

	virtual iString GetFileName(const iString &header, const iString &file, const iString &selection, bool reading) = 0;

protected:

	ibgMenuWindowSubject(iggMenuWindow *owner, const iImage *icon, const iString &title);
	virtual ~ibgMenuWindowSubject();
	
	iggMenuWindow *mOwner;

private:

	ibgMenuWindowSubject(const ibgMenuWindowSubject&); // Not implemented.
	void operator=(const ibgMenuWindowSubject&);  // Not implemented.
};

#endif  // IBGMENUWINDOWSUBJECT_H

