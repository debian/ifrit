/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Toolkit-independent base for window subject - classes that implement basic
//  window functions like resizing and showing/hiding
//

#ifndef IBGWINDOWHELPER_H
#define IBGWINDOWHELPER_H


#include "ibgelement.h"


class iImage;
class iString;


class ibgWindowHelper : public ibgElement
{

	friend class iggShell;

public:

	//
	//  Save and restore geometries for docking
	//
	void SaveWindowGeometry();
	void RestoreWindowGeometry();

	//
	//  Inheritable interface
	//
	virtual void SetWindowGeometry(int wg[4], bool force) = 0;
	virtual void GetWindowGeometry(int wg[4], bool force) const = 0;

	virtual void GetFrameSize(int &w, int &h) const = 0;

	virtual void Show(bool s) = 0;
	virtual bool IsVisible() const = 0;

	virtual void ShowAsWindow() = 0;
	virtual void ShowAsIcon() = 0;

	virtual void RestoreDecoration() = 0;

	inline static int GetFontOffset(){ return mFontOffset; }

protected:

	ibgWindowHelper(const iImage *icon, const iString &title, bool disableClose);

	static void ChangeFontSize(int s);

	const iImage *mIcon;
	const iString &mTitle;

	bool mDisableClose;
	int mSavedGeometry[4];

	static int mFontOffset;
};

#endif  // IBGWINDOWHELPER_H

