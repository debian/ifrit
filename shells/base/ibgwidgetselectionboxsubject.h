/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Generic selection box subject
//
#ifndef IBGWIDGETSELECTIONBOXSUBJECT_H
#define IBGWIDGETSELECTIONBOXSUBJECT_H


#include "ibgwidgetsubject.h"


class iImage;
class iString;


//
//  Abstract owner class
//
class ibgWidgetSelectionBoxSubject : public ibgWidgetSubject
{

public:

	virtual ~ibgWidgetSelectionBoxSubject();

	//
	//  Decorator functions
	//
	virtual int GetValue() const = 0;
	virtual void SetValue(int v) = 0;

protected:

	ibgWidgetSelectionBoxSubject(iggWidget *owner);
};


//
//  Spin box
//
class ibgWidgetSpinBoxSubject : public ibgWidgetSelectionBoxSubject
{

public:

	virtual ~ibgWidgetSpinBoxSubject();

	virtual void SetStretch(int title, int box) = 0;

	//
	//  Decorator functions
	//
	virtual void SetFirstEntryText(const iString &text) = 0;
	virtual void SetRange(int min, int max) = 0;
	virtual void SetStep(int step) = 0;
	virtual int Count() = 0;

protected:

	ibgWidgetSpinBoxSubject(iggWidget *owner, int min, int max, const iString &title, int step);
};


//
//  Combo box
//
class ibgWidgetComboBoxSubject : public ibgWidgetSelectionBoxSubject
{

public:

	virtual ~ibgWidgetComboBoxSubject();

	virtual void SetStretch(int title, int box) = 0;

	//
	//  Decorator functions
	//
	virtual const iString GetText(int index = -1) const = 0;
	virtual void SetText(const iString &text) = 0;
	virtual void InsertItem(const iString &text, int index = -1) = 0;
	virtual void InsertItem(const iImage &icon, const iString &text, int index = -1) = 0;
	virtual void SetItem(const iString &text, int index, bool vis) = 0;
	virtual void RemoveItem(int index) = 0;
	virtual int Count() = 0;
	virtual void Clear() = 0;
	virtual bool DoesContentFit() const = 0;

protected:

	ibgWidgetComboBoxSubject(iggWidget *owner, const iString &title, bool bold);
};


//
//  Radio button box
//
class ibgWidgetRadioBoxSubject : public ibgWidgetSelectionBoxSubject
{

public:

	virtual ~ibgWidgetRadioBoxSubject();

	//
	//  Decorator functions
	//
	virtual void InsertItem(const iString &text, int index = -1) = 0;
	virtual void InsertItem(const iImage &icon, const iString &text, int index = -1) = 0;
	virtual void SetItem(const iString &text, int index, bool vis) = 0;
	virtual void RemoveItem(int index) = 0;
	virtual int Count() = 0;

protected:

	ibgWidgetRadioBoxSubject(iggWidget *owner, int cols, const iString &title);
};

#endif  // IBGWIDGETSELECTIONBOXSUBJECT_H

