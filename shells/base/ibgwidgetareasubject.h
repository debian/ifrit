/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Nontrivial classes defined in this file
//
//  ibgWidgetTextAreaSubject: element that shows text or a single image;
//  ibgWidgetDrawAreaSubject: element that allows painting on itself and responds to mouse interaction;
//  ibgWidgetScrollAreaSubject: element that contains an area with scroll-bars and a FrameBase inside
//

#ifndef IBGWIDGETAREASUBJECT_H
#define IBGWIDGETAREASUBJECT_H


#include "ibgwidgetsubject.h"


#include "iarray.h"
#include "icolor.h"
#include "iimage.h"

class iString;

class iggFrame;
class iggWidget;
class iggWidgetDrawArea;


namespace iParameter
{
	namespace DrawMode
	{
		const int All =			0;
		const int Background =	1;
		const int Foreground =	2;

		inline bool IsValid(int m){ return m>=0 && m<=2; }
	};
};


class ibgWidgetDisplayAreaSubject : public ibgWidgetSubject
{

public:

	virtual ~ibgWidgetDisplayAreaSubject();
	
	//
	//  Decorator functions
	//
	virtual void SetText(const iString &text) = 0;
	virtual void SetImage(const iImage *image, bool withMask, bool scaled) = 0;
	virtual void SetAlignment(int s) = 0;
	virtual void ShowFrame(bool s) = 0;

protected:

	ibgWidgetDisplayAreaSubject(iggWidget *owner, const iString &text);
};


class ibgWidgetMultiImageDisplayAreaSubject : public ibgWidgetSubject
{

	friend class iggWidgetImageFlipper;

public:

	virtual ~ibgWidgetMultiImageDisplayAreaSubject();
	
	void AddImage(const iImage &image, bool withmask);
    void ShowImage(int n);

	//
	//  For efficiency purposes, we keep the image data in the native format within this class
	//
	class Image
	{
	public:
		Image(){}
		virtual ~Image(){}
	};

protected:

	ibgWidgetMultiImageDisplayAreaSubject(iggWidget *owner);

	bool Advance();
	inline int GetNumberOfImages() const { return mImages.Size(); }

	virtual Image* AddImageBody(const iImage &image, bool withmask, bool first) = 0;
    virtual void ShowImageBody(int n) = 0;
	virtual void Start() = 0;
	virtual void Abort() = 0;

	iArray<Image*> mImages;
	int mCurrentImage;
};


class ibgWidgetDrawAreaSubject : public ibgWidgetSubject
{

public:

	virtual ~ibgWidgetDrawAreaSubject();
	
	void SetBackgroundColor(const iColor &c);

	//
	//  Decorator functions
	//
	virtual int Width() const = 0;
	virtual int Height() const = 0;

	virtual void SetMinimumSize(int w, int h) = 0;
	
	virtual void Begin(int m, bool clear) = 0;
	virtual void End() = 0;
	virtual void Frame() = 0; // show the image

	virtual void Clear() = 0;

	virtual void DrawLine(int x1, int y1, int x2, int y2, const iColor &color, int width = 1, bool dotted = false) = 0;
	virtual void DrawRectangle(int x1, int y1, int x2, int y2, const iColor &color, const iColor& fillColor, int width = 1) = 0;
	virtual void DrawEllipse(int x1, int y1, int x2, int y2, const iColor &color, const iColor& fillColor, int width = 1) = 0;
	virtual void DrawImage(int x1, int y1, int x2, int y2, const iImage &image, iImage::ScaleMode mode) = 0;
	virtual void DrawTextLine(int x1, int y1, int x2, int y2, const iString &text, const iColor &color) = 0;

	virtual void BlendForegroundOntoBackground(float opacity, const iColor &transparent = iColor(255,255,255)) = 0;

	virtual void SetFixedSize(int w, int h) = 0;

	void RequestPainting(int pm = iParameter::DrawMode::All);

protected:

	ibgWidgetDrawAreaSubject(iggWidgetDrawArea *owner, bool interactive);

	void Paint();
	virtual void RequestPaintingBody() = 0;

	//
	//  Decorator functions
	//
	void OnMousePress(int x, int y, int b);
	void OnMouseRelease(int x, int y, int b);
	void OnMouseMove(int x, int y, int b);

	bool mInPaint, mIsInteractive;
	int mCurrentMode, mPaintMode;
	iColor mBackgroundColor;
	iggWidgetDrawArea *mDrawOwner;
};

#endif  // IBGWIDGETAREASUBJECT_H

