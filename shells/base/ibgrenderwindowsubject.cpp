/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/
  

#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "ibgrenderwindowsubject.h"


#include "ierror.h"
#include "iviewmodule.h"

#include "iggmainwindow.h"
#include "iggrenderwindow.h"
#include "iggrenderwindowinteractor.h"
#include "iggshell.h"


bool ibgRenderWindowSubject::RenderOnFocus = false;


ibgRenderWindowSubject::ibgRenderWindowSubject(iggRenderWindow *owner) : ibgWindowSubject(owner?iRequiredCast<iggShell>(INFO,owner->GetViewModule()->GetShell()):0)
{
	IASSERT(owner);

	mOwner = owner;
	mInteractor = 0;

	mInRender = false;
}


ibgRenderWindowSubject::~ibgRenderWindowSubject()
{
}


void ibgRenderWindowSubject::Render()
{
	if(mInRender || RenderOnFocus || !mOwner->IsInitialized()) return;
	mInRender = true;
	mOwner->Render();
	mInRender = false;
}


void ibgRenderWindowSubject::OnMouseMoveBody(int x, int y, int b) 
{
	if(!this->IsBlocked() && this->IsInteractorPresent()) mInteractor->OnMouseMove(x,y,b);
}


void ibgRenderWindowSubject::OnMousePressBody(int x, int y, int b) 
{
	if(!this->IsBlocked() && this->IsInteractorPresent()) mInteractor->OnMousePress(x,y,b);
}


void ibgRenderWindowSubject::OnMouseReleaseBody(int x, int y, int b) 
{
	if(!this->IsBlocked() && this->IsInteractorPresent()) mInteractor->OnMouseRelease(x,y,b);
}


void ibgRenderWindowSubject::OnKeyPressBody(char key, int b) 
{
	if(!this->IsBlocked() && this->IsInteractorPresent()) mInteractor->OnKeyPress(key,b);
}


void ibgRenderWindowSubject::OnTimerBody() 
{
	if(!this->IsBlocked() && this->IsInteractorPresent()) mInteractor->OnTimer();
}


void ibgRenderWindowSubject::OnWindowFocusInBody()
{
	if(!this->IsBlocked())
	{
		if(RenderOnFocus)
		{
			mOwner->Render();
		}

		if(mOwner->GetMainWindow()!=0)
		{
			mOwner->GetMainWindow()->OnRenderWindowFocusIn(mOwner->GetViewModule()->GetWindowNumber());
			if(this->IsInteractorPresent()) mInteractor->OnExpose();
		}
	}
}


void ibgRenderWindowSubject::OnWindowFocusOutBody()
{
	if(!this->IsBlocked() && mOwner->GetMainWindow()!=0)
	{
		mOwner->GetMainWindow()->OnRenderWindowFocusOut(mOwner->GetViewModule()->GetWindowNumber());
		if(this->IsInteractorPresent()) mInteractor->OnExpose();
	}
}


void ibgRenderWindowSubject::OnWindowEnterBody()
{
	if(!this->IsBlocked() && mOwner->GetMainWindow()!=0)
	{
		mOwner->GetMainWindow()->OnRenderWindowEnter(mOwner->GetViewModule()->GetWindowNumber());
		if(this->IsInteractorPresent()) mInteractor->OnEnter();
	}
}


void ibgRenderWindowSubject::OnWindowLeaveBody()
{
	if(!this->IsBlocked() && mOwner->GetMainWindow()!=0)
	{
		mOwner->GetMainWindow()->OnRenderWindowLeave(mOwner->GetViewModule()->GetWindowNumber());
		if(this->IsInteractorPresent()) mInteractor->OnLeave();
	}
}


void ibgRenderWindowSubject::OnWindowResizeBody()
{
	if(!this->IsBlocked() && mOwner->GetMainWindow()!=0)
	{
		mOwner->GetMainWindow()->OnRenderWindowResize(mOwner->GetViewModule()->GetWindowNumber());
		mOwner->Modified();
	}
}


void ibgRenderWindowSubject::OnWindowMoveBody()
{
	if(!this->IsBlocked() && mOwner->GetMainWindow()!=0)
	{
		mOwner->GetMainWindow()->OnRenderWindowMove(mOwner->GetViewModule()->GetWindowNumber());
		mOwner->Modified();
	}
}


bool ibgRenderWindowSubject::IsInteractorPresent()
{
	if(mInteractor == 0)
	{
		mInteractor = iRequiredCast<iggRenderWindowInteractor>(INFO,mOwner->GetInteractor());
	}
	return mInteractor != 0;
}

#endif
