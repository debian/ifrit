/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "ibgwindowhelper.h"


#include "ibgframesubject.h"


int ibgWindowHelper::mFontOffset = 0; // default value


ibgWindowHelper::ibgWindowHelper(const iImage *icon, const iString &title, bool disableClose) : mIcon(icon), mTitle(title), mDisableClose(disableClose)
{
	mSavedGeometry[0] = mSavedGeometry[1] = 100;
	mSavedGeometry[2] = mSavedGeometry[3] = 0;
}


//
//  Save and restore geometries for docking
//
void ibgWindowHelper::SaveWindowGeometry()
{
	this->GetWindowGeometry(mSavedGeometry,true);
}


void ibgWindowHelper::RestoreWindowGeometry()
{
	this->SetWindowGeometry(mSavedGeometry,true);
}


void ibgWindowHelper::ChangeFontSize(int s)
{
	if(s>-10 && s<10)
	{
		mFontOffset = s;
		if(s < 0)
		{
			ibgFrameSubject::mPaddingWidth = 2;
		}
		else
		{
			ibgFrameSubject::mPaddingWidth = 4;
		}
	}
}

#endif
