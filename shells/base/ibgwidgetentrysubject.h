/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A combination of a slider with an LCD and/or line edit 
//
#ifndef IBGWIDGETENTRYSUBJECT_H
#define IBGWIDGETENTRYSUBJECT_H


#include "ibgwidgetsubject.h"


#include <vtkType.h>

class iString;

class iggWidget;
class ibgWidgetButtonSubject;


class ibgWidgetEntrySubject : public ibgWidgetSubject
{

public:

	ibgWidgetEntrySubject(iggWidget *owner, bool slider, int numdig, const iString &label);
	virtual ~ibgWidgetEntrySubject();

	virtual void FreezeNumberOfDigits(bool s);
	void IncrementNumDigits(int n){ if(mNumDigits>0 && mNumDigits+n>0) mNumDigits += n; }

	virtual void AddButton(ibgWidgetButtonSubject* button) = 0;
	virtual void SetEditable(bool s) = 0;

	//
	//  Decorator functions
	//
	virtual int GetValue() const = 0;
	virtual void GetRange(int &min, int &max) const = 0;
	virtual const iString GetText() const = 0;

	virtual void SetValue(int v) = 0;
	virtual void SetValue(int v, double dv) = 0;
	virtual void SetValue(int v, vtkIdType lv) = 0;
	virtual void SetNumberOfTicks(int n) = 0;
	virtual void SetRange(int smin, int smax, int pagestep = 0) = 0;
	virtual void SetText(const iString &text) = 0;
	virtual void SelectText(int start, int len = -1) = 0;

	virtual void ShowTail() = 0;

	virtual void SetStretch(int label, int slider, int number = 0) = 0;

protected:

	int mNumDigits;
	bool mNumDigitsFrozen;
};

#endif  // IBGWIDGETENTRYSUBJECT_H

