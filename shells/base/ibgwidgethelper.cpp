/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "ibgwidgethelper.h"


#include "ierror.h"
#include "ihelpfactory.h"

#include "iggdialoghelp.h"
#include "iggmainwindow.h"
#include "iggwidget.h"


//
//  Main class
//
ibgWidgetHelper::ibgWidgetHelper(iggWidget *owner)
{
	IASSERT(owner);

	mOwner = owner;
}


ibgWidgetHelper::~ibgWidgetHelper()
{
}


void ibgWidgetHelper::OpenHelpPage(const iString &tag)
{
	mOwner->GetMainWindow()->GetDialogHelp()->Open(tag.ToCharPointer());
}


//
//  Generic slots
//
void ibgWidgetHelper::OnInt1Body(int v)
{
	mOwner->OnInt1Body(v);
	mOwner->UpdateDependents();
}


void ibgWidgetHelper::OnInt2Body(int v)
{
	mOwner->OnInt2Body(v);
	mOwner->UpdateDependents();
}


void ibgWidgetHelper::OnVoid1Body()
{
	mOwner->OnVoid1Body();
	mOwner->UpdateDependents();
}


void ibgWidgetHelper::OnVoid2Body()
{
	mOwner->OnVoid2Body();
	mOwner->UpdateDependents();
}


void ibgWidgetHelper::OnVoid3Body()
{
	mOwner->OnVoid3Body();
	mOwner->UpdateDependents();
}


void ibgWidgetHelper::OnBool1Body(bool v)
{
	mOwner->OnBool1Body(v);
	mOwner->UpdateDependents();
}

#endif
