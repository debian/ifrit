/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Text editor widget subject
//

#ifndef IBGWIDGETTEXTEDITORSUBJECT_H
#define IBGWIDGETTEXTEDITORSUBJECT_H


#include "ibgwidgetsubject.h"


class iColor;
class iString;

class iggWidgetTextEditor;


class ibgWidgetTextEditorSubject : public ibgWidgetSubject
{

public:

	virtual ~ibgWidgetTextEditorSubject();

	virtual bool AppendBreaksLine() const = 0;
	virtual bool SupportsHTML() const = 0;
	virtual bool TextModified() const = 0;

	//
	//  Text manipulations
	//
	virtual void Clear() = 0;
	virtual void SetModified(bool s) = 0;
	virtual void SetText(const iString &text) = 0;
	virtual iString GetText() const = 0;
	virtual void AppendText(const iString &text, bool scroll = true) = 0;

	//
	//  Line-by-line manipulation
	//
	virtual int GetNumberOfLines() const = 0;
	virtual void RemoveLine(int n) = 0;
	virtual iString GetLine(int n) const = 0;

	//
	//  Text attribute manipulations
	//
	virtual void SetBoldText(bool s) = 0;
	virtual void SetTextColor(const iColor &color) = 0;
	virtual void SetLineColor(int line, const iColor &color) = 0;
	virtual void AdjustFontSize(int s) = 0;
	virtual void UnSetLineColor(int line) = 0;
	virtual iColor GetLineColor(int line) const = 0;
	virtual void SetReadOnly(bool s) = 0;

	//
	//  Selection & cursor location
	//
	virtual void Select(int lineFrom, int indexFrom, int lineTo, int indexTo, bool leftJustified = false, int selNum = 0) = 0;
	virtual void GetCursorPosition(int &line, int &index) = 0;
	virtual void SetCursorPosition(int line, int index) = 0;

	//
	//  Editing functions
	//
	virtual bool HasEditingFunction(int type) const = 0;
	virtual void UseEditingFunction(int type) = 0;

	//
	//  Search & find
	//
	virtual bool Find(const iString &text, bool cs, bool wo, bool forward, int &line, int &index) = 0;

protected:

	void OnTextChangedBody();
	void OnSelectionChangedBody(int lineFrom, int indexFrom, int lineTo, int indexTo);
	void OnCursorPositionChangedBody(int line, int index);
	void OnReturnPressedBody();
	//void OnMousePressBody(int line, int index);
	//void OnMouseReleaseBody(int line, int index);

	ibgWidgetTextEditorSubject(iggWidgetTextEditor *owner, unsigned int mode);

	iggWidgetTextEditor *mConcreteOwner;
};

#endif  //  IBGWIDGETTEXTEDITORSUBJECT_H
