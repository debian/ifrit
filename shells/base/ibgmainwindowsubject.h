/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Toolkit-independent base for a main window subject class.
//

#ifndef IBGMAINWINDOWSUBJECT_H
#define IBGMAINWINDOWSUBJECT_H


#include "ibgelement.h"


class iString;

class iggDialog;
class iggFrame;
class iggFrameTopParent;
class iggMainWindow;
class iggRenderWindow;

class ibgWidgetProgressBarSubject;


class ibgMainWindowSubject : public ibgElement
{

	friend class iggMainWindow;

public:

	virtual void AttachRenderWindow(iggRenderWindow *rw) = 0;

protected:

	ibgMainWindowSubject(iggMainWindow *owner);
	virtual ~ibgMainWindowSubject();

	//
	//  General members
	//
	virtual void ShowToolTips(bool s) = 0;
	virtual void SetTheme(const iString &name) = 0;
	virtual void StartTimer() = 0;

	//
	//  Construction helpers
	//
	virtual void SetTopParentSubjects(iggFrameTopParent *busyIndicatorFrame, iggFrameTopParent *visitedFileListFrame) = 0;
	virtual void PopulateStatusBar(iggFrame *busyIndicatorFrame, ibgWidgetProgressBarSubject *progressBarSubject, iggFrame *visitedFileListFrame) = 0;

	//
	//  Docking helpers
	//
	virtual void PlaceWindowsInDockedPositions() = 0;
	virtual void RestoreWindowsFromDockedPositions() = 0;
	
	//
	//  Helper variables
	//
	iggMainWindow *mMainWindow;
};

#endif  // IBGMAINWINDOWSUBJECT_H

