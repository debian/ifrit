/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Generic trackball
//
#ifndef IBGWIDGETTRACKBALLSUBJECT_H
#define IBGWIDGETTRACKBALLSUBJECT_H


#include "ibgwidgetsubject.h"


class iColor;
class iTransform;

class ibgWidgetButtonSubject;


class ibgWidgetTrackBallSubject : public ibgWidgetSubject
{

	friend class iggWidgetPropertyControlTrackBall;

public:

	virtual ~ibgWidgetTrackBallSubject();

	void SetDirection(const double dir[3]);
	void GetDirection(double dir[3]) const;

protected:

    ibgWidgetTrackBallSubject(iggWidget *owner, bool followCamera);

	virtual void AddButton(ibgWidgetButtonSubject* button) = 0;
	virtual void GetSizes(int &w0, int &h0, int &rad, int &cor) = 0;
	virtual void Repaint() = 0;

	void UpdateTransform(int newX, int newY);
	const iColor& ColorAtLonLat(double lon, double lat);

	bool mDoubleBuffer, mResizable, mFollowCamera;
	int mOldX, mOldY, mOldRad;
	const int mFixedSize;
	double mLightDirection[3];
	iTransform *mTransform, *mBallTransform;

	//
	//  Earth albedo
	//
	const struct EarthData
	{
		int NumLon;
		int NumLat;
		unsigned char *Data;
		EarthData(int n, int m, unsigned char *d){ NumLon = n; NumLat = m; Data = d; }
	}
	mEarth;

private:

	ibgWidgetTrackBallSubject(const ibgWidgetTrackBallSubject&) ; // never implemented
	void operator=(const ibgWidgetTrackBallSubject&) ; // never implemented 
};

#endif // IBGWIDGETTRACKBALLSUBJECT_H
