/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "ibgwindowsubject.h"


#include "ierror.h"

#include "iggshell.h"

#include "ibgwindowhelper.h"


bool ibgWindowSubject::mIsBlocked = false;


ibgWindowSubject::ibgWindowSubject(iggShell *shell) : mShell(shell)
{
	mWindowHelper = 0;
	mIsBeingDeleted = false;
}


ibgWindowSubject::~ibgWindowSubject()
{
	IASSERT(mWindowHelper);
	delete mWindowHelper;
}


void ibgWindowSubject::Delete()
{
	mIsBeingDeleted = true;
	delete this;
}


void ibgWindowSubject::SetWindowGeometry(int wg[4], bool force)
{
	IASSERT(mWindowHelper);
	mWindowHelper->SetWindowGeometry(wg,force);
}


void ibgWindowSubject::GetWindowGeometry(int wg[4], bool force) const
{
	IASSERT(mWindowHelper);
	mWindowHelper->GetWindowGeometry(wg,force);
}


void ibgWindowSubject::GetFrameSize(int &w, int &h) const
{
	IASSERT(mWindowHelper);
	mWindowHelper->GetFrameSize(w,h);
}


void ibgWindowSubject::Show(bool s)
{
	IASSERT(mWindowHelper);
	mWindowHelper->Show(s);
}


bool ibgWindowSubject::IsVisible() const
{
	IASSERT(mWindowHelper);
	return mWindowHelper->IsVisible();
}


void ibgWindowSubject::Block(bool s)
{
	mIsBlocked = s;
}


bool ibgWindowSubject::IsBlocked() const
{
	return mIsBlocked || mIsBeingDeleted;
}

#endif
