/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_GG)


#include "ibgframesubject.h"


#include "ierror.h"

#include "iggframe.h"


int ibgFrameSubject::mPaddingWidth = 4;


//
//  Frame class
//
ibgFrameSubject::ibgFrameSubject(iggFrame *owner, int /*cols*/) : ibgWidgetSubject(owner)
{
	mFrameOwner = owner; // parent can be zero
}


bool ibgFrameSubject::BeforeShow()
{
	if(mFrameOwner != 0)
	{
		if(mFrameOwner->NeedsRedoLayout()) return true;
	}
	return false;
}


//
//  FrameBook class
//
ibgFrameBookSubject::ibgFrameBookSubject(iggFrameBook *owner, bool /*withFeedback*/) : ibgWidgetSubject(owner)
{
}


//
//  FrameFlip class
//
ibgFrameFlipSubject::ibgFrameFlipSubject(iggFrameFlip *owner, bool) : ibgWidgetSubject(owner)
{
	mCount = 0;
}


void ibgFrameFlipSubject::AddLayer(iggFrame *page)
{
	this->AddLayerBody(page);
	mCount++;
}


//
//  FrameScroll class
//
ibgFrameScrollSubject::ibgFrameScrollSubject(iggFrameScroll *owner, bool) : ibgWidgetSubject(owner)
{
}


//
//  FrameSplit class
//
ibgFrameSplitSubject::ibgFrameSplitSubject(iggFrameSplit *owner, bool) : ibgWidgetSubject(owner)
{
}

#endif
