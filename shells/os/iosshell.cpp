/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_OS)


#include "iosshell.h"


#include "idatareaderobserver.h"
#include "idirectory.h"
#include "ierror.h"
#include "ifile.h"
#include "ioutputchannel.h"
#include "iscript.h"
#include "ishelleventobservers.h"
//#include "isystem.h"
//#include "iversion.h"
//#include "iviewmodule.h"
//#include "iviewmodulecollection.h"
#include "iviewmoduleeventobservers.h"
//#include "ivtk.h"

#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkTimerLog.h>


//
//  Templates
//
#include "iarray.tlh"


using namespace iParameter;


namespace iosShell_Private
{
	class OutputChannel : public iOutputChannel
	{

	public:

		OutputChannel(iShell *s) : iOutputChannel(s)
		{
		}

	protected:

		virtual void DisplayBody(int type, const iString &text)
		{
			this->iOutputChannel::DisplayBody(type,text);
		}
	};


	class ExecutionEventObserver : public iExecutionEventObserver
	{

	public:

		ExecutionEventObserver(iShell *s) : iExecutionEventObserver(s)
		{
		}

	protected:

		virtual void OnStart()
		{
		}

		virtual void OnFinish()
		{
		}

		virtual void OnProgress(double fraction, bool &abort)
		{
		}

		virtual void OnSetLabel(const iString &label)
		{
		}
	};


	class ParallelUpdateEventObserver : public iParallelUpdateEventObserver
	{

	public:

		virtual void UpdateInformation()
		{
		}

		ParallelUpdateEventObserver(iShell *s) : iParallelUpdateEventObserver(s)
		{
		}
	};


	class DataReaderObserver : public iDataReaderObserver
	{

	public:

		DataReaderObserver(iShell *s) : iDataReaderObserver(s)
		{
		}

	protected:

		virtual void OnFileStart(const iString &fname, const iDataType &type)
		{
			this->iDataReaderObserver::OnFileStart(fname,type);
		}
	};


	class RenderEventObserver : public iRenderEventObserver
	{

	public:


		RenderEventObserver(iViewModule *vm) : iRenderEventObserver(vm)
		{
		}

	protected:

		virtual void OnStart()
		{
		}

		virtual void OnFinish()
		{
		}

		virtual bool CheckAbort()
		{
			return false;
		}

		virtual void PostFinished()
		{
		}
	};


	class PickEventObserver : public iPickEventObserver
	{

	public:

		PickEventObserver(iViewModule *vm) : iPickEventObserver(vm)
		{
		}

	protected:

		virtual void OnStart()
		{
		}

		virtual void OnFinish()
		{
		}
	};
};


using namespace iosShell_Private;


iosShell::iosShell(iRunner *runner, const iString &type, int argc, const char **argv) : iShell(runner,false,type,argc,argv)
{
}


iosShell::~iosShell()
{
}


bool iosShell::ConstructorBody()
{
	mScript = iScript::New(this);
	return (mScript != 0);
}


void iosShell::DestructorBody()
{
	mScript->Delete();
}


void iosShell::StartBody()
{
	this->OutputText(MessageType::Information,"Starting...");

	mScript->ExecuteSegment(mScriptText);
}


void iosShell::StopBody()
{
}


bool iosShell::OnCommandLineArguments(iArray<iString> &argv)
{
	if(argv.Size() > 0)
	{
		iString fname(argv[0]);
		argv.Remove(0);
		iDirectory::ExpandFileName(fname,this->GetEnvironment(Environment::Base));

		iFile F(fname);
		if(F.Open(iFile::_Read,iFile::_Text)) 
		{
			iString s;
			while(F.ReadLine(s))
			{
				mScriptText += s + "\n";
			}
			F.Close();
			return true;
		}
		else
		{
			this->OutputText(MessageType::Error,"File "+fname+" is not accessible.");
			return false;
		}
	}
	else
	{
		this->OutputText(MessageType::Error,"Off-screen shell requires a script file name as the first non-option argument.");
		return false;
	}
}


//
//  Factory methods
//
iDataReaderObserver* iosShell::CreateDataReaderObserver()
{
	return new DataReaderObserver(this);
}


iEventObserver* iosShell::CreateShellEventObserver(ShellObserver::Type type)
{
	switch (type)
	{
		case ShellObserver::Execution:
		{
			return new ExecutionEventObserver(this);
		}
		case ShellObserver::ParallelUpdate:
		{
			return new ParallelUpdateEventObserver(this);
		}
		default:
		{
			IBUG_FATAL("Invalid ShellObserver type.");
			return 0;
		}
	}
}


iOutputChannel* iosShell::CreateOutputChannel()
{
	return new OutputChannel(this);
}


iEventObserver* iosShell::CreateViewModuleEventObserver(ViewModuleObserver::Type type, iViewModule *vm) const
{
	IASSERT(vm);

	switch(type)
	{
		case ViewModuleObserver::Render:
		{
			return new RenderEventObserver(vm);
		}
		case ViewModuleObserver::Pick:
		{
			return new PickEventObserver(vm);
		}
		default:
		{
			IBUG_FATAL("Invalid ViewModuleObserver type.");
			return 0;
		}
	}
}


vtkRenderWindow* iosShell::CreateRenderWindow(iViewModule *vm, bool stereo) const
{
	IASSERT(vm);
	vtkRenderWindow *win = vtkRenderWindow::New();
	if(win != 0)
	{
		win->SetStereoCapableWindow(stereo?1:0);
		win->SetOffScreenRendering(1);
	}
	return win;
}


vtkRenderWindowInteractor* iosShell::CreateRenderWindowInteractor(iViewModule *vm) const
{
	return 0;
}

#endif
