/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IOSSHELL_H
#define IOSSHELL_H

//
//  A off-screen rendering shell
//
#include "ishell.h"


class iScript;


class iosShell : public iShell
{

	friend class iShell;

public:

	vtkTypeMacro(iosShell,iShell);

	//
	//  Factory methods
	//
	virtual vtkRenderWindow* CreateRenderWindow(iViewModule *vm, bool stereo) const;
	virtual vtkRenderWindowInteractor* CreateRenderWindowInteractor(iViewModule *vm) const;
	virtual iEventObserver* CreateViewModuleEventObserver(iParameter::ViewModuleObserver::Type type, iViewModule *vm) const;

protected:

	iosShell(iRunner *runner, const iString &type, int argc, const char **argv);
	virtual ~iosShell();

	virtual bool ConstructorBody();  
	virtual void DestructorBody();  
	virtual void StartBody();
	virtual void StopBody();

	virtual bool OnCommandLineArguments(iArray<iString> &argv);

	virtual iDataReaderObserver* CreateDataReaderObserver();
	virtual iEventObserver* CreateShellEventObserver(iParameter::ShellObserver::Type);
	virtual iOutputChannel* CreateOutputChannel();

private:

	iString mScriptText;
	iScript *mScript;
};

#endif  // IOSSHELL_H
