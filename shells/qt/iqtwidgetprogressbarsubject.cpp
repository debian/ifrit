/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_QT)


#include "iqtwidgetprogressbarsubject.h"


#include "ierror.h"

#include "iggwidgetprogressbar.h"

#include "iqthelper.h"
#include "iqtwidgethelper.h"


//
//  This is thread-safe
//
#ifdef IQT_3
#include <qapplication.h>
#else
#ifdef IQT_4
#include <QtGui/QApplication>
#else
#include <QtWidgets/QApplication>
#endif
#endif


iqtWidgetProgressBarSubject::iqtWidgetProgressBarSubject(iggWidget *owner) : QProgressBar(iqtHelper::Convert(owner->GetParent())), ibgWidgetProgressBarSubject(owner)
{
	mWidgetHelper = new iqtWidgetHelper(this,owner); IERROR_CHECK_MEMORY(mWidgetHelper);

#ifdef IQT_3
	this->setTotalSteps(100);
	this->setCenterIndicator(true);
#else
	this->setMinimum(0);
	this->setMaximum(100);
	this->setAlignment(Qt::AlignHCenter);
#endif
}


iqtWidgetProgressBarSubject::~iqtWidgetProgressBarSubject()
{
}


void iqtWidgetProgressBarSubject::SetProgress(int progress)
{
#if defined(QT_THREAD_SUPPORT) && defined(IQT_3)
	if(!qApp->tryLock()) return;
#endif
#ifdef IQT_3
	QProgressBar::setProgress(progress);
#else
	QProgressBar::setValue(progress);
#endif
#if defined(QT_THREAD_SUPPORT) && defined(IQT_3)
	qApp->unlock();
#endif
	qApp->flush();
}


void iqtWidgetProgressBarSubject::ResetBody()
{
	QProgressBar::reset();
}


#ifdef IQT_3

bool iqtWidgetProgressBarSubject::setIndicator(QString &indicator, int progress, int totalSteps)
{
    if(mLabel.IsEmpty() || progress<0)
	{
		return QProgressBar::setIndicator(indicator,progress,totalSteps);
	}
	else 
	{
		if(mModified)
		{
			indicator = iqtHelper::Convert(mLabel);
			return true;
		}
		else return false;
    }
}

#else

QString iqtWidgetProgressBarSubject::text() const
{
    if(mLabel.IsEmpty() || this->value()<0)
	{
		return QProgressBar::text();
	}
	else 
	{
		return iqtHelper::Convert(mLabel);
    }
}

#endif

#endif
