/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A Qt-based subject class that creates menus and toolbars
//

#ifndef IQTMENUWINDOWSUBJECT_H
#define IQTMENUWINDOWSUBJECT_H


#include "iqt.h"

#ifdef IQT_3
#include <qaction.h>
#include <qmainwindow.h>
#include <qpopupmenu.h>
#define QMenu QPopupMenu
#elif defined(IQT_4)
#include <QtGui/QAction>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#else
#include <QtWidgets/QAction>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#endif
#include "ibgmenuwindowsubject.h"


#include "iarray.h"

class iqtMenu;
class iqtMenuWindowSubject;

class QActionGroup;
class QToolBar;


//
//  helper class - it is not hidden so that only this file needs to be MOCed
//
class iqtMenuItem : public QAction
{

	Q_OBJECT

public:

	iqtMenuItem(int id, iqtMenuWindowSubject *subject, const iggMenuHelperBase *enabler, const iggMenuHelperBase *checker);
	~iqtMenuItem();

	void Update();
	inline int GetId() const { return mId; }

signals:

	void VoidAction(int id);
	void BoolAction(int id, bool v);

protected slots:

	void OnActivated();
	void OnToggled(bool v);

protected:

	int mId;
	iggMenuHelperBase *mEnabler, *mChecker;
	iqtMenuWindowSubject *mSubject;
};


//
//  helper class
//
class iqtMenu : public QMenu
{

public:

	iqtMenu(iqtMenuWindowSubject *subject, const iString &text, bool exclusive);
	iqtMenu(iqtMenu *owner, const iString &text, bool exclusive);

	QActionGroup* GetGroup() const { return mGroup; }
	iqtMenuWindowSubject* GetSubject() const { return mSubject; }
	iqtMenu* GetParent() const { return mParent; }

	void AddMenuItem(iqtMenuItem *item);
	void AddSeparator();

protected:

	iqtMenu *mParent;
	iqtMenuWindowSubject *mSubject;

private:

	void Define(bool exclusive);
	QActionGroup *mGroup;
};


//
//
//  Main class
//
class iqtMenuWindowSubject : public QMainWindow, public ibgMenuWindowSubject
{

	Q_OBJECT

	friend class iggSubjectFactory;

public:

	virtual void BeginMenu(const iString &text, bool exclusive);
	virtual void EndMenu();
	virtual void AddMenuItem(int id, const iString &text, const iImage *icon, const iString &accel, bool toggle, bool on, const iggMenuHelperBase *enabler = 0, const iggMenuHelperBase *checker = 0);
	virtual void AddMenuSeparator();
	virtual void AddToolBarButton(int id, const iString &tooltip, const iImage *icon, bool toggle, const iggMenuHelperBase *enabler = 0, const iggMenuHelperBase *checker = 0);
	virtual void AddToolBarSeparator();
	virtual void CompleteMenu();

	virtual void SetToolBarIcon(int id, const iImage &icon);
	virtual void UpdateMenus();

	virtual void SetGlobalFrame(iggFrameTopParent *globalFrame, int cols);

	virtual iString GetFileName(const iString &header, const iString &file, const iString &selection, bool reading);

protected:

	iqtMenuWindowSubject(iggMenuWindow *owner, const iImage *icon, const iString &title);
	virtual ~iqtMenuWindowSubject();

	//
	//  Menu manipulation
	//
	iArray<iqtMenuItem*> mMenuItems;
	iArray<iqtMenuItem*> mToolBarButtons;

	//
	//  Qt-specific
	//
	virtual bool event(QEvent *e);
	virtual void closeEvent(QCloseEvent *e);

protected slots:

	void OnMenuVoid(int id);
	void OnMenuBool(int id, bool v);

private:

	QToolBar *mToolBar;
	iqtMenu *mCurrentMenu;

	iqtMenuWindowSubject(const iqtMenuWindowSubject&); // Not implemented.
	void operator=(const iqtMenuWindowSubject&); // Not implemented.
};

#endif  // IQTMENUWINDOWSUBJECT_H
