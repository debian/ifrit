/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_QT)


#include "iqtwidgettrackballsubject.h"


#include "ierror.h"
#include "imath.h"
#include "itransform.h"
#include "iviewmodule.h"

#include "iggimagefactory.h"
#include "iggshell.h"
#include "iggwidget.h"
#include "iggwidgetrendermodebutton.h"

#include "iqthelper.h"
#include "iqtwidgetbuttonsubject.h"
#include "iqtwidgethelper.h"

#include <vtkCamera.h>
#include <vtkMath.h>
#include <vtkRenderer.h>

#ifdef IQT_3
#include <qbitmap.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qpainter.h>
#include <qtoolbutton.h>
#else
#include <QtGui/QBitmap>
#include <QtGui/QMouseEvent>
#include <QtGui/QPainter>
#ifdef IQT_4
#include <QtGui/QGridLayout>
#include <QtGui/QLabel>
#include <QtGui/QToolButton>
#else
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QToolButton>
#endif
#endif

//
//  Templates
//
#include "iarray.tlh"


namespace iqtWidgetTrackBall_Private
{
	const double PI = 3.1415926535897932384626433;
	const double TWOPI = 2*PI;
};


using namespace iqtWidgetTrackBall_Private;

//
// Constructs a iTrackball.
//
iqtWidgetTrackBallSubject::iqtWidgetTrackBallSubject(iggWidget *owner, bool followCamera) : QWidget(iqtHelper::Convert(owner->GetParent())
#ifdef IQT_3
																																	,0,Qt::WRepaintNoErase
#endif
																																	), ibgWidgetTrackBallSubject(owner,followCamera)
{
	mWidgetHelper = new iqtWidgetHelper(this,owner); IERROR_CHECK_MEMORY(mWidgetHelper);
	//
	//  Create a layout
	//
	if(this->layout() != 0) delete this->layout();
	QGridLayout *l = iqtHelper::NewLayout(this,1);

	mPixmap = new QPixmap(this->size());
	mPixmapMask = new QBitmap(this->size());
	mPainter = new QPainter;

	this->setMaximumSize(mEarth.NumLat,mEarth.NumLat);
	this->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding));
	this->setMinimumSize(mFixedSize,mFixedSize);

#ifdef IQT_3
	l->expand(2,3);
#endif
	
	QToolButton *db = new QToolButton(this);
#ifdef IQT_3
	db->setPixmap(iqtHelper::ConvertToPixmap(*iggImageFactory::FindIcon("data.png")));
#else
	const iImage &icon(*iggImageFactory::FindIcon("data.png"));
	QIcon ic = iqtHelper::ConvertToIcon(icon);
	db->setIcon(ic);
	if(db->minimumSize() == db->maximumSize())
	{
		db->setIconSize(QSize(icon.Width(),icon.Height()));
	}
#endif
	//db->setAutoRaise(true);
	if(!this->connect(db,SIGNAL(clicked()),iDynamicCast<iqtWidgetHelper,ibgWidgetHelper>(INFO,mWidgetHelper),SLOT(OnVoid1()))) IBUG_WARN("Missed connection.");

	l->addWidget(db,0,0);
#ifdef IQT_3
	l->setColStretch(1,10);
#else
	l->setColumnStretch(1,10);
#endif
	l->setRowStretch(1,10);

	mInUpdate = false;
}


iqtWidgetTrackBallSubject::~iqtWidgetTrackBallSubject()
{
	while(mTimerIds.Size() > 0) this->killTimer(mTimerIds.RemoveLast());

	delete mPixmapMask;
	delete mPixmap;
	delete mPainter;
}


void iqtWidgetTrackBallSubject::GetSizes(int &w0, int &h0, int &rad, int &cor)
{
	w0 = this->width()/2 - 1;
	h0 = this->height()/2 - 1;
	if(mResizable) rad = (w0 < h0) ? w0 : h0; else rad = mFixedSize/2 - 1;
	cor = mFixedSize/8;
}


void iqtWidgetTrackBallSubject::AddButton(ibgWidgetButtonSubject* button)
{
	if(button != 0)
	{
		QGridLayout *l = iDynamicCast<QGridLayout,QLayout>(INFO,this->layout());
		iqtWidgetButtonSubject *bs = iDynamicCast<iqtWidgetButtonSubject,ibgWidgetButtonSubject>(INFO,button);
		iqtHelper::SetParent(bs,this);
		l->addWidget(bs,0,2);
	}
}


void iqtWidgetTrackBallSubject::Repaint()
{
	this->update();
}


void iqtWidgetTrackBallSubject::mousePressEvent(QMouseEvent *e)
{
	int w0, h0, rad, cor;
	this->GetSizes(w0,h0,rad,cor);

	if(e!=0 && e->button()==Qt::LeftButton)
	{
		mOldX = e->x();
		mOldY = e->y();

		if(mOldX>w0+rad-cor && mOldY>h0+rad-cor)
		{
			mResizable = !mResizable;
			this->layout()->invalidate();
			this->update();
#ifdef IQT_3
			this->erase();
#endif
		}
		//iDynamicCast<iqtWidgetHelper,ibgWidgetHelper>(INFO,mWidgetHelper)->OnVoid1();
	}

	if(e!=0 && e->button()==Qt::RightButton)
	{
		mOldX = e->x();
		mOldY = e->y();
		mTimerIds.Add(this->startTimer(100));
	}
}


void iqtWidgetTrackBallSubject::mouseReleaseEvent(QMouseEvent *e)
{
	if(e!=0 && (e->button()==Qt::LeftButton || e->button()==Qt::RightButton))
	{
		iDynamicCast<iqtWidgetHelper,ibgWidgetHelper>(INFO,mWidgetHelper)->OnBool1(true);
		//iDynamicCast<iqtWidgetHelper,ibgWidgetHelper>(INFO,mWidgetHelper)->OnVoid2();
		if(e->button() == Qt::RightButton)
		{
			while(mTimerIds.Size() > 0) this->killTimer(mTimerIds.RemoveLast());
		}
	}
}

	
void iqtWidgetTrackBallSubject::mouseMoveEvent(QMouseEvent *e)
{
	if(e == 0) return;
		
	this->UpdateTransform(e->x(),e->y());
	iDynamicCast<iqtWidgetHelper,ibgWidgetHelper>(INFO,mWidgetHelper)->OnBool1(false);
}


void iqtWidgetTrackBallSubject::timerEvent(QTimerEvent *e)
{
	if(e == 0) return;

	int x0 = mOldX;
	int y0 = mOldY;
	mOldX = this->width()/2 - 1;
	mOldY = this->height()/2 - 1;

	int x = mOldX + (x0-mOldX)/3;  // slow down the motion
	int y = mOldY + (y0-mOldY)/3;

	this->UpdateTransform(x,y);
	iDynamicCast<iqtWidgetHelper,ibgWidgetHelper>(INFO,mWidgetHelper)->OnBool1(false);

	mOldX = x0;
	mOldY = y0;
}


void iqtWidgetTrackBallSubject::paintEvent(QPaintEvent * )
{
	if(mInUpdate) return;

	mInUpdate = true;

	vtkLinearTransform *ct = 0;
	int i, j;
	int w0, h0, rad, cor;
	this->GetSizes(w0,h0,rad,cor);

	if(!mFollowCamera)
	{
		ct = mOwner->GetShell()->GetViewModule()->GetRenderer()->GetActiveCamera()->GetViewTransformObject()->GetLinearInverse();
	}

	//
	//  Expansion triangle parameters
	//
#ifdef IQT_3
	QPointArray t(3);
#else
	QPolygon t(3);
#endif
	t.setPoint(0,w0+rad-cor,h0+rad);
	t.setPoint(1,w0+rad,h0+rad-cor);
	if(mResizable)
	{
		t.setPoint(2,w0+rad-cor,h0+rad-cor);
	}
	else
	{
		t.setPoint(2,w0+rad,h0+rad);
	}
	//
	//   Mini-renderer
	//
#ifdef IQT_3
	if(mDoubleBuffer)
	{
		if(mPixmap->size() != this->size())
		{
			mPixmap->resize(this->size());
			mPixmapMask->resize(this->size());
			mOldRad = 0;
		}

		if(mOldRad != rad)
		{
			//
			//  Create mask
			//
			mPixmap->fill(this->foregroundColor());
			mPixmapMask->fill(Qt::color0);
			mPainter->begin(mPixmapMask);
			mPainter->setPen(Qt::color1);
			mPainter->setBrush(Qt::color1);
			mPainter->drawEllipse(w0-rad,h0-rad,2*rad+1,2*rad+1);
			//
			//  Expansion triangle
			//
			mPainter->drawPolygon(t);
			mPainter->end();
			mPixmap->setMask(*mPixmapMask);

			mOldRad = rad;
		}
		mPainter->begin(mPixmap);
	}
	else
#endif
	{
		mPainter->begin(this);
	}

	QColor c;
	double r2, lon, lat, dimFactor, n0[3], n[3];
	for(i=-rad; i<=rad; i++)
	{
		n0[0] = double(i)/rad;
		for(j=-rad; j<=rad; j++)
		{
			n0[1] = double(j)/rad;
			r2 = n0[0]*n0[0] + n0[1]*n0[1];
			if(r2 <= 1.0)
			{
				n0[2] = sqrt(1.0-r2);
				
				if(this->isEnabled())
				{
					mBallTransform->TransformVector(n0,n);
					if(ct != 0)	ct->TransformVector(n,n);

					lat = asin(n[2]);
					lon = atan2(n[1],n[0]);
					
					//
					//  Unmodified color
					//
					c = iqtHelper::Convert(this->ColorAtLonLat(lon,lat)); 
				}
				else
				{
					c = Qt::gray;
				}
				
				//
				//  Light calculation
				//
				dimFactor = vtkMath::Dot(mLightDirection,n0);
				if(dimFactor < 0.1) dimFactor = (double)0.1;
				c.setRgb(iMath::Round2Int(c.red()*dimFactor),iMath::Round2Int(c.green()*dimFactor),iMath::Round2Int(c.blue()*dimFactor));
				
				//
				//  Draw the scene
				//
				mPainter->setPen(c);
				mPainter->drawPoint(w0+i,h0-j);
			}
		}
	}
	
#ifdef IQT_3
	if(mDoubleBuffer)
	{
		bitBlt(this,w0-rad,h0-rad,mPixmap,w0-rad,h0-rad,2*rad+1,2*rad+1,Qt::CopyROP,false); 
	}
	else
#endif
	{
		//
		//  Expansion triangle
		//
#ifdef IQT_3
		mPainter->setBrush(this->foregroundColor());
#else
		mPainter->setBrush(this->palette().color(this->backgroundRole()));
#endif
		mPainter->drawPolygon(t);
	}

	mPainter->end();

	mInUpdate = false;
}

#endif
