/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Qt-implementation of ibgFrameSubject with explicit parent (used in iqtMainWindow)
//

#ifndef IQTEXPLICITFRAMESUBJECT_H
#define IQTEXPLICITFRAMESUBJECT_H


#include "iqt.h"

#ifdef IQT_3
#include <qgroupbox.h>
#elif defined(IQT_4)
#include <QtGui/QGroupBox>
#else
#include <QtWidgets/QGroupBox>
#endif
#include "ibgframesubject.h"


class QGridLayout;


class iqtExplicitFrameSubject : public QGroupBox, public ibgFrameSubject
{

	friend class iqtExtensionWindow;
	friend class iqtMainWindow;

public:

	iqtExplicitFrameSubject(QWidget *qparent, iggFrame *owner, int cols); // public, so that it can be created by other classes
	virtual ~iqtExplicitFrameSubject();
	
	virtual void SetTitle(const iString &title);
	virtual void ShowFrame(bool s);

	virtual void SetPadding(bool s);
	virtual void SetColStretch(int col, int s);
	virtual void SetRowStretch(int row, int s);
	virtual void PlaceWidget(int col, int row, ibgWidgetSubject *child, int nc, bool expand);

	QGridLayout* GetLayout() const { return mLayout; }
	virtual void GetFrameGeometry(int wg[4]) const;

	//
	//  QT-specific
	//
	virtual void show();

protected:

	virtual void showEvent(QShowEvent *e);
	virtual void paintEvent(QPaintEvent *e);

	QGridLayout *mLayout;
	int mNumCols, mNumRows;

private:

	void Qt3Fix();
};

#endif  // IQTEXPLICITFRAMESUBJECT_H

