/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A Qt-based widget helper
//

#ifndef IQTWIDGETHELPER_H
#define IQTWIDGETHELPER_H


#include "iqt.h"

#ifdef IQT_3
#include <qobject.h>
#else
#include <QtCore/QObject>
class QAbstractButton;
#endif
#include "ibgwidgethelper.h"


class iqtWidgetBaloonHelp;

class QWidget;


class iqtWidgetHelper : public QObject, public ibgWidgetHelper
{

	Q_OBJECT

	friend class iqtWidgetBaloonHelp;

public:

	iqtWidgetHelper(QWidget *widget, iggWidget *owner);
	virtual ~iqtWidgetHelper();

	virtual void SetBaloonHelp(const iString &tooltip, const iString &whatsthis);
	virtual bool SupportsHTMLHelp() const;
	virtual bool HasBaloonHelp() const;

	virtual void Show(bool s);
	virtual bool IsVisible() const;

	virtual void Enable(bool s);
	virtual bool IsEnabled() const;

	virtual void SetBackgroundColor(const iColor &c);
	virtual void UnSetBackgroundColor();

	inline QWidget* GetWidget() const { return mWidget; }

	void SetDisabling(bool s){ mIsDisabling = s; }

protected:
	
	iqtWidgetBaloonHelp  *mBaloonHelp;
	QWidget *mWidget;

public slots:

	//
	//  Generic slots
	//
	virtual void OnInt1(int);
	virtual void OnInt2(int);
	virtual void OnVoid1();
	virtual void OnVoid2();
	virtual void OnVoid3();
	virtual void OnBool1(bool);
	virtual void OnButtonClicked(QAbstractButton * );

private:

	bool mIsDisabling;

	iqtWidgetHelper(const iqtWidgetHelper &);
	void operator=(const iqtWidgetHelper &);
};

#endif  // IQTWIDGETHELPER_H

