/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_QT)


#include "iqtdialogsubject.h"


#include "ierror.h"

#include "iggdialog.h"
#include "iggframe.h"
#include "iggmainwindow.h"

#include "iqtframesubject.h"
#include "iqthelper.h"
#include "iqtmainwindowsubject.h"
#include "iqtwindowhelper.h"

#ifdef IQT_3
#include <qdialog.h>
#include <qlayout.h>
#include <qpushbutton.h>
#include <qtooltip.h>
#else
#include <QtGui/QCloseEvent>
#ifdef IQT_4
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QPushButton>
#include <QtGui/QToolTip>
#else
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QToolTip>
#endif
#endif


using namespace iParameter;


//
//  Main class
//
iqtDialogSubject::iqtDialogSubject(iggDialog *owner, const ibgWindowSubject *base, unsigned int mode, const iImage *icon, const iString &title) : 
#ifdef IQT_3
	QDialog((mode&DialogFlag::Unattached)?0:iDynamicCast<iqtWindowHelper,ibgWindowHelper>(INFO,base->GetHelper())->GetWidget(),"",false,iqtHelper::GetFlags(WindowType::Dialog,mode)), 
#else
	QDialog((mode&DialogFlag::Unattached)?0:iDynamicCast<iqtWindowHelper,ibgWindowHelper>(INFO,base->GetHelper())->GetWidget(),iqtHelper::GetFlags(WindowType::Dialog,mode)), 
#endif
	ibgDialogSubject(owner,base,mode,icon,title)
{
	mWindowHelper = new iqtWindowHelper(this,icon,title); IERROR_CHECK_MEMORY(mWindowHelper);

	mMode = mode;
	QGridLayout *layout = iqtHelper::NewLayout(this,1);

	if((mode & DialogFlag::Blocking) == 0) this->setSizeGripEnabled(true); else
	{
#ifndef IQT_3
		 this->setAttribute(Qt::WA_ShowModal); 
#endif
	}
	if((mode & DialogFlag::NoTitleBar) != 0)
	{
		this->setFocusPolicy(QFocus::NoFocus);
		iqtHelper::SetBackground(this,0);
	}
}


iqtDialogSubject::~iqtDialogSubject()
{
}


void iqtDialogSubject::Show(bool s)
{
	if(s)
	{
		if(mMode & DialogFlag::WithEventLoop)
		{
			this->exec();
		}
		else
		{
			this->show();
			if(!this->isHidden()) this->raise();
		}
	}
	else this->hide();
}


void iqtDialogSubject::SetFrame(ibgFrameSubject *frame0)
{
	iqtFrameSubject *frame = iDynamicCast<iqtFrameSubject,ibgFrameSubject>(INFO,frame0);

	if(frame->parentWidget() != this)
	{
		iqtHelper::SetParent(frame,this);
		iDynamicCast<QGridLayout,QLayout>(INFO,this->layout())->addWidget(frame,0,0);
	}
	if((mMode & DialogFlag::NoTitleBar) != 0)
	{
#ifdef IQT_3
		frame->setFrameShape(QFrame::Box);
		frame->setFrameShadow(QFrame::Raised);
		frame->setLineWidth(3);
		frame->setMidLineWidth(2);
#else
		frame->setFlat(false);
#endif
		frame->layout()->setMargin(5);
	}
}


void iqtDialogSubject::closeEvent(QCloseEvent *e)
{
	this->OnClose();
	e->ignore();
}


void iqtDialogSubject::keyPressEvent(QKeyEvent *e)
{
	//
	//  Block closing of dialogs on Enter
	//
	if(e->key() == Qt::Key_Return) e->accept(); else QDialog::keyPressEvent(e);
}

#endif
