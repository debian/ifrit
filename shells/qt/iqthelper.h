/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A class miscellaneous helping functions for IFrIT-Qt conversions and porting. Basically, a mixed bag
//

#ifndef IQTHELPER_H
#define IQTHELPER_H


#include "iqt.h"


#ifdef IQT_3
#include <qnamespace.h>
#else
#include <QtCore/Qt>
#endif

class iColor;
class iImage;
class iString;

class iggFrameBase;

class QColor;
class QFont;
class QGridLayout;
class QImage;
class QLayout;
class QPaintEvent;
class QPixmap;
class QString;
class QWidget;


namespace iParameter
{
	//
	//  Specify widget flags in a uniform way in one place
	//
	namespace WindowType
	{
		const int Main =				0;
		const int Render =				1;
		const int BorderlessRender =	2;
		const int Extension =			3;
		const int Dialog =				4;
	};
};


class iqtHelper
{

public:

	static Qt::WFlags GetFlags(int window, int mode = 0);

	//
	//  Porting helpers
	//
	static void SetBackground(QWidget *widget, const QColor &color);
	static void SetBackground(QWidget *widget, const QPixmap &pixmap);
	static void SetBackground(QWidget *widget, int mode);

	static void SetParent(QWidget *widget, QWidget *parent, Qt::WFlags f = 0);
	static void SetFont(QWidget *widget, QFont &f);

	static QGridLayout* NewLayout(QWidget *parent, int nCols);
	static QGridLayout* NewLayout(QLayout *parentLayout, int nCols);

	//
	//  Color conversion
	//
	static iColor Convert(const QColor &c);
	static QColor Convert(const iColor &c);

	//
	//  String conversion
	//
	static iString Convert(const QString &s);
	static QString Convert(const iString &s);
	static QString ConvertWithModifiers(QWidget *w, const iString &s);

	//
	//  Image conversion
	//
	static iImage Convert(const QImage &i);
	static QImage Convert(const iImage &i, float scale = 1.0f);
	static QPixmap ConvertToPixmap(const iImage &i, bool withMask = true);
	static QIcon ConvertToIcon(const iImage &i);

	//
	//  Mouse button conversion
	//
	static int Convert(int state);

	//
	//  Widget conversion
	//
	static QWidget* Convert(const iggFrameBase *w);
};

#endif  // IQTHELPER_H

