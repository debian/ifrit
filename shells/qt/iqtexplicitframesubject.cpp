/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_QT)


#include "iqtexplicitframesubject.h"


#include "ierror.h"

#include "iggframe.h"

#include "iqthelper.h"
#include "iqtwidgethelper.h"

#ifdef IQT_3
#include <qlayout.h>
#else
#ifdef IQT_4
#include <QtGui/QGridLayout>
#else
#include <QtWidgets/QGridLayout>
#endif
#endif


//
//  Main class
//
iqtExplicitFrameSubject::iqtExplicitFrameSubject(QWidget *qparent, iggFrame *owner, int cols) : QGroupBox(qparent), ibgFrameSubject(owner,cols)
{
	if(cols < 1)
	{
		IBUG_WARN("Incorrect number of columns.");
		cols = 1;
	}
	if(cols > 7)
	{
		IBUG_WARN("Current implementation does not support more than 7 columns.");
		cols = 7;
	}

	mNumCols = cols;
	mNumRows = 1;

#ifdef IQT_3
	this->setFrameShape(QFrame::NoFrame);
	this->setOrientation(Qt::Vertical);
	this->setInsideMargin(0);
#else
	this->setFlat(true);
#endif
	this->setSizePolicy(QSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum));

	if(this->layout() == 0)
	{
		mLayout = iqtHelper::NewLayout(this,cols);
	}
	else
	{
		mLayout = iqtHelper::NewLayout(this->layout(),cols);
	}

	mWidgetHelper = new iqtWidgetHelper(this,owner); IERROR_CHECK_MEMORY(mWidgetHelper);
}


iqtExplicitFrameSubject::~iqtExplicitFrameSubject()
{
}


void iqtExplicitFrameSubject::SetTitle(const iString &title)
{
	this->setTitle(iqtHelper::ConvertWithModifiers(this,title));
	this->layout()->invalidate(); // Qt bug!!! 
}


void iqtExplicitFrameSubject::ShowFrame(bool s)
{
#ifdef IQT_3
	if(s) this->setFrameShape(s?QFrame::Box:QFrame::NoFrame);
#else
	this->setFlat(!s);
#endif
	this->layout()->invalidate(); // Qt bug!!! 
}


void iqtExplicitFrameSubject::GetFrameGeometry(int wg[4]) const
{
	QPoint p = this->mapToGlobal(this->pos());

	wg[0] = p.x();
	wg[1] = p.y();
	wg[2] = this->width();
	wg[3] = this->height();
}


//
//  Layout helpers
//
void iqtExplicitFrameSubject::SetPadding(bool s)
{
	if(s)
	{
		mLayout->setMargin(mPaddingWidth*2);
		mLayout->setSpacing(mPaddingWidth);
	}
	else
	{
		mLayout->setMargin(0);
		mLayout->setSpacing(0);
	}
	mLayout->invalidate();
	this->updateGeometry();
}


void iqtExplicitFrameSubject::SetColStretch(int col, int s)
{
#ifdef IQT_3
	if(mLayout->numCols() != mNumCols)
	{
		IBUG_WARN("Corrupted layout.");
	}
	if(col>=0 && col<mNumCols) mLayout->setColStretch(col,s);
#else
	if(col>=0 && col<mNumCols) mLayout->setColumnStretch(col,s);
#endif
}


void iqtExplicitFrameSubject::SetRowStretch(int row, int s)
{
#ifdef IQT_3
	if(mLayout->numRows() != mNumRows)
	{
		IBUG_WARN("Corrupted layout.");
	}
	if(row>=0 && row<mNumRows) mLayout->setRowStretch(row,s);
#else
	if(row>=0 && row<mNumRows) mLayout->setRowStretch(row,s);
#endif
}


void iqtExplicitFrameSubject::PlaceWidget(int col, int row, ibgWidgetSubject *child, int nc, bool expand)
{
#ifdef IQT_3
	if(expand && row>=mNumRows)
	{
		mLayout->expand(row+1,mLayout->numCols());
		mNumRows = mLayout->numRows();
	}
#else
	if(expand && row>=mNumRows)	mNumRows = row + 1;
#endif

	if(child == 0) return;

	if(nc>0 && col>=0 && col+nc<=mNumCols && row>=0 && row<mNumRows)
	{
		//
		//  First we need to reach QWidget hidden in ibgWidgetBase - well hidden too!!!
		//
		iqtWidgetHelper *tmp = iDynamicCast<iqtWidgetHelper,ibgWidgetHelper>(INFO,child->GetWidgetHelper());
#ifdef IQT_3
		mLayout->addMultiCellWidget(tmp->GetWidget(),row,row,col,col+nc-1);
		this->clearWFlags(WState_Polished);
#else
		mLayout->addWidget(tmp->GetWidget(),row,col,1,nc);
#endif
	}
	else
	{
		IBUG_WARN("Invalid layout cell.");
	}
}


//
//  QT-specific
//
void iqtExplicitFrameSubject::show()
{
	if(this->BeforeShow())
	{
#ifdef IQT_3
		this->Qt3Fix();
#endif
	}
	QGroupBox::show();
}


void iqtExplicitFrameSubject::showEvent(QShowEvent *e)
{
	if(this->BeforeShow())
	{
#ifdef IQT_3
		this->Qt3Fix();
#endif
	}
    QGroupBox::showEvent(e);
	mFrameOwner->UpdateWidget();
}


void iqtExplicitFrameSubject::paintEvent(QPaintEvent *e)
{
#ifdef IQT_3
	this->QGroupBox::paintEvent(e);
#else
	if(!this->isFlat()) // bug fixing
	{
		this->QGroupBox::paintEvent(e);
	}
#endif
}


#ifdef IQT_3
#include <qapplication.h>

void iqtExplicitFrameSubject::Qt3Fix()
{
	qApp->processEvents();
}
#endif

#endif
