/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Nontrivial classes defined in this file
//
//  iqtWidgetDisplayAreaSubject: element that shows text or a single image;
//  iqtWidgetDrawAreaSubject: element that allows painting on itself and responds to mouse interaction;
//  iqtWidgetScrollAreaSubject: element that contains an area with scroll-bars and a FrameBase inside
//

#ifndef IQTWIDGETAREASUBJECT_H
#define IQTWIDGETAREASUBJECT_H


#include "iqt.h"

#ifdef IQT_3
#include <qframe.h>
#include <qlabel.h>
#include <qpixmap.h>
#else
#include <QtGui/QPixmap>
#ifdef IQT_4
#include <QtGui/QFrame>
#include <QtGui/QLabel>
#else
#include <QtWidgets/QFrame>
#include <QtWidgets/QLabel>
#endif
#endif
#include "ibgwidgetareasubject.h"

class QPainter;
class QTimer;


class iqtWidgetDisplayAreaSubject : public QLabel, public ibgWidgetDisplayAreaSubject
{

	friend class iggSubjectFactory;

public:
	
	//
	//  Decorator functions
	//
	virtual void SetText(const iString &text);
	virtual void SetImage(const iImage *image, bool withMask = true, bool scaled = false);
	virtual void SetAlignment(int s);
	virtual void ShowFrame(bool s);

protected:

	iqtWidgetDisplayAreaSubject(iggWidget *owner, const iString &text);
};


class iqtWidgetMultiImageDisplayAreaSubject : public QLabel, public ibgWidgetMultiImageDisplayAreaSubject
{

	friend class iggSubjectFactory;

public:

	virtual ~iqtWidgetMultiImageDisplayAreaSubject();
	
protected:

	class qtImage : public Image
	{
	public:
		qtImage(QPixmap &p){ Pixmap = p; }
		virtual ~qtImage(){ }
		QPixmap Pixmap;
	};

	iqtWidgetMultiImageDisplayAreaSubject(iggWidget *owner);

	virtual Image* AddImageBody(const iImage &image, bool withmask, bool first);
    virtual void ShowImageBody(int n);
	virtual void Start();
	virtual void Abort();

	QTimer *mTimer;
};


class iqtWidgetDrawAreaSubject : public QFrame, public ibgWidgetDrawAreaSubject
{

	friend class iggSubjectFactory;

public:

	virtual ~iqtWidgetDrawAreaSubject();
	
	//
	//  Decorator functions
	//
	virtual int Width() const;
	virtual int Height() const;

	virtual void SetMinimumSize(int w, int h);
	
	virtual void Begin(int m, bool clear);
	virtual void End();
	virtual void Frame(); // show the image

	virtual void Clear();

	virtual void DrawLine(int x1, int y1, int x2, int y2, const iColor &color, int width = 1, bool dotted = false);
	virtual void DrawRectangle(int x1, int y1, int x2, int y2, const iColor &color, const iColor& fillColor, int width = 1);
	virtual void DrawEllipse(int x1, int y1, int x2, int y2, const iColor &color, const iColor& fillColor, int width = 1);
	virtual void DrawImage(int x1, int y1, int x2, int y2, const iImage &image, iImage::ScaleMode mode);
	virtual void DrawTextLine(int x1, int y1, int x2, int y2, const iString &text, const iColor &color);

	virtual void BlendForegroundOntoBackground(float opacity, const iColor &transparent = iColor(255,255,255));

	virtual void SetFixedSize(int w, int h);

protected:

	iqtWidgetDrawAreaSubject(iggWidgetDrawArea *owner, bool interactive);

	virtual void RequestPaintingBody();

	int mOffsetY;
	QPainter *mPainter;
	QPixmap *mForegroundPixmap, *mBackgroundPixmap;

	//
	//  Qt specific
	//
	virtual void paintEvent(QPaintEvent *e);
    virtual void mouseMoveEvent(QMouseEvent *e);
    virtual void mousePressEvent(QMouseEvent *e);
    virtual void mouseReleaseEvent(QMouseEvent *e);
};

#endif  // IQTWIDGETAREASUBJECT_H

