/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Qt-bases Text editor widget subject
//

#ifndef IQTWIDGETTEXTEDITORSUBJECT_H
#define IQTWIDGETTEXTEDITORSUBJECT_H


#include "iqt.h"

#ifdef IQT_3
#include <qtextedit.h>
#elif defined(IQT_4)
#include <QtGui/QTextEdit>
#else
#include <QtWidgets/QTextEdit>
#endif
#include "ibgwidgettexteditorsubject.h"


class iggWidgetTextEditor;

class iqtLocationHelper;

class QEvent;
class QKeyEvent;
class QMouseEvent;
class QTextEdit;


class iqtWidgetTextEditorSubject : public QTextEdit, public ibgWidgetTextEditorSubject
{

	Q_OBJECT

	friend class iggSubjectFactory;

public:

	virtual ~iqtWidgetTextEditorSubject();

	virtual bool AppendBreaksLine() const;
	virtual bool SupportsHTML() const;
	virtual bool TextModified() const;

	//
	//  Text manipulations
	//
	virtual void Clear();
	virtual void SetModified(bool s);
	virtual void SetText(const iString &text);
	virtual iString GetText() const;
	virtual void AppendText(const iString &text, bool scroll = true);
	virtual void SetReadOnly(bool s);

	//
	//  Line-by-line manipulation
	//
	virtual int GetNumberOfLines() const;
	virtual void RemoveLine(int n);
	virtual iString GetLine(int n) const;

	//
	//  Text attribute manipulations
	//
	virtual void SetBoldText(bool s);
	virtual void SetTextColor(const iColor &color);
	virtual void SetLineColor(int line, const iColor &color);
	virtual void AdjustFontSize(int s);
	virtual void UnSetLineColor(int line);
	virtual iColor GetLineColor(int line) const;

	//
	//  Selection & cursor location
	//
	virtual void Select(int lineFrom, int indexFrom, int lineTo, int indexTo, bool leftJustified = false, int selNum = 0);
	virtual void GetCursorPosition(int &line, int &index);
	virtual void SetCursorPosition(int line, int index);

	//
	//  Editing functions
	//
	virtual bool HasEditingFunction(int type) const;
	virtual void UseEditingFunction(int type);

	//
	//  Search & find
	//
	virtual bool Find(const iString &text, bool cs, bool wo, bool forward, int &line, int &index);

protected:

	iqtWidgetTextEditorSubject(iggWidgetTextEditor *owner, unsigned int mode);

	//
	//  Qt specific
	//
	virtual void enterEvent(QEvent *e);
	virtual void keyPressEvent(QKeyEvent *e);
#ifdef IQT_3
	virtual void contentsMouseDoubleClickEvent(QMouseEvent *e);
#else
	virtual void mouseDoubleClickEvent(QMouseEvent *e);
#endif

signals:

	void returnPressed2();

public slots:

	void OnTextChanged();
	void OnSelectionChanged();
	void OnReturnPressed();
	void OnCursorPositionChanged();
	void OnCursorPositionChanged(int,int);

private:

	void GetSelection(int &lineFrom, int &indexFrom, int &lineTo, int &indexTo);

	class Location;

	const int mMode;
};

#endif  //  IQTWIDGETTEXTEDITORSUBJECT_H
