/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Qt-based HTML Browser widget subject
//
#ifndef IQTWIDGETHELPBROWSERSUBJECT_H
#define IQTWIDGETHELPBROWSERSUBJECT_H


#include "iqt.h"

#ifdef IQT_3
#include <qtextbrowser.h>
#elif defined(IQT_4)
#include <QtGui/QTextBrowser>
#else
#include <QtWidgets/QTextBrowser>
#endif
#include "ibgwidgethelpbrowsersubject.h"


class iqtWidgetHelpBrowserSubject : public QTextBrowser, public ibgWidgetHelpBrowserSubject
{

	friend class iggSubjectFactory;

public:

	virtual ~iqtWidgetHelpBrowserSubject();

	virtual void SetText(const iString &text);
	virtual iString GetText() const;

	virtual void AddImage(const iString &name, const iImage &image);

protected:

	iqtWidgetHelpBrowserSubject(iggWidgetHelpBrowser *owner);

	//
	//  Qt specific
	//
#ifdef IQT_3
	virtual void setSource(const QString &name);
	virtual void contentsMouseReleaseEvent(QMouseEvent *e);
#else
	virtual void setSource(const QUrl &name);
	virtual void mouseReleaseEvent(QMouseEvent *e);
	virtual QVariant loadResource(int type, const QUrl &name);
#endif
};

#endif  //  IQTWIDGETHELPBROWSERSUBJECT_H
