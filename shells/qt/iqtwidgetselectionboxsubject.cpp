/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_QT)


#include "iqtwidgetselectionboxsubject.h"


#include "ierror.h"
#include "istring.h"

#include "iggwidget.h"

#include "ibgframesubject.h"

#include "iqthelper.h"
#include "iqtwidgethelper.h"

#ifdef IQT_3
#include <qcombobox.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qlistbox.h>
#include <qradiobutton.h>
#include <qspinbox.h>
#else
#include <QtGui/QStandardItem>
#include <QtGui/QStandardItemModel>
#ifdef IQT_4
#include <QtGui/QAbstractItemView>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QGridLayout>
#include <QtGui/QLabel>
#include <QtGui/QRadioButton>
#include <QtGui/QSpinBox>
#else
#include <QtWidgets/QAbstractItemView>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpinBox>
#endif
#endif


//
//******************************************
//
//  SpinBox
//
//******************************************
//
iqtWidgetSpinBoxSubject::iqtWidgetSpinBoxSubject(iggWidget *owner, int min, int max, const iString &title, int step) : QWidget(iqtHelper::Convert(owner->GetParent())), ibgWidgetSpinBoxSubject(owner,min,max,title,step)
{
	mWidgetHelper = new iqtWidgetHelper(this,owner); IERROR_CHECK_MEMORY(mWidgetHelper);
	//
	//  Create a layout
	//
	if(this->layout() != 0) delete this->layout();
	QGridLayout *l = iqtHelper::NewLayout(this,2);

	mTitle = new QLabel(this);
	mTitle->setText(iqtHelper::ConvertWithModifiers(mTitle,title));
	mTitle->setSizePolicy(QSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum));
	l->addWidget(mTitle,0,0);

#ifdef IQT_3
	mBox = new QSpinBox(min,max,step,this);
#else
	mBox = new QSpinBox(this);
	mBox->setMinimum(min);
	mBox->setMaximum(max);
	mBox->setSingleStep(step);
#endif
	mBox->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,QSizePolicy::Minimum));
	l->addWidget(mBox,0,1);

	this->setSizePolicy(QSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum));
	this->setMinimumHeight(25);

	iqtWidgetHelper *h = iDynamicCast<iqtWidgetHelper,ibgWidgetHelper>(INFO,mWidgetHelper);
	if(h != 0) h->SetDisabling(true);
	if(!this->connect(mBox,SIGNAL(valueChanged(int)),h,SLOT(OnInt1(int)))) IBUG_WARN("Missed connection.");
}


iqtWidgetSpinBoxSubject::~iqtWidgetSpinBoxSubject()
{
}


int iqtWidgetSpinBoxSubject::GetValue() const
{
	return mBox->value();
}


void iqtWidgetSpinBoxSubject::SetValue(int v)
{
	mBox->blockSignals(true);
	mBox->setValue(v);
	mBox->blockSignals(false);
}


void iqtWidgetSpinBoxSubject::SetFirstEntryText(const iString &text)
{
	mBox->setSpecialValueText(iqtHelper::Convert(text));
}


void iqtWidgetSpinBoxSubject::SetRange(int min, int max)
{
	mBox->setRange(min,max);
}


void iqtWidgetSpinBoxSubject::SetStep(int step)
{
#ifdef IQT_3
	mBox->setLineStep(step);
#else
	mBox->setSingleStep(step);
#endif
}


void iqtWidgetSpinBoxSubject::SetStretch(int title, int box)
{
	QGridLayout *l = iDynamicCast<QGridLayout,QLayout>(INFO,this->layout());
#ifdef IQT_3
	l->setColStretch(0,title);
	l->setColStretch(1,box);
#else
	l->setColumnStretch(0,title);
	l->setColumnStretch(1,box);
#endif
}


int iqtWidgetSpinBoxSubject::Count()
{
#ifdef IQT_3
	return mBox->maxValue() - mBox->minValue() + 1;
#else
	return mBox->maximum() - mBox->minimum() + 1;
#endif
}

//
//******************************************
//
//  ComboBox
//
//******************************************
//
iqtWidgetComboBoxSubject::iqtWidgetComboBoxSubject(iggWidget *owner, const iString &title, bool bold) : QWidget(iqtHelper::Convert(owner->GetParent())), ibgWidgetComboBoxSubject(owner,title,bold)
{
	mWidgetHelper = new iqtWidgetHelper(this,owner); IERROR_CHECK_MEMORY(mWidgetHelper);
	//
	//  Create a layout
	//
	if(this->layout() != 0) delete this->layout();
	QGridLayout *l = iqtHelper::NewLayout(this,2);

	mTitle = new QLabel(this);
	mTitle->setText(iqtHelper::ConvertWithModifiers(mTitle,title));
	mTitle->setSizePolicy(QSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum));
	l->addWidget(mTitle,0,0);
	if(title.IsEmpty()) mTitle->hide();

	mBox = new QComboBox(this);
	mBox->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,QSizePolicy::Minimum));
	l->addWidget(mBox,0,1);
	if(bold)
	{
		QFont f = mBox->font();
		f.setBold(true);
		iqtHelper::SetFont(mBox,f);
	}
//	mBox->listBox()->setAutoBottomScrollBar(true); 

	if(!this->connect(mBox,SIGNAL(activated(int)),iDynamicCast<iqtWidgetHelper,ibgWidgetHelper>(INFO,mWidgetHelper),SLOT(OnInt1(int)))) IBUG_WARN("Missed connection.");
//	if(!this->connect(mBox,SIGNAL(highlighted(int)),iDynamicCast<iqtWidgetHelper,ibgWidgetHelper>(INFO,mWidgetHelper),SLOT(OnInt2(int)))) IBUG_WARN("Missed connection.");
}


iqtWidgetComboBoxSubject::~iqtWidgetComboBoxSubject()
{
}


bool iqtWidgetComboBoxSubject::DoesContentFit() const
{
#ifdef IQT_3
	if(mBox->listBox() != 0)
	{
		return mBox->width() > mBox->listBox()->maxItemWidth();
	}
#else
	if(mBox->view() != 0)
	{
		return mBox->width() > mBox->view()->sizeHintForColumn(0);
	}
#endif
	else return true;
}


int iqtWidgetComboBoxSubject::GetValue() const
{
#ifdef IQT_3
	return mBox->currentItem();
#else
	return mBox->currentIndex();
#endif
}


void iqtWidgetComboBoxSubject::SetValue(int v)
{
	mBox->blockSignals(true);
#ifdef IQT_3
	if(v < mBox->count()) mBox->setCurrentItem(v);
#else
	mBox->setCurrentIndex(v);
#endif
	mBox->blockSignals(false);
}


const iString iqtWidgetComboBoxSubject::GetText(int index) const
{
	static const iString none;

	if(index < 0)
	{
		return iqtHelper::Convert(mBox->currentText());
	}
	else if(index < mBox->count())
	{
#ifdef IQT_3
		return iqtHelper::Convert(mBox->text(index));
#else
		return iqtHelper::Convert(mBox->itemText(index));
#endif
	}
	else
	{
		return none;
	}
}


void iqtWidgetComboBoxSubject::SetText(const iString &text)
{
	mBox->blockSignals(true);
#ifdef IQT_3
	mBox->setCurrentText(iqtHelper::Convert(text));
#else
	mBox->setItemText(mBox->currentIndex(),iqtHelper::Convert(text));
#endif
	mBox->blockSignals(false);
}


void iqtWidgetComboBoxSubject::SetStretch(int title, int box)
{
	QGridLayout *l = iDynamicCast<QGridLayout,QLayout>(INFO,this->layout());
#ifdef IQT_3
	l->setColStretch(0,title);
	l->setColStretch(1,box);
#else
	l->setColumnStretch(0,title);
	l->setColumnStretch(1,box);
#endif
}


void iqtWidgetComboBoxSubject::InsertItem(const iString &text, int index)
{
#ifdef IQT_3
	mBox->insertItem(iqtHelper::Convert(text),index);
#else
	if(index < 0) mBox->addItem(iqtHelper::Convert(text)); else mBox->insertItem(index,iqtHelper::Convert(text));
#endif
}


void iqtWidgetComboBoxSubject::InsertItem(const iImage &icon, const iString &text, int index)
{
#ifdef IQT_3
	mBox->insertItem(iqtHelper::ConvertToPixmap(icon),iqtHelper::Convert(text),index);
#else
	if(index < 0) mBox->addItem(iqtHelper::ConvertToIcon(icon),iqtHelper::Convert(text)); else mBox->insertItem(index,iqtHelper::ConvertToIcon(icon),iqtHelper::Convert(text));
#endif
}


void iqtWidgetComboBoxSubject::SetItem(const iString &text, int index, bool vis)
{
	if(index < 0) return;

	if(index < mBox->count())
	{
		if(vis)
		{
			QString s = iqtHelper::Convert(text);
#ifdef IQT_3
			if(!s.isEmpty() && s!=mBox->text(index)) mBox->changeItem(s,index);
			mBox->listBox()->item(index)->setSelectable(true);
#else
			if(!s.isEmpty() && s!=mBox->itemText(index)) mBox->setItemText(index,s);
			QStandardItemModel *m = dynamic_cast<QStandardItemModel*>(mBox->model());
			if(m != 0)
			{
				m->item(index)->setSelectable(true);
				m->item(index)->setForeground(Qt::black);
			}
#endif
		}
		else
		{
#ifdef IQT_3
			mBox->listBox()->item(index)->setSelectable(false);
#else
			QStandardItemModel *m = dynamic_cast<QStandardItemModel*>(mBox->model());
			if(m != 0)
			{
				m->item(index)->setSelectable(false);
				m->item(index)->setForeground(Qt::gray);
			}
#endif
		}
	}
	else
	{
#ifdef IQT_3
		if(vis) mBox->insertItem(iqtHelper::Convert(text));
#else
		if(vis) mBox->addItem(iqtHelper::Convert(text));
#endif
	}
}


void iqtWidgetComboBoxSubject::RemoveItem(int index)
{
	if(index < 0) return;
	mBox->removeItem(index);
}


int iqtWidgetComboBoxSubject::Count()
{
	return mBox->count();
}


void iqtWidgetComboBoxSubject::Clear()
{
	mBox->clear(); 
}


//
//******************************************
//
//  RadioBox
//
//******************************************
//
iqtWidgetRadioBoxSubject::iqtWidgetRadioBoxSubject(iggWidget *owner, int cols, const iString &title) : QRadioBox(iqtHelper::Convert(owner->GetParent())), ibgWidgetRadioBoxSubject(owner,cols,title)
{
	mWidgetHelper = new iqtWidgetHelper(this,owner); IERROR_CHECK_MEMORY(mWidgetHelper);
	//
	//  Create a layout
	//
	if(this->layout() != 0) delete this->layout();
	QGridLayout *l = iqtHelper::NewLayout(this,1);

	if(cols < 0)
	{
		IBUG_WARN("Incorrect number of columns.");
		cols = 1;
	}

#ifdef IQT_3
	if(cols == 0) this->setOrientation(Qt::Horizontal); else this->setColumnLayout(cols,Qt::Horizontal);
#else
	l->setMargin(ibgFrameSubject::GetPaddingWidth());
	mGroup = new QButtonGroup(this);
	mGroup->setExclusive(true);
	if(cols > 1) l->setColumnStretch(cols-1,0);
#endif
	this->setSizePolicy(QSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum));
	this->setTitle(iqtHelper::ConvertWithModifiers(this,title));

#ifdef IQT_3
	if(!this->connect(this,SIGNAL(clicked(int)),iDynamicCast<iqtWidgetHelper,ibgWidgetHelper>(INFO,mWidgetHelper),SLOT(OnInt1(int)))) IBUG_WARN("Missed connection.");
#else
	if(!this->connect(mGroup,SIGNAL(buttonClicked(QAbstractButton * )),iDynamicCast<iqtWidgetHelper,ibgWidgetHelper>(INFO,mWidgetHelper),SLOT(OnButtonClicked(QAbstractButton * )))) IBUG_WARN("Missed connection.");
#endif
}


iqtWidgetRadioBoxSubject::~iqtWidgetRadioBoxSubject()
{
}


int iqtWidgetRadioBoxSubject::GetValue() const
{
#ifdef IQT_3
	return this->id(this->selected());
#else
	return mGroup->buttons().indexOf(mGroup->checkedButton());
#endif
}


void iqtWidgetRadioBoxSubject::SetValue(int v)
{
#ifdef IQT_3
	this->blockSignals(true);
	this->setButton(v);
	this->blockSignals(false);
#else
	QAbstractButton *b = this->FindButton(v);
	if(b != 0)
	{
		b->blockSignals(true);
		b->setChecked(true);
		b->blockSignals(false);
	}
#endif
}


void iqtWidgetRadioBoxSubject::InsertItem(const iString &text, int index)
{
	QRadioButton *r = new QRadioButton(this);
#ifdef IQT_3
	this->insert(r,index);
#else
	mGroup->addButton(r);
	this->layout()->addWidget(r);
#endif
	r->setText(iqtHelper::ConvertWithModifiers(r,text));
	this->layout()->invalidate(); // Qt bug!!! 
}


void iqtWidgetRadioBoxSubject::InsertItem(const iImage &icon, const iString &text, int index)
{
	QRadioButton *r = new QRadioButton(this);
#ifdef IQT_3
	r->setPixmap(iqtHelper::ConvertToPixmap(icon));
	this->insert(r,index);
#else
	r->setIcon(iqtHelper::ConvertToIcon(icon));
	mGroup->addButton(r);
	this->layout()->addWidget(r);
#endif
	r->setText(iqtHelper::ConvertWithModifiers(r,text));
	this->layout()->invalidate(); // Qt bug!!! 
}


QAbstractButton* iqtWidgetRadioBoxSubject::FindButton(int index)
{
#ifdef IQT_3
	return this->find(index);
#else
	if(index<0 || index>=mGroup->buttons().count()) return 0; else return mGroup->buttons()[index];
#endif
}


void iqtWidgetRadioBoxSubject::SetItem(const iString &text, int index, bool vis)
{
	if(index < 0) return;

	QAbstractButton *b = this->FindButton(index);
	if(b == 0)
	{
		b = new QRadioButton(this);
#ifdef IQT_3
		this->insert(b,index);
#else
		mGroup->addButton(b);
		this->layout()->addWidget(b);
#endif
		b->setText("unavailable");
		this->layout()->invalidate(); // Qt bug!!! 
	}
	QString s = iqtHelper::ConvertWithModifiers(b,text);
	if(!s.isEmpty() && s!=b->text()) b->setText(s);
	if(b->isEnabled() != vis) b->setEnabled(vis);
}


void iqtWidgetRadioBoxSubject::RemoveItem(int index)
{
	QAbstractButton *b = this->FindButton(index);
	if(b != 0)
	{
#ifdef IQT_3
		this->remove(b);
		this->layout()->invalidate(); // Qt bug!!! 
#else
		delete b;
#endif
	}
}


int iqtWidgetRadioBoxSubject::Count()
{
#ifdef IQT_3
	return this->count();
#else
	return mGroup->buttons().count();
#endif
}

#endif
