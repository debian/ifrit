/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_QT)


#include "iqthelper.h"


#include "icolor.h"
#include "ierror.h"
#include "iimage.h"
#include "imath.h"
#include "istring.h"
#include "isystem.h"

#include "iggdialog.h"
#include "iggframe.h"
#include "iggimagefactory.h"

#include "ibgwindowhelper.h"

#include "iqtwidgethelper.h"

#include <vtkImageData.h>

#ifdef IQT_3
#include <qbitmap.h>
#include <qcolor.h>
#include <qiconset.h>
#include <qimage.h>
#include <qlayout.h>
#include <qpixmap.h>
#include <qstring.h>
#include <qwidget.h>
#else
#include <QtCore/QString>
#include <QtGui/QBitmap>
#include <QtGui/QColor>
#include <QtGui/QFont>
#include <QtGui/QIcon>
#include <QtGui/QImage>
#include <QtGui/QPalette>
#include <QtGui/QPixmap>
#ifdef IQT_4
#include <QtGui/QGridLayout>
#include <QtGui/QWidget>
#else
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QWidget>
#endif
#endif


using namespace iParameter;


//
//  Widget flags
//
Qt::WFlags iqtHelper::GetFlags(int window, int mode)
{
	Qt::WFlags f = 0;

	switch(window)
	{
	case WindowType::Main:
		{
#ifdef IQT_3
			f |= Qt::WType_TopLevel;
//			f |= Qt::WStyle_Customize | Qt::WStyle_Title | Qt::WStyle_SysMenu | Qt::WStyle_MinMax | Qt::WStyle_ContextHelp;
#else
			f |= Qt::Window;
#endif
			break;
		}
	case WindowType::Render:
		{
#ifdef IQT_3
			f |= (Qt::WType_TopLevel | Qt::WResizeNoErase | Qt::WRepaintNoErase);
#else
			f |= Qt::Window;
#endif
			break;
		}
	case WindowType::BorderlessRender:
		{
#ifdef IQT_3
			f |= (Qt::WType_TopLevel | Qt::WResizeNoErase | Qt::WRepaintNoErase | Qt::WStyle_Customize | Qt::WStyle_NoBorder);
#else
			f |= (Qt::Window | Qt::FramelessWindowHint);
#endif
			break;
		}
	case WindowType::Extension:
		{
#ifdef IQT_3
			f |= Qt::WType_TopLevel;
#else
			f |= Qt::Window;
#endif
			break;
		}
	case WindowType::Dialog:
		{
#ifdef IQT_3
			f |= (Qt::WStyle_Customize | Qt::WType_Dialog | Qt::WStyle_Dialog);
#else
			f |= (Qt::Dialog | Qt::WindowCloseButtonHint);
#endif
			if((mode&DialogFlag::NoTitleBar) == 0)
			{
#ifdef IQT_3
				f |= (Qt::WStyle_Title | Qt::WStyle_SysMenu | Qt::WStyle_NormalBorder);
#else
				f |= (Qt::WindowTitleHint | Qt::WindowSystemMenuHint);
#endif
			}
			else
			{
#ifndef IQT_3
				f |= Qt::FramelessWindowHint;
#endif
			}
			if(mode & DialogFlag::Blocking)
			{
#ifdef IQT_3
				f |= Qt::WShowModal;
#endif
			}
			break;
		}
	}
	return f;
}


//
//  Porting helpers
//
void iqtHelper::SetBackground(QWidget *widget, const QColor &color)
{
#ifdef IQT_3
	widget->setPaletteBackgroundColor(color);
#else
	QPalette p = widget->palette();
	p.setBrush(widget->backgroundRole(),QBrush(color));
#ifndef IQT_40
	widget->setAutoFillBackground(true);
#endif
	widget->setPalette(p);
#endif
}


void iqtHelper::SetBackground(QWidget *widget, const QPixmap &pixmap)
{
#ifdef IQT_3
	widget->setPaletteBackgroundPixmap(pixmap);
#else
	QPalette p = widget->palette();
	p.setBrush(widget->backgroundRole(),QBrush(pixmap));
#ifndef IQT_40
	widget->setAutoFillBackground(true);
#endif
	widget->setPalette(p);
#endif
}


void iqtHelper::SetBackground(QWidget *widget, int mode)
{
	static const QColor  bg0(iqtHelper::Convert(iColor::IFrIT()));
	static const QPixmap bg1(iqtHelper::ConvertToPixmap(*iggImageFactory::FindIcon("bkgr1.png"),false));
	static const QPixmap bg2(iqtHelper::ConvertToPixmap(*iggImageFactory::FindIcon("bkgr2.png"),false));

	switch(mode)
	{
	case 0:
		{
			iqtHelper::SetBackground(widget,bg0);
			break;
		}
	case 1:
		{
			iqtHelper::SetBackground(widget,bg1);
			break;
		}
	case 2:
		{
			iqtHelper::SetBackground(widget,bg2);
			break;
		}
	default:
		{
			IBUG_WARN("Invalid background mode.");
		}
	}
}


void iqtHelper::SetParent(QWidget *widget, QWidget *parent, Qt::WFlags f)
{
#ifdef IQT_3
	static const QPoint p(0,0);
	widget->reparent(parent,f,p);
#else
	widget->setParent(parent,f);
#endif
}


void iqtHelper::SetFont(QWidget *widget, QFont &f)
{
#ifdef IQT_3
	if(f.pointSize() > 10) f.setPointSize(10);
#endif
	if(ibgWindowHelper::GetFontOffset() != 0)
	{
		int ns = f.pointSize() + ibgWindowHelper::GetFontOffset();
		if(ns > 5) f.setPointSize(ns);
	}
	widget->setFont(f);
}


QGridLayout* iqtHelper::NewLayout(QWidget *parent, int nCols)
{
	QGridLayout *l;
#ifdef IQT_3
	l = new QGridLayout(parent,1,nCols,0,-1); IERROR_CHECK_MEMORY(l);
#else
	l = new QGridLayout(parent); IERROR_CHECK_MEMORY(l);
	l->setMargin(0);
	l->setSpacing(-1);
	parent->setLayout(l);
#endif
	return l;
}


QGridLayout* iqtHelper::NewLayout(QLayout *parentLayout, int nCols)
{
	QGridLayout *l;
#ifdef IQT_3
	l = new QGridLayout(parentLayout,1,nCols,-1); IERROR_CHECK_MEMORY(l);
#else
	IASSERT(parentLayout);
	l = new QGridLayout(parentLayout->parentWidget()); IERROR_CHECK_MEMORY(l);
	l->setMargin(0);
	l->setSpacing(-1);
	parentLayout->addItem(l);
#endif
	return l;
}


//
//  Color conversion
//
iColor iqtHelper::Convert(const QColor &c)
{
	return iColor(c.red(),c.green(),c.blue());
}


QColor iqtHelper::Convert(const iColor &c)
{
	return QColor(c.Red(),c.Green(),c.Blue());
}


//
//  String conversion
//
iString iqtHelper::Convert(const QString &s)
{
#ifdef IQT_3
	return iString(s.latin1());
#else
	return iString(s.toLatin1().data());
#endif
}


QString iqtHelper::Convert(const iString &s)
{
	return QString(s.ToCharPointer());
}


QString iqtHelper::ConvertWithModifiers(QWidget *w, const iString &s)
{
	const char *text = s.ToCharPointer();
	
	if(w != 0)
	{
		QFont f = w->font();
		bool cont = true;
		bool work = false;
		while(cont && text[0]=='%')
		{
			switch(text[1])
			{
			case 'b':
				{
					f.setBold(true);
					text += 2;
					work = true;
					break;
				}
			case '+':
				{
					if(f.pointSize() > 0)
					{
						f.setPointSize(f.pointSize()+2);
						work = true;
					}
					text += 2;
					break;
				}
			default: cont = false;
			}
		}
		if(work) w->setFont(f);
	}
	return QString(text);
}


//
//  Image conversion
//
iImage iqtHelper::Convert(const QImage &src)
{
	int w = src.width();
	int h = src.height();

	iImage tmp(4);
	tmp.Scale(w,h);

	unsigned char *sPtr = (unsigned char *)src.bits();
	unsigned char *dPtr = tmp.DataPointer();

	sPtr += 4*w*(h-1);

	int j, i;

	if(iSystem::IsBigEndianMachine())
	{
		for(j=0; j<h; j++)
		{
			for(i=0; i<w; i++)
			{
				dPtr[3] = sPtr[2];
				dPtr[2] = sPtr[1];
				dPtr[1] = sPtr[0];
				dPtr[0] = sPtr[3];
				dPtr += 4;
				sPtr += 4;
			}
			sPtr -= 4*2*w;
		}
	}
	else
	{
		for(j=0; j<h; j++)
		{
			for(i=0; i<w; i++)
			{
				dPtr[0] = sPtr[2];
				dPtr[1] = sPtr[1];
				dPtr[2] = sPtr[0];
				dPtr[3] = sPtr[3];
				dPtr += 4;
				sPtr += 4;
			}
			sPtr -= 4*2*w;
		}
	}

	return tmp;
}


QImage iqtHelper::Convert(const iImage &src, float scale)
{
	int d = src.Depth();
	int w = src.Width();
	int h = src.Height();

#ifdef IQT_3
	QImage tmp(w,h,32);
#else
	QImage tmp(w,h,QImage::Format_RGB32);
#endif

	if(d < 3)
	{
		IBUG_WARN("Invalid depth of iImage in iqtHelper::Convert.");
		return tmp;
	}

	unsigned char *sPtr = src.DataPointer();
	unsigned char *dPtr = tmp.bits();

	dPtr += 4*w*(h-1);

	int j, i;

	if(iSystem::IsBigEndianMachine())
	{
		for(j=0; j<h; j++)
		{
			for(i=0; i<w; i++)
			{
				dPtr[3] = sPtr[2];
				dPtr[2] = sPtr[1];
				dPtr[1] = sPtr[0];
				dPtr[0] = (d<4) ? 255 : sPtr[3];
				sPtr += d;
				dPtr += 4;
			}
			dPtr -= 4*2*w;
		}
	}
	else
	{
		for(j=0; j<h; j++)
		{
			for(i=0; i<w; i++)
			{
				dPtr[0] = sPtr[2];
				dPtr[1] = sPtr[1];
				dPtr[2] = sPtr[0];
				dPtr[3] = (d<4) ? 255 : sPtr[3];
				sPtr += d;
				dPtr += 4;
			}
			dPtr -= 4*2*w;
		}
	}

	if(scale < 1.0f)
	{
#ifdef IQT_PRE47
		tmp.scale(iMath::Round2Int(scale*w),iMath::Round2Int(scale*h));
#else
		return tmp.scaled(iMath::Round2Int(scale*w),iMath::Round2Int(scale*h));
#endif
	}

	return tmp;
}


QPixmap iqtHelper::ConvertToPixmap(const iImage &src, bool withMask)
{
#ifdef IQT_3
	QPixmap tmp = iqtHelper::Convert(src);
#else
	QPixmap tmp = QPixmap::fromImage(iqtHelper::Convert(src));
#endif
	if(withMask) tmp.setMask(tmp.createHeuristicMask());

	return tmp;
}


QIcon iqtHelper::ConvertToIcon(const iImage &src)
{
	return QIcon(iqtHelper::ConvertToPixmap(src,true));
}


int iqtHelper::Convert(int state)
{
	int b = MouseButton::NoButton;

	if(state & Qt::LeftButton) b |= MouseButton::LeftButton;
	if(state & Qt::MidButton) b |= MouseButton::MiddleButton;
	if(state & Qt::RightButton) b |= MouseButton::RightButton;

#ifdef IQT_3
	if(state & Qt::ControlButton) b |= MouseButton::ControlKey;
	if(state & Qt::ShiftButton) b |= MouseButton::ShiftKey;
#else
	if(state & Qt::CTRL) b |= MouseButton::ControlKey;
	if(state & Qt::SHIFT) b |= MouseButton::ShiftKey;
#endif
	return b;
}


QWidget* iqtHelper::Convert(const iggFrameBase *w)
{
	if(w != 0)
	{	
		//
		//  We need to reach QWidget hidden in ibgWidgetBase - well hidden too!!!
		//
		return iDynamicCast<iqtWidgetHelper,ibgWidgetHelper>(INFO,w->GetHelper())->GetWidget();
	}
	else
	{
		return 0;
	}
}

#endif
