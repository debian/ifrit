/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_QT)


#include "iqtwidgethelpbrowsersubject.h"


#include "ierror.h"
#include "istring.h"

#include "iggimagefactory.h"
#include "iggwidgettext.h"

#include "iqthelper.h"
#include "iqtwidgethelper.h"

#ifdef IQT_3
#include <qapplication.h>
#include <qcursor.h>
#include <qimage.h>
#else
#include <QtCore/QVariant>
#include <QtGui/QCursor>
#include <QtGui/QImage>
#ifdef IQT_4
#include <QtGui/QApplication>
#else
#include <QtWidgets/QApplication>
#endif
#endif


using namespace iParameter;


//
// Constructs a browser
//
iqtWidgetHelpBrowserSubject::iqtWidgetHelpBrowserSubject(iggWidgetHelpBrowser *owner) : QTextBrowser(iqtHelper::Convert(owner->GetParent())), ibgWidgetHelpBrowserSubject(owner)
{
	mWidgetHelper = new iqtWidgetHelper(this,owner->Self()); IERROR_CHECK_MEMORY(mWidgetHelper);

	this->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding));
	this->setFocusPolicy(QFocus::StrongFocus);
}


iqtWidgetHelpBrowserSubject::~iqtWidgetHelpBrowserSubject()
{
}


//
//  Text manipulations
//
void iqtWidgetHelpBrowserSubject::SetText(const iString &text)
{
#ifdef IQT_3
	this->setText(iqtHelper::Convert(text));
#else
	this->setHtml(iqtHelper::Convert(text));
#endif
}


iString iqtWidgetHelpBrowserSubject::GetText() const
{
#ifdef IQT_3
	return iqtHelper::Convert(this->text());
#else
	return iqtHelper::Convert(this->toHtml());
#endif
}


void iqtWidgetHelpBrowserSubject::AddImage(const iString &name, const iImage &image)
{
#ifdef IQT_3
	this->mimeSourceFactory()->setImage(iqtHelper::Convert(name),iqtHelper::Convert(image,0.5f));
#else
#endif
}


//
//  Qt specific
//
#ifdef IQT_3

void iqtWidgetHelpBrowserSubject::setSource(const QString &name)
{
	this->OnLinkFollowedBody(name.latin1());
}


void iqtWidgetHelpBrowserSubject::contentsMouseReleaseEvent(QMouseEvent *e)
{
	qApp->setOverrideCursor(QCursor(Qt::WaitCursor));
	QTextBrowser::contentsMouseReleaseEvent(e);
	qApp->restoreOverrideCursor();
}

#else

void iqtWidgetHelpBrowserSubject::setSource(const QUrl &name)
{
	this->OnLinkFollowedBody(name.toString().toLatin1());
}


void iqtWidgetHelpBrowserSubject::mouseReleaseEvent(QMouseEvent *e)
{
	qApp->setOverrideCursor(QCursor(Qt::WaitCursor));
	QTextBrowser::mouseReleaseEvent(e);
	qApp->restoreOverrideCursor();
}


QVariant iqtWidgetHelpBrowserSubject::loadResource(int type, const QUrl &name)
{
	if(type == QTextDocument::ImageResource)
	{
		//
		//  Look for an image
		//
		iString ws = iqtHelper::Convert(name.toString());
		const iImage* im = iggImageFactory::FindHelpChart(ws.Part(8));
		if(im != 0)
		{
			return QVariant(iqtHelper::Convert(*im));
		}
	}

#ifdef I_DEBUG
	qWarning("Looking for a resource: %s",name.toString().toLatin1().data());
#endif
	return QTextBrowser::loadResource(type,name);
}

#endif

#endif
