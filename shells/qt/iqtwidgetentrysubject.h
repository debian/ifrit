/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A combination of a slider with an LCD and/or line edit 
//
#ifndef IQTWIDGETENTRYSUBJECT_H
#define IQTWIDGETENTRYSUBJECT_H


#include "iqt.h"

#ifdef IQT_3
#include <qwidget.h>
#elif defined(IQT_4)
#include <QtGui/QWidget>
#else
#include <QtWidgets/QWidget>
#endif
#include "ibgwidgetentrysubject.h"


class QLineEdit;
class QMouseEvent;
class QSlider;


class iqtWidgetEntrySubject : public QWidget, public ibgWidgetEntrySubject
{

	friend class iggSubjectFactory;

public:

	virtual ~iqtWidgetEntrySubject();

	virtual void AddButton(ibgWidgetButtonSubject* button);
	virtual void SetEditable(bool s);

	//
	//  Decorator functions
	//
	virtual int GetValue() const;
	virtual void GetRange(int &min, int &max) const;
	virtual const iString GetText() const;

	virtual void SetValue(int v);
	virtual void SetValue(int v, double dv);
	virtual void SetValue(int v, vtkIdType lv);
	virtual void SetNumberOfTicks(int n);
	virtual void SetRange(int smin, int smax, int pagestep = 0);
	virtual void SetText(const iString &text);
	virtual void SelectText(int start, int len = -1);

	virtual void ShowTail();

	virtual void SetStretch(int label, int slider, int number = 0);

	void OnKeyUpDown(bool up);

protected:

	iqtWidgetEntrySubject(iggWidget *owner, bool slider, int numdig, const iString &label);

	int mNumButtons;

	QLineEdit *mDisplay;
	QSlider *mSlider;
};

#endif  // IQTWIDGETENTRYSUBJECT_H
