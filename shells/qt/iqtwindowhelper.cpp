/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_QT)


#include "iqtwindowhelper.h"


#include "ierror.h"
#include "istring.h"

#include "iqt.h"
#include "iqthelper.h"

#ifdef IQT_3
#include <qapplication.h>
#include <qlayout.h>
#include <qpixmap.h>
#include <qwidget.h>
#include <qgl.h>  //  needed for diabling close button under windows
#else
#include <QtGui/QIcon>
#include <QtGui/QPixmap>
#include <QtGui/QResizeEvent>
#include <QtOpenGL/QGLWidget>  //  needed for disabling close button under windows
#ifdef IQT_4
#include <QtGui/QApplication>
#include <QtGui/QLayout>
#include <QtGui/QWidget>
#else
#include <QtWidgets/QApplication>
#include <QtWidgets/QLayout>
#include <QtWidgets/QWidget>
#endif
#endif


//
//  Main class
//
iqtWindowHelper::iqtWindowHelper(QWidget *widget, const iImage *icon, const iString &title, bool disableClose) : ibgWindowHelper(icon,title,disableClose)
{
	IASSERT(widget);
	mWidget = widget; 

#ifndef IQT_40
	iqtHelper::SetBackground(mWidget,1); // messes things on Unix under Qt 4.0
#endif

	QFont f(mWidget->font());
	iqtHelper::SetFont(mWidget,f);

	if(!title.IsEmpty())
	{
#ifdef IQT_3
		mWidget->setCaption(iqtHelper::Convert(title));
#else
		mWidget->setWindowTitle(iqtHelper::Convert(title));
#endif
	}

	this->RestoreDecoration();  // this does not always work
}


void iqtWindowHelper::RestoreDecoration()
{
	if(mIcon != 0)
	{
#ifdef IQT_3
		mWidget->setIcon(iqtHelper::ConvertToPixmap(*mIcon));
#else
		mWidget->setWindowIcon(iqtHelper::ConvertToIcon(*mIcon));
#endif
	}

#ifdef _WIN32
	if(mDisableClose)
	{
		HMENU hmenu = GetSystemMenu((HWND)mWidget->winId(),false);
//		DeleteMenu(hmenu,SC_CLOSE,MF_BYCOMMAND);  //ok actually remove the close button
		EnableMenuItem(hmenu,SC_CLOSE,MF_BYCOMMAND|MF_GRAYED);  //ok actually remove the close button
	}
#endif

}


void iqtWindowHelper::SetWindowGeometry(int wg[4], bool force)
{
	if(wg[0]!=mWidget->x() || wg[1]!=mWidget->y()) mWidget->move(wg[0],wg[1]);
	if(wg[2]!=mWidget->width() || wg[3]!=mWidget->height())	mWidget->resize(wg[2],wg[3]);
	if(force)
	{
		qApp->sendPostedEvents(mWidget,0);
	}
}


void iqtWindowHelper::GetWindowGeometry(int wg[4], bool force) const
{
	if(force && mWidget->layout())
	{
		mWidget->layout()->activate();
	}
	wg[0] = mWidget->x();
	wg[1] = mWidget->y();
	wg[2] = mWidget->width();
	wg[3] = mWidget->height();
}


void iqtWindowHelper::GetFrameSize(int &w, int &h) const
{
	QRect fg, wg;
	fg = mWidget->frameGeometry();
	wg = mWidget->geometry();
	w = fg.width() - wg.width();
	if(w < 0) w = 0;
	h = fg.height() - wg.height();
	if(h < 0) h = 0;
}


void iqtWindowHelper::Show(bool s)
{
	if(s)
	{
		if(!mWidget->isVisible()) mWidget->show();
	}
	else mWidget->hide();
}


bool iqtWindowHelper::IsVisible() const
{
	return mWidget->isVisible();
}


void iqtWindowHelper::ShowAsIcon()
{
	if(!mWidget->isHidden() && !mWidget->isMinimized() && 
#ifdef IQT_3
		!mWidget->isDialog()
#else
		mWidget->windowType()!=Qt::Dialog 
#endif
	)
	{
		mWidget->showMinimized();
	}
}


void iqtWindowHelper::ShowAsWindow()
{
	if(mWidget->isMinimized() && !mWidget->isHidden())
	{
		mWidget->showNormal();
	}
}

#endif
