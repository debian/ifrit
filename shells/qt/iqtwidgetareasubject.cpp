/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_QT)


#include "iqtwidgetareasubject.h"


#include "ierror.h"
#include "imath.h"
#include "istring.h"

#include "iggframe.h"
#include "iggwidgetarea.h"

#include "iqtframesubject.h"
#include "iqthelper.h"
#include "iqtwidgethelper.h"

#ifdef IQT_3
#include <qevent.h>
#include <qimage.h>
#include <qpainter.h>
#include <qtimer.h>
#else
#include <QtCore/QTimer>
#include <QtGui/QImage>
#include <QtGui/QMouseEvent>
#include <QtGui/QPainter>
using namespace Qt;
#endif


//
//  Templates
//
#include "iarray.tlh"


using namespace iParameter;


//
//  WidgetDisplayAreaSubject class
//
iqtWidgetDisplayAreaSubject::iqtWidgetDisplayAreaSubject(iggWidget *owner, const iString &text) : QLabel(iqtHelper::Convert(owner->GetParent())), ibgWidgetDisplayAreaSubject(owner,text)
{
	mWidgetHelper = new iqtWidgetHelper(this,owner); IERROR_CHECK_MEMORY(mWidgetHelper);

	this->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
	this->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding));
	this->SetText(text);
};


void iqtWidgetDisplayAreaSubject::SetText(const iString &text)
{
	this->setPixmap(QPixmap());
	this->setText(iqtHelper::ConvertWithModifiers(this,text));
	this->setScaledContents(true);
}


void iqtWidgetDisplayAreaSubject::SetAlignment(int s)
{
	if(s < 0)
	{
		this->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
	}
	else if(s > 0)
	{
		this->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
	}
	else
	{
		this->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
	}
}


void iqtWidgetDisplayAreaSubject::ShowFrame(bool s)
{
#ifdef IQT_3
	this->setMargin(s?3:0);
#else
	this->setMargin(s?3:0);
#endif
	this->setFrameShape(s?QFrame::Box:QFrame::NoFrame);
	this->setFrameShadow(QFrame::Plain);
}


void iqtWidgetDisplayAreaSubject::SetImage(const iImage *image, bool withMask, bool scaled)
{
	if(image != 0)
	{
		this->setText(0);
		this->setScaledContents(scaled);
		this->setPixmap(iqtHelper::ConvertToPixmap(*image,withMask));
	}
	else this->setPixmap(QPixmap());
	this->repaint(); // needed for proper update outside of an event loop
}


//
//  WidgetMultiImageDisplayAreaSubject class
//
iqtWidgetMultiImageDisplayAreaSubject::iqtWidgetMultiImageDisplayAreaSubject(iggWidget *owner) : QLabel(iqtHelper::Convert(owner->GetParent())), ibgWidgetMultiImageDisplayAreaSubject(owner)
{
	mWidgetHelper = new iqtWidgetHelper(this,owner); IERROR_CHECK_MEMORY(mWidgetHelper);

	this->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
	this->setScaledContents(false);
	this->setSizePolicy(QSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum));

	mTimer = new QTimer(this); IERROR_CHECK_MEMORY(mTimer);
	if(!connect(mTimer,SIGNAL(timeout()),iDynamicCast<iqtWidgetHelper,ibgWidgetHelper>(INFO,mWidgetHelper),SLOT(OnVoid1()))) IBUG_WARN("Missed connection.");
};


iqtWidgetMultiImageDisplayAreaSubject::~iqtWidgetMultiImageDisplayAreaSubject()
{
}


ibgWidgetMultiImageDisplayAreaSubject::Image* iqtWidgetMultiImageDisplayAreaSubject::AddImageBody(const iImage &image, bool withmask, bool first)
{
	QPixmap tmp(iqtHelper::ConvertToPixmap(image,withmask));
	if(first)
	{
		this->setFixedSize(tmp.size());
		this->setPixmap(tmp);
	}
	else
	{
		QPixmap &base(iDynamicCast<qtImage,Image>(INFO,mImages[0])->Pixmap);
		if(tmp.size() != base.size())
		{
			IBUG_WARN("Incompatible size of iqtWidgetMultiImageDisplayAreaSubject images.");
			QPixmap tmp2(base.size()); // workaround of some bug in 64-bit version
			tmp = tmp2;
		}
	}
	return new qtImage(tmp);
}


void iqtWidgetMultiImageDisplayAreaSubject::ShowImageBody(int n)
{
	QPixmap &tmp(iDynamicCast<qtImage,Image>(INFO,mImages[n])->Pixmap);
#ifdef IQT_3
	bitBlt(this,0,0,&tmp,0,0,tmp.width(),tmp.height(),Qt::CopyROP); 
#else
	this->setPixmap(tmp);
#endif
}


void iqtWidgetMultiImageDisplayAreaSubject::Start()
{
	mTimer->start(100);
}


void iqtWidgetMultiImageDisplayAreaSubject::Abort()
{
	mTimer->stop();
}


//
//  WidgetDrawAreaSubject class
//
iqtWidgetDrawAreaSubject::iqtWidgetDrawAreaSubject(iggWidgetDrawArea *owner, bool interactive) : QFrame(iqtHelper::Convert(owner->GetParent())
#ifdef IQT_3
																			  ,0,Qt::WRepaintNoErase|Qt::WResizeNoErase
#endif
																			  ), ibgWidgetDrawAreaSubject(owner,interactive)
{
#ifdef IQT_4
	this->setAttribute(Qt::WA_PaintOnScreen);
#endif

	mWidgetHelper = new iqtWidgetHelper(this,owner); IERROR_CHECK_MEMORY(mWidgetHelper);

	mPainter = new QPainter();
	mBackgroundColor = iColor(255,255,255);
	mBackgroundPixmap = new QPixmap(this->size());
	mForegroundPixmap = new QPixmap(this->size());
	
	this->setFrameStyle(QFrame::Panel | QFrame::Sunken);
	this->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding));

//	this->setMargin(0);

	this->setMinimumSize(24,24);
}
	

iqtWidgetDrawAreaSubject::~iqtWidgetDrawAreaSubject()
{
	delete mPainter;
	delete mBackgroundPixmap;
	delete mForegroundPixmap;
}


void iqtWidgetDrawAreaSubject::SetMinimumSize(int w, int h)
{
	this->setMinimumSize(w,h);
}


void iqtWidgetDrawAreaSubject::SetFixedSize(int w, int h)
{
	this->setFixedSize(w,h);
}


int iqtWidgetDrawAreaSubject::Width() const
{
	return this->contentsRect().width();
}


int iqtWidgetDrawAreaSubject::Height() const
{
	return this->contentsRect().height();
}


void iqtWidgetDrawAreaSubject::Begin(int m, bool clear)
{
	switch(m)
	{
	case DrawMode::Background:
		{
			mCurrentMode = DrawMode::Background;
			if(this->contentsRect().size() != mBackgroundPixmap->size())
			{
				QPixmap tmp2(this->contentsRect().size()); // workaround of some bug in 64-bit version
				*mBackgroundPixmap = tmp2;
			}
			mPainter->begin(mBackgroundPixmap);
#ifdef IQT_3
			mPainter->setBackgroundColor(iqtHelper::Convert(mBackgroundColor));
			if(clear) mPainter->fillRect(0,0,mBackgroundPixmap->width(),mBackgroundPixmap->height(),mPainter->backgroundColor());
#else
			mPainter->setBackground(iqtHelper::Convert(mBackgroundColor));
			if(clear) mPainter->fillRect(0,0,mBackgroundPixmap->width(),mBackgroundPixmap->height(),mPainter->background().color());
#endif
			break;
		}
	case DrawMode::Foreground:
		{
			mCurrentMode = DrawMode::Foreground;
			if(clear)
			{
				if(mForegroundPixmap->size() != mBackgroundPixmap->size())
				{
					QPixmap tmp2(mBackgroundPixmap->size()); // workaround of some bug in 64-bit version
					*mForegroundPixmap = tmp2;
				}
			}
			else
			{
				*mForegroundPixmap = *mBackgroundPixmap; 
			}
			mPainter->begin(mForegroundPixmap);
#ifdef IQT_3
			mPainter->setBackgroundColor(iqtHelper::Convert(mBackgroundColor));
			if(clear) mPainter->fillRect(0,0,mForegroundPixmap->width(),mForegroundPixmap->height(),mPainter->backgroundColor());
#else
			mPainter->setBackground(iqtHelper::Convert(mBackgroundColor));
			if(clear) mPainter->fillRect(0,0,mForegroundPixmap->width(),mForegroundPixmap->height(),mPainter->background().color());
#endif
			break;
		}
	default:
		{
			IBUG_WARN("Incorrect iqtWidgetDrawArea mode.");
		}
	}
	mOffsetY = this->Height() - 1;
}


void iqtWidgetDrawAreaSubject::End()
{
	switch(mCurrentMode)
	{
	case DrawMode::Background:
	case DrawMode::Foreground:
		{
			mPainter->end();
			break;
		}
	default:
		{
			IBUG_WARN("Incorrect iqtWidgetDrawArea mode.");
		}
	}
}


void iqtWidgetDrawAreaSubject::Frame()
{
#ifdef IQT_3
	bitBlt(this,this->contentsRect().x(),this->contentsRect().y(),mForegroundPixmap,0,0,mForegroundPixmap->width(),mForegroundPixmap->height(),Qt::CopyROP,false);
#else
	mPainter->begin(this);
	mPainter->drawPixmap(this->contentsRect(),*mForegroundPixmap);
	mPainter->end();
#endif
}


void iqtWidgetDrawAreaSubject::Clear()
{
	mPainter->eraseRect(0,0,this->Width()-1,this->Height()-1);
}


void iqtWidgetDrawAreaSubject::DrawLine(int x1, int y1, int x2, int y2, const iColor &color, int width, bool dotted)
{
	mPainter->setPen(QPen(iqtHelper::Convert(color),width,(width>0)?(dotted?DotLine:SolidLine):NoPen));
	mPainter->drawLine(x1,mOffsetY-y1,x2,mOffsetY-y2);
}


void iqtWidgetDrawAreaSubject::DrawRectangle(int x1, int y1, int x2, int y2, const iColor &color, const iColor& fillColor, int width)
{
	mPainter->setPen(QPen(iqtHelper::Convert(color),width,(width>0)?SolidLine:NoPen));
	mPainter->setBrush(iqtHelper::Convert(fillColor));
	mPainter->drawRect(x1,mOffsetY-y2,x2-x1+1,y2-y1+1);
}


void iqtWidgetDrawAreaSubject::DrawEllipse(int x1, int y1, int x2, int y2, const iColor &color, const iColor& fillColor, int width)
{
	mPainter->setPen(QPen(iqtHelper::Convert(color),width,(width>0)?SolidLine:NoPen));
	mPainter->setBrush(iqtHelper::Convert(fillColor));
	mPainter->drawEllipse(x1,mOffsetY-y2,x2-x1+1,y2-y1+1);
}

void iqtWidgetDrawAreaSubject::DrawImage(int x1, int y1, int x2, int y2, const iImage &image, iImage::ScaleMode mode)
{
	if(!image.IsEmpty())
	{
		iImage tmp = image;
		tmp.Scale(x2-x1+1,y2-y1+1,mode);
		mPainter->drawImage(x1+(x2-x1+1-tmp.Width())/2,mOffsetY-y2+(y2-y1+1-tmp.Height())/2,iqtHelper::Convert(tmp));
	}
}


void iqtWidgetDrawAreaSubject::DrawTextLine(int x1, int y1, int x2, int y2, const iString &text, const iColor &color)
{
	mPainter->setPen(iqtHelper::Convert(color));
	int f1 = y2 - y1 + 1;
	int f2 = (x2-x1+1)/text.Length();
	QFont font(mPainter->font());
	font.setBold(true);
	font.setPixelSize((f1>f2)?f2:f1);
	mPainter->setFont(font);
	mPainter->drawText(x1,mOffsetY-y2,x2-x1+1,y2-y1+1,AlignVCenter|AlignHCenter|
#ifdef IQT_3
		SingleLine
#else
		TextSingleLine
#endif
		,iqtHelper::Convert(text));
}


void iqtWidgetDrawAreaSubject::BlendForegroundOntoBackground(float opacity, const iColor &transparent)
{
	float w0, w1;
	w0 = 1.0 - opacity;
	w1 = opacity;
#ifdef IQT_3
	QImage imB = mBackgroundPixmap->convertToImage();
	QImage imF = mForegroundPixmap->convertToImage();
#else
	QImage imB = mBackgroundPixmap->toImage();
	QImage imF = mForegroundPixmap->toImage();
#endif
	unsigned char *sPtrB = imB.bits();
	unsigned char *sPtrF = imF.bits();
	unsigned char tr = transparent.Red();
	unsigned char tg = transparent.Green();
	unsigned char tb = transparent.Blue();
	int j, jmax = 4*imB.width()*imB.height();
	for(j=0; j<jmax; j+=4)
	{
		if(sPtrF[j]!=tr || sPtrF[j+1]!=tg || sPtrF[j+2]!=tb)
		{
			sPtrB[j+0] = (unsigned char)iMath::Round2Int(sPtrB[j+0]*w0+sPtrF[j+0]*w1);
			sPtrB[j+1] = (unsigned char)iMath::Round2Int(sPtrB[j+1]*w0+sPtrF[j+1]*w1);
			sPtrB[j+2] = (unsigned char)iMath::Round2Int(sPtrB[j+2]*w0+sPtrF[j+2]*w1);
		}
	}
#ifdef IQT_3
	mBackgroundPixmap->convertFromImage(imB);
#else
	QPixmap tmp2(QPixmap::fromImage(imB)); // workaround of some bug in 64-bit version
	*mBackgroundPixmap = tmp2;
#endif
}


void iqtWidgetDrawAreaSubject::RequestPaintingBody()
{
	this->update();
}


//
//  Qt-specific
//
void iqtWidgetDrawAreaSubject::paintEvent(QPaintEvent *e)
{
	//
	//  Layout may have changed since we were painted, so if this is the case, ask the owner to update
	//
	if(this->contentsRect().size() != mForegroundPixmap->size())
	{
		iDynamicCast<iqtFrameSubject,QWidget>(INFO,this->parentWidget())->GetFrame()->UpdateWidget();
	}

	QFrame::paintEvent(e);

	this->Paint();	
}


void iqtWidgetDrawAreaSubject::mouseMoveEvent(QMouseEvent *e)
{
	if(e == 0) return;

#ifdef IQT_3
	this->OnMouseMove(e->x(),mOffsetY-e->y(),iqtHelper::Convert(e->state()));
#else
	this->OnMouseMove(e->x(),mOffsetY-e->y(),iqtHelper::Convert(e->buttons() | e->modifiers()));
#endif
	QFrame::mouseMoveEvent(e);
}


void iqtWidgetDrawAreaSubject::mousePressEvent(QMouseEvent *e)
{
	if(e == 0) return;

#ifdef IQT_3
	this->OnMousePress(e->x(),mOffsetY-e->y(),iqtHelper::Convert(e->stateAfter()));
#else
	this->OnMousePress(e->x(),mOffsetY-e->y(),iqtHelper::Convert(e->buttons() | e->modifiers()));
#endif
	QFrame::mousePressEvent(e);
}


void iqtWidgetDrawAreaSubject::mouseReleaseEvent(QMouseEvent *e)
{
	if(e == 0) return;

#ifdef IQT_3
	this->OnMouseRelease(e->x(),mOffsetY-e->y(),iqtHelper::Convert(e->state()));
#else
	this->OnMouseRelease(e->x(),mOffsetY-e->y(),iqtHelper::Convert(e->buttons() | e->button() | e->modifiers()));
#endif
	QFrame::mouseReleaseEvent(e);
}

#endif
