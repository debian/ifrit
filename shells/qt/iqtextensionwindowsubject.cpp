/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_QT)


#include "iqtextensionwindowsubject.h"


#include "ierror.h"
#include "istring.h"

#include "iggabstractextension.h"
#include "iggextensionwindow.h"
#include "iggimagefactory.h"

#include "iqthelper.h"
#include "iqtwindowhelper.h"
#include "iqtframesubject.h"

#ifdef IQT_3
#include <qevent.h>
#include <qlayout.h>
#include <qstyle.h>
#else
#include <QtGui/QMouseEvent>
#ifdef IQT_4
#include <QtGui/QGridLayout>
#else
#include <QtWidgets/QGridLayout>
#endif
#endif


using namespace iParameter;


//
//  Main class
//
iqtExtensionWindowSubject::iqtExtensionWindowSubject(iggExtensionWindow *ew) : 
#ifdef IQT_3
	QWidget(0,0,iqtHelper::GetFlags(WindowType::Extension)), 
#else
	QWidget(0,iqtHelper::GetFlags(WindowType::Extension)), 
#endif
	ibgExtensionWindowSubject(ew)
{
	mWindowHelper = new iqtWindowHelper(this,iggImageFactory::FindIcon("genie1ext.png"),"IFrIT - Extension Window",true); IERROR_CHECK_MEMORY(mWindowHelper);
}


iqtExtensionWindowSubject::~iqtExtensionWindowSubject()
{
}


void iqtExtensionWindowSubject::AttachPanels(iggFrame *main, iggFrame *bottom, iggFrame *left)
{
	QGridLayout *layout = iqtHelper::NewLayout(this,2);
#ifdef IQT_3
	layout->expand(2,2);
#endif

	iqtFrameSubject *ms = iDynamicCast<iqtFrameSubject,ibgFrameSubject>(INFO,main->GetSubject());
	if(ms->parentWidget() != this)
	{
		iqtHelper::SetParent(ms,this);
		layout->addWidget(ms,0,1);
	}

	iqtFrameSubject *bs = iDynamicCast<iqtFrameSubject,ibgFrameSubject>(INFO,bottom->GetSubject());
	if(bs->parentWidget() != this)
	{
		iqtHelper::SetParent(bs,this);
		layout->addWidget(bs,1,1);
	}

	iqtFrameSubject *ls = iDynamicCast<iqtFrameSubject,ibgFrameSubject>(INFO,left->GetSubject());
	if(ls->parentWidget() != this)
	{
		iqtHelper::SetParent(ls,this);
		layout->addWidget(ls,0,0);
	}

	//
	//  Beautify
	//
#ifdef IQT_3
	QFrame *f = new QFrame(ls);
	f->setFrameStyle(QFrame::TabWidgetPanel|QFrame::Raised);
	f->setLineWidth(f->style().pixelMetric(QStyle::PM_DefaultFrameWidth,this));
	f->setMinimumWidth(2*f->lineWidth()+1);
	ls->GetLayout()->addMultiCellWidget(f,0,ls->GetLayout()->numRows()-1,1,1);
#else
	bs->setMinimumHeight(35);
#endif

	layout->setRowStretch(0,10);
	layout->setRowStretch(1,0);
#ifdef IQT_3
	layout->setColStretch(1,10);
#else
	layout->setColumnStretch(1,10);
#endif
	layout->invalidate();
}


//
//  Qt-specific
//
bool iqtExtensionWindowSubject::event(QEvent *e)
{
	if(e!=0 && e->spontaneous() && this->IsBlocked()) 
	{
		return false;
	}
	else
	{
		return QWidget::event(e);
	}
}


void iqtExtensionWindowSubject::closeEvent(QCloseEvent *e)
{
	e->ignore();
	mExtensionWindow->OnCloseAttempt();
}

#endif
