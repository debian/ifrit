/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_QT)


#include "iqtwidgetbuttonsubject.h"


#include "ierror.h"
#include "iimage.h"
#include "istring.h"

#include "iggwidget.h"

#include "iqthelper.h"
#include "iqtwidgethelper.h"

#ifdef IQT_3
#include <qcheckbox.h>
#include <qimage.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qpixmap.h>
#include <qpushbutton.h>
#include <qtoolbutton.h>
#else
#include <QtGui/QImage>
#include <QtGui/QPixmap>
#ifdef IQT_4
#include <QtGui/QCheckBox>
#include <QtGui/QGridLayout>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QToolButton>
#else
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QToolButton>
#endif
#endif


using namespace iParameter;


iqtWidgetButtonSubject::iqtWidgetButtonSubject(iggWidget *owner, int type, const iString &text, int slot, int minsize) : QWidget(iqtHelper::Convert(owner->GetParent())), ibgWidgetButtonSubject(owner,type,text,slot,minsize)
{
	mWidgetHelper = new iqtWidgetHelper(this,owner); IERROR_CHECK_MEMORY(mWidgetHelper);

	//
	//  Create a layout
	//
	if(this->layout() != 0) delete this->layout();
	QGridLayout *l = iqtHelper::NewLayout(this,1);

	mType = type;
	switch(mType)
	{
	case ButtonType::PushButton:
	case ButtonType::SwitchButton:
		{
			mButton = new QPushButton(this);
#ifdef IQT_3
			iDynamicCast<QPushButton,QAbstractButton>(INFO,mButton)->setToggleButton(mType==ButtonType::SwitchButton);
#else
			iDynamicCast<QPushButton,QAbstractButton>(INFO,mButton)->setCheckable(mType==ButtonType::SwitchButton);
#endif
			if(minsize > 0) mButton->setMinimumHeight(minsize);
			break;
		}
	case ButtonType::ToolButton:
	case ButtonType::ToggleButton:
		{
			mButton = new QToolButton(this);
#ifdef IQT_3
			iDynamicCast<QToolButton,QAbstractButton>(INFO,mButton)->setToggleButton(mType==ButtonType::ToggleButton);
#else
			iDynamicCast<QToolButton,QAbstractButton>(INFO,mButton)->setCheckable(mType==ButtonType::ToggleButton);
#endif
			if(minsize == 0) mButton->setMinimumSize(25,25); else mButton->setMinimumSize(minsize,minsize);
			break;
		}
	case ButtonType::CheckBox:
		{
			mButton = new QCheckBox(this);
			if(minsize > 0) mButton->setMinimumHeight(minsize);
			break;
		}
	default: IBUG_WARN("Incorrect button type.");
	}

	mButton->setText(iqtHelper::ConvertWithModifiers(mButton,text));
	mButton->setSizePolicy(QSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum));
	l->addWidget(mButton,0,0);

	this->setSizePolicy(QSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum));

	switch(slot)
	{
	case 1:
		{
			if(!this->connect(mButton,SIGNAL(clicked()),iDynamicCast<iqtWidgetHelper,ibgWidgetHelper>(INFO,mWidgetHelper),SLOT(OnVoid1()))) IBUG_WARN("Missed connection.");
			break;
		}
	case 2:
		{
			if(!this->connect(mButton,SIGNAL(clicked()),iDynamicCast<iqtWidgetHelper,ibgWidgetHelper>(INFO,mWidgetHelper),SLOT(OnVoid2()))) IBUG_WARN("Missed connection.");
			break;
		}
	case 3:
		{
			if(!this->connect(mButton,SIGNAL(clicked()),iDynamicCast<iqtWidgetHelper,ibgWidgetHelper>(INFO,mWidgetHelper),SLOT(OnVoid3()))) IBUG_WARN("Missed connection.");
			break;
		}
	default:
		{
			IBUG_FATAL("Incorrect slot number.");
		}
	}
}


iqtWidgetButtonSubject::~iqtWidgetButtonSubject()
{
}


void iqtWidgetButtonSubject::SetSize(int w, int h)
{
	mButton->setFixedSize(w,h);
}


void iqtWidgetButtonSubject::SetFlat(bool s)
{
	switch(mType)
	{
	case ButtonType::PushButton:
	case ButtonType::SwitchButton:
		{
			iDynamicCast<QPushButton,QAbstractButton>(INFO,mButton)->setFlat(s);
			break;
		}
	case ButtonType::ToolButton:
	case ButtonType::ToggleButton:
		{
			iDynamicCast<QToolButton,QAbstractButton>(INFO,mButton)->setAutoRaise(s);
			break;
		}
	}
}


void iqtWidgetButtonSubject::SetAutoRepeat(bool s)
{
	mButton->setAutoRepeat(s);
}


bool iqtWidgetButtonSubject::IsDown() const
{
	switch(mType)
	{
	case ButtonType::PushButton:
	case ButtonType::ToolButton:
		{
			return false;
		}
	case ButtonType::SwitchButton:
	case ButtonType::ToggleButton:
	case ButtonType::CheckBox:
		{
#ifdef IQT_3
			return mButton->isOn();
#else
			return mButton->isChecked();
#endif
		}
	default:
		{
			IBUG_WARN("Incorrect button type.");
			return false;
		}
	}
}


void iqtWidgetButtonSubject::SetDown(bool s)
{
	mButton->blockSignals(true);
	switch(mType)
	{
	case ButtonType::PushButton:
	case ButtonType::ToolButton:
		{
			break;
		}
	case ButtonType::SwitchButton:
		{
#ifdef IQT_3
			iDynamicCast<QPushButton,QAbstractButton>(INFO,mButton)->setOn(s);
#else
			iDynamicCast<QPushButton,QAbstractButton>(INFO,mButton)->setChecked(s);
#endif
			break;
		}
	case ButtonType::ToggleButton:
		{
#ifdef IQT_3
			iDynamicCast<QToolButton,QAbstractButton>(INFO,mButton)->setOn(s);
#else
			iDynamicCast<QToolButton,QAbstractButton>(INFO,mButton)->setChecked(s);
#endif
			break;
		}
	case ButtonType::CheckBox:
		{
			iDynamicCast<QCheckBox,QAbstractButton>(INFO,mButton)->setChecked(s);
			break;
		}
	default: IBUG_WARN("Incorrect button type.");
	}
	mButton->blockSignals(false);
}


void iqtWidgetButtonSubject::SetIcon(const iImage &icon)
{
#ifdef IQT_3
	mButton->setPixmap(iqtHelper::ConvertToPixmap(icon));
#else
	QIcon ic = iqtHelper::ConvertToIcon(icon);
	mButton->setIcon(ic);
	if(mButton->minimumSize() == mButton->maximumSize())
	{
		mButton->setIconSize(QSize(icon.Width(),icon.Height()));
	}
#endif
}


void iqtWidgetButtonSubject::SetText(const iString &text)
{
	mButton->setText(iqtHelper::ConvertWithModifiers(mButton,text));
}

#endif
