/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Qt-implementation of ibgFrameSubject
//

#ifndef IQTFRAMESUBJECT_H
#define IQTFRAMESUBJECT_H


#include "iqtexplicitframesubject.h"

#include <iqt.h>

#ifdef IQT_3
#include <qtabwidget.h>
#include <qwidgetstack.h>
#include <qscrollview.h>
#include <qsplitter.h>
#define QScrollArea		QScrollView
#define QStackedWidget	QWidgetStack
#elif defined(IQT_4)
#include <QtGui/QTabWidget>
#include <QtGui/QStackedWidget>
#include <QtGui/QScrollArea>
#include <QtGui/QSplitter>
#else
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSplitter>
#endif


class iqtFrameSubject : public iqtExplicitFrameSubject
{

	friend class iggSubjectFactory;

public:

	virtual ~iqtFrameSubject();
	
protected:

	iqtFrameSubject(iggFrame *owner, int cols);
};


class iqtFrameBookSubject : public QTabWidget, public ibgFrameBookSubject
{

	Q_OBJECT

	friend class iggSubjectFactory;

public:

	virtual ~iqtFrameBookSubject();
	
	virtual void OpenPage(int i);
	virtual void AddPage(const iString &title, const iImage *image, iggFrame *frame);
	virtual void SetTabMode(int n, int m, const iString &title, const iImage *image);
	virtual int GetTabMode();
	virtual void ChangeIcon(int n, const iImage &image);
	virtual void SetOrientation(int v);
	virtual int GetOrientation();

protected:

	iqtFrameBookSubject(iggFrameBook *owner, bool withFeedback);

	//
	//  Qt specific
	//
protected slots:

	void OnCurrentChanged(QWidget * );
	void OnCurrentChanged(int);
};


class iqtFrameFlipSubject : public QStackedWidget, public ibgFrameFlipSubject
{

	friend class iggSubjectFactory;

public:

	virtual ~iqtFrameFlipSubject();

	void ShowLayer(int i);

protected:

	iqtFrameFlipSubject(iggFrameFlip *owner, bool expanding);

	virtual void AddLayerBody(iggFrame *layer);

	//
	//  Dummy overloads
	//
	virtual void SetTitle(const iString &){}
	virtual void ShowFrame(bool){}

	virtual void SetPadding(bool){}
	virtual void SetColStretch(int, int){}
	virtual void SetRowStretch(int, int){}
	virtual void PlaceWidget(int, int, ibgWidgetSubject *, int, bool){}

	virtual void GetFrameGeometry(int [4]) const {}
};


class iqtFrameScrollSubject : public QScrollArea, public ibgFrameScrollSubject
{

	friend class iggSubjectFactory;

public:

	virtual void AttachContents(iggFrame *contents);

protected:

	iqtFrameScrollSubject(iggFrameScroll *owner, bool withHor, bool withVer);

	//
	//  Qt specific
	//
	virtual bool event(QEvent *event);
#ifndef IQT_3
	virtual QSize sizeHint() const;
#endif
};


class iqtFrameSplitSubject : public QSplitter, public ibgFrameSplitSubject
{

	friend class iggSubjectFactory;

public:

	void AddSegment(iggFrame *frame);

protected:

	iqtFrameSplitSubject(iggFrameSplit *owner, bool hor);
};

#endif  // IQTFRAMESUBJECT_H

