/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_QT)


#include "iqtwidgethelper.h"


#include "ierror.h"
#include "istring.h"

#include "iggwidget.h"

#include "iqthelper.h"

#ifdef IQT_3
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qwidget.h>
#else
#include <QtGui/QWhatsThisClickedEvent>
#ifdef IQT_4
#include <QtGui/QAbstractButton>
#include <QtGui/QButtonGroup>
#include <QtGui/QToolTip>
#include <QtGui/QWhatsThis>
#include <QtGui/QWidget>
#else
#include <QtWidgets/QAbstractButton>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QToolTip>
#include <QtWidgets/QWhatsThis>
#include <QtWidgets/QWidget>
#endif
using namespace Qt;
#endif


//
//  helper class
//
class iqtWidgetBaloonHelp : public
#ifdef IQT_3
	QWhatsThis
#else
	QObject
#endif
{
	
public:
	
	iqtWidgetBaloonHelp(iqtWidgetHelper *owner, const iString &text) :
#ifdef IQT_3
		QWhatsThis(owner->mWidget)
#else
		QObject(owner->mWidget)
#endif
	{
		mOwner = owner;
		mText = text;
#ifndef IQT_3
		owner->mWidget->installEventFilter(this);
#endif
	}
	
	virtual ~iqtWidgetBaloonHelp()
	{
	}

#ifdef IQT_3
	virtual QString text(const QPoint& /*pos*/)
	{
		if(mText.IsEmpty()) return QString::null; else return iqtHelper::Convert(mText);
	}

	virtual bool clicked(const QString& ref)
	{
		mOwner->OpenHelpPage(iqtHelper::Convert(ref));
		return true;
	}
#else
	virtual bool eventFilter(QObject *obj, QEvent *e)
	{
		if(e!=0 && e->type()==QEvent::WhatsThisClicked && obj==mOwner->mWidget)
		{
			mOwner->OpenHelpPage(iqtHelper::Convert(iDynamicCast<QWhatsThisClickedEvent,QEvent>(INFO,e)->href()));
		}
		return QObject::eventFilter(obj,e);
	}
#endif

protected:
	
	iqtWidgetHelper *mOwner;
	iString mText;
};	


//
//  Main class
//
iqtWidgetHelper::iqtWidgetHelper(QWidget *widget, iggWidget *owner) : ibgWidgetHelper(owner)
{
	IASSERT(widget);
	mWidget = widget;
	mBaloonHelp = 0;

#ifndef IQT_3
#ifndef IQT_40
	widget->setAutoFillBackground(true);
#endif
#endif

	mIsDisabling = false;
}


iqtWidgetHelper::~iqtWidgetHelper()
{
//	if(mBaloonHelp != 0) delete mBaloonHelp; It is automatically deleted when a parent is deleted
}

//
//  Context-sensitive baloon help
//
void iqtWidgetHelper::SetBaloonHelp(const iString &tooltip, const iString &whatsthis)
{
#ifdef IQT_3
	QToolTip::add(mWidget,iqtHelper::Convert(tooltip));
#else
	mWidget->setToolTip(iqtHelper::Convert(tooltip));
	mWidget->setWhatsThis(iqtHelper::Convert(whatsthis));
#endif
	//
	//  Make the element to accept keyboard events
	//
	if(!whatsthis.IsEmpty())
	{
		mWidget->setFocusPolicy(QFocus::StrongFocus);
	}

	if(mBaloonHelp != 0)
	{
		delete mBaloonHelp;
	}

	mBaloonHelp = new iqtWidgetBaloonHelp(this,whatsthis);
}


bool iqtWidgetHelper::SupportsHTMLHelp() const
{
	return true;
}


bool iqtWidgetHelper::HasBaloonHelp() const
{
	return mBaloonHelp != 0;
}


//
//  Decorator functionality
//
void iqtWidgetHelper::Enable(bool s)
{
	mWidget->setEnabled(s);
}


bool iqtWidgetHelper::IsEnabled() const
{
	return mWidget->isEnabled();
}


void iqtWidgetHelper::Show(bool s)
{
	if(s) mWidget->show(); else mWidget->hide();
	mWidget->update();
}


bool iqtWidgetHelper::IsVisible() const
{
	return mWidget->isVisible();
}


void iqtWidgetHelper::SetBackgroundColor(const iColor &c)
{
	iqtHelper::SetBackground(mWidget,iqtHelper::Convert(c));
}


void iqtWidgetHelper::UnSetBackgroundColor()
{
#ifdef IQT_3
	mWidget->unsetPalette();
#else
	mWidget->setPalette(QPalette());
#ifndef IQT_40
	mWidget->setAutoFillBackground(false);
#endif
#endif
}


//
//  Generic slots
//
void iqtWidgetHelper::OnInt1(int v)
{
	if(!mWidget->signalsBlocked()) 
	{
		if(mIsDisabling) mWidget->setEnabled(false);
		this->OnInt1Body(v);
		if(mIsDisabling) mWidget->setEnabled(true);
	}
}


void iqtWidgetHelper::OnInt2(int v)
{
	if(!mWidget->signalsBlocked())
	{
		this->OnInt2Body(v);
	}
}


void iqtWidgetHelper::OnVoid1()
{
	if(!mWidget->signalsBlocked())
	{
		this->OnVoid1Body();
	}
}


void iqtWidgetHelper::OnVoid2()
{
	if(!mWidget->signalsBlocked())
	{
		this->OnVoid2Body();
	}
}


void iqtWidgetHelper::OnVoid3()
{
	if(!mWidget->signalsBlocked())
	{
		this->OnVoid3Body();
	}
}


void iqtWidgetHelper::OnBool1(bool v)
{
	if(!mWidget->signalsBlocked())
	{
		this->OnBool1Body(v);
	}
}


void iqtWidgetHelper::OnButtonClicked(QAbstractButton *b)
{
#ifndef IQT_3
	if(b!=0 && !mWidget->signalsBlocked())
	{
		QButtonGroup *g = b->group();
		if(g != 0) this->OnInt1Body(g->buttons().indexOf(g->checkedButton()));
	}
#endif
}

#endif
