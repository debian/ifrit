/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_QT)


#include "iqtwidgettexteditorsubject.h"


#include "iarray.h"
#include "ierror.h"
#include "istring.h"

#include "iggwidgettext.h"

#include "ibgwindowhelper.h"

#include "iqthelper.h"
#include "iqtwidgethelper.h"

#include "iarray.tlh"


using namespace iParameter;


#ifndef IQT_3

#include <QtGui/QMouseEvent>
#include <QtGui/QTextBlock>

//
// Helper class
//
class iqtWidgetTextEditorSubject::Location
{

public:

	Location(const QTextEdit *editor, int pos = -1)
	{
		this->Define(editor,editor->textCursor(),pos);
	}

	Location(const QTextEdit *editor, const QTextCursor &c, int pos = -1)
	{
		this->Define(editor,c,pos);
	}

	inline int Line() const { return mLine; }
	inline int Index() const { return mIndex; }

private:

	void Define(const QTextEdit *editor, const QTextCursor &c, int pos)
	{
		if(pos < 0) pos = c.position(); 
		QTextBlock b = editor->document()->findBlock(pos);
		mLine = b.blockNumber();
		mIndex = pos - b.position();
	}

	int mLine, mIndex;

};

#endif


//
// Constructs an editor and connects a syntax highlighter
//
iqtWidgetTextEditorSubject::iqtWidgetTextEditorSubject(iggWidgetTextEditor *owner, unsigned int mode) : QTextEdit(iqtHelper::Convert(owner->GetParent())), ibgWidgetTextEditorSubject(owner,mode), mMode(mode)
{
	mWidgetHelper = new iqtWidgetHelper(this,owner); IERROR_CHECK_MEMORY(mWidgetHelper);

	this->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding));
	this->setFocusPolicy(QFocus::StrongFocus);

	//
	//  Configure editor
	//
	QFont f(this->font());
	f.setFamily("courier");
	f.setFixedPitch(true);
	iqtHelper::SetFont(this,f);
	if(mode & TextEditorFlag::Wrapping)
	{
#ifdef IQT_3
		this->setWordWrap(QTextEdit::WidgetWidth);
#else
		this->setLineWrapMode(QTextEdit::WidgetWidth);
#endif
	}
	else
	{
#ifdef IQT_3
		this->setWordWrap(QTextEdit::NoWrap);
#else
		this->setLineWrapMode(QTextEdit::NoWrap);
#endif
	}
	this->setReadOnly(mode & TextEditorFlag::ReadOnly);
#ifdef IQT_3
	if(mode & TextEditorFlag::WithHTML) this->setTextFormat(Qt::RichText); else this->setTextFormat(Qt::PlainText);
#endif

	if(!connect(this,SIGNAL(textChanged()),this,SLOT(OnTextChanged()))) IBUG_WARN("Missed connection.");
	if(!connect(this,SIGNAL(selectionChanged()),this,SLOT(OnSelectionChanged()))) IBUG_WARN("Missed connection.");
#ifdef IQT_3
	if(!connect(this,SIGNAL(returnPressed()),this,SLOT(OnReturnPressed()))) IBUG_WARN("Missed connection.");
	if(!connect(this,SIGNAL(cursorPositionChanged(int,int)),this,SLOT(OnCursorPositionChanged(int,int)))) IBUG_WARN("Missed connection.");
#else
	if(!connect(this,SIGNAL(returnPressed2()),this,SLOT(OnReturnPressed()))) IBUG_WARN("Missed connection.");
	if(!connect(this,SIGNAL(cursorPositionChanged()),this,SLOT(OnCursorPositionChanged()))) IBUG_WARN("Missed connection.");
#endif
}


iqtWidgetTextEditorSubject::~iqtWidgetTextEditorSubject()
{
}


bool iqtWidgetTextEditorSubject::SupportsHTML() const
{
	return (mMode & TextEditorFlag::WithHTML);
}
	

bool iqtWidgetTextEditorSubject::TextModified() const
{
#ifdef IQT_3
	return this->isModified();
#else
	return this->document()->isModified();
#endif
}
	

bool iqtWidgetTextEditorSubject::AppendBreaksLine() const
{
	if(this->SupportsHTML()) return strcmp(qVersion(),"3.0.4") > 0; else return true;
}


//
//  Text manipulations
//
void iqtWidgetTextEditorSubject::Clear()
{
	this->blockSignals(true);
	this->clear();
	this->blockSignals(false);
}


void iqtWidgetTextEditorSubject::SetModified(bool s)
{
	this->blockSignals(true);
#ifdef IQT_3
	this->setModified(s);
#else
	this->document()->setModified(s);
#endif
	this->blockSignals(false);
}


void iqtWidgetTextEditorSubject::SetText(const iString &text)
{
	this->blockSignals(true);
#ifdef IQT_3
	this->setText(iqtHelper::Convert(text));
#else
	if(this->SupportsHTML()) this->setHtml(iqtHelper::Convert(text)); else this->setPlainText(iqtHelper::Convert(text));
#endif
	this->blockSignals(false);
}


iString iqtWidgetTextEditorSubject::GetText() const
{
#ifdef IQT_3
	return iqtHelper::Convert(this->text());
#else
	if(this->SupportsHTML()) return iqtHelper::Convert(this->toHtml()); else return iqtHelper::Convert(this->toPlainText());
#endif
}


void iqtWidgetTextEditorSubject::AppendText(const iString &text, bool scroll)
{
	this->blockSignals(true);
	//
	//  A work-around for a bug in some versions - needed on idunn, for example
	//
	// if(this->GetText().IsEmpty()) this->SetText(text); else
	this->append(iqtHelper::Convert(text));
#ifdef IQT_3
	if(scroll) this->scrollToBottom();
#endif
	this->blockSignals(false);
}

//
//  Line-by-line manipulation
//
int iqtWidgetTextEditorSubject::GetNumberOfLines() const
{
	//
	//  Fix a bug that returns 1 for empty text
	//
#ifdef IQT_3
	int n = this->paragraphs();
#else
	int n = this->document()->blockCount();
#endif
	if(n > 1) return n;
	if(this->GetText().IsEmpty())
	{
		return 0;
	}
	else
	{
		return 1;
	}
}


void iqtWidgetTextEditorSubject::RemoveLine(int n)
{
	if(n>=0 && n<this->GetNumberOfLines())
	{
		this->blockSignals(true);
#ifdef IQT_3
		this->removeParagraph(n);
#else
		QTextCursor c(this->document());
		c.setPosition(this->document()->findBlockByNumber(n).position());
		c.select(QTextCursor::BlockUnderCursor);
		c.removeSelectedText();
#endif
		this->blockSignals(false);
	}
}


iString iqtWidgetTextEditorSubject::GetLine(int n) const
{
	static iString none;

	if(n>=0 && n<this->GetNumberOfLines())
	{
#ifdef IQT_3
		return iqtHelper::Convert(this->text(n));
#else
		return iqtHelper::Convert(this->document()->findBlockByNumber(n).text());
#endif
	}
	else return none;
}


//
//  Text attribute manipulations
//
void iqtWidgetTextEditorSubject::AdjustFontSize(int s)
{
	if(!this->SupportsHTML())
	{
		this->blockSignals(true);
		if(s > 0) this->zoomIn(s); else if(s < 0) this->zoomOut(-s);
		this->blockSignals(false);
	}
}


void iqtWidgetTextEditorSubject::SetTextColor(const iColor &color)
{
	if(!this->SupportsHTML())
	{
		this->blockSignals(true);
#ifdef IQT_3
		this->setColor(iqtHelper::Convert(color));
#else
		this->setTextColor(iqtHelper::Convert(color));
#endif
		this->blockSignals(false);
	}
}


void iqtWidgetTextEditorSubject::SetBoldText(bool s)
{
	if(!this->SupportsHTML())
	{
		this->blockSignals(true);
#ifdef IQT_3
		this->setBold(s);
#else
		this->setFontWeight(QFont::Bold);
#endif
		this->blockSignals(false);
	}
}


void iqtWidgetTextEditorSubject::SetLineColor(int line, const iColor &color)
{
	this->blockSignals(true);
#ifdef IQT_3
	this->setParagraphBackgroundColor(line,iqtHelper::Convert(color));
#else
	QTextCursor c(this->document());
	c.setPosition(this->document()->findBlockByNumber(line).position());
	QTextBlockFormat f = c.blockFormat();
	f.setBackground(QBrush(iqtHelper::Convert(color)));
	c.setBlockFormat(f);
#endif
	this->blockSignals(false);
}


void iqtWidgetTextEditorSubject::UnSetLineColor(int line)
{
	this->blockSignals(true);
#ifdef IQT_3
	this->clearParagraphBackground(line);
#else
	QTextCursor c(this->document());
	c.setPosition(this->document()->findBlockByNumber(line).position());
	QTextBlockFormat f = c.blockFormat();
	f.clearBackground();
	c.setBlockFormat(f);
#endif
	this->blockSignals(false);
}


iColor iqtWidgetTextEditorSubject::GetLineColor(int line) const
{
#ifdef IQT_3
	return iqtHelper::Convert(this->paragraphBackgroundColor(line));
#else
	QTextCursor c(this->document());
	c.setPosition(this->document()->findBlockByNumber(line).position());
	return iqtHelper::Convert(c.blockFormat().background().color());
#endif
}


void iqtWidgetTextEditorSubject::SetReadOnly(bool s)
{
	this->setReadOnly(s);
}


//
//  Selection
//
void iqtWidgetTextEditorSubject::Select(int lineFrom, int indexFrom, int lineTo, int indexTo, bool leftJustified, int selNum)
{
	this->blockSignals(true);
	if(lineFrom>lineTo || (lineFrom==lineTo && indexFrom>=indexTo))
	{
#ifdef IQT_3
		this->removeSelection(selNum);
#else
		QTextCursor c(this->textCursor());
		c.clearSelection();
		this->setTextCursor(c);
#endif
	}
	else
	{
#ifdef IQT_3
		this->setSelection(lineFrom,indexFrom,lineTo,indexTo,selNum);
		if(leftJustified)
		{
			this->setCursorPosition(lineTo,0);
			this->ensureCursorVisible(); 
		}
#else
		QTextCursor c(this->textCursor());
		c.setPosition(this->document()->findBlockByNumber(lineFrom).position()+indexFrom);
		c.setPosition(this->document()->findBlockByNumber(lineTo).position()+indexTo,QTextCursor::KeepAnchor);
		if(leftJustified)
		{
//			c.setPosition(this->document()->findBlockByNumber(lineTo).position());
		}
		this->setTextCursor(c);
		if(leftJustified)
		{
			this->ensureCursorVisible(); 
		}
#endif
	}
	this->blockSignals(false);
}


void iqtWidgetTextEditorSubject::OnTextChanged()
{
	this->OnTextChangedBody();
}


void iqtWidgetTextEditorSubject::OnSelectionChanged()
{
	int lineFrom, indexFrom, lineTo, indexTo;

	this->GetSelection(lineFrom,indexFrom,lineTo,indexTo);

	if(lineFrom < 0) lineFrom = 0;
	this->OnSelectionChangedBody(lineFrom,indexFrom,lineTo,indexTo);
}


void iqtWidgetTextEditorSubject::OnCursorPositionChanged()
{
	int line, index;
	this->GetCursorPosition(line,index);
	this->OnCursorPositionChangedBody(line,index);
}


void iqtWidgetTextEditorSubject::OnCursorPositionChanged(int line, int index)
{
	this->OnCursorPositionChangedBody(line,index);
}


void iqtWidgetTextEditorSubject::OnReturnPressed()
{
	this->OnReturnPressedBody();
}


void iqtWidgetTextEditorSubject::GetCursorPosition(int &line, int &index)
{
#ifdef IQT_3
	this->getCursorPosition(&line,&index);
#else
	Location loc(this);
	line = loc.Line();
	index = loc.Index();
#endif
}


bool iqtWidgetTextEditorSubject::HasEditingFunction(int type) const
{
	switch(type)
	{
	case TextEditorFunction::Undo:
	case TextEditorFunction::Redo:
	case TextEditorFunction::Cut:
	case TextEditorFunction::Copy:
	case TextEditorFunction::Paste:
		{
			return true;
		}
	default:
		{
			return false;
		}
	}
}


void iqtWidgetTextEditorSubject::UseEditingFunction(int type)
{
	switch(type)
	{
	case TextEditorFunction::Undo:
		{
			this->undo();
			break;
		}
	case TextEditorFunction::Redo:
		{
			this->redo();
			break;
		}
	case TextEditorFunction::Cut:
		{
			this->cut();
			break;
		}
	case TextEditorFunction::Copy:
		{
			this->copy();
			break;
		}
	case TextEditorFunction::Paste:
		{
			this->paste();
			break;
		}
	default:
		{
		}
	}
}


//
//  Search & find
//
bool iqtWidgetTextEditorSubject::Find(const iString &text, bool cs, bool wo, bool forward, int &line, int &index)
{
#ifdef IQT_3
	return this->find(iqtHelper::Convert(text),cs,wo,forward,&line,&index);
#else
	QTextCursor prev(this->document());
	if(line>=0 && index>=0)
	{
		prev.setPosition(this->document()->findBlockByNumber(line).position()+index);
	}
	else
	{
		prev.setPosition(0);
	}
	QTextCursor next = this->document()->find(iqtHelper::Convert(text),prev.position(),QTextDocument::FindFlags((cs?QTextDocument::FindCaseSensitively:0)|(wo?QTextDocument::FindWholeWords:0)|(forward?QTextDocument::FindBackward:0)));
	if(next.isNull())
	{
		return false;
	}
	else
	{
		Location loc(this,next);
		line = loc.Line();
		index = loc.Index();
		return true;
	}
#endif
}


void iqtWidgetTextEditorSubject::enterEvent(QEvent * )
{
	//
	//  So that keyboard events are delivered without clicking first
	//
	if(!this->hasFocus())
	{
		this->setFocus();
	}
}


#ifdef IQT_3
void iqtWidgetTextEditorSubject::contentsMouseDoubleClickEvent(QMouseEvent *e)
#else
void iqtWidgetTextEditorSubject::mouseDoubleClickEvent(QMouseEvent *e)
#endif
{
	if(this->isReadOnly() && e->button()==Qt::LeftButton)
	{
		e->accept();
#ifdef IQT_3
		this->removeSelection();
		emit returnPressed();
#else
		QTextCursor c(this->textCursor());
		c.clearSelection();
		this->setTextCursor(c);
		emit returnPressed2();
#endif
	}
#ifdef IQT_3
	else QTextEdit::contentsMouseDoubleClickEvent(e);
#else
	else QTextEdit::mouseDoubleClickEvent(e);
#endif
}


void iqtWidgetTextEditorSubject::keyPressEvent(QKeyEvent *e)
{
	if(this->isReadOnly())
	{
		switch(e->key())
		{
		case Qt::Key_Return:
		case Qt::Key_Enter:
		case Qt::Key_Space:
			{
#ifdef IQT_3
				if(this->isReadOnly()) emit returnPressed();
#else
				if(this->isReadOnly()) emit returnPressed2();
#endif
				break;
			}
		}

#ifdef IQT_3
		if(e->state() & ControlButton)
#else
		if(e->modifiers() & Qt::CTRL)
#endif
		{
			int line, index;
			this->GetCursorPosition(line,index);
			//
			//  Add cursor moving if Cntr button is pressed
			//
			switch(e->key())
			{
			case Qt::Key_Up:
				{
					line--;
					this->SetCursorPosition(line,index);
					this->ensureCursorVisible();
					e->accept();
					return;
				}
			case Qt::Key_Down:
				{
					line++;
					this->SetCursorPosition(line,index);
					this->ensureCursorVisible();
					e->accept();
					return;
				}
			case Qt::Key_PageUp:
				{
					line -= 10;
					this->SetCursorPosition(line,index);
					this->ensureCursorVisible();
					e->accept();
					return;
				}
			case Qt::Key_PageDown:
				{
					line += 10;
					this->SetCursorPosition(line,index);
					this->ensureCursorVisible();
					e->accept();
					return;
				}
			case Qt::Key_Home:
				{
					line = 0;
					this->SetCursorPosition(line,index);
					this->ensureCursorVisible();
					e->accept();
					return;
				}
			case Qt::Key_End:
				{
					line = this->GetNumberOfLines() - 1;
					this->SetCursorPosition(line,index);
					this->ensureCursorVisible();
					e->accept();
					return;
				}
			}
		}

#ifdef IQT_3
		if(e->state() & ShiftButton)
#else
		if(e->modifiers() & Qt::SHIFT)
#endif
		{
			int lineFrom, indexFrom, lineTo, indexTo;
			this->GetSelection(lineFrom,indexFrom,lineTo,indexTo);

			//
			//  Add cursor moving if Cntr button is pressed
			//
			switch(e->key())
			{
			case Qt::Key_Up:
				{
					this->Select(--lineFrom,indexFrom,lineTo,indexTo);
					emit selectionChanged();
					e->accept();
					return;
				}
			case Qt::Key_Down:
				{
					this->Select(lineFrom,indexFrom,++lineTo,indexTo);
					emit selectionChanged();
					e->accept();
					return;
				}
			case Qt::Key_PageUp:
				{
					lineFrom -= 10;
					this->Select(lineFrom,indexFrom,lineTo,indexTo);
					emit selectionChanged();
					e->accept();
					return;
				}
			case Qt::Key_PageDown:
				{
					lineTo += 10;
					this->Select(lineFrom,indexFrom,lineTo,indexTo);
					emit selectionChanged();
					e->accept();
					return;
				}
			case Qt::Key_Home:
				{
					lineFrom = 0;
					this->Select(lineFrom,indexFrom,lineTo,indexTo);
					emit selectionChanged();
					e->accept();
					return;
				}
			case Qt::Key_End:
				{
					lineTo = this->GetNumberOfLines() - 1;
					this->Select(lineFrom,indexFrom,lineTo,indexTo);
					emit selectionChanged();
					e->accept();
					return;
				}
			}
		}
	}

	QTextEdit::keyPressEvent(e);
}


//
//  Private helpers
//
void iqtWidgetTextEditorSubject::GetSelection(int &lineFrom, int &indexFrom, int &lineTo, int &indexTo)
{
#ifdef IQT_3
	this->getSelection(&lineFrom,&indexFrom,&lineTo,&indexTo,0);
#else
	Location from(this,this->textCursor(),this->textCursor().selectionStart());
	lineFrom = from.Line();
	indexFrom = from.Index();

	Location to(this,this->textCursor(),this->textCursor().selectionEnd());
	lineTo = to.Line();
	indexTo = to.Index();
#endif
}


void iqtWidgetTextEditorSubject::SetCursorPosition(int line, int index)
{
#ifdef IQT_3
	this->setCursorPosition(line,index);
#else
	QTextCursor c(this->textCursor());
	c.setPosition(this->document()->findBlockByNumber(line).position()+index);
	this->setTextCursor(c);
#endif
}

#endif
