/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_QT)


#include "iqtframesubject.h"


#include "ierror.h"
#include "iimage.h"

#include "iggframe.h"

#include "iqthelper.h"
#include "iqtwidgethelper.h"

#ifdef IQT_3
#include <qimage.h>
#include <qlayout.h>
#include <qobjectlist.h> 
#else
#include <QtCore/QEvent>
#include <QtGui/QImage>
#ifdef IQT_4
#include <QtGui/QLayout>
#include <QtGui/QScrollBar>
#include <QtGui/QTabBar>
#else
#include <QtWidgets/QLayout>
#include <QtWidgets/QScrollBar>
#include <QtWidgets/QTabBar>
#endif
#endif


using namespace iParameter;


//
//  Plain Frame class
//
iqtFrameSubject::iqtFrameSubject(iggFrame *owner, int cols) : iqtExplicitFrameSubject(iqtHelper::Convert(owner->GetParent()),owner,cols)
{
}


iqtFrameSubject::~iqtFrameSubject()
{
}


//
//  FrameBook class
//
iqtFrameBookSubject::iqtFrameBookSubject(iggFrameBook *owner, bool withFeedback) : QTabWidget(iqtHelper::Convert(owner->GetParent())), ibgFrameBookSubject(owner,withFeedback)
{
	mWidgetHelper = new iqtWidgetHelper(this,owner); IERROR_CHECK_MEMORY(mWidgetHelper);

	this->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding));

#ifndef IQT_3
#ifndef IQT_40
	this->tabBar()->setIconSize(QSize(22,22));
#endif
#endif

	if(withFeedback)
	{
#ifdef IQT_3
		if(!this->connect(this,SIGNAL(currentChanged(QWidget * )),this,SLOT(OnCurrentChanged(QWidget * )))) IBUG_WARN("Missed connection.");
#else
		if(!this->connect(this,SIGNAL(currentChanged(int)),this,SLOT(OnCurrentChanged(int)))) IBUG_WARN("Missed connection.");
#endif
	}
}


iqtFrameBookSubject::~iqtFrameBookSubject()
{
	//
	//  Need to unparent all pages first
	//
	while(this->count() > 0)
	{
#ifdef IQT_3
		this->removePage(this->page(0));
#else
		this->removeTab(0);
#endif
	}
}


void iqtFrameBookSubject::OnCurrentChanged(QWidget *w)
{
	iDynamicCast<iqtWidgetHelper,ibgWidgetHelper>(INFO,mWidgetHelper)->OnInt1(this->indexOf(w));
}


void iqtFrameBookSubject::OnCurrentChanged(int v)
{
	iDynamicCast<iqtWidgetHelper,ibgWidgetHelper>(INFO,mWidgetHelper)->OnInt1(v);
}


void iqtFrameBookSubject::OpenPage(int i)
{
//	this->blockSignals(true);
#ifdef IQT_3
	this->setCurrentPage(i);
#else
	this->setCurrentIndex(i);
#endif
//	this->blockSignals(false);
}


void iqtFrameBookSubject::AddPage(const iString &title, const iImage *image, iggFrame *frame)
{
	QWidget *w = iqtHelper::Convert(frame); 
	IASSERT(w);
#ifndef IQT_3
	iqtHelper::SetParent(w,0);
#endif
	if(image == 0)
	{
		this->addTab(w,iqtHelper::ConvertWithModifiers(this,title));
	}
	else
	{
		this->addTab(w,iqtHelper::ConvertToIcon(*image),iqtHelper::ConvertWithModifiers(this,title));
	}
	if(this->parentWidget()!=0 && this->parentWidget()->layout()!=0)
	{
		this->parentWidget()->layout()->invalidate();
		this->parentWidget()->updateGeometry();
	}
}


void iqtFrameBookSubject::SetTabMode(int n, int m, const iString &title, const iImage *image)
{
	switch(m)
	{
	case BookTab::ImageOnly:
		{
			if(image != 0)
			{
#ifdef IQT_3
				this->changeTab(this->page(n),iqtHelper::ConvertToIcon(*image),QString());
#else
				this->setTabText(n,QString());
				this->setTabIcon(n,iqtHelper::ConvertToIcon(*image));
#endif
			}
			break;
		}
	case BookTab::TitleOnly:
		{
#ifdef IQT_3
			this->changeTab(this->page(n),QIconSet(),iqtHelper::ConvertWithModifiers(this,title));
#else
			this->setTabText(n,iqtHelper::ConvertWithModifiers(this,title));
			this->setTabIcon(n,QIcon());
#endif
			break;
		}
	case BookTab::TitleAndImage:
		{
			if(image != 0)
			{
#ifdef IQT_3
				this->changeTab(this->page(n),iqtHelper::ConvertToIcon(*image),iqtHelper::ConvertWithModifiers(this,title));
#else
				this->setTabText(n,iqtHelper::ConvertWithModifiers(this,title));
				this->setTabIcon(n,iqtHelper::ConvertToIcon(*image));
#endif
			}
			else
			{
#ifdef IQT_3
				this->changeTab(this->page(n),iqtHelper::ConvertWithModifiers(this,title));
#else
				this->setTabText(n,iqtHelper::ConvertWithModifiers(this,title));
				this->setTabIcon(n,QIcon());
#endif
			}
			break;
		}
	}
}


int iqtFrameBookSubject::GetTabMode()
{
	if(this->count() == 0) return BookTab::TitleAndImage;
	
#ifdef IQT_3
	bool t = !this->tabLabel(this->page(0)).isEmpty();
	bool i = !this->tabIconSet(this->page(0)).isNull();
#else
	bool t = !this->tabText(0).isEmpty();
	bool i = !this->tabIcon(0).isNull();
#endif

	if(t && i) return BookTab::TitleAndImage; else if(i) return BookTab::ImageOnly; else return BookTab::TitleOnly;
}


void iqtFrameBookSubject::ChangeIcon(int n, const iImage &image)
{
	if(n>=0 && n<this->count() && 
#ifdef IQT_3
		!this->tabIconSet(this->page(n)).isNull())
	{
		this->setTabIconSet(this->page(n),iqtHelper::ConvertToIcon(image));
#else
		!this->tabIcon(n).isNull())
	{
		this->setTabIcon(n,iqtHelper::ConvertToIcon(image));
#endif
	}
}


void iqtFrameBookSubject::SetOrientation(int v)
{
#ifdef IQT_3
	v = v % 2;
#else
	v = v % 4;
#endif
	this->setTabPosition(TabPosition(v));
}


int iqtFrameBookSubject::GetOrientation()
{
	return this->tabPosition();
}


//
//  FrameFlip class
//
iqtFrameFlipSubject::iqtFrameFlipSubject(iggFrameFlip *owner, bool expanding) : QStackedWidget(iqtHelper::Convert(owner->GetParent())), ibgFrameFlipSubject(owner,expanding)
{
	mWidgetHelper = new iqtWidgetHelper(this,owner); IERROR_CHECK_MEMORY(mWidgetHelper);

	this->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,expanding?QSizePolicy::Expanding:QSizePolicy::Minimum));
}


iqtFrameFlipSubject::~iqtFrameFlipSubject()
{
	int i;
	//
	//  Need to unparent all pages first
	//
	for(i=0; i<mCount; i++) this->removeWidget(this->widget(i));
}


void iqtFrameFlipSubject::ShowLayer(int i)
{
	if(i>=0 && i<mCount)
	{
#ifdef IQT_3
		this->raiseWidget(i);
#else
		this->setCurrentIndex(i);
#endif
	}
}


void iqtFrameFlipSubject::AddLayerBody(iggFrame *page)
{
#ifdef IQT_3
	this->addWidget(iqtHelper::Convert(page),mCount);
#else
	this->addWidget(iqtHelper::Convert(page));
	mCount = this->count();
#endif
	if(this->parentWidget()!=0 && this->parentWidget()->layout()!=0)
	{
		this->parentWidget()->layout()->invalidate();
		this->parentWidget()->updateGeometry();
	}
}


//
//  FrameScroll class
//
iqtFrameScrollSubject::iqtFrameScrollSubject(iggFrameScroll *owner, bool withHor, bool withVer) : QScrollArea(iqtHelper::Convert(owner->GetParent())), ibgFrameScrollSubject(owner,withHor)
{
	mWidgetHelper = new iqtWidgetHelper(this,owner); IERROR_CHECK_MEMORY(mWidgetHelper);

#ifdef IQT_3
	this->setVScrollBarMode(withVer?QScrollView::Auto:QScrollView::AlwaysOff);
	this->setHScrollBarMode(withHor?QScrollView::Auto:QScrollView::AlwaysOff);
	this->setResizePolicy(QScrollView::AutoOneFit);
#else
	this->setVerticalScrollBarPolicy(withVer?Qt::ScrollBarAsNeeded:Qt::ScrollBarAlwaysOff);
	this->setHorizontalScrollBarPolicy(withHor?Qt::ScrollBarAsNeeded:Qt::ScrollBarAlwaysOff);
	this->setWidgetResizable(true);
#endif

	this->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding));
}


void iqtFrameScrollSubject::AttachContents(iggFrame *frame)
{
#ifdef IQT_3
	if(this->viewport()->children() == 0)
#else
	if(this->viewport()->children().count() == 0)
#endif
	{
		iqtFrameSubject *f = iDynamicCast<iqtFrameSubject,ibgFrameSubject>(INFO,frame->GetSubject());
#ifdef IQT_3
		f->reparent(this->viewport(),QPoint(0,0));
		this->addChild(f);
#else
		this->setWidget(f);
#endif
	}
}


//
//  Qt-specific
//
bool iqtFrameScrollSubject::event(QEvent *e)
{
	//
	//  Remove the frame if the contents fits in full
	//
	if(e!=0 && (
#ifndef IQT_3
		e->type()==QEvent::LayoutRequest || 
#endif
		e->type()==QEvent::Paint))
	{
		if(this->verticalScrollBar()->isVisible())
		{
			if(this->frameShape() != QFrame::Panel)
			{
				this->setFrameStyle(QFrame::Panel | QFrame::Sunken);
				iqtHelper::SetBackground(this,0);
			}
		}
		else
		{
			if(this->frameShape() != QFrame::NoFrame)
			{
				this->setFrameStyle(QFrame::NoFrame);
#ifdef IQT_3
				this->unsetPalette();
#else
				this->setPalette(QPalette());
#endif
			}
		}
	}
	return QScrollArea::event(e);
}


#ifndef IQT_3
QSize iqtFrameScrollSubject::sizeHint() const
{
	return this->QAbstractScrollArea::sizeHint();
}
#endif


//
//  FrameSplit class
//
iqtFrameSplitSubject::iqtFrameSplitSubject(iggFrameSplit *owner, bool hor) : QSplitter(hor?Qt::Horizontal:Qt::Vertical ,iqtHelper::Convert(owner->GetParent())), ibgFrameSplitSubject(owner,hor)
{
	mWidgetHelper = new iqtWidgetHelper(this,owner); IERROR_CHECK_MEMORY(mWidgetHelper);
}


void iqtFrameSplitSubject::AddSegment(iggFrame *frame)
{
	//
	//  This is automatic
	//
}

#endif
