/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Qt-based selection box
//
#ifndef IQTWIDGETSELECTIONBOXSUBJECT_H
#define IQTWIDGETSELECTIONBOXSUBJECT_H


#include "iqt.h"

#ifdef IQT_3
#include <qbuttongroup.h>
#include <qwidget.h>
#define QRadioBox QButtonGroup
#elif defined(IQT_4)
#include <QtGui/QGroupBox>
#include <QtGui/QWidget>
#define QRadioBox QGroupBox
#else
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QWidget>
#define QRadioBox QGroupBox
#endif
#include "ibgwidgetselectionboxsubject.h"

class QButtonGroup;
class QComboBox;
class QLabel;
class QSpinBox;


//
//  Spin box
//
class iqtWidgetSpinBoxSubject : public QWidget, public ibgWidgetSpinBoxSubject
{

	friend class iggSubjectFactory;

public:

	virtual ~iqtWidgetSpinBoxSubject();

	virtual void SetStretch(int title, int box);

	//
	//  Decorator functions
	//
	virtual int GetValue() const;
	virtual void SetValue(int v);
	virtual void SetFirstEntryText(const iString &text);
	virtual void SetRange(int min, int max);
	virtual void SetStep(int step);
	virtual int Count();

protected:

	iqtWidgetSpinBoxSubject(iggWidget *owner, int min, int max, const iString &title, int step);

	QSpinBox *mBox;
	QLabel *mTitle;
};


//
//  Combo box
//
class iqtWidgetComboBoxSubject : public QWidget, public ibgWidgetComboBoxSubject
{

	friend class iggSubjectFactory;

public:

	virtual ~iqtWidgetComboBoxSubject();

	virtual void SetStretch(int title, int box);

	//
	//  Decorator functions
	//
	virtual int GetValue() const;
	virtual void SetValue(int v);
	virtual const iString GetText(int index = -1) const;
	virtual void SetText(const iString &text);
	virtual void InsertItem(const iString &text, int index = -1);
	virtual void InsertItem(const iImage &icon, const iString &text, int index = -1);
	virtual void SetItem(const iString &text, int index, bool vis);
	virtual void RemoveItem(int index);
	virtual int Count();
	virtual void Clear();
	virtual bool DoesContentFit() const;

protected:

	iqtWidgetComboBoxSubject(iggWidget *owner, const iString &title, bool bold);

	QComboBox *mBox;
	QLabel *mTitle;
};


//
//  Radio button box
//
class iqtWidgetRadioBoxSubject : public QRadioBox, public ibgWidgetRadioBoxSubject
{

	friend class iggSubjectFactory;

public:

	virtual ~iqtWidgetRadioBoxSubject();

	//
	//  Decorator functions
	//
	virtual int GetValue() const;
	virtual void SetValue(int v);
	virtual void InsertItem(const iString &text, int index = -1);
	virtual void InsertItem(const iImage &icon, const iString &text, int index = -1);
	virtual void SetItem(const iString &text, int index, bool vis);
	virtual void RemoveItem(int index);
	virtual int Count();

protected:

	iqtWidgetRadioBoxSubject(iggWidget *owner, int cols, const iString &title);

	QAbstractButton* FindButton(int index); // porting helper

#ifndef IQT_3
	QButtonGroup *mGroup;
#endif
};

#endif  // IQTWIDGETSELECTIONBOXSUBJECT_H

