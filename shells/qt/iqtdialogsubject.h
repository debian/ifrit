/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Qt-implementation of ibgDialogBase
//

#ifndef IQTDIALOGSUBJECT_H
#define IQTDIALOGSUBJECT_H


#include "iqt.h"

#ifdef IQT_3
#include <qdialog.h>
#elif defined(IQT_4)
#include <QtGui/QDialog>
#else
#include <QtWidgets/QDialog>
#endif
#include "ibgdialogsubject.h"


class QCloseEvent;
class QHBoxLayout;
class QKeyEvent;


class iqtDialogSubject : public QDialog, public ibgDialogSubject
{

	friend class iggSubjectFactory;

public:

	virtual ~iqtDialogSubject();

	virtual void Show(bool s);

protected:

	iqtDialogSubject(iggDialog *owner, const ibgWindowSubject *base, unsigned int mode, const iImage *icon, const iString &title);

	virtual void SetFrame(ibgFrameSubject *frame);

	//
	//  QT specific
	//
	virtual void keyPressEvent(QKeyEvent *e);
	virtual void closeEvent(QCloseEvent *e);
};

#endif  // IQTDIALOGSUBJECT_H

