/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A Qt-based window helper
//

#ifndef IQTWINDOWHELPER_H
#define IQTWINDOWHELPER_H


#include "ibgwindowhelper.h"


class iImage;
class iString;

class QWidget;


class iqtWindowHelper : public ibgWindowHelper
{

public:

	iqtWindowHelper(QWidget *widget, const iImage *icon, const iString &title, bool disableClose = false);

	virtual void SetWindowGeometry(int wg[4], bool force);
	virtual void GetWindowGeometry(int wg[4], bool force) const;

	virtual void GetFrameSize(int &w, int &h) const;

	virtual void Show(bool s);
	virtual bool IsVisible() const;

	virtual void ShowAsWindow();
	virtual void ShowAsIcon();

	virtual void RestoreDecoration();

	inline QWidget* GetWidget() const { return mWidget; }

protected:

	QWidget *mWidget;

private:

	iqtWindowHelper(const iqtWindowHelper &);
	void operator=(const iqtWindowHelper &);
};

#endif  // IQTWINDOWHELPER_H
