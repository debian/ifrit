/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/
  
//
//  This class is based on original vtkQGLRenderWindow class written by Manish P. Pagey [pagey@drcsdca.com ]
//

#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_QT)


#include "iqtrenderwindowsubject.h"


#include "ierror.h"
#include "iviewmodule.h"
#include "ivtk.h"

#include "iggimagefactory.h"
#include "iggmainwindow.h"
#include "iggrenderwindow.h"
#include "iggshell.h"

#include "iqthelper.h"
#include "iqtmainwindowsubject.h"
#include "iqtwindowhelper.h"

#include <vtkRenderWindowInteractor.h>

#ifdef IQT_3
#include <qapplication.h>
#include <qimage.h> 
#include <qlayout.h>
#include <qtimer.h>
#else
#include <QtCore/QTimer>
#include <QtGui/QImage> 
#include <QtGui/QMouseEvent>
#ifdef IQT_4
#include <QtGui/QApplication>
#include <QtGui/QLayout>
#else
#include <QtWidgets/QApplication>
#include <QtWidgets/QLayout>
#include <QtGui/QWindow>
#endif
#ifdef Q_WS_X11
#include <QtGui/QX11Info>
#endif
#endif

//
//  Templates
//
#include "iarray.tlh"


using namespace iParameter;


iqtRenderWindowSubject::iqtRenderWindowSubject(iggRenderWindow *owner, QWidget* parent) : 
#ifdef IQT_3
	QGLWidget(parent,0,0,parent?Qt::WFlags(0):iqtHelper::GetFlags(WindowType::Render)), 
#else
	QGLWidget(parent,0,parent?Qt::WFlags(0):iqtHelper::GetFlags(WindowType::Render)), 
#endif
	ibgRenderWindowSubject(owner)
{
	mWindowHelper = new iqtWindowHelper(this,iggImageFactory::FindIcon("genie1vtk.png"),"",true); IERROR_CHECK_MEMORY(mWindowHelper);

	this->setFocusPolicy(QFocus::StrongFocus);

	//
	//  Window stuff
	//
	if(!this->doubleBuffer())
	{
		IBUG_FATAL("IFrIT only works in double buffering mode.");
	}
	this->setMinimumWidth(120);
	this->setMinimumHeight(120);
	this->resize(320,240);
	this->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding));
	this->setAutoBufferSwap(false);

	if(parent!=0 && parent->layout()!=0) iDynamicCast<QGridLayout,QLayout>(INFO,parent->layout())->addWidget(this,0,0);
}


iqtRenderWindowSubject::~iqtRenderWindowSubject() 
{
	int i;
	//
	//  Do we have undeleted timers?
	//
	for(i=0; i<mTimers.Size(); i++) delete mTimers[i];

	if(this->parentWidget() != 0)
	{
		this->AttachToParent(0);
		delete this->parentWidget();
	}
}


void iqtRenderWindowSubject::AttachToParent(QWidget *parent, bool border)
{
	if(parent == 0)
	{
		iqtHelper::SetParent(this,parent,iqtHelper::GetFlags(border ? WindowType::Render : WindowType::BorderlessRender));
#ifndef IQT_3
		this->setFormat(this->format()); // is this Qt 4 bug?
#endif
	}
	else
	{
		iqtHelper::SetParent(this,parent,0);
	}
#ifdef I_DEBUG
	if(!this->context()->isValid())
	{
		IBUG_FATAL("Invalid context in iqtRenderWindowSubject.");
	}
#endif
}


//
// Initialize the window for rendering.
//
void iqtRenderWindowSubject::Initialize(bool show) 
{
	//
	// Enable mouse tracking. It is required by trackball interactor style. 
	//
	this->setMouseTracking(true);

	//
	//  show the window
	//
	if(show)
	{
		if(this->parentWidget() != 0) this->parentWidget()->show(); else this->show();
	}
}


//
// Finalize the window before deletion
//
void iqtRenderWindowSubject::Finalize() 
{
	// I don't yet know what should go here
	//this->doneCurrent();
	this->hide();
}


//
//  Mark active/inactive window
//
void iqtRenderWindowSubject::UpdateActiveStatus(bool last)
{
	if(mOwner->IsActiveWindow() || last)
	{
		static const iImage *icon = iggImageFactory::FindIcon("genie1vtk.png");
		if(icon != 0)
		{
#ifdef IQT_3
			this->setIcon(iqtHelper::ConvertToPixmap(*icon));
#else
			this->setWindowIcon(iqtHelper::ConvertToIcon(*icon));
#endif
		}
	}
	else
	{
		static const iImage *icon = iggImageFactory::FindIcon("genie1vtk2.png");
		if(icon != 0)
		{
#ifdef IQT_3
			this->setIcon(iqtHelper::ConvertToPixmap(*icon));
#else
			this->setWindowIcon(iqtHelper::ConvertToIcon(*icon));
#endif
		}
	}

#ifdef IQT_3
	QString s = this->caption();
#else
	QString s = this->windowTitle();
#endif
	if(mOwner->IsActiveWindow() && !last)
	{
		if(s.left(4) != "*** ")
		{
			s = "*** " + s;
#ifdef IQT_3
			this->setCaption(s);
#else
			this->setWindowTitle(s);
#endif
		}
	}
	else
	{
		if(s.left(4) == "*** ")
		{
			s = s.mid(4);
#ifdef IQT_3
			this->setCaption(s);
#else
			this->setWindowTitle(s);
#endif
		}
	}
}


//
// End the rendering process and display the image. 
//
void iqtRenderWindowSubject::SwapBuffers() 
{
	if(this->isVisible())
	{
#ifndef IQT_PRE5		
		if(this->windowHandle()!=0 && this->windowHandle()->isExposed())
#endif
			this->swapBuffers();
	}
}


void iqtRenderWindowSubject::SetBorder(bool s)
{
    if(!this->isTopLevel()) return;

	QPoint p = this->pos();
	this->AttachToParent(0,s);
	this->move(p);
	this->show();
}


void iqtRenderWindowSubject::SetFullScreen(bool s) 
{
	if(s) this->showFullScreen(); else this->showNormal();
}


void iqtRenderWindowSubject::MakeCurrent() 
{
	this->makeCurrent();
}


void* iqtRenderWindowSubject::GetDisplay() const
{
#if defined(Q_WS_X11)
#ifdef IQT_3
	return (void *)this->x11Display();
#else
	return (void *)this->x11Info().display();
#endif
#else
	return 0;
#endif
}


void* iqtRenderWindowSubject::GetContext() const
{
	return (void *)this->context();
}


void* iqtRenderWindowSubject::GetWindowId() const
{
	return (void *)this->winId();
}


void iqtRenderWindowSubject::SetWindowName(const iString &s)
{
#ifdef IQT_3
	this->setCaption(iqtHelper::Convert(s));
#else
	this->setWindowTitle(iqtHelper::Convert(s));
#endif
} 


void iqtRenderWindowSubject::RenderIntoMemory(iImage &image)
{
#ifdef IQT_3
	QImage q = this->renderPixmap(image.Width(),image.Height(),false).convertToImage();
#else
	QImage q = this->renderPixmap(image.Width(),image.Height(),false).toImage();
#endif
	image = iqtHelper::Convert(q);
}


bool iqtRenderWindowSubject::IsReadyForDumpingImage()
{
	if(this->isMinimized()) 
	{
		this->showNormal();
		if(this->isMinimized()) 
		{
			this->GetShell()->PopupWindow("The visualization window must be visible.\n Please, restore the visualization window before creating an image.",MessageType::Warning);
			return false;
		}
	}
	//
	//  Make sure we are ready for the action...
	//
	this->raise();
	qApp->flush();
//	qApp->processEvents();

	if(this->isVisible()) return true; else return false;
}


//
//  RenderWindowInteractor methods
//
int iqtRenderWindowSubject::InternalCreateTimer(int /*timerId*/, int timerType, unsigned long duration)
{
	QTimer *timer = new QTimer(this); IERROR_CHECK_MEMORY(timer);
	if(!connect(timer,SIGNAL(timeout()),this,SLOT(OnTimer()))) IBUG_FATAL("Missed connection.");

	mTimers.Add(timer);
	//
	// Start a one-shot timer for 10ms. 
	//
	bool ss = (timerType==vtkRenderWindowInteractor::OneShotTimer);
#ifdef IQT_3
	timer->start(duration,ss);
#else
	timer->setSingleShot(ss);
	timer->start(duration);
#endif
	return mTimers.Size();
}


int iqtRenderWindowSubject::InternalDestroyTimer(int platformTimerId)
{
	if(platformTimerId<1 || platformTimerId>mTimers.Size()) return 0;
	platformTimerId--;

	mTimers[platformTimerId]->stop();
	delete mTimers[platformTimerId];
	mTimers.Remove(platformTimerId);
	//
	// QTimer will automatically expire after 10ms. So 
	// we do not need to do anything here.
	//
	return 1;
}


// ------------------------------------------------------------
// Methods from QGLWidget class. 
//
void iqtRenderWindowSubject::initializeGL() 
{
	//
	//  I don't think we need this - it crashes X-server over the remote connection
	//
	//mOwner->OpenGLInit();
	mWindowHelper->RestoreDecoration();
}


void iqtRenderWindowSubject::paintGL() 
{
	//
	//  iqtRenderWindowSubject only works with AutoBufferSwap turned off
	//
	if(this->autoBufferSwap())
	{
		IBUG_WARN("iqtRenderWindowSubject only works with AutoBufferSwap turned off.");
		return;
	}

	//
	//  Must do it this way so that child Render operations are not called from here
	//
	this->Render();
}


void iqtRenderWindowSubject::resizeGL(int w, int h) 
{
	//
	//  I do not see why we need it
	//
}


void iqtRenderWindowSubject::mouseMoveEvent(QMouseEvent *event) 
{
#ifdef IQT_3
	this->OnMouseMoveBody(event->x(),this->height()-event->y()-1,iqtHelper::Convert(event->state()));
#else
	this->OnMouseMoveBody(event->x(),this->height()-event->y()-1,iqtHelper::Convert(event->buttons() | event->modifiers()));
#endif
}


void iqtRenderWindowSubject::mousePressEvent(QMouseEvent *event) 
{
#ifdef IQT_3
	this->OnMousePressBody(event->x(),this->height()-event->y()-1,iqtHelper::Convert(event->stateAfter()));
#else
	this->OnMousePressBody(event->x(),this->height()-event->y()-1,iqtHelper::Convert(event->buttons() | event->modifiers()));
#endif
}


void iqtRenderWindowSubject::mouseReleaseEvent(QMouseEvent *event) 
{
#ifdef IQT_3
	this->OnMouseReleaseBody(event->x(),this->height()-event->y()-1,iqtHelper::Convert(event->state()));
#else
	this->OnMouseReleaseBody(event->x(),this->height()-event->y()-1,iqtHelper::Convert(event->buttons() | event->button() | event->modifiers()));
#endif
}


void iqtRenderWindowSubject::keyPressEvent(QKeyEvent * event)
{
#ifdef IQT_3
	this->OnKeyPressBody(event->key(),iqtHelper::Convert(event->state()));
#else
	this->OnKeyPressBody(event->key(),iqtHelper::Convert(event->modifiers()));
#endif
}


void iqtRenderWindowSubject::OnTimer() 
{
	this->OnTimerBody();
}


bool iqtRenderWindowSubject::IsDirect() const
{
	return this->format().directRendering();
}


bool iqtRenderWindowSubject::IsCurrent() const
{
	return this->context() == QGLContext::currentContext();
}


void iqtRenderWindowSubject::RequestStereo(bool s)
{
	QGLFormat f = this->format();
	//
	//  This misbehaves non-fatally under Qt 4.8, so be conservative
	//
	if(f.stereo() != s)
	{
		f.setStereo(s);
		//this->setFormat(f);
	}
}


bool iqtRenderWindowSubject::event(QEvent *e)
{
	//
	//  I do not remember now why we need to bounce back only spontaneous events. Under at least some X11 servers 
	//  a non-spontaneous paint event arrives during the new file load, when the window is blocked. It causes the
	//  execution of the pipeline without the complete data, which causes a seg fault inside some VTK classes.
	//  hence, a check for the paint event here.
	//
	if(e!=0 && (e->spontaneous() || e->type()==QEvent::Paint) && this->IsBlocked()) 
	{
		return false;
	}
	else
	{
		return QGLWidget::event(e);
	}
}

//
//  Event handles
//
void iqtRenderWindowSubject::closeEvent(QCloseEvent *e)
{
	e->ignore();
	mOwner->OnCloseAttempt();
}


void iqtRenderWindowSubject::focusInEvent(QFocusEvent *e)
{ 
    QGLWidget::focusInEvent(e);
//	repaint(false);  not sure why we need it
	this->OnWindowFocusInBody();
}


void iqtRenderWindowSubject::focusOutEvent(QFocusEvent *e)
{
    QGLWidget::focusOutEvent(e);
//	repaint(false);
	this->OnWindowFocusOutBody();
}
	

void iqtRenderWindowSubject::enterEvent(QEvent *e)
{ 
    QGLWidget::enterEvent(e);
	if(this->hasFocus()) this->OnWindowEnterBody();
}


void iqtRenderWindowSubject::leaveEvent(QEvent *e)
{
    QGLWidget::leaveEvent(e);
	if(this->hasFocus()) this->OnWindowLeaveBody();
}
	

void iqtRenderWindowSubject::resizeEvent(QResizeEvent *e)
{
	QGLWidget::resizeEvent(e);
	this->OnWindowResizeBody();
	//qApp->removePostedEvents(this);
}


void iqtRenderWindowSubject::moveEvent(QMoveEvent *e)
{
    QGLWidget::moveEvent(e);
    this->OnWindowMoveBody();
}

#endif
