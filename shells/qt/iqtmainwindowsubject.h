/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A Qt-ased implementation for the MainWindowSubject class
//

#ifndef IQTMAINWINDOWSUBJECT_H
#define IQTMAINWINDOWSUBJECT_H


#include "iqtmenuwindowsubject.h"
#include "ibgmainwindowsubject.h"


#ifdef IQT_3
class QWorkspace;
#else
class QMdiArea;
#define QWorkspace	QMdiArea
#endif


class iqtMainWindowSubject : public iqtMenuWindowSubject, public ibgMainWindowSubject
{

	Q_OBJECT

	friend class iggSubjectFactory;

public:

	virtual void AttachRenderWindow(iggRenderWindow *rw);

protected:

	iqtMainWindowSubject(iggMainWindow *owner);
	virtual ~iqtMainWindowSubject();

	//
	//  General members
	//
	virtual void ShowToolTips(bool s);
	virtual void SetTheme(const iString &name);
	virtual void StartTimer();

	//
	//  Construction helpers
	//
	virtual void SetTopParentSubjects(iggFrameTopParent *busyIndicatorFrame, iggFrameTopParent *visitedFileListFrame);
	virtual void PopulateStatusBar(iggFrame *busyIndicatorFrame, ibgWidgetProgressBarSubject *progressBarSubject, iggFrame *visitedFileListFrame);

	//
	//  Docking helpers
	//
	virtual void PlaceWindowsInDockedPositions();
	virtual void RestoreWindowsFromDockedPositions();

	//
	//  Qt-specific
	//
	virtual bool event(QEvent *e);
	virtual void moveEvent(QMoveEvent *e);
	virtual void closeEvent(QCloseEvent *e);
	virtual void timerEvent(QTimerEvent *e);
	virtual void resizeEvent(QResizeEvent *e);
//	virtual void keyPressEvent(QKeyEvent *e); 
//	virtual void keyReleaseEvent(QKeyEvent *e); 
//	virtual void focusInEvent(QFocusEvent *e);
//	virtual void focusOutEvent(QFocusEvent *e);

private:

	int mTimerId;
	QWidget *mMainFrame;
	QWorkspace *mDockingWorkspace;

	iqtMainWindowSubject(const iqtMainWindowSubject&); // Not implemented.
	void operator=(const iqtMainWindowSubject&);  // Not implemented.
};

#endif  // IQTMAINWINDOWSUBJECT_H
