/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_QT)


#include "iqtmenuwindowsubject.h"


#include "iobject.h"
#include "ishell.h"

#include "iggframetopparent.h"
#include "igghandle.h"
#include "iggmenuhelper.h"
#include "iggmenuwindow.h"

#include "iqtexplicitframesubject.h"
#include "iqthelper.h"
#include "iqtwindowhelper.h"

#ifdef IQT_3
#include <qfiledialog.h>
#include <qmenubar.h>
#include <qtoolbar.h>
#include <qtoolbutton.h>
#include <qwhatsthis.h>
#else
#include <QtCore/QEvent>
#include <QtGui/QCloseEvent>
#ifdef IQT_4
#include <QtGui/QActionGroup>
#include <QtGui/QFileDialog>
#include <QtGui/QMenuBar>
#include <QtGui/QToolBar>
#include <QtGui/QToolButton>
#include <QtGui/QWhatsThis>
#else
#include <QtWidgets/QActionGroup>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWhatsThis>
#endif
#endif

//
//  templates
//
#include "iarray.tlh"


using namespace iParameter;


//
//  helper class
//
iqtMenuItem::iqtMenuItem(int id, iqtMenuWindowSubject *subject, const iggMenuHelperBase *enabler, const iggMenuHelperBase *checker) : QAction(subject)
{
	if(enabler == 0)
	{
		mEnabler = 0;
	}
	else
	{
		mEnabler = enabler->Clone(); IASSERT(mEnabler);
	}

	if(checker == 0)
	{
		mChecker = 0;
	}
	else
	{
		mChecker = checker->Clone(); IASSERT(mChecker);
	}

	mId = id;
	mSubject = subject;

#ifdef IQT_3
	connect(this,SIGNAL(activated()),this,SLOT(OnActivated()));
#else
	connect(this,SIGNAL(triggered()),this,SLOT(OnActivated()));
#endif
	connect(this,SIGNAL(toggled(bool)),this,SLOT(OnToggled(bool)));
}


iqtMenuItem::~iqtMenuItem()
{
	if(mEnabler != 0) delete mEnabler;
	if(mChecker != 0) delete mChecker;
}


void iqtMenuItem::Update()
{
	if(mEnabler != 0)
	{
#ifdef I_DEBUG
		if(mId == 904)
		{
			int o = 0;
		}
#endif
		this->setEnabled(mEnabler->IsSet());
	}
	else this->setEnabled(true);

	if(mChecker != 0)
	{
		this->blockSignals(true);
#ifdef IQT_3
		this->setOn(mChecker->IsSet());
#else
		this->setChecked(mChecker->IsSet());
#endif
		this->blockSignals(false);
	}
}


void iqtMenuItem::OnActivated()
{
	emit VoidAction(mId);
}


void iqtMenuItem::OnToggled(bool v)
{
	emit BoolAction(mId,v);
}


//
//  helper class
//
iqtMenu::iqtMenu(iqtMenuWindowSubject *subject, const iString &text, bool exclusive) : QMenu(subject->menuBar())
{
	IASSERT(subject);
	mSubject = subject;
	mParent = 0;
	this->Define(exclusive);
#ifdef IQT_3
	subject->menuBar()->insertItem(iqtHelper::Convert(text),this);
#else
	this->setTitle(iqtHelper::Convert(text));
	subject->menuBar()->addMenu(this);
#endif
}


iqtMenu::iqtMenu(iqtMenu *parent, const iString &text, bool exclusive) : QMenu(parent)
{
	IASSERT(parent);
	mSubject = parent->GetSubject();
	mParent = parent;
	this->Define(exclusive);
#ifdef IQT_3
	parent->insertItem(iqtHelper::Convert(text),this);
#else
	this->setTitle(iqtHelper::Convert(text));
	parent->addMenu(this);
#endif
}


void iqtMenu::Define(bool exclusive)
{
#ifdef IQT_3
	mGroup = new QActionGroup(this,0,exclusive); IERROR_CHECK_MEMORY(mGroup);
	mGroup->setUsesDropDown(false);
#else
	mGroup = new QActionGroup(this); IERROR_CHECK_MEMORY(mGroup);
	mGroup->setExclusive(exclusive);
#endif
}


void iqtMenu::AddMenuItem(iqtMenuItem *item)
{
#ifdef IQT_3
	item->addTo(this);
	mGroup->add(item);
#else
	this->addAction(item);
	mGroup->addAction(item);
#endif
}


void iqtMenu::AddSeparator()
{
#ifdef IQT_3
	this->insertSeparator();
#else
	this->addSeparator();
#endif
}


//
//  Main class
//
iqtMenuWindowSubject::iqtMenuWindowSubject(iggMenuWindow *owner, const iImage *icon, const iString &title) : 
#ifdef IQT_3
	QMainWindow(0,0,iqtHelper::GetFlags(WindowType::Main)), 
#else
	QMainWindow(0,iqtHelper::GetFlags(WindowType::Main)), 
#endif
	ibgMenuWindowSubject(owner,icon,title)
{
	mWindowHelper = new iqtWindowHelper(this,icon,title.IsEmpty()?title:("IFrIT - "+title)); IERROR_CHECK_MEMORY(mWindowHelper);
	mCurrentMenu = 0;

	mToolBar = new QToolBar(this); 
#ifndef IQT_3
	mToolBar->setBackgroundRole(QPalette::Button);
#ifndef IQT_40
	mToolBar->setAutoFillBackground(true);
#endif
	this->addToolBar(mToolBar);
#endif

//	mToolbar->setAcceptDrops(true);  //  add dynamic toolbar later?

	iqtHelper::SetBackground(this->menuBar(),2);
}


iqtMenuWindowSubject::~iqtMenuWindowSubject()
{
}


void iqtMenuWindowSubject::SetGlobalFrame(iggFrameTopParent *globalFrame, int cols)
{
	if(globalFrame!=0 && globalFrame==mOwner->GetGlobalFrame() && globalFrame->GetSubject()==0)
	{
		iqtExplicitFrameSubject *s = new iqtExplicitFrameSubject(this,globalFrame,cols);
		globalFrame->AttachSubject(s);
		this->setCentralWidget(s);
	}
}


void iqtMenuWindowSubject::SetToolBarIcon(int id, const iImage &icon)
{
	int i, n;
	
	n = mToolBarButtons.Size();
	for(i=0; i<n; i++) if(mToolBarButtons[i]->GetId() == id)
	{
#ifdef IQT_3
		mToolBarButtons[i]->setIconSet(iqtHelper::ConvertToIcon(icon));
#else
		mToolBarButtons[i]->setIcon(iqtHelper::ConvertToIcon(icon));
#endif
		return;
	}
}


void iqtMenuWindowSubject::UpdateMenus()
{
	int i, n;
	
	n = mMenuItems.Size();
	for(i=0; i<n; i++)
	{
		mMenuItems[i]->Update();
	}

	n = mToolBarButtons.Size();
	for(i=0; i<n; i++)
	{
		mToolBarButtons[i]->Update();
	}
}


iString iqtMenuWindowSubject::GetFileName(const iString &header, const iString &file, const iString &selection, bool reading)
{
	static QFileDialog *fd = new QFileDialog(this); IERROR_CHECK_MEMORY(fd); // only one is needed, as it is modal

	iString out;
	if(reading)
	{
		out = iqtHelper::Convert(QFileDialog::getOpenFileName(
#ifdef IQT_3
			//  QString QFileDialog::getOpenFileName(const QString &dir, const QString &filter, QWidget *parent, const char *name, const QString &caption, QString *selectedFilter, bool resolveSymlinks) 
			iqtHelper::Convert(file),iqtHelper::Convert(selection),this,0,iqtHelper::Convert(header))
#else
			//  QString QFileDialog::getOpenFileName(QWidget *parent, const QString &caption, const QString &dir, const QString &filter, QString *selectedFilter, Options options) 
			this,iqtHelper::Convert(header),iqtHelper::Convert(file),iqtHelper::Convert(selection))
#endif
			);
	}
	else
	{
		QString selected;
		out = iqtHelper::Convert(QFileDialog::getSaveFileName(
#ifdef IQT_3
			iqtHelper::Convert(file),iqtHelper::Convert(selection),this,0,iqtHelper::Convert(header),&selected)
#else
			this,iqtHelper::Convert(header),iqtHelper::Convert(file),iqtHelper::Convert(selection),&selected)
#endif
			);
		if(!selected.isEmpty())
		{
			iString s = iqtHelper::Convert(selected).Section("(",1,1).Section(")",0,0);
			if(s.BeginsWith("*."))
			{
				s = s.Part(1);
				if(!out.EndsWith(s)) out += s;
			}
		}
	}
	return out;
}


//
//  Menu creation
//
void iqtMenuWindowSubject::BeginMenu(const iString &text, bool exclusive)
{
	if(mCurrentMenu == 0)
	{
		mCurrentMenu = new iqtMenu(this,text,exclusive);
	}
	else
	{
		mCurrentMenu = new iqtMenu(mCurrentMenu,text,exclusive);
	}
	IERROR_CHECK_MEMORY(mCurrentMenu);
}


void iqtMenuWindowSubject::EndMenu()
{
	if(mCurrentMenu == 0)
	{
		IBUG_FATAL("Misconfigured menu structure");
	}

	mCurrentMenu = mCurrentMenu->GetParent();
}


void iqtMenuWindowSubject::AddMenuItem(int id, const iString &text, const iImage *icon, const iString &accel, bool toggle, bool on, const iggMenuHelperBase *enabler, const iggMenuHelperBase *checker)
{
	if(checker!=0 && !toggle)
	{
		IBUG_FATAL("Incorrectly configured iqtMenuItem");
	}

	iqtMenuItem *mi = new iqtMenuItem(id,this,enabler,checker); IERROR_CHECK_MEMORY(mi);
	mMenuItems.Add(mi);

#ifdef IQT_3
	if(icon != 0) mi->setIconSet(iqtHelper::ConvertToIcon(*icon));
	mi->setMenuText(iqtHelper::Convert(text));
	mi->setToggleAction(toggle);
	if(!accel.IsEmpty()) mi->setAccel(QKeySequence(iqtHelper::Convert(accel)));
#else
	if(icon != 0) mi->setIcon(iqtHelper::ConvertToIcon(*icon));
	mi->setText(iqtHelper::Convert(text));
	mi->setCheckable(toggle);
	if(!accel.IsEmpty()) mi->setShortcut(QKeySequence(iqtHelper::Convert(accel)));
#endif

	if(toggle)
	{
#ifdef IQT_3
		mi->setOn(on);
#else
		mi->setChecked(on);
#endif
		connect(mi,SIGNAL(BoolAction(int,bool)),this,SLOT(OnMenuBool(int,bool)));
	}
	else
	{
		connect(mi,SIGNAL(VoidAction(int)),this,SLOT(OnMenuVoid(int)));
	}

	mCurrentMenu->AddMenuItem(mi);
	mi->Update();
}


void iqtMenuWindowSubject::AddMenuSeparator()
{
	mCurrentMenu->AddSeparator();
}


//
//  Tool bar creation
//
void iqtMenuWindowSubject::AddToolBarButton(int id, const iString &tooltip, const iImage *icon, bool toggle, const iggMenuHelperBase *enabler, const iggMenuHelperBase *checker)
{
	int i, n = mMenuItems.Size();

	for(i=0; i<n; i++)
	{
		if(mMenuItems[i]->GetId() == id) break;
	}

	if(i < n)
	{
		//
		//  This action already exists
		//
#ifdef IQT_3
		mMenuItems[i]->addTo(mToolBar);
#else
		mToolBar->addAction(mMenuItems[i]);
#endif
		mMenuItems[i]->setToolTip(iqtHelper::Convert(tooltip));
	}
	else
	{
		IASSERT(icon);

		if(checker!=0 && !toggle)
		{
			IBUG_FATAL("Incorrectly configured iqtMenuItem");
		}

		iqtMenuItem *mi = new iqtMenuItem(id,this,enabler,checker); IERROR_CHECK_MEMORY(mi);
		mToolBarButtons.Add(mi);

#ifdef IQT_3
		mi->setIconSet(iqtHelper::ConvertToIcon(*icon));
		mi->setToggleAction(toggle);
#else
		mi->setIcon(iqtHelper::ConvertToIcon(*icon));
		mi->setCheckable(toggle);
#endif

		if(toggle)
		{
			connect(mi,SIGNAL(BoolAction(int,bool)),this,SLOT(OnMenuBool(int,bool)));
		}
		else
		{
			connect(mi,SIGNAL(VoidAction(int)),this,SLOT(OnMenuVoid(int)));
		}
#ifdef IQT_3
		mi->addTo(mToolBar);
#else
		mToolBar->addAction(mi);
#endif
		mi->setToolTip(iqtHelper::Convert(tooltip));
		mi->Update();
	}
}


void iqtMenuWindowSubject::AddToolBarSeparator()
{
	mToolBar->addSeparator();
}


void iqtMenuWindowSubject::CompleteMenu()
{
#ifdef IQT_3
	this->menuBar()->insertItem(QWhatsThis::whatsThisButton(this->menuBar())); 
#else
	this->menuBar()->addAction(QWhatsThis::createAction(this->menuBar())); 
#endif
}


//
//  Internal implementation
//
void iqtMenuWindowSubject::OnMenuVoid(int id)
{
	mOwner->OnMenu(id,false);
}


void iqtMenuWindowSubject::OnMenuBool(int id, bool v)
{
	mOwner->OnMenu(id,v);
}


//
//  Qt-specific
//
bool iqtMenuWindowSubject::event(QEvent *e)
{
	if(e!=0 && e->spontaneous() && this->IsBlocked()) 
	{
		return false;
	}
	else
	{
		return QMainWindow::event(e);
	}
}


void iqtMenuWindowSubject::closeEvent(QCloseEvent *e)
{
	if(mOwner->CanBeClosed())
	{
		QMainWindow::closeEvent(e);
	}
	else e->ignore();
}

#endif
