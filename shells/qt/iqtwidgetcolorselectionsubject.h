/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Qt-based color selection widget
//
#ifndef IQTWIDGETCOLORSELECTIONSUBJECT_H
#define IQTWIDGETCOLORSELECTIONSUBJECT_H


#include "iqt.h"

#ifdef IQT_3
#include <qwidget.h>
#elif defined(IQT_4)
#include <QtGui/QWidget>
#else
#include <QtWidgets/QWidget>
#endif
#include "ibgwidgetcolorselectionsubject.h"


class QLabel;
class QPushButton;


class iqtWidgetColorSelectionSubject : public QWidget, public ibgWidgetColorSelectionSubject
{

	friend class iggSubjectFactory;

public:

	virtual ~iqtWidgetColorSelectionSubject();

	virtual const iColor GetColor() const;
	virtual void SetColor(const iColor &c);

	virtual void SetText(const iString &text);

protected:

	iqtWidgetColorSelectionSubject(iggWidget *owner);

	void DisplayColor() const;

	QPushButton *mButton;
	mutable QColor mColor;
	const int mSize;
};

#endif  // IQTWIDGETCOLORSELECTIONSUBJECT_H

