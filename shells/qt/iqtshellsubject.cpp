/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_QT)


#include "iqtshellsubject.h"


#include "ierror.h"
#include "istring.h"

#include "iggshell.h"

#include "ibgwindowsubject.h"

#include "iqt.h"
#include "iqthelper.h"
#include "iqtwindowhelper.h"

#include <vtkIOStream.h>

#ifdef IQT_3
#include <qapplication.h>
#include <qmessagebox.h>
#include <qgl.h>
#else
#include <QtOpenGL/QGLFormat>
#ifdef IQT_4
#include <QtGui/QApplication>
#include <QtGui/QDesktopWidget>
#include <QtGui/QMessageBox>
#else
#include <QtWidgets/QApplication>
#include <QtWidgets/QDesktopWidget>
#include <QtWidgets/QMessageBox>
#endif
#ifdef Q_WS_X11
#include <QtGui/QX11Info>
#endif
#endif


using namespace iParameter;


iqtShellSubject::iqtShellSubject(iggShell *owner) : ibgShellSubject(owner)
{
	//
	//  Workaround for that weird Qt bug
	//
#ifdef IQT_PRE5
	static int qtargc = 1;
	static char* qtargv[1];
	qtargv[0] = (char *)"IFrIT";
#else
	static int qtargc = 3;
	static char* qtargv[3];
	qtargv[0] = (char *)"IFrIT";
	qtargv[1] = (char *)"-platformpluginpath";
	qtargv[2] = (char *)".";
#endif

#ifndef IQT_3
	static bool first = true;
	if(!first)
	{
		IBUG_FATAL("Due to a bug in Qt4, IFrIT can only be started once.");
	}
	first = false;
	//QCoreApplicationPrivate::theMainThread = 0;
#endif
	new QApplication(qtargc,qtargv);

	//
	//  Test that Qt is compiled with RTTI 
	//
	QWidget *pp = new QMessageBox();
	if(pp == 0)
	{
		IBUG_FATAL("There is not enough memory to run IFrIT.");
	}
	QMessageBox *ps = dynamic_cast<QMessageBox*>(pp);
	if(ps == 0)
	{
		IBUG_FATAL("Qt has not been compiled with the RTTI (Run-Time Type Information) support.\nIFrIT needs all components to be compiled with the RTTI support.\nPlease re-compile Qt with the RTTI support enabled.");
	}
	delete ps;

	//
	//  Do OpenGL stuff
	//
	QGLFormat f;
    f.setAlpha(true);
#ifdef I_NO_STEREO
	f.setStereo(false);
#else
	f.setStereo(true);
#endif
    QGLFormat::setDefaultFormat( f );
}


iqtShellSubject::~iqtShellSubject()
{
	if(qApp != 0) delete qApp;
}


void iqtShellSubject::Exit()
{
	qApp->quit();
}


void iqtShellSubject::GetDesktopDimensions(int &w, int &h) const
{
	w = qApp->desktop()->width();
	h = qApp->desktop()->height();
}


void iqtShellSubject::ProcessEvents(bool sync)
{
	qApp->flush();
	qApp->sendPostedEvents();
	qApp->processEvents();
}


void iqtShellSubject::CompleteStartUp()
{
	qApp->connect(qApp,SIGNAL(lastWindowClosed()),qApp,SLOT(quit()));
}


void iqtShellSubject::EnterEventLoop()
{
	//
	//  Just run the Qt Application
	//
	qApp->exec();
}


int iqtShellSubject::PopupWindow(ibgWindowSubject *parent, const iString &text, int type, const char *button0, const char *button1, const char *button2) const
{
	QWidget *w = 0;

	if(parent != 0)
	{
		iqtWindowHelper *h = dynamic_cast<iqtWindowHelper*>(parent->GetHelper());
		if(h != 0) w = h->GetWidget();
	}

	switch(type)
	{
	case MessageType::Information:
		{
			return QMessageBox::information(w,"IFrIT",iqtHelper::Convert(text),button0,button1,button2);
		}
	case MessageType::Warning:
		{
			return QMessageBox::warning(w,"IFrIT",iqtHelper::Convert(text),button0,button1,button2);
		}
	case MessageType::Error:
		{
			return QMessageBox::critical(w,"IFrIT",iqtHelper::Convert(text),button0,button1,button2);
		}
	default:
		{
			IBUG_WARN("Incorrect type for pop-up window.");
		}
	}
	return -1;
}

#endif
