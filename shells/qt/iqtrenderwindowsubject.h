/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


//
//  This class is based on original vtkQGLRenderWindow class written by Manish P. Pagey [pagey@drcsdca.com ]
//
#ifndef IQTRENDERWINDOWSUBJECT_H
#define IQTRENDERWINDOWSUBJECT_H


#include "iqt.h"

#ifdef IQT_3
#include <qgl.h>
#else
#include <QtOpenGL/QGLWidget>
#endif
#include "ibgrenderwindowsubject.h"

#include "iarray.h"

class iImage;
class iView;


class iqtRenderWindowSubject : public QGLWidget, public ibgRenderWindowSubject
{

	Q_OBJECT 

	friend class iggSubjectFactory;

public:

	void AttachToParent(QWidget *parent, bool border = true);

protected:

	iqtRenderWindowSubject(iggRenderWindow *owner, QWidget* parent = 0);
	virtual ~iqtRenderWindowSubject();

	//
	//  Shell-specific
	//
	virtual void UpdateActiveStatus(bool last);

	//
	//  RenderWindow methods
	//
	virtual void Initialize(bool show);
	virtual void Finalize();
	virtual void SwapBuffers();

	virtual void SetBorder(bool);
	virtual void SetFullScreen(bool);
	virtual void MakeCurrent();

	virtual void* GetDisplay() const;
	virtual void* GetContext() const;
	virtual void* GetWindowId() const;
	virtual void SetWindowName(const iString &s);

	virtual void RenderIntoMemory(iImage &image);
	virtual bool IsReadyForDumpingImage();

	virtual bool IsDirect() const;
	virtual bool IsCurrent() const;

	virtual void RequestStereo(bool s);

	//
	//  RenderWindowInteractor methods
	//
	virtual int InternalCreateTimer(int timerId, int timerType, unsigned long duration);
	virtual int InternalDestroyTimer(int platformTimerId);

	//
	// Timer used during various mouse events to figure 
	// out mouse movements. 
	//
	iArray<QTimer*> mTimers;

	// ------------------------------------------------------------
	// Methods from QGLWidget class. 
	//
	virtual void initializeGL();
	virtual void paintGL();
	virtual void resizeGL(int w, int h);

	virtual void mouseMoveEvent(QMouseEvent *e);
	virtual void mousePressEvent(QMouseEvent *e);
	virtual void mouseReleaseEvent(QMouseEvent *e);
	virtual void keyPressEvent(QKeyEvent *e);

	//
	// Every focus-in and focus-out event results in a repaint 
	// through the default implementations of focusInEvent and
	// focusOutEvent. This results in a flicker in the vtkQGLRenderWindow
	// ever time the cursor moves in or out of the widget. We can 
	// disable this by re-implementing these methods. 
	//
	virtual void focusInEvent(QFocusEvent *e);
	virtual void focusOutEvent(QFocusEvent *e);

	//
	// If this widget is not the top level widget, it does not 
	// get focus until it receives its first mouse click. By 
	// overloading the enterEvent and leaveEvent methods, we 
	// give keyboard focus to the widget when the mouse enters
	// and remove the focus once the mouse leaves. 
	//
	virtual void enterEvent(QEvent *e);
	virtual void leaveEvent(QEvent *e);

	//
	//  Other useful event functions
	//
	virtual bool event(QEvent *e);
	virtual void moveEvent(QMoveEvent *e);
	virtual void closeEvent(QCloseEvent *e);
	virtual void resizeEvent(QResizeEvent *e);

protected slots:

	void OnTimer();
};

#endif //IQTRENDERWINDOWSUBJECT_H
