/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_QT)


#include "iqtwidgetentrysubject.h"


#include "ierror.h"
#include "imath.h"
#include "istring.h"

#include "iggwidgetrendermodebutton.h"

#include "iqthelper.h"
#include "iqtwidgetbuttonsubject.h"
#include "iqtwidgethelper.h"

#ifdef IQT_3
#include <qevent.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qlineedit.h>
#include <qslider.h>
#include <qvalidator.h>
#else
#include <QtGui/QMouseEvent>
#include <QtGui/QValidator>
#ifdef IQT_4
#include <QtGui/QGridLayout>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QSlider>
#else
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSlider>
#endif
using namespace Qt;
#ifndef IQT_4
#define setNumDigits setDigitCount
#define numDigits digitCount
#endif
#endif


namespace iqtWidgetEntrySubject_Private
{
	class Slider : public QSlider
	{

	public:

		Slider(QWidget *owner) : QSlider(Horizontal,owner)
		{
			this->setMinimumWidth(32);
		}

	protected:

//		void mouseReleaseEvent(QMouseEvent *e)
//		{
//			QSlider::mouseReleaseEvent(e);
//			emit sliderReleased();  This is emitted inside the mouseReleaseEvent
//		}
	};

	class LineEdit : public QLineEdit
	{

	public:

		LineEdit(bool isshort, iqtWidgetEntrySubject *owner) : QLineEdit(owner)
		{
			mIsShort = isshort;
#ifdef IQT_3
			QColor fg = this->paletteForegroundColor();
			QColor bg = this->paletteBackgroundColor();
#else
			QColor fg = this->palette().color(this->foregroundRole());
			QColor bg = this->palette().color(this->backgroundRole());
#endif
			mTemporaryColor = QColor((3*fg.red()+2*bg.red())/5,(3*fg.green()+2*bg.green())/5,(3*fg.blue()+2*bg.blue())/5);
			mPermanentColor = fg;

			mOwner = owner;
		}

		virtual QSize sizeHint() const
		{
			QSize s = QLineEdit::sizeHint();
			//
			//  Default size of QLineEdit is too large, squeeze it horizontally
			//
			if(mIsShort) s.setWidth((2*s.width())/3);
			return s;
		}
	
		virtual QSize minimumSizeHint () const
		{
			QSize s = QLineEdit::minimumSizeHint();
			//
			//  Default minimum size of QLineEdit is too small, expand it horizontally
			//
			s.setWidth((3*s.width())/2);
			return s;
		}

	protected:

		virtual void mouseDoubleClickEvent(QMouseEvent *e)
		{
			e->accept();
			emit returnPressed();
		}

		virtual void keyPressEvent(QKeyEvent *e)
		{
			if(!this->text().isEmpty() && e->key()!=Qt::Key_Up && e->key()!=Qt::Key_Down)
			{
#ifdef IQT_3
				this->setPaletteForegroundColor((e->key()== Qt::Key_Return)?mPermanentColor:mTemporaryColor);
#else
				QPalette p = this->palette();
				p.setColor(this->foregroundRole(),(e->key()== Qt::Key_Return)?mPermanentColor:mTemporaryColor);
				this->setPalette(p);
#endif
			}
			QLineEdit::keyPressEvent(e);
			//
			//  Special keys
			//
			if(e->key() == Qt::Key_Up)
			{
				mOwner->OnKeyUpDown(true);
			}
			if(e->key() == Qt::Key_Down)
			{
				mOwner->OnKeyUpDown(false);
			}
		}

		virtual void focusOutEvent(QFocusEvent *e)
		{
			QLineEdit::focusOutEvent(e);
#ifdef IQT_3
			if(this->paletteForegroundColor() == mTemporaryColor)
#else
			if(this->palette().color(this->foregroundRole()) == mTemporaryColor)
#endif
			{
#ifdef IQT_3
				this->setPaletteForegroundColor(mPermanentColor);
#else
				QPalette p = this->palette();
				p.setColor(this->foregroundRole(),mPermanentColor);
				this->setPalette(p);
#endif
				emit returnPressed();
			}
		}

		bool mIsShort;
		QColor mTemporaryColor, mPermanentColor;
		iqtWidgetEntrySubject *mOwner;
	};
};


iqtWidgetEntrySubject::iqtWidgetEntrySubject(iggWidget *owner, bool slider, int numdig, const iString &label) : QWidget(iqtHelper::Convert(owner->GetParent())), ibgWidgetEntrySubject(owner,slider,numdig,label)
{
	mWidgetHelper = new iqtWidgetHelper(this,owner); IERROR_CHECK_MEMORY(mWidgetHelper);

	//
	//  Create a layout
	//
	if(this->layout() != 0) delete this->layout();
	QGridLayout *w = iqtHelper::NewLayout(this,3);

	QLabel *l = new QLabel(this);
	l->setText(iqtHelper::ConvertWithModifiers(l,label));
	l->setSizePolicy(QSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum));
	w->addWidget(l,0,0);
	if(label.IsEmpty()) l->hide();

	mSlider = new iqtWidgetEntrySubject_Private::Slider(this);
#ifdef IQT_3
	mSlider->setTickmarks(QSlider::Below);
	mSlider->setMinValue(0);
	mSlider->setMaxValue(20);
	mSlider->setLineStep(1);
#else
	mSlider->setTickPosition(QSlider::TicksBelow);
	mSlider->setMinimum(0);
	mSlider->setMaximum(20);
	mSlider->setSingleStep(1);
#endif
	mSlider->setTickInterval(5);
	mSlider->setPageStep(1);
	mSlider->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,QSizePolicy::Minimum));
	w->addWidget(mSlider,0,1);
	if(!this->connect(mSlider,SIGNAL(valueChanged(int)),iDynamicCast<iqtWidgetHelper,ibgWidgetHelper>(INFO,mWidgetHelper),SLOT(OnInt1(int)))) IBUG_WARN("Missed connection.");
	if(!this->connect(mSlider,SIGNAL(sliderReleased()),iDynamicCast<iqtWidgetHelper,ibgWidgetHelper>(INFO,mWidgetHelper),SLOT(OnVoid2()))) IBUG_WARN("Missed connection.");
	if(!slider) mSlider->hide();

	mDisplay = new iqtWidgetEntrySubject_Private::LineEdit(slider,this);
	if(slider || numdig>0) mDisplay->setValidator(new QDoubleValidator(mDisplay));
	if(slider) mDisplay->setSizePolicy(QSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum)); else mDisplay->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,QSizePolicy::Minimum));
	w->addWidget(mDisplay,0,2);
	if(!this->connect(mDisplay,SIGNAL(returnPressed()),iDynamicCast<iqtWidgetHelper,ibgWidgetHelper>(INFO,mWidgetHelper),SLOT(OnVoid1()))) IBUG_WARN("Missed connection.");
	if(slider && numdig<=0) mDisplay->hide();

	mNumButtons = 0;

	this->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,QSizePolicy::Minimum));
}


iqtWidgetEntrySubject::~iqtWidgetEntrySubject()
{
}


void iqtWidgetEntrySubject::AddButton(ibgWidgetButtonSubject* button)
{
	if(button != 0)
	{
		QGridLayout *w = iDynamicCast<QGridLayout,QLayout>(INFO,this->layout());
		iqtWidgetButtonSubject *bs = iDynamicCast<iqtWidgetButtonSubject,ibgWidgetButtonSubject>(INFO,button);
		iqtHelper::SetParent(bs,this);

		mNumButtons++;
#ifdef IQT_3
		w->expand(1,mNumButtons+3);
		w->addWidget(bs,0,mNumButtons+2);
#else
		w->addWidget(bs,0,mNumButtons+3);
#endif
	}
}


int iqtWidgetEntrySubject::GetValue() const
{
	return mSlider->value();
}


void iqtWidgetEntrySubject::GetRange(int &min, int &max) const
{
#ifdef IQT_3
	min = mSlider->minValue();
	max = mSlider->maxValue();
#else
	min = mSlider->minimum();
	max = mSlider->maximum();
#endif
}


void iqtWidgetEntrySubject::SetValue(int v)
{
	if(!mSlider->isHidden())
	{
		mSlider->blockSignals(true);
		mSlider->setValue(v);
		mSlider->blockSignals(false);
	}
}


void iqtWidgetEntrySubject::SetValue(int v, double dv)
{
	this->SetValue(v);

	if(mNumDigits > 0)
	{
		mDisplay->blockSignals(true);
		mDisplay->setText(QString::number(dv,'g',mNumDigits));
		mDisplay->setCursorPosition(0);
		mDisplay->blockSignals(false);
	}
}


void iqtWidgetEntrySubject::SetValue(int v, vtkIdType lv)
{
	this->SetValue(v);
	
	if(mNumDigits > 0)
	{
		mDisplay->blockSignals(true);
		mDisplay->setText(QString::number(lv));
		mDisplay->setCursorPosition(0);
		mDisplay->blockSignals(false);
	}
}


void iqtWidgetEntrySubject::SetRange(int smin, int smax, int pagestep)
{
	int nt = 0;
	if(mSlider->tickInterval() > 0)
	{
#ifdef IQT_3
		nt = (mSlider->maxValue()-mSlider->minValue())/mSlider->tickInterval();
#else
		nt = (mSlider->maximum()-mSlider->minimum())/mSlider->tickInterval();
#endif
	}
	
	mSlider->blockSignals(true);
	mSlider->setRange(smin,smax);
	if(pagestep > 0) mSlider->setPageStep(pagestep);
	mSlider->blockSignals(false);
	
	if(nt > 0) this->SetNumberOfTicks(nt);
}


void iqtWidgetEntrySubject::ShowTail()
{
	mDisplay->end(false);
}


void iqtWidgetEntrySubject::SetStretch(int label, int slider, int number)
{
	QGridLayout *l = iDynamicCast<QGridLayout,QLayout>(INFO,this->layout());
#ifdef IQT_3
	l->setColStretch(0,label);
	l->setColStretch(1,slider);
	l->setColStretch(2,number);
	l->setColStretch(3,number);
#else
	l->setColumnStretch(0,label);
	l->setColumnStretch(1,slider);
	l->setColumnStretch(2,number);
	l->setColumnStretch(3,number);
#endif
}


void iqtWidgetEntrySubject::SetNumberOfTicks(int number)
{
	if(number > 0)
	{
#ifdef IQT_3
		mSlider->setTickmarks(QSlider::Below);
		mSlider->setTickInterval((mSlider->maxValue()-mSlider->minValue())/number);
#else
		mSlider->setTickPosition(QSlider::TicksBelow);
		mSlider->setTickInterval((mSlider->maximum()-mSlider->minimum())/number);
#endif
	}
	else
	{
#ifdef IQT_3
		mSlider->setTickmarks(QSlider::NoMarks);
#else
		mSlider->setTickPosition(QSlider::NoTicks);
#endif
	}
}


void iqtWidgetEntrySubject::SetEditable(bool s)
{
	mDisplay->setReadOnly(!s);
}

//
//  Line edit functions
//
const iString iqtWidgetEntrySubject::GetText() const
{
	return iqtHelper::Convert(mDisplay->text());
}


void iqtWidgetEntrySubject::SetText(const iString &s)
{
	mDisplay->blockSignals(true);
	mDisplay->setText(iqtHelper::Convert(s));
	mDisplay->setCursorPosition(0);
	mDisplay->blockSignals(false);
}


void iqtWidgetEntrySubject::SelectText(int start, int len)
{
	if(start < 0) start = 0;
	if(len < 0) len = mDisplay->text().length() - start;
	mDisplay->setSelection(start,len);
}



void iqtWidgetEntrySubject::OnKeyUpDown(bool up)
{
	iDynamicCast<iqtWidgetHelper,ibgWidgetHelper>(INFO,mWidgetHelper)->OnBool1(up);
}

#endif
