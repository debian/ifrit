/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_QT)


#include "iqtwidgetcolorselectionsubject.h"


#include "icolor.h"
#include "ierror.h"

#include "iggwidget.h"

#include "ibgframesubject.h"

#include "iqthelper.h"
#include "iqtwidgethelper.h"

#ifdef IQT_3
#include <qcolordialog.h>
#include <qframe.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qpushbutton.h>
#else
#ifdef IQT_4
#include <QtGui/QColorDialog>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#else
#include <QtWidgets/QColorDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#endif
#endif


iqtWidgetColorSelectionSubject::iqtWidgetColorSelectionSubject(iggWidget *owner) : QWidget(iqtHelper::Convert(owner->GetParent())), ibgWidgetColorSelectionSubject(owner), mSize(16)
{
	mWidgetHelper = new iqtWidgetHelper(this,owner); IERROR_CHECK_MEMORY(mWidgetHelper);

	//
	//  Create a layout
	//
	if(this->layout() != 0) delete this->layout();
	QGridLayout *l = iqtHelper::NewLayout(this,1);

	mButton = new QPushButton(this);
	mButton->setText("Color");
	l->addWidget(mButton,0,0);

	this->setSizePolicy(QSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum));

	if(!this->connect(mButton,SIGNAL(clicked()),iDynamicCast<iqtWidgetHelper,ibgWidgetHelper>(INFO,mWidgetHelper),SLOT(OnVoid1()))) IBUG_WARN("Missed connection.");
}


iqtWidgetColorSelectionSubject::~iqtWidgetColorSelectionSubject()
{
}


const iColor iqtWidgetColorSelectionSubject::GetColor() const
{
	QColor c = QColorDialog::getColor(mColor); 
	if(c.isValid())
	{
		mColor = c;
		this->DisplayColor();
	}
	return iqtHelper::Convert(mColor);
}


void iqtWidgetColorSelectionSubject::SetColor(const iColor &v)
{
	mColor = iqtHelper::Convert(v);
	this->DisplayColor();
}


#ifdef IQT_3
#include <qdrawutil.h>
#include <qpainter.h>
#else
#include <QtGui/QPainter>
#ifdef IQT_4
#include <QtGui/qdrawutil.h>
#else
#include <QtWidgets/qdrawutil.h>
#endif
#endif


void iqtWidgetColorSelectionSubject::DisplayColor() const
{
	QPixmap p(mSize,mSize);
	p.fill(mColor);
	QPainter w(&p);
#ifdef IQT_3
	qDrawShadePanel(&w,0,0,mSize,mSize,mButton->colorGroup(),true,2,0);
	mButton->setIconSet(QIconSet(p));
#else
	qDrawShadePanel(&w,0,0,mSize,mSize,mButton->palette(),true,2,0);
	mButton->setIcon(QIcon(p));
#endif
}

	
void iqtWidgetColorSelectionSubject::SetText(const iString &text)
{
	mButton->setText(iqtHelper::ConvertWithModifiers(mButton,text));
}

#endif
