/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISHELL_INCLUDED(ISHELL_QT)


#include "iqtmainwindowsubject.h"


#include "ierror.h"
#include "iviewmodule.h"

#include "iggdialog.h"
#include "iggextensionwindow.h"
#include "iggframe.h"
#include "iggframetopparent.h"
#include "iggimagefactory.h"
#include "iggmainwindow.h"
#include "iggrenderwindow.h"
#include "iggshell.h"

#include "ibgwindowhelper.h"

#include "iqtextensionwindowsubject.h"
#include "iqtframesubject.h"
#include "iqthelper.h"
#include "iqtrenderwindowsubject.h"
#include "iqtwidgetprogressbarsubject.h"

#include <vtkRenderWindowCollection.h>

#ifdef IQT_3
#include <qapplication.h>
#include <qcursor.h>
#include <qlayout.h>
#include <qmenubar.h>
#include <qstatusbar.h>
#include <qstylefactory.h>
#include <qtoolbutton.h>
#include <qtooltip.h>
#include <qworkspace.h>
#else
#include <QtGui/QWindowStateChangeEvent>
#ifdef IQT_4
#include <QtGui/QApplication>
#include <QtGui/QLayout>
#include <QtGui/QMenuBar>
#include <QtGui/QStatusBar>
#include <QtGui/QStyleFactory>
#include <QtGui/QToolButton>
#include <QtGui/QMdiArea>
#else
#include <QtWidgets/QApplication>
#include <QtWidgets/QLayout>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QStyleFactory>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QMdiArea>
#endif
#define addWindow	addSubWindow
#define windowList	subWindowList
#define cascade		cascadeSubWindows
#endif

//
//  templates
//
#include "iarray.tlh"


using namespace iParameter;


namespace iqtMainWindowSubject_Private
{
	class Workspace : public QWorkspace
	{

	public:

		Workspace(QWidget *parent) : QWorkspace(parent)
		{
#ifdef IQT_3
			this->setScrollBarsEnabled(true);
#endif
		}

		void AddWindow(iqtRenderWindowSubject *rw)
		{
			rw->AttachToParent(this);
#ifndef IQT_3
			this->addWindow(rw);
#endif
			qApp->sendPostedEvents();  // workspace is updated on childEvent

			rw->setFocusPolicy(QFocus::ClickFocus);
			if(this->windowList().count() == 1)
			{
				rw->showMaximized();
			}
			else
			{
				rw->showNormal();
				this->cascade();
			}
		}

		//
		//  Who was that idiot who set a fixed size hint for QWorkspace?!!!!!!!
		//
		virtual QSize sizeHint () const
		{
			return QWidget::sizeHint();
		}
	};
};


using namespace iqtMainWindowSubject_Private;


//
//  Main class
//
iqtMainWindowSubject::iqtMainWindowSubject(iggMainWindow *owner) : iqtMenuWindowSubject(owner,iggImageFactory::FindIcon("genie1gui.png"),"Ionization Front Interactive Tool"), ibgMainWindowSubject(owner)
{
	int i;

	mTimerId = -1;
	mMainFrame = 0;
	mDockingWorkspace = 0;

	//
	//  Available themes
	//
	QStringList l = QStyleFactory::keys();
	for(i=0; i<(int)l.count(); i++) mMainWindow->AddTheme(iqtHelper::Convert(l[i]));
}


iqtMainWindowSubject::~iqtMainWindowSubject()
{
	if(mTimerId > -1) this->killTimer(mTimerId);
}


void iqtMainWindowSubject::SetTopParentSubjects(iggFrameTopParent *busyIndicatorFrame, iggFrameTopParent *visitedFileListFrame)
{
	busyIndicatorFrame->AttachSubject(new iqtExplicitFrameSubject(this->statusBar(),busyIndicatorFrame,1));
	visitedFileListFrame->AttachSubject(new iqtExplicitFrameSubject(this->statusBar(),visitedFileListFrame,1));
}


void iqtMainWindowSubject::PopulateStatusBar(iggFrame *busyIndicatorFrame, ibgWidgetProgressBarSubject *progressBarSubject, iggFrame *visitedFileListFrame)
{
	iqtWidgetProgressBarSubject *pb = iDynamicCast<iqtWidgetProgressBarSubject,ibgWidgetProgressBarSubject>(INFO,progressBarSubject);
	iqtHelper::SetParent(pb,this->statusBar());

#ifdef IQT_3
	this->statusBar()->addWidget(iqtHelper::Convert(busyIndicatorFrame),0,true);
	this->statusBar()->addWidget(pb,2,true);
    this->statusBar()->addWidget(iqtHelper::Convert(visitedFileListFrame),3,true);
#else
	this->statusBar()->addWidget(iqtHelper::Convert(busyIndicatorFrame),0);
	this->statusBar()->addWidget(pb,2);
    this->statusBar()->addWidget(iqtHelper::Convert(visitedFileListFrame),3);
#endif
}


void iqtMainWindowSubject::ShowToolTips(bool s)
{
#ifdef IQT_3
	QToolTip::setGloballyEnabled(s);
#endif
}


void iqtMainWindowSubject::SetTheme(const iString &name)
{
	qApp->setStyle(iqtHelper::Convert(name));
}


void iqtMainWindowSubject::StartTimer()
{
	mTimerId = this->startTimer(10);
}


//
//  Docking helpers
//
void iqtMainWindowSubject::PlaceWindowsInDockedPositions()
{
	if(mMainFrame != 0)
	{
		IBUG_FATAL("Invalid call to iqtMainWindowSubject::PlaceWindowsInDockedPositions.");
	}

	QSize s = this->size();

	this->hide();

	mMainFrame = this->centralWidget();

	QWidget *base = new QWidget(this);
	QGridLayout *l = iqtHelper::NewLayout(base,2); IERROR_CHECK_MEMORY(l);
#ifdef IQT_3
	l->expand(2,2);
	l->setColStretch(0,10);
#else
	l->setColumnStretch(0,10);
#endif
	l->setRowStretch(0,10);

	l->setMargin(ibgFrameSubject::GetPaddingWidth()*2);
	l->setSpacing(ibgFrameSubject::GetPaddingWidth()*2);

	QFrame *fw = new QFrame(base);
	fw->setLineWidth(3);
	fw->setFrameStyle(QFrame::Panel|QFrame::Sunken);

	QGridLayout *fl = iqtHelper::NewLayout(fw,1);

    Workspace *ws = new Workspace(fw); IERROR_CHECK_MEMORY(ws);
	fl->addWidget(ws,0,0);

	l->addWidget(fw,0,0);

	//
	//  Attach extension
	//
	iqtExtensionWindowSubject *ext = iDynamicCast<iqtExtensionWindowSubject,ibgExtensionWindowSubject>(INFO,mMainWindow->GetExtensionWindow()->GetSubject());
	iqtHelper::SetParent(ext,base,0);
	l->addWidget(ext,1,0);

	//
	//  Attach global frame
	//
	iqtHelper::SetParent(mMainFrame,base);
#ifdef IQT_3
	l->addMultiCellWidget(mMainFrame,0,1,1,1);
#else
	l->addWidget(mMainFrame,0,1,2,1);
#endif

	//
	//  Attach render windows
	//
	FOR_EVERY_RENDER_WINDOW(w)
	{
		ws->AddWindow(iDynamicCast<iqtRenderWindowSubject,ibgRenderWindowSubject>(INFO,w->GetSubject()));
	}

	this->setCentralWidget(base);

	this->move(100,100);
	this->resize(2*s.width(),s.height());
	this->show();

	mDockingWorkspace = ws;
}


void iqtMainWindowSubject::RestoreWindowsFromDockedPositions()
{
	if(mMainFrame == 0)
	{
		IBUG_FATAL("Invalid call to iqtMainWindowSubject::RestoreWindowsFromDockedPositions.");
	}

	this->hide();

	QWidget *base = this->centralWidget();

	//
	//  Detach render windows
	//
	FOR_EVERY_RENDER_WINDOW(w)
	{
		iDynamicCast<iqtRenderWindowSubject,ibgRenderWindowSubject>(INFO,w->GetSubject())->showNormal();
		iDynamicCast<iqtRenderWindowSubject,ibgRenderWindowSubject>(INFO,w->GetSubject())->AttachToParent(0);
#ifdef IQT_3
		iDynamicCast<iqtRenderWindowSubject,ibgRenderWindowSubject>(INFO,w->GetSubject())->setIcon(iqtHelper::ConvertToPixmap(*iggImageFactory::FindIcon("genie1vtk.png")));
#else
		iDynamicCast<iqtRenderWindowSubject,ibgRenderWindowSubject>(INFO,w->GetSubject())->setWindowIcon(iqtHelper::ConvertToPixmap(*iggImageFactory::FindIcon("genie1vtk.png")));
#endif
	}

	//
	//  Detach extension
	//
	iqtExtensionWindowSubject *ext = iDynamicCast<iqtExtensionWindowSubject,ibgExtensionWindowSubject>(INFO,mMainWindow->GetExtensionWindow()->GetSubject());
	iqtHelper::SetParent(ext,0,iqtHelper::GetFlags(WindowType::Extension));

	//
	//  Re-attach main window
	//
	iqtHelper::SetParent(mMainFrame,this);

	this->setCentralWidget(mMainFrame);

#ifdef IQT_3
	delete base;
#endif

	mDockingWorkspace = 0;
	mMainFrame = 0;

	this->layout()->activate();
//	this->show();
}


//
//  Internal implementation
//
void iqtMainWindowSubject::AttachRenderWindow(iggRenderWindow *rw)
{
	if(rw==0 || mDockingWorkspace==0) return;

	iqtRenderWindowSubject *s = iDynamicCast<iqtRenderWindowSubject,ibgRenderWindowSubject>(INFO,rw->GetSubject());
	Workspace *ws = iDynamicCast<Workspace,QWorkspace>(INFO,mDockingWorkspace);

	if(mMainWindow->GetDocked())
	{
		s->GetHelper()->SaveWindowGeometry();
		ws->AddWindow(s);
	}
}


bool iqtMainWindowSubject::event(QEvent *e)
{
	if(e!=0 && e->spontaneous() && this->IsBlocked()) return false;

	if(e != 0)
	{
#ifdef IQT_3
		if(e->type() == QEvent::Accel)
#else
		if(e->type() == QEvent::Shortcut)
#endif
		{
			QKeyEvent *ke = static_cast<QKeyEvent*>(e);
			if(ke->key()==Qt::Key_F1 && 
#ifdef IQT_3
				ke->state()==Qt::ShiftButton
#else
				ke->modifiers()==Qt::SHIFT
#endif
				)
			{
#ifdef IQT_3
				QWidget *w = QApplication::widgetAt(QCursor::pos(),true);
#else
				QWidget *w = QApplication::widgetAt(QCursor::pos());
#endif
				if(w != 0) w->setFocus();
			}
		}
	}
	return QMainWindow::event(e);
}


void iqtMainWindowSubject::moveEvent(QMoveEvent *e)
{
	int pos[2];
	pos[0] = this->x();
	pos[1] = this->y();
	mMainWindow->MoveWindows(pos);
	QMainWindow::moveEvent(e);
}


void iqtMainWindowSubject::closeEvent(QCloseEvent *e)
{
	e->ignore();
	mMainWindow->Exit();
}


void iqtMainWindowSubject::timerEvent(QTimerEvent *e)
{
	this->GetShell()->OnTimerEvent();
}


void iqtMainWindowSubject::resizeEvent(QResizeEvent *e)
{
	QMainWindow::resizeEvent(e);
}

#endif
