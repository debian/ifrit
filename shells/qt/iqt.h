/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  QT version and build (porting helper)
//
#ifndef IQT_H
#define IQT_H

#include <qglobal.h>

#if (QT_VERSION < 0x040000)
#define IQT_3
#endif

#if (QT_VERSION < 0x040700)
#define IQT_PRE47
#endif

#if (QT_VERSION < 0x050000)
#ifndef IQT_3
#define IQT_4
#endif
#define IQT_PRE5
#endif

#ifdef IQT_3

class QButton;
class QIconSet;

typedef QButton		QAbstractButton;
typedef QIconSet	QIcon;

#define QFocus		QWidget

#else

class QAbstractButton;
class QIcon;

#define QFocus				Qt

#if (QT_VERSION < 0x040100)
#define IQT_40
#endif

#ifndef IQT_4
#define WFlags WindowFlags
#endif

#endif

#endif // IQT_H

