/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISCRIPT_INCLUDED(ISCRIPT_PYTHON)
#include <Python.h>
#include <frameobject.h>


#include "ipyscript.h"


#include "ierror.h"
#include "ishell.h"

#include "ipyobject.h"


using namespace iParameter;


namespace ipyPrivate
{
	extern iShell *Root;

	bool IsString(PyObject *obj)
	{
#if (PY_MAJOR_VERSION > 2)
		return (PyUnicode_Check(obj) != 0);
#else
		return (PyString_Check(obj) != 0);
#endif
	}

	//
	//  ScriptIO interface
	//
	struct ScriptIO
	{
		PyObject_HEAD
		iScriptIOHelper *Target;
		bool Error;
	};


	const char* ExtractError(PyObject *value);


	PyObject* ScriptIOWrite(ScriptIO *self, PyObject *obj)
	{
		if(Root == 0)
		{
			return 0;
		}

		if(self == 0)
		{
			IBUG_ERROR("Invalid ifrit.ScriptIO");
			return 0;
		}

		if(self->Target == 0)
		{
			IBUG_ERROR("Invalid ifrit.ScriptIO");
			return 0;
		}

		PyObject *text = 0;
#if (PY_MAJOR_VERSION > 2)
		if(PyArg_ParseTuple(obj,"U:write",&text)==0 || PyUnicode_READY(text)==-1)
#else
		if(PyArg_ParseTuple(obj,"S:write",&text) == 0)
#endif
		{
			IBUG_ERROR("Invalid ifrit.ScriptIO");		
			return 0;
		}

		int ret = 0;
		const char *str = 0;
		if(IsString(text) && PyArg_Parse(text,"s",&str)!=0)
		{
			ret = self->Target->Write(str,self->Error);
		}
		else
		{
			IBUG_ERROR("Python Object sent to stdout is not a string");		
		}

		return Py_BuildValue("i",ret);
	}

	PyMethodDef ScriptIOMethods[] =
	{
		{ "write", (PyCFunction)ScriptIOWrite, METH_VARARGS, "Write a string" },
		{ 0 }  /* Sentinel */
	};


	PyTypeObject ScriptIOType = 
	{
		PyVarObject_HEAD_INIT(NULL, 0)
		"ifrit.ScriptIO",			// tp_name
		sizeof(ScriptIO),			// tp_basicsize
		0,							// tp_itemsize
		0,							// tp_dealloc
		0,							// tp_print
		0,							// tp_getattr
		0,							// tp_setattr
		0,							// tp_reserved
		0,							// tp_repr
		0,							// tp_as_number
		0,							// tp_as_sequence
		0,							// tp_as_mapping
		0,							// tp_hash 
		0,							// tp_call
		0,							// tp_str
		0,							// tp_getattro
		0,							// tp_setattro
		0,							// tp_as_buffer
		Py_TPFLAGS_DEFAULT,			// tp_flags
		"IFrIT Input/Output Helper",	// tp_doc
		0,							// tp_traverse
		0,							// tp_clear
		0,							// tp_richcompare
		0,							// tp_weaklistoffset
		0,							// tp_iter
		0,							// tp_iternext
		ScriptIOMethods,			// tp_methods
	};


	class ScriptExitObserver : public iShellExitObserver
	{
	protected:

		virtual void OnExit(iShell *shell)
		{
			PyGILState_STATE gstate;
			gstate = PyGILState_Ensure();

			/* Perform Python actions here. */
			PyRun_SimpleString("exit()");
			/* evaluate result or handle exception */

			/* Release the thread. No Python API allowed beyond this point. */
			PyGILState_Release(gstate);
		}
	};


	void ScriptInit(PyObject* module);

#if (PY_MAJOR_VERSION > 2)
	PyModuleDef ScriptModule =
	{
		PyModuleDef_HEAD_INIT,
		"ifrit",
		"Module that provides access to IFrIT objects and their properties.",
		-1,
		NULL, NULL, NULL, NULL, NULL
	};

	PyMODINIT_FUNC GlobalInit(void) 
	{
		//
		//  Create the module
		//
		PyObject* module = PyModule_Create(&ScriptModule);
		if(module == 0) return 0;

		ScriptInit(module);
		return module;
	}
#else
#if (PY_MINOR_VERSION < 7)
#error IFrIT needs Python 2.7 or Python 3
#endif

	PyMethodDef ScriptModuleMethods[] = 
	{
		{NULL}  /* Sentinel */
	};
	
	PyMODINIT_FUNC GlobalInit(void) 
	{
		//
		//  Create the module
		//
		PyObject* module = Py_InitModule3("ifrit",ScriptModuleMethods,"Module that provides access to IFrIT objects and their properties.");
		if(module == 0) return;

		ScriptInit(module);
	}
#endif

	void ScriptInit(PyObject* module) 
	{
		PyObject *root = ipyObject::New(Root,module);
		PyModule_AddObject(module,"ifrit",root);

		if(PyType_Ready(&ScriptIOType) != 0)
		{
			IBUG_FATAL("Unable to create ifrit.ScriptIO type.");
		}
	}


	int TraceFunc(PyObject *obj, PyFrameObject *frame, int what, PyObject *arg)
	{
		iScriptObserver *obs = (iScriptObserver*)PyCapsule_GetPointer(obj,0);
		if(obs != 0)
		{
			if(obs->IsTerminated())
			{
				PyErr_SetString(PyExc_KeyboardInterrupt,"Terminated.");
				return -1;
			}

			const char *file = 0;
			PyArg_Parse(frame->f_code->co_filename,"s",&file);

			//
			//  Are we tracing ourselves or something else?
			//
			//if(strcmp(file,"<string>") != 0) return 0;

			int line = PyFrame_GetLineNumber(frame) - 1;

			switch(what)
			{
			case PyTrace_LINE:
				{
					obs->OnBeginLine(line,file);
					return 0;
				}
			case PyTrace_CALL:
			case PyTrace_RETURN:
			case PyTrace_C_CALL:
			case PyTrace_C_RETURN:
				{
					//
					//  Function call tracing may go here
					//
					return 0;
				}
			}
		}

		return 0;
	}


	const char* ExtractError(PyObject *value)
	{
		const char *str;
		
		if(PyUnicode_Check(value)!=0 && PyArg_Parse(value,"s",&str)!=0)
		{
			return str;
		}
		else if(PyTuple_Check(value))
		{
			int i, n = PyTuple_Size(value);
			for(i=0; i<n; i++)
			{
				PyObject *obj = PyTuple_GetItem(value,i);
				str = ExtractError(obj);
				if(str != 0) return str;
			}
			return 0;
		}
		else return 0;
	}
};


using namespace ipyPrivate;


ipyScript::ipyScript(iShell *s) : iScript("Python",s)
{
	mName += " " + iString::FromNumber(PY_MAJOR_VERSION) + "." + iString::FromNumber(PY_MINOR_VERSION) + "." + iString::FromNumber(PY_MICRO_VERSION);

	if(Root != 0)
	{
		IBUG_FATAL("Only one instance of IFrIT can be running simultaneously with Python.")
	}
	Root = s;

#if (PY_MAJOR_VERSION > 2)
	Py_SetProgramName(L"IFrIT");
#else
	Py_SetProgramName("IFrIT");
#endif

	PyImport_AppendInittab("ifrit",&GlobalInit);
	Py_Initialize();

	if(PyRun_SimpleString("from ifrit import ifrit") != 0)
	{
		IBUG_FATAL("Unable to initialize object <ifrit>.");
	}

#ifndef _WIN32
	if(PyRun_SimpleString("import readline") != 0)
	{
		IBUG_FATAL("Unable to import module <readline>");
	}
#endif

	iString str = "def inc(file):\n"
		"    if(file[0] == '+'):\n"
		"        s = '" + s->GetEnvironment(Environment::Base) + "' + file[1:]\n"
		"    else:\n"
		"        s = file\n" ;
#if (PY_MAJOR_VERSION > 2)
	str += 
		"    exec(open(s).read())\n";
#else
	str += 
		"    execfile(s)\n";
#endif
	if(PyRun_SimpleString(str.ToCharPointer()) != 0)
	{
		IBUG_ERROR("Unable to define convenience function inc(...).");
	}

#ifdef I_DEBUG
	if(PyRun_SimpleString("i = ifrit") != 0)
	{
		IBUG_FATAL("Unable to initialize an alias <i> to object <ifrit>.");
	}
#endif
}


ipyScript::~ipyScript()
{
	Py_Finalize();
	Root = 0;
}


bool ipyScript::RunInterpreter()
{
	PyEval_InitThreads();
	Root->SetExitObserver(new ScriptExitObserver);
	PyRun_InteractiveLoop(stdin,"IFrIT");
	Root->SetExitObserver(0);
	return true;
}


bool ipyScript::ExecuteSegment(const iString &text)
{
	PyRun_SimpleString(text.ToCharPointer());

	return (PyErr_Occurred() == 0);
}


void ipyScript::OnInstallObserver()
{
	if(mObserver != 0)
	{
		PyObject *cap = PyCapsule_New(mObserver,0,0);
		PyEval_SetTrace(&TraceFunc,cap);
		mObserver->OnStart();
	}
	else
	{
		PyEval_SetTrace(0,0);
	}
}


void ipyScript::OnInstallIOHelper()
{
	if(mIOHelper != 0)
	{
		ScriptIO *out = (ScriptIO *)_PyObject_New(&ScriptIOType);
		out->Target = mIOHelper;
		out->Error = false;
		PySys_SetObject("stdout",(PyObject*)out); 

		ScriptIO *err = (ScriptIO *)_PyObject_New(&ScriptIOType);
		err->Target = mIOHelper;
		err->Error = true;
		PySys_SetObject("stderr",(PyObject*)err); 
	}
	else
	{
		PySys_SetObject("stdout",PySys_GetObject("__stdout__")); 
		PySys_SetObject("stderr",PySys_GetObject("__stderr__")); 
	}
}


iString ipyScript::GetPrompt(char *name, const char *repr) const
{
	const char *str;
	PyObject *obj = PySys_GetObject(name);
	if(obj == 0)
	{
		obj = PyUnicode_FromFormat("%s",repr); IASSERT(obj);
		if(PySys_SetObject(name,obj) != 0)
		{
			IBUG_FATAL("Unable to set a prompt string");
		}
	}

	if(PyArg_Parse(obj,"s",&str) == 0)
	{
		str = repr;
	}

	return iString(str);
}


iString ipyScript::GetPrompt1() const
{
	return this->GetPrompt("ps1",">>> ");
}


iString ipyScript::GetPrompt2() const
{
	return this->GetPrompt("ps2","... ");
}


const iString& ipyScript::GetScriptFileSuffix() const
{
	static const iString str("py");

	return str;
}


const iString& ipyScript::CreateTemplate() const
{
	static const iString text(
		"#\n"
		"#  Reference to the current window\n"
		"#\n"
		"win = ifrit.Window[ifrit.Window.Current]\n"
		"#\n"
		"#  Reset the Animator to the clean state\n"
		"#\n"
		"win.Animator.Reset()\n"
		"#\n"
		"#  Run the Animator\n"
		"#\n"
		"while(win.Animator.Continue()):\n"
		"	#  Put your code here...\n"
		);

	return text;
}


bool ipyScript::IsTraceable() const
{
	return true;
}

#endif
