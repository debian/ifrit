/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISCRIPT_INCLUDED(ISCRIPT_PYTHON)
#include <Python.h>


#include "ipyobject.h"


#include "idata.h"
#include "ierror.h"
#include "iobjectcollection.h"
#include "iproperty.h"
#include "ishell.h"
#include "iviewobject.h"
#include "iviewsubject.h"

#include <vtkTimerLog.h>

//
//  Templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


//
//  Actual implementation of callback functions functions - separated into a different file as not to clutter the main implementation
//
namespace ipyPrivate
{
	extern iShell *Root;

	bool IsString(PyObject *obj);

	//
	// Type declarations
	//
	struct Object
	{
		PyObject_HEAD
		iObject *Target;
		PyObject *Module;
	};

	extern PyTypeObject PropertyType;


	//
	//  De-allocator (does not need a shell to exist)
	//  **************************
	//
	void ObjectDel(Object *self)
	{
		if(self == 0)
		{
			PyErr_SetString(PyExc_TypeError,"Invalid ifrit.Object");
			return;
		}

#if (PY_MAJOR_VERSION > 2)
		if(self->ob_base.ob_type->tp_members != 0)
#else
		if(self->ob_type->tp_members != 0)
#endif
		{
			int i;
			PyObject **ptr = (PyObject**)((char *)self + sizeof(Object));
			for(i=0; ptr[i]!=0; i++)
			{
				Py_DECREF(ptr[i]);
			}
		}
	}

	//
	//  String representation
	//  **************************
	//
	PyObject* ObjectStr(Object *self)
	{
		if(Root == 0)
		{
			PyErr_SetString(PyExc_RuntimeError,"Dangling reference to a stopped IFrIT object");
			return 0;
		}

		if(self == 0)
		{
			PyErr_SetString(PyExc_TypeError,"Invalid ifrit.Object");
			return 0;
		}

		if(self->Target == 0)
		{
			PyErr_SetString(PyExc_TypeError,"Invalid ifrit.Object");
			return 0;
		}

		iString str;
		self->Target->WriteState(str);

		//
		//  Translate composite types
		//
		static iString ops[] = { "C{", "F{", "V{" };

		int i;
		int io, ic;
		for(i=0; i<3; i++)
		{
			do
			{
				str.FindTokenMatch(ops[i],io,"}",ic,0,false);
				if(io != -1)
				{
					if(ic == -1)
					{
						// What the heck is this?
						break;
					}

					iString w = str.Part(io+2,ic-io-2);
					w.Replace("/",",");
					str.Replace(io,ic-io+1,"("+w+")");
				}
			}
			while(io != -1);
		}

		return Py_BuildValue("s#",str.ToCharPointer(),str.Length());
	}


	//
	//  Get method and its helpers
	//  **************************
	//
	//  Primitive types
	//
	template<iType::TypeId id>
	PyObject* GetHelper1(const char* format, typename iType::Traits<id>::Type val)
	{
		return Py_BuildValue(format,val);
	}

	template<>
	PyObject* GetHelper1<iType::Bool>(const char* , bool val)
	{
		PyObject *v = val ? Py_True : Py_False;
		Py_INCREF(v);
		return v;
	}

	template<>
	PyObject* GetHelper1<iType::String>(const char* , const iString& val)
	{
		return Py_BuildValue("s#",val.ToCharPointer(),val.Length());
	}

	template<>
	PyObject* GetHelper1<iType::DataType>(const char* , const iDataType& val)
	{
		return Py_BuildValue("s#",val.LongName().ToCharPointer(),val.LongName().Length());
	}

	//
	//  Compound types
	//
	template<iType::TypeId id>
	PyObject* GetHelper2(const char* format, typename iType::Traits<id>::Type val)
	{
		int i;
		PyObject *ret = PyTuple_New(iType::Traits<id>::NumFields); IERROR_CHECK_MEMORY(ret);
		for(i=0; i<iType::Traits<id>::NumFields; i++)
		{
			if(PyTuple_SetItem(ret,i,Py_BuildValue(format,val[i])) != 0)
			{
				PyErr_SetString(PyExc_TypeError,"Unable to set the tuple with the property value");
				return 0;
			}
		}
		return ret;
	}

	template<>
	PyObject* GetHelper1<iType::Pair>(const char* format, const iPair& val)
	{
		return GetHelper2<iType::Pair>(format,val);
	}

	template<>
	PyObject* GetHelper1<iType::Color>(const char* format, const iColor& val)
	{
		return GetHelper2<iType::Color>(format,val);
	}

	template<>
	PyObject* GetHelper1<iType::Vector>(const char* format, const iVector3D& val)
	{
		return GetHelper2<iType::Vector>(format,val);
	}

	template<iType::TypeId id>
	PyObject* GetHelper(const char* format, const iPropertyVariable *prop)
	{
		const iPropertyDataVariable<id> *var = iDynamicCast<const iPropertyDataVariable<id> >(INFO,prop);
		if(var->Size()==1 && var->IsFixedSize())
		{
			return GetHelper1<id>(format,var->GetValue(0));
		}
		else
		{
			int i;
			PyObject *ret = PyTuple_New(var->Size()+1);
			for(i=0; i<var->Size(); i++)
			{
				PyTuple_SetItem(ret,i,GetHelper1<id>(format,var->GetValue(i)));
			}
			PyTuple_SetItem(ret,i,(PyObject *)prop->GetKey());
	        Py_SIZE(ret) = var->Size();
	        Py_TYPE(ret) = &PropertyType;
			return ret;
		}
	}

	//
	//  Actual getter
	//
	PyObject* PropertyGet(Object *self, void *closure)
	{
		if(Root == 0)
		{
			PyErr_SetString(PyExc_RuntimeError,"Dangling reference to a stopped IFrIT object");
			return 0;
		}

		if(self == 0)
		{
			PyErr_SetString(PyExc_TypeError,"Invalid ifrit.Object");
			return 0;
		}

		if(self->Target == 0)
		{
			PyErr_SetString(PyExc_TypeError,"Invalid ifrit.Object");
			return 0;
		}

		//
		//  We use the closure argument to pass the index of this property
		//  This needs to be done carefully, as g++ refuses to C-cast a pointer to an int.
		//
		union
		{
			void *cls;
			int idx;
		};
		cls = closure;
		if(idx<0 || idx>=self->Target->Variables().Size())
		{
			PyErr_SetString(PyExc_TypeError,"Invalid property index value for ifrit.Object");
			return 0;
		}

		const iPropertyVariable *prop = self->Target->Variables()[idx];

		switch(prop->GetType())
		{
		case iType::Int:		return GetHelper<iType::Int>("i",prop);
		case iType::Bool:		return GetHelper<iType::Bool>("b",prop);
		case iType::Float:		return GetHelper<iType::Float>("f",prop);
		case iType::Double:		return GetHelper<iType::Double>("d",prop);
		case iType::String:		return GetHelper<iType::String>("s",prop);
		case iType::DataType:	return GetHelper<iType::DataType>("s",prop);
		case iType::Pair:		return GetHelper<iType::Pair>("f",prop);
		case iType::Color:		return GetHelper<iType::Color>("i",prop);
		case iType::Vector:		return GetHelper<iType::Vector>("d",prop);
		default:
			{
				IBUG_FATAL("That should NEVER happen: invalid TypeId "+iString::FromNumber(prop->GetType())+" for property "+prop->LongName());
				return 0;
			}
		}
		
	}


	//
	//  Set method and its helpers
	//  **************************
	//
	//  Conversion helpers
	//
	template<iType::TypeId id>
	bool Convert(const char* format, PyObject* val, typename iType::Traits<id>::ContainerType &out)
	{
		return (PyArg_Parse(val,format,&out) != 0);
	}

	template<>
	bool Convert<iType::Bool>(const char*, PyObject* val, bool &out)
	{
		if(val == Py_True)
		{
			out = true;
			return true;
		}
		else if(val == Py_False)
		{
			out = false;
			return true;
		}
		else
		{
			return false;
		}
	}

	template<>
	bool Convert<iType::String>(const char*, PyObject* val, iString &out)
	{
		const char *q = 0;
		if(PyArg_Parse(val,"s",&q) != 0)
		{
			out = iString(q);
			return true;
		}
		else return false;
	}

	template<>
	bool Convert<iType::DataType>(const char*, PyObject* val, iDataId &out)
	{
		const char *q = 0;
		if(PyArg_Parse(val,"s",&q) != 0)
		{
			const iDataType& type(iDataType::FindTypeByName(q));
			if(type != iDataType::Null())
			{
				out = type.GetId();
				return true;
			}
		}
		
		return false;
	}


	//
	//  Set method and its helpers
	//  **************************
	//
	//
	//  Primitive types
	//
	template<iType::TypeId id>
	bool SetHelper1(const iPropertyVariable *prop, int idx, const char* format, PyObject* val)
	{
		typename iType::Traits<id>::ContainerType q;
		if(Convert<id>(format,val,q))
		{
			return iDynamicCast<const iPropertyDataVariable<id> >(INFO,prop)->_PostValue(idx,q);
		}
		else
		{
			iString err = "Property " + prop->LongName() + " only accepts values of type <" + iType::Traits<id>::TypeName() +"> or a compatible type";
			PyErr_SetString(PyExc_TypeError,err.ToCharPointer());
			return false;
		}
	}

	//
	//  Compound types
	//
	template<iType::TypeId id>
	bool SetHelper2(const iPropertyVariable *prop, int idx, const char* format, PyObject* val)
	{
		if(!PyTuple_Check(val) || PyTuple_Size(val)!=iType::Traits<id>::NumFields)
		{
			iString err = "Property " + prop->LongName() + " only accepts values of type <" + iType::Traits<iType::DataType>::TypeName() +"> or a compatible type\nThis type is represented in Python as a tuple of size " + iString::FromNumber(iType::Traits<id>::NumFields);
			PyErr_SetString(PyExc_TypeError,err.ToCharPointer());
			return false;
		}

		int i;
		PyTupleObject *tuple = (PyTupleObject*)val;
		typename iType::Traits<id>::ContainerType q;
		for(i=0; i<iType::Traits<id>::NumFields; i++)
		{
			if(PyArg_Parse(PyTuple_GetItem(val,i),format,&q[i]) == 0)
			{
				iString err = "Elements of property " + prop->LongName() + " tuple must be of type <" + iType::Traits<iType::Traits<id>::FieldTypeId>::TypeName() +"> or of a compatible type";
				PyErr_SetString(PyExc_TypeError,err.ToCharPointer());
				return false;
			}
		}
		
		return iDynamicCast<const iPropertyDataVariable<id> >(INFO,prop)->_PostValue(idx,q);
	}

	template<>
	bool SetHelper1<iType::Pair>(const iPropertyVariable *prop, int idx, const char* format, PyObject* val)
	{
		return SetHelper2<iType::Pair>(prop,idx,format,val);
	}

	template<>
	bool SetHelper1<iType::Color>(const iPropertyVariable *prop, int idx, const char* format, PyObject* val)
	{
		return SetHelper2<iType::Color>(prop,idx,format,val);
	}

	template<>
	bool SetHelper1<iType::Vector>(const iPropertyVariable *prop, int idx, const char* format, PyObject* val)
	{
		return SetHelper2<iType::Vector>(prop,idx,format,val);
	}

	template<iType::TypeId id>
	bool SetHelper(const iPropertyVariable *prop, const char* format, PyObject* val)
	{
		const iPropertyDataVariable<id> *var = iDynamicCast<const iPropertyDataVariable<id> >(INFO,prop);
		if(var->Size()==1 && var->IsFixedSize())
		{
			return SetHelper1<id>(prop,0,format,val);
		}
		else
		{
			if(var->Size()>1 && (!PyTuple_Check(val) || PyTuple_Size(val)!=var->Size()))
			{
				iString err = "The full assignment of property " + prop->LongName() + " only accepts a tuple of size " + iString::FromNumber(var->Size());
				PyErr_SetString(PyExc_TypeError,err.ToCharPointer());
				return false;
			}

			//
			//  Single component assignment
			//
			if(var->Size()==1 && !PyTuple_Check(val))
			{
				return SetHelper1<id>(prop,0,format,val);
			}
			else
			{
				int i;
				for(i=0; i<var->Size(); i++)
				{
					if(!SetHelper1<id>(prop,i,format,PyTuple_GetItem(val,i))) return false;
				}
				return true;
			}
		}
	}

	//
	//  Actual setter
	//
	int PropertySet(Object *self, PyObject *val, void *closure)
	{
		if(Root == 0)
		{
			PyErr_SetString(PyExc_RuntimeError,"Dangling reference to a stopped IFrIT object");
			return -1;
		}

		if(self == 0)
		{
			PyErr_SetString(PyExc_TypeError,"Invalid ifrit.Object");
			return -1;
		}

		if(self->Target == 0)
		{
			PyErr_SetString(PyExc_TypeError,"Invalid ifrit.Object");
			return -1;
		}

		//
		//  We use the closure argument to pass the index of this property
		//  This needs to be done carefully, as g++ refuses to C-cast a pointer to an int.
		//
		union
		{
			void *cls;
			int idx;
		};
		cls = closure;
		if(idx<0 || idx>=self->Target->Variables().Size())
		{
			PyErr_SetString(PyExc_TypeError,"Invalid property index value for ifrit.Object");
			return -1;
		}

		iPropertyVariable *prop = self->Target->Variables()[idx];

		if(val == 0)
		{
			iString err = "Invalid value for property " + prop->LongName();
			PyErr_SetString(PyExc_TypeError,err.ToCharPointer());
			return -1;
		}

		bool ok; 
		switch(prop->GetType())
		{
		case iType::Int:		{ ok = SetHelper<iType::Int>(prop,"i",val); break; }
		case iType::Bool:		{ ok = SetHelper<iType::Bool>(prop,"b",val); break; }
		case iType::Float:		{ ok = SetHelper<iType::Float>(prop,"f",val); break; }
		case iType::Double:		{ ok = SetHelper<iType::Double>(prop,"d",val); break; }
		case iType::String:		{ ok = SetHelper<iType::String>(prop,"s",val); break; }
		case iType::DataType:	{ ok = SetHelper<iType::DataType>(prop,"s",val); break; }
		case iType::Pair:		{ ok = SetHelper<iType::Pair>(prop,"f",val); break; }
		case iType::Color:		{ ok = SetHelper<iType::Color>(prop,"i",val); break; }
		case iType::Vector:		{ ok = SetHelper<iType::Vector>(prop,"d",val); break; }
		default:
			{
				IBUG_FATAL("That should NEVER happen: invalid TypeId "+iString::FromNumber(prop->GetType())+" for property "+prop->LongName());
				ok = false;
			}
		}

		if(ok)
		{
			return 0;
		}
		else
		{
			if(!PyErr_Occurred())
			{
				iString err = "The assigned value is not a valid value for property " + prop->LongName();
				PyErr_SetString(PyExc_TypeError,err.ToCharPointer());
			}
			return -1;
		}
	}

	//
	//  Individual component setter
	//
	int PropertySetItem(PyObject *self, Py_ssize_t idx, PyObject *val)
	{
		if(Root == 0)
		{
			PyErr_SetString(PyExc_RuntimeError,"Dangling reference to a stopped IFrIT object");
			return -1;
		}

		if(self == 0)
		{
			PyErr_SetString(PyExc_TypeError,"Invalid ifrit.Object");
			return -1;
		}

		PyTupleObject *ret = (PyTupleObject*)self;
#if (PY_MAJOR_VERSION > 2)
		int n = ret->ob_base.ob_size;
#else
		int n = ret->ob_size;
#endif
		iProperty::Key key = (iProperty::Key)ret->ob_item[n];

		if(key == 0)
		{
			PyErr_SetString(PyExc_TypeError,"'tuple' object does not support item assignment");
			return -1;
		}

		const iPropertyVariable *prop = dynamic_cast<const iPropertyVariable *>(iProperty::FindPropertyByKey(key));
		if(self->ob_refcnt>1 || prop==0)  //  only immediate assignment
		{
			key = 0;
			ret->ob_item[n] = (PyObject *)key;
			PyErr_SetString(PyExc_TypeError,"'tuple' object does not support item assignment");
			return -1;
		}

		if(idx<0 || idx>=prop->Size())
		{
			iString err = "index " + iString::FromNumber(idx) + " is out of allowed range 0 to " + iString::FromNumber(prop->Size()-1); 
			PyErr_SetString(PyExc_TypeError,err.ToCharPointer());
			return -1;
		}

		bool ok; 
		switch(prop->GetType())
		{
		case iType::Int:		{ ok = SetHelper1<iType::Int>(prop,idx,"i",val); break; }
		case iType::Bool:		{ ok = SetHelper1<iType::Bool>(prop,idx,"b",val); break; }
		case iType::Float:		{ ok = SetHelper1<iType::Float>(prop,idx,"f",val); break; }
		case iType::Double:		{ ok = SetHelper1<iType::Double>(prop,idx,"d",val); break; }
		case iType::String:		{ ok = SetHelper1<iType::String>(prop,idx,"s",val); break; }
		case iType::DataType:	{ ok = SetHelper1<iType::DataType>(prop,idx,"s",val); break; }
		case iType::Pair:		{ ok = SetHelper1<iType::Pair>(prop,idx,"f",val); break; }
		case iType::Color:		{ ok = SetHelper1<iType::Color>(prop,idx,"i",val); break; }
		case iType::Vector:		{ ok = SetHelper1<iType::Vector>(prop,idx,"d",val); break; }
		default:
			{
				IBUG_FATAL("That should NEVER happen: invalid TypeId "+iString::FromNumber(prop->GetType())+" for property "+prop->LongName());
				ok = false;
			}
		}

		if(ok)
		{
			return 0;
		}
		else
		{
			if(!PyErr_Occurred())
			{
				iString err = "The assigned value is not a valid value for property " + prop->LongName();
				PyErr_SetString(PyExc_TypeError,err.ToCharPointer());
			}
			return -1;
		}
	}


	//
	//  Method function and its helpers
	//  **************************
	//
	//  Various action/query types
	//
	template<iType::TypeId id>
	PyObject* CallHelper0(const char *format, iPropertyFunction *prop)
	{
		return GetHelper1<id>(format,iDynamicCast<const iPropertyQuery<id> >(INFO,prop)->GetQuery());
	}

	template<>
	PyObject* CallHelper0<iType::Bool>(const char *format, iPropertyFunction *prop)
	{
		//
		//  Are we action or query?
		//
		iPropertyAction *fun = dynamic_cast<iPropertyAction*>(prop);
		if(fun != 0)
		{
			return GetHelper1<iType::Bool>(format,fun->_PostCall());
		}
		else
		{
			return GetHelper1<iType::Bool>(format,iDynamicCast<const iPropertyQuery<iType::Bool> >(INFO,prop)->GetQuery());
		}
	}

	template<iType::TypeId id>
	PyObject* CallHelper1(const char *format, PyObject *val, iPropertyFunction *prop)
	{
		typename iType::Traits<id>::ContainerType q;
		if(Convert<id>(format,PyTuple_GetItem(val,0),q))
		{
			return GetHelper1<iType::Bool>("b",iDynamicCast<const iPropertyAction1<id> >(INFO,prop)->_PostCall(q));
		}
		else
		{
			iString err = "Function " + prop->LongName() + " only accepts values of type <" + iType::Traits<id>::TypeName() +"> or a compatible type as its argument";
			PyErr_SetString(PyExc_TypeError,err.ToCharPointer());
			return 0;
		}
	}

	template<>
	PyObject* CallHelper1<iType::DataType>(const char *format, PyObject *val, iPropertyFunction *prop)
	{
		iString s;
		if(Convert<iType::String>(format,PyTuple_GetItem(val,0),s))
		{
			const iDataType &type(iDataType::FindTypeByName(s));
			if(type != iDataType::Null()) return GetHelper1<iType::Bool>("b",iDynamicCast<const iPropertyAction1<iType::DataType> >(INFO,prop)->_PostCall(type));
		}

		iString err = "Function " + prop->LongName() + " only accepts values of type <" + iType::Traits<iType::DataType>::TypeName() +"> or a compatible type as its argument";
		PyErr_SetString(PyExc_TypeError,err.ToCharPointer());
		return 0;
	}

	template<iType::TypeId id1, iType::TypeId id2>
	PyObject* CallHelper22(const char *format, PyObject *val, iPropertyFunction *prop)
	{
		typename iType::Traits<id1>::ContainerType q1;
		if(!Convert<id1>(format+0,PyTuple_GetItem(val,0),q1))
		{
			iString err = "Function " + prop->LongName() + " only accepts values of type <" + iType::Traits<id1>::TypeName() +"> or a compatible type as its first argument";
			PyErr_SetString(PyExc_TypeError,err.ToCharPointer());
			return 0;
		}
		typename iType::Traits<id2>::ContainerType q2;
		if(!Convert<id2>(format+1,PyTuple_GetItem(val,1),q2))
		{
			iString err = "Function " + prop->LongName() + " only accepts values of type <" + iType::Traits<id2>::TypeName() +"> or a compatible type as its second argument";
			PyErr_SetString(PyExc_TypeError,err.ToCharPointer());
			return 0;
		}

		return GetHelper1<iType::Bool>("b",iDynamicCast<const iPropertyAction2<id1,id2> >(INFO,prop)->_PostCall(q1,q2));
	}

	template<iType::TypeId id>
	PyObject* CallHelper2(const char *format, PyObject *val, iPropertyFunction *prop)
	{
		char fmt[3];
		fmt[0] = format[0];
		fmt[2] = 0;
		switch(prop->GetArgumentType(1))
		{
		case iType::Int:		{ fmt[1] = 'i'; return CallHelper22<id,iType::Int>(fmt,val,prop); }
		case iType::Bool:		{ fmt[1] = 'b'; return CallHelper22<id,iType::Bool>(fmt,val,prop); }
		case iType::Float:		{ fmt[1] = 'f'; return CallHelper22<id,iType::Float>(fmt,val,prop); }
		case iType::Double:		{ fmt[1] = 'd'; return CallHelper22<id,iType::Double>(fmt,val,prop); }
		case iType::String:		{ fmt[1] = 's'; return CallHelper22<id,iType::String>(fmt,val,prop); }
		//case iType::DataType:	{ fmt[1] = 's'; return CallHelper22<id,iType::DataType>(fmt,val,prop); }
		default:
			{
				IBUG_FATAL("That should NEVER happen: invalid TypeId "+iString::FromNumber(prop->GetArgumentType(1))+" for property "+prop->LongName());
				return 0;
			}
		}
	}


	//
	//  Template form of function property caller/query
	//
	template<int idx> PyObject* PropertyCall(Object *self, PyObject* args)
	{
		if(Root == 0)
		{
			PyErr_SetString(PyExc_RuntimeError,"Dangling reference to a stopped IFrIT object");
			return 0;
		}

		if(self == 0)
		{
			PyErr_SetString(PyExc_TypeError,"Invalid ifrit.Object");
			return 0;
		}

		if(self->Target == 0)
		{
			PyErr_SetString(PyExc_TypeError,"Invalid ifrit.Object");
			return 0;
		}

#ifdef I_DEBUG
		int ideb = idx;
#endif
		if(idx<0 || idx>=self->Target->Functions().Size())
		{
			PyErr_SetString(PyExc_TypeError,"Invalid property index value for ifrit.Object");
			return 0;
		}

		iPropertyFunction *prop = self->Target->Functions()[idx];

		if(args == 0)
		{
			iString err = "Invalid argument for function " + prop->LongName();
			PyErr_SetString(PyExc_TypeError,err.ToCharPointer());
			return 0;
		}

		if(!PyTuple_Check(args) || PyTuple_Size(args)!=prop->NumArguments())
		{
			iString err = "Function " + prop->LongName() + " requires exactly " + iString::FromNumber(prop->NumArguments()) + " argument(s)";
			PyErr_SetString(PyExc_TypeError,err.ToCharPointer());
			return 0;
		}

		PyObject *ret = 0;
		if(prop->NumArguments() == 0)
		{
			switch(prop->GetReturnType())
			{
			case iType::Int:		{ ret = CallHelper0<iType::Int>("i",prop); break; }
			case iType::Bool:		{ ret = CallHelper0<iType::Bool>("b",prop); break; }
			case iType::Float:		{ ret = CallHelper0<iType::Float>("f",prop); break; }
			case iType::Double:		{ ret = CallHelper0<iType::Double>("d",prop); break; }
			case iType::String:		{ ret = CallHelper0<iType::String>("s",prop); break; }
			case iType::DataType:	{ ret = CallHelper0<iType::DataType>("s",prop); break; }
			//case iType::Pair:		{ ret = CallHelper0<iType::Pair>("f",prop); break; }
			//case iType::Color:	{ ret = CallHelper0<iType::Color>("i",prop); break; }
			//case iType::Vector:	{ ret = CallHelper0<iType::Vector>("d",prop); break; }
			default:
				{
					IBUG_FATAL("That should NEVER happen: invalid TypeId "+iString::FromNumber(prop->GetReturnType())+" for property "+prop->LongName());
				}
			}
		}

		if(prop->NumArguments() == 1)
		{
			switch(prop->GetArgumentType(0))
			{
			case iType::Int:		{ ret = CallHelper1<iType::Int>("i",args,prop); break; }
			case iType::Bool:		{ ret = CallHelper1<iType::Bool>("b",args,prop); break; }
			case iType::Float:		{ ret = CallHelper1<iType::Float>("f",args,prop); break; }
			case iType::Double:		{ ret = CallHelper1<iType::Double>("d",args,prop); break; }
			case iType::String:		{ ret = CallHelper1<iType::String>("s",args,prop); break; }
			case iType::DataType:	{ ret = CallHelper1<iType::DataType>("s",args,prop); break; }
			//case iType::Pair:		{ ret = CallHelper1<iType::Pair>("f",args,prop); break; }
			//case iType::Color:	{ ret = CallHelper1<iType::Color>("i",args,prop); break; }
			//case iType::Vector:	{ ret = CallHelper1<iType::Vector>("d",args,prop); break; }
			default:
				{
					IBUG_FATAL("That should NEVER happen: invalid TypeId "+iString::FromNumber(prop->GetArgumentType(0))+" for property "+prop->LongName());
				}
			}
		}

		if(prop->NumArguments() == 2)
		{
			switch(prop->GetArgumentType(0))
			{
			case iType::Int:		{ ret = CallHelper2<iType::Int>("i",args,prop); break; }
			case iType::Bool:		{ ret = CallHelper2<iType::Bool>("b",args,prop); break; }
			case iType::Float:		{ ret = CallHelper2<iType::Float>("f",args,prop); break; }
			case iType::Double:		{ ret = CallHelper2<iType::Double>("d",args,prop); break; }
			case iType::String:		{ ret = CallHelper2<iType::String>("s",args,prop); break; }
			case iType::DataType:	{ ret = CallHelper2<iType::DataType>("s",args,prop); break; }
			//case iType::Pair:		{ ret = CallHelper1<iType::Pair>("f",args,prop); break; }
			//case iType::Color:	{ ret = CallHelper1<iType::Color>("i",args,prop); break; }
			//case iType::Vector:	{ ret = CallHelper1<iType::Vector>("d",args,prop); break; }
			default:
				{
					IBUG_FATAL("That should NEVER happen: invalid TypeId "+iString::FromNumber(prop->GetArgumentType(0))+" for property "+prop->LongName());
				}
			}
		}

		if(ret == 0) IBUG_FATAL("That should NEVER happen: invalid number of arguments "+iString::FromNumber(prop->NumArguments())+" for property "+prop->LongName());
		return ret;
	}

	//
	//  With functions it is tricky, as Python does not give us any option of passing the property index with
	//  the method function. Hence, we need to use templates.
	//	It may be possible to do better than the explicit switch statement, but I don't know how.
	//
	PyCFunction GetObjectMethod(int i)
	{
		switch(i)
		{
		case 0:
			{
				//
				//  This intermediat assignment is needed for g++ compiler
				//
				PyObject* (*fun)(Object*,PyObject*) = PropertyCall<0>;
				return PyCFunction(fun);
			}
		case 1:
			{
				PyObject* (*fun)(Object*,PyObject*) = PropertyCall<1>;
				return PyCFunction(fun);
			}
		case 2:
			{
				PyObject* (*fun)(Object*,PyObject*) = PropertyCall<2>;
				return PyCFunction(fun);
			}
		case 3:
			{
				PyObject* (*fun)(Object*,PyObject*) = PropertyCall<3>;
				return PyCFunction(fun);
			}
		case 4:
			{
				PyObject* (*fun)(Object*,PyObject*) = PropertyCall<4>;
				return PyCFunction(fun);
			}
		case 5:
			{
				PyObject* (*fun)(Object*,PyObject*) = PropertyCall<5>;
				return PyCFunction(fun);
			}
		case 6:
			{
				PyObject* (*fun)(Object*,PyObject*) = PropertyCall<6>;
				return PyCFunction(fun);
			}
		case 7:
			{
				PyObject* (*fun)(Object*,PyObject*) = PropertyCall<7>;
				return PyCFunction(fun);
			}
		case 8:
			{
				PyObject* (*fun)(Object*,PyObject*) = PropertyCall<8>;
				return PyCFunction(fun);
			}
		case 9:
			{
				PyObject* (*fun)(Object*,PyObject*) = PropertyCall<9>;
				return PyCFunction(fun);
			}
		default:
			{
				IBUG_FATAL("Not enough internal instantanizations for so many property functions.");
				return 0;
			}
		}
	}


	//
	//  Sequence protocol for ObjectCollection(s)
	//  **************************
	//
	Py_ssize_t CollectionSize(Object *self)
	{
		if(Root == 0)
		{
			PyErr_SetString(PyExc_RuntimeError,"Dangling reference to a stopped IFrIT object");
			return 0;
		}

		if(self == 0)
		{
			PyErr_SetString(PyExc_TypeError,"Invalid ifrit.Object");
			return 0;
		}

		if(self->Target == 0)
		{
			PyErr_SetString(PyExc_TypeError,"Invalid ifrit.Object");
			return 0;
		}

		iObjectCollection *coll = dynamic_cast<iObjectCollection*>(self->Target);
		if(coll == 0)
		{
			IBUG_FATAL("Incorrectly implemented iObjectCollection protocol.");
			return 0;
		}

		return coll->Size();
	}

	PyObject* CollectionGetItem(Object *self, Py_ssize_t i)
	{
		if(Root == 0)
		{
			PyErr_SetString(PyExc_RuntimeError,"Dangling reference to a stopped IFrIT object");
			return 0;
		}

		if(self == 0)
		{
			PyErr_SetString(PyExc_TypeError,"Invalid ifrit.Object");
			return 0;
		}

		if(self->Target == 0)
		{
			PyErr_SetString(PyExc_TypeError,"Invalid ifrit.Object");
			return 0;
		}

		if(self->Module == 0)
		{
			PyErr_SetString(PyExc_TypeError,"Invalid ifrit.Object");
			return 0;
		}

		iObjectCollection *coll = dynamic_cast<iObjectCollection*>(self->Target);
		if(coll == 0)
		{
			IBUG_FATAL("Incorrectly implemented iObjectCollection protocol.");
			return 0;
		}

		if(i<0 || i>=coll->Size())
		{
			PyErr_SetString(PyExc_TypeError,"Invalid index value for ifrit.Object member");
			return 0;
		}

		//
		// TODO: consider implementing internal caching
		//
		return ipyObject::New(coll->GetMember(i),self->Module);
	}

	
	//
	//  Mapping protocol for ViewObject(s)
	//  **************************
	//
	Py_ssize_t ViewObjectSize(Object *self)
	{
		if(Root == 0)
		{
			PyErr_SetString(PyExc_RuntimeError,"Dangling reference to a stopped IFrIT object");
			return 0;
		}

		if(self == 0)
		{
			PyErr_SetString(PyExc_TypeError,"Invalid ifrit.Object");
			return 0;
		}

		if(self->Target == 0)
		{
			PyErr_SetString(PyExc_TypeError,"Invalid ifrit.Object");
			return 0;
		}

		iViewObject *vobj = dynamic_cast<iViewObject*>(self->Target);
		if(vobj == 0)
		{
			IBUG_FATAL("Incorrectly implemented iViewObject protocol.");
			return 0;
		}

		return vobj->GetNumberOfSubjects();
	}

	PyObject* ViewObjectGetItem(Object *self, PyObject *key)
	{
		if(Root == 0)
		{
			PyErr_SetString(PyExc_RuntimeError,"Dangling reference to a stopped IFrIT object");
			return 0;
		}

		if(self == 0)
		{
			PyErr_SetString(PyExc_TypeError,"Invalid ifrit.Object");
			return 0;
		}

		if(self->Target == 0)
		{
			PyErr_SetString(PyExc_TypeError,"Invalid ifrit.Object");
			return 0;
		}

		if(self->Module == 0)
		{
			PyErr_SetString(PyExc_TypeError,"Invalid ifrit.Object");
			return 0;
		}

		iViewObject *vobj = dynamic_cast<iViewObject*>(self->Target);
		if(vobj == 0)
		{
			IBUG_FATAL("Incorrectly implemented iViewObject protocol.");
			return 0;
		}

		//
		//  If not zero, then it should be a name of a valid data type
		//
		const char *str = 0;
		if(!IsString(key) || PyArg_Parse(key,"s",&str)==0)
		{
			PyErr_SetString(PyExc_TypeError,"A key must be a string with the valid data type name");
			return 0;
		}

		const iDataType& type(iDataType::FindTypeByName(str));
		if(type.IsNull())
		{
			iString err = "String <" + iString(str) + "> is not a name of a valid data type";
			PyErr_SetString(PyExc_TypeError,err.ToCharPointer());
			return 0;
		}

		//
		// TODO: consider implementing internal caching
		//
		return ipyObject::New(vobj->GetSubject(type),self->Module);
	}
};

#endif
