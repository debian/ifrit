
#include "iconfigure.h"
#if ISCRIPT_INCLUDED(ISCRIPT_PYTHON)
#include <Python.h>


#include "ierror.h"
#include "irunner.h"
#include "ishell.h"
#include "istring.h"

#include "ipyobject.h"


namespace ipyPrivate
{
	extern iShell *Root;
	extern PyCFunction StopPtr;

	//
	// Generic Object interface
	//
	struct Object
	{
		PyObject_HEAD
		iObject *Target;
		PyObject *Module;
	};


	iRunner *Runner = 0;

	class ImportExitObserver : public iShellExitObserver
	{
	protected:

		virtual void OnExit(iShell *shell)
		{
			Py_AddPendingCall(&ImportExitObserver::Kill,0);
		}

		static int Kill(void *)
		{
			if(Root != 0)
			{
				Runner->Stop();
				Root = 0;

				if(PyRun_SimpleString("import ifrit") != 0)
				{
					PyErr_SetString(PyExc_RuntimeError,"Unable to import module <ifrit>");
				}
				if(PyRun_SimpleString("del(ifrit.ifrit)") != 0)
				{
					PyErr_SetString(PyExc_RuntimeError,"Unable to delete object <ifrit>");
				}
			}
			return 0;
		}
	};


	//
	//  Global stop function - stop ifrit, delete python objects
	//  **************************
	//
	PyObject* Stop(Object *self, PyObject*)
	{
		if(Root == 0)
		{
			PyErr_SetString(PyExc_RuntimeError,"Dangling reference to a stopped IFrIT object");
			return 0;
		}

		if(self == 0)
		{
			PyErr_SetString(PyExc_TypeError,"Invalid ifrit.Object");
			return 0;
		}

		if(self->Target == 0)
		{
			PyErr_SetString(PyExc_TypeError,"Invalid ifrit.Object");
			return 0;
		}

		if(self->Target != Root)
		{
			PyErr_SetString(PyExc_TypeError,"Invalid ifrit.Root object");
			return 0;
		}

		if(Root != 0)
		{
			Root->SetExitObserver(0);
			Runner->Stop();
			Root = 0;

			if(PyRun_SimpleString("import ifrit") != 0)
			{
				PyErr_SetString(PyExc_RuntimeError,"Unable to import module <ifrit>");
			}
			if(PyRun_SimpleString("del(ifrit.ifrit)") != 0)
			{
				PyErr_SetString(PyExc_RuntimeError,"Unable to delete object <ifrit>");
			}
		}

		return Py_None;
	}


	PyObject* Start(PyObject *module, PyObject *args)
	{
		static const char* list[] = 
		{
#if ISHELL_INCLUDED(ISHELL_QT)
			"qt",
#endif
#if ISHELL_INCLUDED(ISHELL_CL)
			"cl",
#endif
			0 
		};
		const char* type = list[0];

		if(PyTuple_Check(args)==0 || PyTuple_Size(args)>1)
		{
			PyErr_SetString(PyExc_TypeError,"ifrit.start([type]) function needs a 2-letter shell type as an optional argument");
			return 0;
		}

		if(PyTuple_Size(args) == 1)
		{
			if(PyArg_ParseTuple(args,"s",&type)== 0)
			{
				PyErr_SetString(PyExc_TypeError,"Function argument is not a string");
				return 0;
			}
		}

		char str[100];  // don't use iString yet, we are not yet catching exceptions
		strcpy(str,type);
		strcat(str,"/exten");
		type = str;

		if(Runner == 0)
		{
			Runner = new iRunner; if(Runner == 0) return 0;
			PyEval_InitThreads();
		}
		if(!Runner->Launch(1,&type) || Runner->GetShell()==0)
		{
			PyErr_SetString(PyExc_TypeError,"ifrit.start(...) is unable to start a new shell");
			return 0;
		}

		StopPtr = (PyCFunction)&Stop;

		Root = Runner->GetShell();
		Root->SetExitObserver(new ImportExitObserver);

		PyObject *root = ipyObject::New(Root,module);
		PyModule_AddObject(module,"ifrit",root);

		if(PyRun_SimpleString("from ifrit import ifrit") != 0)
		{
			PyErr_SetString(PyExc_TypeError,"Unable to import object <ifrit> from module <ifrit>");
			return 0;
		}

		return Py_None;
	}

	void Free(void *ptr)
	{
		if(Root != 0)
		{
			Runner->Stop();
			Root = 0;
		}
	}
};

#endif
