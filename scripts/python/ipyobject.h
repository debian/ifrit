/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IPYOBJECT_H
#define IPYOBJECT_H


#include <Python.h>

#include "iarray.h"
#include "istring.h"


class iObject;
class iShell;


class ipyTypeObject
{

public:

	static ipyTypeObject* GetTypeObject(iObject *target, PyObject *module);

	inline PyTypeObject* PythonType() const { return mType; }

private:

	ipyTypeObject(iObject *target, PyObject *module, const iString &name);
	~ipyTypeObject();

	char* UseName(const iString& name);
	char* UseString(const iString& str);

	iString mName, mPythonName;
	PyTypeObject *mType;
	PyGetSetDef *mVariables;
	PyMethodDef *mFunctions; 
	PyMemberDef *mChildren;

	iLookupArray<iString> mNameCache;
	iLookupArray<iString> mStringCache;

	static iArray<ipyTypeObject*> mRegisteredTypes;
};


class ipyObject
{

public:

	static PyObject* New(iObject *target, PyObject *module);
};

#endif // IPYOBJECT_H
