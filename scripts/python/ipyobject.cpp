/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#if ISCRIPT_INCLUDED(ISCRIPT_PYTHON)
#include <Python.h>
#include "structmember.h"


#include "ipyobject.h"


#include "idata.h"
#include "idatasubject.h"
#include "ierror.h"
#include "ihelpfactory.h"
#include "iobjectcollection.h"
#include "irunner.h"
#include "ishell.h"
#include "iviewobject.h"
#include "iviewsubject.h"

//
//  Templates
//
#include "iarray.tlh"


iArray<ipyTypeObject*> ipyTypeObject::mRegisteredTypes;


namespace ipyPrivate
{
	//
	//  Global data
	//
	iShell *Root = 0;
	PyCFunction StopPtr = 0;		// stop function - stop ifrit, delete python objects

	//
	// Generic Object interface
	//
	struct Object
	{
		PyObject_HEAD
		iObject *Target;
		PyObject *Module;
	};

	PyTypeObject ObjectType = 
	{
		PyVarObject_HEAD_INIT(NULL, 0)
		"ifrit.Object",				// tp_name
		sizeof(Object),				// tp_basicsize
		0,							// tp_itemsize
		0,							// tp_dealloc
		0,							// tp_print
		0,							// tp_getattr
		0,							// tp_setattr
		0,							// tp_reserved
		0,							// tp_repr
		0,							// tp_as_number
		0,							// tp_as_sequence
		0,							// tp_as_mapping
		0,							// tp_hash 
		0,							// tp_call
		0,							// tp_str
		0,							// tp_getattro
		0,							// tp_setattro
		0,							// tp_as_buffer
		Py_TPFLAGS_DEFAULT,			// tp_flags
		"IFrIT object",				// tp_doc
	};

	void ObjectDel(Object *self);	// de-allocator
	PyObject* ObjectStr(Object *self);

	PyObject* PropertyGet(Object *self, void *closure);
	int PropertySet(Object *self, PyObject *value, void *closure);

	PyCFunction GetObjectMethod(int i); // return the i-th method

	//
	//  Special type to support indexed assignment of properties
	//
	PyTypeObject PropertyType = PyTuple_Type;
	PySequenceMethods PropertyTypeSQ = *PropertyType.tp_as_sequence;

	int PropertySetItem(PyObject *self, Py_ssize_t i, PyObject *v);

	//
	//  Sequence protocol for ObjectCollection(s)
	//
	PySequenceMethods CollectionSQ = { 0 };
	Py_ssize_t CollectionSize(Object *self);
	PyObject* CollectionGetItem(Object *self, Py_ssize_t i);

	//
	//  Mapping protocol for ViewObject(s)
	//
	PyMappingMethods ViewObjectMP = { 0 };
	Py_ssize_t ViewObjectSize(Object *self);
	PyObject* ViewObjectGetItem(Object *self, PyObject *key);
};


using namespace ipyPrivate;


ipyTypeObject* ipyTypeObject::GetTypeObject(iObject *target, PyObject *module)
{
	IASSERT(target);
	IASSERT(module);

	if(mRegisteredTypes.Size() == 0)
	{
		//
		//  Special type to support indexed assignment of properties
		//
		PropertyType.tp_name = "ifrit.Property";
		PropertyType.tp_doc = "IFrIT object property";
		PropertyType.tp_basicsize = sizeof(PyTuple_Type) + sizeof(void*);
		PropertyType.tp_flags = Py_TPFLAGS_DEFAULT;
		PropertyType.tp_as_sequence = &PropertyTypeSQ;
		PropertyTypeSQ.sq_ass_item = PropertySetItem;

		if(PyType_Ready(&PropertyType) != 0)
		{
			IBUG_FATAL("Unable to create ifrit.Property type.");
		}

		Py_INCREF(&PropertyType);
		PyModule_AddObject(module,"ifrit.Property",(PyObject*)&PropertyType);

		//
		//  Sequence protocol for ObjectCollection(s)
		//
		CollectionSQ.sq_length = (lenfunc)&CollectionSize;
		CollectionSQ.sq_item = (ssizeargfunc)&CollectionGetItem;

		//
		//  Mapping protocol for ViewObject(s)
		//
		ViewObjectMP.mp_length = (lenfunc)&ViewObjectSize;
		ViewObjectMP.mp_subscript = (binaryfunc)&ViewObjectGetItem;
	}

	//
	//  For Python we need to distinguish between collections and their components
	//  (in IFrIT they are named identically)
	//
	iString name = target->LongName();
	if(dynamic_cast<iObjectCollection*>(target) != 0) name += "Collection";

	//
	//  DataSujects are special, they are prepended with "Data."
	//
	if(dynamic_cast<iDataSubject*>(target) != 0) name = "Data." + name;

	//
	//  ViewSubjects are special, since they depend on the data type
	//
	iViewSubject *sub = dynamic_cast<iViewSubject*>(target);
	if(sub != 0) name += "For" + sub->GetDataType().LongName();

	int i;
	for(i=0; i<mRegisteredTypes.Size(); i++)
	{
		if(name == mRegisteredTypes[i]->mName) return mRegisteredTypes[i];
	}

	//
	//  We need to create a new type
	//
	ipyTypeObject *obj = new ipyTypeObject(target,module,name); IERROR_CHECK_MEMORY(obj);
	mRegisteredTypes.Add(obj);

	return obj;
}


ipyTypeObject::ipyTypeObject(iObject *target, PyObject *module, const iString &name)
{
	static PyMethodDef fend = { NULL };
	static PyGetSetDef vend = { NULL };
	static PyMemberDef cend = { NULL };
	int i, j;

	mName = name;
	mPythonName = "ifrit." + mName;

	//
	//  We need to create a new type
	//
	mType = new PyTypeObject; IERROR_CHECK_MEMORY(mType);
	*mType = ObjectType;
	mType->tp_name = mPythonName.ToCharPointer();
	mType->tp_str = (reprfunc)ObjectStr;
	mType->tp_dealloc = (destructor)ObjectDel;

	iString s = "or." + target->HelpTag();
	const iHelpDataBuffer &buf = iHelpFactory::FindData(s.ToCharPointer(),iHelpFactory::_ObjectReference);
	mType->tp_doc = UseString(buf.GetText());

	//
	// Variables as getset
	//
	mVariables = new PyGetSetDef[2*target->Variables().Size()+1]; IERROR_CHECK_MEMORY(mVariables);

	for(i=0; i<target->Variables().Size(); i++)
	{
		j = i + target->Variables().Size();

		mVariables[i].name = UseName(target->Variables()[i]->LongName());
		mVariables[j].name = UseName(target->Variables()[i]->ShortName());

		mVariables[i].get = mVariables[j].get = (getter)PropertyGet;
		mVariables[i].set = mVariables[j].set = (setter)PropertySet;

		iString s = "or." + target->Variables()[i]->HelpTag();
		const iHelpDataBuffer &buf = iHelpFactory::FindData(s.ToCharPointer(),iHelpFactory::_ObjectReference);
		mVariables[i].doc = UseString(buf.GetText());
		mVariables[j].doc = UseString(iString("Alias for ")+mVariables[i].name);

		//
		//  We use the closure argument to pass the index of this property
		//  This needs to be done carefully, as g++ refuses to C-cast a pointer to an int.
		//
		union
		{
			void *cls;
			int idx;
		};
		idx = i;
		mVariables[i].closure = mVariables[j].closure = cls;
	}

	mVariables[2*target->Variables().Size()] = vend;
	mType->tp_getset = mVariables;

	//
	//  Functions as methods
	//
	bool withstop = (target==Root && Root->IsThreaded() && StopPtr!=0);
	int nf = 2*target->Functions().Size() + 1;
	if(withstop) nf++;
	mFunctions = new PyMethodDef[nf]; IERROR_CHECK_MEMORY(mFunctions);

	for(i=0; i<target->Functions().Size(); i++)
	{
		j = i + target->Functions().Size();

		mFunctions[i].ml_name = UseName(target->Functions()[i]->LongName());
		mFunctions[j].ml_name = UseName(target->Functions()[i]->ShortName());

		mFunctions[i].ml_meth = mFunctions[j].ml_meth = GetObjectMethod(i);
		mFunctions[i].ml_flags = mFunctions[j].ml_flags = METH_VARARGS;

		iString s = "or." + target->Functions()[i]->HelpTag();
		const iHelpDataBuffer &buf = iHelpFactory::FindData(s.ToCharPointer(),iHelpFactory::_ObjectReference);

		mFunctions[i].ml_doc = UseString(buf.GetText());
		mFunctions[j].ml_doc = UseString(iString("Alias for ")+mFunctions[i].ml_name);
	}

	if(withstop)
	{
		i = 2*target->Functions().Size();

		mFunctions[i].ml_name = "stop";;
		mFunctions[i].ml_meth = StopPtr;
		mFunctions[i].ml_flags = METH_NOARGS;
		mFunctions[i].ml_doc = "Stop IFrIT and delete Python objects";
	}

	mFunctions[nf-1] = fend;
	mType->tp_methods = mFunctions;

	if(target->Children().Size() > 0)
	{
		//
		//  Implement separate protocols for ObjectCollection(s), ViewObject(s), and all other objects
		//
		iObjectCollection *coll = dynamic_cast<iObjectCollection*>(target);
		iViewObject *obj = dynamic_cast<iViewObject*>(target);
		if(coll != 0)
		{
			mType->tp_as_sequence = &CollectionSQ;
		}
		else if(obj != 0)
		{
			mType->tp_as_mapping = &ViewObjectMP;
		}
		else
		{
			//
			//  Children as members - for all other objects
			//
			mChildren = new PyMemberDef[2*target->Children().Size()+1]; IERROR_CHECK_MEMORY(mChildren);

			int nc = 0;
			for(i=0; i<target->Children().Size(); i++) if(!target->Children()[i]->IsHidden())
			{
				mChildren[2*nc+0].name = UseName(target->Children()[i]->LongName());
				mChildren[2*nc+1].name = UseName(target->Children()[i]->ShortName());

				mChildren[2*nc+0].type = mChildren[2*nc+1].type = T_OBJECT_EX;
				mChildren[2*nc+0].offset = mChildren[2*nc+1].offset = sizeof(Object) + sizeof(PyObject*)*nc;
				mChildren[2*nc+0].flags = mChildren[2*nc+1].flags = READONLY;

				iString s = "or." + target->Children()[i]->HelpTag();
				const iHelpDataBuffer &buf = iHelpFactory::FindData(s.ToCharPointer(),iHelpFactory::_ObjectReference);

				mChildren[2*nc+0].doc = UseString(buf.GetText());
				mChildren[2*nc+1].doc = UseString(iString("Alias for ")+mChildren[i].name);

				nc++;
			}

			mType->tp_basicsize = sizeof(Object) + sizeof(PyObject*)*(nc+1);

			for(i=2*nc; i<=2*target->Children().Size(); i++) mChildren[i] = cend; // don't leave holes
			mType->tp_members = mChildren;
		}
	}

	if(PyType_Ready(mType) != 0)
	{
		IBUG_FATAL("Unable to create a new Python type "+mPythonName);
	}

	Py_INCREF(mType);
	PyModule_AddObject(module,mName.ToCharPointer(),(PyObject*)mType);
}


char* ipyTypeObject::UseName(const iString& name)
{
	if(!mNameCache.AddUnique(name))
	{
		IBUG_FATAL("Non-unique name");
	}
	return (char*)mNameCache.Last().ToCharPointer();
}


char* ipyTypeObject::UseString(const iString& str)
{
	mStringCache.Add(str);
	return (char*)mStringCache.Last().ToCharPointer();
}


PyObject* ipyObject::New(iObject *target, PyObject *module)
{
	IASSERT(target);
	IASSERT(module);

	ipyTypeObject *type = ipyTypeObject::GetTypeObject(target,module);

	Object *ret = (Object *)_PyObject_New(type->PythonType());
	ret->Target = target;
	ret->Module = module;

	//
	//  Assign static children
	//
	if(type->PythonType()->tp_members != 0)
	{
		int i, nc = 0;
		PyObject **ptr = (PyObject**)((char *)ret + sizeof(Object));
		for(i=0; i<target->Children().Size(); i++) if(!target->Children()[i]->IsHidden())
		{
			ptr[nc++] = ipyObject::New(target->Children()[i],module);
		}
		ptr[nc] = 0;  // sentinel
	}

	return (PyObject*)ret;
}

#endif
