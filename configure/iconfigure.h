/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Main configuration parameters and macros for working with them
//
#ifndef ICONFIGURE_H
#define ICONFIGURE_H

//
//  Supported extensions
//
#define IEXTENSION_NONE		0
#define IEXTENSION_MV		0x80000000

#include "iextensiondefines.h"

//
//  Supported shells
//
#define ISHELL_OS			1
#define ISHELL_CL			2
#define ISHELL_GG			0xFFFFFFFC
#define ISHELL_QT			4

//
//  Supported scripts
//
#define ISCRIPT_PYTHON		1

//
//  Multiple extensions can be included, they are not exclusive
//
#define IEXTENSION_INCLUDED(_Type_)  ((IEXTENSION & _Type_) != 0)

//
//  Multiple shells can be included, they are not exclusive
//
#define ISHELL_INCLUDED(_Type_)  ((ISHELL & _Type_) != 0)

//
//  Multiple scripts can be included, they are not exclusive
//
#define ISCRIPT_INCLUDED(_Type_)  ((ISCRIPT & _Type_) != 0)

//
//  ******************************************************************************
//
//  Set the parameters that define shell and extension.
//
#ifdef I_EXTERNAL_CONFIGURATION
#include "iconfiguresettings.ext.h"
#else
#define IEXTENSION	0xFFFFFFFF
#define ISHELL		0xFFFFFFFF 
#define ISCRIPT		1
#endif
//
//  ******************************************************************************
//

#if !defined(IEXTENSION) 
#error Misconfiguration: extension (IEXTENSION) is not set.
!TERMINATE!
#endif

#if !defined(ISHELL) || (ISHELL == 0)
#error Misconfiguration: shell (ISHELL) is not set.
!TERMINATE!
#endif

#if !defined(ISCRIPT)
#error Misconfiguration: script (ISCRIPT) is not set.
!TERMINATE!
#endif

#endif // ICONFIGURE_H
