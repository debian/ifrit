/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#include "iversion.h"


//
//  IFrIT version
//
#define IVERSION			"4.1.2"
#define IREVISION			"581"

//
//  History
//
//  060608  Base-3.0.0b1  ART-2.0.0b1  MV-1.0.0b1  -- First beta release
//  060609  Base-3.0.0b2  ART-2.0.0b2  MV-1.0.0b2  -- Added special locations in CrossSectionViewSubject
//  060622  Base-3.0.0b3  ART-2.0.0b3  MV-1.0.0b3  -- Added coordinate system switch for ART extension
//  060701  Base-3.0.0b4  ART-2.0.0b4  MV-1.0.0b4  -- Added abstract/generic filters for porting, cleaned up class names
//  060707  Base-3.0.0b5  ART-2.0.0b5  MV-1.0.0b5  -- Completed Script Debugger
//  060721  Base-3.0.0b6  ART-2.0.0b6  MV-1.0.0b6  -- Completed port to Qt4
//  060728  Base-3.0.0b7  ART-2.0.0b7  MV-1.0.0b7  -- Internal changes, docking fixed (?)
//  060815  Base-3.0.0b8  ART-2.0.0b8  MV-1.0.0b8  -- New icons, docking now really fixed, x-section in texture mode on multiple processors is fixed
//                                                     Includes 3D texture model for volume rendering and direct MPEG/AVI movie creation
//  060817  Base-3.0.0    ART-2.0.0    MV-1.0.0    -- First release: basic things seem to work now.
//  060830  Base-3.0.1    ART-2.0.1    MV-1.0.0    -- Some bug fixes
//  060926  Base-3.0.2    ART-2.0.2    MV-1.0.0    -- Refactored for simplified extensions (ViewSubjects etc)
//  060930  Base-3.0.3    ART-2.0.3    MV-1.0.0    -- Need to re-release, created a branch in CVS too early by mistake
//  061003  Base-3.0.4    ART-2.0.4    MV-1.0.0    -- Added special brightness palette, fixed iArray out-of-boundary bug, fixed DataExplorer bug
//  061010  Base-3.0.5    ART-2.0.5    MV-1.0.0    -- fixed Interactor bug, ART isosurface bugs(?), more iArray out-of-boundary bugs
//  061119  Base-3.0.6    ART-2.0.6    MV-1.0.0    -- fixed DataLimits+clones bug, RulerDialog bug, parallel stitch bug, added window list idalog, x-section placement enhancements
//  061120  Base-3.0.7    ART-2.0.7    MV-1.0.0    -- work around vtkMarchingContourFilter multi-threaded bug.
//  061221  Base-3.0.8    ART-2.0.8    MV-1.0.1    -- internal changes, pseudo-color composing in ImageComposer, minor GUI modifications, bug fixes.
//  070203  Base-3.0.9    ART-2.0.9    MV-1.0.1    -- internal changes to particle masking, new masking modes, bug fixes
//
//  070325  Base-3.1.0    ART-2.1.0    MV-1.0.2    VTK-1.0.0b0     GADGET-1.0.0b0 
//          split DataSubject into DataSubject proper and DataFileLoader
//          added VTK & GADGET extensions
//          fixed MultiView full-screen mode bugs
//
//  070422  Base-3.1.1    ART-2.1.2    MV-1.0.2    VTK-1.0.0b0     GADGET-1.0.0b1
//          fixed marker Create/Delete panel bug
//          other stylistic bug fixes
//          redone volume rendering interface
//          ART 2D & 3D texture volume rendering by uniform resampling
//
//  070502  Base-3.1.2    ART-2.1.3    MV-1.0.3    VTK-1.0.0b0     GADGET-1.0.0b2
//			C-style casting replaced whereever possible
//			ViewModule::LabelOffset property added
//			Fixed bugs in iDataExplorer
//			Fixed bugs in recognizing animatable filename without a suffix
//
//  070508  Base-3.1.3    ART-2.1.3    MV-1.0.3    VTK-1.0.0b0     GADGET-1.0.0b2
//			Fixed bugs with saving markers and animation not stopping on error
//			New markers are placed in the camera focal point
//
//  070527  Base-3.1.4    ART-2.1.4    MV-1.0.4    VTK-1.0.0b1     GADGET-1.0.0b2
//			Added external image as a background
//			Fixed batch mode and channeling output
//			Fixed shifting
//
//  070618  Base-3.1.5    ART-2.1.5    MV-1.0.4    VTK-1.0.0b1     GADGET-1.0.0b2
//			Added zooms in Composer
//			Fixed particle picking
//			Added particle auto-scaling
//			Fixed offscreen mode with Mesa libraries
//
//  070710  Base-3.1.6    ART-2.1.5    MV-1.0.4    VTK-1.0.0b1     GADGET-1.0.0b2
//			Composer bug fixes
//
//  070826  Base-3.2.0    ART-2.1.5    MV-1.0.4    VTK-1.0.0b1     GADGET-1.0.0b2
//			VTK-style iExpressionParser replaced with iCalculator
//          scripts are completely re-written and expanded
//
//  070916  Base-3.2.1    ART-2.1.5    MV-1.0.4    VTK-1.0.0b1     GADGET-1.0.0b2
//			Bug fixes
//
//  071018  Base-3.2.2    ART-2.1.6    MV-1.0.4    VTK-1.0.0b1     GADGET-1.0.0b2
//			Abundances/densities switch for species in ART extension
//			Bug fixes
//
//  080112  Base-3.2.3    ART-2.1.6    MV-1.0.4    VTK-1.0.0b1     GADGET-1.0.0b2
//			Bug fixes in iOrthoSlicer and iBoundedPolyDataSource
//
//  080510  Base-3.2.4    ART-2.1.7    MV-1.0.4    VTK-1.0.0     GADGET-1.0.0
//			Minor bug fixes
//
//  080510  Base-3.2.5    ART-2.1.8    MV-1.0.4    VTK-1.0.0     GADGET-1.0.0
//			Color bar size, minor bug fixes, including finite cross-section bar in ART edition
//
//  080824  Base-3.2.6    ART-2.1.8    MV-1.0.4    VTK-1.0.0     GADGET-1.0.0
//			Depth search option as a way to fix VTK artifacts, refresh button on FileSet dialog
//
//  081019  Base-3.2.7    ART-2.1.8    MV-1.1.0    VTK-1.0.0     GADGET-1.0.0
//			MultiView extension redone, port to VTK 5.2, fixed overlays
//
//  090822  Base-3.2.8    ART-2.2.0    MV-1.1.0    VTK-1.0.0     GADGET-1.0.0
//          Fixed isosurface making for left/right coordinate systems in ART extension
//			Renamed HART extension into ART extension and added support for CART
//
//  091210  Base-3.2.9    ART-2.2.1    MV-1.1.0    VTK-1.0.0     GADGET-1.0.0
//          Added double precision particle positions reading for CART
//			Fixed a few minor bugs
//
//  100222  Base-3.3.0    ART-2.3.0    MV-1.2.0    VTK-1.1.0     GADGET-1.1.0
//          Removed last vestiges of Qt3 when using Qt4 - Qt3Support is not needed any more
//			Ported to 64-bin windows
//
//  100315  Base-3.3.1    ART-2.3.0    MV-1.2.0    VTK-1.1.0     GADGET-1.1.0
//          Emergency release: removed support for VTK 5.4 due to its performance bug
//
//  100503  Base-3.3.2    ART-2.3.1    MV-1.2.1    VTK-1.1.0     GADGET-1.1.0
//          Fixed bug with ART streamlines, fixed anti-aliasiang bug in docking, blocked
//			all extraneous functions in iggRenderWindow, re-factored the stereo mode to
//			fix the flicker reported by Doug and stereo non-recongnition reported by Patrick.
//
//  100722  Base-3.3.3    ART-2.3.1    MV-1.2.1    VTK-1.1.0     GADGET-1.1.0
//          Fixed a bug with Qt4 under Unix
//
//  100818  Base-3.3.4    ART-2.3.1    MV-1.2.1    VTK-1.1.0     GADGET-1.1.0
//          Fixed a bug with crashes in cross section, reported by Robyn
//			Ported to VTK 5.6.1, added GPU Volume rendering support
//			Removed Event Recorder as it caused compilation errors on some compilers
//
//  120320  Base-3.3.5    ART-2.3.2    MV-1.2.1    VTK-1.1.0     GADGET-1.1.0
//			A few bug fixes.
//			Ported to VTK 5.8.0, added GPU Volume rendering support
//
//  120809  Base-3.4.0    ART-2.4.0    MV-1.2.1    VTK-1.1.0     GADGET-1.1.0
//          Mioved to Mercurial SC
//			ARTIO and CART io support
//			Some widget modification (FileName, Color)
//			Removed CameraPath Animator mode, fixed title page and logo
//
//  121219  Base-3.4.1    ART-2.4.0    MV-1.2.1    VTK-1.1.0     GADGET-1.1.0
//          some bug fixes
//
//  130710  Base-3.4.2    ART-2.4.1    MV-1.2.1    VTK-1.1.0     GADGET-1.1.0
//          some bug fixes
//
//  581***********************************************************************
//          Drop sub-versioning for extensions
//
//  140309  4.0.0B1
//          Full Python integration, initial release
//
//  141118  4.0.0B2
//          Bug fixes, added two new palettes
//
//  141225  4.0.0B3
//          Active instances/etc are controlled by the shell now, small changes in the GUI
//
//  141228  4.0.0
//          Only tiny changes over B3, releasing as 4.0.0
//
//  150212  4.0.1
//          Two small bugs fixed
//
//  150216  4.1.0
//          Port to VTK6, refactored eror reporting (use Monitor), progress observer, output, histogram making, some GUI changes.
//
//  150301  4.1.1
//          Off-screen shell
//
//  15....  4.1.2
//          Bug fixes, Debian portability switch, fixed the bug with render observer not displaying, new ART mode
//

//
//  ToDo (major additions)
//
//  1. Point sprats for particles
//  2. Multiple images in Composer (do we need this???)
//

#if ISHELL_INCLUDED(ISHELL_GG)
#include "iggsubjectfactory.h"
#endif


const iString iVersion::GetVersion()
{ 
	return iString(IVERSION);
}


const iString iVersion::GetRevisionId()
{ 
	return iString(IREVISION);
}


const iString iVersion::GetIncludedExtensions()
{ 
	iString tmp; 
#if IEXTENSION_INCLUDED(IEXTENSION_ART)
	tmp += "ART extension;";
#endif
#if IEXTENSION_INCLUDED(IEXTENSION_MV)
	tmp += "MultiView extension;";
#endif
#if IEXTENSION_INCLUDED(IEXTENSION_GADGET)
	tmp += "GADGET extension;";
#endif
#if IEXTENSION_INCLUDED(IEXTENSION_VTK)
	tmp += "Native-VTK extension;";
#endif
	return tmp;
}


const iString iVersion::GetIncludedShells()
{ 
	iString tmp;
#if ISHELL_INCLUDED(ISHELL_OS)
	tmp += iString("Off-screen shell") + ";";
#endif
#if ISHELL_INCLUDED(ISHELL_CL)
	tmp += iString("Command-line shell") + ";";
#endif
#if ISHELL_INCLUDED(ISHELL_GG)
	tmp += iggSubjectFactory::GetIncludedShells();
#endif
	return tmp;
}
