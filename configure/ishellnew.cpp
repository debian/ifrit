/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iconfigure.h"
#include "ishell.h"


//
//  Factory method
//
#if ISHELL_INCLUDED(ISHELL_OS)
#include "iosshell.h"
#endif

#if ISHELL_INCLUDED(ISHELL_CL)
#include "iclshell.h"
#endif

#if ISHELL_INCLUDED(ISHELL_GG)
#include "iggshell.h"
#endif


iShell* iShell::New(iRunner *runner, int argc, const char **argv)
{
	//
	//  Shell specifications
	//
	const char* list[] = 
	{
#if ISHELL_INCLUDED(ISHELL_QT)
		"qt",
		"qt/embed",
		"qt/exten",
#endif
#if ISHELL_INCLUDED(ISHELL_CL)
		"cl",
#endif
#if ISHELL_INCLUDED(ISHELL_OS)
		"os",
#endif
		0 
	};

	iString type = list[0];

	//
	//  Also use options to specify shell - they overwrite the default choice
	//
	if(argc > 0)
	{
		iString s = iString(argv[0]);
		if(s[0] == '-') s = s.Part(1);
		int i = 0;
		while(list[i] != 0)
		{
			if(s == list[i])
			{
				type = s;
				argc--;
				argv++;
				break;
			}
			i++;
		}
	}

	int mode = 0;
	if(type.EndsWith("/exten")) mode = 1;
	if(type.EndsWith("/embed")) mode = 2;

#if ISHELL_INCLUDED(ISHELL_OS)
	if(type.BeginsWith("os"))
	{
		return new iosShell(runner,type.Part(0,2),argc,argv);
	}
#endif

#if ISHELL_INCLUDED(ISHELL_CL)
	if(type.BeginsWith("cl"))
	{
		return new iclShell(runner,true,type.Part(0,2),argc,argv);
	}
#endif

#if ISHELL_INCLUDED(ISHELL_GG)
	return new iggShell(runner,(mode==1),type.Part(0,2),argc,argv);
#else
	return 0;
#endif
}

