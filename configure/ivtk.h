/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  VTK version and build (porting helper)
//
#ifndef IVTK_H
#define IVTK_H


#include <vtkToolkits.h>
#include <vtkConfigure.h>


#if (VTK_MAJOR_VERSION < 5) || (VTK_MAJOR_VERSION==5 && VTK_MINOR_VERSION<6)
#error "This version of VTK is too old and is no longer supported."
#endif


#if (VTK_MAJOR_VERSION==5 && VTK_MINOR_VERSION<10)
#define IVTK_5_PRE10
#endif


#if (VTK_MAJOR_VERSION==5 && VTK_MINOR_VERSION<8)
#define IVTK_5_PRE8
#endif


#if (VTK_MAJOR_VERSION == 5)
#define IVTK_5
#define SetInputData   SetInput
#define AddInputData   AddInput
#define SetSourceData  SetSource
#endif


#define IVTK_QUAD_ORDER 3

#ifdef VTK_USE_VIDEO_FOR_WINDOWS
#define IVTK_SUPPORTS_AVI
#endif

#ifdef VTK_USE_MPEG2_ENCODER
#define IVTK_SUPPORTS_MPEG
#endif

#endif // IVTK_H
