/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "irendertool.h"


#include "iactor.h"
#include "icamera.h"
#include "ierror.h"
#include "ieventobserver.h"
#include "imagnifier.h"
#include "irendertoolbackground.h"
#include "ishell.h"
#include "istereoimage.h"
#include "istereoimagearray.h"
#include "iviewmodule.h"

#include <vtkActor2D.h>
#include <vtkCamera.h>
#include <vtkCuller.h>
#include <vtkCullerCollection.h>
#include <vtkDepthPeelingPass.h>
#include <vtkLight.h>
#include <vtkLightCollection.h>
#include <vtkLineSource.h>
#include <vtkOpenGLRenderWindow.h>
#include <vtkPolyDataMapper.h>
#include <vtkPolyDataMapper2D.h>
#include <vtkPropAssembly.h>
#include <vtkPropCollection.h>
#include <vtkProperty.h>
#include <vtkProperty2D.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowCollection.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkTranslucentPass.h>

//
//  Templates
//
#include "iarray.tlh"


using namespace iParameter;


//
//  Helper classes
//
class iMainWindowObserver : public iEventObserver
{

public:

	vtkTypeMacro(iMainWindowObserver,iEventObserver);
	static iMainWindowObserver* New(iRenderTool *rt = 0)
	{
		IASSERT(rt);
		return new iMainWindowObserver(rt);
	}

protected:

	iMainWindowObserver(iRenderTool *rt) : iEventObserver()
	{
		mRenderTool = rt;
	}

	virtual void ExecuteBody(vtkObject *, unsigned long event, void *)
	{
		switch(event)
		{
		case ModifiedEvent:
			{
				mRenderTool->mRenderWindowSize = mRenderTool->mMainWin->GetSize();
				mRenderTool->mRenderWindowPosition = mRenderTool->mMainWin->GetPosition();
				break;
			}
		}
	}

private:

	iRenderTool *mRenderTool;
};


class iDualWindowObserver : public iEventObserver
{

public:

	vtkTypeMacro(iDualWindowObserver,iEventObserver);
	static iDualWindowObserver* New(iRenderTool *rt = 0)
	{
		IASSERT(rt);
		return new iDualWindowObserver(rt);
	}

protected:

	iDualWindowObserver(iRenderTool *rt) : iEventObserver()
	{
		mRenderTool = rt;
	}

	virtual void ExecuteBody(vtkObject *, unsigned long event, void *)
	{
		switch(event)
		{
		case EndEvent:
			{
				if(mRenderTool->mDualWin != 0) mRenderTool->mDualWin->Render();
				break;
			}
		case ModifiedEvent:
			{
				if(mRenderTool->mDualWin != 0) mRenderTool->mDualWin->SetSize(mRenderTool->mMainWin->GetSize());
				break;
			}
		}
	}

private:

	iRenderTool *mRenderTool;
};


class iRendererObserver : public iEventObserver
{

public:

	vtkTypeMacro(iRendererObserver,iEventObserver);
	static iRendererObserver* New(iRenderTool *rt = 0)
	{
		IASSERT(rt);
		return new iRendererObserver(rt);
	}

protected:

	iRendererObserver(iRenderTool *rt) : iEventObserver()
	{
		mRenderTool = rt;
	}

	virtual void ExecuteBody(vtkObject *, unsigned long event, void *)
	{
		switch(event)
		{
		case ModifiedEvent:
			{
				break;
			}
		case ResetCameraClippingRangeEvent:
			{
				double *cr = mRenderTool->mMainRen->GetActiveCamera()->GetClippingRange();
				if(mRenderTool->mAutoClippingRange)
				{
					mRenderTool->mClippingRange[0] = cr[0];
					mRenderTool->mClippingRange[1] = cr[1];
				}
				else
				{
					cr[0] = mRenderTool->mClippingRange[0];
					cr[1] = mRenderTool->mClippingRange[1];
				}
				break;
			}
		}
	}

private:

	iRenderTool *mRenderTool;
};


class iStereoRenderObserver : public iEventObserver
{

public:

	vtkTypeMacro(iStereoRenderObserver,iEventObserver);
	static iStereoRenderObserver* New(iRenderTool *rt = 0)
	{
		IASSERT(rt);
		return new iStereoRenderObserver(rt);
	}

protected:

	iStereoRenderObserver(iRenderTool *rt) : iEventObserver()
	{
		mRenderTool = rt;
	}

	virtual void ExecuteBody(vtkObject *, unsigned long event, void *)
	{
		switch(event)
		{
		case ModifiedEvent:
			{
				if(mRenderTool->mMainWinStereoRender != (mRenderTool->mMainWin->GetStereoRender()!=0))
				{
					mRenderTool->mMainWin->SetStereoRender(mRenderTool->mMainWinStereoRender?1:0);
				}
				break;
			}
		}
	}

private:

	iRenderTool *mRenderTool;
};


//
//  Main class
//
iRenderTool::iRenderTool(iViewModule *vm, vtkRenderer *ren, iMagnifier *mag) : iViewModuleComponent(vm), mStereoModeOffset(2-VTK_STEREO_CRYSTAL_EYES)
{
	//
	//  Keeping the rendering order (a workaround for a VTK bug)
	//
	mActorObjects = vtkPropCollection::New(); IERROR_CHECK_MEMORY(mActorObjects);
	mVolumeObjects = vtkPropCollection::New(); IERROR_CHECK_MEMORY(mVolumeObjects);

	//
	//  Main Window stuff
	//
	mDualWin = 0;
#ifdef I_NO_STEREO
	bool stereo = false;
#else
	bool stereo = true;
#endif
	mMainWin = this->GetViewModule()->GetShell()->CreateRenderWindow(this->GetViewModule(),stereo);	IERROR_CHECK_MEMORY(mMainWin);
	mMainWin->SetAlphaBitPlanes(1);
	mMainWin->SetMultiSamples(0);

	mDepthPeelingStyle = DepthPeeling::Low;

	if(ren == 0)
	{
		mMainRen = vtkRenderer::New(); IERROR_CHECK_MEMORY(mMainRen);
		mMainCam = iCamera::New(this); IERROR_CHECK_MEMORY(mMainCam);
		mMainRen->SetActiveCamera(mMainCam->GetDevice());
		//
		//  Configure Renderer. It is important to switch the backing store off for the
		//  stereo mode to work properly.
		//
		mMainRen->SetLayer(IRENDERTOOLBACKGROUND_FRONT_LAYER);
		mMainRen->BackingStoreOff();
		mMainRen->TwoSidedLightingOff();
		mMainRen->LightFollowCameraOn();
		mMainRen->GetActiveCamera()->SetEyeAngle(2.0);
		this->ConfigureDepthPeeling(mMainRen);

		//
		//  Configure Interactor (can be null)
		//
		mMainInt = this->GetViewModule()->GetShell()->CreateRenderWindowInteractor(this->GetViewModule());
		if(mMainInt != 0)
		{
			mMainInt->SetRenderWindow(mMainWin);
			mMainInt->LightFollowCameraOff();
		}
		//
		//  Image magnifier
		//
		mMagnifier = iMagnifier::New(); IERROR_CHECK_MEMORY(mMagnifier);
	}
	else
	{
		mMainRen = ren;
		mMainInt = 0;
		mMainCam = 0;
		mMagnifier = mag;
		IASSERT(mag);
		mMagnifier->Register(ren);
	}

	//
	//  Create backgrounds
	//
	mMainBkg = iRenderToolBackground::New(); IERROR_CHECK_MEMORY(mMainBkg);
	mDualBkg = iRenderToolBackground::New(); IERROR_CHECK_MEMORY(mDualBkg);

	//
	//  Configure Window
	//
	mMainWin->SetSize(640,480);
	mMainWin->SetNumberOfLayers(2);
	mMainWin->AddRenderer(mMainRen);
	mMainWin->AddRenderer(mMainBkg->GetRenderer());
	mMainWin->SetWindowName("IFrIT - Visualization Window");

	//
	//  Dual window renderer and observer
	//
	mDualRen = this->CreateRenderer();

	mDualRen->SetActiveCamera(mMainRen->GetActiveCamera());
	mDualRen->LightFollowCameraOff();

	//
	//  Window observers
	//
	mMainWindowObserver = iMainWindowObserver::New(this); IERROR_CHECK_MEMORY(mMainWindowObserver);
	mDualWindowObserver = iDualWindowObserver::New(this); IERROR_CHECK_MEMORY(mDualWindowObserver);

	mMainWin->AddObserver(vtkCommand::ModifiedEvent,mMainWindowObserver);

	//
	//  Dual window alignment markers (only needed for the primary window)
	//
	if(ren == 0)
	{
		mStereoAlignmentMarksActor = vtkPropAssembly::New(); IERROR_CHECK_MEMORY(mStereoAlignmentMarksActor);
		mStereoAlignmentMarksActor->VisibilityOff();
		mStereoAlignmentMarksActor->PickableOff();
		vtkPolyDataMapper2D *mapper2D = vtkPolyDataMapper2D::New(); IERROR_CHECK_MEMORY(mapper2D);
		vtkLineSource *ls;
		vtkActor2D *actor2D[8];

		float aml = 0.15;
		float amo = 0.1;
		int i;
		for(i=0; i<8; i++)
		{
			actor2D[i] = vtkActor2D::New(); IERROR_CHECK_MEMORY(actor2D[i]);
			mapper2D = vtkPolyDataMapper2D::New(); IERROR_CHECK_MEMORY(mapper2D);
			ls = vtkLineSource::New(); IERROR_CHECK_MEMORY(ls);
			ls->SetResolution(1);
			if(i%2 == 0)
			{
				ls->SetPoint1(-aml,0.0,0.0);
				ls->SetPoint2(aml,0.0,0.0);
			}
			else
			{
				ls->SetPoint1(0.0,-aml,0.0);
				ls->SetPoint2(0.0,aml,0.0);
			}
			ls->Update();
			mapper2D->SetInputConnection(ls->GetOutputPort());
			ls->Delete();
			vtkCoordinate *c = vtkCoordinate::New(); IERROR_CHECK_MEMORY(c);
			c->SetCoordinateSystemToView();
			mapper2D->SetTransformCoordinate(c);
			c->Delete();
			actor2D[i]->SetMapper(mapper2D);
			mapper2D->Delete();
			actor2D[i]->GetProperty()->SetColor(0.0,0.0,0.0);
			actor2D[i]->GetPositionCoordinate()->SetCoordinateSystemToNormalizedViewport();
			actor2D[i]->PickableOff();	
			actor2D[i]->SetPosition((0.5-amo)*(2*((i/2)%2)-1),(0.5-amo)*(2*((i/4)%2)-1));
			actor2D[i]->SetLayerNumber(1);
			mStereoAlignmentMarksActor->AddPart(actor2D[i]);
			actor2D[i]->Delete();
		}

		this->AddObject(mStereoAlignmentMarksActor);
		mStereoAlignmentMarksActor->Delete();
	}
	else
	{
		mStereoAlignmentMarksActor = 0;
	}

	//
	//  Clipping range controls
	//
	mRendererObserver = iRendererObserver::New(this); IERROR_CHECK_MEMORY(mRendererObserver);
	mAutoClippingRange = true;
	mMainRen->AddObserver(vtkCommand::ResetCameraClippingRangeEvent,mRendererObserver);
	mMainRen->AddObserver(vtkCommand::ModifiedEvent,mRendererObserver);

	//
	//  Disable pressing 3 in the render window
	//
	mStereoRenderObserver = iStereoRenderObserver::New(this); IERROR_CHECK_MEMORY(mStereoRenderObserver);
	mMainWin->AddObserver(vtkCommand::ModifiedEvent,mStereoRenderObserver);

	//
	//  RenderWindow collection
	//
	mWindowCollection = vtkRenderWindowCollection::New(); IERROR_CHECK_MEMORY(mWindowCollection);
	mWindowCollectionUpToDate = false;

	//
	//  Properties
	//
	mStereoMode = 0;
	this->SetMainWinStereoRender(false);
	mMainWin->SetStereoType(0);  // for pressing 3 in the render window
	this->SetAntialiasingMode(0*Antialiasing::Points+1*Antialiasing::Lines+1*Antialiasing::Polys);
	this->SetDepthPeelingStyle(DepthPeeling::Low);
	mStereoAlignmentMarksOn = true;

	//
	//  Set the properties
	//
	mFontScale = 0;
	mFontType = TextType::Arial;
}


iRenderTool::~iRenderTool()
{
	this->ShowDualWindow(false);

	mActorObjects->Delete();
	mVolumeObjects->Delete();

	mWindowCollection->RemoveAllItems();
	mWindowCollection->Delete();
	mDualWindowObserver->Delete();
	mMainWindowObserver->Delete();

	mMagnifier->Delete();

	mMainRen->RemoveObserver(mStereoRenderObserver);
	mStereoRenderObserver->Delete();

	mMainRen->RemoveObserver(mRendererObserver);
	mRendererObserver->Delete();

	if(mMainInt != 0) mMainInt->Delete();
	if(mMainCam != 0) mMainCam->Delete();

	mMainRen->SetRenderWindow(0);
	mMainWin->RemoveRenderer(mMainBkg->GetRenderer());
	mMainWin->RemoveRenderer(mMainRen);

	mMainBkg->Delete();
	mDualBkg->Delete();

	mMainWin->RemoveAllObservers();   
	mDualRen->Delete();
	mMainRen->Delete();

	mMainWin->Finalize();
	mMainWin->Delete();
}


int iRenderTool::GetRenderingMagnification() const
{
	return mMagnifier->GetMagnification();
}


int iRenderTool::GetNumberOfActiveViews() const
{
	return (mDualWin == 0) ? 1 : 2;
}


void iRenderTool::Render()
{
	mMainWin->InvokeEvent(vtkCommand::StartEvent,NULL);
	this->ResetCameraClippingRange();
	mMainWin->Render();
	mMainWin->InvokeEvent(vtkCommand::EndEvent,NULL);
}


void iRenderTool::ResetCamera()
{
	const double scale = 1.6/sqrt(3.0);

	int i;
	double bounds[6], fp[3];
	mMainRen->ComputeVisiblePropBounds(bounds);
	mMainRen->GetActiveCamera()->GetFocalPoint(fp);

	for(i=0; i<3; i++)
	{
		if(fabs(bounds[2*i+0]-fp[i]) > fabs(bounds[2*i+1]-fp[i]))
		{
			bounds[2*i+1] = 2*fp[i] - bounds[2*i+0];
		}
		else
		{
			bounds[2*i+0] = 2*fp[i] - bounds[2*i+1];
		}
	}
	mMainRen->ResetCamera(bounds);

	//
	//  IFrIT-style reset
	//
	mMainCam->GetDevice()->SetParallelScale(scale*mMainCam->GetDevice()->GetParallelScale());
	mMainCam->GetDevice()->Dolly(1.0/scale);

	mMainRen->ResetCameraClippingRange();
}


void iRenderTool::AddObserver(unsigned long event, iEventObserver *command)
{
	Observer tmp;

	tmp.Event = event;
	tmp.Command = command;
	mObservers.Add(tmp);

	mMainWin->AddObserver(event,command);
	if(mDualWin != 0) mDualWin->AddObserver(event,command);
}


void iRenderTool::UpdateWindowName()
{
	static const iString null;

	this->UpdateWindowName(null);
}


void iRenderTool::UpdateWindowName(const iString& suffix)
{
	static iString WindowName("IFrIT - Visualization Window");

	iString base(WindowName);
	if(this->GetShell()->GetNumberOfViewModules() > 1) base += " #" + iString::FromNumber(this->GetViewModule()->GetWindowNumber()+1);

	if(!suffix.IsEmpty()) base += ": " + suffix;

	if(mDualWin == 0)
	{
		mMainWin->SetWindowName(base.ToCharPointer());
	}
	else
	{
		mMainWin->SetWindowName((base+": Left Eye").ToCharPointer());
		mDualWin->SetWindowName((base+": Right Eye").ToCharPointer());
	}
}


vtkRenderWindowCollection* iRenderTool::GetRenderWindowCollection()
{
	if(!mWindowCollectionUpToDate)
	{
		this->UpdateWindowCollection();
		mWindowCollectionUpToDate = true;
	}
	return mWindowCollection;
}


void iRenderTool::UpdateWindowCollection()
{
	mWindowCollection->RemoveAllItems();
	mWindowCollection->AddItem(mMainWin);
	if(mDualWin != 0) mWindowCollection->AddItem(mDualWin);
}


void iRenderTool::WindowsModified()
{
	mWindowCollectionUpToDate = false;
}


void iRenderTool::ResetCameraClippingRange()
{
	if(mAutoClippingRange) mMainRen->ResetCameraClippingRange();
}


void iRenderTool::SetBackground(const iColor &color)
{
	mMainRen->SetBackground(color.ToVTK());
	mDualRen->SetBackground(color.ToVTK());

	mMainBkg->SetColor(color);
	mDualBkg->SetColor(color);
}


void iRenderTool::SetBackground(const iImage &image)
{
	mMainBkg->SetImage(image);
	mDualBkg->SetImage(image);
}


void iRenderTool::AddObject(vtkProp* p)
{
	if(p == 0) return;
	
	if(p->IsA("vtkVolume") != 0)
	{
		mVolumeObjects->AddItem(p);
		mMainRen->AddViewProp(p);
		mDualRen->AddViewProp(p);
	}
	else
	{
		//
		//  Make sure all actors are ahead of volumes (is it a VTK bug?)
		//
		if(mActorObjects->GetNumberOfItems() == 0)
		{
			mMainRen->AddViewProp(p);
			mDualRen->AddViewProp(p);
		}
		else
		{
			mMainRen->GetViewProps()->InsertItem(mActorObjects->GetNumberOfItems()-1,p);
			mDualRen->GetViewProps()->InsertItem(mActorObjects->GetNumberOfItems()-1,p);
		}
		mActorObjects->AddItem(p);
	}
}


void iRenderTool::RemoveObject(vtkProp* p)
{
	if(p == 0) return;

	if(p->IsA("vtkVolume") != 0)
	{
		mVolumeObjects->RemoveItem(p);
	}
	else
	{
		mActorObjects->RemoveItem(p);
	}

	mMainRen->RemoveViewProp(p);
	mDualRen->RemoveViewProp(p);
}


void iRenderTool::SetRenderWindowSize(int w, int h)
{ 
	if(w>0 && h>0)
	{
		mMainWin->SetSize(w,h);
		if(mDualWin != 0) mDualWin->SetSize(w,h);
		mRenderWindowSize = mMainWin->GetSize();
	}
}


void iRenderTool::SetRenderWindowPosition(int x, int y)
{ 
	mMainWin->SetPosition(x,y);
	mRenderWindowPosition = mMainWin->GetPosition();
}


void iRenderTool::Reset()
{
#ifndef I_OFFSCREEN
	vtkOpenGLRenderWindow *w = vtkOpenGLRenderWindow::SafeDownCast(mMainWin);
	if(w!=0 && w->GetNeverRendered()==0)
	{
		w->MakeCurrent();
		w->OpenGLInit();
	}
	if(mDualWin != 0)
	{
		w = vtkOpenGLRenderWindow::SafeDownCast(mDualWin);
		if(w!=0 && w->GetNeverRendered()==0)
		{
			w->MakeCurrent();
			w->OpenGLInit();
		}
	}
#endif
}


void iRenderTool::SetAntialiasingMode(int m)
{
	bool s;

	mAntialiasingMode = m;

	s = (m & Antialiasing::Points) > 0;
	mMainWin->SetPointSmoothing(s?1:0);
	if(mDualWin != 0)
	{
		mDualWin->SetPointSmoothing(s?1:0);
	}

	s = (m & Antialiasing::Lines) > 0;
	mMainWin->SetLineSmoothing(s?1:0);
	if(mDualWin != 0)
	{
		mDualWin->SetLineSmoothing(s?1:0);
	}

	s = (m & Antialiasing::Polys) > 0;
	mMainWin->SetPolygonSmoothing(s?1:0);
	if(mDualWin != 0)
	{
		mDualWin->SetPolygonSmoothing(s?1:0);
	}

	this->Reset();
}


void iRenderTool::SetDepthPeelingStyle(int s)
{
	mDepthPeelingStyle = s;

	this->ConfigureDepthPeeling(mMainRen);
	this->ConfigureDepthPeeling(mDualRen);

	this->Reset();
}


void iRenderTool::ConfigureDepthPeeling(vtkRenderer *ren) const
{
	if(ren == 0) return;

	ren->SetUseDepthPeeling((mDepthPeelingStyle != DepthPeeling::Off)?1:0);
 
	switch(mDepthPeelingStyle)
	{
	case DepthPeeling::Off:
		{
			//
			//  Already set
			//
			break;
		}
	case DepthPeeling::Low:
		{
			ren->SetOcclusionRatio(0.1);
			ren->SetMaximumNumberOfPeels(3);
			break;
		}
	case DepthPeeling::High:
		{
			ren->SetOcclusionRatio(0.03);
			ren->SetMaximumNumberOfPeels(10);
			break;
		}
	case DepthPeeling::Exact:
		{
			ren->SetOcclusionRatio(0.0);
			ren->SetMaximumNumberOfPeels(0);
			break;
		}
	default:
		{
			IBUG_ERROR("Invalid DepthPeelingStyle.");
		}
	}
}

	
void iRenderTool::SetAdjustCameraClippingRangeAutomatically(bool s)
{
	mAutoClippingRange = s;
}


void iRenderTool::SetBackgroundImageFixedAspect(bool s)
{
	if(mBackgroundImageFixedAspect != s)
	{
		mBackgroundImageFixedAspect = s;
		mMainBkg->SetImageFixedAspect(s);
		mDualBkg->SetImageFixedAspect(s);
	}
}


void iRenderTool::SetBackgroundImageTile(float tx, float ty, float tw, float th)
{
	float tile[4];

	tile[0] = tx;
	tile[1] = ty;
	tile[2] = tw;
	tile[3] = th;

	this->SetBackgroundImageTile(tile);
}


void iRenderTool::SetBackgroundImageTile(const float tile[4])
{
	mMainBkg->SetImageTile(tile);
	mDualBkg->SetImageTile(tile);
}


void iRenderTool::SetCameraClippingRange(double cr[2])
{
	//
	//  This is a dirty trick to avoid the minimum bound on the nearest clipping range of 0.0001
	//
	if(!mAutoClippingRange)
	{
		double *d = mMainRen->GetActiveCamera()->GetClippingRange();
		mClippingRange[0] = d[0] = cr[0];
		mClippingRange[1] = d[1] = cr[1];
	}
}


void iRenderTool::RenderImages(int mag, iStereoImageArray &images)
{
	iStereoImage tmp;
	
	images.Clear();
	this->RenderStereoImage(mag,tmp);
	images.Add(tmp);
}


void iRenderTool::RenderStereoImage(int mag, iStereoImage &image)
{
	int v = 0;

	mDualWindowObserver->Block(true);
	if(mStereoAlignmentMarksActor != 0)
	{
		v = mStereoAlignmentMarksActor->GetVisibility();
		mStereoAlignmentMarksActor->VisibilityOff();
	}

	mMagnifier->SetMagnification(mag);

	//
	//  Render main window
	//
	int lfc = mMainRen->GetLightFollowCamera();
	mMainRen->SetLightFollowCamera(0);  //  Maintain correct lighting under magnification
	mMagnifier->SetInput(mMainRen);
	mMagnifier->Modified();
	mMagnifier->UpdateWholeExtent();
	image.ReplaceData(0,mMagnifier->GetOutput());
	mMainRen->SetLightFollowCamera(lfc);

	//
	//  Render dual window
	//
	if(mDualWin != 0)
	{
		mMagnifier->SetInput(mDualRen);
		mMagnifier->Modified();
		mMagnifier->UpdateWholeExtent();
		mMagnifier->Update();
		image.ReplaceData(1,mMagnifier->GetOutput());
	}

	mMagnifier->SetMagnification(1);

	if(mStereoAlignmentMarksActor != 0)
	{
		mStereoAlignmentMarksActor->SetVisibility(v);
	}
	mDualWindowObserver->Block(false);
}


float iRenderTool::GetLastRenderTimeInSeconds() const
{
	float s = mMainRen->GetLastRenderTimeInSeconds();
	if(mDualWin != 0) s += mDualRen->GetLastRenderTimeInSeconds();
	return s;
}


void iRenderTool::GetAspectRatio(double ar[2]) const
{
	mMainRen->GetAspect(ar);
}


//
//  Stereo operations
//
void iRenderTool::ShowStereoAlignmentMarks(bool s)
{
	mStereoAlignmentMarksOn = s;
	mStereoAlignmentMarksActor->SetVisibility((s && mDualWin!=0)?1:0);
}


void iRenderTool::SetStereoMode(int m)
{
	if(m==mStereoMode || m<0) return;

	if(mStereoMode == 1) this->ShowDualWindow(false);
	if(mStereoMode == 2)
	{
		//
		//  This seems extraneous, but that trick was needed to avoid flicker
		//  that Doug was complaining about. It was debugged on Robert's laptop.
		//
		this->SetMainWinStereoRender(false);
		mMainWin->Render();
	}
	mStereoMode = m;

	//
	//  Don't get stuck in a right-eye-only mode, it is not renderable in the non-stereo mode
	//
	mMainWin->SetStereoType(0);

	switch(mStereoMode)
	{
	case 0:
		{
			this->SetMainWinStereoRender(false);
			break;
		}
	case 1:
		{
			this->ShowDualWindow(true);
			break;
		}
	case 2:
		{
			if(mMainWin->GetStereoCapableWindow() == 0)
			{
				this->SetStereoMode(0);
				this->OutputText(MessageType::Error,"Crystal Eyes mode is not supported on this machine.");
			}
			else
			{
				mMainWin->SetStereoType(VTK_STEREO_CRYSTAL_EYES);
				this->SetMainWinStereoRender(true);
			}
			break;
		}
	default:
		{
			mMainWin->SetStereoType(mStereoMode-mStereoModeOffset);
			mStereoMode = mMainWin->GetStereoType() + mStereoModeOffset;
			this->SetMainWinStereoRender(true);
			break;
		}
	}
}


void iRenderTool::ShowDualWindow(bool s)
{
	if(s)
	{
		if(mDualWin == 0) // DualWindow is not shown yet
		{
			mDualWin = this->GetViewModule()->GetShell()->CreateRenderWindow(this->GetViewModule(),false); IERROR_CHECK_MEMORY(mDualWin);
			mDualWin->SetAlphaBitPlanes(1);
			mDualWin->SetMultiSamples(0);
			mDualWin->SetStereoTypeToRight();
			mDualWin->StereoRenderOn();
			int *s = mMainWin->GetSize();
			mDualWin->SetSize(s[0],s[1]);
			this->WindowsModified();

			int i;
			for(i=0; i<mObservers.Size(); i++)
			{
				mDualWin->AddObserver(mObservers[i].Event,mObservers[i].Command);
			}
			//
			//  Copy RenderWindows
			//
			mDualWin->SetLineSmoothing(mMainWin->GetLineSmoothing());
			mDualWin->SetPointSmoothing(mMainWin->GetPointSmoothing());
			mDualWin->SetPolygonSmoothing(mMainWin->GetPolygonSmoothing());

			mDualWin->SetNumberOfLayers(2);
			mDualWin->AddRenderer(mDualRen);
			mDualWin->AddRenderer(mDualBkg->GetRenderer());

			mMainWin->SetStereoTypeToLeft();
			this->SetMainWinStereoRender(true);
			mMainWin->AddObserver(vtkCommand::StartEvent,mDualWindowObserver);
			mMainWin->AddObserver(vtkCommand::EndEvent,mDualWindowObserver);
			mMainWin->AddObserver(vtkCommand::ModifiedEvent,mDualWindowObserver);

			if(mStereoAlignmentMarksActor!=0 && mStereoAlignmentMarksOn) mStereoAlignmentMarksActor->VisibilityOn();
			this->UpdateWindowName();
		}
	}
	else
	{
		if(mDualWin != 0)
		{
			this->WindowsModified();
			mDualRen->SetRenderWindow(0);
			mDualBkg->GetRenderer()->SetRenderWindow(0);
			mDualWin->RemoveRenderer(mDualRen);
			mDualWin->RemoveRenderer(mDualBkg->GetRenderer());
			mDualWin->Finalize();
			mDualWin->Delete();
			mDualWin = 0;

			this->SetMainWinStereoRender(false);
			mMainWin->RemoveObserver(mDualWindowObserver);

			if(mStereoAlignmentMarksActor != 0) mStereoAlignmentMarksActor->VisibilityOff();
			this->UpdateWindowName();
		}
	}
}


iRenderTool* iRenderTool::CreateInstance(vtkRenderer *ren) const
{
	return new iRenderTool(this->GetViewModule(),ren,mMagnifier);
}


vtkRenderer* iRenderTool::CreateRenderer() const
{
	vtkRenderer *ren = vtkRenderer::New();
	if(ren == 0) return 0;

	ren->SetLayer(IRENDERTOOLBACKGROUND_FRONT_LAYER);

	//
	//  Copy the state of the primary renderer
	//
	ren->SetBackingStore(mMainRen->GetBackingStore());
	ren->SetTwoSidedLighting(mMainRen->GetTwoSidedLighting());
	ren->SetBackground(mMainRen->GetBackground());
	ren->LightFollowCameraOff(); // only the primary renderer can control lights
	this->ConfigureDepthPeeling(ren);

	//
	//  Copy props. The trick is that we need to register the new renderer with every iActor
	//  so that iActors can create special mappers for this renderer. Otherwise, vtkPolyDataMappers 
	//  remember the last window they rendered into, and will reset at switching to a different window.
	//
	vtkPropCollection *pc = mMainRen->GetViewProps();
	vtkProp *p;
	pc->InitTraversal();
	while((p = pc->GetNextProp()) != 0)
	{
		ren->AddViewProp(p);
	}

	//
	//  Copy lights
	//
	vtkLight *l;
	vtkLightCollection *lc = mMainRen->GetLights();
	lc->InitTraversal();
	while((l = lc->GetNextItem()) != 0) ren->AddLight(l);

	//
	//  Copy cullers
	//
	vtkCuller *c;
	vtkCullerCollection *cc = mMainRen->GetCullers();
	cc->InitTraversal();
	ren->GetCullers()->RemoveAllItems();
	while((c = cc->GetNextItem()) != 0) ren->AddCuller(c);

	return ren;
}


int iRenderTool::GetFullScreenMode() const
{
	return 1;
}


void iRenderTool::CopyBackground(const iRenderTool *source)
{
	if(source != 0)
	{
		mMainBkg->Copy(source->mMainBkg);
		mDualBkg->Copy(source->mDualBkg);
	}
}


void iRenderTool::SetFontScale(int s)
{
	if(s>-10 && s<10)
	{
		mFontScale = s; 
	}
}


void iRenderTool::SetFontType(int s)
{
	if(TextType::IsValid(s))
	{
		mFontType = s; 
	}
}


void iRenderTool::SetMainWinStereoRender(bool s)
{
	mMainWinStereoRender = s;
	mMainWin->SetStereoRender(s?1:0);
}


#include <vtkOBJExporter.h>
#include <vtkVRMLExporter.h>
#include <vtkX3DExporter.h>


void iRenderTool::ExportScene(const iString &fname)
{
	vtkExporter *ex = 0;

	iString type = fname.Section(".",-1);
	
	if(type == "obj")
	{
		vtkOBJExporter *w = vtkOBJExporter::New(); IERROR_CHECK_MEMORY(w);
		w->SetFilePrefix(fname.Section(".",0,-2).ToCharPointer());
		ex = w;
	}
	else if(type == "x3d")
	{
		vtkX3DExporter  *w = vtkX3DExporter ::New(); IERROR_CHECK_MEMORY(w);
		w->SetBinary(0);
		w->SetFileName(fname.ToCharPointer());
		ex = w;
	}
	else if(type == "vrml")
	{
		vtkVRMLExporter  *w = vtkVRMLExporter ::New(); IERROR_CHECK_MEMORY(w);
		w->SetFileName(fname.ToCharPointer());
		ex = w;
	}

	if(ex != 0)
	{
		ex->SetRenderWindow(mMainWin);
		mMainWin->RemoveRenderer(mMainBkg->GetRenderer());
		ex->Write();
		mMainWin->AddRenderer(mMainBkg->GetRenderer());
	}
}
