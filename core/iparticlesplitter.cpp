/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iparticlesplitter.h"


#include "idataconsumer.h"
#include "idatasubject.h"
#include "ihistogrammaker.h"
#include "ierror.h"

#include <vtkCellArray.h>
#include <vtkDoubleArray.h>
#include <vtkFloatArray.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>

#include <limits.h>

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"
#include "igenericfilter.tlh"


namespace iParticleSplitter_Private
{
//
//  QuickFind
//
#define ARR(i)	Vars[ncom*(i)+varSort]

bool Find(vtkIdType n, int ncom, int varSort, float *Vars, vtkIdType iStart, float aval, vtkIdType &iLo, vtkIdType &iHi)
{
	vtkIdType i1 = iStart;
	vtkIdType i2 = n - 1;
	vtkIdType ic;

	if(aval <= ARR(i1))
	{
		iLo = i1;
		iHi = i1;
		return true;
	}
	
	if(aval >= ARR(i2))
	{
		iLo = i2;
		iHi = i2;
		return true;
	}

	while(i2-i1 > 1)
	{
		ic = (i1+i2)/2;
		if(aval >= ARR(ic)) i1 = ic;
		if(aval <= ARR(ic)) i2 = ic;
	}

	iLo = i1;
	iHi = i2;

	return true;

}
//
//  QuickSort
//
#define SAVE(CELL,i1)    { for(ii=0; ii<3; ii++) poi##CELL[ii] = Points[3*(i1)+ii]; for(ii=0; ii<ncom; ii++) var##CELL[ii] = Vars[ncom*(i1)+ii]; if(Nrms != 0) for(ii=0; ii<3; ii++) nrm##CELL[ii] = Nrms[3*(i1)+ii]; }
#define MOVE(i1,i2)      { for(ii=0; ii<3; ii++) Points[3*(i1)+ii] = Points[3*(i2)+ii]; for(ii=0; ii<ncom; ii++) Vars[ncom*(i1)+ii] = Vars[ncom*(i2)+ii]; if(Nrms != 0) for(ii=0; ii<3; ii++) Nrms[3*(i1)+ii] = Nrms[3*(i2)+ii]; }
#define RESTORE(i2,CELL) { for(ii=0; ii<3; ii++) Points[3*(i2)+ii] = poi##CELL[ii]; for(ii=0; ii<ncom; ii++) Vars[ncom*(i2)+ii] = var##CELL[ii]; if(Nrms != 0) for(ii=0; ii<3; ii++) Nrms[3*(i2)+ii] = nrm##CELL[ii]; }
#define SWAP(i1,i2)      { SAVE(1,i1); MOVE(i1,i2); RESTORE(i2,1); }

//
//  Recursive worker
//
template<class T>
void SortWorker(vtkAlgorithm *filter, vtkIdType l, vtkIdType r, int ncom, int varSort, T *Points, float *Vars, float *Nrms, T *poi1, float *var1, float *nrm1)
{
	const int M = 8;
	vtkIdType i, j;
	float v;
	int ii;

	if(filter!=0 && filter->GetAbortExecute()) return;

	if ((r-l)>M)
	{
		//
		// Use quicksort
		//
		i = (r+l)/2;
		if (ARR(l)>ARR(i)) SWAP(l,i);     // Tri-Median Method!
		if (ARR(l)>ARR(r)) SWAP(l,r);
		if (ARR(i)>ARR(r)) SWAP(i,r);
		
		j = r-1;
		SWAP(i,j);
		i = l;
		v = ARR(j);
		for(;;)
		{
			do i++; while(ARR(i) < v); // no ++i/--j in macro expansion!
			do j--; while(ARR(j) > v);
			if (j<i) break;
			SWAP(i,j);
		}
		SWAP(i,r-1);
		SortWorker(filter,l,j,ncom,varSort,Points,Vars,Nrms,poi1,var1,nrm1);
		SortWorker(filter,i+1,r,ncom,varSort,Points,Vars,Nrms,poi1,var1,nrm1);
	}
	else 
	{
		//
		// Array is small, use insertion sort. 
		//
		for (i=l+1; i<=r; i++)
		{
			SAVE(1,i);
			v = ARR(i);
			j = i;
			while (j>l && ARR(j-1)>v)
			{
				MOVE(j,j-1);
				j--;
			}
			RESTORE(j,1);
		}
    }
}

//
// Do our own quick sort for efficiency reason (based on a Java code by Denis Ahrens)
//
template<class T>
void Sort(vtkAlgorithm *filter, vtkIdType n, int ncom, int varSort, T *Points, float *Vars, float* Nrms)
{
	T poi1[3];
	float nrm1[3];
	float *var1; var1 = new float[ncom]; IERROR_CHECK_MEMORY(var1);

	SortWorker(filter,0,n-1,ncom,varSort,Points,Vars,Nrms,poi1,var1,nrm1);
	
	delete [] var1;
}
};


using namespace iParticleSplitter_Private;


iParticleSplitter::iParticleSplitter(iDataConsumer *consumer) : iGenericPolyDataFilter<vtkPolyDataAlgorithm>(consumer,1,true)
{
	mOwnsData = false;

	mSavedPoints = 0;
	mSavedVerts = 0;
	mSavedNorms = 0;
	mSavedVars = 0;

	vtkPolyData *i = InputData();
	vtkPolyData *o = OutputData();

	//
	//  Splitter functionality
	//
	mSplitVariable = -1;
	mRanges.Add(iPair(-iMath::_LargeFloat,iMath::_LargeFloat));

	mSorted = mSorterMode = false;
	mSortingMTime = 0;
}


iParticleSplitter::~iParticleSplitter()
{
	if(mOwnsData)
	{
		mSavedPoints->Delete();
		mSavedVerts->Delete();
		mSavedNorms->Delete();
		mSavedVars->Delete();
	}
}


float iParticleSplitter::GetMemorySize() const
{
	float s = 0.0;
	if(mOwnsData) 
	{
		s += mSavedPoints->GetActualMemorySize();
		s += mSavedVerts->GetActualMemorySize();
		s += mSavedNorms->GetActualMemorySize();
		s += mSavedVars->GetActualMemorySize();
	}
	return s;
}


bool iParticleSplitter::CreatePiece(const iPair &p)
{ 
	if(mSorterMode || mSplitVariable==-1) return false;

	//
	//  Create a new piece (somehow it is not automatically generated - must be a VTK bug)
	//
	int n = this->GetNumberOfOutputPorts();
	this->SetNumberOfOutputPorts(n+1);
	vtkPolyData *d = vtkPolyData::New(); IERROR_CHECK_MEMORY(d);
	this->GetExecutive()->SetOutputData(n,d);
#ifndef I_NO_CHECK
	IASSERT(this->OutputData(n));
#endif

	mRanges.Add(p);
	this->Modified();

	return true;
}


bool iParticleSplitter::DeletePiece(int n)
{
	if(n>=0 && n<mRanges.Size() && mRanges.Size()>1)
	{
		mRanges.Remove(n);

		int i;
		for(i=n; i<mRanges.Size(); i++)
		{
			this->GetExecutive()->SetOutputData(i,this->GetExecutive()->GetOutputData(i+1),this->GetExecutive()->GetOutputInformation(i+1));
		}
		this->SetNumberOfOutputPorts(mRanges.Size());
		return true;
	}
	else return false;
}


void iParticleSplitter::SetPieceRange(int n, const iPair &p)
{
	if(n>=0 && n<mRanges.Size() && (p.Min!=mRanges[n].Min || p.Max!=mRanges[n].Max))
	{
		mRanges[n] = p;
		this->Modified();
	}
}


void iParticleSplitter::TakeOverData(bool s)
{
	if(mOwnsData == s) return;
	mOwnsData = s;
	if(mOwnsData)
	{
		mSavedPoints = vtkPoints::New(); IERROR_CHECK_MEMORY(mSavedPoints);
		mSavedVerts = vtkCellArray::New(); IERROR_CHECK_MEMORY(mSavedVerts);
		mSavedNorms = vtkFloatArray::New(); IERROR_CHECK_MEMORY(mSavedNorms);
		mSavedVars = vtkFloatArray::New(); IERROR_CHECK_MEMORY(mSavedVars);
	}
	else
	{
		mSavedPoints->Delete(); mSavedPoints = 0;
		mSavedVerts->Delete(); mSavedVerts = 0;
		mSavedNorms->Delete(); mSavedNorms = 0;
		mSavedVars->Delete(); mSavedVars = 0;
	}
	this->Modified();
}


void iParticleSplitter::SetSplitVariable(int v)
{
	//
	//  Reset to a single piece if the Variable changes
	//
	if(v!=mSplitVariable && v>=-1 && v<this->GetLimits()->GetNumVars())
	{
		mRanges.Clear();
		this->SetNumberOfOutputPorts(1);

		mSplitVariable = v;
		mSorted = false;

		if(v == -1)
		{
			mRanges.Add(iPair(-iMath::_LargeFloat,iMath::_LargeFloat));
		}
		else
		{
			mRanges.Add(this->GetLimits()->GetDataRange(mSplitVariable));
		}
		this->Modified();
	}
}


void iParticleSplitter::SetSorterMode(bool s)
{
	if(s == mSorterMode) return;

	mSorterMode = s;
	//
	//  Reset to a single piece if the Variable changes
	//
	if(s)
	{
		mRanges.Clear();
		this->SetNumberOfOutputPorts(1);
		mSorted = false;

		mRanges.Add(iPair(-iMath::_LargeFloat,iMath::_LargeFloat));

		this->Modified();
	}
}


void iParticleSplitter::ProvideOutput()
{
	vtkPolyData *input = this->InputData();
	vtkPolyData *output = this->OutputData();

	output->Initialize();

	vtkPoints *newPoints, *oldPoints;
	vtkCellArray *newVerts, *oldVerts;
	vtkFloatArray *newNorms, *oldNorms;
	vtkFloatArray *newVars, *oldVars;

	if(mSorterMode || mSplitVariable==-1 || mSplitVariable>=this->GetLimits()->GetNumVars())
	{
		output->ShallowCopy(input);
		if(!mSorterMode) return;
	}

	if(mOwnsData)
	{
		oldPoints = mSavedPoints;
		oldVerts = mSavedVerts;
		oldVars = mSavedVars;
		oldPoints->SetDataType(input->GetPoints()->GetDataType());
		oldPoints->DeepCopy(input->GetPoints());
		oldVerts->DeepCopy(input->GetVerts());
		if(input->GetPointData()->GetNormals() != 0)
		{
			oldNorms = mSavedNorms;
			oldNorms->DeepCopy(input->GetPointData()->GetNormals());
		}
		else
		{
			oldNorms = 0;
		}
		oldVars->DeepCopy(input->GetPointData()->GetScalars());
	}
	else
	{
		oldPoints = input->GetPoints();
		oldVerts = input->GetVerts();
		if(input->GetPointData()->GetNormals() != 0)
		{
			oldNorms = vtkFloatArray::SafeDownCast(input->GetPointData()->GetNormals());
		}
		else
		{
			oldNorms = 0;
		}
		oldVars = vtkFloatArray::SafeDownCast(input->GetPointData()->GetScalars());
	}

	float *pPointsF = 0;
	double *pPointsD = 0;

	bool pointsAreFloat;
	switch(oldPoints->GetDataType())
	{
	case VTK_FLOAT:
		{
			pointsAreFloat = true;
			pPointsF = (float *)oldPoints->GetVoidPointer(0);
			break;
		}
	case VTK_DOUBLE:
		{
			pointsAreFloat = false;
			pPointsD = (double *)oldPoints->GetVoidPointer(0);
			break;
		}
	default: 
		{
			output->ShallowCopy(input);
			vtkErrorMacro("Incorrect Points type");
			return;
		}
	}

	vtkIdType  *pVerts = (vtkIdType *)oldVerts->GetPointer();
	float *pNorms = 0;
	if(oldNorms != 0) pNorms = (float *)oldNorms->GetPointer(0);
	float *pVars = (float *)oldVars->GetPointer(0);

	int ncom = oldVars->GetNumberOfComponents();
	vtkIdType ntup = oldVars->GetNumberOfTuples();

	if(!mSorted || mSortingMTime<input->GetMTime())
	{
		mSorted = true;
		mSortingMTime = input->GetMTime();
		if(pointsAreFloat) 
		{
			iParticleSplitter_Private::Sort(this,ntup,ncom,mSplitVariable,pPointsF,pVars,pNorms); 
		}
		else
		{
			iParticleSplitter_Private::Sort(this,ntup,ncom,mSplitVariable,pPointsD,pVars,pNorms);
		}
#ifdef I_CHECK
		for(vtkIdType l=0; l<ntup-1; l++) if(pVars[mSplitVariable+ncom*l] > pVars[mSplitVariable+ncom*(l+1)])
		{
			IBUG_WARN("Sorted incorrectly.");
			break;
		}
#endif
	}

	if(mSorterMode) return;

	int n, nmax = mRanges.Size();
	vtkIdType iLo, iHi;
	vtkIdType iStart, iTotal;

	vtkFloatArray  *fArray;
	vtkDoubleArray *dArray;
	vtkIdTypeArray *jArray;

	for(n=0; n<nmax; n++)
	{
		if(!Find(ntup,ncom,mSplitVariable,pVars,0,mRanges[n].Min,iLo,iHi))
		{
			vtkErrorMacro("Error #1 in iGroupSplitter");
			iLo = iHi = 0;
		}
		iStart = iHi;

		if(!Find(ntup,ncom,mSplitVariable,pVars,iLo,mRanges[n].Max,iLo,iHi))
		{
			vtkErrorMacro("Error #2 in iGroupSplitter");
			iLo = iHi = ntup - 1;
		}
		iTotal = iLo - iStart + 1;

		if(iTotal<0 || iStart<0 || iLo>=ntup)
		{
			vtkErrorMacro("Error #3 in iGroupSplitter");
		}
		else
		{
			output = this->OutputData(n);
			output->Initialize();
			
			if(iTotal > 0)
			{
				newPoints = vtkPoints::New(oldPoints->GetDataType()); IERROR_CHECK_MEMORY(newPoints);

				if(pointsAreFloat)
				{
					fArray = vtkFloatArray::New(); IERROR_CHECK_MEMORY(fArray);
					fArray->SetNumberOfComponents(3);
					fArray->SetArray(pPointsF+3*iStart,3*iTotal,1);
					newPoints->SetData(fArray);
					fArray->Delete();
				}
				else
				{
					dArray = vtkDoubleArray::New(); IERROR_CHECK_MEMORY(dArray);
					dArray->SetNumberOfComponents(3);
					dArray->SetArray(pPointsD+3*iStart,3*iTotal,1);
					newPoints->SetData(dArray);
					dArray->Delete();
				}
				output->SetPoints(newPoints);
				newPoints->Delete();

				newVars = vtkFloatArray::New(); IERROR_CHECK_MEMORY(newVars);
				newVars->SetNumberOfComponents(ncom);
				newVars->SetArray(pVars+ncom*iStart,ncom*iTotal,1);
				output->GetPointData()->SetScalars(newVars);
				newVars->Delete();

				if(oldNorms != 0) 
				{
					newNorms = vtkFloatArray::New(); IERROR_CHECK_MEMORY(newNorms);
					newNorms->SetNumberOfComponents(3);
					newNorms->SetArray(pNorms+3*iStart,3*iTotal,1);
					output->GetPointData()->SetNormals(newNorms);
					newNorms->Delete();
				}

				newVerts = vtkCellArray::New(); IERROR_CHECK_MEMORY(newVerts);
				jArray = vtkIdTypeArray::New(); IERROR_CHECK_MEMORY(jArray);
				jArray->SetNumberOfComponents(1);
				jArray->SetArray(pVerts,2*iTotal,1);
				newVerts->SetCells(iTotal,jArray);
				jArray->Delete();
				output->SetVerts(newVerts);
				newVerts->Delete();
			}
		}
	}
}



