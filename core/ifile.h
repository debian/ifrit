/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IFILE_H
#define IFILE_H

//
//  Using ifstream should be completely portable. Nevertheless, we encapsulate some of
//  the functions to allow for the use of other file reading libraries if the need arises.
//

#include "istring.h"
#include <vtkIOStreamFwd.h>


struct iFilePosition; // this structure encapsulates file position type


class iFile
{

public:

	enum OpenMode 
	{
		_None = 0,
		_Read = 1,
		_Write = 2,
		_ReadWrite = 3,
		_Append = 4
	};

	enum OpenType
	{
		_Text = 0,
		_Binary = 1
	};

	iFile(const iString &name);
	~iFile();
	//
	//  Open & close
	//
	void SetName(const iString &name);
	bool Open(OpenMode mode, OpenType type);
	void Close();
	//
	//  Reading
	//
	bool ReadLine(iString &s, int max = iString::MaxLength);
	bool ReadBlock(void *p, size_t length);
	//
	//  Writing
	//
	bool WriteLine(const iString &s);
	//
	//  Skipping
	//
	bool SkipBlock(size_t length);
	//
	//  (Semi-)Random access
	//
	bool Rewind();
	int SetMarker(); // marks the current location
	bool ReturnToMarker(int marker, bool remove = false); // returns to a given location
	int GetNumberOfMarkers() const { return mNumMarker; }
	//
	//  Properties
	//
	static bool IsReadable(const iString &name);
	static bool IsWritable(const iString &name);

	inline const iString& GetName() const { return mName; }

private:

	fstream *mStream;
	iString mName;

	iFilePosition *mMarker;
	int mNumMarker, mMaxMarker;

};

#endif // IFILE_H
 

