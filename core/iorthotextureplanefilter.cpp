/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iorthotextureplanefilter.h"


#include "idatalimits.h"
#include "ierror.h"
#include "iorthoslicer.h"
#include "imath.h"

#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkImageData.h>
#include <vtkPointData.h>

//
//  Templates
//
#include "igenericfilter.tlh"


iOrthoTexturePlaneFilter::iOrthoTexturePlaneFilter(iDataConsumer *consumer) : iGenericFilter<vtkDataSetAlgorithm,vtkImageData,vtkPolyData>(consumer,1,true) 
{
	mTexture = 0;
	mOffX = mOffY = 0;
}


void iOrthoTexturePlaneFilter::SetTexturePiece(vtkImageData *texture, int offx, int offy, double globalOrg[3], int globalDims[3])
{
	int i;

	mTexture = texture;
	mOffX = offx;
	mOffY = offy;

	for(i=0; i<3; i++)
	{
		mGlobalOrg[i] = globalOrg[i];
		mGlobalDims[i] = globalDims[i];
	}

	this->Modified();
}


void iOrthoTexturePlaneFilter::ProvideOutput()
{
	vtkImageData *input = this->InputData();
	vtkPolyData *output = this->OutputData();
	int dims[3], dimsTexture[3];
	double org[3], spa[3];

	output->Initialize();

	if(mTexture == 0) return;

	//
	//  Do we have cell or point data?
	//
	vtkFloatArray *scalars;
	bool isPointData;
	if(input->GetPointData()->GetScalars() != 0)
	{
		isPointData = true;
		scalars = vtkFloatArray::SafeDownCast(input->GetPointData()->GetScalars());
	}
	else if(input->GetCellData()->GetScalars() != 0)
	{
		isPointData = false;
		scalars = vtkFloatArray::SafeDownCast(input->GetCellData()->GetScalars());
	}
	else
	{
		//
		//  Must be quiet here for parallel execution
		//
		return;
	}

	if(scalars == 0) return;

	//
	//  Prepare texture support
	//
	input->GetOrigin(org);
	input->GetSpacing(spa);
	input->GetDimensions(dims);

	int i, Axis = -1;
	for(i=0; i<3; i++)
	{
		if(dims[i] == 1) Axis = i;
	}
	if(Axis == -1) return;

    int u, v, dimU, dimV;
	iOrthoSlicer::GetUV(Axis,u,v);

	if(isPointData)
	{
		dimU = dims[u] - 1;
		dimV = dims[v] - 1;
	}
	else
	{
		dimU = dims[u];
		dimV = dims[v];
	}

	if(dimU<1 || dimV<1)
	{
		return;
	}

	mTexture->GetDimensions(dimsTexture);

	//
	//  AppendPolyDataFilter does not merge texture coordinates into one, so we need to create the correct texture support here.
	//  We create it for zero offset instance, and skip it altogether for other instances.
	//
	if(mOffX==0 && mOffY==0)
	{
		float tc[2], pad;
		static vtkIdType pts[4]={0,1,2,3};

		int dimU, dimV;
		if(isPointData)
		{
			dimU = mGlobalDims[u] - 1;
			dimV = mGlobalDims[v] - 1;
			pad = 0.0;
		}
		else
		{
			dimU = mGlobalDims[u] - 1;
			dimV = mGlobalDims[v] - 1;
			pad = 0.5;
		}
		//
		// We'll create the building blocks of polydata including data attributes.
		//
		vtkPoints *points = vtkPoints::New(VTK_FLOAT); IERROR_CHECK_MEMORY(points);
		points->SetNumberOfPoints(4);

		vtkCellArray *polys = vtkCellArray::New(); IERROR_CHECK_MEMORY(polys);

		vtkFloatArray *tcoords = vtkFloatArray::New(); IERROR_CHECK_MEMORY(tcoords);
		tcoords->SetNumberOfComponents(2);
		tcoords->SetNumberOfTuples(4);

		vtkFloatArray *normals = vtkFloatArray::New(); IERROR_CHECK_MEMORY(normals);
		normals->SetNumberOfComponents(3);
		normals->SetNumberOfTuples(4);

		//
		// Load the cell, and data attributes.
		//
		polys->InsertNextCell(4,pts);

		//
		//  Place the support plane
		//
		double x1[3];
		x1[Axis] = org[Axis];
		//
		//  LL point 
		//
		x1[u] = mGlobalOrg[u] - pad*spa[u];
		x1[v] = mGlobalOrg[v] - pad*spa[v];
		points->SetPoint(0,x1);
		tc[0] = 0.0;
		tc[1] = 0.0;
		tcoords->SetTuple(0,tc);
		//
		//  LR point 
		//
		x1[u] = mGlobalOrg[u] - pad*spa[u] + spa[u]*dimU;
		x1[v] = mGlobalOrg[v] - pad*spa[v];
		points->SetPoint(1,x1);
		tc[0] = float(dimU)/dimsTexture[0];
		tc[1] = 0.0;
		tcoords->SetTuple(1,tc);
		//
		//  UR point 
		//
		x1[u] = mGlobalOrg[u] - pad*spa[u] + spa[u]*dimU;
		x1[v] = mGlobalOrg[v] - pad*spa[v] + spa[v]*dimV;
		points->SetPoint(2,x1);
		tc[0] = float(dimU)/dimsTexture[0];
		tc[1] = float(dimV)/dimsTexture[1];
		tcoords->SetTuple(2,tc);
		//
		//  UL point 
		//
		x1[u] = mGlobalOrg[u] - pad*spa[u];
		x1[v] = mGlobalOrg[v] - pad*spa[v] + spa[v]*dimV;
		points->SetPoint(3,x1);
		tc[0] = 0.0;
		tc[1] = float(dimV)/dimsTexture[1];
		tcoords->SetTuple(3,tc);

		x1[u] = x1[v] = 0.0; x1[Axis] = 1.0;
		normals->SetTuple(0,x1);
		normals->SetTuple(1,x1);
		normals->SetTuple(2,x1);
		normals->SetTuple(3,x1);

		//
		// We now assign the pieces to the vtkPolyData.
		//
		output->SetPolys(polys);
		output->SetPoints(points);
		output->GetPointData()->SetTCoords(tcoords);
		output->GetPointData()->SetNormals(normals);
		polys->Delete();
		points->Delete();
		tcoords->Delete();
		normals->Delete();
	}

	//
	//  Fill in our portion of the texture
	//
 	int ncomIn = scalars->GetNumberOfComponents();
    float *ptrIn = scalars->GetPointer(0);
	float *ptrOut = (float *)mTexture->GetScalarPointer();

	if(ptrOut == 0)
	{
		vtkErrorMacro("Texture data has not been allocated properly.");
		return;
	}

	int ijk[3], ijkmin[3], ijknum[3];

	ijk[Axis] = 0;

	ijkmin[u] = 0;
	ijkmin[v] = 0;
	ijknum[u] = dimU;
	ijknum[v] = dimV;

	if(mOffX < 0) ijkmin[u] -= mOffX;
	if(mOffY < 0) ijkmin[v] -= mOffY;
	if(mOffX+ijknum[u] > dimsTexture[0]) ijknum[u] = dimsTexture[0] - mOffX;
	if(mOffY+ijknum[v] > dimsTexture[1]) ijknum[v] = dimsTexture[1] - mOffY;

	vtkIdType off1 = 0, off2 = 0, off3;
	switch(Axis)
	{
	case 0:
		{
			//
			//  u = 1, v = 2;
			//
			off1 = dims[0];
			off2 = dims[0]*dims[1];
			break;
		}
	case 1:
		{
			//
			//  u = 0, v = 1;
			//
			off1 = 1;
			off2 = dims[0]*dims[1];
			break;
		}
	case 2:
		{
			//
			//  u = 0, v = 1;
			//
			off1 = 1;
			off2 = dims[0];
			break;
		}
	}

	off1 *= ncomIn;
	off2 *= ncomIn;
	off3 = off1 + off2;

	vtkIdType lIn, lOut;
	for(ijk[v]=ijkmin[v]; ijk[v]<ijknum[v]; ijk[v]++)
	{
		this->UpdateProgress(double(ijk[v]-ijkmin[v])/(ijknum[v]-ijkmin[v]));
		if(this->GetAbortExecute()) break;

		for(ijk[u]=ijkmin[u]; ijk[u]<ijknum[u]; ijk[u]++)
		{
			lIn = ncomIn*(ijk[0]+dims[0]*(ijk[1]+(vtkIdType)dims[1]*ijk[2]));
			lOut = mOffX + ijk[u] + dimsTexture[0]*(mOffY+ijk[v]);
			if(isPointData)
			{
				ptrOut[lOut] = 0.25f*(ptrIn[lIn]+ptrIn[lIn+off1]+ptrIn[lIn+off2]+ptrIn[lIn+off3]);
			}
			else
			{
				ptrOut[lOut] = ptrIn[lIn];
			}
//			ptrOut[lOut] = iMath::Pow10(-0.1*float(ijk[u]));
		}
	}
	mTexture->Modified();
}

