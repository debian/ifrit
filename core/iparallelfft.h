/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IPARALLELFFT_H
#define IPARALLELFFT_H


#include "iparallelworker.h"


class iParallelFFT : protected iParallelWorker
{

public:

	static iParallelFFT* New(iParallelManager *pp);
	virtual void Delete(){ delete this; }

	//
	// data must be a float arrays with dimensions n1*n2*n3
	// FFT component #icomp with ncomp altogether
	// dnyq (Nyquist frequency) must be a float arrays with dimensions 2*n2*n3
	// Memory allocation is not checked.
	//
	int Transform(bool forward, int comp, int ncomp, int n1, int n2, int n3, float *data, float *dnyq);

protected:

	virtual ~iParallelFFT();

private:

	iParallelFFT(iParallelManager *pp);

	virtual int ExecuteStep(int step, iParallel::ProcessorInfo &p);

	void ExecuteForwardTransform1(iParallel::ProcessorInfo &p);
	void ExecuteForwardTransform2(iParallel::ProcessorInfo &p);
	void ExecuteForwardTransform3(iParallel::ProcessorInfo &p);
	void ExecuteInverseTransform3(iParallel::ProcessorInfo &p);
	void ExecuteInverseTransform2(iParallel::ProcessorInfo &p);
	void ExecuteInverseTransform1(iParallel::ProcessorInfo &p);

	float *mWork1, *mWork2, *mWork3, *mW1, *mZ1, *mData, *mDnyq;
	int mNC, mN1, mN2, mN3, mN12, mNumProcs;
	float mProg, mProgStart, mProgScale;
};

#endif  // IPARALLELFFT_H
