/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

#include "ivtk.h"
#ifndef IVTK_5
#include <vtkAutoInit.h>
VTK_MODULE_INIT(vtkInteractionStyle);
VTK_MODULE_INIT(vtkRenderingOpenGL);
VTK_MODULE_INIT(vtkRenderingVolumeOpenGL);
VTK_MODULE_INIT(vtkRenderingFreeType);
VTK_MODULE_INIT(vtkRenderingFreeTypeOpenGL);
#endif

#include "irunner.h"


#include "ierror.h"
#include "iexception.h"
#include "iscript.h"
#include "ishell.h"
#include "isystem.h"
#include "iversion.h"


#include <vtkMultiThreader.h>


//
// Generic implementation of the runner
//
namespace iRunner_Private
{
	bool RunShell(iShell *shell)
	{
		bool ret = false;

		try
		{
			//
			//  Run application
			//
			if(shell->Initialize())
			{
				shell->Start();
				ret = !shell->IsFailed();
			}
			else
			{
				//this->Display(MessageType::Error,"Shell <"+shell->Type()+"> failed to initialize.");
			}
		}
		catch(iException::TotalDisaster)
		{
			cerr << "IFrIT experienced a \"Total Disaster\" failure." << endl;
		}

		return ret;
	}

	VTK_THREAD_RETURN_TYPE ThreadRunner(void *data)
	{
		vtkMultiThreader::ThreadInfo *ti = static_cast<vtkMultiThreader::ThreadInfo*>(data); IASSERT(ti);
		iShell *shell = static_cast<iShell*>(ti->UserData); IASSERT(shell);

		RunShell(shell);

		*(ti->ActiveFlag) = 0;

		return VTK_THREAD_RETURN_VALUE;
	}
};


using namespace iRunner_Private;
using namespace iParameter;




//
//  Main class
//
iRunner::iRunner()
{
	//
	//  Constructor is trivial
	//
	mShell = 0;

	mThreader = vtkMultiThreader::New();
	mThreadId = -1;

	//
	//  Check that sizes of basic types are correct
	//
	if(sizeof(int)!=4 || sizeof(float)!=4 || sizeof(double)!=8)
	{
		int sint = sizeof(int);
		int sflt = sizeof(float);
		int sdbl = sizeof(double);
		int sptr = sizeof(void*);
		int slng = sizeof(long);
		int svtk = sizeof(vtkIdType);
		int ssiz = sizeof(size_t);
		iOutput::Display(MessageType::Error,"IFrIT has not been ported to this machine.");
		exit(0);
	}
	//
	//  Check that vtkIdType can address a full pointer
	//
	if(sizeof(vtkIdType) < sizeof(void*))
	{
		iOutput::Display(MessageType::Error,"This machine is "+iString::FromNumber(8*int(sizeof(void*)))+"-bit, but VTK has been compiled with "+iString::FromNumber(8*int(sizeof(vtkIdType)))+"-bit ids.\n"
			"IFrIT will not be able to use more than 2GB of memory per single array.\n"
			"Just letting you know.\n");
	}
	if(sizeof(vtkIdType) > sizeof(void*))
	{
		iOutput::Display(MessageType::Error,"This machine is "+iString::FromNumber(8*int(sizeof(void*)))+"-bit, but VTK has been compiled with "+iString::FromNumber(8*int(sizeof(vtkIdType)))+"-bit ids.\n"
			"This configuration is inconsistent and is prone to crashes.\n"
			"Please recompile VTK with the advanced option VTK_USE_64BIT_IDS set to OFF.\n");
		exit(0);
	}
}


iRunner::~iRunner()
{
	if(mShell != 0)
	{
		iOutput::Display(MessageType::Error,"Invalid use of iRunner: class deleted before the launched shell is stopped; that causes a memory leak.");
	}
	if(mThreader != 0) mThreader->Delete();
}


bool iRunner::Launch(int argc, const char **argv)
{
	if(mShell != 0)
	{
		iOutput::Display(MessageType::Error,"Runner can run only one shell at a time.");
		return false;
	}

	//
	//  Create the shell object
	//
	mShell = iShell::New(this,argc,argv);
	if(mShell == 0)
	{
		iOutput::Display(MessageType::Error,"Invalid shell specification.");
		return false;
	}

	if(mShell->IsFailed())
	{
		iOutput::Display(MessageType::Error,"Shell failed to initialize.");
		return false;
	}

	if(mShell->PrintHelp())
	{
		mShell->Delete();
		mShell = 0;
		return false;
	}

	if(mShell->IsThreaded())
	{
		if(mThreader == 0)
		{
			iOutput::Display(MessageType::Error,"Threader failed to initialize.");
			return false;
		}
		mThreadId = mThreader->SpawnThread(ThreadRunner,mShell);
		if(mThreadId < 0)
		{
			iOutput::Display(MessageType::Error,"Unable to start a new thread. Without threads IFrIT cannot be run in the external mode.");
			mShell->Delete();
			mShell = 0;
			return false;
		}

		while(!mShell->IsRunning() && mThreader->IsThreadActive(mThreadId)!=0)
		{
			iSystem::Sleep(100);
		}

		if(mThreader->IsThreadActive(mThreadId) == 0)
		{
			mShell->Delete();
			mShell = 0;
			return false;
		}
	}
	else
	{
		if(!RunShell(mShell))
		{
			mShell = 0;
			return false;
		}
	}

	return true;
}


void iRunner::Stop()
{
	if(mShell == 0 )
	{
		iOutput::Display(MessageType::Error,"Cannot stop a shell that hasn't been launched.");
		return;
	}

	if(mShell->IsThreaded())
	{
		if(!mShell->IsStopped()) // in case the interpreter was terminated before the shell was killed
		{
			mShell->Stop();
			mThreader->TerminateThread(mThreadId);
		}
	}

	mShell->Delete();
	mShell = 0;
}


void iRunner::Run(int argc, const char **argv)
{
	if(this->Launch(argc,argv))
	{
		if(mShell->IsThreaded())
		{
			//
			//  For now do this...
			//
			iScript *script = iScript::New(mShell);
			if(script == 0)
			{
				this->Stop();
				iOutput::Display(MessageType::Error,"No script is available. Shell needs a script to run in the extending mode.");
				return;
			}

			cout << "Using scripting language: " << script->Name().ToCharPointer() << endl;

			if(!script->RunInterpreter())
			{
				iOutput::Display(MessageType::Error,"This script does not have a full command interpreter.");
			}

			script->Delete();
		}

		this->Stop();
	}
}

