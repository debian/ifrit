/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
// An object that controls materil for props 
//

#ifndef IMATERIAL_H
#define IMATERIAL_H


#include "iobject.h"


#include "iarray.h"

class iMaterialObject;

class vtkProperty;
class vtkVolumeProperty;


class iMaterial : public iObject
{

public:

	vtkTypeMacro(iMaterial,iObject);
	static iMaterial* New(iMaterialObject *parent = 0);

	iObjectPropertyMacro1S(ShadingMode,Int);
	iObjectPropertyMacro1S(Ambient,Float);
	iObjectPropertyMacro1S(Diffuse,Float);
	iObjectPropertyMacro1S(Specular,Float);
	iObjectPropertyMacro1S(SpecularPower,Float);

	void SetShading(bool s);

	void AddProperty(vtkProperty *prop);
	void AddProperty(vtkVolumeProperty *prop);
	
	void RemoveProperty(vtkProperty *prop);
	void RemoveProperty(vtkVolumeProperty *prop);

protected:
	
	virtual ~iMaterial();
	iMaterial* Self(){ return this; }  // needed to avoid compiler warning


private:

	iMaterial(iMaterialObject *parent, const iString &fname, const iString &sname);

	void UpdateProperties();

	iLookupArray<vtkProperty*> mActorProperties;
	iLookupArray<vtkVolumeProperty*> mVolumeProperties;
	iMaterialObject *mRealParent;
};

#endif // IMATERIAL_H

