/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ilookuptable.h"


#include "ierror.h"
#include "imath.h"
#include "ipalette.h"
#include "ipalettecollection.h"
#include "istretch.h"

#include <vtkMath.h>


//
//  Optimized vtkLookupTable
//
iLookupTable* iLookupTable::New()
{
	return new iLookupTable;
}


iLookupTable::iLookupTable()
{
	mPaletteId = 0;
}


iLookupTable::~iLookupTable()
{
}


void iLookupTable::SetPaletteId(int n)
{
	if(n != mPaletteId)
	{
		mPaletteId = n;
		this->Modified();
		this->Build();
	}
}


void iLookupTable::SetReversed(bool s)
{
	if(s && mPaletteId>0)
	{
		mPaletteId = -mPaletteId;
		this->Modified();
		this->Build();
	}
	if(!s && mPaletteId<0)
	{
		mPaletteId = -mPaletteId;
		this->Modified();
		this->Build();
	}
}


void iLookupTable::SetColor(const iColor &c)
{
	double *rgb = c.ToVTK();
	double hsv[3];
	vtkMath::RGBToHSV(rgb,hsv);
	this->SetHueRange(hsv[0],hsv[0]);
	this->SetSaturationRange(hsv[1],hsv[1]);
	this->SetValueRange(0.0,1.0);
	this->Modified();
	this->Build();
}


void iLookupTable::SetStretchId(int s)
{
	switch(s)
	{
	case iStretch::Log:
		{
			if(this->TableRange[0] < 1*iMath::_DoubleRes) this->TableRange[0] = 1*iMath::_DoubleRes;
			if(this->TableRange[1] < 2*iMath::_DoubleRes) this->TableRange[1] = 2*iMath::_DoubleRes;
			this->SetScaleToLog10();
			break;
		}
	default:
		{
			this->SetScaleToLinear();
		}
	}
	this->Modified();
}


void iLookupTable::ForceBuild()
{
	vtkLookupTable *lut = iPaletteCollection::Global()->GetLookupTable(mPaletteId);

	if(lut != 0)
	{
		this->Table->Initialize();
		this->Table->SetArray(lut->GetPointer(0),4*lut->GetNumberOfTableValues(),1);
		this->BuildTime.Modified();
	}
	else
	{
		this->Table->Initialize();
		vtkLookupTable::ForceBuild();
	}
}


unsigned long iLookupTable::GetMTime()
{
	vtkLookupTable *lut = iPaletteCollection::Global()->GetLookupTable(mPaletteId);
	if(lut != 0)
	{
		if(lut->GetMTime() > this->vtkObject::GetMTime()) this->Modified();
	}

	return this->vtkObject::GetMTime();
}


void iLookupTable::MapScalarsThroughTable2(void *input, unsigned char *output, int inputDataType, int numberOfValues, int inputIncrement, int outputFormat)
{
	//
    // This is how this function is used.
	//
	//this->MapScalarsThroughTable2(scalars->GetVoidPointer(comp), 
    //                              newColors->GetPointer(0),
    //                              scalars->GetDataType(),
    //                              scalars->GetNumberOfTuples(),
    //                              scalars->GetNumberOfComponents(), 
    //                              VTK_RGBA);

	this->vtkLookupTable::MapScalarsThroughTable2(input,output,inputDataType,numberOfValues,inputIncrement,outputFormat);
}

