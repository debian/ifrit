/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IVOLUMEVIEWSUBJECT_H
#define IVOLUMEVIEWSUBJECT_H


#include "iviewsubject.h"


#include "iarray.h"
#include "icolor.h"


class iPiecewiseFunction;
class iReplicatedGridData;
class iReplicatedVolume;
class iViewObject;
class iVolumeDataConverter;
class iVolumeViewSubject;
class iVolumeViewInstance;

class vtkImageData;
class vtkVolumeMapper;


//
//  Helper class
//
class iVolumeRenderingHelper
{

	friend class iVolumeViewInstance;

public:

	virtual bool IsUsingData(vtkDataSet *ds) = 0;
	virtual vtkVolumeMapper* GetMapper() = 0;

	virtual void Reset(){}

	virtual void SetVar(int){}
	virtual void SetBlendMode(int){}

	virtual float GetMemorySize() { return 0.0f; }

	void SetDataSpacing(double v);
	void SetImageDownsampleFactor(float v);
	void SetDepthDownsampleFactor(float v);

	inline bool IsAttachedDirectly() const { return mAttachedDirectly; }
	inline const iString& GetName() const { return mName; }
	
protected:
	
	iVolumeRenderingHelper(iVolumeViewInstance *parent, const iString &name, bool attachedDirectly);
	virtual ~iVolumeRenderingHelper();

	virtual void AdjustSpacing(){}

	double mDataSpacing;
	float mImageDownsampleFactor, mDepthDownsampleFactor;
	iVolumeViewInstance *mParent;

private:

	const bool mAttachedDirectly;
	const iString mName;

	iVolumeRenderingHelper();  // not implemented
};


class iVolumeViewInstance : public iViewInstance
{
	
	friend class iVolumeViewSubject;

public:
	
	iDataConsumerTypeMacro(iVolumeViewInstance,iViewInstance);
		
	iObjectSlotMacro1SV(Var,int);
	iObjectSlotMacro2S(Method,const iString&);
	iObjectSlotMacro1SV(MethodId,int);
	iObjectSlotMacro1SV(Palette,int);
	iObjectSlotMacro1SV(BlendMode,int);
	iObjectSlotMacro1SV(InterpolationType,int);
	iObjectSlotMacro1SV(OpacityScaleFactor,float);
	iObjectSlotMacro1SV(ImageDownsampleFactor,float);
	iObjectSlotMacro1SV(DepthDownsampleFactor,float);

	inline iReplicatedVolume* GetVolume() const { return mVolume; }
	inline iPiecewiseFunction* GetOpacityFunction() const { return mOpacityFunction; }
	
	const iString& GetMethodName(int i) const;
	bool IsMethodAvailable(int i) const;

protected:
	
	iVolumeViewInstance(iVolumeViewSubject *owner);
	virtual ~iVolumeViewInstance();
	virtual void ConfigureBody();
	virtual void FinishInitialization();
	
	virtual float GetMemorySize();

	virtual void ResetBody();
	virtual void ShowBody(bool s);
	virtual bool CanBeShown() const;

	virtual void UpdateColorBars();

	virtual void UpdateIndex();

	//
	//  VTK members
	//
	iReplicatedVolume *mVolume;
	iReplicatedGridData *mDataReplicated;
	iVolumeDataConverter *mDataConverter;
	iArray<iVolumeRenderingHelper*> mHelpers;
	iPiecewiseFunction *mOpacityFunction;
};


class iVolumeViewSubject : public iViewSubject
{
	
public:
	
	iViewSubjectTypeMacro(iVolumeView,iViewSubject);
	
	iType::vsp_int Var;
	iType::vsp_string Method;
	iType::vsp_int MethodId;
	iType::vsp_int Palette;
	iType::vsp_int BlendMode;
	iType::vsp_int InterpolationType;
	iType::vsp_float OpacityScaleFactor;
	iType::vsp_float ImageDownsampleFactor;
	iType::vsp_float DepthDownsampleFactor;

	iPropertyPiecewiseFunction OpacityFunction;

	iObjectPropertyMacro2Q(AllMethods,String);

	const iString& GetMethodName(int i) const;
	bool IsMethodAvailable(int i) const;

	static void SetGPUMode(int m);
	static int GPUMode(){ return mGPUMode; }

protected:
	
	iVolumeViewSubject(iViewObject *parent, const iDataType &type);
	virtual ~iVolumeViewSubject();

	virtual void InitInstances();

	virtual void UpdateAutomaticShadingBody();

	static int mGPUMode;
	mutable iString mMethodsCache;
};

#endif // IVOLUMEVIEWSUBJECT_H
 
