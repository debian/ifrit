/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A stereo (two-eyed) image
//
#ifndef ISTEREOIMAGE_H
#define ISTEREOIMAGE_H


#include "iimage.h"


class iStereoImage
{

public:

	iStereoImage();
	iStereoImage(const iImage &img);
	iStereoImage(const iStereoImage &img);

	const iImage& Eye(int n) const;
	const iImage& LeftEye() const;
	const iImage& RightEye() const;

    iStereoImage& operator=(const iStereoImage &img);   // impl-shared copy
    iStereoImage& operator=(const iImage &img);   // impl-shared copy
	void CopyLeftEyeToRightEye();
	void SetStereo(bool s);

	//
	//  iImage functions
	//
	void Scale(int w, int h, iImage::ScaleMode m = iImage::_Both, iImage::ScaleFilter f = iImage::_None);
//	void Smooth(float dx, float dy);
//	void Clear();
	void ReplaceData(int n, vtkImageData *data);
	void Blend(const iImage& im, float op);
	void Blend(const iStereoImage& im, float op);
	void Overlay(int x, int y, const iImage& ovr, float opacity = 1.0, bool masking = true);
	void Overlay(int x, int y, const iStereoImage& ovr, float opacity = 1.0, bool masking = true);
	bool CombineInPseudoColor(const iStereoImage &imRed, const iStereoImage &imGreen, const iStereoImage &imBlue);
	void DrawFrame(int width, const iColor &color);

	inline bool IsStereo() const { return mIsStereo; }
	inline bool IsEmpty() const { return mEyeImage[0].IsEmpty(); }
	inline int Depth() const { return mEyeImage[0].Depth(); }
	inline int Width() const { return mEyeImage[0].Width(); }
	inline int Height() const { return mEyeImage[0].Height(); }

	void SetTag(const iString& tag);
	inline const iString& GetTag() const { return mTag; }

private:

	iImage mEyeImage[2];
	bool mIsStereo;
	iString mTag;
};


inline const iImage& iStereoImage::Eye(int n) const
{ 
	return mEyeImage[(n==0 || !mIsStereo)?0:1]; 
}


inline const iImage& iStereoImage::LeftEye() const
{
	return mEyeImage[0];
}


inline const iImage& iStereoImage::RightEye() const
{
	return mEyeImage[mIsStereo?1:0];
}

#endif // ISTEREOIMAGE_H

