/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A ViewSubject that uses scalar data and has several instances
//
#ifndef IMULTIVIEWSUBJECT_H
#define IMULTIVIEWSUBJECT_H


#include "ipaintedviewsubject.h"


class iMultiViewSubject;


class iMultiViewInstance : public iPaintedViewInstance
{
	
	friend class iMultiViewSubject;

public:
	
	iDataConsumerTypeMacroPass(iMultiViewInstance,iPaintedViewInstance);

protected:
	
	iMultiViewInstance(iMultiViewSubject *owner, int numactors, int numpaintedactors, int priority, bool alwayspainted);
	virtual ~iMultiViewInstance();

	virtual bool IsConnectedToPaintingData() const;
};


class iMultiViewSubject : public iPaintedViewSubject
{
	
public:
	
	iDataConsumerTypeMacroPass(iMultiViewSubject,iPaintedViewSubject);

	iType::ps_int Number;
	iPropertyAction Create;
	iPropertyAction1<iType::Int> Remove;

protected:
	
	iMultiViewSubject(iObject *parent, const iString &fname, const iString &sname, iViewModule *vm, const iDataType &type, unsigned int flags, int minsize);
	virtual ~iMultiViewSubject();
};

#endif // IMULTIVIEWSUBJECT_H

