/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IVIEWSUBJECTOBSERVER_H
#define IVIEWSUBJECTOBSERVER_H


#include <vtkCommand.h>


#include <vtkSetGet.h>

class iDataType;
class iString;
class iViewSubject;

//
//  This observer does not execute anything - it is just a place holder for object info
//
class iViewSubjectObserver : public vtkCommand
{

public:
	
	vtkTypeMacro(iViewSubjectObserver,vtkCommand);
	static iViewSubjectObserver* New(iViewSubject *subject = 0);

	iViewSubject* GetSubject() const { return mSubject; }
	const iString& GetSubjectName() const;
	const iDataType& GetSubjectDataType() const;

protected:

	virtual ~iViewSubjectObserver();

private:

	iViewSubjectObserver(iViewSubject *subject);

	virtual void Execute(vtkObject *, unsigned long, void *){}

	iViewSubject *mSubject;
};


#endif // IVIEWOBJECTOBSERVER_H

