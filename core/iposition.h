/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A double-valued position
//

#ifndef IPOSITION_H
#define IPOSITION_H


#include "iviewmodulecomponent.h"


#include "ivector3D.h"


namespace iParameter
{
	namespace SpecialLocation
	{
		const int FIRST__ =			-2;
		const int PickedPoint =		-2;
		const int FocalPoint =		-1;
		const int BoxCenter =		0;
	};
};


class iCoordinate : public iViewModuleComponent
{

public:

	iCoordinate(iViewModule *vm);

	inline double BoxValue() const { this->UpdateBoxValue(); return mBoxValue; }
	inline operator double() const { return mOGLValue; } 
	inline double OpenGLValue() const { return mOGLValue; } 

	void SetBoxValue(double x);
	void operator=(const iCoordinate&);
	void operator=(double);

	inline bool operator<(const iCoordinate &p) const { return mOGLValue < p.mOGLValue; }
	inline bool operator>(const iCoordinate &p) const { return mOGLValue > p.mOGLValue; }
	inline bool operator<(double x) const { return mOGLValue < x; }
	inline bool operator>(double x) const { return mOGLValue > x; }

	void PeriodicWrap();
	void CutToBounds();

protected:
	
	iCoordinate(iViewModule *vm, int offset);

	void UpdateOGLValue();
	void UpdateBoxValue() const;

	mutable double mBoxValue;
	double mOGLValue;
	const int mOffset;

private:

	iCoordinate(const iCoordinate&); // not implemented
};


class iDistance : public iCoordinate
{

public:

	iDistance(iViewModule *vm);

	void SetBoxValue(double x);
	void operator=(const iDistance&);
	void operator=(double);

private:
	
	void PeriodicWrap();

	iDistance(const iDistance&); // not implemented
};


class iPosition : public iViewModuleComponent
{

public:

	iPosition(iViewModule *vm);

	inline const iVector3D& BoxValue() const { this->UpdateBoxValue(); return mBoxValue; }
	inline double BoxValue(int i) const { this->UpdateBoxValue(); return mBoxValue[i]; }

	inline operator double*(){ return mOGLValue; }
	inline operator const double*() const { return mOGLValue; }
 
	inline double operator[](int i) const { return mOGLValue[i]; }
	inline double& operator[](int i){ return mOGLValue[i]; }

	inline const iVector3D& OpenGLValue() const { return mOGLValue; }
	inline double OpenGLValue(int i) const { return mOGLValue[i]; }

	bool SetToSpecialLocation(int v);

	void SetBoxValue(const iVector3D &v);
	void SetBoxValue(int i, double x);
	
	void operator=(const iPosition&);
	void operator=(const double *);
 
 	void PeriodicWrap();
 	void CutToBounds();
 
 private:
 	
	void UpdateOGLValue();
	void UpdateBoxValue() const;

	mutable iVector3D mBoxValue;
	iVector3D mOGLValue;

	iPosition(const iPosition&); // not implemented
};


//
//  iCoordinate functions
//
inline void iCoordinate::SetBoxValue(double x)
{
	mBoxValue = x;
	this->UpdateOGLValue();
}


inline void iCoordinate::operator=(const iCoordinate &p)
{
	mOGLValue = p.mOGLValue;
}


inline void iCoordinate::operator=(double s)
{
	mOGLValue = s;
}


//
//  iDistance functions
//
inline void iDistance::SetBoxValue(double x)
{
	if(x > 0.0)
	{
		mBoxValue = x;
		this->UpdateOGLValue();
	}
}


inline void iDistance::operator=(const iDistance &p)
{
	if(p.mOGLValue > 0.0) mOGLValue = p.mOGLValue;
}


inline void iDistance::operator=(double s)
{
	if(s > 0.0) mOGLValue = s;
}


//
//  iPosition functions
//
inline void iPosition::SetBoxValue(const iVector3D &v)
{
	mBoxValue = v;
	this->UpdateOGLValue();
}


inline void iPosition::SetBoxValue(int i, double x)
{
	if(i>=0 && i<3)
 	{
		this->UpdateBoxValue(); // needed for out-of-date box values at other indicies not to propagate into OGL values
		mBoxValue[i] = x;
		this->UpdateOGLValue();
	}
}


inline void iPosition::operator=(const iPosition &p)
{
	mOGLValue = p.mOGLValue;
}


inline void iPosition::operator=(const double *p)
{
	mOGLValue = p;
}

#endif  // IPOSITION_H

