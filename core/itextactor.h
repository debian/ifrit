/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef ITEXTACTOR_H
#define ITEXTACTOR_H


#include "igenericprop.h"
#include <vtkProp.h>


#include "istring.h"
#include "ipointermacro.h"

class iOverlayHelper;
class iRenderTool;
class iTextSubject;

class vtkTextProperty;


class iTextActor: public iGenericProp<vtkProp>
{

	IPOINTER_AS_PART(RenderTool);
	IPOINTER_AS_USER(OverlayHelper);

public:

	vtkTypeMacro(iTextActor,vtkProp);
	static iTextActor* New(iRenderTool *rv = 0);
	
	void SetBold(bool s);

	void SetJustification(int jx);
	void SetJustification(int jx, int jy);

	void MoveJustification(int jx);

	void SetPosition(float x, float y);
	inline const float* GetPosition() const { return mPos; }
	
	void SetAngle(float a);
	inline float GetAngle() const { return mAngle; }

	void SetText(const iString &s);
	inline const iString& GetText() const { return mText; }

	void GetSize(vtkViewport *vp, float s[2]) const;
	void GetBounds(vtkViewport *vp, float b[4]) const;
	void GetTextAnchor(vtkViewport *vp, float p[2]) const;

	vtkTextProperty* GetTextProperty() const { return mProperty; }

protected:
	
	iTextActor(iRenderTool *rv);
	virtual ~iTextActor();

	virtual void UpdateGeometry(vtkViewport *vp);

	void SetPadding(float p);

private:

	int mCurrentSubject;
	float mPos[2], mAngle, mPadding;
	iString mText;

	vtkTextProperty *mProperty;
	iTextSubject* mSubjects[2];
};

#endif // ITEXTACTOR_H
