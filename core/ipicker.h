/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IPICKER_H
#define IPICKER_H


#include "iextendableobject.h"
#include "iviewmodulecomponent.h"


#include "iposition.h"

class iActor;
class iDataFormatter;
class iDataType;

class vtkPicker;
class vtkPolyData;
class vtkSphereSource;


namespace iParameter
{
	namespace PickMethod
	{
		//
		// Picker parameters
		//
		const int Cell =		0;
		const int Point =		1;
		const int Object =		2;
	};
};


class iPicker : public iExtendableObject, public iViewModuleComponent
{

	IPOINTER_AS_USER(DataFormatter);

public:

	vtkTypeMacro(iPicker,iExtendableObject);
	static iPicker* New(iViewModule *vm = 0);

	iObjectPropertyMacro1S(Accuracy,Float);
	iObjectPropertyMacro1S(PickMethod,Int);
	iObjectPropertyMacro1S(PointSize,Float);
	iPropertyQuery<iType::Vector> Position;

	inline const iPosition& GetPosition() const { return mPosition; }
	inline const iVector3D& GetBoxPosition() const { return mPosition.BoxValue(); }
	inline const iString& GetObjectName() const { return mSubjectName; }

	void ShowPickedPoint(bool s);

	inline vtkPicker* GetHandler() const { return mHandler; }

	void UpdateReport();
	virtual void Modified();

protected:

	iPicker(iViewModule *vm, const iString &fname, const iString &sname);
	virtual ~iPicker();

	virtual void UpdateReportBody();

private:

	iPosition mPosition;
	iString mSubjectName;
	const iDataType *mDataTypePointer;
	vtkPolyData *mProbeInput;

	//
	//  Actual workers
	//
	vtkPicker *mHandler;

	//
	//  Graphical representation
	//
	iActor *mPointActor;
	vtkSphereSource *mPointSource;
};


class iPickerExtension : public iObjectExtension
{

	iObjectExtensionDeclareAbstractMacro(iPicker);

public:

	virtual bool IsUsingData(const iDataType &type) const = 0;
	virtual void ModifyActor(const iDataType &type, iActor *actor) = 0;
	virtual void AddInfo(const iDataType &type) = 0;
};

#endif  // IPICKER_H
