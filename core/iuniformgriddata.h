/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IUNIFORMGRIDDATA_H
#define IUNIFORMGRIDDATA_H


#include <vtkImageData.h>


class iUniformGridFileLoader;


namespace iParameter
{
	namespace VoxelLocation
	{
		const int Vertex = 0;
		const int Center = 1;

		inline bool IsValid(int m){ return m>=0 && m<=1; }
	};
};


class iUniformGridData : public vtkImageData
{

	friend class iUniformGridFileLoader;

public:
	
	vtkTypeMacro(iUniformGridData,vtkImageData);
	static iUniformGridData* New();

	//
	//  VTK methods
	//
	virtual void Initialize();
	virtual void ShallowCopy(vtkDataObject *d);
	virtual void DeepCopy(vtkDataObject *d);

	//
	//  Special methods
	//
	inline int GetVoxelLocation() const { return mVoxelLocation; }
	void GetPeriodicOffsets(int perOff[3]) const;

	void GetNumCells(int numCells[3]);
	void GetBounds(double min[3], double max[3]);
	void GetGlobalOrigin(double org[3]);

	//
	//  Just keep them here because they should be somewhere
	//
	static void CIC(bool periodic[3], int dims[3], double org[3], double spa[3],  float pos[3], int ijk1[3], int ijk2[3], double d1[3], double d2[3]);
	static void CIC(bool periodic[3], int dims[3], double org[3], double spa[3], double pos[3], int ijk1[3], int ijk2[3], double d1[3], double d2[3]);

protected:
	
	iUniformGridData();
	virtual ~iUniformGridData();

private:

	void SetInternalProperties(int voxLoc, int fileDims[3], int dataDims[3]);

	int mVoxelLocation;
	int mPeriodicOffsets[3];
};

#endif // IUNIFORMGRIDDATA_H

