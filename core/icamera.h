/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  This class is a merger of iObject with vtkCamera class. But since we cannot inherit, we must decorate
//
#ifndef ICAMERA_H
#define ICAMERA_H


#include "iobject.h"
#include "iviewmodulecomponent.h"


#include "iposition.h"
#include "ipropertyposition.h"

class iEventObserver;
class iRenderTool;

class vtkCamera;


class iCamera : public iObject, public iViewModuleComponent
{

	friend class iExtensionFactory;

public:

	vtkTypeMacro(iCamera,iObject);
	static iCamera* New(iRenderTool *rt = 0);

	iObjectSlotMacro2S(Position,const iPosition&);
	iObjectSlotMacro2S(FocalPoint,const iPosition&);
	iPropertyScalarPosition Position, FocalPoint;

	//
	//  Camera orientation
	//
	iObjectPropertyMacro2S(ViewUp,Vector);
	iObjectPropertyMacro2S(ClippingRange,Pair);
	iObjectPropertyMacro2S(ParallelProjection,Bool);
	iObjectPropertyMacro2S(ParallelScale,Double);
	iObjectPropertyMacro2S(ViewAngle,Double);
	iObjectPropertyMacro2S(ViewAngleVertical,Bool);
	iObjectPropertyMacro2S(EyeAngle,Double);
	iObjectPropertyMacro2S(ClippingRangeAuto,Bool);

	//
	//  actions
	//
	iObjectPropertyMacroA1(Azimuth,Double);
	iObjectPropertyMacroA1(Elevation,Double);
	iObjectPropertyMacroA1(Yaw,Double);
	iObjectPropertyMacroA1(Pitch,Double);
	iObjectPropertyMacroA1(Roll,Double);
	iObjectPropertyMacroA1(Zoom,Double);
	iObjectPropertyMacroA(ResetCamera);
	iObjectPropertyMacroA(OrthogonalizeCameraView);
	
	bool Reset();
	bool OrthogonalizeView();
	void ShiftTo(const iPosition& pos);

	vtkCamera* GetDevice() const { return mDevice; }

protected:

	virtual ~iCamera();

private:

	iCamera(iRenderTool *rt, const iString &fname, const iString &sname);

	void SyncProjections();

	bool mSyncProjections;
	vtkCamera *mDevice;
	iRenderTool *mRenderTool;

	mutable iVector3D mViewUp;
	mutable iPair mClippingRange;
	mutable iPosition mPosition, mFocalPoint;
};

#endif

