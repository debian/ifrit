/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef ILIGHTS_H
#define ILIGHTS_H


#include <vtkLightKit.h>
#include "iobject.h"
#include "iviewmodulecomponent.h"


class iLights;

class vtkRenderer;


class iLightKit : public vtkLightKit
{

	friend class iLights;

public:

	void SetAngles(double a[2]);
	inline void GetAngles(double a[2]) const { a[0] = mAngles[0]; a[1] = mAngles[1]; }

	void SetIntensities(float i[3]);
	void GetIntensities(float i[3]) const;

protected:

	virtual ~iLightKit();

private:

	iLightKit(vtkRenderer *renderer);

	void UpdateAngles();

	//
	//  Hide setting directions for individual lights
	//
	void SetKeyLightAngle(double elevation, double azimuth){}
	void SetKeyLightAngle(double angle[2]){}
	void SetKeyLightElevation(double x){}
	void SetKeyLightAzimuth(double x){}

	void SetFillLightAngle(double elevation, double azimuth){}
	void SetFillLightAngle(double angle[2]){}
	void SetFillLightElevation(double x){}
	void SetFillLightAzimuth(double x){}

	void SetBackLightAngle(double elevation, double azimuth){}
	void SetBackLightAngle(double angle[2]){}
	void SetBackLightElevation(double x){}
	void SetBackLightAzimuth(double x){}

	double mAngles[2];
};


class iLights : public iObject, public iViewModuleComponent
{
	
public:

	vtkTypeMacro(iLights,iObject);
	static iLights* New(iViewModule *vm = 0);

	iObjectPropertyMacro1S(Direction,Vector);
	iObjectPropertyMacro2V(Intensities,Float);
	iObjectPropertyMacro2S(TwoSided,Bool);

	inline iLightKit* GetLightKit() const { return mLightKit; }

protected:

	virtual ~iLights();

private:
	
	iLights(iViewModule *vm, const iString &fname, const iString &sname);

	iLightKit *mLightKit;
};

#endif  // ILIGHTS_H
