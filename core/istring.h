/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef ISTRING_H
#define ISTRING_H


#include <string.h>


class iCharArray;


class iString 
{

public:

	const static int MaxLength;

	iString(const char *s = 0, int l = -1);
	iString(const iString &s);
	~iString();

	//
	//  Operators
	//
    iString& operator=(const iString &str);   // impl-shared copy
    iString& operator+=(const iString &str);
    char operator[](int i) const;

    bool operator==(const iString &str) const;
    bool operator!=(const iString &str) const;
    bool operator<(const iString &str) const;
    bool operator>(const iString &str) const;

	//
	//  Return as char*
	//
    const char* ToCharPointer(int i = 0) const;

	//
	//  Useful functions
	//
    bool IsEmpty() const;
    int Length() const;
	void Clear();  // equivalent to assigning "" to the string, but faster
	void Init(int size); // clears the string and resizes to size

	//
	//  Case changing
	//
	iString Lower() const;
	iString Upper() const;

	//
	//  Finding & replacing substrings
	//
	int Contains(char c, int index = 0) const;
	int Contains(const char *str, int index = 0) const;
  	int Contains(const iString &str, int index = 0) const;
    int Find(char c, int index = 0) const;
    int Find(const char *str, int index = 0) const;
    int Find(const iString &str, int index = 0) const;  // inlined below
    int FindLast(char c) const;
    int FindIgnoringWhiteSpace(const iString &str, int index = 0, int *last = 0) const;
	int FindFromSet(const char *set, int index = 0, int *length = 0) const;
	void Replace(int index, char c);
	void Replace(int index, int len, const iString &str);
	void Replace(const iString &tar, const iString &str, int index = 0, bool once = false); 
	iString Substitute(const iString &tar, const iString &str) const;
	bool IsAt(const iString &str, int index) const;
	bool BeginsWith(const iString &str) const;
	bool EndsWith(const iString &str) const;
	bool IsWhiteSpace(int i) const;

	//
	//  Getting parts of the string
	//
	iString Section(const iString &str, int start, int end = MaxLength) const;
	iString Section(const char *s, int start, int end = MaxLength) const; // inlined below
    iString Part(int index, int len = MaxLength) const;

	//
	//  Number conversions
	//
    static iString FromNumber(int n, const char *format = "%d");
    static iString FromNumber(long n, const char *format = "%ld");
    static iString FromNumber(float n, const char *format = "%g");
    static iString FromNumber(double n, const char *format = "%lg");
    static iString FromNumber(size_t n, const char *format = "%ld");
    static iString FromNumber(void *n);
    static iString FromNumber(bool n);

#ifndef I_NO_LONG_LONG
	// 
	//  Not all compilers may support long long type...
	//
	static iString FromNumber(long long n, const char *format = "%lld");
#endif

	int ToInt(bool &ok) const;
	float ToFloat(bool &ok) const;
	double ToDouble(bool &ok) const;
	void* ToPointer(bool &ok) const;

	//
	//  Using strings as buffers
	//
	char* GetWritePointer(int len);

	//
	//  Formatting functions
	//
	void ReduceWhiteSpace(bool preserve_quotes = false);
	void RemoveWhiteSpace(bool preserve_quotes = false);
	void RemoveFormatting(const iString &tokBeg, const iString &tokEnd, bool multiLevel = false);
	void ReformatHTMLToText(int length = 0);
	void FindTokenMatch(const iString &tokBeg, int &indBeg, const iString &tokEnd, int &indEnd, int index = 0, bool multiLevel = false) const;

	//
	//  Misc functions
	//
	static iString Whitespace(int len);

private:

#ifdef I_DEBUG
	char *Ptr;
#endif
	iCharArray *mArr;
	int mLen;
};


inline const iString operator+(const iString &s1, const iString &s2)
{
    iString tmp(s1);
    tmp += s2;
    return tmp;
}


inline int iString::Length() const
{
	return mLen;
}


inline bool iString::IsEmpty() const
{
	return mLen == 0;
}


inline int iString::Contains(const iString &str, int index) const 
{ 
	return this->Contains(str.ToCharPointer(),index); 
}


inline int iString::Find(const iString &str, int index) const 
{ 
	return this->Find(str.ToCharPointer(),index); 
}


inline void iString::Replace(const iString &tar, const iString &str, int index, bool once)
{
	int i;

	if(tar == str) return;

	if(once)
	{
		if((i=this->Find(tar,index)) > -1) this->Replace(i,tar.mLen,str);
	}
	else
	{
		while((i=this->Find(tar,index)) > -1) this->Replace(i,tar.mLen,str);
	}
}


inline iString iString::Substitute(const iString &tar, const iString &str) const
{
	iString s = *this;
	s.Replace(tar,str);
	return s;
}


inline iString iString::Section(const char *s, int start, int end) const
{
	return this->Section(iString(s),start,end);
}


inline bool iString::operator!=(const iString &str) const
{
	return !(*this==str);
}


inline bool iString::IsWhiteSpace(int i) const
{
	if(i>=0 && i<mLen)
	{
		char p = this->operator[](i);
		return p==32 || (p>=9 && p<=13);
	}
	else return false;
}

#endif // ISTRING_H
 

