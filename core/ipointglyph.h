/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A representation for a single particle
//  

#ifndef IPOINTGLYPH_H
#define IPOINTGLYPH_H


#include <vtkPolyDataAlgorithm.h>


class iDataConsumer;


namespace iParameter
{
	namespace PointGlyphType
	{
		const int Point =			0;
		const int Sphere =			1;
		const int Tetrahydron =		2;
		const int Cube =			3;
		const int Octahedron =		4;
		const int Icosahedron =		5;
		const int Dodecahedron =	6;
		const int Cone =			7;
		const int Cylinder =		8;
		const int Arrow =			9;
		const int Cluster =			10;
		const int Galaxy =			11;
		
		const int SIZE =			12;
	};
};


class iPointGlyph: public vtkPolyDataAlgorithm
{

public:

	vtkTypeMacro(iPointGlyph,vtkPolyDataAlgorithm);
	static iPointGlyph* New();
	
	inline int GetType() const { return mType; }
	void SetType(int m);

	bool GetOriented() const;
	const char* GetName() const;
	
	static bool GetOriented(int t);
	static const char* GetName(int t);

	vtkPolyData* GetData();

protected:
	
	iPointGlyph();
	virtual ~iPointGlyph();
	
	virtual int	RequestData(vtkInformation *request, vtkInformationVector **inputVector, vtkInformationVector *outputVector);

private:

	void CreateData(int res);
	void ScaleData(vtkPolyData *d, double fac);
	void SwapAxes(vtkPolyData *d, int a1, int a2);
	void ShiftData(vtkPolyData *d, int a, double dx);

	int mType;
	vtkPolyData *mData[iParameter::PointGlyphType::SIZE];

	//
	//  Block for better control
	//
	void GetOutput();
	void OutputData();
};

#endif // IPOINTGLYPH_H


