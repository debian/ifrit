/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  FileFileLoader(s) for core IFrIT DataType(s)
//
#ifndef ICOREFILELOADERS_H
#define ICOREFILELOADERS_H


#include "iuniformgridfileloader.h"
#include "iparticlefileloader.h"


#include "iarray.h"
#include "ibuffer.h"

#include <vtkObject.h>

class iDataReader;

class vtkImageData;


namespace iCoreData
{
	class ScalarHelper;
	class VectorFileLoader : public iUniformGridFileLoader
	{

		friend class ::iDataReader;
		friend class ScalarHelper;

	public:

		vtkTypeMacro(VectorFileLoader,iUniformGridFileLoader);

	protected:

		VectorFileLoader(iDataReader *r);

		virtual int GetNumComponents(int nvars) const;
		virtual void AssignBinData(int ncoms, vtkIdType ntot, int com, float *d);
		virtual void AssignTxtData(int ncoms, vtkIdType ind, float *f);
		virtual void AttachBuffer(vtkImageData *data);
	};


	class TensorFileLoader : public iUniformGridFileLoader
	{

		friend class ::iDataReader;

	public:

		vtkTypeMacro(TensorFileLoader,iUniformGridFileLoader);

	protected:

		TensorFileLoader(iDataReader *r);

		virtual int GetNumComponents(int nvars) const;
		virtual void AssignBinData(int ncoms, vtkIdType ntot, int com, float *d);
		virtual void AssignTxtData(int ncoms, vtkIdType ind, float *f);
		virtual void AttachBuffer(vtkImageData *data);
	};


	class ScalarFileLoader : public iUniformGridFileLoader
	{

		friend class ::iDataReader;
		friend class ScalarHelper;

	public:

		vtkTypeMacro(ScalarFileLoader,iUniformGridFileLoader);

		void SetCalculatorExpression(const iString &s);
		inline const iString& GetCalculatorExpression() const { return mCalculatorExpression; }

		void SetCalculatorOutput(int n);
		inline int GetCalculatorOutput() const { return mCalculatorOutput; }

		bool Smooth(int var, float r);

	protected:

		ScalarFileLoader(iDataReader *r, VectorFileLoader *vs);
		virtual ~ScalarFileLoader();

		virtual int GetNumComponents(int nvars) const;
		virtual void AssignBinData(int ncoms, vtkIdType ntot, int com, float *d);
		virtual void AssignTxtData(int ncoms, vtkIdType ind, float *f);
		virtual void AttachBuffer(vtkImageData *data);

	private:

		iString mCalculatorExpression;
		int mCalculatorOutput;

		VectorFileLoader *mVectorFileLoader;
		ScalarHelper *mHelper2;
	};


	class ParticleFileLoader : public iParticleFileLoader
	{

		friend class ::iDataReader;

	public:

		vtkTypeMacro(ParticleFileLoader,iParticleFileLoader);

	protected:

		ParticleFileLoader(iDataReader *r);
		virtual ~ParticleFileLoader();

		virtual void LoadParticleFileBody(const iString &suffix, const iString &fname);

		virtual bool ReadBinFileHeader(iFile &F, vtkIdType &ntot0, float *ll, float *ur, bool &paf, int &nvar);
		virtual bool ReadTxtFileHeader(iFile &F, vtkIdType &ntot0, float *ll, float *ur, bool &paf, int &nvar, iString &buffer);
		virtual bool ReadBinFileContents(iFile &F, bool paf, float *scaleF, float *offsetF, double *scaleD, double *offsetD);
		virtual bool ReadTxtFileContents(iFile &F, bool paf, float *scaleF, float *offsetF, double *scaleD, double *offsetD, iString buffer);

	private:

		ScalarHelper *mHelper2;
	};
};

#endif

