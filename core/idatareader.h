/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IDATAREADER_H
#define IDATAREADER_H


#include "iextendableobject.h"
#include "iviewmodulecomponent.h"


#include "iarray.h"

class iDataInfo;
class iDataLimits;
class iDataReaderHelper;
class iDataSubject;
class iDataType;
class iDirectory;
class iFile;
class iFileLoader;

namespace iCoreData
{
	class ScalarFileLoader;
	class VectorFileLoader;
	class TensorFileLoader;
	class ParticleFileLoader;
};

class vtkDataSet;
class vtkPolyData;
class vtkImageData;


class iDataReader : public iExtendableObject, public iViewModuleComponent
{

	friend class iDataReaderHelper;

public:

	struct VisitedFile
	{
		iString Name;
		const iFileLoader *Loader;
	};

	vtkTypeMacro(iDataReader,iExtendableObject);
	static iDataReader* New(iViewModule *vm = 0);
	
	//
	//  Non-static members
	//
	iObjectPropertyMacro1S(BoundaryConditions,Int);
	iObjectPropertyMacro1S(ScaledDimension,Int);
	iObjectPropertyMacro1S(VoxelLocation,Int);
	iObjectPropertyMacro1S(MaxZDimension,Int);
	iObjectPropertyMacro1V(Shift,Double,3); 

	iObjectSlotMacroA2(Load,const iDataType&,const iString&);
	iPropertyAction2<iType::DataType,iType::String> Load;
	iObjectSlotMacroA1(Erase,const iDataType&);
	iPropertyAction1<iType::DataType> Erase;
	iObjectPropertyMacroA(Reload);  // reload requested data

	inline bool IsSet() const { return mSetLoaders.Size() > 0; }

	//
	//  Loading of data
	//
	void LoadFile(const iDataType &type, const iString &fname, bool savename = true);
    bool LoadRecord(int rec, int skip = 0, bool savename = false);  //  return false if the record is missing, true otherwise (even in case of an error)

	//
	//  Operation on data
	//
	bool IsThereData(const iDataType &type) const;
	bool IsThereData(const iDataInfo &info) const;

	void EraseData(const iDataType &type);
	void ReloadData(const iDataType &type);
	void RequestDataReload(const iDataInfo &info);
	bool HasDataToReload() const;

	void SetTwoCopies(bool s);

	void ResetPipeline();

	iDataSubject* GetSubject(const iDataType &type) const;
	iDataLimits* GetLimits(const iDataType &type) const;

	//
	//  Generic GetData
	//
	vtkDataSet* GetData(const iDataType &type) const;

	inline bool IsFileAnimatable() const { return (mCurrentRecord >= 0); }
	inline int GetRecordNumber() const { return mCurrentRecord; }

	virtual void ShiftData(const double *dx = 0);

	virtual float GetMemorySize() const;
	
	//
	//  Functions for file manipulation
	//
	const iString& GetLastFileSetName() const;
	bool IsAnotherSetLeader(const iString &fname) const;
	bool IsCurrentSetMember(const iString &fname) const;
	const iString GetFileSetName(int rec) const;
	const iString GetFileSetRoot() const;
	const iDataType& GetFileSetDataType(int member = 0) const;

	const iString& GetLastFileName(const iDataType &type) const;
	inline const iString& GetLastFileName() const { return mLastFileName; }
	inline const iString& GetLastAttemptedFileName() const { return mLastAttemptedFileName; }

	inline const iArray<VisitedFile>& GetVisitedFilesList() const { return mVisitedFilesList; }
	inline int GetRecordLength() const { return mRecordLength; }

protected:

	virtual ~iDataReader();

private:
	
	iDataReader(iViewModule *vm, const iString &fname, const iString &sname);

	void LoadFile(iFileLoader *loader, const iString &fname, bool savename);

	void InsertIntoVisitedFilesList(const iFileLoader *loader, const iString &name);
	void BreakSet();
	bool IsSetLeader(const iFileLoader *loader) const;

	bool mTwoCopies, mInExtensionUpdate;
	int mCellToPointMode;
	iString mMode;

	iFileLoader *mPeriodicityLeader;
	iLookupArray<iFileLoader*> mSetLoaders;
	iArray<iFileLoader*> mLoaders;
	iCoreData::ScalarFileLoader *mScalarLoader;
	iCoreData::VectorFileLoader *mVectorLoader;
	iCoreData::TensorFileLoader *mTensorLoader;
	iCoreData::ParticleFileLoader *mParticleLoader;

	int mCurrentRecord, mRecordLength;

	iDataInfo *mDataReloadInfo;

	iString mLastFileName, mLastAttemptedFileName;
	iArray<VisitedFile> mVisitedFilesList;
	iDirectory *mDirectory;
};


class iDataReaderExtension : public iObjectExtension
{

	iObjectExtensionDeclareAbstractMacro(iDataReader);

public:

	inline iDataReader* GetReader() const { return mRealOwner; }

	virtual iFileLoader* GetLoader(int n) const = 0;

	//
	//  It is a good idea to keep track of the last file read.
	//
	inline const iString& GetLastFileName() const { return mLastFileName; }

	void AfterLoadFile(iFileLoader *loader, const iString &fname);

protected:

	virtual void AfterLoadFileBody(iFileLoader *loader, const iString &fname);

	iString mLastFileName;
};

#endif

