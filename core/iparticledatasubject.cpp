/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iparticledatasubject.h"


#include "idata.h"
#include "idatalimits.h"
#include "ierror.h"
#include "iparticlefileloader.h"
#include "iparticleprobefilter.h"

//
//  Templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


iParticleDataSubject::iParticleDataSubject(iParticleFileLoader *fl, const iDataType& type) : iDataSubject(fl,type), 
	iObjectConstructPropertyMacroQ(Bool,iParticleDataSubject,UseExtras,ue),
	iObjectConstructPropertyMacroS(Bool,iParticleDataSubject,TypeIncluded,ti),
	iObjectConstructPropertyMacroS(Int,iParticleDataSubject,DownsampleMode,dm),
	iObjectConstructPropertyMacroS(Int,iParticleDataSubject,DownsampleFactor,df),
	iObjectConstructPropertyMacroS(Bool,iParticleDataSubject,AddOrderAsVar,ov),
	iObjectConstructPropertyMacroS(Int,iParticleDataSubject,DensityVar,dv)
{	
	DensityVar.AddFlag(iProperty::_FunctionsAsIndex);

	mParticleLoader = fl; IASSERT(mParticleLoader);
}


iDataLimits* iParticleDataSubject::CreateLimits() const
{
	return iDataLimits::New(this);
}



bool iParticleDataSubject::GetTypeIncluded() const
{
	return mParticleLoader->GetTypeIncluded(this->GetId());
}


int iParticleDataSubject::GetDownsampleMode() const
{
	return mParticleLoader->GetDownsampleMode();
}


int iParticleDataSubject::GetDownsampleFactor() const
{
	return mParticleLoader->GetDownsampleFactor(this->GetId());
}


bool iParticleDataSubject::GetAddOrderAsVar() const
{
	return mParticleLoader->GetAddOrderAsVariable(this->GetId());
}


int iParticleDataSubject::GetDensityVar() const
{
	return mParticleLoader->GetDensityVariable(this->GetId());
}


bool iParticleDataSubject::GetUseExtras() const
{
	return mParticleLoader->IsUsingExtras();
}


bool iParticleDataSubject::SetTypeIncluded(bool b)
{
	mParticleLoader->SetTypeIncluded(this->GetId(),b);
	return true;;
}


bool iParticleDataSubject::SetDownsampleMode(int i)
{
	mParticleLoader->SetDownsampleMode(i);
	return true;;
}


bool iParticleDataSubject::SetDownsampleFactor(int i)
{
	mParticleLoader->SetDownsampleFactor(this->GetId(),i);
	return true;;
}


bool iParticleDataSubject::SetAddOrderAsVar(bool b)
{
	mParticleLoader->SetAddOrderAsVariable(this->GetId(),b);
	return true;;
}


bool iParticleDataSubject::SetDensityVar(int i)
{
	mParticleLoader->SetDensityVariable(this->GetId(),i);
	return true;;
}


iProbeFilter* iParticleDataSubject::CreateProbeFilter(iDataConsumer *consumer) const
{
	return iParticleProbeFilter::New(consumer);
}

