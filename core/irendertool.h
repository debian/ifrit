/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


//
//  iRenderTool is a combination of a RenderWindow and a Renderer.
//  In dual-window stereo mode it pops up a separate view for the right eye.
//
#ifndef IRENDERTOOL_H
#define IRENDERTOOL_H


#include <vtkObjectBase.h>
#include "iviewmodulecomponent.h"


#include "iarray.h"

#include <vtkSetGet.h>

class iActor;
class iCamera;
class iColor;
class iEventObserver;
class iImage;
class iMagnifier;
class iRenderToolBackground;
class iString;
class iStereoImage;
class iStereoImageArray;

class vtkProp;
class vtkPropAssembly;
class vtkPropCollection;
class vtkRenderer;
class vtkRenderWindow;
class vtkRenderWindowCollection;
class vtkRenderWindowInteractor;


namespace iParameter
{
	namespace TextType
	{
		const int Arial = 0;
		const int Courier = 1;
		const int Times = 2;

		inline bool IsValid(int m){ return m>=0 && m<=2; }
	};

	namespace Antialiasing
	{
		const int Points = 1;
		const int Lines  = 2;
		const int Polys  = 4;
	};

	namespace DepthPeeling
	{
		const int Off   = 0;
		const int Low   = 1;
		const int High  = 2;
		const int Exact = 3;
	};
};


class iRenderTool : public vtkObjectBase, public iViewModuleComponent
{
	
	friend class iCamera;
	friend class iDualWindowObserver;
	friend class iExtensionFactory;
	friend class iMainWindowObserver;
	friend class iRendererObserver;
	friend class iStereoRenderObserver;

public:

	vtkTypeMacro(iRenderTool,vtkObjectBase);
	static iRenderTool* New(iViewModule *vm = 0);

	virtual int GetNumberOfActiveViews() const;

	void SetRenderWindowPosition(int x, int y);
	void SetRenderWindowPosition(const int *p){ this->SetRenderWindowPosition(p[0],p[1]); }
	inline const int* GetRenderWindowPosition() const { return mRenderWindowPosition; }
	virtual void SetRenderWindowSize(int w, int h);
	void SetRenderWindowSize(const int *s){ this->SetRenderWindowSize(s[0],s[1]); }
	inline const int* GetRenderWindowSize() const { return mRenderWindowSize; }

	inline iMagnifier* GetMagnifier() const { return mMagnifier; }
	inline vtkRenderer* GetRenderer() const { return mMainRen; }
	inline vtkRenderWindow* GetRenderWindow() const { return mMainWin; }
	inline vtkRenderWindowInteractor* GetRenderWindowInteractor() const { return mMainInt; }
	vtkRenderWindowCollection* GetRenderWindowCollection();
	inline iCamera* GetCamera() const { return mMainCam; }

	int GetRenderingMagnification() const;

	void Render();
	virtual void AddObserver(unsigned long event, iEventObserver *observer);    
	virtual void UpdateWindowName();
	virtual void UpdateWindowName(const iString& suffix);

	virtual void SetBackground(const iColor &color);
	virtual void SetBackground(const iImage &image);
	virtual float GetLastRenderTimeInSeconds() const;
	void GetAspectRatio(double ar[2]) const;

	virtual void AddObject(vtkProp* p);
	virtual void RemoveObject(vtkProp* p);

	virtual void SetAntialiasingMode(int m);
	inline int GetAntialiasingMode() const { return mAntialiasingMode; }

	virtual void SetDepthPeelingStyle(int s);
	inline int GetDepthPeelingStyle() const { return mDepthPeelingStyle; }

	virtual void SetStereoMode(int m);
	inline int GetStereoMode() const { return mStereoMode; }

	void SetBackgroundImageTile(float tx, float ty, float tw, float th);
	void SetBackgroundImageTile(const float tile[4]);
	virtual void SetBackgroundImageFixedAspect(bool s);
	inline bool GetBackgroundImageFixedAspect() const { return mBackgroundImageFixedAspect; }

	virtual int GetFullScreenMode() const;

	virtual	void CopyBackground(const iRenderTool *source);

	virtual void ShowStereoAlignmentMarks(bool s); 

	virtual void RenderImages(int mag, iStereoImageArray &images);
	void RenderStereoImage(int mag, iStereoImage &image);

	void ExportScene(const iString &fname);

	virtual void SetFontType(int t);
	inline int GetFontType() const { return mFontType; }

	virtual void SetFontScale(int t);
	inline int GetFontScale() const { return mFontScale; }

protected:
	
	iRenderTool(iViewModule *vm, vtkRenderer *ren = 0, iMagnifier *mag = 0);
	virtual ~iRenderTool();

	virtual iRenderTool* CreateInstance(vtkRenderer *ren) const;
	vtkRenderer* CreateRenderer() const;
	void ShowDualWindow(bool s);

	//
	//  Functions used by iCamera class
	//
	virtual void ResetCamera();
	void ResetCameraClippingRange();
	void SetAdjustCameraClippingRangeAutomatically(bool s);
	inline bool GetAdjustCameraClippingRangeAutomatically() const { return mAutoClippingRange; }
	void SetCameraClippingRange(double cr[2]);
	inline void GetCameraClippingRange(double cr[2]) const { cr[0] = mClippingRange[0]; cr[1] = mClippingRange[1]; }

	virtual void UpdateWindowCollection();
	void WindowsModified();

	//
	//  Rendering pipeline
	//
	vtkRenderWindow *mMainWin, *mDualWin;
	vtkRenderer *mMainRen, *mDualRen;
	iRenderToolBackground *mMainBkg, *mDualBkg;
	vtkRenderWindowInteractor *mMainInt;
	vtkRenderWindowCollection *mWindowCollection;
	iCamera *mMainCam;
	vtkPropAssembly *mStereoAlignmentMarksActor;

	iEventObserver *mMainWindowObserver, *mDualWindowObserver, *mRendererObserver, *mStereoRenderObserver;

	//
	//  Observers registry
	//
	struct Observer
	{
		unsigned long Event;
		iEventObserver *Command;
		Observer(){ Event = 0L; Command = 0; }
	};
	iArray<Observer> mObservers;

private:

	void Reset();
	void ConfigureDepthPeeling(vtkRenderer *ren) const;

	//
	//  Other members
	//
	iMagnifier *mMagnifier;
	int mStereoMode, mFontScale, mFontType, mDepthPeelingStyle, mAntialiasingMode;
	double mClippingRange[2];
	bool mStereoAlignmentMarksOn, mAutoClippingRange, mWindowCollectionUpToDate, mBackgroundImageFixedAspect;
	const int mStereoModeOffset;
	int *mRenderWindowPosition, *mRenderWindowSize;

	bool mMainWinStereoRender;
	void SetMainWinStereoRender(bool s);

	//
	//  For ordered rendering
	//
	vtkPropCollection *mActorObjects, *mVolumeObjects;
};

#endif // IRENDERVIEW_H

