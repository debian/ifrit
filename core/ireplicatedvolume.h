/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


//
//  A volume that can be replicated periodically. This is a preferred way to replicate, since
//  we create no new data and re-use mappers.
//
#ifndef IREPLICATEDVOLUME_H
#define IREPLICATEDVOLUME_H


#include "igenericprop.h"
#include <vtkVolume.h>
#include "ireplicatedelement.h"
#include "iviewmodulecomponent.h"


#include "iarray.h"

class vtkFrustumCoverageCuller;


class iReplicatedVolume : public iGenericProp<vtkVolume>, public iReplicatedElement, public iViewModuleComponent
{
	
public:
	
	vtkTypeMacro(iReplicatedVolume,vtkVolume);
	static iReplicatedVolume* New(iViewSubject *owner = 0);

	virtual double* GetBounds();

	inline vtkTransform* GetTransform() const { return this->Transform; }

protected:
	
	iReplicatedVolume(iViewSubject *owner);
	virtual ~iReplicatedVolume();
	
	virtual void UpdateGeometry(vtkViewport *vp);

	virtual void UpdateReplicasBody();

private:

	void CreateReplica(int i, int j, int k);

	struct Replica
	{
		vtkVolume *Volume;
		int Pos[3];
		Replica(){ Volume = 0; Pos[0] = Pos[1] = Pos[2] = 0; }
	};

	iArray<Replica> mReplicas;
	vtkFrustumCoverageCuller *mCuller;
};

#endif // IREPLICATEDVOLUME_H
