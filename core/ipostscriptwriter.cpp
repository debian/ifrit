/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ipostscriptwriter.h"


#include <vtkImageData.h>
#include <vtkPointData.h>


#define VTK_MARGIN 0.95


iPostScriptWriter::Paper iPostScriptWriter::mPaper[NFORMATS];


iPostScriptWriter::iPostScriptWriter()
{
	int i = 0;
	if(i < NFORMATS) { mPaper[i].Width = 33.0556; mPaper[i].Height = 46.7778; mPaper[i].Name = "A0"; i++; }
	if(i < NFORMATS) { mPaper[i].Width = 23.3889; mPaper[i].Height = 33.0556; mPaper[i].Name = "A1"; i++; }
	if(i < NFORMATS) { mPaper[i].Width = 16.5278; mPaper[i].Height = 23.3889; mPaper[i].Name = "A2"; i++; }
	if(i < NFORMATS) { mPaper[i].Width = 11.6944; mPaper[i].Height = 16.5278; mPaper[i].Name = "A3"; i++; }
	if(i < NFORMATS) { mPaper[i].Width = 8.26389; mPaper[i].Height = 11.6944; mPaper[i].Name = "A4"; i++; }
	if(i < NFORMATS) { mPaper[i].Width = 5.84772; mPaper[i].Height = 8.26389; mPaper[i].Name = "A5"; i++; }
	if(i < NFORMATS) { mPaper[i].Width = 4.12500; mPaper[i].Height = 5.84772; mPaper[i].Name = "A6"; i++; }
	if(i < NFORMATS) { mPaper[i].Width = 2.91667; mPaper[i].Height = 4.12500; mPaper[i].Name = "A7"; i++; }
	if(i < NFORMATS) { mPaper[i].Width = 2.05556; mPaper[i].Height = 2.91667; mPaper[i].Name = "A8"; i++; }
	if(i < NFORMATS) { mPaper[i].Width = 8.5    ; mPaper[i].Height = 11.0   ; mPaper[i].Name = "Letter"; i++; }
	if(i < NFORMATS) { mPaper[i].Width = 11.0   ; mPaper[i].Height = 17.0   ; mPaper[i].Name = "11x17"; i++; }
	
	mFormat = 9;
	mOrient = 0;
}


iPostScriptWriter::~iPostScriptWriter()
{
}


void iPostScriptWriter::SetPaperFormat(int f)
{
	if(f>=0 && f<NFORMATS) mFormat = f;
}


void iPostScriptWriter::SetOrientation(int o)
{
	if(o>=0 && o<=1) mOrient = o;
}


void iPostScriptWriter::WriteFileHeader(ofstream *file, vtkImageData *cache)
{
	//
	//  Copied from vtkPostScriptWriter.cxx with addition of options to specify paper size and orientation.
	//
	int min1, max1, min2, max2, min3, max3;
	int bpp;
	int cols, rows, scols, srows;
	float scale = 1;
	int pagewid = (int)(mPaper[mFormat].Width*72);
	int pagehgt = (int)(mPaper[mFormat].Height*72);
	
	// Find the length of the rows to write.
	cache->GetExtent(min1, max1, min2, max2, min3, max3);
	bpp = cache->GetNumberOfScalarComponents();
	
	cols = max1 - min1 + 1;
	rows = max2 - min2 + 1;

	if(mOrient == 1)
	{
		//
		//  Landscape orientation
		//
		int q = cols;
		cols = rows;
		rows = q;
	}
	//
	//  Fit to width
	//
	scale = float(pagewid)/cols;
	scols = int(scale*cols*VTK_MARGIN);
	srows = int(scale*rows*VTK_MARGIN);
	//
	//  If too high, fit to height
	//
	if(srows > pagehgt*VTK_MARGIN)
    {
		scale = scale*(pagehgt*VTK_MARGIN/srows);
		scols = (int)(scale*cols*VTK_MARGIN);
		srows = (int)(scale*rows*VTK_MARGIN);
    }
	float llx = (pagewid-scols) / 2;
	float lly = (pagehgt-srows) / 2;
	
	// spit out the PostScript header
	*file << "%!PS-Adobe-2.0 EPSF-2.0\n";
	*file << "%%Creator: IFrIT\n";
	*file << "%%Title: " << this->InternalFileName << endl;
	*file << "%%Pages: 1\n";
	*file << "%%BoundingBox: " << (int) llx << " "  << (int) lly
		<< " " << (int) ( llx + scols + 0.5 ) << " " << 
		(int) ( lly + srows + 0.5 ) << endl;
	*file << "%%EndComments\n";
	*file << "/readstring {\n";
	*file << "  currentfile exch readhexstring pop\n";
	*file << "} bind def\n";
	
	if ( bpp == 3)
    {
		*file << "/rpicstr " << cols << " string def\n";
		*file << "/gpicstr " << cols << " string def\n";
		*file << "/bpicstr " << cols << " string def\n";
    }
	else if (bpp == 1)
    {
		*file << "/picstr " << cols << " string def\n";
    }
	else 
    {
		vtkWarningMacro( " vtkPostScriptWriter only supports 1 and 3 component images");
    }
	
	*file << "%%EndProlog\n";
	*file << "%%Page: 1 1\n";
	*file << "gsave\n";
	*file << llx << " " << lly << " translate\n";
	*file << scols << " " << srows << " scale\n";
	*file << cols << " " << rows << " 8\n";
	*file << "[ " << cols << " 0 0 " << -rows << " 0 " << rows << " ]\n";
	if (bpp == 3)
    {
		*file << "{ rpicstr readstring }\n";
		*file << "{ gpicstr readstring }\n";
		*file << "{ bpicstr readstring }\n";
		*file << "true 3\n";
		*file << "colorimage\n";
    }
	else
    {
		*file << "{ picstr readstring }\n";
		*file << "image\n";
    }
}


void iPostScriptWriter::WriteFile(ofstream *file, vtkImageData *data, int extent[6])
{
	//
	//  Copied from vtkPostScriptWriter.cxx with addition of options to specify paper size and orientation.
	//
	int idxC, idx0, idx1, idx2;
	unsigned char *ptr;
	unsigned long count = 0;
	unsigned long tarGet;
	float progress = this->Progress;
	float area;
	int *wExtent;
	static int itemsperline = 0;
	char* hexits = (char *) "0123456789abcdef";
	
	// Make sure we actually have data.
	if ( !data->GetPointData()->GetScalars())
    {
		vtkErrorMacro(<< "Could not Get data from input.");
		return;
    }
	
	// take into consideration the scalar type
	switch (data->GetScalarType())
    {
    case VTK_UNSIGNED_CHAR:
		break;
    default:
		vtkErrorMacro("PostScriptWriter only accepts unsigned char scalars!");
		return; 
    }
	
	int i0 = 0, i1 = 1, i2 = 2, i3 = 3;
	if(mOrient == 1)
	{
		i0 = 2; i1 = 3; i2 = 0; i3 = 1;
	}

	wExtent = this->GetInput()->GetExtent();
	area = ((extent[5] - extent[4] + 1)*(extent[3] - extent[2] + 1)*
		(extent[1] - extent[0] + 1)) / 
		((wExtent[5] -wExtent[4] + 1)*(wExtent[3] -wExtent[2] + 1)*
		(wExtent[1] -wExtent[0] + 1));
    
	
	int numComponents = data->GetNumberOfScalarComponents();
	// ignore alpha
	int maxComponent = numComponents;
	if (numComponents == 2) 
    {
		maxComponent = 1;
    }
	if (numComponents == 4) 
    {
		maxComponent = 3;
    }
	tarGet = (unsigned long)((extent[5]-extent[4]+1)*
		(extent[i3]-extent[i2]+1)/(50.0*area));
	tarGet++;
	
	int ptrOffset = numComponents;
	int idx1run;
	if(mOrient == 1)
	{
		ptrOffset = numComponents*(extent[1]-extent[0]+1);
	}

	for (idx2 = extent[4]; idx2 <= extent[5]; ++idx2)
    {
		for (idx1run = extent[i3]; idx1run >= extent[i2]; idx1run--)
		{
			//
			//  Orientation
			//
			idx1 = idx1run;
			if(mOrient == 1)
			{
				idx1 = extent[i3] + extent[i2] - idx1run;
			}

			if (!(count%tarGet))
			{
				this->UpdateProgress(progress + count/(50.0*tarGet));
			}
			count++;
			// write out components one at a time because
			for (idxC = 0; idxC < maxComponent; idxC++)
			{
				//
				//  Orientation
				//
				if(mOrient == 1)
				{
					ptr = (unsigned char *)data->GetScalarPointer(idx1,extent[2],idx2);
				}
				else
				{
					ptr = (unsigned char *)data->GetScalarPointer(extent[0],idx1,idx2);
				}

				ptr += idxC;
				for ( idx0 = extent[i0]; idx0 <= extent[i1]; idx0++ )
				{
					if ( itemsperline == 30 )
					{
						*file << endl;
						itemsperline = 0;
					}
					*file << hexits[*ptr >> 4] << hexits[*ptr & 15];
					ptr += ptrOffset;
				}
			}
		}
    }
	
}

