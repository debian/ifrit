/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ireducepolydatafilter.h"


#include "ierror.h"

#include <vtkDecimatePro.h>
#include <vtkTriangleFilter.h>

//
//  Templates
//
#include "igenericfilter.tlh"


iReducePolyDataFilter::iReducePolyDataFilter(iDataConsumer *consumer) : iGenericPolyDataFilter<vtkPolyDataAlgorithm>(consumer,1,true)
{
	mTriangulator = vtkTriangleFilter::New(); IERROR_CHECK_MEMORY(mTriangulator);
	mDecimator = vtkDecimatePro::New(); IERROR_CHECK_MEMORY(mDecimator);

	mDecimator->PreserveTopologyOn();
	mDecimator->BoundaryVertexDeletionOff();
	mDecimator->SetInputConnection(mTriangulator->GetOutputPort());
}
	

void iReducePolyDataFilter::SetTargetReduction(double v)
{
	mDecimator->SetTargetReduction(v);
	this->Modified();
}


double iReducePolyDataFilter::GetTargetReduction()
{
	return mDecimator->GetTargetReduction();
}


iReducePolyDataFilter::~iReducePolyDataFilter()
{
	mDecimator->Delete();
	mTriangulator->Delete();
}


void iReducePolyDataFilter::ProvideOutput()
{
	mTriangulator->SetInputData(this->InputData());
	mDecimator->Update();
	this->OutputData()->ShallowCopy(mDecimator->GetOutput());
}
