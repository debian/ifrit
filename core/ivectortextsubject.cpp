/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ivectortextsubject.h"


#include "ierror.h"
#include "imath.h"
#include "ioverlayhelper.h"
#include "itextactor.h"
#include "itransform.h"

#include <vtkActor2D.h>
#include <vtkCamera.h>
#include <vtkMath.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper2D.h>
#include <vtkProperty2D.h>
#include <vtkRenderer.h>
#include <vtkTextProperty.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkViewport.h>
#include <vtkVectorText.h>

//
//  Templates
//
#include "iarray.tlh"
#include "igenericprop.tlh"


iVectorTextSubject* iVectorTextSubject::New(iTextActor *parent, iRenderTool *rt)
{
	IASSERT(rt);
	return new iVectorTextSubject(parent,rt);
}


iVectorTextSubject::iVectorTextSubject(iTextActor *parent, iRenderTool *rt) : iTextSubject(parent,rt)
{
	mSource = vtkVectorText::New(); IERROR_CHECK_MEMORY(mSource);
	mTransform = iTransform::New(); IERROR_CHECK_MEMORY(mTransform);
	mFilter = vtkTransformPolyDataFilter::New(); IERROR_CHECK_MEMORY(mFilter);
	mMapper = vtkPolyDataMapper2D::New(); IERROR_CHECK_MEMORY(mMapper);
	mActorShadow = vtkActor2D::New(); IERROR_CHECK_MEMORY(mActorShadow);
	mActor = vtkActor2D::New(); IERROR_CHECK_MEMORY(mActor);
	this->AppendComponent(mActorShadow);  //  shadow rendered first
	this->AppendComponent(mActor);
	
	mFilter->SetInputConnection(mSource->GetOutputPort());
	mFilter->SetTransform(mTransform);
	mMapper->SetInputConnection(mFilter->GetOutputPort());
	mActor->SetMapper(mMapper);
	mActorShadow->SetMapper(mMapper);

	mActor->GetProperty()->SetPointSize(8.0);
	mActorShadow->GetProperty()->SetPointSize(4.0);
	mActor->GetProperty()->SetLineWidth(8.0);
	mActorShadow->GetProperty()->SetLineWidth(4.0);

	mActor->GetPositionCoordinate()->SetCoordinateSystemToNormalizedViewport();
	mActorShadow->GetPositionCoordinate()->SetCoordinateSystemToNormalizedViewport();
}


iVectorTextSubject::~iVectorTextSubject()
{
	mSource->Delete();
	mTransform->Delete();
	mFilter->Delete();
	mMapper->Delete();
	mActor->Delete();
	mActorShadow->Delete();
}


void iVectorTextSubject::ComputeSize(vtkViewport *vp, float s[2])
{
	mSource->SetText(mParent->GetText().ToCharPointer());
	mSource->Update();

	this->ComputeScaledSize(vp,s,1);

	mSize[0] = s[0];
	mSize[1] = s[1];
}


void iVectorTextSubject::ComputeScaledSize(vtkViewport *vp, float s[2], int mag)
{
	float cosa = cos(mParent->GetAngle()*0.017453292);
	float sina = sin(mParent->GetAngle()*0.017453292);

	double *b = mSource->GetOutput()->GetBounds();
	
	float ff = 1.3*this->GetOverlayHelper()->GetFontSize(vp);
	float w = 0.618*(b[1]-b[0])*ff;
	float h = 0.953*ff;
	float wt = w*fabs(cosa) + h*fabs(sina);
	float ht = h*fabs(cosa) + w*fabs(sina);

	float c[4][2], x[2];

	c[0][0] = c[3][0] = 0.0;
	c[1][0] = c[2][0] = w;
	c[0][1] = c[1][1] = 0.0;
	c[2][1] = c[3][1] = h;

	float dx = 0.0, dy = 0.0;
	int j;
	for(j=0; j<4; j++)
	{
		x[0] = c[j][0]*cosa - c[j][1]*sina;
		x[1] = c[j][0]*sina + c[j][1]*cosa;
		if(x[0] < dx) dx = x[0];
		if(x[1] < dy) dy = x[1];
	}

	mTransform->Identity();
	mTransform->Translate(-dx*mag,-dy*mag,0.0);
	mTransform->RotateZ(mParent->GetAngle());
	mTransform->Scale(0.6*ff*mag,0.65*ff*mag,1.0);
	mTransform->Translate(-0.17,0.09,0.0);
	mMapper->Update();

	int *ws = vp->GetSize();
	s[0] = wt/ws[0]; 
	s[1] = ht/ws[1];
}


void iVectorTextSubject::UpdateGeometryBody(vtkViewport* vp, int mag)
{
	int *ws = vp->GetSize();
	float cosa = cos(mParent->GetAngle()*0.017453292);
	float sina = sin(mParent->GetAngle()*0.017453292);

	float s[2], pshadow[2];
	pshadow[0] = 1.0/ws[0]*(cosa+sina);
	pshadow[1] = -1.0/ws[1]*(cosa-sina);

	this->ComputeScaledSize(vp,s,mag);

	if(mag == 1)
	{
		mActor->GetProperty()->SetColor(this->GetOverlayHelper()->GetColor(vp).ToVTK());
		mActorShadow->GetProperty()->SetColor(this->GetOverlayHelper()->GetColor(vp).Shadow().ToVTK());

		//
		//  Account for justification
		//
		mPos[0] -= 0.5*mParent->GetTextProperty()->GetJustification()*mSize[0];
		mPos[1] -= 0.5*mParent->GetTextProperty()->GetVerticalJustification()*mSize[1];

		mActor->SetPosition(mPos[0],mPos[1]);
		mActor->SetPosition2(mSize[0],mSize[1]);
		mActorShadow->SetPosition(mPos[0]+pshadow[0],mPos[1]+pshadow[1]);
		mActorShadow->SetPosition2(mSize[0],mSize[1]);
	}
	else
	{
		int winij[2];
		this->GetOverlayHelper()->ComputePositionShiftsUnderMagnification(winij);
		
		mActor->SetPosition(mag*mPos[0]-winij[0],mag*mPos[1]-winij[1]);
		mActor->SetPosition2(mag*mSize[0],mag*mSize[1]);
		mActorShadow->SetPosition(mag*(mPos[0]+pshadow[0])-winij[0],mag*(mPos[1]+pshadow[1])-winij[1]);
		mActorShadow->SetPosition2(mag*mSize[0],mag*mSize[1]);
	}
}
