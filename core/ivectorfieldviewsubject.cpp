/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ivectorfieldviewsubject.h"


#include "iactor.h"
#include "iboundeddisksource.h"
#include "iboundedplanesource.h"
#include "iboundedspheresource.h"
#include "icommondatadistributors.h"
#include "icoredatasubjects.h"
#include "idatalimits.h"
#include "idatasubject.h"
#include "ierror.h"
#include "imarker.h"
#include "imath.h"
#include "istreamlinefilter.h"
#include "ivectorfieldglyphpipeline.h"
#include "ivectorfieldstreamlinepipeline.h"
#include "iviewmodule.h"
#include "iviewobject.h"
#include "iviewsubjectparallelpipeline.h"

#include <vtkCellArray.h>
#include <vtkFloatArray.h>
#include <vtkMath.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkProperty.h>

//
//  Templates
//
#include "iarray.tlh"
#include "igenericfilter.tlh"
#include "iproperty.tlh"


using namespace iParameter;


//
//  single prop class
//
iVectorFieldViewInstance::iVectorFieldViewInstance(iVectorFieldViewSubject *owner) : iFieldViewInstance(owner,3), iPropPlacementHelper(owner->GetViewModule())
{
	mMethod = VectorField::Method::Glyph;
	mLineWidth = 1;
	mLineQuality = 0;
	mLineLength = 1.0;
	mLineDirection = StreamLine::Direction::BothWays;
	mTubeSize = 1;
	mTubeRangeFactor = 10;
	mTubeVariationFactor = 0.01; 

	mGlyphBaseSize = 0;
	mShowSourceObject = false;
	mSourceType = StreamLine::Source::Plane;
	mNumberOfStreamLines = 4;
	mSize = 0.5;

	mPaintOffset = 0;

	//
	//  Do VTK stuff
	//	
	mStreamLineSourcePoints = vtkPoints::New(VTK_DOUBLE); IERROR_CHECK_MEMORY(mStreamLineSourcePoints);
	mStreamLineSourceVerts = vtkCellArray::New(); IERROR_CHECK_MEMORY(mStreamLineSourceVerts);
	mStreamLineSourceNorms = vtkFloatArray::New(); IERROR_CHECK_MEMORY(mStreamLineSourceNorms);
	mStreamLineSourceNorms->SetNumberOfComponents(3);

	mSourceDisk = iBoundedDiskSource::New(this); IERROR_CHECK_MEMORY(mSourceDisk);
	mSourcePlane = iBoundedPlaneSource::New(this); IERROR_CHECK_MEMORY(mSourcePlane);
	mSourceSphere = iBoundedSphereSource::New(this); IERROR_CHECK_MEMORY(mSourceSphere);

	mSourceDisk->SetResolution(10);
	mSourcePlane->SetResolution(10);
	mSourceSphere->SetResolution(10);

#ifdef I_DEBUG
	mSourceDisk->SetWithArrow(true);
#else
	mSourceDisk->SetWithArrow(false);
#endif

	this->SetSourceOpacity(0.5);
 	mActors[1]->SetInputConnection(mSourcePlane->GetOutputPort());
}


iVectorFieldViewInstance::~iVectorFieldViewInstance()
{
	mStreamLineSourcePoints->Delete();
	mStreamLineSourceVerts->Delete();
	mStreamLineSourceNorms->Delete();
	mSourceDisk->Delete();
	mSourcePlane->Delete();
	mSourceSphere->Delete();
}


void iVectorFieldViewInstance::ConfigureBody()
{
	//
	//  Create pipelines (must be created after the object is fully created)
	//	
	this->AddMainPipeline(1);
	this->AddMainPipeline(2);
	mActors[0]->SetInputConnection(this->Pipeline()->GetOutputPort(0));

	vtkPolyData *pd = vtkPolyData::New(); IERROR_CHECK_MEMORY(pd);
	pd->SetPoints(mStreamLineSourcePoints);
	pd->SetVerts(mStreamLineSourceVerts);
	pd->GetPointData()->SetNormals(mStreamLineSourceNorms);
	this->Pipeline(1)->SetInputData(pd);
	pd->Delete();

	//
	//  Source point representation
	//
	mActors[2]->SetInputData(pd);
	mActors[2]->GetProperty()->SetPointSize(10);
	mActors[2]->GetProperty()->SetColor(0,0,0);
}


void iVectorFieldViewInstance::ResetPipelineInput(vtkDataSet *input)
{
	this->Pipeline(0)->SetInputData(0,input);
	this->Pipeline(1)->SetInputData(1,input); // input(0) for Pipeline(1) is streamline sources for parallel execution
}


void iVectorFieldViewInstance::ResetBody()
{
	this->iFieldViewInstance::ResetBody();

	mStreamLineSourcePoints->Initialize();
	mStreamLineSourceVerts->Initialize();
	this->UpdatePipelines();
}


void iVectorFieldViewInstance::UpdatePipelines()
{
	this->Pipeline(0)->UpdateContents(iVectorFieldGlyphPipeline::KeyGlyphSize);
	this->Pipeline(1)->UpdateContents(iVectorFieldStreamLinePipeline::KeyPipeline);
}


void iVectorFieldViewInstance::UpdateOnMarkerChange()
{
	if(mSourceType == StreamLine::Source::Marker)
	{
		int i;
		vtkIdType l[1];
		bool reset = false;

		if(mStreamLineSourcePoints->GetNumberOfPoints() != this->GetViewModule()->GetNumberOfMarkers())
		{
			mStreamLineSourcePoints->Initialize();
			mStreamLineSourceVerts->Initialize();
			mStreamLineSourceNorms->Initialize();
			mStreamLineSourcePoints->SetNumberOfPoints(this->GetViewModule()->GetNumberOfMarkers());
			reset = true;
		}

		iMarkerInstance *m;
		for(i=0; i<this->GetViewModule()->GetNumberOfMarkers(); i++)
		{
			m = this->GetViewModule()->GetMarker(i);
			mStreamLineSourcePoints->SetPoint(i,m->GetPosition());
			if(reset)
			{
				//
				//  If we are not resetting, then the vert array did not change
				//
				l[0] = i;
				mStreamLineSourceVerts->InsertNextCell(1,l);
			}
		}

		this->Pipeline(1)->InputData(0)->Modified();
	}
}


void iVectorFieldViewInstance::UpdateStreamLineSource()
{ 
	switch(mSourceType)
	{
	case StreamLine::Source::Marker:
		{
			this->UpdateOnMarkerChange();
			break;
		}
	case StreamLine::Source::Disk:
	case StreamLine::Source::Plane:
	case StreamLine::Source::Sphere:
		{
			int i, k;
			vtkIdType n, l;
			double x[3], rx, ry, rf;
			bool reset = false;
			
			n = mNumberOfStreamLines;
			if(n < 1) return;

			mSourceDisk->SetCenter(mPosition);
			mSourceDisk->SetNormal(mDirection);
			mSourceDisk->SetRadius(mSize);
			mSourcePlane->SetCenter(mPosition);
			mSourcePlane->SetNormal(mDirection);
			mSourceSphere->SetCenter(mPosition);
			mSourceSphere->SetRadius(mSize);

			if(mStreamLineSourcePoints->GetNumberOfPoints() != n)
			{
				mStreamLineSourcePoints->Initialize();
				mStreamLineSourceVerts->Initialize();
				mStreamLineSourceNorms->Initialize();
				mStreamLineSourcePoints->SetNumberOfPoints(n);
				mStreamLineSourceNorms->SetNumberOfComponents(3);
				mStreamLineSourceNorms->SetNumberOfTuples(n);
				reset = true;
			}

			l = 0;
			iMath::Random ran(123456789);
			//
			//  Find two basis vectors for the plane
			//
			double t, t1[3], t2[3], z[3];
			do
			{
				for(i=0; i<3; i++)
				{
					t1[i] = 2*ran.NextValue() - 1;
				}
				vtkMath::Normalize(t1);
				vtkMath::Cross(mDirection,t1,t2);
			}
			while(t2[0]*t2[0]+t2[1]*t2[1]+t2[2]*t2[2] < 1.0e-10);
			
			t = vtkMath::Dot(mDirection,t1);
			for(i=0; i<3; i++) t1[i] -= t*mDirection[i];
			vtkMath::Normalize(t1);
			vtkMath::Normalize(t2);

#ifdef I_CHECK
			if(fabs(vtkMath::Dot(t1,mDirection))>1.0e-10 || fabs(vtkMath::Dot(t2,mDirection))>1.0e-10 || fabs(vtkMath::Dot(t1,t2))>1.0e-10)
			{
				IBUG_ERROR("Bug in computing plane basis");
			}
#endif

			k = 0;
			rf = 1.0;
			double sz = mSize;
			double pos[3];
			for(i=0; i<3; i++) pos[i] = mPosition[i];

			while(l < n)
			{
				//
				//  Pick up a random point on the source surface
				//
				switch(mSourceType)
				{
				case StreamLine::Source::Disk:
					{
						//
						//  Random point on the disk
						//
						rx = 2*ran.NextValue() - 1;
						ry = 2*ran.NextValue() - 1;
						if(rx*rx+ry*ry > 1.0)
						{
							x[0] = x[1] = x[2] = 2.0;
						}
						else
						{
							for(i=0; i<3; i++) 
							{
								x[i] = pos[i] + rf*sz*(rx*t1[i]+ry*t2[i]);
								z[i] = mDirection[i];
							}
						}
						break;
					}
				case StreamLine::Source::Plane:
					{
						//
						//  Random point on the plane
						//
						rx = 3.5*ran.NextValue() - 1.75;
						ry = 3.5*ran.NextValue() - 1.75;
						for(i=0; i<3; i++) 
						{
							x[i] = pos[i] + rf*(rx*t1[i]+ry*t2[i]);
							z[i] = mDirection[i];
						}
						break;
					}
				case StreamLine::Source::Sphere:
					{
						//
						//  Random point on the sphere
						//
						z[2] = 2*ran.NextValue() - 1;
						rx = sqrt(1.0-z[2]*z[2]);
						ry = 6.2831853*ran.NextValue();
						z[0] = rx*cos(ry);
						z[1] = rx*sin(ry);
						for(i=0; i<3; i++)
						{
							x[i] = pos[i] + sz*z[i];
							if(x[i] < -1.0) x[i] += 2.0; 
							if(x[i] >  1.0) x[i] -= 2.0;
						}
						break;
					}
				}

				k++;

				if(k<100 && (x[0]<-1.0 || x[0]>1.0 || x[1]<-1.0 || x[1]>1.0 || x[2]<-1.0 || x[2]>1.0)) 
				{
					if(k > 20) rf *= 0.5;
					continue;
				}

				if(k >= 100)
				{
#ifndef I_NO_CHECK
					IBUG_WARN("Internal check failed.");
#endif
					for(i=0; i<3; i++) x[i] = mPosition[i];
				}

				k = 0;
				rf = 1.0;
				mStreamLineSourcePoints->SetPoint(l,x); 
				mStreamLineSourceNorms->SetTuple(l,z); 
				if(reset) mStreamLineSourceVerts->InsertNextCell(1,&l); 
				l++;
			}
			break;
		}
	default:
		{
#ifndef I_NO_CHECK
			IBUG_WARN("Internal check failed.");
#endif
		}
	}

	this->Pipeline(1)->InputData(0)->Modified();
}


bool iVectorFieldViewInstance::SetShowSourceObject(bool s)
{
	if(s)
	{
		mActors[1]->VisibilityOn();
#ifdef I_DEBUG
		mActors[2]->VisibilityOn();
#endif
	}
	else
	{
		mActors[1]->VisibilityOff();
#ifdef I_DEBUG
		mActors[2]->VisibilityOff();
#endif
	}
	mShowSourceObject = s;
	return true;
}


bool iVectorFieldViewInstance::SetSourceType(int v)
{
	if(StreamLine::Source::IsValid(v))
	{
		mSourceType = v;
		switch(mSourceType)
		{
		case StreamLine::Source::Disk:
			{
				mActors[1]->SetInputConnection(mSourceDisk->GetOutputPort());
				this->SetShowSourceObject(mShowSourceObject);
				break;
			}
		case StreamLine::Source::Plane:
			{
				mActors[1]->SetInputConnection(mSourcePlane->GetOutputPort());
				this->SetShowSourceObject(mShowSourceObject);
				break;
			}
		case StreamLine::Source::Sphere:
			{
				mActors[1]->SetInputConnection(mSourceSphere->GetOutputPort());
				this->SetShowSourceObject(mShowSourceObject);
				break;
			}
		default:
			{
				this->SetShowSourceObject(false);
			}
		}
		this->UpdateStreamLineSource();
		return true;
	}
	else return false;
}


bool iVectorFieldViewInstance::SetNumberOfStreamLines(int v)
{
	if(v > 0)
	{
		mNumberOfStreamLines = v;
		this->UpdateStreamLineSource();
		return true;
	}
	else return false;
}


bool iVectorFieldViewInstance::SetSourceOpacity(float o)
{
	if(o<0 || o>1) return false;

	mSourceOpacity = o;
	mActors[1]->GetProperty()->SetOpacity(mSourceOpacity);
	return true;
}


bool iVectorFieldViewInstance::SetMethod(int v)
{ 
	if(VectorField::Method::IsValid(v)) 
	{
		mMethod = v;
		switch(mMethod)
		{
		case VectorField::Method::Glyph:
			{
				mActors[0]->SetInputConnection(this->Pipeline(0)->GetOutputPort());
				mPaintOffset = 0;
				break;
			}
		case VectorField::Method::StreamLine:
		case VectorField::Method::StreamTube:
		case VectorField::Method::StreamBand:
			{
				mActors[0]->SetInputConnection(this->Pipeline(1)->GetOutputPort());
				mPaintOffset = 1;
				break;
			}
		}
		this->UpdatePipelines();
		this->UpdateStreamLineSource();
		return true;;
	}
	else return false;
}


bool iVectorFieldViewInstance::SetLineDirection(int v)
{ 
	if(StreamLine::Direction::IsValid(v)) 
	{
		mLineDirection = v;
		this->Pipeline(1)->UpdateContents(iVectorFieldStreamLinePipeline::KeyLineDirection);
		return true;;
	}
	else return false;
}


bool iVectorFieldViewInstance::SetLineWidth(int w)
{
	if(w>=1 && w<=100)
	{
		mLineWidth = w;
		mActors[0]->GetProperty()->SetLineWidth(mLineWidth);
		return true;
	}
	else return false;
}


void iVectorFieldViewInstance::UpdateGlyphSize()
{ 
	this->Pipeline()->UpdateContents(iVectorFieldGlyphPipeline::KeyGlyphSize);
}


bool iVectorFieldViewInstance::SetLineLength(float v)
{ 
	if(v>0.0 && v<100.0)
	{
		mLineLength = v;
		this->Pipeline(1)->UpdateContents(iVectorFieldStreamLinePipeline::KeyLineLength);
		return true;;
	}
	else return false;
}


void iVectorFieldViewInstance::UpdateGlyphSampleRate()
{ 
	this->Pipeline()->UpdateContents(iVectorFieldGlyphPipeline::KeyGlyphSampleRate);
}


bool iVectorFieldViewInstance::SetGlyphBaseSize(int v)
{ 
	if(v>=0 && v<=100) 
	{
		mGlyphBaseSize = v;
		this->Pipeline(0)->UpdateContents(iVectorFieldGlyphPipeline::KeyGlyphBaseSize);
		mActors[0]->GetProperty()->SetPointSize(mGlyphBaseSize);
		return true;;
	}
	else return false;
}


bool iVectorFieldViewInstance::SetLineQuality(int v)
{ 
	if(v>=0 && v<10) 
	{
		mLineQuality = v;
		this->Pipeline(1)->UpdateContents(iVectorFieldStreamLinePipeline::KeyLineQuality);
		return true;;
	}
	else return false;
}


bool iVectorFieldViewInstance::SetTubeSize(int v)
{ 
	if(v>=1 && v<=10) 
	{
		mTubeSize = v;
		this->Pipeline(1)->UpdateContents(iVectorFieldStreamLinePipeline::KeyTubeSize);
		mActors[0]->GetProperty()->SetLineWidth(v);
		return true;;
	}
	else return false;
}


bool iVectorFieldViewInstance::SetTubeRangeFactor(float q)
{ 
	if(q>1.0 && q<100.0) 
	{
		mTubeRangeFactor = q;
		this->Pipeline(1)->UpdateContents(iVectorFieldStreamLinePipeline::KeyTubeRangeFactor);
		return true;;
	}
	else return false;
}


bool iVectorFieldViewInstance::SetTubeVariationFactor(float q)
{ 
	if(q>1.0e-7 && q<1.0) 
	{
		mTubeVariationFactor = q;
		this->Pipeline(1)->UpdateContents(iVectorFieldStreamLinePipeline::KeyTubeVariationFactor);
		return true;;
	}
	else return false;
}


void iVectorFieldViewInstance::ShowBody(bool show)
{
	if(show)
	{
		this->UpdatePipelines();
		this->UpdateStreamLineSource();
		mActors[0]->VisibilityOn();
	} 
	else 
	{
		this->SetShowSourceObject(false);
		mActors[0]->VisibilityOff();
	}
}


float iVectorFieldViewInstance::GetMemorySize()
{
	float s = this->iFieldViewInstance::GetMemorySize();
	s += mStreamLineSourcePoints->GetActualMemorySize();
	s += mStreamLineSourceVerts->GetActualMemorySize();
	return s;
}


bool iVectorFieldViewInstance::CanBeShown() const
{
	return this->Owner()->GetSubject()->IsThereVectorData();
}


void iVectorFieldViewInstance::UpdateSize()
{
	this->UpdateStreamLineSource();
}
	

void iVectorFieldViewInstance::UpdatePosition()
{
	mPosition.CutToBounds();
	this->UpdateStreamLineSource();
}


void iVectorFieldViewInstance::UpdateDirection()
{
	this->UpdateStreamLineSource();
}


bool iVectorFieldViewInstance::SyncWithDataBody()
{
	this->UpdatePipelines();
	return true;
}


iViewSubjectPipeline* iVectorFieldViewInstance::CreatePipeline(int id)
{
	switch(id)
	{
	case 0: return new iVectorFieldGlyphPipeline(this);
	case 1: return new iVectorFieldStreamLinePipeline(this);
	default: return 0;
	}
}


void iVectorFieldViewInstance::ConfigureMainPipeline(iViewSubjectPipeline *p, int id)
{
	iViewSubjectParallelPipeline *pp = iRequiredCast<iViewSubjectParallelPipeline>(INFO,p);

	switch(id)
	{
	case 0:
		{
			iImageDataDistributor *idd = new iImageDataDistributor(pp->GetDataManager());
			pp->GetDataManager()->AddDistributor(idd);
			pp->GetDataManager()->AddCollector(new iPolyDataCollector(pp->GetDataManager(),idd));
			break;
		}
	case 1:
		{
            pp->GetDataManager()->AddDistributor(new iPolyDataDistributor(pp->GetDataManager()));
			pp->GetDataManager()->AddCollector(new iPolyDataCollector(pp->GetDataManager()));
			break;
		}
	}
}


void iVectorFieldViewInstance::RemoveInternalData()
{
	this->iFieldViewInstance::RemoveInternalData();

	mStreamLineSourcePoints->Initialize();
	mStreamLineSourceVerts->Initialize();
	mStreamLineSourceNorms->Initialize();
}


//
// iVectorFieldViewSubject class
//
iVectorFieldViewSubject::iVectorFieldViewSubject(iViewObject *parent, const iDataType &type, const iDataType &stype) : iFieldViewSubject(parent,parent->LongName(),parent->ShortName(),parent->GetViewModule(),type,stype,0U),
	iViewSubjectPropertyConstructMacro(Int,iVectorFieldViewInstance,Method,m),
	iViewSubjectPropertyConstructMacro(Int,iVectorFieldViewInstance,LineWidth,lw),
	iViewSubjectPropertyConstructMacro(Int,iVectorFieldViewInstance,LineQuality,lq),
	iViewSubjectPropertyConstructMacro(Float,iVectorFieldViewInstance,LineLength,ll),
	iViewSubjectPropertyConstructMacro(Int,iVectorFieldViewInstance,LineDirection,ld),
	iViewSubjectPropertyConstructMacro(Int,iVectorFieldViewInstance,TubeSize,ts),
	iViewSubjectPropertyConstructMacro(Float,iVectorFieldViewInstance,TubeRangeFactor,tr),
	iViewSubjectPropertyConstructMacro(Float,iVectorFieldViewInstance,TubeVariationFactor,tv),
	iViewSubjectPropertyConstructMacro(Int,iVectorFieldViewInstance,NumberOfStreamLines,nl),
	iViewSubjectPropertyConstructMacro(Int,iVectorFieldViewInstance,SourceType,st),
	iViewSubjectPropertyConstructMacro(Float,iVectorFieldViewInstance,SourceOpacity,so),
	iViewSubjectPropertyConstructMacro(Vector,iVectorFieldViewInstance,SourceDirection,sd), 
	iViewSubjectPropertyConstructMacro(Vector,iVectorFieldViewInstance,SourcePosition,sx),
	iViewSubjectPropertyConstructMacro(Double,iVectorFieldViewInstance,SourceSize,ss),
	iViewSubjectPropertyConstructMacro(Bool,iVectorFieldViewInstance,ShowSourceObject,sso),
	iViewSubjectPropertyConstructMacro(Int,iVectorFieldViewInstance,GlyphBaseSize,gbs)
{
	this->GetViewModule()->GetMarkerObject()->AddUser(this);
}


iVectorFieldViewSubject::~iVectorFieldViewSubject()
{
	this->GetViewModule()->GetMarkerObject()->RemoveUser(this);
}

