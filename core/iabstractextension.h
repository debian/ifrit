/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IABSTRACTEXTENSION_H
#define IABSTRACTEXTENSION_H


#include "icolor.h"
#include "istring.h"

class iAnimator;
class iAnimatorScript;
class iCrossSectionViewSubject;
class iDataReader;
class iDataReaderExtension;
class iDataType;
class iObjectType;
class iParticleViewSubject;
class iPicker;
class iPickerExtension;
class iSurfaceViewSubject;
class iTensorFieldViewSubject;
class iVectorFieldViewSubject;
class iViewModule;
class iViewModuleExtension;
class iViewObject;
class iViewSubject;
class iVolumeViewSubject;


class iAbstractExtension
{

	friend class iObjectFactory;

public:

	inline int GetId() const { return mId; }
	inline int GetSubjectCounter() const { return ++mSubjectCounter; }
	inline const iString& GetName() const { return mName; }

	static int DefineToId(int n);

protected:

	iAbstractExtension(int id, const iString& name);

	virtual iDataReaderExtension*			CreateDataReaderExtension(iDataReader *reader) const = 0;
	virtual iPickerExtension*				CreatePickerExtension(iPicker *picker) const;
	virtual iViewModuleExtension*			CreateViewModuleExtension(iViewModule *vm) const;

	virtual iCrossSectionViewSubject*		CreateCrossSectionSubject(iViewObject *obj, int index) const;
	virtual iParticleViewSubject*			CreateParticleSubject(iViewObject *obj, int index) const;
	virtual iSurfaceViewSubject*			CreateSurfaceSubject(iViewObject *obj, int index) const;
	virtual iTensorFieldViewSubject*		CreateTensorFieldSubject(iViewObject *obj, int index) const;
	virtual iVectorFieldViewSubject*		CreateVectorFieldSubject(iViewObject *obj, int index) const;
	virtual iVolumeViewSubject*				CreateVolumeSubject(iViewObject *obj, int index) const;

	virtual iViewObject*					CreateSpecialObject(iViewModule *parent, const iString &name) const;

	//
	//  Creater helpers
	//
	iCrossSectionViewSubject*	NewCrossSectionSubject(iViewObject *obj, const iDataType& type) const;
	iParticleViewSubject*		NewParticleSubject(iViewObject *obj, const iDataType& type, const iColor& color = iColor::Invalid()) const;
	iSurfaceViewSubject*		NewSurfaceSubject(iViewObject *obj, const iDataType& type) const;
	iTensorFieldViewSubject*	NewTensorFieldSubject(iViewObject *obj, const iDataType& type, const iDataType& stype) const;
	iVectorFieldViewSubject*	NewVectorFieldSubject(iViewObject *obj, const iDataType& type, const iDataType& stype) const;
	iVolumeViewSubject*			NewVolumeSubject(iViewObject *obj, const iDataType& type) const;

private:

	const int mId;
	const iString mName;
	mutable int mSubjectCounter;
	
	iAbstractExtension(); // not implemented
	iAbstractExtension(const iAbstractExtension &); // not implemented
	void operator=(const iAbstractExtension&); // not implemented
};

//
//  Useful macros
//
#define iExtensionDeclareMacro(_class_) \
	public: \
		static int Id(){ return Self()->GetId(); } \
		static int SubjectId(int n = -1){ return (1+Self()->GetId())*1000 + (n == -1 ? Self()->GetSubjectCounter() : n); } \
		static const iString& Name(){ return Self()->GetName(); } \
	private: \
		_class_(); \
		static const int mAutoInstall; \
		static const iAbstractExtension* Self()

#define iExtensionDefineMacro(_name_,_class_) \
	const iAbstractExtension* _class_::Self() \
	{ \
		static iAbstractExtension* self = new _class_; \
		return self; \
	} \
	_class_::_class_() : iAbstractExtension(IEXTENSION_##_name_,#_name_){}

#define iExtensionDeclareClassMacro(_prefix_) \
	class _prefix_##Extension : public iAbstractExtension \
	{ \
		iExtensionDeclareMacro(_prefix_##Extension); \
	private: \
		virtual iDataReaderExtension*			CreateDataReaderExtension(iDataReader *reader) const; \
		virtual iCrossSectionViewSubject*		CreateCrossSectionSubject(iViewObject *obj, int index) const; \
		virtual iParticleViewSubject*			CreateParticleSubject(iViewObject *obj, int index) const; \
		virtual iSurfaceViewSubject*			CreateSurfaceSubject(iViewObject *obj, int index) const; \
		virtual iTensorFieldViewSubject*		CreateTensorFieldSubject(iViewObject *obj, int index) const; \
		virtual iVectorFieldViewSubject*		CreateVectorFieldSubject(iViewObject *obj, int index) const; \
		virtual iVolumeViewSubject*				CreateVolumeSubject(iViewObject *obj, int index) const; \
	}

#define iExtensionDefineClassMacro(_name_,_prefix_,_stype_,_vtype_,_ttype_,_ptype_) \
	iExtensionDefineMacro(_name_,_prefix_##Extension); \
	iCrossSectionViewSubject* _prefix_##Extension::CreateCrossSectionSubject(iViewObject *obj, int index) const \
	{ \
		if(index == 0) return this->NewCrossSectionSubject(obj,_stype_::DataType()); else return 0; \
	} \
	iParticleViewSubject* _prefix_##Extension::CreateParticleSubject(iViewObject *obj, int index) const \
	{ \
		if(index == 0) return this->NewParticleSubject(obj,_ptype_::DataType()); else return 0; \
	} \
	iSurfaceViewSubject* _prefix_##Extension::CreateSurfaceSubject(iViewObject *obj, int index) const \
	{ \
		if(index == 0) return this->NewSurfaceSubject(obj,_stype_::DataType()); else return 0; \
	} \
	iTensorFieldViewSubject* _prefix_##Extension::CreateTensorFieldSubject(iViewObject *obj, int index) const \
	{ \
		if(index == 0) return this->NewTensorFieldSubject(obj,_ttype_::DataType(),_stype_::DataType()); else return 0; \
	} \
	iVectorFieldViewSubject* _prefix_##Extension::CreateVectorFieldSubject(iViewObject *obj, int index) const \
	{ \
		if(index == 0) return this->NewVectorFieldSubject(obj,_vtype_::DataType(),_stype_::DataType()); else return 0; \
	} \
	iVolumeViewSubject* _prefix_##Extension::CreateVolumeSubject(iViewObject *obj, int index) const \
	{ \
		if(index == 0) return this->NewVolumeSubject(obj,_stype_::DataType()); else return 0; \
	} \
	iDataReaderExtension* _prefix_##Extension::CreateDataReaderExtension(iDataReader *reader) const \
	{ \
		return new _prefix_##DataReaderExtension(reader); \
	}

#endif // IABSTRACTEXTENSION_H

