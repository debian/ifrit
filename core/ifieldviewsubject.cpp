/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ifieldviewsubject.h"


#include "iactor.h"
#include "icolorbar.h"
#include "idatareader.h"
#include "idatasubject.h"
#include "imergedatafilter.h"
#include "iviewmodule.h"

#include <vtkDataSet.h>

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"
#include "igenericfilter.tlh"
#include "iproperty.tlh"


using namespace iParameter;


//
//  Single instance class
//
iFieldViewInstance::iFieldViewInstance(iFieldViewSubject *owner, int numactors) : iPaintedViewInstance(owner,numactors,1.0,1,ColorBarPriority::Field,false)
{
	mGlyphSize = 0.1;
	mGlyphSampleRate = 16;

	this->AddSecondaryDataType(owner->GetPaintingDataType());

	//
	//  Do VTK stuff
	//	
	mMergeDataFilter = iMergeDataFilter::New(this); IERROR_CHECK_MEMORY(mMergeDataFilter);
}


iFieldViewInstance::~iFieldViewInstance()
{
	mMergeDataFilter->Delete();
}


iFieldViewSubject* iFieldViewInstance::Owner() const
{
	return iRequiredCast<iFieldViewSubject>(INFO,this->iPaintedViewInstance::Owner());
}


bool iFieldViewInstance::SetGlyphSize(float v)
{ 
	if(v < 10.0)
	{
		mGlyphSize = v;
		this->UpdateGlyphSize();
		return true;
	}
	else return false;
}


bool iFieldViewInstance::SetGlyphSampleRate(int v)
{ 
	if(v>0 && v<1000) 
	{
		mGlyphSampleRate = v;
		this->UpdateGlyphSampleRate();
		return true;
	}
	else return false;
}


bool iFieldViewInstance::IsConnectedToPaintingData() const
{
	if(this->IsThereData())
	{
		return mMergeDataFilter->HasData(0);
	}
	else return true;
}


bool iFieldViewInstance::SyncWithDataBody()
{
	if(this->IsThereData())
	{
		mMergeDataFilter->SetInputData(1,this->GetViewModule()->GetReader()->GetData(this->Owner()->GetPaintingDataType()));
		mMergeDataFilter->SetInputData(0,this->GetSubject()->GetData());
		mMergeDataFilter->Update();
		this->ResetPipelineInput(mMergeDataFilter->OutputData());
	}

	return true;
}


iFieldViewSubject::iFieldViewSubject(iObject *parent, const iString &fname, const iString &sname, iViewModule *vm, const iDataType &type, const iDataType &sdt, unsigned int flags) : iPaintedViewSubject(parent,fname,sname,vm,type,sdt,flags,1,1),
	iViewSubjectPropertyConstructMacro(Float,iFieldViewInstance,GlyphSize,gs),
	iViewSubjectPropertyConstructMacro(Int,iFieldViewInstance,GlyphSampleRate,gr),
	iObjectConstructPropertyMacroQ(Bool,iFieldViewSubject,IsConnectedToScalars,cs)
{
	this->AddSecondaryDataType(mPaintingDataType);
}


iFieldViewSubject::~iFieldViewSubject()
{
}


bool iFieldViewSubject::GetIsConnectedToScalars() const
{
	int i;

	for(i=0; i<mInstances.Size(); i++)
	{
		if(!iRequiredCast<iFieldViewInstance>(INFO,mInstances[i])->IsConnectedToPaintingData()) return false;
	}

	return true;
}

