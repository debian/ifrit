/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Object that has material properties. This is a separate class as it serves as a parent to Material object.
//  It also isolated material interface so that it can be modified in the future.
//
#ifndef IMATERIALOBJECT_H
#define IMATERIALOBJECT_H


#include "iobject.h"


class iMaterial;


class iMaterialObject : public iObject
{
	
	friend class iMaterial;

public:
	
	vtkTypeMacro(iMaterialObject,iObject);

	iMaterial* GetMaterial() const { return mMaterial; }

	void UpdateAutomaticShading();

protected:
	
	iMaterialObject(iObject *parent, const iString &fname, const iString &sname, int level = 0);
	virtual ~iMaterialObject();

	virtual void UpdateAutomaticShadingBody();

private:

	iMaterial *mMaterial;
};

#endif // IMATERIALOBJECT_H

