/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iparticledownsampleiterator.h"


#include "imath.h"

#include <vtkDataArray.h>


//
//  Templates
//
#include "iarray.tlh"


//
//  Main class
//
iParticleDownsampleIterator::iParticleDownsampleIterator()
{
	mMaskId = -1;
	mDownsampleMode = 0;

	wIdTot = -1L;
	wIdLoc = wIdGlob = -1;
	wMask = 0;
}


iParticleDownsampleIterator::~iParticleDownsampleIterator()
{
}


void iParticleDownsampleIterator::SetDownsampleMode(int v)
{
	if(v>=0 && v<=5 && v!=mDownsampleMode)
	{
		mDownsampleMode = v;
	}
}

	
vtkIdType iParticleDownsampleIterator::GetNumGlobalTotal() const
{
	int i;
	vtkIdType n = 0L;
	for(i=0; i<mMasks.Size(); i++) if(!mMasks[i].Skip) n += mMasks[i].NumTotal;
	return n;
}


vtkIdType iParticleDownsampleIterator::GetNumGlobalSelected() const
{
	int i;
	vtkIdType n = 0;
	for(i=0; i<mMasks.Size(); i++) if(!mMasks[i].Skip) n += mMasks[i].NumSelected;
	return n;
}


void iParticleDownsampleIterator::CreateMasks(int n, vtkIdType ntot[], int df[])
{
	int i;
	for(i=0; i<n; i++) this->CreateOneMask(i,ntot[i],df[i]);
}


vtkIdType iParticleDownsampleIterator::CreateOneMask(int i, vtkIdType ntot, int df)
{
	if(i < 0)
	{
		IBUG_ERROR("Mask id should be non-negative.")
		return 0;
	}

	if(i == 0)
	{
		mMaskId = 0;
		while(mMasks.Size() > 0) mMasks.RemoveLast();
	}
	else
	{
		if(i != mMaskId+1)
		{
			IBUG_ERROR("iParticleDownsampleIterator::PrepareMask should be called sequentially.");
			return 0;
		}
		mMaskId++;
	}

	while(i > mMasks.MaxIndex()) mMasks.Add(Mask());

	mMasks[i].DownsampleFactor = df;
	if(df < 2)
	{
		mMasks[i].Dim1D = 1; 
		mMasks[i].Dim2D = 1;
		mMasks[i].NumSelected = (df == 1) ? ntot : 0;
		mMasks[i].RandomThreshold = (df == 1) ? 0.0 : 1.0;
	}
	else
	{
		switch(mDownsampleMode)
		{
		case 1:
			{
				vtkIdType n1 = (vtkIdType)(0.5+sqrt(double(ntot)));
				if(n1>1 && n1*n1>ntot) n1--;
				mMasks[i].Dim1D = n1;
				mMasks[i].Dim2D = 1;
				vtkIdType ns = (mMasks[i].Dim1D+df-1)/df;
				mMasks[i].NumSelected = ns*ns;
				mMasks[i].RandomThreshold = 1.0;
				break;
			}
		case 2:
			{
				vtkIdType n1 = (vtkIdType)(0.5+pow(double(ntot),1.0/3.0));
				if(n1>1 && n1*n1*n1>ntot) n1--;
				mMasks[i].Dim1D = n1; 
				mMasks[i].Dim2D = n1*n1; 
				vtkIdType ns = (mMasks[i].Dim1D+df-1)/df;
				mMasks[i].NumSelected = ns*ns*ns;
				mMasks[i].RandomThreshold = 1.0;
				break;
			}
		default:
			{
				mMasks[i].Dim1D = 1; 
				mMasks[i].Dim2D = 1;
				mMasks[i].NumSelected = (ntot+df-1)/df;
				mMasks[i].RandomThreshold = 1.0/df;
			}
		}
	}

	mMasks[i].NumTotal = ntot;
	mMasks[i].NumSkipped = mMasks[i].NumTotal - mMasks[i].NumSelected;
	if(i == 0)
	{
		mMasks[i].OffsetTotal = 0;
		mMasks[i].OffsetSelected = 0;
	}
	else
	{
		mMasks[i].OffsetTotal = mMasks[i-1].OffsetTotal + mMasks[i-1].NumTotal;
		mMasks[i].OffsetSelected = mMasks[i-1].OffsetSelected + mMasks[i-1].NumSelected;
	}
	
	mMasks[i].Skip = false;
	mMasks[i].Buffer = 0;
	mMasks[i].BufferWidth = mMasks[i].BufferIncrement = 0;

	return mMasks[i].NumSelected;
}


bool iParticleDownsampleIterator::AttachBuffer(int i, vtkDataArray *array, int offset)
{
	if(i>=0 && i<mMasks.Size() && array!=0 && offset>=0 && offset<array->GetNumberOfComponents())
	{
		mMasks[i].Buffer = (char *)array->GetVoidPointer(offset);
		mMasks[i].BufferWidth = array->GetNumberOfComponents();
		mMasks[i].BufferIncrement = mMasks[i].BufferWidth*array->GetDataTypeSize();
		return true;
	}
	else return false;
}


void iParticleDownsampleIterator::SkipMask(int n, bool s)
{
	if(n>=0 && n<mMasks.Size()) mMasks[n].Skip = s;
}


void iParticleDownsampleIterator::Start(int m)
{
	if(m<0 || m>mMasks.MaxIndex()) m = 0;

	wMask = mMasks.Data() + m;
	wIdTot = wMask->OffsetTotal - 1;
	wIdLoc = -1;
	wIdGlob = wMask->OffsetSelected - 1;
}


void iParticleDownsampleIterator::Stop(int m)
{
#ifndef I_NO_CHECK
	wIdTot++;
	wIdGlob++;
	if(m==-1 && wIdTot==wMask->OffsetTotal+wMask->NumTotal)
	{
		this->AdvanceCurrentMask();
	}
	if(m<0 || m>mMasks.MaxIndex()) m = mMasks.MaxIndex();
	if(wMask!=&mMasks[m] || wIdGlob!=wMask->NumSelected+wMask->OffsetSelected || wIdTot!=wMask->NumTotal+wMask->OffsetTotal)
	{
		IBUG_WARN("Internal check failed.");
	}
#endif
	wIdTot = -1L;
	wIdLoc = wIdGlob = -1;
	wMask = 0;
}


void iParticleDownsampleIterator::AdvanceCurrentMask()
{
	//
	//  Do nothing if requested to advance the last mask
	//
	if(wMask == &mMasks[mMasks.MaxIndex()]) return; 

	wMask++;
	while(wMask->Skip || wMask->NumTotal==0)
	{
		wIdTot += wMask->NumTotal;
		wIdGlob += wMask->NumSelected;
		wIdLoc = 0;
		if(wMask != &mMasks[mMasks.MaxIndex()]) wMask++; else break;
	}
}


#ifdef I_CHECK

void iParticleDownsampleIterator::ReportBug()
{
	IBUG_WARN("Internal check failed.");
}

#endif

