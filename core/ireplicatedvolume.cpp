/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ireplicatedvolume.h"


#include "ibuffer.h"
#include "ierror.h"
#include "iviewsubject.h"

#include <vtkFrustumCoverageCuller.h>
#include <vtkMatrix4x4.h>
#include <vtkRenderer.h>
#include <vtkTransform.h>
#include <vtkVolumeMapper.h>

//
//  Templates
//
#include "iarray.tlh"
#include "ibuffer.tlh"
#include "igenericprop.tlh"


//
//  Main class
//
iReplicatedVolume* iReplicatedVolume::New(iViewSubject *owner)
{
	IASSERT(owner);
	return new iReplicatedVolume(owner);
}


iReplicatedVolume::iReplicatedVolume(iViewSubject *owner) : iGenericProp<vtkVolume>(false), iReplicatedElement(false), iViewModuleComponent(owner->GetViewModule())
{
	//
	//  Must create property first - it is not created by default
	//
	IASSERT(this->GetProperty());

	//
	//  Create main replica
	//
	this->CreateReplica(0,0,0);
	this->ReplicateAs(owner);

	mCuller = vtkFrustumCoverageCuller::New(); IERROR_CHECK_MEMORY(mCuller);
	mCuller->SetSortingStyleToBackToFront();
}


iReplicatedVolume::~iReplicatedVolume()
{
	mCuller->Delete();
	while(mReplicas.Size() > 0) mReplicas.RemoveLast().Volume->Delete();
}


void iReplicatedVolume::CreateReplica(int i, int j, int k)
{
	mReplicas.Add(Replica());

	Replica &tmp = mReplicas[mReplicas.MaxIndex()];
	tmp.Pos[0] = i;
	tmp.Pos[1] = j;
	tmp.Pos[2] = k;
	tmp.Volume = vtkVolume::New();
	tmp.Volume->ShallowCopy(this);
	tmp.Volume->SetPosition(2.0*tmp.Pos[0],2.0*tmp.Pos[1],2.0*tmp.Pos[2]);
}


double* iReplicatedVolume::GetBounds()
{
	int i;
	
	vtkVolume::GetBounds();

	if(this->Bounds != 0)
	{
		for(i=0; i<3; i++)
		{
			this->Bounds[2*i+0] -= 2.0*mReplicationFactors[2*i+0];
			this->Bounds[2*i+1] += 2.0*mReplicationFactors[2*i+1];
		}
	}

	return this->Bounds;
}


void iReplicatedVolume::UpdateGeometry(vtkViewport *vp)
{
	int initialized = 0;
	int i, n = mReplicas.Size();

	vtkProp** buf = new vtkProp*[n]; IERROR_CHECK_MEMORY(buf);
	for(i=0; i<mReplicas.Size(); i++)
	{
		if(mReplicas[i].Volume->GetMTime() < this->GetMTime())
		{
			mReplicas[i].Volume->ShallowCopy(this);
			mReplicas[i].Volume->SetPosition(2.0*mReplicas[i].Pos[0],2.0*mReplicas[i].Pos[1],2.0*mReplicas[i].Pos[2]);
		}
		buf[i] = mReplicas[i].Volume;
	}

	//
	//  Reorder volumes
	//
	mCuller->Cull(iRequiredCast<vtkRenderer>(INFO,vp),buf,n,initialized);

	//
	//  n can change inside Cull()
	//
	this->RemoveAllComponents();
	for(i=0; i<n; i++) this->AppendComponent(buf[i]);
	delete [] buf;
}


void iReplicatedVolume::UpdateReplicasBody()
{
	int i, j, k;
	Replica tmp;

	//
	//  Add needed actors
	//
	for(k=-mReplicationFactors[4]; k<=mReplicationFactors[5]; k++)
	{
		for(j=-mReplicationFactors[2]; j<=mReplicationFactors[3]; j++)
		{
			for(i=-mReplicationFactors[0]; i<=mReplicationFactors[1]; i++)
			{
				if(i<-mOldReplicationFactors[0] || i>mOldReplicationFactors[1] || j<-mOldReplicationFactors[2] || j>mOldReplicationFactors[3] || k<-mOldReplicationFactors[4] || k>mOldReplicationFactors[5])
				{
					this->CreateReplica(i,j,k);
				}
			}
		}
	}

	//
	//  Delete unneeded actors
	//
	for(k=0; k<mReplicas.Size(); k++)
	{
		if(mReplicas[k].Pos[0]<-mReplicationFactors[0] || mReplicas[k].Pos[0]>mReplicationFactors[1] || mReplicas[k].Pos[1]<-mReplicationFactors[2] || mReplicas[k].Pos[1]>mReplicationFactors[3] || mReplicas[k].Pos[2]<-mReplicationFactors[4] || mReplicas[k].Pos[2]>mReplicationFactors[5])
		{
			mReplicas[k].Volume->Delete();
			mReplicas.Remove(k);
		}
	}
}

