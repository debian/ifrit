/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IRULER_H
#define IRULER_H


#include "iaxis.h"
#include "iviewmoduletool.h"


#include "istring.h"

class iRuler;


class iRulerActor: public iAxis, public iViewModuleComponent
{
	
	friend class iRuler;

public:
	
	vtkTypeMacro(iRulerActor,iAxis);
	
	void SetBaseScale(float s);

	void SetScale(float s);
	void SetTitle(const iString &title);

protected:

	virtual void UpdateGeometry(vtkViewport *vp);
	virtual void UpdateOverlay(vtkViewport *vp);

private:
	
	iRulerActor(iRuler *owner);
	
	iRuler *mOwner;
	bool mStarted;
	float mBaseScale;
};


class iRuler : public iViewModuleTool
{
	
public:

	vtkTypeMacro(iRuler,iViewModuleTool);
	static iRuler* New(iViewModule *vm = 0);
		
	iObjectPropertyMacro1S(Scale,Float);
	iObjectPropertyMacro1S(Title,String);

	inline iRulerActor* GetActor() const { return mActor; }

protected:

	virtual ~iRuler();

private:
	
	iRuler(iViewModule *vm, const iString &fname, const iString &sname);

	virtual bool ShowBody(bool s);

	//
	//  Actors displayed by this class
	//
	iRulerActor *mActor;
};

#endif // IRULER_H
