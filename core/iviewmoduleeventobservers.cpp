/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iviewmoduleeventobservers.h"


#include "idatareader.h"
#include "ierror.h"
#include "ishell.h"
#include "iviewmodule.h"

#include <vtkAlgorithm.h>
#include <vtkMultiThreader.h>
#include <vtkRenderWindow.h>
#include <vtkTimerLog.h>


bool iRenderEventObserver::mTheseAreBlocked = false;


using namespace iParameter;


//
//  iRenderEventObserver class
//
iRenderEventObserver* iRenderEventObserver::New(iViewModule *vm)
{
	IASSERT(vm);
	return iRequiredCast<iRenderEventObserver>(INFO,vm->GetShell()->CreateViewModuleEventObserver(iParameter::ViewModuleObserver::Render,vm));
}


iRenderEventObserver::iRenderEventObserver(iViewModule *vm) : iEventObserver(), iViewModuleComponent(vm)
{
	mCancelled = mInProgress = false;
	mRenderTimer = vtkTimerLog::New(); IERROR_CHECK_MEMORY(mRenderTimer);
	mRenderTime = 0.0f;
	mInteractive = true;

	mMasterThreadId = vtkMultiThreader::GetCurrentThreadID();
}


iRenderEventObserver::~iRenderEventObserver()
{
	mRenderTimer->Delete();
}


void iRenderEventObserver::ExecuteBody(vtkObject *caller, unsigned long event, void *)
{
	if(mTheseAreBlocked) return;

	switch(event)
	{
	case vtkCommand::StartEvent:
		{
			if(!mInProgress)
			{
				mInProgress = true;
				this->OnStart();
				mCancelled = false;
				mRenderTimer->StartTimer();
			}
			break;
		}
	case vtkCommand::AbortCheckEvent:
	case vtkCommand::ProgressEvent:
		{
			if(mInProgress)
			{
				if(mMasterThreadId == vtkMultiThreader::GetCurrentThreadID())
				{
					//
					//  I am the master
					//
					mCancelled = this->CheckAbort();
					if(mCancelled)
					{
						vtkRenderWindow *rw = vtkRenderWindow::SafeDownCast(caller);
						if(rw != 0) rw->SetAbortRender(1);
					}
				}

				if(mCancelled)
				{
					vtkAlgorithm *alg = vtkAlgorithm::SafeDownCast(caller);
					if(alg != 0)
					{
#ifdef I_DEBUG
						int threadId = vtkMultiThreader::GetCurrentThreadID();

#endif
						alg->SetAbortExecute(1);
					}
				}
			}
			break;
		}
	case vtkCommand::EndEvent:
		{
			if(mInProgress)
			{
				mRenderTimer->StopTimer();
				mRenderTime = mRenderTimer->GetElapsedTime();
				mInProgress = false;
				this->OnFinish();
				if(mCancelled)
				{
					//
					//  If rendering was cancelled, then different parts of the pipeline
					//  remain in different stages of completion, which confuses VTK.
					//  To avoid that we modify the time stamp of the data, so that next
					//  rendering starts from scratch. It is not an elegant solution,
					//  but it works.
					//
					this->GetViewModule()->GetReader()->ResetPipeline();
					this->OutputText(MessageType::Information,"Render aborted.");
				}
			}
			break;
		}
	case vtkCommand::UserEvent:
		{
			this->PostFinished();
			break;
		}
	}
}


//
//  iPickEventObserver class
//
iPickEventObserver* iPickEventObserver::New(iViewModule *vm)
{
	IASSERT(vm);
	return iRequiredCast<iPickEventObserver>(INFO,vm->GetShell()->CreateViewModuleEventObserver(iParameter::ViewModuleObserver::Pick,vm));
}


iPickEventObserver::iPickEventObserver(iViewModule *vm) : iEventObserver(), iViewModuleComponent(vm)
{
}


void iPickEventObserver::ExecuteBody(vtkObject *caller, unsigned long eventId, void *)
{
	switch (eventId)
	{
	case vtkCommand::StartPickEvent: 
		{ 
			this->OnStart();
			break; 
		}
	case vtkCommand::EndPickEvent: 
		{ 
			this->OnFinish();
			break; 
		}
	}
}

