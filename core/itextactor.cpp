/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "itextactor.h"


#include "ibitmaptextsubject.h"
#include "ierror.h"
#include "imath.h"
#include "ioverlayhelper.h"
#include "irendertool.h"
#include "ivectortextsubject.h"

#include <vtkRenderer.h>
#include <vtkTextProperty.h>

//
//  Templates
//
#include "iarray.tlh"
#include "igenericprop.tlh"


using namespace iParameter;


iTextActor* iTextActor::New(iRenderTool *rv)
{
	IASSERT(rv);
	return new iTextActor(rv);
}


iTextActor::iTextActor(iRenderTool *rv) : iGenericProp<vtkProp>(false), mRenderTool(rv), mOverlayHelper(rv)
{
	mPos[0] = mPos[1] = 0.0;
	mAngle = 0.0;
	mPadding = 0.0;

	mProperty = vtkTextProperty::New(); IERROR_CHECK_MEMORY(mProperty);
	mProperty->SetBold(1);
	mProperty->SetJustificationToLeft();
	mProperty->SetVerticalJustificationToBottom();

	mSubjects[0] = iBitmapTextSubject::New(this,rv); IERROR_CHECK_MEMORY(mSubjects[0]);
	mSubjects[1] = iVectorTextSubject::New(this,rv); IERROR_CHECK_MEMORY(mSubjects[1]);
	this->AppendComponent(mSubjects[0]);
	this->AppendComponent(mSubjects[1]);

	mCurrentSubject = 0;
	this->ToggleComponent(mSubjects[0]);

	this->PickableOff();
}


iTextActor::~iTextActor()
{
	int i;

	for(i=0; i<2; i++)
	{
		mSubjects[i]->Delete();
	}
	mProperty->Delete();
}


void iTextActor::SetPadding(float p)
{
	if(p >= 0.0f)
	{
		mPadding = p;
		this->Modified();
	}
}


void iTextActor::SetBold(bool s)
{
	mProperty->SetBold(s?1:0);
	this->Modified();
}


void iTextActor::SetJustification(int jx)
{
	if(jx < -1) jx = -1;
	if(jx > 1) jx = 1;

	mProperty->SetJustification(jx+1);
	this->Modified();
}


void iTextActor::SetJustification(int jx, int jy)
{
	this->SetJustification(jx);

	if(jy < -1) jy = -1;
	if(jy > 1) jy = 1;

	mProperty->SetVerticalJustification(jy+1);
	this->Modified();
}


void iTextActor::MoveJustification(int jx)
{
	float s[2];

	this->GetSize(mRenderTool->GetRenderer(),s);

	mPos[0] -= s[0]*(mProperty->GetJustification()-1);

	this->SetJustification(jx);
}


void iTextActor::SetPosition(float x, float y)
{
	mPos[0] = x;
	mPos[1] = y;
	this->Modified();
}


void iTextActor::SetAngle(float a)
{
	mAngle = a;
	//
	//  Current (hopefully temporal) solution for rotated texts
	//
	if(fabs(mAngle) < 1.0e-10)
	{
		mCurrentSubject = 0;
		this->ToggleComponent(mSubjects[0]);
	}
	else
	{
		mCurrentSubject = 1;
		this->ToggleComponent(mSubjects[1]);
	}
	this->Modified();
}


void iTextActor::SetText(const iString &s)
{
	mText = s;
	this->Modified();
}


void iTextActor::UpdateGeometry(vtkViewport *)
{
	//
	//  All work is done by subjects
	//
}


void iTextActor::GetSize(vtkViewport *vp, float s[2]) const
{
	mSubjects[mCurrentSubject]->GetSize(vp,s);
	if(mPadding > 0.0f)
	{
		s[0] += 2.0*mPadding;
		s[1] += 1.6*mPadding;
	}
}


void iTextActor::GetBounds(vtkViewport *vp, float b[4]) const
{
	float s[2];
	this->GetSize(vp,s);

	b[0] = mPos[0] - 0.5*s[0]*mProperty->GetJustification();
	b[1] = b[0] + s[0];
	b[2] = mPos[1] - 0.5*s[1]*mProperty->GetVerticalJustification();
	b[3] = b[2] + s[1];
}


void iTextActor::GetTextAnchor(vtkViewport *, float p[2]) const
{
	p[0] = mPos[0] + 1.0*mPadding;
	p[1] = mPos[1] + 1.0*mPadding;
}
