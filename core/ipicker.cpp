/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ipicker.h"


#include "iactor.h"
#include "idata.h"
#include "idataformatter.h"
#include "idatalimits.h"
#include "idatareader.h"
#include "idatasubject.h"
#include "ierror.h"
#include "iobjectfactory.h"
#include "iprobefilter.h"
#include "iviewmodule.h"
#include "iviewmoduleeventobservers.h"
#include "iviewsubject.h"
#include "iviewsubjectobserver.h"

#include <vtkCellArray.h>
#include <vtkCellPicker.h>
#include <vtkFloatArray.h>
#include <vtkImageData.h>
#include <vtkPointData.h>
#include <vtkPointLocator.h>
#include <vtkPointPicker.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProp.h>
#include <vtkProperty.h>
#include <vtkPropPicker.h>
#include <vtkSphereSource.h>

//
//  templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


//
//  Picker must be an object so that it can be created by the ObjectFactory
//


namespace iPicker_Private
{
	const int NumPickMethods = 3;

	class CellPicker : public vtkCellPicker
	{

	public:

		static CellPicker* New()
		{ 
			return new CellPicker;
		}

		virtual vtkAssemblyPath* GetPath()
		{
			return 0; // we do not highlight the picked prop
		}
	};


	class PointPicker : public vtkPointPicker
	{

	public:

		static PointPicker* New()
		{ 
			return new PointPicker;
		}

		virtual vtkAssemblyPath* GetPath()
		{
			return 0; // we do not highlight the picked prop
		}
	};


	class ObjectPicker : public vtkPropPicker
	{

	public:

		static ObjectPicker* New()
		{ 
			return new ObjectPicker;
		}

		virtual vtkAssemblyPath* GetPath()
		{
			return 0; // we do not highlight the picked prop
		}
	};


	class PickHandler : public vtkPicker
	{

	public:

		static PickHandler* New(iPicker *parent)
		{
			return new PickHandler(parent);
		}

		virtual vtkAssemblyPath* GetPath()
		{
			return 0; // we do not highlight the picked prop
		}

		virtual int Pick(double x, double y, double z, vtkRenderer *ren)
		{
			int pm = mParent->GetPickMethod();

			if(pm>=0 && pm<NumPickMethods)
			{
				vtkPicker *p = vtkPicker::SafeDownCast(mDevices[pm]);
				if(p != 0) p->SetTolerance(this->GetTolerance());

				this->InvokeEvent(vtkCommand::StartPickEvent,0);
				int ret = mDevices[pm]->Pick(x,y,z,ren);
				mDevices[pm]->GetPickPosition(this->PickPosition);
				this->SetPath(mDevices[pm]->vtkAbstractPropPicker::GetPath());
				mParent->UpdateReport();
				this->InvokeEvent(vtkCommand::EndPickEvent,0);

				return ret;
			}
			else
			{
				vtkErrorMacro("Invalid pick method.");
				return 0;
			}
		}

		virtual void Modified()
		{
			int i;
			for(i=0; i<NumPickMethods; i++) mDevices[i]->Modified();
			this->vtkAbstractPropPicker::Modified();
		}

	private:

		PickHandler(iPicker *parent)
		{
			IASSERT(parent);
			mParent = parent;

			//
			//  Pickers
			//
			mDevices[0] = CellPicker::New(); IERROR_CHECK_MEMORY(mDevices[0]);
			mDevices[1] = PointPicker::New(); IERROR_CHECK_MEMORY(mDevices[1]);
			mDevices[2] = ObjectPicker::New(); IERROR_CHECK_MEMORY(mDevices[2]);

			//
			//  Observer
			//
			mObserver = iPickEventObserver::New(mParent->GetViewModule()); IERROR_CHECK_MEMORY(mObserver);
			this->AddObserver(vtkCommand::StartPickEvent,mObserver);
			this->AddObserver(vtkCommand::EndPickEvent,mObserver);
			int i;
			for(i=0; i<NumPickMethods; i++)
			{
				mDevices[i]->AddObserver(vtkCommand::AbortCheckEvent,mParent->GetViewModule()->GetRenderEventObserver());
			}
		}

		~PickHandler()
		{
			int i;
			for(i=0; i<NumPickMethods; i++) mDevices[i]->Delete();
			mObserver->Delete();
		}

		iPicker *mParent;
		iPickEventObserver *mObserver;
		vtkAbstractPropPicker *mDevices[NumPickMethods];
	};
};


using namespace iParameter;
using namespace iPicker_Private;

	
iPicker* iPicker::New(iViewModule *vm)
{
	static iString LongName("Picker");
	static iString ShortName("pi");

	IASSERT(vm);
	iPicker *tmp = new iPicker(vm,LongName,ShortName); IERROR_CHECK_MEMORY(tmp);
	iObjectFactory::InstallExtensions(tmp);
	return tmp;
}


iPicker::iPicker(iViewModule *vm, const iString &fname, const iString &sname) : iExtendableObject(vm,fname,sname), iViewModuleComponent(vm), mDataFormatter(vm), mPosition(vm),
	Position(iObject::Self(),static_cast<iPropertyQuery<iType::Vector>::GetterType>(&iPicker::GetBoxPosition),"Position","x"),
	iObjectConstructPropertyMacroS(Float,iPicker,Accuracy,a),
	iObjectConstructPropertyMacroS(Int,iPicker,PickMethod,m),
	iObjectConstructPropertyMacroS(Float,iPicker,PointSize,ps)
{
	mRenderMode = RenderMode::Self;

	mSubjectName = "";
	mDataTypePointer = 0;
	mPickMethod = PickMethod::Point;

	//
	//  Handler
	//
	mHandler = PickHandler::New(this); IERROR_CHECK_MEMORY(mHandler);
	mAccuracy = 0.02;
	this->SetAccuracy(mAccuracy);

	//
	//  Graphical representation
	//
	mPointActor = iActor::New(); IERROR_CHECK_MEMORY(mPointActor);
	mPointSource = vtkSphereSource::New(); IERROR_CHECK_MEMORY(mPointSource);

	mPointSize = 0.1;
	this->SetPointSize(mPointSize);
	mPointSource->SetRadius(0.5);
	mPointActor->GetProperty()->SetColor(0.9,0.9,0.9);
	mPointActor->VisibilityOff();
	mPointActor->PickableOff();

	this->GetViewModule()->AddObject(mPointActor);

	//
	//  PolyData inout for a probe filter
	//
	mProbeInput = vtkPolyData::New(); IERROR_CHECK_MEMORY(mProbeInput);
	vtkCellArray *v = vtkCellArray::New(); IERROR_CHECK_MEMORY(v);
	v->InsertNextCell(1);
	v->InsertCellPoint(0);
	mProbeInput->SetVerts(v);
	v->Delete();
	vtkPoints *p = vtkPoints::New(VTK_DOUBLE); IERROR_CHECK_MEMORY(p);
	p->SetNumberOfPoints(1);
	p->SetPoint(0,0.0,0.0,0.0);
	mProbeInput->SetPoints(p);
	p->Delete();
}


iPicker::~iPicker()
{
	mHandler->Delete();
	this->GetViewModule()->RemoveObject(mPointActor);
	mPointActor->Delete();
	mPointSource->Delete();
	mProbeInput->Delete();
}


void iPicker::Modified()
{
	mHandler->Modified();
}


void iPicker::UpdateReport()
{
	mSubjectName = "Unknown";
	mPosition = mHandler->GetPickPosition();

	this->SetPointSize(mPointSize);
	mPointActor->SetPosition(mPosition);

	mDataFormatter->ClearReport();
	this->UpdateReportBody(); // this can replace mPointObject via its extensions

	mPointActor->SetInputConnection(mPointSource->GetOutputPort());
	mPointActor->SetScaled(true);
	mPointActor->SetPosition(mPosition);

	//
	//  Check if extensions want to modify the point actor
	//
	if(mDataTypePointer != 0)
	{
		int i;
		for(i=0; i<mExtensions.Size(); i++) if(iRequiredCast<iPickerExtension>(INFO,mExtensions[i])->IsUsingData(*mDataTypePointer))
		{
			iRequiredCast<iPickerExtension>(INFO,mExtensions[i])->ModifyActor(*mDataTypePointer,mPointActor); // the last installed extension prevails
		}
	}

	this->ShowPickedPoint(mDataTypePointer != 0);
}


void iPicker::UpdateReportBody()
{
	iString s1, s2;

	//
	//  Get the object info
	//
	vtkProp *obj = mHandler->GetViewProp();
	if(obj == 0) 
	{
		mSubjectName = "";
		mPointActor->VisibilityOff(); 
		return;
	}
	iViewSubjectObserver *obs = iRequiredCast<iViewSubjectObserver>(INFO,obj->GetCommand(1));

	if(obs != 0)
	{
		mSubjectName = obs->GetSubjectName();
		mDataTypePointer = &obs->GetSubjectDataType();
	}
	else
	{
		mSubjectName = "Unknown";
		mDataTypePointer = 0;
	}

	//
	//  Get the data info
	//
	if(mDataTypePointer == 0) return;

	iDataSubject *subject = this->GetViewModule()->GetReader()->GetSubject(*mDataTypePointer);
	iDataLimits *lim = subject->GetLimits();

	//
	//  Use the ProbeFilter to find attribute values
	//
	mProbeInput->GetPoints()->SetPoint(0,mPosition);
	iProbeFilter *probe = subject->CreateProbeFilter(obs->GetSubject());
	probe->SetInputData(mProbeInput);
	probe->SetSourceData(subject->GetData());
	probe->Update();
	vtkPolyData *out = probe->GetPolyDataOutput();

	if(out==0 || out->GetPointData()==0)
	{
		IBUG_WARN("Unable to probe data for the picked object");
		return;
	}

	//
	//  Ask extensions to provide extra information
	//
	int i;
	for(i=0; i<mExtensions.Size(); i++) if(iRequiredCast<iPickerExtension>(INFO,mExtensions[i])->IsUsingData(*mDataTypePointer))
	{
		iRequiredCast<iPickerExtension>(INFO,mExtensions[i])->AddInfo(*mDataTypePointer);
	}

	vtkDataArray *d = 0;
	int nvar = 0;
	if(subject->IsThereScalarData())
	{
		d = out->GetPointData()->GetScalars();
		nvar = lim->GetNumVars();
		if(d==0 || nvar<1 || d->GetNumberOfComponents()!=nvar || !d->IsA("vtkFloatArray"))
		{
			IBUG_WARN("Unable to probe data for the picked object");
			return;
		}
		float *v = (float *)d->GetVoidPointer(0);
		mDataFormatter->FormatScalarData(lim,nvar,v);
	}

	if(subject->IsThereVectorData())
	{
		d = out->GetPointData()->GetVectors();
		nvar = 3;
		if(d==0 || nvar<1 || d->GetNumberOfComponents()!=nvar || !d->IsA("vtkFloatArray"))
		{
			IBUG_WARN("Unable to probe data for the picked object");
			return;
		}
		float *v = (float *)d->GetVoidPointer(0);
		mDataFormatter->FormatVectorData(lim,-1,v);
	}

	if(subject->IsThereTensorData())
	{
		d = out->GetPointData()->GetTensors();
		nvar = 9;
		if(d==0 || nvar<1 || d->GetNumberOfComponents()!=nvar || !d->IsA("vtkFloatArray"))
		{
			IBUG_WARN("Unable to probe data for the picked object");
			return;
		}
		float *v = (float *)d->GetVoidPointer(0);
		mDataFormatter->FormatTensorData(lim,-1,v);
	}

	probe->Delete();
}


bool iPicker::SetPointSize(float s)
{
	if(s > 0.0)
	{
		mPointSize = s;
		mPointActor->SetScaled(true);
		mPointActor->SetBasicScale(mPointSize);
		this->Modified();
		return true;
	}
	else return false;
}


void iPicker::ShowPickedPoint(bool s)
{
	if(!mSubjectName.IsEmpty() && s)
	{
		mPointActor->VisibilityOn();
	}
	if(!s)
	{
		mPointActor->VisibilityOff();
	}
	this->RequestRender();
}


bool iPicker::SetPickMethod(int s)
{
	if(s>=0 && s<NumPickMethods)
	{
		mPickMethod = s;
		return true;
	}
	else return false;
}


bool iPicker::SetAccuracy(float s)
{
	if(s > 0.0f)
	{
		mHandler->SetTolerance(s);
		return true;
	}
	else return false;
}


//
//  Extension class
//
iObjectExtensionAbstractConstructorBeginMacro(iPicker)
{
}

