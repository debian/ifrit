/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iwriter.h"


#include "ierror.h"
#include "ipostscriptwriter.h"
#include "ishell.h"
#include "ishelleventobservers.h"
#include "iviewmodule.h"
#include "ivtk.h"

#include <vtkDirectory.h>
#include <vtkGenericMovieWriter.h>
#include <vtkImageData.h>

#include <vtkBMPWriter.h>
#include <vtkJPEGWriter.h>
#include <vtkPNGWriter.h>
#include <vtkPNMWriter.h>
#include <vtkTIFFWriter.h>

#ifdef IVTK_SUPPORTS_AVI
#include <vtkAVIWriter.h>
#endif

#ifdef IVTK_SUPPORTS_MPEG
#include <vtkMPEG2Writer.h>
#endif


//
//  Templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


using namespace iParameter;


//
//  Declare static members
//
int iWriter::mImageIndex = 1;
int iWriter::mMovieIndex = 1;
iString iWriter::mImageRootName;
iString iWriter::mMovieRootName;


//
//  Helper class
//
class iGenericMultiImageMovieWriter : public vtkObjectBase, public iViewModuleComponent
{

public:

	vtkTypeMacro(iGenericMultiImageMovieWriter,vtkObjectBase);

	void Start()
	{
		mStarted = true;
	}

	void WriteFrame(const iStereoImageArray& images, const iString &root);

	void Finish()
	{
		int i;

		for(i=0; i<mImageWriters.Size(); i++) if(mImageWriters[i]->GetInput() != 0)
		{
			mImageWriters[i]->End();
			if(mImageWriters[i]->GetError() != 0)
			{
				IBUG_WARN("Unable to write a movie file: different frames have varied number of images");
				return;
			}
		}
		mStarted = false;
	}

protected:

	iGenericMultiImageMovieWriter(iViewModule *vm, vtkImageWriter *writer) : iViewModuleComponent(vm)
	{
		IASSERT(vm);
		mStarted = false;
		mParentWriter = writer;
	}

	virtual ~iGenericMultiImageMovieWriter()
	{
		while(mImageWriters.Size() > 0) mImageWriters.RemoveLast()->Delete();
	}

	virtual vtkGenericMovieWriter* CreateWriter() const = 0;  // factory method
	virtual const iString& GetFileSuffix() const = 0;

	bool mStarted;
	iArray<vtkGenericMovieWriter*> mImageWriters;
	vtkImageWriter *mParentWriter;
};


namespace iWriter_Private
{
	class FileSetMovieWriter : public vtkGenericMovieWriter
	{
	public:

		static FileSetMovieWriter* New(vtkImageWriter *writer)
		{
			return new FileSetMovieWriter(writer);
		}

		virtual void Start()
		{
			mRoot = iString(FileName);
			if(mRoot[mRoot.Length()-1] != '-')
			{
				vtkDirectory *d = vtkDirectory::New(); IERROR_CHECK_MEMORY(d);
				if(d->Open(mRoot.ToCharPointer()) == 0)
				{
					if(vtkDirectory::MakeDirectory(mRoot.ToCharPointer())==0 || d->Open(mRoot.ToCharPointer())==0)
					{
						mRoot += "-";
					}
					else
					{
						mRoot += "/frame-";
					}
				}
				else
				{
					mRoot += "/frame-";
				}
			}
			else
			{
			}

			mFrame = 0;
		}

		virtual void Write()
		{
			iString suf = ".png";

			if(mWriter->IsA("vtkJPEGWriter") != 0) suf = ".jpg";
			if(mWriter->IsA("vtkPNMWriter")  != 0) suf = ".ppm";
			if(mWriter->IsA("vtkBMPWriter")  != 0) suf = ".bmp";
			if(mWriter->IsA("vtkTIFFWriter") != 0) suf = ".tif";
			if(mWriter->IsA("vtkEPSWriter")  != 0) suf = ".eps";

			mWriter->SetInputData(this->GetInput());
			mWriter->SetFileName((mRoot+iString::FromNumber(mFrame,"%05d")+suf).ToCharPointer());
			mWriter->Write();
			mFrame++;
		}

		virtual void End()
		{
		}

	protected:

		FileSetMovieWriter(vtkImageWriter *writer)
		{
			IASSERT(writer);

			mWriter = writer; 
			mWriter->Register(this);
		}

		~FileSetMovieWriter()
		{
			mWriter->Delete();
		}

		int mFrame;
		iString mRoot;
		vtkImageWriter *mWriter;
	};

	template <class T>
	class MultiImageMovieWriter : public iGenericMultiImageMovieWriter
	{

	public:

		static MultiImageMovieWriter<T>* New(iViewModule *vm, const iString &suf, vtkImageWriter *writer = 0)
		{
			return new MultiImageMovieWriter<T>(vm,suf,writer);
		}

	protected:

		MultiImageMovieWriter(iViewModule *vm, const iString &suf, vtkImageWriter *writer) : iGenericMultiImageMovieWriter(vm,writer)
		{
			if(!suf.IsEmpty()) mSuffix = "." + suf;
		}

		virtual vtkGenericMovieWriter* CreateWriter() const;

		virtual const iString& GetFileSuffix() const
		{
			return mSuffix;
		}

		iString mSuffix;
	};

	
	template <>
	vtkGenericMovieWriter* MultiImageMovieWriter<FileSetMovieWriter>::CreateWriter() const
	{
		return FileSetMovieWriter::New(mParentWriter);
	}


	template <class T>
	vtkGenericMovieWriter* MultiImageMovieWriter<T>::CreateWriter() const
	{
		return T::New();
	}


	iString FrameFileName(int i, const iString &tag, const iString &root)
	{
		if(tag.IsEmpty())
		{
			if(i == 0)
			{
				//
				//  Main view
				//
				return root;
			}
			else
			{
				//
				//  Other views
				//
				return root + "-v" + iString::FromNumber(i+1,(i<99)?"%02d":"%d");
			}
		}
		else
		{
			return root + "-" + tag;
		}
	}
};


using namespace iWriter_Private;


void iGenericMultiImageMovieWriter::WriteFrame(const iStereoImageArray& images, const iString &root)
{
	if(mStarted)
	{
		while(2*images.Size() < mImageWriters.Size()) mImageWriters.RemoveLast()->Delete();
		while(2*images.Size() > mImageWriters.Size())
		{
			vtkGenericMovieWriter *tmp = this->CreateWriter(); IERROR_CHECK_MEMORY(tmp);
			tmp->AddObserver(vtkCommand::StartEvent,this->GetShell()->GetExecutionEventObserver());
			tmp->AddObserver(vtkCommand::ProgressEvent,this->GetShell()->GetExecutionEventObserver());
			tmp->AddObserver(vtkCommand::EndEvent,this->GetShell()->GetExecutionEventObserver());
			mImageWriters.Add(tmp);
		}

		int i;
		iString name;
		for(i=0; i<mImageWriters.Size(); i+=2)
		{
			name = FrameFileName(i/2,images[i/2].GetTag(),root);
			
			if(images[i/2].IsStereo())
			{
				mImageWriters[i]->SetFileName((name+"-left"+this->GetFileSuffix()).ToCharPointer());
			}
			else
			{
				mImageWriters[i]->SetFileName((name+this->GetFileSuffix()).ToCharPointer());
			}
			mImageWriters[i]->SetInputData(images[i/2].LeftEye().DataObject());
			mImageWriters[i]->Start();
			//
			//  Right eye
			//
			if(images[i/2].IsStereo())
			{
				mImageWriters[i+1]->SetInputData(images[i/2].RightEye().DataObject());
				mImageWriters[i+1]->SetFileName((name+"-right"+this->GetFileSuffix()).ToCharPointer());
				mImageWriters[i+1]->Start();
			}
			else
			{
				mImageWriters[i+1]->SetInputData(0);
			}
		}
		mStarted = false;
	}

	if(!mStarted && 2*images.Size()!=mImageWriters.Size())
	{
		IBUG_WARN("Unable to write a movie file: different frames have varied number of images.");
		return;
	}

	int i;
	for(i=0; i<mImageWriters.Size(); i+=2)
	{
		mImageWriters[i]->SetInputData(images[i/2].LeftEye().DataObject());
		mImageWriters[i]->Write();
		mImageWriters[i]->SetInputData(0);
		if(mImageWriters[i]->GetError() != 0)
		{
			IBUG_WARN("Unable to write a movie file: VTK reported an unidentified error.");
			return;
		}
		if(images[i/2].IsStereo())
		{
			mImageWriters[i+1]->SetInputData(images[i/2].RightEye().DataObject());
			mImageWriters[i+1]->Write();
			mImageWriters[i+1]->SetInputData(0);
			if(mImageWriters[i+1]->GetError() != 0)
			{
				IBUG_WARN("Unable to write a movie file: VTK reported an unidentified error.");
				return;
			}
		}
	}
}


//
//  Main class
//
iWriter* iWriter::New(iViewModule *vm)
{
	static iString LongName("ImageWriter");
	static iString ShortName("iw");

	IASSERT(vm);
	return new iWriter(vm,LongName,ShortName);
}


iWriter::iWriter(iViewModule *vm, const iString &fname, const iString &sname) : iObject(vm,fname,sname), iViewModuleComponent(vm),
	iObjectConstructPropertyMacroS(Int,iWriter,ImageFormat,f),
	iObjectConstructPropertyMacroS(Int,iWriter,MovieOutput,mo),
	iObjectConstructPropertyMacroS(Int,iWriter,PostScriptPaperFormat,pf),
	iObjectConstructPropertyMacroS(Int,iWriter,PostScriptOrientation,po),
	iObjectConstructPropertyMacroS(String,iWriter,ImageRootName,in),
	iObjectConstructPropertyMacroS(String,iWriter,MovieRootName,mn),
	iObjectConstructPropertyMacroA(iWriter,Write,w)
{
	mRenderMode = RenderMode::NoRender;

	mPostScriptPaperFormat = 9;
	mPostScriptOrientation = 0;

	mImageWriter = 0;
	mMovieWriter = 0;

	this->SetImageFormat(ImageFormat::PNG);
	this->SetMovieOutput(MovieOutput::Image);

	if(mImageRootName.IsEmpty()) this->SetImageRootName("+image");
	if(mMovieRootName.IsEmpty()) this->SetMovieRootName("+movie");
}


iWriter::~iWriter()
{
	if(mImageWriter != 0) mImageWriter->Delete();
	if(mMovieWriter != 0) mMovieWriter->Delete();
}


bool iWriter::SetImageFormat(int i)
{
	if(ImageFormat::IsValid(i))
	{
		mImageFormat = i;
		if(mImageWriter != 0) mImageWriter->Delete();
		switch(mImageFormat) 
		{
		case ImageFormat::JPG:
			{
				mImageWriter = vtkJPEGWriter::New(); IERROR_CHECK_MEMORY(mImageWriter); 
				break;
			}
		case ImageFormat::PNM:
			{
				mImageWriter = vtkPNMWriter::New(); IERROR_CHECK_MEMORY(mImageWriter); 
				break;
			}
		case ImageFormat::BMP:
			{
				mImageWriter = vtkBMPWriter::New(); IERROR_CHECK_MEMORY(mImageWriter); 
				break;
			}
		case ImageFormat::TIF:
			{
				mImageWriter = vtkTIFFWriter::New(); IERROR_CHECK_MEMORY(mImageWriter); 
				break;
			}
		case ImageFormat::EPS:
			{
				iPostScriptWriter *p = iPostScriptWriter::New(); IERROR_CHECK_MEMORY(p);
				p->SetPaperFormat(mPostScriptPaperFormat);
				p->SetOrientation(mPostScriptOrientation);
				mImageWriter = p;
				break;
			}
		default:
			{
				mImageWriter = vtkPNGWriter::New(); IERROR_CHECK_MEMORY(mImageWriter); 
				break;
			}
		}

		mImageWriter->AddObserver(vtkCommand::StartEvent,this->GetShell()->GetExecutionEventObserver());
		mImageWriter->AddObserver(vtkCommand::ProgressEvent,this->GetShell()->GetExecutionEventObserver());
		mImageWriter->AddObserver(vtkCommand::EndEvent,this->GetShell()->GetExecutionEventObserver());

		//
		//  Reset the animation writer if needed
		//
		if(mMovieOutput == MovieOutput::Image)
		{
			mMovieOutput = -1;
			this->SetMovieOutput(MovieOutput::Image);
		}

		return true;
	}
	else return false;
}


bool iWriter::SetMovieOutput(int i)
{
	if(MovieOutput::IsValid(i))
	{
		mMovieOutput = i;
		if(mMovieWriter != 0)
		{
			mMovieWriter->Delete();
			mMovieWriter = 0;
		}
		switch(mMovieOutput)
		{
#ifdef IVTK_SUPPORTS_MPEG
		case MovieOutput::MPEG2:
			{
				mMovieWriter = MultiImageMovieWriter<vtkMPEG2Writer>::New(this->GetViewModule(),"mpg"); IERROR_CHECK_MEMORY(mMovieWriter);
				break;
			}
#endif
#ifdef IVTK_SUPPORTS_AVI
		case MovieOutput::AVI:
			{
				mMovieWriter = MultiImageMovieWriter<vtkAVIWriter>::New(this->GetViewModule(),"avi"); IERROR_CHECK_MEMORY(mMovieWriter);
				break;
			}
#endif
		default:
			{
				//
				//  File-set movie writer
				//
				mMovieWriter = MultiImageMovieWriter<FileSetMovieWriter>::New(this->GetViewModule(),"",mImageWriter); IERROR_CHECK_MEMORY(mMovieWriter);
			}
		}

		return true;
	}
	else return false;
}


bool iWriter::SetPostScriptPaperFormat(int i)
{
	if(i >= 0)
	{
		mPostScriptPaperFormat = i;
		if(mImageFormat == ImageFormat::EPS) iRequiredCast<iPostScriptWriter>(INFO,mImageWriter)->SetPaperFormat(i);
		return true;
	}
	else return false;
}


bool iWriter::SetPostScriptOrientation(int i)
{
	if(i >= 0)
	{
		mPostScriptOrientation = i;	
		if(mImageFormat == ImageFormat::EPS) iRequiredCast<iPostScriptWriter>(INFO,mImageWriter)->SetOrientation(i);
		return true;
	}
	else return false;
}


void iWriter::StartMovie()
{
	IASSERT(mMovieWriter);
	mMovieWriter->Start();
}


void iWriter::FinishMovie()
{
	IASSERT(mMovieWriter);
	mMovieWriter->Finish();
	
	mMovieIndex++;
}


bool iWriter::CallWrite()
{
	return this->GetViewModule()->CallWriteImage();
}


void iWriter::WriteImageFrame(const iStereoImageArray& images)
{
	int i;
	iString suf, name;

	IASSERT(mImageWriter);

	switch(mImageFormat) 
	{
	case ImageFormat::JPG: { suf = ".jpg"; break; }
	case ImageFormat::PNM: { suf = ".ppm"; break; }
	case ImageFormat::BMP: { suf = ".bmp"; break; }
	case ImageFormat::TIF: { suf = ".tif"; break; }
	case ImageFormat::EPS: { suf = ".eps"; break; }
	default:			   { suf = ".png"; }
	}
	
	//
	// Create and write files
	//
	for(i=0; i<images.Size(); i++)
	{
		name = FrameFileName(i,images[i].GetTag(),this->GetImageFileName());

		mImageWriter->SetInputData(images[i].LeftEye().DataObject());
		if(images[i].IsStereo())
		{
			mImageWriter->SetFileName((name+"-left"+suf).ToCharPointer());
		}
		else
		{
			mImageWriter->SetFileName((name+suf).ToCharPointer());
		}
		mImageWriter->Write();
		//
		//  Right eye
		//
		if(images[i].IsStereo())
		{
			mImageWriter->SetInputData(images[i].RightEye().DataObject());
			mImageWriter->SetFileName((name+"-right"+suf).ToCharPointer());
			mImageWriter->Write();
		}
	}

	mImageIndex++;
}


void iWriter::WriteMovieFrame(const iStereoImageArray& images)
{
	IASSERT(mMovieWriter);
	mMovieWriter->WriteFrame(images,this->GetMovieFileName());
}


bool iWriter::SetImageRootName(const iString& name)
{
	if(!name.IsEmpty())
	{
		mImageIndex = 1;
		mImageRootName = name;

		if(mImageRootName[0] == '+')
		{
			mImageRootName = this->GetShell()->GetEnvironment(Environment::Image) + mImageRootName.Part(1);
		}

#ifdef _WIN32
		mImageRootName.Replace("\\","/");
#endif		
		return true;
	}
	else return false;
}


const iString& iWriter::GetImageRootName() const
{
	return mImageRootName;
}


iString iWriter::GetImageFileName()
{
	return mImageRootName + "-" + iString::FromNumber(mImageIndex,"%05d");
}


bool iWriter::SetMovieRootName(const iString& name)
{
	if(!name.IsEmpty())
	{
		mMovieIndex = 1; 
		mMovieRootName = name;

		if(mMovieRootName[0] == '+')
		{
			mMovieRootName = this->GetShell()->GetEnvironment(Environment::Image) + mMovieRootName.Part(1);
		}

#ifdef _WIN32
		mMovieRootName.Replace("\\","/");
#endif
		return true;
	}
	else return false;
}


const iString& iWriter::GetMovieRootName() const
{
	return mMovieRootName;
}


iString iWriter::GetMovieFileName()
{
	iString root = mMovieRootName;
	
	if(mMovieIndex > 1)
	{
		iString tail;
		if(root[root.Length()-1] == '-')
		{
			tail = "-";
			root = root.Part(0,root.Length()-1);
		}
		root += iString::FromNumber(mMovieIndex) + tail;
	}

	return root;
}

