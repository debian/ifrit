/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


//
//  vtkRenderLargeImage with a bug fix
//
#ifndef IMAGNIFIER_H
#define IMAGNIFIER_H


#include <vtkRenderLargeImage.h>


class iMagnifier : public vtkRenderLargeImage
{

public:

	static iMagnifier* New();

	inline void GetTile(int &x, int &y){ x = mTileX; y = mTileY; }

	virtual int ProcessRequest(vtkInformation *request, vtkInformationVector **inInfo, vtkInformationVector *outInfo);

protected:

	void RequestData(vtkInformation *request, vtkInformationVector **inInfo, vtkInformationVector *outInfo);

	virtual ~iMagnifier();

private:

	iMagnifier();

	void Render(vtkImageData *data);

	int mTileX, mTileY;
};

#endif // IMAGNIFIER_H
 

