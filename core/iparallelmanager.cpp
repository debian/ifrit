/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iparallelmanager.h"


#include "iarray.h"
#include "ierror.h"
#include "ieventobserver.h"
#include "iparallel.h"
#include "iparallelworker.h"
#include "iviewmodule.h"

#include <vtkMultiThreader.h>
#include <vtkTimerLog.h>


#if defined(I_DEBUG)
#include "ishell.h"
#endif

//
//  Templates
//
#include "iarray.tlh"


using namespace iParallel;
using namespace iParameter;


#ifdef I_DEBUG
int iParallelManager::DebugSwitch = 0;
#endif


//
//  Helper classes
//
class iParallelTimer
{

public:

	iParallelTimer()
	{
		mClock = vtkTimerLog::New(); IERROR_CHECK_MEMORY(mClock);
		mValue = 0.0;
	}

	~iParallelTimer()
	{
		mClock->Delete();
	}

	void Reset()
	{
		mValue = 0.0;
	}

	void Start()
	{
		mClock->StartTimer();
	}

	void Stop()
	{
		mClock->StopTimer();
		mValue += mClock->GetElapsedTime();
	}

	double GetValue() const { return mValue; }

private:

	vtkTimerLog *mClock;
	double mValue;
};


class iParallelHelper
{

public:

	iParallelHelper()
	{
		mWallTimer = 0;
	}

	virtual ~iParallelHelper()
	{
		int i;

		if(mWallTimer != 0) delete mWallTimer;
	
		for(i=0; i<mProcTimers.Size(); i++) delete mProcTimers[i];
	}

	void Initialize(int &minnp, int &maxnp)
	{
		this->GetMinMax(minnp,maxnp);

		mWallTimer = new iParallelTimer; IERROR_CHECK_MEMORY(mWallTimer);

		iParallelTimer *tmp;
		while(mProcTimers.Size() < maxnp)
		{
			tmp = new iParallelTimer; IERROR_CHECK_MEMORY(tmp);
			mProcTimers.Add(tmp);
		}
	}

	void ResetMaxNumProcs(int &n)
	{
		this->SetNumProcs(n);

		iParallelTimer *tmp;
		while(mProcTimers.Size() < n)
		{
			tmp = new iParallelTimer; IERROR_CHECK_MEMORY(tmp);
			mProcTimers.Add(tmp);
		}
		while(mProcTimers.Size() > n) delete mProcTimers.RemoveLast();
	}

	void StartTimers()
	{
		int i;

		for(i=0; i<mProcTimers.Size(); i++) mProcTimers[i]->Reset();
		if(mWallTimer != 0)
		{
			mWallTimer->Reset();
			mWallTimer->Start();
		}
	}

	bool StopTimers()
	{
		if(mWallTimer != 0) mWallTimer->Stop();
		if(mProcTimers[0] != 0) return (mProcTimers[0]->GetValue() > 0.01); else return false;
	}

	double GetProcessorExecutionTime(int n) const
	{
		if(n>=0 && n<mProcTimers.Size()) return mProcTimers[n]->GetValue(); else return 0.0;
	}

	double GetWallClockExecutionTime() const
	{
		if(mWallTimer != 0) return mWallTimer->GetValue(); else return 0.0;
	}

	int ExecuteWorkerInParallel(iParallelWorker *worker, ProcessorInfo &p)
	{
		if(worker!=0 && p.NumProcs<=mProcTimers.Size() && p.ThisProc>=0 && p.ThisProc<p.NumProcs)
		{
			mProcTimers[p.ThisProc]->Start();
			int ret = worker->ExecuteInternal(p);
			mProcTimers[p.ThisProc]->Stop();
			return ret;
		}
		else return 1;
	}

	virtual void GetMinMax(int &minnp, int &maxnp) = 0;
	virtual void SetNumProcs(int &np) = 0;
	virtual int ExecuteWorker(iParallelWorker *worker) = 0;

protected:

	iArray<iParallelTimer*> mProcTimers;
	iParallelTimer *mWallTimer;
};


//
//  Private namespace with actual helpers
//
namespace iParallelManager_Private
{
	class SharedMemoryHelper : public iParallelHelper
	{

	public:

		SharedMemoryHelper()
		{
			mExec = vtkMultiThreader::New(); IERROR_CHECK_MEMORY(mExec);
		}

		virtual ~SharedMemoryHelper()
		{
			mExec->Delete();
		}

		virtual void GetMinMax(int &minnp, int &maxnp)
		{
			minnp = 1;
			maxnp = mExec->GetGlobalDefaultNumberOfThreads();
		}

		virtual void SetNumProcs(int &np)
		{
			mExec->SetNumberOfThreads(np);
			np = mExec->GetNumberOfThreads();
		}

		virtual int ExecuteWorker(iParallelWorker *worker)
		{
			Data d;

			d.Self = this;
			d.Worker = worker;
			d.Value = 0;

			if(worker != 0)
			{
				mExec->SetSingleMethod(MySingleMethod,&d);
				mExec->SingleMethodExecute();
			}

			return d.Value;
		}

		static VTK_THREAD_RETURN_TYPE MySingleMethod(void *input)
		{
			if(input != 0)
			{
				vtkMultiThreader::ThreadInfo *ti = (vtkMultiThreader::ThreadInfo * )input;
				Data *d = (Data * )ti->UserData;
				if(d != 0)
				{
					ProcessorInfo p;
					p.NumProcs = ti->NumberOfThreads;
					p.ThisProc = ti->ThreadID;
					d->Value = d->Self->ExecuteWorkerInParallel(d->Worker,p);
				}
			}
			return VTK_THREAD_RETURN_VALUE;
		}

	protected:

		struct Data
		{
			SharedMemoryHelper *Self;
			iParallelWorker *Worker;
			int Value;
		};

		vtkMultiThreader *mExec;
	};


	class SharedMemoryManager : public iParallelManager
	{

	public:

		SharedMemoryManager(iShell *s) : iParallelManager(s)
		{
		}

	protected:

		virtual iParallelHelper* CreateHelper() const
		{
			return new SharedMemoryHelper;
		}
	};
};


using namespace iParallelManager_Private;


iParallelManager* iParallelManager::New(iShell *s)
{
	iParallelManager *ret =  new SharedMemoryManager(s);
	ret->Define();
	return ret;
}


iParallelManager::iParallelManager(iShell *s) : iShellComponent(s)
{
}


void iParallelManager::Define()
{
	mHelper = this->CreateHelper();
	if(mHelper != 0)
	{
		mHelper->Initialize(mMinNumProcs,mMaxNumProcs);
		this->SetNumberOfProcessors(1);

#ifdef I_DEBUG
		iOutput::DisplayDebugMessage("Max # of procs: "+iString::FromNumber(mMaxNumProcs)+", using "+iString::FromNumber(this->GetNumberOfProcessors()));
#endif

		mCount = 0;
	}
	else
	{
		mCount = -1;
		mNumProcs = mMinNumProcs = mMaxNumProcs = 1;
	}
}


iParallelManager::~iParallelManager()
{
	if(mHelper != 0) delete mHelper;
}


int iParallelManager::ExecuteWorker(iParallelWorker *worker)
{
	int ret;

	bool single = (mCount == 0);

	if(single) this->StartCounters();

	if(mHelper!=0 && mNumProcs>1)
	{
		ret = mHelper->ExecuteWorker(worker);
	}
	else
	{
		ProcessorInfo p;
		if(worker != 0) ret = worker->ExecuteInternal(p); else ret = 1;
	}

	if(single) this->StopCounters();
	return ret;
}


void iParallelManager::SetNumberOfProcessors(int n)
{ 
	if(mHelper != 0)
	{
		if(n == 0) n = mMaxNumProcs; // auto-set to max available number
		if(n>0 && n<=mMaxNumProcs)
		{
			mNumProcs = n;
			mHelper->SetNumProcs(mNumProcs); 
		}
	}
}


double iParallelManager::GetProcessorExecutionTime(int n) const
{
	if(mHelper != 0)
	{
		return mHelper->GetProcessorExecutionTime(n);
	}
	else return 0.0;
}


double iParallelManager::GetWallClockExecutionTime() const
{
	if(mHelper != 0)
	{
		return mHelper->GetWallClockExecutionTime();
	}
	else return 0.0;
}


void iParallelManager::StartCounters()
{
	if(mHelper != 0)
	{
		if(mCount == 0)
		{
			mHelper->StartTimers();
		}
		mCount++;
	}
}


void iParallelManager::StopCounters()
{
	if(mHelper != 0)
	{
		mCount--;
		if(mCount == 0)
		{
			if(mHelper->StopTimers()) this->InvokeEvent(InformationEvent,0);
		}
#ifndef I_NO_CHECK
		if(mCount < 0) IBUG_WARN("Imbalanced start/stop counters.");
#endif
	}
}

