/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ispheresource.h"


#include "ierror.h"
#include "ivtk.h"

#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkFloatArray.h>
#include <vtkPointData.h>
#include <vtkPointLocator.h>
#include <vtkPolyData.h>


namespace iSphereSource_private
{
	void pix2vec(int order, int ipix, double *vec);
	void neighbors(int order, int ipix, int *result);
	void boundaries(int order, int pix, double out[4][3]);
};


using namespace iSphereSource_private;


iSphereSource* iSphereSource::New()
{
	return new iSphereSource();
}


iSphereSource::iSphereSource()
{
	mOrder = 1;
	mRadius = 1.0;
	mCenter[0] = mCenter[1] = mCenter[2] = 0;

	this->SetNumberOfInputPorts(0);
}


void iSphereSource::SetOrder(int order)
{
	if(order>=0 && order<=13)  //  int-valued npix
	{
		mOrder = order;
		this->Modified();
	}
}


void iSphereSource::SetResolution(int res)
{
	if(res > 0)  //  int-valued npix
	{
		int order = 0;
		while(order<13 && (res>>order)>2) order++;
		this->SetOrder(order);
	}
}


void iSphereSource::SetRadius(double v)
{
	if(v > 0) 
	{
		mRadius = v;
		this->Modified();
	}
}


void iSphereSource::SetCenter(double pos[3])
{
	int j;
	for(j=0; j<3; j++) 
	{
		mCenter[j] = pos[j];
	}
	this->Modified();
}


void iSphereSource::GetCenter(double pos[3]) const
{
	int j;
	for(j=0; j<3; j++) 
	{
		pos[j] = mCenter[j];
	}
}


int iSphereSource::RequestData(vtkInformation *request, vtkInformationVector **inputVector, vtkInformationVector *outputVector)
{
	vtkPolyData *output = this->GetOutput();
	output->Initialize();

	int nside = (1 << mOrder);

	int numPixs = 12*nside*nside;
 	vtkCellArray *cells = vtkCellArray::New(); IERROR_CHECK_MEMORY(cells);
	cells->Allocate(cells->EstimateSize(numPixs,4));
    
	vtkPoints *points = vtkPoints::New(VTK_DOUBLE); IERROR_CHECK_MEMORY(points);
	vtkPointLocator *loc = vtkPointLocator::New(); IERROR_CHECK_MEMORY(loc);
	loc->SetTolerance(1.0e-10);
	loc->Initialize();

	double bounds[6];
	bounds[0] = -1.01;
	bounds[1] =  1.01;
	bounds[2] = -1.01;
	bounds[3] =  1.01;
	bounds[4] = -1.01;
	bounds[5] =  1.01;
	loc->InitPointInsertion(points,bounds);

	int j, pix;
	vtkIdType cell[4];
	double vec[4][3];
	for(pix=0; pix<numPixs; pix++)
	{
		if(pix%1000 == 0)
		{
			this->UpdateProgress(double(pix)/numPixs);
			if(this->GetAbortExecute()) return 0;
		}

		boundaries(mOrder,pix,vec);
	
		for(j=0; j<4; j++)
		{
			cell[j] = loc->InsertNextPoint(vec[j]);
		}

		cells->InsertNextCell(4,cell);
	}

	output->SetPolys(cells);
	cells->Delete();

	//
	//  Scale/shoft points, add normals
	//
	vtkIdType l, n = points->GetNumberOfPoints();

	vtkFloatArray *norms = vtkFloatArray::New(); IERROR_CHECK_MEMORY(norms);
	norms->SetNumberOfComponents(3);
	norms->SetNumberOfTuples(n);
	float *ptrNorms = (float*)norms->GetVoidPointer(0);
	double *ptrPoints = (double*)points->GetVoidPointer(0);

	for(l=0; l<n; l++)
	{
		for(j=0; j<3; j++)
		{
			ptrNorms[3*l+j] = ptrPoints[3*l+j];
			ptrPoints[3*l+j] = mCenter[j] + mRadius*ptrPoints[3*l+j];
		}
	}

	output->GetPointData()->SetNormals(norms);
	norms->Delete();

	output->SetPoints(points);
	points->Delete();

	return 1;
}


namespace iSphereSource_private
{
	const double twothird=2.0/3.0;
	const double pi=3.141592653589793238462643383279502884197;
	const double twopi=6.283185307179586476925286766559005768394;
	const double halfpi=1.570796326794896619231321691639751442099;
	const double inv_halfpi=0.6366197723675813430755350534900574;

	const unsigned short utab[] =
	{
#define Z(a) 0x##a##0, 0x##a##1, 0x##a##4, 0x##a##5
#define Y(a) Z(a##0), Z(a##1), Z(a##4), Z(a##5)
#define X(a) Y(a##0), Y(a##1), Y(a##4), Y(a##5)
		X(0),X(1),X(4),X(5)
#undef X
#undef Y
#undef Z
	};

	const unsigned short ctab[] =
	{
#define Z(a) a,a+1,a+256,a+257
#define Y(a) Z(a),Z(a+2),Z(a+512),Z(a+514)
#define X(a) Y(a),Y(a+4),Y(a+1024),Y(a+1028)
		X(0),X(8),X(2048),X(2056)
#undef X
#undef Y
#undef Z
	};

	const int jrll[] = { 2,2,2,2,3,3,3,3,4,4,4,4 };
	const int jpll[] = { 1,3,5,7,0,2,4,6,1,3,5,7 };

	const int nb_xoffset[] = { -1,-1, 0, 1, 1, 1, 0,-1 };
    const int nb_yoffset[] = {  0, 1, 1, 1, 0,-1,-1,-1 };
	const int nb_facearray[][12] =
	{
		{8,9,10,11,-1,-1,-1,-1,10,11,8,9},   // S
		{5,6,7,4,8,9,10,11,9,10,11,8},   // SE
		{-1,-1,-1,-1,5,6,7,4,-1,-1,-1,-1},   // E
		{4,5,6,7,11,8,9,10,11,8,9,10},   // SW
		{0,1,2,3,4,5,6,7,8,9,10,11},   // center
		{1,2,3,0,0,1,2,3,5,6,7,4},   // NE
		{-1,-1,-1,-1,7,4,5,6,-1,-1,-1,-1},   // W
		{3,0,1,2,3,0,1,2,4,5,6,7},   // NW
		{2,3,0,1,-1,-1,-1,-1,0,1,2,3} // N
	};
	const int nb_swaparray[][3] =
	{
		{0,0,3},   // S
		{0,0,6},   // SE
		{0,0,0},   // E
		{0,0,5},   // SW
		{0,0,0},   // center
		{5,0,0},   // NE
		{0,0,0},   // W
		{6,0,0},   // NW
		{3,0,0}    // N
	};

	inline int spread_bits (int v)
	{ 
		return utab[v&0xff] | (utab[(v>>8)&0xff]<<16);
	}

	inline int compress_bits(int v)
	{
		int raw = (v&0x5555) | ((v&0x55550000)>>15);
		return ctab[raw&0xff] | (ctab[raw>>8]<<4);
	}

	inline void pix2xyf(int order, int pix, int &ix, int &iy, int &face_num)
	{
		face_num = pix>>(2*order);
		int nside = 1<<order;
		int npface = nside<<order;
		pix &= (npface-1);
		ix = compress_bits(pix);
		iy = compress_bits(pix>>1);
	}

	inline int xyf2pix (int order, int ix, int iy, int face_num)
	{
		return (face_num<<(2*order)) + spread_bits(ix) + (spread_bits(iy)<<1);
	}

	void neighbors(int order, int ipix, int *result)
	{
		int nside = 1<<order;
		int ix, iy, face_num;

		pix2xyf(order,ipix,ix,iy,face_num);

		int nsm1 = nside - 1;
		if((ix>0)&&(ix<nsm1)&&(iy>0)&&(iy<nsm1))
		{
			int fpix = face_num<<(2*order),
				px0 = spread_bits(ix), py0 = spread_bits(iy)<<1,
				pxp = spread_bits(ix+1), pyp = spread_bits(iy+1)<<1,
				pxm = spread_bits(ix-1), pym = spread_bits(iy-1)<<1;

			result[0] = fpix + pxm + py0; result[1] = fpix + pxm + pyp;
			result[2] = fpix + px0 + pyp; result[3] = fpix + pxp + pyp;
			result[4] = fpix + pxp + py0; result[5] = fpix + pxp + pym;
			result[6] = fpix + px0 + pym; result[7] = fpix + pxm + pym;
		}
		else
		{
			int i;
			for(i=0; i<8; i++)
			{
				int x = ix + nb_xoffset[i], y = iy + nb_yoffset[i];
				int nbnum = 4;
				if(x<0)
				{
					x += nside; nbnum -= 1;
				}
				else if(x>=nside)
				{
					x -= nside; nbnum += 1;
				}
				if(y<0)
				{
					y += nside; nbnum -= 3;
				}
				else if(y>=nside)
				{
					y -= nside; nbnum += 3;
				}

				int f = nb_facearray[nbnum][face_num];
				if(f>=0)
				{
					int bits = nb_swaparray[nbnum][face_num>>2];
					if(bits&1) x = nside-x-1;
					if(bits&2) y = nside-y-1;
					if(bits&4) std::swap(x,y);
					result[i] = xyf2pix(order,x,y,f);
				}
				else
				{
					result[i] = -1;
				}
			}
		}
	}

	void xyf2loc(double x, double y, int face, double &phi, double &z)
	{
		double jr = jrll[face] - x - y;
		double nr;
		if(jr < 1)
		{
			nr = jr;
			double tmp = nr*nr/3.;
			z = 1 - tmp;
		}
		else if(jr > 3)
		{
			nr = 4 - jr;
			double tmp = nr*nr/3.;
			z = tmp - 1;
		}
		else
		{
			nr = 1;
			z = (2-jr)*2./3.;
		}

		double tmp = jpll[face]*nr + x - y;
		if(tmp < 0) tmp += 8;
		if(tmp >= 8) tmp -= 8;

		phi = (nr<1e-15) ? 0 : (0.5*halfpi*tmp)/nr;
	}

	void pix2vec(int order, int ipix, double *vec)
	{
		int nside = 1<<order;
		int face, ix, iy;
		pix2xyf(order,ipix,ix,iy,face);

		double x = (ix+0.5)/nside, y = (iy+0.5)/nside;

		double phi, sth, z;
		xyf2loc(x,y,face,phi,z);

		sth = sqrt((1-z)*(1+z));
		vec[0] = sth*cos(phi);
		vec[1] = sth*sin(phi);
		vec[2] = z;
	}

	void boundaries(int order, int pix, double out[4][3])
	{
		int nside = 1<<order;
		int ix,iy,face;

		pix2xyf(order,pix,ix,iy,face);

		double dc = 0.5 / nside;
		double xc = (ix+0.5)/nside, yc = (iy+0.5)/nside;
		double z, phi, sth;

		xyf2loc(xc-dc,yc-dc,face,phi,z);
		sth = sqrt((1-z)*(1+z));
		out[0][0] = sth*cos(phi);
		out[0][1] = sth*sin(phi);
		out[0][2] = z;

		xyf2loc(xc-dc,yc+dc,face,phi,z);
		sth = sqrt((1-z)*(1+z));
		out[1][0] = sth*cos(phi);
		out[1][1] = sth*sin(phi);
		out[1][2] = z;

		xyf2loc(xc+dc,yc+dc,face,phi,z);
		sth = sqrt((1-z)*(1+z));
		out[2][0] = sth*cos(phi);
		out[2][1] = sth*sin(phi);
		out[2][2] = z;

		xyf2loc(xc+dc,yc-dc,face,phi,z);
		sth = sqrt((1-z)*(1+z));
		out[3][0] = sth*cos(phi);
		out[3][1] = sth*sin(phi);
		out[3][2] = z;
	}
};

