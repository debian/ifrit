/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "icoredatasubjects.h"


#include "icorefileloaders.h"
#include "idata.h"
#include "idatalimits.h"
#include "idatareader.h"

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"
#include "iproperty.tlh"


namespace iCoreData
{
	class FieldLimits : public iDataLimits
	{

	public:

		vtkTypeMacro(FieldLimits,iDataLimits);

		static FieldLimits* New(const iDataSubject *subject = 0, unsigned rank = 0U)
		{
			IASSERT(subject);
			return new FieldLimits(subject,rank);
		}

protected:
	
		FieldLimits(const iDataSubject *subject, unsigned rank) : iDataLimits(subject,true)
		{
			switch(rank)
			{
			case 1U:
				{
					this->DeclareVars(1);
					this->SetName(0,"Vector amplitude");
					break;
				}
			case 2U:
				{
					this->DeclareVars(3);
					this->SetName(0,"Min eigenvalue");
					this->SetName(1,"Med eigenvalue");
					this->SetName(2,"Max eigenvalue");
					break;
				}
			}
		}
	};


	//
	//  ********************************************************************
	//
	//  ScalarDataSubject class
	//
	//  ********************************************************************
	//
	iDataSubjectDefineTypeMacro(ScalarDataSubject,1,"Scalars","s",0,"core,field,scalars","IFRIT_SCALAR_FIELD_DATA_DIR");


	ScalarDataSubject::ScalarDataSubject(ScalarFileLoader *fl) : iDataSubject(fl,ScalarDataSubject::DataType()),
		iObjectConstructPropertyMacroA2(Int,Float,ScalarDataSubject,Smooth,sm)
	{
		Smooth.AddFlag(iProperty::_FunctionsAsIndex);

		mConcreteLoader = fl;
	}


	bool ScalarDataSubject::CallSmooth(int v, float r)
	{
		return mConcreteLoader->Smooth(v,r);
	}


	const iString ScalarDataSubject::HelpTag() const
	{
		return "~_" + this->ShortName();
	}


	//
	//  ********************************************************************
	//
	//  VectorDataSubject class
	//
	//  ********************************************************************
	//
	iDataSubjectDefineTypeMacro(VectorDataSubject,2,"Vectors","v",1,"basic,field,vectors","IFRIT_VECTOR_FIELD_DATA_DIR");


	VectorDataSubject::VectorDataSubject(iFileLoader *fl) : iDataSubject(fl,VectorDataSubject::DataType())
	{
	}


	const iString VectorDataSubject::HelpTag() const
	{
		return "~_" + this->ShortName();
	}


	iDataLimits* VectorDataSubject::CreateLimits() const
	{
		return FieldLimits::New(this,1U);
	}


	//
	//  ********************************************************************
	//
	//  TensorDataSubject class
	//
	//  ********************************************************************
	//
	iDataSubjectDefineTypeMacro(TensorDataSubject,3,"Tensors","t",2,"basic,field,tensors","IFRIT_TENSOR_FIELD_DATA_DIR");


	TensorDataSubject::TensorDataSubject(iFileLoader *fl) : iDataSubject(fl,TensorDataSubject::DataType())
	{
	}


	const iString TensorDataSubject::HelpTag() const
	{
		return "~_" + this->ShortName();
	}


	iDataLimits* TensorDataSubject::CreateLimits() const
	{
		return FieldLimits::New(this,2U);
	}


	//
	//  ********************************************************************
	//
	//  ParticleDataSubject class
	//
	//  ********************************************************************
	//
	iDataSubjectDefineTypeMacro(ParticleDataSubject,4,"Particles","p",0,"core,particles","IFRIT_PARTICLE_SET_DATA_DIR");


	ParticleDataSubject::ParticleDataSubject(iParticleFileLoader *fl) : iParticleDataSubject(fl,ParticleDataSubject::DataType())
	{
	}

	const iString ParticleDataSubject::HelpTag() const
	{
		return "~_" + this->ShortName();
	}
};

