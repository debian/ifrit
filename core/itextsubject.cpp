/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "itextsubject.h"


#include "ioverlayhelper.h"
#include "irendertool.h"
#include "itextactor.h"

#include <vtkTextProperty.h>
#include <vtkViewport.h>

//
//  Templates
//
#include "iarray.tlh"
#include "igenericprop.tlh"


iTextSubject::iTextSubject(iTextActor *parent, iRenderTool *rv) : iGenericProp<vtkProp>(false), mOverlayHelper(rv), mParent(parent)
{
	IASSERT(parent);

	mPos[0] = mPos[1] = 0.0;

	this->PickableOff();
}


iTextSubject::~iTextSubject()
{
}


void iTextSubject::GetSize(vtkViewport *vp, float s[2])
{
#ifndef I_NO_CHECK
	int mag = mOverlayHelper->GetRenderTool()->GetRenderingMagnification();
	if(mag > 1)
	{
		IBUG_WARN("Incorrect call to iTextSubject::GetSize");
	}
#endif

	mOverlayHelper->UpdateTextProperty(vp,mParent->GetTextProperty());
	mParent->GetTextProperty()->SetFontSize(mOverlayHelper->GetFontSize(vp,mag));
	this->ComputeSize(vp,s);
}


void iTextSubject::UpdateGeometry(vtkViewport *vp)
{
	int mag = this->GetOverlayHelper()->GetRenderingMagnification();

	if(mag == 1)
	{
		this->GetSize(vp,mSize);
		mParent->GetTextAnchor(vp,mPos);
	}

	mParent->GetTextProperty()->SetFontSize(mOverlayHelper->GetFontSize(vp,mag));

	this->UpdateGeometryBody(vp,mag);
}

