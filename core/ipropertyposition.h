/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Position-values property
//

#ifndef IPROPERTYPOSITION_H
#define IPROPERTYPOSITION_H


#include "iproperty.h"
#include "iviewmodulecomponent.h"


class iDistance;
class iPosition;

//
//  Index that automatically shifts by 1 for script indexing
//
class iPropertyScalarPosition : public iPropertyScalar<iType::Vector>, public iViewModuleComponent
{

public:

	typedef bool (iObject::*SetterType)(const iPosition& val);
	typedef const iPosition& (iObject::*GetterType)() const;

	iPropertyScalarPosition(iObject *owner, iViewModule *vm, SetterType setter, GetterType getter, const iString &name, const iString &sname);

protected:

	virtual const iVector3D& GetValue(int i) const;
	virtual bool SetValue(int i, const iVector3D& v) const;

private:

	SetterType mSetter;
	GetterType mGetter;
};


class iPropertyScalarDistance : public iPropertyScalar<iType::Double>, public iViewModuleComponent
{

public:

	typedef bool (iObject::*SetterType)(const iDistance& val);
	typedef const iDistance& (iObject::*GetterType)() const;

	iPropertyScalarDistance(iObject *owner, iViewModule *vm, SetterType setter, GetterType getter, const iString &name, const iString &sname);

protected:

	virtual double GetValue(int i) const;
	virtual bool SetValue(int i, double v) const;

private:

	SetterType mSetter;
	GetterType mGetter;
};



#endif // IPROPERTYPOSITION_H

