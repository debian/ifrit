/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "itensorfieldviewsubject.h"


#include "iactor.h"
#include "icommondatadistributors.h"
#include "icoredatasubjects.h"
#include "idatalimits.h"
#include "idatasubject.h"
#include "ierror.h"
#include "itensorfieldglyphpipeline.h"
#include "iviewmodule.h"
#include "iviewobject.h"
#include "iviewsubjectparallelpipeline.h"

#include <vtkPolyData.h>

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"
#include "iproperty.tlh"


using namespace iParameter;


//
//  single prop class
//
iTensorFieldViewInstance::iTensorFieldViewInstance(iTensorFieldViewSubject *owner) : iFieldViewInstance(owner,1)
{
	mMethod = 0;
	mScaling = true;
	mGlyphType = 0;
}


iTensorFieldViewInstance::~iTensorFieldViewInstance()
{
}


void iTensorFieldViewInstance::ConfigureBody()
{
	//
	//  Create pipeline (must be created after the object is fully created)
	//	
	this->AddMainPipeline(1);
	mActors[0]->SetInputConnection(this->Pipeline()->GetOutputPort(0));
}


void iTensorFieldViewInstance::ResetPipelineInput(vtkDataSet *input)
{
	this->Pipeline()->SetInputData(0,input);
}


bool iTensorFieldViewInstance::SetMethod(int v)
{ 
	if(v == 0) 
	{
		mMethod = v;
		return true;
	}
	else return false;
}


bool iTensorFieldViewInstance::SetGlyphType(int v)
{ 
	if(v>=0 && v<=1) 
	{
		mGlyphType = v;
		this->Pipeline()->UpdateContents(iTensorFieldGlyphPipeline::KeyGlyphType);
		return true;
	}
	else return false;
}


bool iTensorFieldViewInstance::SetScaling(bool s)
{
	mScaling = s;
	this->Pipeline()->UpdateContents(iTensorFieldGlyphPipeline::KeyScaling);
	return true;
}


void iTensorFieldViewInstance::UpdatePainting()
{
	this->iFieldViewInstance::UpdatePainting();

	this->Pipeline()->UpdateContents(iTensorFieldGlyphPipeline::KeyPainting);
}


void iTensorFieldViewInstance::UpdateGlyphSize()
{ 
	this->Pipeline()->UpdateContents(iTensorFieldGlyphPipeline::KeyGlyphSize);
}


void iTensorFieldViewInstance::UpdateGlyphSampleRate()
{ 
	this->Pipeline()->UpdateContents(iTensorFieldGlyphPipeline::KeyGlyphSampleRate);
}


void iTensorFieldViewInstance::ShowBody(bool show)
{
	if(show)
	{
		mActors[0]->VisibilityOn();
	} 
	else 
	{
		mActors[0]->VisibilityOff();
	}
}


bool iTensorFieldViewInstance::CanBeShown() const
{
	return this->Owner()->GetSubject()->IsThereTensorData();
}


iViewSubjectPipeline* iTensorFieldViewInstance::CreatePipeline(int)
{
	return new iTensorFieldGlyphPipeline(this);
}


void iTensorFieldViewInstance::ConfigureMainPipeline(iViewSubjectPipeline *p, int)
{
	iViewSubjectParallelPipeline *pp = iRequiredCast<iViewSubjectParallelPipeline>(INFO,p);

	iImageDataDistributor *idd = new iImageDataDistributor(pp->GetDataManager());
	pp->GetDataManager()->AddDistributor(idd);
	pp->GetDataManager()->AddCollector(new iPolyDataCollector(pp->GetDataManager(),idd));
}


//
//  Main class
//
iTensorFieldViewSubject::iTensorFieldViewSubject(iViewObject *parent, const iDataType &type, const iDataType &stype) : iFieldViewSubject(parent,parent->LongName(),parent->ShortName(),parent->GetViewModule(),type,stype,0U),
	iViewSubjectPropertyConstructMacro(Int,iTensorFieldViewInstance,Method,m),
	iViewSubjectPropertyConstructMacro(Bool,iTensorFieldViewInstance,Scaling,s),
	iViewSubjectPropertyConstructMacro(Int,iTensorFieldViewInstance,GlyphType,gt)
{
}


iTensorFieldViewSubject::~iTensorFieldViewSubject()
{
}

