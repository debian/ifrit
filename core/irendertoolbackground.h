/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


//
//  A background renderer for iRenderTool
//
#ifndef IRENDERTOOLBACKGROUND_H
#define IRENDERTOOLBACKGROUND_H


#include <vtkObjectBase.h>


#include "icolor.h"
#include "iimage.h"

#include <vtkSetGet.h>


class iRenderToolBackgroundImageMapper;

class vtkActor2D;
class vtkRenderer;

#define IRENDERTOOLBACKGROUND_BACK_LAYER		0
#define IRENDERTOOLBACKGROUND_FRONT_LAYER		1


class iRenderToolBackground : public vtkObjectBase
{

public:

	vtkTypeMacro(iRenderToolBackground,vtkObjectBase);
	static iRenderToolBackground* New();

	inline vtkRenderer* GetRenderer() const { return mRenderer; }

	void SetColor(const iColor &color);
	inline const iColor& GetColor() const { return mColor; }

	void SetImage(const iImage &image);
	inline const iImage& GetImage() const { return mImage; }
	void SetImageFixedAspect(bool s);
	void SetImageTile(const float tile[4]);

	void Copy(const iRenderToolBackground *source);

protected:
	
	virtual ~iRenderToolBackground();

private:

	iRenderToolBackground();

	iColor mColor;
	iImage mImage;

	vtkActor2D *mActor;
	iRenderToolBackgroundImageMapper *mMapper;
	vtkRenderer *mRenderer;
};

#endif // IRENDERTOOLBACKGROUND_H

