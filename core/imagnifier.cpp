/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "imagnifier.h"


#include "ierror.h"
#include "itransform.h"
#include "ivtk.h"

#include <vtkCamera.h>
#include <vtkImageData.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkStreamingDemandDrivenPipeline.h>


iMagnifier* iMagnifier::New()
{
	return new iMagnifier;
}


iMagnifier::iMagnifier()
{
	this->Magnification = 1;
	mTileX = mTileY = 0;
}


iMagnifier::~iMagnifier()
{
}


int iMagnifier::ProcessRequest(vtkInformation* request, vtkInformationVector** inputVector, vtkInformationVector* outputVector)
{
	// generate the data
	if(request->Has(vtkDemandDrivenPipeline::REQUEST_DATA()))
	{
		this->RequestData(request, inputVector, outputVector);
		return 1;
	}

	// execute information
	if(request->Has(vtkDemandDrivenPipeline::REQUEST_INFORMATION()))
	{
		this->RequestInformation(request, inputVector, outputVector);
		return 1;
	}

	return this->Superclass::ProcessRequest(request, inputVector, outputVector);
}


void iMagnifier::RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector* outputVector)
{
	vtkInformation *outInfo = outputVector->GetInformationObject(0);
	vtkImageData *data = vtkImageData::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));
	data->SetExtent(outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_EXTENT()));

#ifdef IVTK_5
	data->AllocateScalars();
#else
	data->AllocateScalars(outInfo);
#endif

	this->Render(data);
}


void iMagnifier::Render(vtkImageData *data)
{
	//
	//  The code between === lines is copied from vtkRenderLargeImage.cxx file with minor modifications, as marked behind /// comments
	//  Below is the original copyright notice.
	//
	//  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
	//  All rights reserved.
	//  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.
	//
	// =========================================================================
	//
	int inExtent[6];

	/// ++++++ Added ++++++++++++++++++++++++++++++++++++++++++++
	vtkIdType inIncr[3];
	/// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	int *size;
	int inWindowExtent[4];

	/// ------ Removed ------------------------------------------
	///	double viewAngle, parallelScale;
	/// ---------------------------------------------------------
	
	double windowCenter[2];
	vtkCamera *cam;
	unsigned char *pixels, *outPtr;
	int x, y, row;
	int rowSize, rowStart, rowEnd, colStart, colEnd;
	int doublebuffer;
	int swapbuffers = 0;

	if (this->GetOutput()->GetScalarType() != VTK_UNSIGNED_CHAR)
	{
		vtkErrorMacro("mismatch in scalar types!");
		return;
	}

	// Get the requested extents.
	this->GetOutput()->GetExtent(inExtent);

	// get and transform the increments
	data->GetIncrements(inIncr);

	// get the size of the render window
	size = this->Input->GetRenderWindow()->GetSize();

	// convert the request into windows
	inWindowExtent[0] = inExtent[0]/size[0];
	inWindowExtent[1] = inExtent[1]/size[0];
	inWindowExtent[2] = inExtent[2]/size[1];
	inWindowExtent[3] = inExtent[3]/size[1];

	// store the old view angle & set the new
	cam = this->Input->GetActiveCamera();
	cam->GetWindowCenter(windowCenter);

	/// ------ Removed ------------------------------------------
	///	viewAngle = cam->GetViewAngle();
	///	parallelScale = cam->GetParallelScale();
	///	cam->SetViewAngle(asin(sin(viewAngle*3.1415926/360.0)/this->Magnification) 
	///		* 360.0 / 3.1415926);
	///	cam->SetParallelScale(parallelScale/this->Magnification);
	/// ---------------------------------------------------------
	
	/// ++++++ Added ++++++++++++++++++++++++++++++++++++++++++++
	//
	//  A better way to magnify is to use the camera's UserTransform
	//
	vtkHomogeneousTransform *ct = cam->GetUserTransform();
	if(ct != 0) ct->Register(this);

	iTransform *ut = iTransform::New();
	cam->SetUserTransform(ut);
	ut->Delete();
	/// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// are we double buffering?  If so, read from back buffer ....
	doublebuffer = this->Input->GetRenderWindow()->GetDoubleBuffer();
	if (doublebuffer) 
	{
		// save swap buffer state to restore later
		swapbuffers = this->Input->GetRenderWindow()->GetSwapBuffers();
		this->Input->GetRenderWindow()->SetSwapBuffers(0);
	}

	// render each of the tiles required to fill this request
	for (y = inWindowExtent[2]; y <= inWindowExtent[3]; y++)
	{
		for (x = inWindowExtent[0]; x <= inWindowExtent[1]; x++)
		{
			/// ------ Removed ------------------------------------------
			///			cam->SetWindowCenter(x*2 - this->Magnification*(1-windowCenter[0]) + 1, 
			///				y*2 - this->Magnification*(1-windowCenter[1]) + 1);
			/// ---------------------------------------------------------
			
			/// ++++++ Added ++++++++++++++++++++++++++++++++++++++++++++
			//
			//  This is where the bug is - in perspective projection the view is skewed, so
			//  a better way to magnify is to use the camera's UserTransform.
			//
			mTileX = x;
			mTileY = y;
			ut->Identity();
			ut->Scale(this->Magnification,this->Magnification,1);
			ut->Translate(-double(2*x+1-this->Magnification)/this->Magnification,-double(2*y+1-this->Magnification)/this->Magnification,0.0);
			/// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++

			this->Input->GetRenderWindow()->Render();
			pixels = this->Input->GetRenderWindow()->GetPixelData(0,0,size[0] - 1,
				size[1] - 1,
				!doublebuffer);

			// now stuff the pixels into the data row by row
			colStart = inExtent[0] - x*size[0];
			if (colStart < 0)
			{
				colStart = 0;
			}
			colEnd = size[0] - 1;
			if (colEnd > (inExtent[1] - x*size[0]))
			{
				colEnd = inExtent[1] - x*size[0];
			}
			rowSize = colEnd - colStart + 1;

			// get the output pointer and do arith on it if necc
			outPtr = 
				(unsigned char *)data->GetScalarPointer(inExtent[0],inExtent[2],0);
			outPtr = outPtr + (x*size[0] - inExtent[0])*inIncr[0] + 
				(y*size[1] - inExtent[2])*inIncr[1];

			rowStart = inExtent[2] - y*size[1];
			if (rowStart < 0)
			{
				rowStart = 0;
			}
			rowEnd = size[1] - 1;
			if (rowEnd > (inExtent[3] - y*size[1]))
			{
				rowEnd = (inExtent[3] - y*size[1]);
			}
			for (row = rowStart; row <= rowEnd; row++)
			{
				memcpy(outPtr + row*inIncr[1] + colStart*inIncr[0], 
					pixels + row*size[0]*3 + colStart*3, rowSize*3);
			}
			// free the memory
			delete [] pixels;
		}
	}

	// restore the state of the SwapBuffers bit before we mucked with it.
	if (doublebuffer && swapbuffers)
	{
		this->Input->GetRenderWindow()->SetSwapBuffers(swapbuffers);
	}

	/// ------ Removed ------------------------------------------
	///	cam->SetViewAngle(viewAngle);
	///	cam->SetParallelScale(parallelScale);
	/// ---------------------------------------------------------
	
	/// ++++++ Added ++++++++++++++++++++++++++++++++++++++++++++
	cam->SetUserTransform(ct);
	if(ct != 0) ct->Delete();
	/// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	cam->SetWindowCenter(windowCenter[0],windowCenter[1]);
	//
	// =========================================================================
	//
}
