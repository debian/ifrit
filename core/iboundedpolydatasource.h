/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IBOUNDEDPOLYDATASOURCE_H
#define IBOUNDEDPOLYDATASOURCE_H


#include "igenericfilter.h"
#include <vtkPolyDataAlgorithm.h>


class iLimits;
class iTransform;

class vtkClipPolyData;
class vtkCommand;
class vtkPlanes;
class vtkTransformPolyDataFilter;


class iBoundedPolyDataSource : public iGenericPolyDataFilter<vtkPolyDataAlgorithm>
{

public:
	
	vtkTypeMacro(iBoundedPolyDataSource,vtkPolyDataAlgorithm);

	unsigned long AddObserver(unsigned long e, vtkCommand *c, float priority = 0.0);
	
	void SetCenter(const double x[3]); 
	inline const double* GetCenter() const { return mCenter; }
		
	void SetResolution(int r); 
	inline int GetResolution() const { return mResolution; }
		
	void SetBounds(double b0, double b1, double b2, double b3, double b4, double b5); 
	inline const double* GetBounds() const { return mBounds; }

	void SetRadius(double r);
	inline double GetRadius() const { return mRadius; }

	void SetNormal(const double n[3]);
	inline const double* GetNormal() const { return mNormal; }

	float GetMemorySize() const;

protected:
	
	iBoundedPolyDataSource(iDataConsumer *consumer, bool normalCanChange, bool radiusCanChange);
	virtual ~iBoundedPolyDataSource();

	virtual void ProvideOutput();

	virtual void UpdateBoundaryConditions() = 0;
	virtual void UpdateSourceResolution() = 0;
	virtual void AddObserverToSource(unsigned long e, vtkCommand *c, float priority) = 0;
	virtual float GetSourceMemorySize() const = 0;

	double mBounds[6];
	double mCenter[3], mNormal[3], mRadius;
	int mResolution;

	bool mNormalChanged;

	const bool mNormalCanChange, mRadiusCanChange;

	//
	//  VTK stuff
	//
	iTransform *mTransform;
	vtkPlanes *mClipPlanes;
	vtkClipPolyData *mClipper;
	vtkTransformPolyDataFilter *mFilter;
};

#endif // IBOUNDEDPOLYDATASOURCE_H

