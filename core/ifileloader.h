/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A base class for all classes for reading data files. Each iFileLoader loads a single kind of data,
//  but may provide data for multiple data types.
//
#ifndef IFILELOADER_H
#define IFILELOADER_H


#include <vtkObjectBase.h>
#include "iviewmodulecomponent.h"


#include "iarray.h"
#include "istring.h"

#include <vtkSetGet.h>

class iDataLimits;
class iDataReader;
class iDataReaderExtension;
class iDataSubject;
class iDataInfo;
class iDataType;
class iFile;

class vtkDataSet;


namespace iParameter
{
	//
    // Data parameters
	//
	namespace BoundaryConditions
	{
		const int None =	 0;
		const int Periodic = 1;

		inline bool IsValid(int m){ return m>=0 && m<=1; }
	};
};


//
//  Helper macros
//
#define	IDATAFILELOADER_DECLARE_SWAP_BYTES_FUNCTIONS(_type_) \
	static void SwapBytes(_type_ &p); \
	static void SwapBytesRange(_type_ *data, vtkIdType count)

#define	IDATAFILELOADER_DECLARE_READ_BLOCK_FUNCTIONS(_type_) \
	bool ReadBlock(iFile& F, _type_ *p, vtkIdType len, double updateStart = 0.0, double updateDuration = 0.0); \
	ReadingBuffer Buffer(_type_ &d) const; \
	ReadingBuffer Buffer(_type_ *d, vtkIdType l) const


class iFileLoader : public vtkObjectBase, public iViewModuleComponent
{

	friend class iDataReader;
	friend class iDataSubject;

public:

	vtkTypeMacro(iFileLoader,vtkObjectBase);

	IDATAFILELOADER_DECLARE_SWAP_BYTES_FUNCTIONS(vtkTypeInt32);
	IDATAFILELOADER_DECLARE_SWAP_BYTES_FUNCTIONS(vtkTypeInt64);
	IDATAFILELOADER_DECLARE_SWAP_BYTES_FUNCTIONS(vtkTypeFloat32);
	IDATAFILELOADER_DECLARE_SWAP_BYTES_FUNCTIONS(vtkTypeFloat64);

	//
	//  Operation on data
	//
	void LoadFile(const iString &fname);
	void LoadFile(const iString &fname, const double *shift);
	void EraseData();
	void Reset();
	void ShiftData(const double *shift);

	inline bool IsTwoCopies() const { return mTwoCopies; }
	void SetTwoCopies(bool s);

	inline bool IsAnimatable() const { return (mRecord >= 0); }

	inline const iString& GetLastFileName() const { return mLastFileName; }
	inline const iString GetFileRoot() const { return mFileRoot; }
	virtual const iString GetFileName(int rec) const;
	iString GetRecordAsString(int rec) const;
	bool IsSeriesFileName(const iString &fname) const;

	virtual float GetMemorySize() const;

	//
	//  Boundary conditions
	//
	int GetBoundaryConditions() const;
	bool IsDirectionPeriodic(int d) const;

	//
	//  Access to components
	//
	inline iDataReader* GetReader() const { return mReader; }
	inline iDataReaderExtension* GetReaderExtension() const { return mReaderExtension; }
	bool IsThereData() const;
	virtual bool IsThereData(int n) const;
	virtual bool IsThereScalarData(int n) const;
	virtual bool IsThereVectorData(int n) const;
	virtual bool IsThereTensorData(int n) const;
	virtual vtkDataSet* GetData(int n) const;

	bool IsUsingData(const iDataType &type) const;
	const iDataType& GetDataType(int n) const;
	const iDataInfo& GetDataInfo() const;
	virtual int DissectFileName(const iString &fname, iString &root, iString &suffix) const;
	inline int NumStreams() const { return mStreams.Size(); }

	bool IsThereData(const iDataType &type) const;
	vtkDataSet* GetData(const iDataType &type) const;

	//
	//  Some of children and collaborating loaders may need access to subject limits,
	//  but they should not use subjects directly.
	//
	virtual iDataLimits* GetLimits(int n) const;

	inline int GetPriority() const { return mPriority; }

	//
	//  Helper struct
	//
	struct ReadingBuffer
	{
		void *Data;
		vtkIdType Length;
		int Type, Size;
		ReadingBuffer(void *d, vtkIdType l, int t, int s)
		{
			Data = d;
			Length = l;
			Type = t;
			Size = s;
		}
	};

protected:

	iFileLoader(iDataReader *r, int priority);
	iFileLoader(iDataReaderExtension *ext, int priority);
	virtual ~iFileLoader();

	virtual void LoadFileBody(const iString &suffix, const iString &fname) = 0;
	virtual void ShiftDataBody(vtkDataSet *data, const double *dx) = 0;
	virtual void Polish(vtkDataSet *data); // optional final touch

	//
	//  Output information about the data.
	//
	virtual void OutputDataDescription() const;

	//
	//  File reading helpers
	//
	IDATAFILELOADER_DECLARE_READ_BLOCK_FUNCTIONS(vtkTypeInt32);
	IDATAFILELOADER_DECLARE_READ_BLOCK_FUNCTIONS(vtkTypeInt64);
	IDATAFILELOADER_DECLARE_READ_BLOCK_FUNCTIONS(vtkTypeFloat32);
	IDATAFILELOADER_DECLARE_READ_BLOCK_FUNCTIONS(vtkTypeFloat64);

	bool DetectFortranFileStructure(iFile &F, vtkIdType len);
	bool ReadFortranHeaderFooter(iFile& F, vtkIdType &lrec);

	bool SkipFortranRecord(iFile& F, vtkIdType len);

	bool ReadFortranRecord(iFile& F, const ReadingBuffer &b, double updateStart = 0.0, double updateDuration = 0.0, bool autoswap = true, bool full = true);
	bool ReadFortranRecord(iFile& F, const ReadingBuffer &b1, const ReadingBuffer &b2, double updateStart = 0.0, double updateDuration = 0.0, bool autoswap = true);
	bool ReadFortranRecord(iFile& F, const ReadingBuffer &b1, const ReadingBuffer &b2, const ReadingBuffer &b3, double updateStart = 0.0, double updateDuration = 0.0, bool autoswap = true);
	bool ReadFortranRecord(iFile& F, int nbufs, const ReadingBuffer *b, double updateStart = 0.0, double updateDuration = 0.0, bool autoswap = true); // read any number of buffers

	void SetDirectionPeriodic(int d, bool s);

	//
	//  Data management
	//
	struct Stream
	{
		iDataSubject *Subject;
		vtkDataSet *ReleasedData;
		bool Periodic[3];

		Stream();
		virtual ~Stream();
	};

	virtual Stream* CreateNewStream() const;
	inline Stream* GetStream(int i) const { return mStreams[i]; } // no bound checking for speed
	void AttachDataToStream(int i, vtkDataSet *ds);

	//
	//  Components
	//
	bool mPeriodic[3];
	bool mTwoCopies, mIsBigEndian;
	int mRecord, mFortranHeaderFooterLength;

private:

	void Define();

	double mShift[3];
	const int mPriority;
	iString mFileRoot, mFileSuffix, mLastFileName;

	iDataReader *mReader;
	iDataReaderExtension *mReaderExtension;

	mutable iDataInfo *mInfo;
	iArray<Stream*> mStreams;

	//
	//  Loaders should not be really accessing subjects, that may create cyclic dependency
	//
	iDataSubject* GetSubject(const iDataType &type) const;
};

#endif  // IFILELOADER_H

