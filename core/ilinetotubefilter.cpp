/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ilinetotubefilter.h"


#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkFloatArray.h>
#include <vtkMath.h>
#include <vtkPointData.h>
#include <vtkPolyLine.h>

//
//  Templates
//
#include "igenericfilter.tlh"


iLineToTubeFilter::iLineToTubeFilter(iDataConsumer *consumer) : iGenericPolyDataFilter<vtkTubeFilter>(consumer,1,true)
{
}
	

void iLineToTubeFilter::ProvideOutput()
{
	vtkPolyData *input = this->InputData();
	vtkPolyData *output = this->OutputData();

	//
	//  The code between === lines is copied from vtkTubeFilter.cxx file with minor modifications, as marked behind /// comments
	//  Below is the original copyright notice.
	//
	//  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
	//  All rights reserved.
	//  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.
	//
	// =========================================================================
	//
	vtkPointData *pd=input->GetPointData();
	vtkPointData *outPD=output->GetPointData();
	vtkCellData *cd=input->GetCellData();
	vtkCellData *outCD=output->GetCellData();
	vtkCellArray *inLines = NULL;
	vtkDataArray *inNormals;
	vtkDataArray *inScalars=pd->GetScalars();
	vtkDataArray *inVectors=pd->GetVectors();

	vtkPoints *inPts;
	vtkIdType numPts = 0;
	vtkIdType numLines;
	vtkIdType numNewPts, numNewCells;
	vtkPoints *newPts;
	int deleteNormals=0;
	vtkFloatArray *newNormals;
	vtkIdType i;
	double range[2], maxSpeed=0;
	vtkCellArray *newStrips;
	vtkIdType npts=0, *pts=NULL;
	vtkIdType offset=0;
	vtkFloatArray *newTCoords=NULL;
	int abort=0;
	vtkIdType inCellId;
	double oldRadius=1.0;

	// Check input and initialize
	//
	vtkDebugMacro(<<"Creating tube");

	if ( !(inPts=input->GetPoints()) || 
		(numPts = inPts->GetNumberOfPoints()) < 1 ||
		!(inLines = input->GetLines()) || 
		(numLines = inLines->GetNumberOfCells()) < 1 )
	{
		return;
	}

	// Create the geometry and topology
	numNewPts = numPts * this->NumberOfSides;

	/// ------ Removed ------------------------------------------
	/// newPts = vtkPoints::New();
	/// ---------------------------------------------------------

	/// ++++++ Added ++++++++++++++++++++++++++++++++++++++++++++
	newPts = vtkPoints::New(inPts->GetDataType());
	/// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	newPts->Allocate(numNewPts);
	newNormals = vtkFloatArray::New();
	newNormals->SetName("TubeNormals");
	newNormals->SetNumberOfComponents(3);
	newNormals->Allocate(3*numNewPts);
	newStrips = vtkCellArray::New();
	newStrips->Allocate(newStrips->EstimateSize(1,numNewPts));
	vtkCellArray *singlePolyline = vtkCellArray::New();

	// Point data: copy scalars, vectors, tcoords. Normals may be computed here.
	outPD->CopyNormalsOff();
	if ( (this->GenerateTCoords == VTK_TCOORDS_FROM_SCALARS && inScalars) ||
		this->GenerateTCoords == VTK_TCOORDS_FROM_LENGTH ||
		this->GenerateTCoords == VTK_TCOORDS_FROM_NORMALIZED_LENGTH )
	{
		newTCoords = vtkFloatArray::New();
		newTCoords->SetNumberOfComponents(2);
		newTCoords->Allocate(numNewPts);
		outPD->CopyTCoordsOff();
	}
	outPD->CopyAllocate(pd,numNewPts);

	int generateNormals = 0;
	if ( !(inNormals=pd->GetNormals()) || this->UseDefaultNormal )
	{
		deleteNormals = 1;
		inNormals = vtkFloatArray::New();
		inNormals->SetNumberOfComponents(3);
		inNormals->SetNumberOfTuples(numPts);

		if ( this->UseDefaultNormal )
		{
			for ( i=0; i < numPts; i++)
			{
				inNormals->SetTuple(i,this->DefaultNormal);
			}
		}
		else
		{
			// Normal generation has been moved to lower in the function.
			// This allows each different polylines to share vertices, but have
			// their normals (and hence their tubes) calculated independently
			generateNormals = 1;
		}      
	}

	// If varying width, get appropriate info.
	//
	if ( inScalars )
	{
		inScalars->GetRange(range,0);
		if ((range[1] - range[0]) == 0.0)
		{
			if (this->VaryRadius == VTK_VARY_RADIUS_BY_SCALAR )
			{
				vtkWarningMacro(<< "Scalar range is zero!");
			}
			range[1] = range[0] + 1.0;
		}
		if (this->VaryRadius == VTK_VARY_RADIUS_BY_ABSOLUTE_SCALAR)
		{
			// temporarily set the radius to 1.0 so that radius*scalar = scalar
			oldRadius = this->Radius;
			this->Radius = 1.0;
			if (range[0] < 0.0)
			{
				vtkWarningMacro(<< "Scalar values fall below zero when using absolute radius values!");
			}
		}
	}
	if ( inVectors )
	{
		maxSpeed = inVectors->GetMaxNorm();
	}

	// Copy selected parts of cell data; certainly don't want normals
	//
	numNewCells = inLines->GetNumberOfCells() * this->NumberOfSides + 2;
	outCD->CopyNormalsOff();
	outPD->CopyAllocate(pd,numNewCells);

	//  Create points along each polyline that are connected into NumberOfSides
	//  triangle strips. Texture coordinates are optionally generated.
	//
	this->Theta = 2.0*vtkMath::Pi() / this->NumberOfSides;
	vtkPolyLine *lineNormalGenerator = vtkPolyLine::New();
	for (inCellId=0, inLines->InitTraversal(); 
		inLines->GetNextCell(npts,pts) && !abort; inCellId++)
	{
		this->UpdateProgress((double)inCellId/numLines);
		abort = this->GetAbortExecute();

		if (npts < 2)
		{
			/// ------ Removed ------------------------------------------
			/// vtkWarningMacro(<< "Less than two points in line!");
			/// ---------------------------------------------------------
			continue; //skip tubing this polyline
		}

		// If necessary calculate normals, each polyline calculates its
		// normals independently, avoiding conflicts at shared vertices.
		if (generateNormals) 
		{
			singlePolyline->Reset(); //avoid instantiation
			singlePolyline->InsertNextCell(npts,pts);
			if ( !lineNormalGenerator->GenerateSlidingNormals(inPts,singlePolyline,
				inNormals) )
			{
				vtkWarningMacro(<< "No normals for line!");
				continue; //skip tubing this polyline
			}
		}

		// Generate the points around the polyline. The tube is not stripped
		// if the polyline is bad.
		//
		if ( !this->GeneratePoints(offset,npts,pts,inPts,newPts,pd,outPD,
			newNormals,inScalars,range,inVectors,
			maxSpeed,inNormals) )
		{
			vtkWarningMacro(<< "Could not generate points!");
			continue; //skip tubing this polyline
		}

		// Generate the strips for this polyline (including caps)
		//
		this->GenerateStrips(offset,npts,pts,inCellId,cd,outCD,newStrips);

		// Generate the texture coordinates for this polyline
		//
		if ( newTCoords )
		{
			this->GenerateTextureCoords(offset,npts,pts,inPts,inScalars,newTCoords);
		}

		// Compute the new offset for the next polyline
		offset = this->ComputeOffset(offset,npts);

	}//for all polylines

	singlePolyline->Delete();

	// reset the radius to ite orginal value if necessary
	if (this->VaryRadius == VTK_VARY_RADIUS_BY_ABSOLUTE_SCALAR)
	{
		this->Radius = oldRadius;
	}

	// Update ourselves
	//
	if ( deleteNormals )
	{
		inNormals->Delete();
	}

	if ( newTCoords )
	{
		outPD->SetTCoords(newTCoords);
		newTCoords->Delete();
	}

	output->SetPoints(newPts);
	newPts->Delete();

	output->SetStrips(newStrips);
	newStrips->Delete();

	outPD->SetNormals(newNormals);
	newNormals->Delete();
	lineNormalGenerator->Delete();

	output->Squeeze();
	//
	// =========================================================================
	//
}
