/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


//
//  An actor that can be replicated periodically. This is a preferred way to replicate, since
//  we create no new data and re-use mappers.
//
#ifndef IREPLICATEDACTOR_H
#define IREPLICATEDACTOR_H


#include "iactor.h"
#include "ireplicatedelement.h"


class iReplicatedActorDevice;


class iReplicatedActor : public iActor, public iReplicatedElement
{
	
public:
	
	vtkTypeMacro(iReplicatedActor,iActor);
	static iReplicatedActor* New(iViewSubject *owner = 0);

	virtual double* GetBounds();
	virtual void Render(vtkRenderer *ren, vtkMapper *m);

protected:
	
	iReplicatedActor(iViewSubject *owner);
	virtual ~iReplicatedActor();
	
	virtual void UpdateReplicasBody();

private:

	iReplicatedActorDevice *mRealDevice;
	vtkTimeStamp mReplicatedBoundsMTime;
};

#endif // IREPLICATEDACTOR_H
