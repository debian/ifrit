/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Helper class for reading a subset of particles
//
#ifndef IPARTICLEDOWNSAMPLEITERATOR_H
#define IPARTICLEDOWNSAMPLEITERATOR_H


#include "iarray.h"

#include <vtkMath.h>


class vtkDataArray;


class iParticleDownsampleIterator
{

public:

	iParticleDownsampleIterator();
	virtual ~iParticleDownsampleIterator();

	void SetDownsampleMode(int v);
	inline int GetDownsampleMode() const { return mDownsampleMode; }

	inline vtkIdType GetNumTotal(int i) const { if(i>=0 && i<mMasks.Size()) return mMasks[i].NumTotal; else return 0; }
	inline vtkIdType GetNumSelected(int i) const { if(i>=0 && i<mMasks.Size()) return mMasks[i].NumSelected; else return 0; }
	inline vtkIdType GetOffsetSelected(int i) const { if(i>=0 && i<mMasks.Size()) return mMasks[i].OffsetSelected; else return 0; }
	inline int GetCurrentMaskId() const { return wMask - mMasks.Data(); }

	inline int GetNumMasks() const { return mMasks.Size(); }
	vtkIdType GetNumGlobalTotal() const;
	vtkIdType GetNumGlobalSelected() const;

	//
	//  Masking
	//
	void CreateMasks(int n, vtkIdType ntot[], int df[]);
	vtkIdType CreateOneMask(int i, vtkIdType ntot, int df);
	void SkipMask(int n, bool s);

	//
	//  Associated buffer
	//
	bool AttachBuffer(int i, vtkDataArray *array, int offset);
	inline char* GetBuffer(int i) const { if(i>=0 && i<mMasks.Size()) return mMasks[i].Buffer; else return 0; }
	inline int GetBufferWidth(int i) const { if(i>=0 && i<mMasks.Size()) return mMasks[i].BufferWidth; else return 0; }

	//
	//  Operation
	//
	void Start(int m = 0);
	bool IsSelected();
	void Stop(int m = -1);
	
	//
	//  These functions only return valid result between calls to Start() and Stop() and if IsSelected() returns true.
	//
	inline vtkIdType Index() const { return wIdLoc; }
	inline vtkIdType GlobalIndex() const { return wIdGlob; }
	inline char* BufferPtr() const { return wMask->Buffer + wMask->BufferIncrement*wIdLoc; }

protected:

	//
	//  Helper types
	//
	struct Mask
	{
		bool Skip;
		vtkIdType NumTotal;
		vtkIdType NumSkipped;
		vtkIdType OffsetTotal;
		vtkIdType NumSelected;
		vtkIdType OffsetSelected;
		int DownsampleFactor;
		vtkIdType Dim1D, Dim2D;
		float RandomThreshold;
		char *Buffer;
		int BufferWidth, BufferIncrement;
		Mask(){ NumTotal = NumSkipped = OffsetTotal = 0; NumSelected = OffsetSelected = 0; DownsampleFactor = 1; Dim1D = Dim2D = 1; RandomThreshold = 0.0f; Buffer = 0; BufferWidth = BufferIncrement = 0; }
	};

private:

	void AdvanceCurrentMask();
#ifdef I_CHECK
	void ReportBug();
#endif

	//
	//  Members
	//
	int mMaskId, mDownsampleMode;
	iArray<Mask> mMasks;

	//
	//  Work variables
	//
	vtkIdType wIdTot;
	vtkIdType wIdGlob, wIdLoc;
	const Mask *wMask;
};


//
//  Inlined for efficiency
//
inline bool iParticleDownsampleIterator::IsSelected()
{
	wIdTot++;
	if(wIdTot == wMask->OffsetTotal+wMask->NumTotal)
	{
		this->AdvanceCurrentMask(); // advance the current mask; skip skipped and empty masks too
	}

	vtkIdType idt = wIdTot - wMask->OffsetTotal;
	bool ret = false;

	if(wMask->DownsampleFactor < 2)
	{
		ret = (wMask->DownsampleFactor == 1);
	}
	else
	{
		switch(mDownsampleMode)
		{
		case 0:
			{
				if(idt%wMask->DownsampleFactor == 0) ret = true;
				break;
			}
		case 1:
			{
				vtkIdType i2 = idt/wMask->Dim1D;
				vtkIdType i1 = idt - wMask->Dim1D*i2;
				if(i1%wMask->DownsampleFactor==0 && i2%wMask->DownsampleFactor==0 && i1<wMask->Dim1D && i2<wMask->Dim1D) ret = true;
				break;
			}
		case 2:
			{
				vtkIdType i3 = idt/wMask->Dim2D;
				vtkIdType i2 = idt/wMask->Dim1D - wMask->Dim1D*i3;
				vtkIdType i1 = idt - wMask->Dim1D*(i2+wMask->Dim1D*i3);
				if(i1%wMask->DownsampleFactor==0 && i2%wMask->DownsampleFactor==0 && i3%wMask->DownsampleFactor==0 && i1<wMask->Dim1D && i2<wMask->Dim1D && i3<wMask->Dim1D) ret = true;
				break;
			}
		case 3:
			{
				if((wIdLoc+1<wMask->NumSelected) && (wIdLoc+wMask->NumSkipped<=idt || vtkMath::Random()<wMask->RandomThreshold)) ret = true;
				break;
			}
		case 4:
			{
				if(idt < wMask->NumSelected) ret = true;
				break;
			}
		case 5:
			{
				if(idt >= wMask->NumSkipped) ret = true;
				break;
			}
		}
	}

	if(ret)
	{
		wIdGlob++;
		wIdLoc = wIdGlob - wMask->OffsetSelected;
#ifdef I_CHECK
		if(wIdLoc >= wMask->NumSelected) this->ReportBug();
#endif
	}

	return ret;
}

#endif  // IPARTICLEDOWNSAMPLEITERATOR_H

