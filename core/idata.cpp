/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "idata.h"


#include "idirectory.h"
#include "ishell.h"

#include <limits.h>

//
//  Templates
//
#include "iarray.tlh"


using namespace iParameter;


namespace iData_Private
{
	iDataInfo& AllData()
	{
		static iDataInfo tmp;
		return tmp;
	}

	iLookupArray<iString>& Keywords()
	{
		static iLookupArray<iString> array;
		return array;
	}
};


using namespace iData_Private;


//
//  DataType class
//
iDataType::iDataType(int id, const iString& tname, const iString& sname, int rank, const iString &keywords, const iString &env) : mId(id), mTextName(tname), mRank(rank), mKeywords(keywords)
{
	int i, n;

	mLongName = tname.Substitute(" ","");
	mShortName = sname;

	for(i=0; i<AllData().Size(); i++)
	{
		if(this->GetId()==AllData().Type(i).GetId() || mLongName==AllData().Type(i).LongName() || mShortName==AllData().Type(i).ShortName())
		{
			IBUG_FATAL("Incorrectly configured DataType object.");
		}
	}

	//
	//  Analyse keywords
	//
	if(!mKeywords.IsEmpty())
	{
		n = mKeywords.Contains(",");
		for(i=0; i<=n; i++)
		{
			const iString &kw(mKeywords.Section(",",i,i));
			if(Keywords().Find(kw) == -1) Keywords().Add(kw);
		}
	}

	AllData() += *this;

	if(!env.IsEmpty())
	{
		char *e = getenv(env.ToCharPointer());
		if(e!=0 && *e!=0)
		{
			mEnv = iString(e);
			if(!mEnv.EndsWith(iDirectory::Separator())) mEnv += iDirectory::Separator();
			iDirectory::ExpandFileName(mEnv);
		}
	}
}


iDataType::~iDataType()
{
	AllData() -= *this;
}


bool iDataType::MatchesKeyword(const iString &str) const
{
	return (mKeywords.Find(str) != -1);
}


void iDataType::FindTypesByKeywords(iDataInfo &info, const iString &str)
{
	int i, j, n = str.Contains(",");
	iString d;
	info.Clear();
	for(i=0; i<=n; i++)
	{
		d = str.Section(",",i,i);
#ifndef I_NO_CHECK
		if(Keywords().Find(d) == -1)
		{
			IBUG_WARN(d+" is not a valid keyword");
			continue;
		}
#endif
		for(j=0; j<AllData().Size(); j++)
		{
			if(AllData().Type(j).MatchesKeyword(d)) info += AllData().Type(j);
		}
	}
}


const iDataType& iDataType::FindTypeById(iDataId id)
{
	int i;
	for(i=0; i<AllData().Size(); i++) if(AllData().Type(i).GetId() == id) return AllData().Type(i);
	return Null();
}


const iDataType& iDataType::FindTypeByName(const iString &name)
{
	int i;
	for(i=0; i<AllData().Size(); i++) if(AllData().Type(i).IsOfType(name)) return AllData().Type(i);
	return Null();
}


iString iDataType::GetEnvironment(iShell *shell) const
{
	static const iString null;

	if(shell != 0)
	{
		if(mEnv.IsEmpty())
		{
			return shell->GetEnvironment(Environment::Data);
		}
		else
		{
			return mEnv + iDirectory::Separator();
		}
	}
	else return null;
}


//
//  DataInfo class
//
iDataInfo::iDataInfo()
{
}


iDataInfo::iDataInfo(const iDataInfo &set)
{
	int i;
	for(i=0; i<set.Size(); i++) mArr.AddUnique(&(set.Type(i)));
}


iDataInfo::iDataInfo(const iDataType &type)
{
	if(!type.IsNull()) mArr.Add(&type);
}


iDataInfo::~iDataInfo()
{
}


void iDataInfo::Clear()
{
	mArr.Clear();
}


iDataInfo& iDataInfo::operator=(const iDataInfo &info)
{
	int i;

	mArr.Clear();
	for(i=0; i<info.Size(); i++) mArr.Add(&(info.Type(i)));
	return *this;
}


iDataInfo& iDataInfo::operator=(const iDataType &type)
{
	mArr.Clear();
	if(!type.IsNull()) mArr.AddUnique(&type);
	return *this;
}


iDataInfo& iDataInfo::operator+=(const iDataInfo &info)
{
	int i;
	const iOrderedArray<iDataTypePointer> &arr(info.mArr); // cache for speed

	for(i=0; i<arr.Size(); i++) if(!arr[i]->IsNull()) mArr.AddUnique(arr[i]);
	return *this;
}


iDataInfo& iDataInfo::operator+=(const iDataType &type)
{
	if(!type.IsNull()) mArr.AddUnique(&type);
	return *this;
}


iDataInfo& iDataInfo::operator-=(const iDataType &type)
{
	if(!type.IsNull()) mArr.Remove(&type);
	return *this;
}


bool iDataInfo::Includes(const iDataType &type) const
{
	return mArr.Find(&type) >= 0;
}


int iDataInfo::Index(const iDataType &type) const
{
	int i;
	for(i=0; i<mArr.Size(); i++) if(*mArr[i] == type) return i;
	return -1;
}


const iDataType& iDataInfo::Type(int i) const
{
	if(i>=0 && i<mArr.Size()) return *(mArr[i]); else return iDataType::Null();
}


//
//  Defined data types
//
const iDataType& iDataType::Null()
{
	static const iDataType tmp(0,"Null","",0,"","");
	return tmp;
}


//
//  Defined data infos
//
const iDataInfo& iDataInfo::None()
{
	static const iDataInfo none(iDataType::Null());
	return none;
}


const iDataInfo& iDataInfo::Any()
{
	return AllData();
}

