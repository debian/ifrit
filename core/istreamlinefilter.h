/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


//
//	A replacement for vtkStreamLineFilter which is VERY SLOW and takes all of the memory
//

#ifndef ISTREAMLINEFILTER_H
#define ISTREAMLINEFILTER_H


#include <vtkStreamTracer.h>
#include "igenericfilter.h"


class iDataLimits;

class vtkImageData;
class vtkPointSet;


namespace iParameter
{
	namespace StreamLine
	{
		namespace Direction
		{
			const int UpStream = 0;
			const int DownStream = 1;
			const int Forward = 2;
			const int Backward = 3;
			const int BothWays = 4;

			inline bool IsValid(int m){ return m>=0 && m<=4; }
			//const int COUNT = 5;
		};
	};
};


class iStreamLineFilter : public iGenericFilter<vtkStreamTracer,vtkDataSet,vtkPolyData>
{

	iGenericFilterTypeMacro(iStreamLineFilter,vtkStreamTracer);

public:

	vtkDataSet* SourceData();

	void SetLength(float d);
	void SetQuality(int q);
	void SetDirection(int d);
	void SetSplitLines(bool s);
	void SetMinimumSpeed(float v);

	inline float GetLength() const { return mLength; }
	inline int GetQuality() const { return mQuality; }
	inline int GetDirection() const { return mDir; }
	inline bool GetSplitLines() const { return mSplitLines; }
	inline float GetMinimumSpeed() const { return mVmin; }

protected:
	
	virtual void ProvideOutput();

	virtual vtkIdType GetVector(double x[3], float v[3]);
	virtual int FollowLine(double x[3], double &d, float &h, float eps, vtkIdType l);
	
	virtual bool DefinePointers(vtkImageData *input);
	virtual void AssignScalars(float *p, vtkIdType l);
	virtual void GetDmin(vtkIdType l, float &ddmin, float &dx);

	static vtkIdType GetVectorWithCIC(double x[3], float v[3], float *ptrdata, int dims[3], double origin[3], double spacing[3], bool per[3]);

	//
	//  Work variables
	//
	const iDataLimits *wLimits;
	int wDims[3], wNsca;
	float *wPtrVec, *wPtrSca;
	double wOrg[3], wSpa[3];
	bool wPer[3];

	float mDmin;

private:

	void ProvideOutputForImageData(vtkImageData *input, vtkPolyData *output, vtkPointSet *source);

	float mVmin, mEps;
	float mLength;
	int mDir, mQuality, mItMax, mAxisDir;
	bool mSplitLines;

	/* blocked */ vtkDataSet *GetSource(){ return this->vtkStreamTracer::GetSource(); }
};

#endif  // ISTREAMLINEFILTER_H


