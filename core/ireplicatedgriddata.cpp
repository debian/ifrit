/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ireplicatedgriddata.h"


#include "idata.h"
#include "ierror.h"
#include "iparallel.h"
#include "iviewmodule.h"
#include "iviewsubject.h"

#include <vtkImageData.h>
#include <vtkPointData.h>
#include <vtkUnsignedCharArray.h>

//
//  Templates
//
#include "igenericfilter.tlh"


iReplicatedGridData* iReplicatedGridData::New(iViewInstance *owner)
{
	IASSERT(owner);
	return new iReplicatedGridData(owner);
}


iReplicatedGridData::iReplicatedGridData(iViewInstance *owner) : iGenericImageDataFilter<vtkImageAlgorithm>(owner,1,true), iParallelWorker(owner->GetViewModule()->GetParallelManager()), iReplicatedElement(true)
{
	this->ReplicateAs(owner->Owner());
}


void iReplicatedGridData::UpdateReplicasBody()
{
	this->Modified();
}


void iReplicatedGridData::ProvideInfo()
{
	vtkImageData *input = this->InputData();
	vtkImageData *output = this->OutputData();
	int i, ext[6];
	double org[3];

	input->GetExtent(ext);
	input->GetOrigin(org);
	
	for(i=0; i<3; i++)
	{
		ext[2*i+1] = 1 + (ext[2*i+1]-1)*(mReplicationFactors[2*i+1]+mReplicationFactors[2*i]+1);
		org[i] -= 2.0*mReplicationFactors[2*i];
	}

	output->SetExtent(ext);
	output->SetSpacing(input->GetSpacing());
	output->SetOrigin(org);

	vtkInformation* outInfo = wCache.OutputVector->GetInformationObject(0);
	outInfo->Set(vtkStreamingDemandDrivenPipeline::WHOLE_EXTENT(),ext,6);
}


void iReplicatedGridData::ProvideOutput()
{
	vtkImageData *input = this->InputData();
	vtkImageData *output = this->OutputData();
	int i, extDown[3], extUp[3];
	double pos[3];
	
	output->Initialize();

	int n = input->GetNumberOfScalarComponents();
	if(n == 0) return;
	
	bool work = false;
	for(i=0; i<3; i++)
	{
		extDown[i] = -mReplicationFactors[2*i];
		extUp[i] = mReplicationFactors[2*i+1];
		if(extDown[i] != 0) work = true;
		if(extUp[i] != 0) work = true;
	}

	if(!work) 
	{
		output->ShallowCopy(input);
		return;
	}

	input->GetSpacing(pos);
	output->SetSpacing(pos);
	
	input->GetDimensions(wDimsIn);
	input->GetOrigin(pos);

	int next = 1;
	for(i=0; i<3; i++)
	{
		wExt[i] = extUp[i] - extDown[i] + 1;
		wDimsOut[i] = 1 + (wDimsIn[i]-1)*wExt[i];
		next *= wExt[i];
	}
	wSize = input->GetScalarSize()*input->GetNumberOfScalarComponents();
	wSize0 = (wDimsIn[0]-1)*wSize;
	wSize1 = wSize0 + wSize;

		
	for(i=0; i<3; i++) pos[i] += 2.0*extDown[i];
	output->SetOrigin(pos);
	output->SetDimensions(wDimsOut);
#ifdef IVTK_5
	output->SetScalarType(input->GetScalarType());
	output->SetNumberOfScalarComponents(input->GetNumberOfScalarComponents());
	output->AllocateScalars();
#else
	output->AllocateScalars(input->GetScalarType(),input->GetNumberOfScalarComponents());
#endif

	wPtrIn = (char *)input->GetScalarPointer();
	wPtrOut = (char *)output->GetScalarPointer();
	
	this->ParallelExecute(0);
}


int iReplicatedGridData::ExecuteStep(int, iParallel::ProcessorInfo &p)
{
	int j, k, i1, j1, k1;
	int kBeg, kEnd, kStp;
	vtkIdType loff, loff1, loff2;

	iParallel::SplitRange(p,wDimsIn[2],kBeg,kEnd,kStp);

	for(k1=0; k1<wExt[2]; k1++) for(k=kBeg; k<kEnd; k++) 
	{
		if(p.IsMaster()) this->UpdateProgress(double(k-kBeg+wDimsIn[2]*k1)/((kEnd-kBeg)*wExt[2]));
		if(this->GetAbortExecute()) break;

		for(j1=0; j1<wExt[1]; j1++) for(j=0; j<wDimsIn[1]; j++)
		{
			loff1 = wSize*wDimsIn[0]*(j+wDimsIn[1]*k);
			loff2 = wSize*wDimsOut[0]*(j+(wDimsIn[1]-1)*j1+wDimsOut[1]*(k+(wDimsIn[2]-1)*k1));
			for(i1=0; i1<wExt[0]; i1++)
			{
				loff = wSize0*i1 + loff2;
				memcpy(wPtrOut+loff,wPtrIn+loff1,wSize1); 
			}
		}
	}
	return 0;
}


float iReplicatedGridData::GetMemorySize()
{
	if(this->IsReplicated())
	{
		return this->iGenericImageDataFilter<vtkImageAlgorithm>::GetMemorySize();
	}
	else return 0.0;
}
