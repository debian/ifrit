/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef ISCRIPT_H
#define ISCRIPT_H


#include "ishellcomponent.h"


#include "istring.h"


class iScriptIOHelper;
class iScriptObserver;


class iScript : public iShellComponent
{

public:

	static iScript* New(iShell *s);  // factory method
	void Delete();

	inline const iString& Name() const { return mName; }
	virtual iString GetPrompt1() const = 0;
	virtual iString GetPrompt2() const = 0;

	virtual const iString& CreateTemplate() const = 0;

	virtual const iString& GetScriptFileSuffix() const = 0;
	virtual bool IsTraceable() const = 0;

	virtual bool RunInterpreter() = 0;  //  return false if this script cannot run an interpreter
	virtual bool ExecuteSegment(const iString &text) = 0;  // return false on error

	void InstallObserver(iScriptObserver *obs);
	void InstallIOHelper(iScriptIOHelper *ioh);

protected:

	iScript(const iString& name, iShell *s);
	virtual ~iScript();

	virtual void OnInstallObserver();
	virtual void OnInstallIOHelper();

	iString mName;
	iScriptObserver *mObserver;
	iScriptIOHelper *mIOHelper;

private:

	iScript(const iScript&); // not implemented
	void operator=(const iScript&);
};


class iScriptIOHelper
{

public:

	virtual iString Read() = 0;
	virtual int Write(const iString &str, bool err = false) = 0;
};


class iScriptObserver
{

	friend class iScript;

public:

	virtual ~iScriptObserver();

	void Enable(bool s);
	inline bool IsEnabled() const { return mIsEnabled; }

	void OnStart();
	void OnStop(bool aborted);
	void OnBeginLine(int line, const iString &file);
	void OnEndLine(int line, const iString &file);
	bool IsTerminated();
	//void OnFunctionCall(int line, const iString &file, const iString &fun);
	//void OnVariableChange(int line, const iString &file, const iString &var);

protected:

	iScriptObserver();

	iScript* Script() const { return mScript; }

	virtual void OnStartBody() = 0;
	virtual void OnStopBody(bool aborted) = 0;
	virtual void OnBeginLineBody(int line, const iString &file) = 0;
	virtual void OnEndLineBody(int line, const iString &file) = 0;
	virtual bool IsTerminatedBody() = 0;
	//virtual void OnFunctionCallBody(int line, const iString &file, const iString &fun) = 0;
	//virtual void OnVariableChangeBody(int line, const iString &file, const iString &var) = 0;

private:

	iScript *mScript;
	bool mIsEnabled;
};

#endif // ISCRIPT_H
