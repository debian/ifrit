/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "imergedatafilter.h"


#include "idata.h"
#include "idatalimits.h"
#include "idatareader.h"
#include "ierror.h"
#include "imath.h"
#include "iviewmodule.h"

#include <vtkFloatArray.h>
#include <vtkPointData.h>
#include <vtkStreamingDemandDrivenPipeline.h>

//
//  Templates
//
#include "igenericfilter.tlh"


iMergeDataFilter::iMergeDataFilter(iDataConsumer *consumer) : iGenericFilter<vtkDataSetAlgorithm,vtkDataSet,vtkDataSet>(consumer,2,false)
{
	mRank = 0;
}


int iMergeDataFilter::FillInputPortInformation(int port, vtkInformation* info)
{
	if(port == 1)
	{
		info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(),"vtkDataSet");
		info->Set(vtkAlgorithm::INPUT_IS_OPTIONAL(),1);
		return 1;
	}
	else return this->iGenericFilter<vtkDataSetAlgorithm,vtkDataSet,vtkDataSet>::FillInputPortInformation(port,info);
}


void iMergeDataFilter::SetRank(int rank)
{
	if(rank>=0 && rank<3)
	{
		mRank = rank;
	}
}


bool iMergeDataFilter::HasData(int rank)
{
	if(rank>=0 && rank<3)
	{
		this->Update();

		IASSERT(this->OutputData()); 
		vtkPointData *pd = this->OutputData()->GetPointData();
		if(pd == 0) return false;
		switch(rank)
		{
		case 0:	return (pd->GetScalars()!=0 && pd->GetScalars()->GetNumberOfTuples()>0);
		case 1: return (pd->GetVectors()!=0 && pd->GetVectors()->GetNumberOfTuples()>0);
		case 2: return (pd->GetTensors()!=0 && pd->GetTensors()->GetNumberOfTuples()>0);
		}
	}
	return false;
}


void iMergeDataFilter::ProvideInfo()
{
	vtkInformation* inInfo = wCache.InputVector[0]->GetInformationObject(0);
	vtkInformation* outInfo = wCache.OutputVector->GetInformationObject(0);

	outInfo->Set(vtkStreamingDemandDrivenPipeline::WHOLE_EXTENT(),inInfo->Get(vtkStreamingDemandDrivenPipeline::WHOLE_EXTENT()),6);
	//outInfo->Set(vtkStreamingDemandDrivenPipeline::UPDATE_EXTENT(),inInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_EXTENT()),6);

	inInfo = wCache.InputVector[1]->GetInformationObject(0);
	if(inInfo != 0)
	{
		//inInfo->Set(vtkStreamingDemandDrivenPipeline::EXACT_EXTENT(),1);
	}
}


void iMergeDataFilter::ProvideOutput()
{
	vtkDataSet *input = this->InputData();
	vtkDataSet *output = this->OutputData();

	vtkPointData *pd = input->GetPointData();
	if(pd == 0)
	{
		output->Initialize();
		return;
	}

	output->ShallowCopy(input);

	switch(mRank)
	{
	case 0:
		{
			if(this->AreDataSetsCompatible(input,this->InputData(1)) && this->InputData(1)->GetPointData()->GetScalars()!=0)
			{
				output->GetPointData()->SetScalars(this->InputData(1)->GetPointData()->GetScalars());
			}
			break;
		}
	case 1:
		{
			if(this->AreDataSetsCompatible(input,this->InputData(1)) && this->InputData(1)->GetPointData()->GetVectors()!=0)
			{
				output->GetPointData()->SetVectors(this->InputData(1)->GetPointData()->GetVectors());
			}
			break;
		}
	case 2:
		{
			if(this->AreDataSetsCompatible(input,this->InputData(1)) && this->InputData(1)->GetPointData()->GetTensors()!=0)
			{
				output->GetPointData()->SetTensors(this->InputData(1)->GetPointData()->GetTensors());
			}
			break;
		}
	}
}


bool iMergeDataFilter::AreDataSetsCompatible(vtkDataSet *ds1, vtkDataSet *ds2) const
{
	int i, numArrays;

	if(ds1==0 || ds2==0) return false;

	if(ds1->GetNumberOfPoints() != ds2->GetNumberOfPoints()) return false;
	vtkIdType numTuples, numPts = ds1->GetNumberOfPoints();

	numArrays = ds1->GetPointData()->GetNumberOfArrays();
	for(i=0; i<numArrays; i++)
	{
		numTuples = ds1->GetPointData()->GetArray(i)->GetNumberOfTuples();
		if(numTuples>0 && numTuples!=numPts) return false;
	}

	numArrays = ds2->GetPointData()->GetNumberOfArrays();
	for(i=0; i<numArrays; i++)
	{
		numTuples = ds2->GetPointData()->GetArray(i)->GetNumberOfTuples();
		if(numTuples>0 && numTuples!=numPts) return false;
	}

	//
	//  If ImageData, check that the dimensions are identical
	//
	vtkImageData *id1 = vtkImageData::SafeDownCast(ds1);
	vtkImageData *id2 = vtkImageData::SafeDownCast(ds2);
	
	if((id1==0 && id2!=0) || (id1!=0 && id2==0)) return false;
	
	if(id1!=0 && id2!=0)
	{
		int dim1[3], dim2[3];
		id1->GetDimensions(dim1);
		id2->GetDimensions(dim2);
		return (dim1[0]==dim2[0] && dim1[1]==dim2[1] && dim1[2]==dim2[2]);
	}

	//
	//  Other data types can be added here
	//
	return true;
}

