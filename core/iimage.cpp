/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iimage.h"

#include "icolor.h"
#include "ierror.h"
#include "imath.h"
#include "ivtk.h"

#include <vtkBMPReader.h>
#include <vtkImageBlend.h>
#include <vtkImageData.h>
#include <vtkImageGaussianSmooth.h>
#include <vtkImageReader2.h>
#include <vtkJPEGReader.h>
#include <vtkPNGReader.h>
#include <vtkPNMReader.h>
#include <vtkPointData.h>
#include <vtkTIFFReader.h>
#include <vtkUnsignedCharArray.h>


iImage::iImage(int depth)
{
	if(depth<1 || depth>4)
	{
		IBUG_WARN("Incorrect number of image components");
		depth = 3;
	}
	mData = 0;
	mDepth = depth;
	mWidth = mHeight = 0;
}


iImage::iImage(const iImage& img)
{
	mData = img.mData;
	if(mData != 0)
	{
		mData->Register(0);
		mDepth = img.mDepth;
		mWidth = img.mWidth;
		mHeight = img.mHeight;
	}
	else
	{
		mDepth = 3;
		mWidth = mHeight = 0;
	}
}


iImage::~iImage()
{
	if(mData != 0) mData->Delete();
}


vtkImageData* iImage::CreateData(int w, int h, int d)
{
	vtkImageData *tmp;

	tmp = vtkImageData::New(); IERROR_CHECK_MEMORY(tmp);
	tmp->SetOrigin(0.0,0.0,0.0);
	tmp->SetSpacing(1.0,1.0,1.0);
	tmp->SetDimensions(w,h,1);

	if(d != 0)
	{
		if(d < 1 || d>4)
		{
			IBUG_WARN("Incorrect number of image components");
			d = 3;
		}
#ifdef IVTK_5
		tmp->SetScalarType(VTK_UNSIGNED_CHAR);
		tmp->SetNumberOfScalarComponents(d);
		tmp->AllocateScalars();
#else
		tmp->AllocateScalars(VTK_UNSIGNED_CHAR,d);
#endif
	}

	return tmp;
}


unsigned char* iImage::DataPointer() const
{ 
	if(mData != 0) return (unsigned char *)mData->GetScalarPointer(); else return 0;
}


void iImage::SetDataPointer(unsigned char* array, int w, int h, int d)
{
	if(array!=0 && w>0 && h>0 && d>0 && d<5)
	{
		if(mData != 0) mData->Delete();
		mData = CreateData(w,h,0); // do not allocate
		vtkUnsignedCharArray *a;
		if(mData->GetPointData()->GetScalars()==0 || (a = vtkUnsignedCharArray::SafeDownCast(mData->GetPointData()->GetScalars()))==0)
		{
			a = vtkUnsignedCharArray::New(); IERROR_CHECK_MEMORY(a);
			mData->GetPointData()->SetScalars(a);
			a->Delete();
		}
		a->SetNumberOfComponents(d);
		a->SetArray(array,d*w*h,1);
		mDepth = d;
		mWidth = w;
		mHeight = h;
	}
}


void iImage::Clear()
{
	mDepth = 3;
	mWidth = mHeight = 0;
	if(mData != 0)
	{
		mData->Delete();
		mData = 0;
	}
}


bool iImage::LoadFromFile(const iString &filename)
{
	return LoadFromFile(filename,*this);
}


bool iImage::LoadFromFile(const iString &filename, iImage &image)
{
	iString suf = filename.Section(".",-1).Lower();

	vtkImageReader2 *ir = 0;

	if(suf=="jpg" || suf=="jpeg")
	{
		ir = vtkJPEGReader::New(); IERROR_CHECK_MEMORY(ir);
	}
	
	if(suf == "pnm")
	{
		ir = vtkPNMReader::New(); IERROR_CHECK_MEMORY(ir);
	}
	
	if(suf == "bmp")
	{
		ir = vtkBMPReader::New(); IERROR_CHECK_MEMORY(ir);
	}
	
	if(suf=="tif" || suf=="tiff")
	{
		ir = vtkTIFFReader::New(); IERROR_CHECK_MEMORY(ir);
	}
	
	if(suf == "png")
	{
		ir = vtkPNGReader::New(); IERROR_CHECK_MEMORY(ir);
	}
	
	if(ir!=0 && ir->CanReadFile(filename.ToCharPointer())==3)
	{
		ir->SetFileName(filename.ToCharPointer());
		ir->SetDataScalarTypeToUnsignedChar();
		ir->Update();
		if(image.mData != 0) image.mData->Delete();
		image.mData = ir->GetOutput();
		image.mData->Register(0);
		image.mDepth = image.mData->GetNumberOfScalarComponents();
		int dims[3];
		image.mData->GetDimensions(dims);
		image.mWidth = dims[0];
		image.mHeight = dims[1];
		ir->Delete();
		return true;
	}
	else return false;
}


void iImage::Blend(const iImage& im, float op)
{
	vtkImageBlend* iblend;

	if(mData==0 || im.mData==0) return;

	if(op < 0.0) op = 0.0;
	if(op > 1.0) op = 1.0;

	iblend = vtkImageBlend::New(); if(iblend == 0) return;
	iblend->SetInputData(0,im.mData);
	iblend->SetInputData(1,mData);
	iblend->SetOpacity(1,(double)op);	
	iblend->Update();
	mData->Delete();
	mData = iblend->GetOutput();
	mData->Register(0);
	iblend->Delete();
}


void iImage::ReplaceData(vtkImageData *newdata)
{
	if(newdata == 0)
	{
		this->Clear();
		return;
	}

	int dims[3];
	newdata->GetDimensions(dims);

	mDepth = newdata->GetNumberOfScalarComponents();
	mWidth = dims[0];
	mHeight = dims[1];
	//
	//  We need to actually copy the data, not just link it here, since if the data object
	//  changes, we would have no way of knowing it.
	//
	if(mData == 0) mData = CreateData(mWidth,mHeight); 
	mData->DeepCopy(newdata);
}


void iImage::Overlay(int x, int y, const iImage& ovr, float opacity, bool masking)
{
	if(mData==0 || ovr.mData==0 || x<=-ovr.mWidth || x>=mWidth || y<=-ovr.mHeight || y>=mHeight || mDepth!=ovr.Depth()) return;


	if(opacity < 0.0) opacity = 0.0;
	if(opacity > 1.0) opacity = 1.0;

	if(mData->GetReferenceCount() > 1)
	{
		vtkImageData *tmp;
		tmp = vtkImageData::New(); if(tmp == 0) return;
		tmp->DeepCopy(mData);
		mData->Delete();
		mData = tmp;
	}
	//
	//  tmp is now the proper image
	//
	int k, j, i, joff, ioff;
	unsigned long lImage, lOvlay;
	unsigned char *iPtr = (unsigned char *)mData->GetScalarPointer();
	unsigned char *oPtr = (unsigned char *)ovr.mData->GetScalarPointer();
	//
	//  totally transparent colors
	//
	unsigned char tc[4];
	for(k=0; k<mDepth; k++) tc[k] = oPtr[k];
				
	float wI = 1.0 - opacity;
	float wO = opacity;
	for(j=0; j<ovr.mHeight; j++) 
	{
		joff = y + j;
		if(joff>=0 && joff<mHeight)
		{
			//
			//  Special case for efficiency
			//
			if(!masking && opacity>0.9999)
			{
				k = ovr.mWidth;
				if(x+k > mWidth) k = mWidth - x;
				lImage = mDepth*(x+(unsigned long)mWidth*joff);
				lOvlay = mDepth*(0+(unsigned long)ovr.mWidth*j);
				memcpy(iPtr+lImage,oPtr+lOvlay,mDepth*(unsigned long)k);
			}
			else
			{
				for(i=0; i<ovr.mWidth; i++)
				{
					ioff = x + i;
					if(ioff>=0 && ioff<mWidth)
					{
						lImage = mDepth*(ioff+(unsigned long)mWidth*joff);
						lOvlay = mDepth*(i+(unsigned long)ovr.mWidth*j);

						for(k=0; masking && k<mDepth; k++) if(tc[k] != oPtr[lOvlay+k]) break;
						if(k < mDepth)
						{
							for(k=0; k<mDepth; k++) // this is not because I am lazy, it is the fastest way
							{
								iPtr[lImage+k] = (unsigned char)iMath::Round2Int(wI*iPtr[lImage+k]+wO*oPtr[lOvlay+k]);
							}
						}
					}
				}
			}
		}
	}
}

			
iImage& iImage::operator=(const iImage &img)
{
	if(mData != 0) mData->Delete();
	mData = img.mData;
	if(mData != 0)
	{
		mData->Register(0);
		mDepth = img.mDepth;
		mWidth = img.mWidth;
		mHeight = img.mHeight;
	}
	else
	{
		mDepth = 3;
		mWidth = mHeight = 0;
	}
	return *this;
}


bool operator==(const iImage &s1, const iImage &s2)
{
	if(s1.mDepth != s2.mDepth) return false;
	if(s1.mWidth != s2.mWidth) return false;
	if(s1.mHeight != s2.mHeight) return false;
	if(s1.mData == s2.mData) return true;
	if(s1.mData==0 || s2.mData==0) return false;
	unsigned long i, n = s1.mDepth*s1.mWidth*s1.mHeight;
	unsigned char *p1 = (unsigned char *)s1.mData->GetScalarPointer();
	unsigned char *p2 = (unsigned char *)s2.mData->GetScalarPointer();
	if(p1 == p2) return true;
	if(p1==0 || p2==0) return false;
	for(i=0; i<n; i++)
	{
		if(p1[i] != p2[i]) return false;
	}
	//
	//  Should we re-register here? This saves memory...
	//
	return true;
}


//
//  This implementation is motivated by ImageMagick, but does not use their software.
//
void iImage::Scale(int w, int h, ScaleMode m, ScaleFilter f)
{
	float fx, fy;

	if(w==mWidth && h==mHeight) return;

	if(w<1 || h<1)
	{
		this->Clear();
		return;
	}

	if(this->IsEmpty())
	{
		mWidth = w;
		mHeight = h;
		mData = CreateData(w,h,mDepth);
		memset(mData->GetScalarPointer(),0,mDepth*w*h);
		return;
	}

	switch(m)
	{
	case _Fit:
		{
			fx = float(w)/mWidth;
			fy = float(h)/mHeight;
			if(fx < fy) fy = fx;
			w = iMath::Round2Int(fy*mWidth);
			h = iMath::Round2Int(fy*mHeight);
			break;
		}
	case _Include:
		{
			fx = float(w)/mWidth;
			fy = float(h)/mHeight;
			if(fx < fy) fx = fy;
			w = iMath::Round2Int(fx*mWidth);
			h = iMath::Round2Int(fx*mHeight);
			break;
		}
	case _FitMin:
		{
			fx = float(w)/mWidth;
			fy = float(h)/mHeight;
			if(fx < fy) fy = fx;
			if(fy > 1.0) fy = 1.0;
			w = iMath::Round2Int(fy*mWidth);
			h = iMath::Round2Int(fy*mHeight);
			break;
		}
	case _Both:
		{
			break;
		}
	}

	//
	//  w and h are now proper size. Scale linearly first.
	//
	vtkImageData *tmp = CreateData(w,h,mDepth);

	unsigned char *sPtr = (unsigned char *)mData->GetScalarPointer();
	unsigned char *dPtr = (unsigned char *)tmp->GetScalarPointer();

	fx = float(mWidth)/w;
	fy = float(mHeight)/h;

	int *i0l = new int[w]; IERROR_CHECK_MEMORY(i0l);
	int *i0s = new int[w]; IERROR_CHECK_MEMORY(i0s);
	float *x0l = new float[w]; IERROR_CHECK_MEMORY(x0l);
	float *x0s = new float[w]; IERROR_CHECK_MEMORY(x0s);

	int *j0l = new int[h]; IERROR_CHECK_MEMORY(j0l);
	int *j0s = new int[h]; IERROR_CHECK_MEMORY(j0s);
	float *y0l = new float[h]; IERROR_CHECK_MEMORY(y0l);
	float *y0s = new float[h]; IERROR_CHECK_MEMORY(y0s);

	int i, j, k;
	float v, x0, y0, x1, y1, dx, dy;

	for(j=0; j<h; j++)
	{
		y1 = j + 0.5;
		y0 = y1*fy;
		j0l[j] = iMath::Round2Int(y0-0.5);
		if(j0l[j]<0 || j0l[j]>=mHeight)
		{
			IBUG_WARN("Corrupted image.");
			return;
		}
		dy = y0 - 0.5 - j0l[j]; 
		if(dy > 0.0)
		{
			j0s[j] = j0l[j] + 1;
			if(j0s[j] >= mHeight) j0s[j] = j0l[j];
			y0l[j] = 1.0 - dy;
			y0s[j] = dy;
		}
		else
		{
			j0s[j] = j0l[j] - 1;
			if(j0s[j] < 0) j0s[j] = j0l[j];
			y0l[j] = 1.0 + dy;
			y0s[j] = -dy;
		}
	}

	for(i=0; i<w; i++)
	{
		x1 = i + 0.5;
		x0 = x1*fx;
		i0l[i] = iMath::Round2Int(x0-0.5);
		if(i0l[i]<0 || i0l[i]>=mWidth)
		{
			IBUG_WARN("Corrupted image.");
			return;
		}
		dx = x0 - 0.5 - i0l[i]; 
		if(dx > 0.0)
		{
			i0s[i] = i0l[i] + 1;
			if(i0s[i] >= mWidth) i0s[i] = i0l[i];
			x0l[i] = 1.0 - dx;
			x0s[i] = dx;
		}
		else
		{
			i0s[i] = i0l[i] - 1;
			if(i0s[i] < 0) i0s[i] = i0l[i];
			x0l[i] = 1.0 + dx;
			x0s[i] = -dx;
		}
	}

	for(j=0; j<h; j++)
	{
		for(i=0; i<w; i++)
		{
			for(k=0; k<mDepth; k++)
			{
				v = 
					x0l[i]*y0l[j]*sPtr[k+mDepth*(i0l[i]+mWidth*j0l[j])] +
					x0s[i]*y0l[j]*sPtr[k+mDepth*(i0s[i]+mWidth*j0l[j])] +
					x0l[i]*y0s[j]*sPtr[k+mDepth*(i0l[i]+mWidth*j0s[j])] +
					x0s[i]*y0s[j]*sPtr[k+mDepth*(i0s[i]+mWidth*j0s[j])];
				if(v < 0.0) v = 0.0;
				if(v > 255.0) v = 255.0;
				dPtr[k+mDepth*(i+w*j)] = (unsigned char)iMath::Round2Int(v);
			}
		}
	}

	delete [] i0l;
	delete [] i0s;
	delete [] x0l;
	delete [] x0s;
	delete [] j0l;
	delete [] j0s;
	delete [] y0l;
	delete [] y0s;

	mWidth = w;
	mHeight = h;
	mData->Delete();
	mData = tmp;
	
	if(f == _None) return;

	this->Smooth(1.0,1.0);
}


void iImage::Smooth(float dx, float dy)
{
	vtkImageGaussianSmooth *filter = vtkImageGaussianSmooth::New();
	filter->SetDimensionality(3);
	filter->SetStandardDeviations(dx,dy,0.0);
	filter->SetRadiusFactors(1.0*dx,1.0*dy,1.0); // exp(-0.5*3.3^2) = 1/255
	filter->SetInputData(mData);
	filter->Update();
	if(mData != 0) mData->Delete();
	mData = filter->GetOutput();
	mData->Register(0);
	filter->Delete();
}


bool iImage::CombineInPseudoColor(const iImage &imRed, const iImage &imGreen, const iImage &imBlue)
{
	bool rIn(!imRed.IsEmpty()), gIn(!imGreen.IsEmpty()), bIn(!imBlue.IsEmpty());
	int d = 0, w = 0, h = 0;

	if(rIn)
	{
		d = imRed.Depth();
		w = imRed.Width();
		h = imRed.Height();
	}

	if(gIn)
	{
		if(d == 0)
		{
			d = imGreen.Depth();
			w = imGreen.Width();
			h = imGreen.Height();
		}
		else
		{
			if(d != imGreen.Depth()) return false;
			if(w != imGreen.Width()) return false;
			if(h != imGreen.Height()) return false;
		}
	}

	if(bIn)
	{
		if(d == 0)
		{
			d = imBlue.Depth();
			w = imBlue.Width();
			h = imBlue.Height();
		}
		else
		{
			if(d != imBlue.Depth()) return false;
			if(w != imBlue.Width()) return false;
			if(h != imBlue.Height()) return false;
		}
	}

	if((d!=3 && d!=4) || w==0 || h==0) return false; // no non-empty image is specified

	//
	//  Reset the data
	//
	this->Clear();
	mWidth = w;
	mHeight = h;
	mDepth = d;
	mData = CreateData(w,h,d);

	unsigned char *dPtr1, *dPtr2, *dPtr = this->DataPointer(); 
	unsigned char *rPtr1, *rPtr2, *rPtr = imRed.DataPointer();
	unsigned char *gPtr1, *gPtr2, *gPtr = imGreen.DataPointer();
	unsigned char *bPtr1, *bPtr2, *bPtr = imBlue.DataPointer();

	//
	//  Fill in the data
	//
	int i, j, a[3], aMax;
	for(j=0; j<h; j++)
	{
		dPtr1 = dPtr + d*w*j;
		rPtr1 = rPtr + d*w*j;
		gPtr1 = gPtr + d*w*j;
		bPtr1 = bPtr + d*w*j;
		for(i=0; i<w; i++)
		{
			dPtr2 = dPtr1 + d*i;
			rPtr2 = rPtr1 + d*i;
			gPtr2 = gPtr1 + d*i;
			bPtr2 = bPtr1 + d*i;
			//
			//  Opacity
			//
			if(d > 3)
			{
				a[0] = rIn ? rPtr2[3] : 0;
				a[1] = gIn ? gPtr2[3] : 0;
				a[2] = bIn ? bPtr2[3] : 0;
				aMax = (a[0] > a[1]) ? a[0] : a[1];
				aMax = (a[2] > aMax) ? a[2] : aMax;
			}
			else
			{
				aMax = a[0] = a[1] = a[2] = 255;
			}
			//
			//  Combine
			//
			dPtr2[0] = rIn ? (a[0]*(int(rPtr2[0])+int(rPtr2[1])+int(rPtr2[2])))/(3*aMax) : 0;
			dPtr2[1] = gIn ? (a[1]*(int(gPtr2[0])+int(gPtr2[1])+int(gPtr2[2])))/(3*aMax) : 0;
			dPtr2[2] = bIn ? (a[2]*(int(bPtr2[0])+int(bPtr2[1])+int(bPtr2[2])))/(3*aMax) : 0;
			if(d > 3) dPtr2[3] = aMax;
		}
	}

	return true;
}


void iImage::Crop(int x, int y, int w, int h)
{
	if(x < 0) x = 0;
	if(y < 0) y = 0;
	if(x+w > mWidth) w = mWidth - x;
	if(y+h > mHeight) h = mHeight - y;

	//
	//  Do nothing is we don't actually crop anything
	//
	if(x==0 && y==0 && w==mWidth && h==mHeight) return;

	vtkImageData *tmp = this->CreateData(w,h,mDepth);

	int j;
	vtkIdType line = mDepth*w;
	for(j=0; j<h; j++)
	{
		memcpy(tmp->GetScalarPointer(0,j,0),mData->GetScalarPointer(x,y+j,0),line);
	}
	
	mWidth = w;
	mHeight = h;
	mData->Delete();
	mData = tmp;
}


void iImage::DrawFrame(int width, const iColor &color)
{
	if(this->IsEmpty()) return;

	int i, j, k;
	unsigned char *ptr, *ptr0 = (unsigned char *)mData->GetScalarPointer();
	unsigned char c[4];

	if(width > (mWidth+1)/2) width = (mWidth+1)/2;
	if(width > (mHeight+1)/2) width = (mHeight+1)/2;

	c[0] = (unsigned char)color.Red();
	c[1] = (unsigned char)color.Green();
	c[2] = (unsigned char)color.Blue();
	c[3] = (unsigned char)color.Alpha();

	for(j=0; j<mHeight; j++) 
	{
		for(i=0; i<width; i++)
		{
			ptr = ptr0 + mDepth*(i+mWidth*j);
			for(k=0; k<mDepth; k++) // this is not because I am lazy, it is the fastest way
			{
				ptr[k] = c[k];
			}
			ptr = ptr0 + mDepth*(mWidth-1-i+mWidth*j);
			for(k=0; k<mDepth; k++) // this is not because I am lazy, it is the fastest way
			{
				ptr[k] = c[k];
			}
		}
	}

	for(j=0; j<width; j++) 
	{
		for(i=0; i<mWidth; i++)
		{
			ptr = ptr0 + mDepth*(i+mWidth*j);
			for(k=0; k<mDepth; k++) // this is not because I am lazy, it is the fastest way
			{
				ptr[k] = c[k];
			}
			ptr = ptr0 + mDepth*(i+mWidth*(mHeight-1-j));
			for(k=0; k<mDepth; k++) // this is not because I am lazy, it is the fastest way
			{
				ptr[k] = c[k];
			}
		}
	}
}
