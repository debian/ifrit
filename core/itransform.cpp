/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "itransform.h"


#include <vtkMath.h>

#include "ivtk.h"


iTransform* iTransform::New()
{
	return new iTransform;
}


iTransform::iTransform()
{
}


iTransform::~iTransform()
{
}


//
// transform so that Z-axis becomes this direction 
//
void iTransform::SetDirection(double x, double y, double z)
{
	double dir[3];

	dir[0] = x;
	dir[1] = y;
	dir[2] = z;
	this->SetDirection(dir);
}


void iTransform::SetDirection(const float *fdir)
{
	double dir[3];

	dir[0] = fdir[0];
	dir[1] = fdir[1];
	dir[2] = fdir[2];
	this->SetDirection(dir);
}


void iTransform::SetDirection(const double *dir)
{
	const double base[3] = { 0.0, 0.0, 1.0 };
	double axis[3];

	vtkMath::Cross(base,dir,axis);
	float d = vtkMath::Dot(base,dir)/vtkMath::Norm(dir);
	if(vtkMath::Norm(axis) > 0.0)
	{
		this->RotateWXYZ(vtkMath::DegreesFromRadians(acos(d)),axis);
	}
	else
	{
		if(d < 0.0)
		{
			this->Scale(-1.0,-1.0,-1.0);
		}
	}
}
