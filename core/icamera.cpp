/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "icamera.h"


#include "ierror.h"
#include "ieventobserver.h"
#include "irendertool.h"
#include "iviewmodule.h"

#include <vtkCamera.h>
#include <vtkMath.h>
#include <vtkRenderer.h>

//
//  Templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


//
//  Main class
//
iCamera* iCamera::New(iRenderTool *rt)
{
	static iString LongName("Camera");
	static iString ShortName("c");

	IASSERT(rt);
	return new iCamera(rt,LongName,ShortName);
}


iCamera::iCamera(iRenderTool *rt, const iString &fname, const iString &sname) : iObject(rt->GetViewModule(),fname,sname), iViewModuleComponent(rt->GetViewModule()), mPosition(rt->GetViewModule()), mFocalPoint(rt->GetViewModule()),
	Position(Self(),rt->GetViewModule(),static_cast<iPropertyScalarPosition::SetterType>(&iCamera::SetPosition),static_cast<iPropertyScalarPosition::GetterType>(&iCamera::GetPosition),"Position","x"),
	FocalPoint(Self(),rt->GetViewModule(),static_cast<iPropertyScalarPosition::SetterType>(&iCamera::SetFocalPoint),static_cast<iPropertyScalarPosition::GetterType>(&iCamera::GetFocalPoint),"FocalPoint","f"),
	iObjectConstructPropertyMacroS(Vector,iCamera,ViewUp,u),
	iObjectConstructPropertyMacroS(Pair,iCamera,ClippingRange,cr),
	iObjectConstructPropertyMacroS(Bool,iCamera,ParallelProjection,pp),
	iObjectConstructPropertyMacroS(Double,iCamera,ParallelScale,ps),
	iObjectConstructPropertyMacroS(Double,iCamera,ViewAngle,va),
	iObjectConstructPropertyMacroS(Bool,iCamera,ViewAngleVertical,vav),
	iObjectConstructPropertyMacroS(Double,iCamera,EyeAngle,ea),
	iObjectConstructPropertyMacroS(Bool,iCamera,ClippingRangeAuto,cra),
	//
	//  actions
	//
	iObjectConstructPropertyMacroA1(Double,iCamera,Azimuth,a),
	iObjectConstructPropertyMacroA1(Double,iCamera,Elevation,e),
	iObjectConstructPropertyMacroA1(Double,iCamera,Yaw,y),
	iObjectConstructPropertyMacroA1(Double,iCamera,Pitch,p),
	iObjectConstructPropertyMacroA1(Double,iCamera,Roll,r),
	iObjectConstructPropertyMacroA1(Double,iCamera,Zoom,z),
	ResetCamera(Self(),static_cast<iPropertyAction::CallerType>(&iCamera::Reset),"Reset","rs"),
	OrthogonalizeCameraView(Self(),static_cast<iPropertyAction::CallerType>(&iCamera::OrthogonalizeView),"OrthogonalizeView","ov")
{
	mRenderMode = iParameter::RenderMode::Self;

	mRenderTool = rt;
	mDevice = vtkCamera::New(); IERROR_CHECK_MEMORY(mDevice);

	mSyncProjections = false;

	mDevice->ParallelProjectionOn();
	mDevice->Modified();
}


iCamera::~iCamera()
{
	mDevice->Delete();
}


bool iCamera::Reset()
{
	mRenderTool->ResetCamera();
	return true;
}


bool iCamera::OrthogonalizeView()
{
	static const double orts[6][3] = 
	{
		{ -1.0,  0.0,  0.0 },
		{  1.0,  0.0,  0.0 },
		{  0.0, -1.0,  0.0 },
		{  0.0,  1.0,  0.0 },
		{  0.0,  0.0, -1.0 },
		{  0.0,  0.0,  1.0 } 
	};

	//
	//  Orthogonalize the camera position.
	//
	int i, j, imax;
	double d, *v, dmax, pos[3];

	imax = 5;
	dmax = 0.0;
	v = mDevice->GetDirectionOfProjection();
	for(i=0; i<6; i++)
	{
		d = vtkMath::Dot(orts[i],v);
		if(d > dmax)
		{
			dmax = d;
			imax = i;
		}
	}

	d = mDevice->GetDistance();
	v = mDevice->GetFocalPoint();
	for(j=0; j<3; j++) pos[j] = v[j] - d*orts[imax][j];
	mDevice->SetPosition(pos);

	//
	//  Orthogonalize the view up.
	//
	imax = 3;
	dmax = 0.0;
	v = mDevice->GetViewUp();
	for(i=0; i<6; i++)
	{
		d = vtkMath::Dot(orts[i],v);
		if(d > dmax)
		{
			dmax = d;
			imax = i;
		}
	}
	mDevice->SetViewUp(orts[imax]);

	return true;
}


void iCamera::ShiftTo(const iPosition& pos)
{
	int j;
	double cp[3], fp[3];
	mDevice->GetPosition(cp);
	mDevice->GetFocalPoint(fp);
	for(j=0; j<3; j++)
	{
		cp[j] += (pos[j]-fp[j]);
	}
	mDevice->SetPosition(cp);
	mDevice->SetFocalPoint(pos);
}


bool iCamera::GetParallelProjection() const
{
	return mDevice->GetParallelProjection() != 0;
}


double iCamera::GetParallelScale() const
{
	return mDevice->GetParallelScale();
}


double iCamera::GetViewAngle() const
{
	return mDevice->GetViewAngle();
}


bool iCamera::GetViewAngleVertical() const
{
	return mDevice->GetUseHorizontalViewAngle() == 0;
}


double iCamera::GetEyeAngle() const
{
	return mDevice->GetEyeAngle();
}


bool iCamera::GetClippingRangeAuto() const
{
	return mRenderTool->GetAdjustCameraClippingRangeAutomatically();
}


bool iCamera::SetParallelProjection(bool b)
{
	//
	//  We shouldn't reset unless we actually change projections
	//
	if(b != this->GetParallelProjection())
	{
		if(mSyncProjections) this->SyncProjections(); else mRenderTool->ResetCamera();
	}
	mDevice->SetParallelProjection(b?1:0);
	return true;
}


bool iCamera::SetParallelScale(double f)
{
	mDevice->SetParallelScale(f);
	return true;
}


bool iCamera::SetViewAngle(double f)
{
	mDevice->SetViewAngle(f);
	return true;
}


bool iCamera::SetViewAngleVertical(bool b)
{
	mDevice->SetUseHorizontalViewAngle(b?0:1);
	return true;
}


bool iCamera::SetEyeAngle(double f)
{
	mDevice->SetEyeAngle(f);
	return true;
}


bool iCamera::SetClippingRangeAuto(bool b)
{
	mRenderTool->SetAdjustCameraClippingRangeAutomatically(b);
	return true;
}


bool iCamera::CallAzimuth(double f)
{
	mDevice->Azimuth(f);
	return true;
}


bool iCamera::CallElevation(double f)
{
	mDevice->Elevation(f);
	return true;
}


bool iCamera::CallYaw(double f)
{
	mDevice->Yaw(f);
	return true;
}


bool iCamera::CallPitch(double f)
{
	mDevice->Pitch(f);
	return true;
}


bool iCamera::CallRoll(double f)
{
	mDevice->Roll(f);
	return true;
}


bool iCamera::CallZoom(double f)
{
	if(f > 1.0e-30)
	{
		if(mDevice->GetParallelProjection() != 0)
		{
			mDevice->SetParallelScale(mDevice->GetParallelScale()/f);
		}
		else
		{
			mDevice->Dolly(f);
		}
		return true;
	}
	else return false;
}


bool iCamera::SetPosition(const iPosition &p)
{
	mDevice->SetPosition(p);
	return true;
}


const iPosition& iCamera::GetPosition() const
{
	mPosition = mDevice->GetPosition();
	return mPosition;
}


bool iCamera::SetFocalPoint(const iPosition &p)
{
	mDevice->SetFocalPoint(p);
	return true;
}


const iPosition& iCamera::GetFocalPoint() const
{
	mFocalPoint = mDevice->GetFocalPoint();
	return mFocalPoint;
}


bool iCamera::SetViewUp(const iVector3D& v)
{
	mDevice->SetViewUp(v);
	return true;
}


const iVector3D& iCamera::GetViewUp() const
{
	mViewUp = mDevice->GetViewUp();
	return mViewUp;
}


bool iCamera::SetClippingRange(const iPair& v)
{
	double d, dv[2];

	d = mDevice->GetDistance();
	dv[0] = v.Min*d;
	dv[1] = v.Max*d;
	mRenderTool->SetCameraClippingRange(dv);
	return true;
}


const iPair& iCamera::GetClippingRange() const
{
	double d, dv[2];
	mDevice->GetClippingRange(dv);
	d = mDevice->GetDistance();
	if(d > 0.0)
	{
		mClippingRange.Min = dv[0]/d;
		mClippingRange.Max = dv[1]/d;
	}
	return mClippingRange;
}


void iCamera::SyncProjections()
{
	static const double minDistance = 0.0002; // idiotic VTK limit

	double r = 1.0e-35 + tan(0.5*iMath::Deg2Rad(mDevice->GetViewAngle()));

	if(mDevice->GetParallelProjection() != 0)
	{
		double d = mDevice->GetParallelScale()/r;
		if(d > minDistance) mDevice->Dolly(mDevice->GetDistance()/d);
	}
	else
	{
		mDevice->SetParallelScale(mDevice->GetDistance()*r);
	}
	mRenderTool->ResetCameraClippingRange();
}

