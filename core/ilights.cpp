/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ilights.h"

#include "ierror.h"
#include "irendertool.h"
#include "iviewmodule.h"
#include "ivtk.h"

#include <vtkLight.h>
#include <vtkMath.h>
#include <vtkRenderer.h>

//
//  Templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


namespace iLightKit_Private
{
	void UpdateLight(vtkLight *l, double elevation, double azimuth, double ex[3], double ey[3], double ez[3])
	{
		int i;
		double org[3], pos[3];

		elevation = vtkMath::RadiansFromDegrees(elevation);
		azimuth = vtkMath::RadiansFromDegrees(azimuth);

		org[0] = cos(elevation)*sin(azimuth);
		org[1] = sin(elevation); 
		org[2] = cos(elevation)*cos(azimuth);
 
		for(i=0; i<3; i++)
		{
			pos[i] = org[0]*ex[i] + org[1]*ey[i] + org[2]*ez[i];
		}
		l->SetPosition(pos);
	}
};


using namespace iLightKit_Private;


//
//  Main class
//
iLightKit::iLightKit(vtkRenderer *renderer)
{
	this->vtkLightKit::SetKeyLightAngle(45.0,0.0);
	this->vtkLightKit::SetFillLightAngle(-75.0,0.0);
	this->vtkLightKit::SetBackLightAngle(0.0,110.0);

	this->vtkLightKit::SetKeyLightIntensity(1.0);
	this->vtkLightKit::SetKeyToFillRatio(3.0);
	this->vtkLightKit::SetKeyToHeadRatio(6.0);
	this->vtkLightKit::SetKeyToBackRatio(3.0);

	mAngles[0] = mAngles[1] = 0.0;

	//
	//  Initiate lights
	//
	if(renderer != 0) 
	{
		//
		//  This is to fix a bug with off-screen rednering: the key light must be first.
		//
		renderer->AddLight(this->KeyLight);
		renderer->AddLight(this->FillLight);
		renderer->AddLight(this->HeadLight);
		renderer->AddLight(this->BackLight0);
		renderer->AddLight(this->BackLight1);
	}

	this->UpdateAngles();
}


iLightKit::~iLightKit()
{
}


void iLightKit::SetAngles(double a[2])
{
	if(fabs(a[0]-mAngles[0])>1.0e-5 || fabs(a[1]-mAngles[1])>1.0e-5)
	{
		mAngles[0] = a[0];
		mAngles[1] = a[1];
		this->UpdateAngles();
	}
}


void iLightKit::SetIntensities(float i[3])
{
	if(i[0] > 1.0e-10)
	{
		this->SetKeyLightIntensity(i[0]);
		if(i[1] > 1.0e-10) this->SetKeyToFillRatio(i[0]/i[1]);
		if(i[2] > 1.0e-10) this->SetKeyToHeadRatio(i[0]/i[2]);
	}
}


void iLightKit::GetIntensities(float i[3]) const
{
	i[0] = this->KeyLightIntensity;
	i[1] = this->KeyLightIntensity/this->KeyToFillRatio;
	i[2] = this->KeyLightIntensity/this->KeyToHeadRatio;
}


void iLightKit::UpdateAngles()
{
	double ex[3], ey[3], ez[3];

	double ce = cos(vtkMath::RadiansFromDegrees(mAngles[0]));
	double se = sin(vtkMath::RadiansFromDegrees(mAngles[0]));
	double ca = cos(vtkMath::RadiansFromDegrees(mAngles[1]));
	double sa = sin(vtkMath::RadiansFromDegrees(mAngles[1]));

	ex[0] =  ce*ca; ex[1] = 0.0; ex[2] = -ce*sa;
	ey[0] = -se*sa; ey[1] =  ce; ey[2] = -se*ca;
	ez[0] =  ce*sa; ez[1] =  se; ez[2] =  ce*ca;

	UpdateLight(this->KeyLight,this->KeyLightAngle[0],this->KeyLightAngle[1],ex,ey,ez);
	UpdateLight(this->FillLight,this->FillLightAngle[0],this->FillLightAngle[1],ex,ey,ez);
//	UpdateLight(this->Headlight,0.0,0.0,ex,ey,ez);
	UpdateLight(this->BackLight0,this->BackLightAngle[0],this->BackLightAngle[1],ex,ey,ez);
	UpdateLight(this->BackLight1,this->BackLightAngle[0],this->BackLightAngle[1],ex,ey,ez);
}


//
//  Main class
//
iLights* iLights::New(iViewModule *vm)
{
	static iString LongName("Lights");
	static iString ShortName("ls");

	IASSERT(vm);
	return new iLights(vm,LongName,ShortName);
}


iLights::iLights(iViewModule *vm, const iString &fname, const iString &sname) : iObject(vm,fname,sname), iViewModuleComponent(vm),
	iObjectConstructPropertyMacroS(Vector,iLights,Direction,d),
	iObjectConstructPropertyMacroV(Float,iLights,Intensities,i,3),
	iObjectConstructPropertyMacroS(Bool,iLights,TwoSided,ts)
{
	mRenderMode = iParameter::RenderMode::Self;

	mLightKit = new iLightKit(this->GetViewModule()->GetRenderTool()->GetRenderer()); IERROR_CHECK_MEMORY(mLightKit);

	double angles[2];
	mLightKit->GetAngles(angles);

	//
	//  Use vtkLight angle convention
	//
	angles[0] = vtkMath::RadiansFromDegrees(angles[0]);
	angles[1] = vtkMath::RadiansFromDegrees(angles[1]);

	mDirection[0] = cos(angles[0])*sin(angles[1]);
	mDirection[1] = sin(angles[0]);
	mDirection[2] = cos(angles[0])*cos(angles[1]);
}


iLights::~iLights()
{
	mLightKit->Delete();
}



bool iLights::SetDirection(const iVector3D& dir)
{
	double ang[2];

	ang[0] = vtkMath::DegreesFromRadians(atan2(dir[1],sqrt(dir[0]*dir[0]+dir[2]*dir[2]))); 
	ang[1] = vtkMath::DegreesFromRadians(atan2(dir[0],dir[2]));

	mLightKit->SetAngles(ang);
	mDirection = dir;
	return true;
}


bool iLights::SetIntensities(int i, float v)
{
	if(i>=0 && i<3)
	{
		float val[3];
		mLightKit->GetIntensities(val);
		val[i] = v;
		mLightKit->SetIntensities(val);
		return true;
	}
	else return false;
}


float iLights::GetIntensities(int i) const
{
	if(i>=0 && i<3)
	{
		float val[3];
		mLightKit->GetIntensities(val);
		return val[i];
	}
	else return 0.0;
}


bool iLights::SetTwoSided(bool v)
{
	this->GetViewModule()->GetRenderer()->SetTwoSidedLighting(v?1:0);
	return true;
}


bool iLights::GetTwoSided() const
{
	return (this->GetViewModule()->GetRenderer()->GetTwoSidedLighting() != 0);
}

