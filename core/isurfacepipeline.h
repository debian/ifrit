/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef ISURFACEPIPELINE_H
#define ISURFACEPIPELINE_H


#include "ireplicatedviewsubjectpipeline.h"


class iBoundedPlaneSource;
class iBoundedSphereSource;
class iContourFilter;
class iFlipNormalsFilter;
class iOptimizePolyDataFilter;
class iProbeFilter;
class iReducePolyDataFilter;
class iReducePolyDataFilter2;
class iSmoothPolyDataFilter;
class iSurfaceViewInstance;


class iSurfacePipeline : public iReplicatedViewSubjectPipeline
{

	friend class iSurfaceViewInstance;

public:
	
	vtkTypeMacro(iSurfacePipeline,iReplicatedViewSubjectPipeline);

	iPipelineKeyDeclareMacro(iSurfacePipeline,Method);
	iPipelineKeyDeclareMacro(iSurfacePipeline,IsoVar);
	iPipelineKeyDeclareMacro(iSurfacePipeline,IsoLevel);
	iPipelineKeyDeclareMacro(iSurfacePipeline,IsoMethod);
	iPipelineKeyDeclareMacro(iSurfacePipeline,IsoPipeline);
	iPipelineKeyDeclareMacro(iSurfacePipeline,NormalsFlipped);
	iPipelineKeyDeclareMacro(iSurfacePipeline,SphereSize);
	iPipelineKeyDeclareMacro(iSurfacePipeline,SphereResolution);
	iPipelineKeyDeclareMacro(iSurfacePipeline,PlaneDirection);
	iPipelineKeyDeclareMacro(iSurfacePipeline,PlaneResolution);
	iPipelineKeyDeclareMacro(iSurfacePipeline,ObjectPosition);

protected:
	
	iSurfacePipeline(iSurfaceViewInstance *owner);
	virtual ~iSurfacePipeline();

	virtual bool PrepareInput();

	virtual vtkAlgorithm* GetIsoFilter() const; // for extensions' use

	iSurfaceViewInstance *mOwner;

	//
	//  VTK stuff
	//
	iContourFilter *mIsoFilter;
	iProbeFilter *mProbeFilter;

	iReducePolyDataFilter *mReduceFilter;
	iReducePolyDataFilter2 *mReduce2Filter;
	iSmoothPolyDataFilter *mSmoothFilter;
	iFlipNormalsFilter *mFlipNormalsFilter;
	iOptimizePolyDataFilter *mOptimizeFilter;

	iBoundedSphereSource *mSphereSource;
	iBoundedPlaneSource *mPlaneSource;

	int mSpecialVar;
};

#endif // ISURFACEPIPELINE_H

