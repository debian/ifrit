/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IIMAGECOMPOSERWINDOWS_H
#define IIMAGECOMPOSERWINDOWS_H


#include "iarray.h"
#include "icolor.h"
#include "iimage.h"
#include "istring.h"

class iImageComposer;
class iImageComposerForegroundWindow;
class iStereoImage;
class iViewModule;


class iImageComposerWindow
{

public:
	
	enum iImageComposerWindowType
	{
		UNDEFINED = 0,
		BACKGROUND = 1,
		FOREGROUND = 2
	};
	
	virtual ~iImageComposerWindow();

	inline iViewModule* GetViewModule(int win = 0) const { return mViewModules[(win>0 && win<3)?win:0]; }
	void SetViewModule(iViewModule *vm, int win = 0);

	inline iImageComposerWindowType GetType() const { return mType; }
	inline const iImageComposer* GetImageComposer() const { return mComposer; }

	int GetWindowNumber(int win = 0) const;
	virtual void UpdateWindow();

	virtual int GetId() const = 0;
	virtual int GetImageX() const = 0;
	virtual int GetImageY() const = 0;
	virtual int GetImageWidth() const = 0;
	virtual int GetImageHeight() const = 0;
	virtual int GetContentsX() const = 0;
	virtual int GetContentsY() const = 0;
	virtual int GetContentsWidth() const = 0;
	virtual int GetContentsHeight() const = 0;
	virtual int GetWindowWidth() const = 0;
	virtual int GetWindowHeight() const = 0;

	virtual bool IsEmpty() const = 0;
	virtual void Draw(iStereoImage &outIm) const = 0;

	void RegisterZoomTarget(iImageComposerForegroundWindow *win);
	void UnRegisterZoomTarget(iImageComposerForegroundWindow *win);

protected:

	iImageComposerWindow(iViewModule *vm, iImageComposer *ic);

	//
	//  Helper functions
	//
	bool RenderStereoImage(iStereoImage &winIm) const;
	void OverlayImage(iStereoImage &outIm, const iStereoImage &winIm, const iColor &bg) const;

	iViewModule *mViewModules[3];
	iImageComposer *mComposer;
	iImageComposerWindowType mType;

	iLookupArray<iImageComposerForegroundWindow*> mZoomTargets;
};


class iImageComposerBackgroundWindow : public iImageComposerWindow
{

public:

	static iImageComposerBackgroundWindow* New(int tx, int ty, iImageComposer *ic);

	virtual int GetId() const;
	virtual int GetImageX() const;
	virtual int GetImageY() const;
	virtual int GetImageWidth() const;
	virtual int GetImageHeight() const;
	virtual int GetContentsX() const;
	virtual int GetContentsY() const;
	virtual int GetContentsWidth() const;
	virtual int GetContentsHeight() const;
	virtual int GetWindowWidth() const;
	virtual int GetWindowHeight() const;

	virtual bool IsEmpty() const;
	virtual void Draw(iStereoImage &outIm) const;

	int GetTileX() const { return mTileX; }
	int GetTileY() const { return mTileY; }
	const iString& GetWallpaperFile() const { return mWallpaperFile; }
	const iImage& GetWallpaperImage() const { return mWallpaperImage; }

	bool LoadWallpaperImage(const iString &s);
	
protected:

	iImageComposerBackgroundWindow(int tx, int ty, iImageComposer *ic);

private:

	int mTileX, mTileY;
	iImage mWallpaperImage;
	iString mWallpaperFile;

	static iColor mEmptyBackground;
};


class iImageComposerForegroundWindow : public iImageComposerWindow
{

public:

	enum Flags
	{
		_Line11 = 1,
		_Line21 = 2,
		_Line12 = 4,
		_Line22 = 8,
		_4Lines = 15,
		_ShiftX = 16,
		_ShiftY = 32
	};

	static iImageComposerForegroundWindow* New(iViewModule *vm, iImageComposer *ic);
	virtual ~iImageComposerForegroundWindow();

	virtual int GetId() const;
	virtual int GetImageX() const;
	virtual int GetImageY() const;
	virtual int GetImageWidth() const;
	virtual int GetImageHeight() const;
	virtual int GetContentsX() const;
	virtual int GetContentsY() const;
	virtual int GetContentsWidth() const;
	virtual int GetContentsHeight() const;
	virtual int GetWindowWidth() const;
	virtual int GetWindowHeight() const;

	virtual void UpdateWindow();

	virtual bool IsEmpty() const;
	virtual void Draw(iStereoImage &outIm) const;

	inline float GetScale() const { return mScale; }
	inline int GetPositionX() const { return mPosX; }
	inline int GetPositionY() const { return mPosY; }
	inline int GetBorderWidth() const { return mBorderWidth; }
	inline const iColor& GetBorderColor() const { return mBorderColor; }
	
	void SetScale(float s);
	void SetPosition(int x, int y);
	void SetPositionX(int x);
	void SetPositionY(int y);
	void SetBorderWidth(int w);
	void SetBorderColor(const iColor& c);

	void CorrectPosition();

	inline float GetZoomX() const { return mZoomX; }
	inline float GetZoomY() const { return mZoomY; }
	inline float GetZoomFactor() const { return mZoomFactor; }
	inline iImageComposerWindow* GetZoomSource() const { return mZoomSource; }
	inline int GetZoomFlag() const { return mZoomFlag; }

	void SetZoomSource(iImageComposerWindow *source);
	void SetZoomPosition(float x, float y);
	void SetZoomFactor(float factor);
	void SetZoomFlag(int f);

	inline bool IsAutoZoom() const { return mZoomAuto; }

	bool GetCoordinatesForZoomLine(int n, int &x, int &y, int wx, int wy, int ww, int wh) const;

	inline float GetZoomWidth() const { return mZoomFactor*this->GetWindowWidth(); }
	inline float GetZoomHeight() const { return mZoomFactor*this->GetWindowHeight(); }

protected:

	iImageComposerForegroundWindow(iViewModule *vm, iImageComposer *ic);

	void UpdateZoomPosition();
	void UpdateZoomFlag();
	bool SyncZoom();

private:

	float mScale;
	iColor mBorderColor;
	int mBorderWidth, mPosX, mPosY;

	int mZoomFlag;
	bool mZoomAuto;
	float mZoomX, mZoomY, mZoomFactor;
	iImageComposerWindow *mZoomSource;

	static int mPosQuantum;
};



inline void iImageComposerForegroundWindow::SetPositionX(int x)
{
	this->SetPosition(x,mPosY);
}


inline void iImageComposerForegroundWindow::SetPositionY(int y)
{
	this->SetPosition(mPosX,y);
}

#endif // IIMAGECOMPOSERWINDOWS_H

