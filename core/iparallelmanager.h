/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IPARALLELMANAGER_H
#define IPARALLELMANAGER_H


#include <vtkObject.h>
#include "ishellcomponent.h"


class iParallelHelper;
class iParallelUpdateEventObserver;
class iParallelWorker;


class iParallelManager : public vtkObject, public iShellComponent
{

public:

	static iParallelManager* New(iShell *s);

	int ExecuteWorker(iParallelWorker *worker); 

	inline int GetMinNumberOfProcessors() const { return mMinNumProcs; }
	inline int GetMaxNumberOfProcessors() const { return mMaxNumProcs; }
	inline int GetNumberOfProcessors() const { return mNumProcs; }
	
	void SetNumberOfProcessors(int n);

	void StartCounters();
	void StopCounters();

	double GetProcessorExecutionTime(int n) const;
	double GetWallClockExecutionTime() const;

#ifdef I_DEBUG
	static int DebugSwitch;
#endif

protected:

	iParallelManager(iShell *s);
	virtual ~iParallelManager();

	virtual iParallelHelper* CreateHelper() const = 0;

private:

	void Define();

	int mNumProcs, mMinNumProcs, mMaxNumProcs;
	int mCount;

	iParallelHelper *mHelper;
};

#endif  // IPARALLELMANAGER_H
