/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iresampleimagedatafilter.h"


#include "idataconsumer.h"
#include "imath.h"
#include "iviewmodule.h"

#include <vtkImageData.h>
#include <vtkPointData.h>

//
//  Templates
//
#include "igenericfilter.tlh"


iResampleImageDataFilter::iResampleImageDataFilter(iDataConsumer *consumer) : iGenericImageDataFilter<vtkImageAlgorithm>(consumer,1,true)
{
	mResampleRate = 1;
}


void iResampleImageDataFilter::SetResampleRate(int v)
{
	if(v>0 && v!=mResampleRate)
	{
		mResampleRate = v;
		this->Modified();
	}
}


void iResampleImageDataFilter::ComputeOutputShape(int dims[3], double spac[3], int outDims[3], double outSpac[3])
{
	int i;

	if(mResampleRate == 1)
	{
		for(i=0; i<3; i++)
		{
			outDims[i] = dims[i];
			outSpac[i] = spac[i];
		}
	}

	int imax = -1, maxdim = 0;
	for(i=0; i<3; i++)
	{
		if(maxdim < dims[i])
		{
			imax = i;
			maxdim = dims[i];
		}
		outDims[i] = (dims[i]+mResampleRate-1)/mResampleRate; // this should never overshoot
#ifndef I_NO_CHECK
		if((outDims[i]-1)*mResampleRate > dims[i]-1)
		{
			IBUG_FATAL("Dimension overshoot bug.");
		}
#endif
	}

	if(fabs(maxdim*spac[imax]-2.0) < 0.001*spac[imax])
	{
		//
		//  Fitted to the wholebox
		//
		outSpac[imax] = 2.0/outDims[imax];
	}
	else
	{
		outSpac[imax] = spac[imax]*dims[imax]/outDims[imax];
	}

	for(i=0; i<3; i++) if(i != imax)
	{
		if(fabs(spac[i]-spac[imax]) < 0.001*spac[imax])
		{
			outSpac[i] = outSpac[imax]; // keep uniformity if present
		}
		else
		{
			outSpac[i] = spac[i]*dims[i]/outDims[i];
		}
	}
}


void iResampleImageDataFilter::ProvideInfo()
{
	vtkInformation *inInfo = wCache.InputVector[0]->GetInformationObject(0);
	vtkInformation *outInfo = wCache.OutputVector->GetInformationObject(0);

	int i, dims[3], outDims[3], ext[6];
	double spac[3], outSpac[3];

	inInfo->Get(vtkStreamingDemandDrivenPipeline::WHOLE_EXTENT(),ext);
	inInfo->Get(vtkDataObject::SPACING(),spac);

	for(i=0; i<3; i++)
	{
		dims[i] = ext[2*i+1] - ext[2*i] + 1;
	}

	this->ComputeOutputShape(dims,spac,outDims,outSpac);

	for(i=0; i<3; i++)
	{
		ext[2*i] = 0;
		ext[2*i+1] = outDims[i] - 1;
	}

	outInfo->Set(vtkStreamingDemandDrivenPipeline::WHOLE_EXTENT(),ext,6);
	outInfo->Set(vtkDataObject::SPACING(),outSpac,3);
}


void iResampleImageDataFilter::ProvideOutput()
{
	vtkImageData *input = this->InputData();
	vtkImageData *output = this->OutputData();
	
	output->Initialize();

	if(input->GetPointData()->GetScalars()==0 && input->GetPointData()->GetVectors()==0 && input->GetPointData()->GetTensors()==0) return;

	if(mResampleRate == 1)
	{
		output->ShallowCopy(input);
		return;
	}

	int i, dims[3], outDims[3];
	double spac[3], outSpac[3];
	double orig[3];

	input->GetDimensions(dims);
	input->GetSpacing(spac);
	input->GetOrigin(orig);

	this->ComputeOutputShape(dims,spac,outDims,outSpac);

	output->SetDimensions(outDims);
	output->SetSpacing(outSpac);

	output->SetOrigin(orig); // cannot change origin, or the scalars and vectors/tensors will be mismatched

	vtkIdType sizeOut = (vtkIdType)outDims[0]*outDims[1]*outDims[2];
	for(i=0; i<3; i++)
	{
		wInDims[i] = dims[i];
		wOutDims[i] = outDims[i];
#ifndef I_NO_CHECK
		if(wOutDims[i] == 0)
		{
			IBUG_FATAL("Zero dimension bug.");
		}
#endif
	}

	if(input->GetPointData()->GetScalars() != 0)
	{
		if(!wArray.Init(input->GetPointData()->GetScalars(),sizeOut))
		{
			this->SetAbortExecute(1);
			return;
		}
		
		this->ResampleArray();

		output->GetPointData()->SetScalars(wArray.ArrOut);
		wArray.ArrOut->Delete();
	}

	if(input->GetPointData()->GetVectors() != 0)
	{
		if(!wArray.Init(input->GetPointData()->GetVectors(),sizeOut))
		{
			this->SetAbortExecute(1);
			return;
		}
		
		this->ResampleArray();

		output->GetPointData()->SetVectors(wArray.ArrOut);
		wArray.ArrOut->Delete();
	}

	if(input->GetPointData()->GetTensors() != 0)
	{
		if(!wArray.Init(input->GetPointData()->GetTensors(),sizeOut))
		{
			this->SetAbortExecute(1);
			return;
		}
		
		this->ResampleArray();

		output->GetPointData()->SetTensors(wArray.ArrOut);
		wArray.ArrOut->Delete();
	}
}


void iResampleImageDataFilter::ResampleArray()
{
	int i, j, k, m;
	float *ptrIn, *ptrOut;
	int ncomp = wArray.DimIn;
	vtkIdType loffIn, loffOut;
#ifdef I_CHECK
	vtkIdType l = 0;
#endif
#ifndef I_NO_CHECK
	vtkIdType sizeIn = wArray.ArrIn->GetNumberOfTuples();
	vtkIdType sizeOut = wArray.ArrOut->GetNumberOfTuples();
	if(wArray.DimIn != wArray.DimOut)
	{
		IBUG_FATAL("Incompatible arrays.");
	}
#endif

	for(k=0; k<wOutDims[2]; k++)
	{
		this->UpdateProgress(double(k)/wOutDims[2]);
		if(this->GetAbortExecute()) break;

		for(j=0; j<wOutDims[1]; j++)
		{
			loffIn = (vtkIdType)mResampleRate*wInDims[0]*(j+wInDims[1]*k);
			loffOut = (vtkIdType)wOutDims[0]*(j+wOutDims[1]*k);
#ifndef I_NO_CHECK
			if(loffIn+mResampleRate*(wOutDims[0]-1) >= sizeIn)
			{
				IBUG_FATAL("Out-of-bounds bug.");
			}
			if(loffOut+(wOutDims[0]-1) >= sizeOut)
			{
				IBUG_FATAL("Out-of-bounds bug.");
			}
#endif
			for(i=0; i<wOutDims[0]; i++)
			{
				//
				//  Assuming the number of components is small, so a loop is faster than memcpy
				//
				ptrIn = wArray.PtrIn + ncomp*(mResampleRate*i+loffIn);
				ptrOut = wArray.PtrOut + ncomp*(i+loffOut);
				for(m=0; m<ncomp; m++)
				{
					ptrOut[m] = ptrIn[m];
				};

#ifdef I_CHECK
				l++;
#endif
			}
		}
	}

#ifdef I_CHECK
	if(l != wArray.ArrOut->GetNumberOfTuples())
	{
		IBUG_FATAL("Out-of-bounds bug.");
	}
#endif
}


