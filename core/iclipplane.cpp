/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iclipplane.h"


#include "iactor.h"
#include "ierror.h"
#include "iposition.h"
#include "itransform.h"
#include "iviewmodule.h"

#include <vtkCellArray.h>
#include <vtkMath.h>
#include <vtkMatrix4x4.h>
#include <vtkPlane.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkProperty.h>

//
//  Templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


using namespace iType;


class iActor;
class iClipPlanes;
class iTransform;


class iClipPlaneInstance : public vtkPlane, public iViewModuleComponent
{

	friend class iClipPlane;

public:

	vtkTypeMacro(iClipPlaneInstance,vtkPlane);

	void SetDirection(const iVector3D &dir);
	inline const iVector3D& GetDirection() const { return mDirection; }

	void SetPosition(const iPosition &pos);
	inline const iPosition& GetPosition() const { return mPosition; }

	void SetGlassPlaneVisible(bool s);
	bool GetGlassPlaneVisible() const;

protected:

	virtual ~iClipPlaneInstance();

private:

	iClipPlaneInstance(iViewModule *vm);

	//
	//  Actors displayed by this class
	//
	iActor *mActor;
	iTransform *mTransform;
	iVector3D mDirection;
	iPosition mPosition;
};


//
//  Actor class
//
iClipPlaneInstance::iClipPlaneInstance(iViewModule *vm) : iViewModuleComponent(vm), mPosition(vm)
{
	int i, j;
	//
	// Set plane parameters
	//	
	vtkPolyData *pd = vtkPolyData::New(); IERROR_CHECK_MEMORY(pd);
	mActor = iActor::New(); IERROR_CHECK_MEMORY(mActor);
	
	static float x1[8][3] = { {1.0,0.0,0.0}, {0.7071,0.7071,0.0}, {0.0,1.0,0.0}, {-0.7071,0.7071,0.0}, 
	{-1.0,0.0,0.0}, {-0.7071,-0.7071,0.0}, {0.0,-1.0,0.0}, {0.7071,-0.7071,0.0} };
	static vtkIdType pts1[8] = { 0, 1, 2, 3, 4, 5, 6, 7 };
	
	for(i=0; i<8; i++) for(j=0; j<3; j++) x1[i][j] *= 2.5;
	
	vtkPoints *points = vtkPoints::New(VTK_FLOAT); IERROR_CHECK_MEMORY(points);
	for(i=0; i<8; i++) points->InsertPoint(i,x1[i]);
	pd->SetPoints(points);
	points->Delete();
	
	vtkCellArray *polys = vtkCellArray::New(); IERROR_CHECK_MEMORY(polys);
	polys->InsertNextCell(8,pts1);
	pd->SetPolys(polys);
	polys->Delete();
	
	mActor->SetInputData(pd);	
	pd->Delete();

	mActor->VisibilityOff();
	mActor->PickableOff();
	
	mActor->GetProperty()->SetOpacity(0.5);
	mActor->GetProperty()->SetAmbient(1.0);
	mActor->GetProperty()->SetDiffuse(1.0);
	mActor->GetProperty()->SetSpecular(0.7);
	mActor->GetProperty()->SetSpecularPower(50.0);
	mActor->GetProperty()->SetColor(0.6,0.7,0.7);

	mTransform = iTransform::New(); IERROR_CHECK_MEMORY(mTransform);
	mActor->SetUserTransform(mTransform);

	this->GetViewModule()->AddObject(mActor);

	iPosition pos(this->GetViewModule());
	this->SetPosition(pos);
	this->SetDirection(iVector3D(0.0,0.0,1.0));
}


iClipPlaneInstance::~iClipPlaneInstance()
{
	this->GetViewModule()->RemoveObject(mActor);
	mActor->Delete();
	mTransform->Delete();
}


void iClipPlaneInstance::SetGlassPlaneVisible(bool s)
{
	mActor->SetVisibility(s?1:0);
}


bool iClipPlaneInstance::GetGlassPlaneVisible() const
{
	return mActor->GetVisibility() != 0;
}


void iClipPlaneInstance::SetDirection(const iVector3D& dir)
{
	mDirection = dir;
	vtkMath::Normalize(mDirection);

	int i;
	double n[3];
	for(i=0; i<3; i++) n[i] = -mDirection[i];
	this->SetNormal(n);
	//
	//  Move the glass plane
	//
	mTransform->Identity();
	mTransform->SetDirection(mDirection);
	mTransform->Translate(mPosition);
}


void iClipPlaneInstance::SetPosition(const iPosition &pos)
{
	mPosition = pos;

	this->SetOrigin(mPosition);
	//
	//  Move the glass plane
	//
	mTransform->Identity();
	mTransform->SetDirection(mDirection);
	mTransform->Translate(mPosition);
}


//
//  Main class
//
iClipPlane* iClipPlane::New(iViewModule *vm)
{
	static iString LongName("ClipPlane");
	static iString ShortName("cp");

	IASSERT(vm);
	return new iClipPlane(vm,LongName,ShortName);
}


iClipPlane::iClipPlane(iViewModule *vm, const iString &fname, const iString &sname) : iViewModuleTool(vm,fname,sname),
	Position(iObject::Self(),static_cast<pv_vector::SetterType>(&iClipPlane::SetPosition),static_cast<pv_vector::GetterType>(&iClipPlane::GetPosition),"Position","x",static_cast<pv_vector::SizeType>(&iClipPlane::GetNumber)),
	Direction(iObject::Self(),static_cast<pv_vector::SetterType>(&iClipPlane::SetDirection),static_cast<pv_vector::GetterType>(&iClipPlane::GetDirection),"Direction","d",static_cast<pv_vector::SizeType>(&iClipPlane::GetNumber)),
	Number(iObject::Self(),static_cast<ps_int::SetterType>(&iClipPlane::SetNumber),static_cast<ps_int::GetterType>(&iClipPlane::GetNumber),"Number","num",9),
	Remove(iObject::Self(),static_cast<iPropertyAction1<Int>::CallerType>(&iClipPlane::CallRemove),"Delete","dlt"),
	Create(iObject::Self(),static_cast<iPropertyAction::CallerType>(&iClipPlane::CallCreate),"New","new")
{
	Remove.AddFlag(iProperty::_FunctionsAsIndex);

	mPlanes = vtkPlaneCollection::New(); IERROR_CHECK_MEMORY(mPlanes);
}


iClipPlane::~iClipPlane()
{
	mPlanes->RemoveAllItems();
	mPlanes->Delete();
	while(mInstances.Size() > 0) mInstances.RemoveLast()->Delete();
}


int iClipPlane::GetNumber() const
{
	return mInstances.Size();
}


bool iClipPlane::SetNumber(int n)
{
	if(n >= 0)
	{
		while(mInstances.Size() > n)
		{
			if(!this->CallRemove(mInstances.MaxIndex())) return false;
		}
		while(mInstances.Size() < n)
		{
			if(!this->CallCreate()) return false;
		}
		return true;
	}
	else return false;
}


bool iClipPlane::CallCreate()
{
	if(mInstances.Size() >= 6) return false;

	iClipPlaneInstance *a = new iClipPlaneInstance(this->GetViewModule());
	if(a != 0)
	{
		mInstances.Add(a);
		mPlanes->AddItem(a);
		return true;
	}
	else return false;
}


bool iClipPlane::CallRemove(int i)
{
	if(i>=0 && i<mInstances.Size())
	{
		mPlanes->RemoveItem(mInstances[i]);
		mInstances[i]->Delete();
		mInstances.Remove(i);
		return true;
	}
	else return false;
}


bool iClipPlane::ShowBody(bool s)
{
	int i;
	for(i=0; i<mInstances.Size(); i++)
	{
		mInstances[i]->SetGlassPlaneVisible(s); 
	}
	return true;
}


bool iClipPlane::SetPosition(int i, const iVector3D& p)
{
	if(i>=0  && i<mInstances.Size())
	{
		iPosition pos(this->GetViewModule());
		pos.SetBoxValue(p);
		mInstances[i]->SetPosition(pos);
		return true;
	}
	else return false;
}


const iVector3D& iClipPlane::GetPosition(int i) const
{
	static const iVector3D none;

	if(i>=0  && i<mInstances.Size())
	{
		return mInstances[i]->GetPosition().BoxValue();
	}
	else return none;
}


bool iClipPlane::SetDirection(int i, const iVector3D& d)
{
	if(i>=0  && i<mInstances.Size())
	{
		mInstances[i]->SetDirection(d);
		return true;
	}
	else return false;
}


const iVector3D& iClipPlane::GetDirection(int i) const
{
	static const iVector3D none;

	if(i>=0  && i<mInstances.Size())
	{
		return mInstances[i]->GetDirection();
	}
	else return none;
}
