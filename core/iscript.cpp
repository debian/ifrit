/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iscript.h"


//
//  Actual base class
//
iScript::iScript(const iString& name, iShell *s) : iShellComponent(s), mName(name)
{
	mIOHelper = 0;
	mObserver = 0;
}


iScript::~iScript()
{
}


void iScript::Delete()
{
	this->InstallIOHelper(0);
	this->InstallObserver(0);

	delete this;
}


void iScript::InstallObserver(iScriptObserver *obs)
{
	if(mObserver != obs)
	{
		if(mObserver != 0) delete mObserver;

		mObserver = obs;
		if(mObserver != 0) mObserver->mScript = this;
		this->OnInstallObserver();
	}
}


void iScript::InstallIOHelper(iScriptIOHelper *ioh)
{
	if(mIOHelper != ioh)
	{
		if(mIOHelper != 0) delete mIOHelper;

		mIOHelper = ioh;
		this->OnInstallIOHelper();
	}
}


void iScript::OnInstallObserver()
{
}


void iScript::OnInstallIOHelper()
{
}



//
//  Observer class
//
iScriptObserver::iScriptObserver()
{
	mScript = 0;
	mIsEnabled = true;
}


iScriptObserver::~iScriptObserver()
{
}


void iScriptObserver::Enable(bool s)
{
	mIsEnabled = s;
}


bool iScriptObserver::IsTerminated()
{
	if(mIsEnabled && mScript!=0) return this->IsTerminatedBody(); else return false;
}


void iScriptObserver::OnStart()
{
	if(mIsEnabled && mScript!=0) this->OnStartBody();
}


void iScriptObserver::OnStop(bool aborted)
{
	if(mIsEnabled && mScript!=0) this->OnStopBody(aborted);
}


void iScriptObserver::OnBeginLine(int line, const iString &file)
{
	if(mIsEnabled && mScript!=0) this->OnBeginLineBody(line,file);
}


void iScriptObserver::OnEndLine(int line, const iString &file)
{
	if(mIsEnabled && mScript!=0) this->OnEndLineBody(line,file);
}


/*
void iScriptObserver::OnFunctionCall(int line, const iString &file, const iString &fun)
{
	if(mIsEnabled && mScript!=0) this->OnFunctionCallBody(line,file,fun);
}


void iScriptObserver::OnVariableChange(int line, const iString &file, const iString &var)
{
	if(mIsEnabled && mScript!=0) this->OnVariableChangeBody(line,file,var);
}
*/

