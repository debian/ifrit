/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  EventObservers used by ViewModule and its components
//

#ifndef IVIEWEVENTOBSERVERS_H
#define IVIEWEVENTOBSERVERS_H


#include "ieventobserver.h"
#include "iviewmodulecomponent.h"


class iString;
class iViewModule;

class vtkTimerLog;


class iRenderEventObserver : public iEventObserver, public iViewModuleComponent
{
	
public:
	
	vtkTypeMacro(iRenderEventObserver,iEventObserver);
	static iRenderEventObserver* New(iViewModule *vm = 0);

	inline bool IsCancelled() const { return mCancelled; }
	inline float GetRenderTime() const { return mRenderTime; }

	void SetInteractive(bool s){ mInteractive = s; }
	bool IsInteractive() const { return mInteractive; }

	static void BlockRenderEventObservers(bool s) { mTheseAreBlocked = s; }
	//static bool RenderEventObserversBlocked(){ return mTheseAreBlocked; }

	virtual void PostFinished() = 0; // can be manually driven

protected:

	iRenderEventObserver(iViewModule *vm);
	~iRenderEventObserver();
	
	vtkTimerLog* Timer() const { return mRenderTimer; }

	virtual void ExecuteBody(vtkObject *caller, unsigned long event, void *data);

	virtual void OnStart() = 0;
	virtual void OnFinish() = 0;
	virtual bool CheckAbort() = 0;

private:

	vtkTimerLog *mRenderTimer;
	float mRenderTime;
	static bool mTheseAreBlocked;
	bool mCancelled, mInProgress, mInteractive;
	long mMasterThreadId;
};


class iPickEventObserver : public iEventObserver, public iViewModuleComponent
{
	
public:
	
	vtkTypeMacro(iPickEventObserver,iEventObserver);
	static iPickEventObserver* New(iViewModule *vm = 0);

protected:

	iPickEventObserver(iViewModule *vm);

	virtual void ExecuteBody(vtkObject *caller, unsigned long event, void *data);
	virtual void OnStart() = 0;
	virtual void OnFinish() = 0;

};

#endif // IVIEWEVENTOBSERVERS_H

