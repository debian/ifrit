/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  This class provides all functionality for writing images and animations
//
#ifndef IWRITER_H
#define IWRITER_H


#include "iobject.h"
#include "iviewmodulecomponent.h"


#include "istring.h"

#include <vtkSetGet.h>

class iGenericMultiImageMovieWriter;
class iStereoImageArray;

class vtkImageWriter;

namespace iParameter
{
	namespace ImageFormat
	{
		const int PNG =		0;
		const int JPG =		1;
		const int PNM =		2;
		const int BMP =		3;
		const int TIF =		4;
		const int EPS =		5;

		inline bool IsValid(int m){ return m>=0 && m<=5; }
	};

	namespace MovieOutput
	{
		const int Image =	0;
		const int MPEG2 =	1;
		const int AVI   =	2;

		inline bool IsValid(int m){ return m>=0 && m<=2; }
	};
};


class iWriter : public iObject, public iViewModuleComponent
{
	
public:

	vtkTypeMacro(iWriter,iObject);
	static iWriter* New(iViewModule *vm = 0);

	iObjectPropertyMacro1S(ImageFormat,Int);
	iObjectPropertyMacro1S(MovieOutput,Int);
	iObjectPropertyMacro1S(PostScriptPaperFormat,Int);
	iObjectPropertyMacro1S(PostScriptOrientation,Int);

	iObjectPropertyMacro2S(ImageRootName,String);
	iObjectPropertyMacro2S(MovieRootName,String);
	iString GetImageFileName();
	iString GetMovieFileName();

	iObjectPropertyMacroA(Write);

	//
	//  Writing functionality
	//
	void WriteImageFrame(const iStereoImageArray& images);
	void StartMovie();
	void WriteMovieFrame(const iStereoImageArray& images);
	void FinishMovie();

protected:
	
	virtual ~iWriter();

private:

	iWriter(iViewModule *vm, const iString &fname, const iString &sname);

	//
	//  VTK classes
	//
	iGenericMultiImageMovieWriter *mMovieWriter;  // for stereo movies
	vtkImageWriter *mImageWriter;

	static int mImageIndex, mMovieIndex;
	static iString mImageRootName, mMovieRootName;
};

#endif // IWRITER_H

