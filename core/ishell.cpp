/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ishell.h"


#include "idatareader.h"
#include "idatareaderobserver.h"
#include "idirectory.h"
#include "ierror.h"
#include "iexception.h"
#include "ifile.h"
#include "iimagecomposer.h"
#include "ioutputchannel.h"
#include "ipalettecollection.h"
#include "iparallel.h"
#include "iparallelmanager.h"
#include "irunner.h"
#include "ishelleventobservers.h"
#include "isystem.h"
#include "iviewmodule.h"
#include "iviewmodulecollection.h"
#include "ivolumeviewsubject.h"

#include <vtkCamera.h>
#include <vtkCriticalSection.h>
#include <vtkDataObject.h>
#include <vtkMapper.h>
#include <vtkMultiThreader.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkTimerLog.h>

//
//  Templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


using namespace iParameter;


const iString iShell::mShortOptionPrefix = "-";
const iString iShell::mLongOptionPrefix = "--";


class iInteractorEventObserver : public iEventObserver, public iShellComponent
{

public:
	
	vtkTypeMacro(iInteractorEventObserver,iEventObserver);
	static iInteractorEventObserver* New(iShell *s = 0)
	{
		IASSERT(s);
		return new iInteractorEventObserver(s);
	}

	void Activate(bool s)
	{
		mActive = s;
	}

protected:

	iInteractorEventObserver(iShell *s) : iEventObserver(), iShellComponent(s)
	{
		mInUpdate = mActive = false;
	}

	virtual void ExecuteBody(vtkObject *caller, unsigned long event1, void *callData)
	{
		if(!mActive || mInUpdate) return;

		vtkRenderWindowInteractor *iren, *driver = vtkRenderWindowInteractor::SafeDownCast(caller);
		if(this->GetShell()!=0 && driver!=0)
		{
			int i;
			mInUpdate = true;
			for(i=0; i<this->GetShell()->GetNumberOfViewModules(); i++) if((iren=this->GetShell()->GetViewModule(i)->GetRenderWindowInteractor())!=driver && iren!=0)
			{
				iren->SetControlKey(driver->GetControlKey());
				iren->SetShiftKey(driver->GetShiftKey());
				iren->SetEventPosition(driver->GetEventPosition());
				iren->SetKeySym(driver->GetKeySym());
				iren->InvokeEvent(event1,callData);
			}
			mInUpdate = false;
		}
	}

	bool mInUpdate, mActive;
};


namespace iShell_Private
{
	iString LongName("ifrit");
	iString ShortName("i");
};



//
//  Main class
//
iShell::iShell(iRunner *runner, bool threaded, const iString &type, int argc, const char **argv) : iObject(0,iShell_Private::LongName,iShell_Private::ShortName), mType(type),
	iObjectConstructPropertyMacroS(Bool,iShell,AutoRender,ar),
	iObjectConstructPropertyMacroS(Int,iShell,OptimizationMode,om),
	iObjectConstructPropertyMacroS(Int,iShell,NumberOfProcessors,np),
	iObjectConstructPropertyMacroS(Bool,iShell,SynchronizeInteractors,si),
	iObjectConstructPropertyMacroSOP(threaded,Bool,iShell,Modal,m),
	iObjectConstructPropertyMacroGenericA1(String,iShell,RestoreState,restore,RestoreStateFromFile),
	iObjectConstructPropertyMacroGenericA1(String,iShell,SaveState,save,SaveStateToFile),
	iObjectConstructPropertyMacroA1(Int,iShell,SynchronizeCameras,sc)
{
	mRenderMode = RenderMode::NoRender;
	mCanBeDeleted = true;

	Modal.AddFlag(iProperty::_OptionalInState);

	try
	{
		mRunner = runner;  IASSERT(mRunner);
		mExitObserver = 0;
		mOutputChannel = 0;

		//
		//  Members for threaded running
		//
		mIsThreaded = threaded;
		mMutex = vtkCriticalSection::New(); IERROR_CHECK_MEMORY(mMutex);

		mModal = mLastPushResult = true;

		mIsRunning = mIsStopped = mBlockAutoRender = mInPush = mIsFailed = false;
		mInitSteps = -1;
		mNumProcs = 0;
		mProgCur = -1;
		mProgMax = 1;

		//
		//  Set environment variables
		//  Is the base-dir environment variable set?
		//
		char *e = getenv("IFRIT_DIR");
		if(e != 0)
		{
			mEnvironment[Environment::Base] = iString(e);
			if(mEnvironment[Environment::Base].IsEmpty()) mEnvironment[Environment::Base] = iDirectory::Current();
			if(!mEnvironment[Environment::Base].EndsWith(iDirectory::Separator())) mEnvironment[Environment::Base] += iDirectory::Separator();
		}
		else
		{
			//
			//  Get the home directory, if it exists
			//
			e = getenv("APPDATA"); // windows?
			if(e != 0)
			{
				mEnvironment[Environment::Base] = iString(e) + iDirectory::Separator() + "IfrIT" + iDirectory::Separator();
			}
			else // unix?
			{
				e = getenv("HOME");
				mEnvironment[Environment::Base] = iString(e) + iDirectory::Separator() + ".ifrit" + iDirectory::Separator();
			}
			//
			//  Is it readable?
			//
			iDirectory dir;
			if(!dir.Open(mEnvironment[Environment::Base]))
			{
				//
				//  try to create
				//
				if(!dir.Make(mEnvironment[Environment::Base]))
				{
					mEnvironment[Environment::Base] = iDirectory::Current();
				}
			}
		}

		//    
		//  Read other environment here    
		//    
		e = getenv("IFRIT_DATA_DIR");               if(e != 0) mEnvironment[Environment::Data] = iString(e) + iDirectory::Separator(); else mEnvironment[Environment::Data] = iDirectory::Current();
		e = getenv("IFRIT_IMAGE_DIR");              if(e != 0) mEnvironment[Environment::Image] = iString(e) + iDirectory::Separator(); else mEnvironment[Environment::Image] = iDirectory::Current();
		e = getenv("IFRIT_SCRIPT_DIR");             if(e != 0) mEnvironment[Environment::Script] = iString(e) + iDirectory::Separator(); else mEnvironment[Environment::Script] = mEnvironment[Environment::Base];
		e = getenv("IFRIT_PALETTE_DIR");            if(e != 0) mEnvironment[Environment::Palette] = iString(e) + iDirectory::Separator(); else mEnvironment[Environment::Palette] = mEnvironment[Environment::Base];

		iDirectory::ExpandFileName(mEnvironment[Environment::Base]);
		iDirectory::ExpandFileName(mEnvironment[Environment::Data]);
		iDirectory::ExpandFileName(mEnvironment[Environment::Image]);
		iDirectory::ExpandFileName(mEnvironment[Environment::Script]);
		iDirectory::ExpandFileName(mEnvironment[Environment::Palette]);

		//
		//  Define some basic command-line options
		//
		this->AddCommandLineOption("i","init-file","load the state from a file with name <arg>",true);
		this->AddCommandLineOption("np","num-procs","set the number of processors to use to <arg>",true);
		this->AddCommandLineOption("GPU","force-GPU","try to use GPU even if VTK does not find it",false);
		this->AddCommandLineOption("no-GPU","force-no-GPU","do not use GPU even if it is present on the system (useful for running IFrIT remotely)",false);

		mAutoRender = true;
		mBlockAutoRender = true; // block render requests while objects are being created
		mSynchronizeInteractors = false;
		//
		//  Start with all members NULL
		//
		mParallelManager = 0;
		mParallelObserver = 0;
		mPalettes = 0;
		mViewModules = 0;

		int i;
		for(i=0; i<argc; i++)
		{
			mArgV.Add(argv[i]);
		}
	}
	catch(iException::GlobalExit)
	{
		mIsFailed = true;
	}
}


iShell::~iShell()
{
	if(mExitObserver!= 0) delete mExitObserver;
	mMutex->Delete();
}


bool iShell::PrintHelp()
{
	//
	//  On --help option print help and return before we created anything
	//
	if(mArgV.Size()>0 && (mArgV[0]=="-h" || mArgV[0]=="-help" || mArgV[0]=="--help"))
	{
		int j;
		iString sl, sh, s = "Format: ifrit [shell] [option] ... [data_dir_name]\n";
		s += "Options:\n";
		for(j=0; j<mOptions.Size(); j++)
		{
			s += mOptions[j].ShortName + " / " + mOptions[j].LongName;
			if(mOptions[j].NeedsValue)
			{
				s += "=<arg>\n";
			}
			else
			{
				s += "\n";
			}
			s += "    " + mOptions[j].Help + "\n";
		}
		this->OutputText(MessageType::Information,s);
		return true;
	}
	else return false;
}


bool iShell::Initialize()
{
	try
	{
		if(!this->StartConstructor()) return false;

		mOutputChannel = this->CreateOutputChannel(); IASSERT(mOutputChannel);
		iOutput::AddChannel(mOutputChannel);

		mWorkTimer = vtkTimerLog::New(); IERROR_CHECK_MEMORY(mWorkTimer);
		mWorkTimer->StartTimer();

		mInitSteps = 0;

		if(!this->ParseCommandLine()) return false;

		this->OnInitStart();

		//
		//  Parallel performance - this must be first, since many components are parallel workers as well
		//
		mParallelManager = iParallelManager::New(this); IERROR_CHECK_MEMORY(mParallelManager);
		if(mNumProcs > 0) mParallelManager->SetNumberOfProcessors(mNumProcs);
		mParallelObserver = iRequiredCast<iParallelUpdateEventObserver>(INFO,this->CreateShellEventObserver(ShellObserver::ParallelUpdate)); IERROR_CHECK_MEMORY(mParallelObserver);
		mParallelManager->AddObserver(iParallel::InformationEvent,mParallelObserver);

		mPalettes = iPaletteCollection::New(this); IERROR_CHECK_MEMORY(mPalettes);

		//
		//  Create observer for synchronizing interactors
		//
		mInteractorObserver = iInteractorEventObserver::New(this); IERROR_CHECK_MEMORY(mInteractorObserver);

		//
		//  Execution observer for monitoring executon and diplaying work progress.
		//
		mExecutionObserver = iRequiredCast<iExecutionEventObserver>(INFO,this->CreateShellEventObserver(ShellObserver::Execution)); IERROR_CHECK_MEMORY(mExecutionObserver);

		//
		//  DataReader observer for displying progress while loading data 
		//
		mDataReaderObserver = this->CreateDataReaderObserver(); IERROR_CHECK_MEMORY(mDataReaderObserver);

		//
		//  Composer must be created before view modules
		//
		mComposer = iImageComposer::New(this); IERROR_CHECK_MEMORY(mComposer);

		//
		//  ViewModule family must be created after ControlModule is fully created
		//
		mViewModules = iViewModuleCollection::New(this); IERROR_CHECK_MEMORY(mViewModules);

		//
		//  Finish construction of a child shell
		//
		if(!this->ConstructorBody()) return false;

		//
		//  Set the init file name
		//
		if(mStateFileName.IsEmpty())
		{
			iString fname = this->GetEnvironment(Environment::Base) + "ifrit.ini";
			if(iFile::IsReadable(fname)) mStateFileName = fname;
		}

		//
		//  Register all objects and their properties with the help factory
		//
		this->RegisterWithHelpFactory();
#ifdef I_DEBUG
		//iPaletteCollection::WritePaletteImages(this,"Work/docs/images");
		//iHelpFactory::CreateUserGuide();
		//exit(0);
#endif

		//
		//  Load the state file if needed
		//
		if(!mStateFileName.IsEmpty())
		{
			this->OnInitState();
			mLoadedState = this->RestoreStateFromFile(mStateFileName);
		}
		else mLoadedState = false;

		this->OnInitFinish();

		mBlockAutoRender = false;
		mCanBeDeleted = false;

		return true;
	}
	catch(iException::GlobalExit)
	{
		mIsFailed = true;
		if(mExitObserver != 0) mExitObserver->OnExit(this);
		return false;
	}
}


void iShell::Delete()
{
	//
	//  Don't delete yet, wait for Finalize to complete - who knows where it is wandering...
	//
	int n = 0;
	while(!mCanBeDeleted && n<100)
	{
		iSystem::Sleep(100);
		n++;
	}

	if(n == 100)
	{
		IBUG_FATAL("Shell is unable to finish.");
	}
	else
	{
		//
		//  Remove the output channel - after the next call it may be invalid
		//
		if(mOutputChannel != 0)
		{
			iOutput::RemoveChannel(mOutputChannel);
			mOutputChannel->Delete();
			mOutputChannel = 0;
		}

		this->iObject::Delete();
	}
}


bool iShell::StartConstructor()
{
	return true;
}


void iShell::FinishDestructor()
{
}


void iShell::Start()
{
	try
	{
		mIsRunning = true;
		mIsStopped = false;

		this->StartBody();
	}
	catch(iException::GlobalExit)
	{
		mIsFailed = true;
		mIsStopped = true;
		mInPush = false;
		mPushRequestQueue.Clear();
	}

	//
	//  Clean up, we are done
	//
	try
	{
		mIsRunning = false;
		mBlockAutoRender = true;

		//
		//  Delete the shell components
		//
		this->DestructorBody();

		//
		//  We are just about to start deleting objects. First thing we do is to block all
		//  observers since we do not know at each moment in the deleting process which objects
		//  are still present and which have been already deleted.
		//
		iEventObserver::BlockAllEventObservers(true);

		//
		//  Delete components
		//
		mViewModules->Delete();

		mComposer->Delete();

		delete mDataReaderObserver;
		mExecutionObserver->Delete();
		mInteractorObserver->Delete();

		mPalettes->Delete();

		mParallelObserver->Delete();
		mParallelManager->Delete();

		mWorkTimer->Delete();

		this->FinishDestructor();

		mCanBeDeleted = true;
	}
	catch(iException::GlobalExit)
	{
		mIsFailed = true;
		mIsRunning = false;
		mBlockAutoRender = true;
		mCanBeDeleted = true;
	}

	if(mExitObserver != 0) mExitObserver->OnExit(this);
}


void iShell::Stop()
{
	try
	{
		this->StopBody();
	}
	catch(iException::GlobalExit)
	{
		mIsFailed = true;
		mInPush = false;
		mPushRequestQueue.Clear();
	}

	mIsStopped = true;
}


void iShell::OutputText(int type, const iString &text) const
{
	if(mOutputChannel != 0)
	{
		mOutputChannel->Display(type,text);
	}
	else
	{
		iOutput::Display(type,text);
	}
}


void iShell::PassRenderRequest(int mode)
{
	switch(mode)
	{
	case RenderMode::NoRender:
		{
			break;
		}
	case RenderMode::All:
		{
			//
			//  Render all
			//
			int j;
			for(j=0; j<mViewModules->Size(); j++) this->RequestRender(mViewModules->GetViewModule(j));
			break;
		}
	default:
		{
			IBUG_FATAL("Invalid RenderMode");
			break;
		}
	}
}


bool iShell::IsImmediatePost() const
{
	return !this->IsThreaded();
}


void iShell::RequestRender(iViewModule* vm, bool force)
{
	IASSERT(vm);

	if(force || this->IsAutoRendering())
	{
		vm->ActualRender();
	}
}


float iShell::GetMemorySize(int mod) const
{
	float s = 0.0;
	if(mod < 0)
	{
		int i;
		for(i=0; i<mViewModules->Size(); i++) s += mViewModules->GetViewModule(i)->GetMemorySize();
	}
	else if(mod>=0 && mod<mViewModules->Size())
	{
		mViewModules->GetViewModule(mod)->GetMemorySize();
	}
	return s;
}


void iShell::SetExitObserver(iShellExitObserver *obs)
{
	if(mExitObserver != 0) delete mExitObserver;
	mExitObserver = obs;
}


//
//  Saving and restoring the state
//
bool iShell::SaveStateToFile(const iString& str)
{
	iString fname(str);

	if(fname.IsEmpty())
	{
		fname = this->GetEnvironment(Environment::Base) + "ifrit.ini";
	}

	if(fname[0] == '+')
	{
		fname.Replace(0,1,this->GetEnvironment(Environment::Base));
	}

	iFile F(fname);
	if(!F.Open(iFile::_Write,iFile::_Text))
	{
		this->OutputText(MessageType::Error,"Unable to open text file "+fname+" for writing.");
		return false;
	}
	
	//
	//  Save the state of this shell and all its children
	//
	iString s;
	this->SaveStateToString(s);

	if(!F.WriteLine(s))
	{
		F.Close();
		return false;
	}
	F.Close();	

	return true;
}


bool iShell::RestoreStateFromFile(const iString& str)
{
	iString fname(str);

	if(fname.IsEmpty())
	{
		fname = this->GetEnvironment(Environment::Base) + "ifrit.ini";
	}

	if(fname[0] == '+')
	{
		fname.Replace(0,1,this->GetEnvironment(Environment::Base));
	}

	iFile F(fname);
	if(!F.Open(iFile::_Read,iFile::_Text))
	{
		this->OutputText(MessageType::Error,"Unable to open text file "+fname+" for reading.");
		return false;
	}

	iObjectCollection::AutoCreate = true;
	mBlockAutoRender = true;

	iString line, s;
	mProgMax = 1;
	while(F.ReadLine(line)) 
	{
		if(line.Contains(" = ") > 0) mProgMax++;
		s += line + "\n";
	}
	F.Close();
	
	mProgCur = 0;
	bool ret = this->LoadStateFromString(s);
	this->OnLoadStateAtomBody(-1);
	mProgCur = -1;

	mBlockAutoRender = false;
	iObjectCollection::AutoCreate = false;

	if(ret)
	{
		int i;
		for(i=0; i<mViewModules->Size(); i++) mViewModules->GetViewModule(i)->RequestRender();
	}
	else
	{
		this->OutputText(MessageType::Error,"The state file <"+fname+"> is corrupted or incomplete.\nSome properties may not have been set."); 
	}
	return ret;
}


void iShell::OnLoadStateAtom()
{
	if(mProgCur >= 0)
	{
		this->OnLoadStateAtomBody((100*mProgCur++)/mProgMax);
	}
}


iEventObserver* iShell::GetInteractorObserver() const
{
	return mInteractorObserver;
}


int iShell::GetNumberOfViewModules() const
{
	return mViewModules->Size();
}


iViewModule* iShell::GetViewModule(int mod) const
{
	if(mod>=0 && mod<mViewModules->Size())
	{
		return mViewModules->GetViewModule(mod);
	}
	else
	{
		IBUG_FATAL("Invalid VM index");
		return 0;
	}
}


//
//  We add/remove observers so that in the non-sync mode no extra callbacks take place.
//
bool iShell::SetAutoRender(bool s)
{
	mAutoRender = s;
	return true;
}


bool iShell::SetSynchronizeInteractors(bool s)
{
	mSynchronizeInteractors = s;
	mInteractorObserver->Activate(s);
	return true;
}


bool iShell::SetOptimizationMode(int m)
{
	iDataConsumer::SetGlobalOptimizationMode(m);
	vtkMapper::SetGlobalImmediateModeRendering((m==DataConsumerOptimizationMode::OptimizeForMemory)?1:0);
	vtkDataObject::SetGlobalReleaseDataFlag((m==DataConsumerOptimizationMode::OptimizeForMemory)?1:0);

	//
	//  We need to reset everything
	//
	int j;
	for(j=0; j<mViewModules->Size(); j++) mViewModules->GetViewModule(j)->GetReader()->ResetPipeline();

	return true;
}


int iShell::GetOptimizationMode() const
{
	return iDataConsumer::GetGlobalOptimizationMode();
}


bool iShell::SetNumberOfProcessors(int n)
{
	if(n > mParallelManager->GetMaxNumberOfProcessors()) n = mParallelManager->GetMaxNumberOfProcessors();
	if(n>=mParallelManager->GetMinNumberOfProcessors() && n<=mParallelManager->GetMaxNumberOfProcessors())
	{
		mParallelManager->SetNumberOfProcessors(n);
	}
	else this->OutputText(MessageType::Warning,"Unable to set the number of processors.");

	//
	//  We need always to return true, for the state files to work on any machine
	//
	return true;
}


int iShell::GetNumberOfProcessors() const
{
	return mParallelManager->GetNumberOfProcessors();
}


bool iShell::CallSynchronizeCameras(int v)
{
	if (v >= 0 && v < mViewModules->Size())
	{
		iViewModule *module = mViewModules->GetViewModule(v); IERROR_CHECK_MEMORY(module);

		int i;
		vtkCamera *c = module->GetRenderer()->GetActiveCamera();
		double *fp = c->GetFocalPoint();
		double *cp = c->GetPosition();
		double *vu = c->GetViewUp();
		double ps = c->GetParallelScale();
		for (i = 0; i < mViewModules->Size(); i++)
		{
			c = mViewModules->GetViewModule(i)->GetRenderer()->GetActiveCamera();
			c->SetFocalPoint(fp);
			c->SetPosition(cp);
			c->SetViewUp(vu);
			c->SetParallelScale(ps);
			mViewModules->GetViewModule(i)->RequestRender();
		}
		return true;
	}
	else return false;
}


//
//  Environment querying
//
const iString& iShell::GetEnvironment(Environment::Type type) const
{
	static const iString none;

	if(type>=0 && type<Environment::SIZE)
	{
		return mEnvironment[type];
	}
	else
	{
		return none;
	}
}


void iShell::OnInitAtom()
{
#ifndef I_NO_CHECK
	if(mInitSteps < 0)
	{
		IBUG_FATAL("Bug in IFrIT initilization process.");
	}
#endif
	mInitSteps++;
	mWorkTimer->StopTimer();
	if(mWorkTimer->GetElapsedTime() > 0.1)
	{
		mWorkTimer->StartTimer();
		this->OnInitAtomBody(true);
	}
	else
	{
		this->OnInitAtomBody(false);
	}
}


void iShell::OnInitAtomBody(bool)
{
}


void iShell::OnLoadStateAtomBody(int prog)
{
}


void iShell::AddCommandLineOption(const iString &sname, const iString& lname, const iString &help, bool value)
{
	Option tmp;
	tmp.ShortName = mShortOptionPrefix + sname;
	tmp.LongName = mLongOptionPrefix + lname;
	tmp.Help = help;
	tmp.NeedsValue = value;
	mOptions.Add(tmp);
}


bool iShell::ParseCommandLine()
{
	//
	//  Parse command-line options
	//
	int i = 0;
	while(i < mArgV.Size())
	{
		int j;
		for(j=0; j<mOptions.Size(); j++)
		{
			if(mArgV[i]==mOptions[j].ShortName || mArgV[i]==mOptions[j].LongName)
			{
				if(mOptions[j].NeedsValue)
				{
					mArgV.Remove(i);
					if(i < mArgV.Size())
					{
						if(!this->OnCommandLineOption(mOptions[j].ShortName.Part(mShortOptionPrefix.Length()),mArgV[i])) return false;
					}
					else
					{
						this->OutputText(MessageType::Error,"Option "+mOptions[j].ShortName+" / "+mOptions[j].LongName+" requires an argument.");
						return false;
					}
				}
				else
				{
					if(!this->OnCommandLineOption(mOptions[j].ShortName.Part(mShortOptionPrefix.Length()),"")) return false;
				}
				break;
			}
			if(mArgV[i].BeginsWith(mOptions[j].ShortName+"=") || mArgV[i].BeginsWith(mOptions[j].LongName+"="))
			{
				if(mOptions[j].NeedsValue)
				{
					int k = mArgV[i].Find('=');
					if(k > -1)
					{
						if(!this->OnCommandLineOption(mOptions[j].ShortName.Part(mShortOptionPrefix.Length()),mArgV[i].Part(k+1))) return false;
						break;
					}
				}
				this->OutputText(MessageType::Error,"Option "+mOptions[j].ShortName+" / "+mOptions[j].LongName+" requires an argument.");
				return false;
			}
		}

		if(j < mOptions.Size())
		{
			mArgV.Remove(i);
		}
		else
		{
			if(i < mArgV.Size()) i++;
		}
	}

	//
	//  Non-option arguments
	//
	if(!this->OnCommandLineArguments(mArgV)) return false;

	//
	//  Not an option or known argument
	//
	if(mArgV.Size() == 1)
	{
		//
		//  This must be the name of the default data directory
		//
		iDirectory d;
		if(d.Open(mArgV[0]))
		{
			d.Close();
			mEnvironment[Environment::Data] = mArgV[0];
		}
		else
		{
			this->OutputText(MessageType::Error,"Unable to open directory "+iString(mArgV[0])+".");
			return false;
		}

		mArgV.Remove(0);
	}

	//
	//  Unrecognized arguments
	//
	if(mArgV.Size() > 0)
	{
		this->OutputText(MessageType::Error,"Command-line argument "+iString(mArgV[0])+" is not supported.");
		return false;
	}

	return true;
}


bool iShell::OnCommandLineOption(const iString &sname, const iString& str)
{
	if(sname == "np")
	{
		bool ok;
		int n = str.ToInt(ok);
		if(ok && n>0 && n<16385) mNumProcs = n;
		return true;
	}

	if(sname == "i")
	{
		iString fn(str);
		iDirectory::ExpandFileName(fn,this->GetEnvironment(Environment::Base));
		if(iFile::IsReadable(fn))
		{
			mStateFileName = fn;
			return true;
		}
		else
		{
			this->OutputText(MessageType::Error,"File "+fn+" is not accessible.\nIFrIT will now exit.");
			return false;
		}
	}

	if(sname == "GPU")
	{
		iVolumeViewSubject::SetGPUMode(1);
		return true;
	}

	if(sname == "no-GPU")
	{
		iVolumeViewSubject::SetGPUMode(-1);
		return true;
	}

	IBUG_ERROR("Comman-line option "+sname+" has been declared but not implemeneted.");
	return false;
}


bool iShell::OnCommandLineArguments(iArray<iString> &argv)
{
	return true;
}


//
//  This is a part of message subsystem for thread-safe execution.
//  Functions begining with underscore can be called from the driver thread.
//  Functions ending with underscore should be called from the slave thread.
//
bool iShell::_RequestPush(iProperty::Key key)
{
	//
	//  Wait until the previous push is completed
	//
	while(mInPush)
	{
		iSystem::Sleep(1);
	}

	mMutex->Lock();
	mPushRequestQueue.Add(key);
	mMutex->Unlock();

	//
	//  If post is modal, we wait for an asnwer
	//
	if(mModal)
	{
		while(this->HasPostedRequests())  // on failure mModal is set to false
		{
			iSystem::Sleep(1);
		}
		return mLastPushResult;
	}
	else return true;
}


void iShell::PushAllRequests_()
{
	if(mInPush) return;
	mInPush = true;

	mMutex->Lock();

	int i;
	const iProperty *prop;
	for(i=0; i<mPushRequestQueue.Size(); i++)
	{
		prop = iProperty::FindPropertyByKey(mPushRequestQueue[i]);
		if(prop != 0)
		{
			mLastPushResult = prop->PushPostedValue_();
			if(!mLastPushResult && !mModal)
			{
				//
				//  Set some kind of error, since none has been returned with the non-modal post
				//
				this->OutputText(MessageType::Error,"Calling object property <"+prop->Owner()->GetFullName()+"."+prop->LongName()+"> did not success.");
			}
		}
	}

	mPushRequestQueue.Clear();

	mMutex->Unlock();

	mInPush = false;
}


bool iShell::HasPostedRequests() const
{
	return (mPushRequestQueue.Size() > 0);
}


bool iShell::SetModal(bool s)
{
	mModal = s;
	return true;
}


void iShell::OnInitStart()
{
	this->OutputText(MessageType::Information,"Initializing...");
}


void iShell::OnInitState()
{
	this->OutputText(MessageType::Information,"Loading the state file...");
}


void iShell::OnInitFinish()
{
	this->OutputText(MessageType::Information,"Initialization complete.");
}
