/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Class for outputing textual information. It is static, so it is available globally.
//  It uses a stack of iOutputChannel(s) to handle the output. Shells can add or remove
//  channels to redirect output where they like.
//
#ifndef IOUTPUT_H
#define IOUTPUT_H


class iOutputChannel;
class iString;


namespace iParameter
{
	namespace MessageType
	{
		const int Error = 0;
		const int Warning = 1;
		const int Information = 2;
#ifdef I_DEBUG
		const int DebugMessage = 3;
#endif
	};
};


class iOutput
{

public:

	static void Display(int type, const iString &text);

	static void ReportBug(const char* text, const char* file, int line, int severity);
	static void ReportBug(const iString& text, const char* file, int line, int severity);

	static void LogError(const iString &text);

	static void AddChannel(iOutputChannel *channel);
	static void RemoveChannel(iOutputChannel *channel);

#ifdef I_DEBUG
	static void DisplayDebugMessage(const iString& text);
#endif
};

#endif  // IOUTPUT_H
