/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IBOUNDINGBOX_H
#define IBOUNDINGBOX_H


#include "iviewmoduletool.h"


#include "istring.h"

class iActor;
class iActorCollection;
class iCubeAxesActor;


namespace iParameter
{
	namespace BoundingBoxType
	{
		const int Default =		0;
		const int Classic =		1;
		const int HairThin =	2;
		const int Axes =		3;

		inline bool IsValid(int m){ return m>=0 && m<=3; }
	};
};


class iBoundingBox : public iViewModuleTool
{
	
public:

	vtkTypeMacro(iBoundingBox,iViewModuleTool);
	static iBoundingBox* New(iViewModule *vm = 0);
		
	iObjectPropertyMacro1S(Type,Int);
	iObjectPropertyMacro2V(AxesRanges,Float);
	iObjectPropertyMacro1V(AxesLabels,String,3);

	void SetMagnification(int m);
	void SetColor(const iColor &c);

protected:

	virtual ~iBoundingBox();

private:
	
	iBoundingBox(iViewModule *vm, const iString &fname, const iString &sname);

	virtual bool ShowBody(bool s);

	//
	//  Actors displayed by this class
	//
	iActor *mBox0Actor, *mBox2Actor;
	iActorCollection *mBox1Actor, *mBox3Actor;
	iCubeAxesActor *mBox3AxesLabels;
};

#endif // IBOUNDINGBOX_H

