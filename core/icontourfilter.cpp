/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "icontourfilter.h"


#include "idataconsumer.h"
#include "iviewmodule.h"

#include <vtkContourFilter.h>
#include <vtkFloatArray.h>
#include <vtkPointData.h>
#include <vtkPolyDataNormals.h>
#include <vtkToolkits.h>

#include <vtkMarchingContourFilter.h>

//
//  Templates
//
#include "igenericfilter.tlh"


iContourFilter::iContourFilter(iDataConsumer *consumer) : iGenericFilter<vtkDataSetAlgorithm,vtkImageData,vtkPolyData>(consumer,1,true)
{
	mMethod = 0;
	mCurVar = 0;

	vtkContourFilter *w0 = vtkContourFilter::New(); IERROR_CHECK_MEMORY(w0);
	w0->UseScalarTreeOn();
	w0->ComputeNormalsOn();
	w0->ComputeScalarsOff();
	w0->ComputeGradientsOff();
	w0->AddObserver(vtkCommand::ProgressEvent,consumer->GetViewModule()->GetRenderEventObserver());

	vtkMarchingContourFilter *w1 = vtkMarchingContourFilter::New(); IERROR_CHECK_MEMORY(w1);
	w1->UseScalarTreeOn();
	w1->ComputeNormalsOn();
	w1->ComputeScalarsOff();
	w1->ComputeGradientsOff();
	w1->AddObserver(vtkCommand::ProgressEvent,consumer->GetViewModule()->GetRenderEventObserver());

	mWorkers[0] = w0;
	mWorkers[1] = w1;
}


iContourFilter::~iContourFilter()
{
	mWorkers[0]->Delete();
	mWorkers[1]->Delete();
}


void iContourFilter::SetMethod(int n)
{ 
	if(n>=0 && n<2 && n!=mMethod)
	{
		mWorkers[mMethod]->GetOutput()->Initialize();  // erase old data
		mMethod = n;
		this->Modified();
	}
}


void iContourFilter::SetCurrentVar(int n)
{ 
	if(n>=0 && n!=mCurVar)
	{
		mCurVar = n;
		this->Modified();
	}
}


float iContourFilter::GetLevel() const
{
	return iRequiredCast<vtkContourFilter>(INFO,mWorkers[0])->GetValue(0);
}


void iContourFilter::SetLevel(float v)
{
	iRequiredCast<vtkContourFilter>(INFO,mWorkers[0])->SetValue(0,v);
	iRequiredCast<vtkMarchingContourFilter>(INFO,mWorkers[1])->SetValue(0,v);
	this->Modified();
}


void iContourFilter::ProvideOutput()
{
	vtkImageData *input = this->InputData();
	
	int numComp;
	if(this->InputData()->GetPointData()==0 || this->InputData()->GetPointData()->GetScalars()==0 || (numComp=this->InputData()->GetPointData()->GetScalars()->GetNumberOfComponents())<1 || mCurVar<0 || mCurVar>=numComp)
	{
		this->OutputData()->Initialize();
		return;
    }

	//
	//  VTK contouring filters always make the isosurface of the first component. We simple shift the data by mCurVar values
	//  so that the current value is always the 0 component.
	//
	vtkImageData *shiftedInput = input->NewInstance(); IERROR_CHECK_MEMORY(shiftedInput);
	shiftedInput->ShallowCopy(input);

	vtkFloatArray *scalars = iRequiredCast<vtkFloatArray>(INFO,input->GetPointData()->GetScalars());
	vtkFloatArray *shiftedScalars = vtkFloatArray::New(); IERROR_CHECK_MEMORY(shiftedScalars);
	shiftedScalars->SetNumberOfComponents(numComp);
	shiftedScalars->SetArray(scalars->GetPointer(mCurVar),scalars->GetSize(),1);
	shiftedInput->GetPointData()->SetScalars(shiftedScalars);
	shiftedScalars->Delete();

	mWorkers[mMethod]->SetInputData(shiftedInput);
	mWorkers[mMethod]->Update();
	shiftedInput->Delete();

	//
	//  Are there normals? Not all combinations of inputs/filters/VTK versions create those
	//
	if(mWorkers[mMethod]->GetOutput()->GetPointData()->GetNormals()==0 || mWorkers[mMethod]->GetOutput()->GetPointData()->GetNormals()->GetNumberOfTuples()==0)
	{
		vtkPolyDataNormals *nf = vtkPolyDataNormals::New(); IERROR_CHECK_MEMORY(nf);
		nf->ConsistencyOn();
		nf->ComputePointNormalsOn(); 
		nf->ComputeCellNormalsOff();
		nf->SetInputConnection(mWorkers[mMethod]->GetOutputPort());
		nf->Update();
		mWorkers[mMethod]->GetOutput()->GetPointData()->SetNormals(nf->GetOutput()->GetPointData()->GetNormals());
		nf->Delete();
	}

	this->OutputData()->ShallowCopy(mWorkers[mMethod]->GetOutput());
}

