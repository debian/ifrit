/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "idatasubject.h"


#include "ibuffer.h"
#include "idata.h"
#include "idataconsumer.h"
#include "idataobject.h"
#include "idatareader.h"
#include "ierror.h"
#include "ifileloader.h"
#include "ihistogrammaker.h"
#include "iprobefilter.h"
#include "ishell.h"
#include "iviewmodule.h"

//
//  Templates
//
#include "iarray.tlh"
#include "ibuffer.tlh"
#include "iproperty.tlh"


using namespace iParameter;


#define iDataSubjectPropertyConstructMacro(_fname_,_sname_) \
	_fname_(iDataSubject::Self(),&iDataLimits::Set##_fname_,&iDataLimits::Get##_fname_,#_fname_,#_sname_)


iDataSubject::iDataSubject(iFileLoader *fl, const iDataType& type) : iObject(fl->GetViewModule()->GetDataObject(),type.LongName(),type.ShortName()), iViewModuleComponent(fl->GetViewModule()),
	DataPresent(iObject::Self(),static_cast<iPropertyQuery<iType::Bool>::GetterType>(&iDataSubject::IsThereData),"DataPresent","dp"),
	iObjectConstructPropertyMacroQ(String,iDataSubject,FileName,fn),
	iObjectConstructPropertyMacroS(Bool,iDataSubject,ResetOnLoad,rol),
	iDataSubjectPropertyConstructMacro(Range,r),
	iDataSubjectPropertyConstructMacro(Name,n),
	iDataSubjectPropertyConstructMacro(Stretch,s),
	iObjectConstructPropertyMacroA1(String,iDataSubject,Load,l),
	iObjectConstructPropertyMacroA(iDataSubject,Erase,e),
	mId(fl->NumStreams())
{
	mRenderMode = RenderMode::Clones;

	IASSERT(fl);
	mLoader = fl;

	//
	//  Add self as a new stream
	//
	iFileLoader::Stream *s = fl->CreateNewStream(); IERROR_CHECK_MEMORY(s);
	s->Subject = this; 
	fl->mStreams.Add(s);

	mLimits = 0;
	mHistogramMaker = 0;
	mResetOnLoad = true;
}


iDataSubject::~iDataSubject()
{
	if(mHistogramMaker != 0) mHistogramMaker->Delete();
	if(mLimits != 0) mLimits->Delete();
}


bool iDataSubject::CallLoad(const iString& name)
{
	return this->GetLoader()->GetReader()->CallLoad(iDataId(this->GetDataType()),name);
}


bool iDataSubject::CallErase()
{
	return this->GetLoader()->GetReader()->CallErase(iDataId(this->GetDataType()));
}


iDataLimits* iDataSubject::CreateLimits() const
{
	return iDataLimits::New(this);
}


iHistogramMaker* iDataSubject::CreateHistogramMaker() const
{
	return iHistogramMaker::New(this);
}


bool iDataSubject::IsThereData() const
{
	return mLoader->IsThereData(mId);
}


bool iDataSubject::IsThereScalarData() const
{
	return mLoader->IsThereScalarData(mId);
}


bool iDataSubject::IsThereVectorData() const
{
	return mLoader->IsThereVectorData(mId);
}


bool iDataSubject::IsThereTensorData() const
{
	return mLoader->IsThereTensorData(mId);
}


bool iDataSubject::IsDirectionPeriodic(int d) const
{
	if(d>=0 && d<3 && mLoader->IsThereData(mId))
	{
		return (mLoader->GetBoundaryConditions()==BoundaryConditions::Periodic && mLoader->GetStream(mId)->Periodic[d]);
	}
	else return false;
}


const iString& iDataSubject::GetFileName() const
{
	return mLoader->GetLastFileName();
}


vtkDataSet* iDataSubject::GetData() const
{
	return mLoader->GetData(mId);
}


iDataLimits* iDataSubject::GetLimits() const
{
	if(mLimits == 0)
	{
		mLimits = this->CreateLimits(); IERROR_CHECK_MEMORY(mLimits);
	}
	return mLimits;
}


iHistogramMaker* iDataSubject::GetHistogramMaker() const
{
	if(mHistogramMaker == 0)
	{
		mHistogramMaker = this->CreateHistogramMaker(); IERROR_CHECK_MEMORY(mHistogramMaker);
	}
	return mHistogramMaker;
}


bool iDataSubject::SetResetOnLoad(bool s)
{
	mResetOnLoad = s;
	return true;
}


iProbeFilter* iDataSubject::CreateProbeFilter(iDataConsumer *consumer) const
{
	return iProbeFilter::New(consumer);
}


void iDataSubject::SyncWithLimits(int var, int mode) const
{
	iSyncWithLimitsRequest req(var,mode,this->GetDataType());

	this->GetViewModule()->SyncWithData(req);
}


void iDataSubject::RequestDataReload() const
{
	this->GetLoader()->GetReader()->RequestDataReload(this->GetDataType());
}


const iString iDataSubject::HelpTag() const
{
	return "~" + this->ShortName();
}


void iDataSubject::SavePropertiesToString(iString &str) const
{
	this->GetLimits()->AddAllDeclared(true);
	this->iObject::SavePropertiesToString(str);
	this->GetLimits()->AddAllDeclared(false);
}


unsigned int iDataSubject::GetDataRank() const
{
	return this->GetDataType().GetRank();
}
