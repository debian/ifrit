/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


//
//  Single-type data consumer
//
#ifndef IDATACONSUMER_H
#define IDATACONSUMER_H


#include "iviewmodulecomponent.h"


#include "iarray.h"
#include "istring.h"

class iDataConsumer;
class iDataHandler;
class iDataInfo;
class iDataLimits;
class iDataSubject;
class iDataType;
class iViewModule;

class vtkDataSet;


namespace iParameter
{
	namespace DataConsumerOptimizationMode
	{
		//
		// DataConsumer parameters
		//
		const int ResetToGlobal = -1;
		const int OptimizeForSpeed = 0;
		const int OptimizeForMemory = 1;
		const int OptimizeForQuality = 2;

		inline bool IsValid(int m){ return m>=0 && m<=2; }
	};
};


//
//  Helper class for providing synchronization requests
//
class iSyncRequest
{

public:

	virtual ~iSyncRequest();

	inline const iDataType& DataType() const { return mDataType; }

	virtual void Sync(iDataConsumer *consumer) const = 0;

protected:

	iSyncRequest(const iDataType& type);

private:

	const iDataType &mDataType;
};


class iSyncWithDataRequest : public iSyncRequest
{

public:

	iSyncWithDataRequest(const iDataType& type);

	virtual void Sync(iDataConsumer *consumer) const;
};


class iSyncWithLimitsRequest : public iSyncRequest
{

public:

	iSyncWithLimitsRequest(int var, int mode, const iDataType& type);

	virtual void Sync(iDataConsumer *consumer) const;

private:

	const int Var, Mode;
};


class iDataConsumer : public iViewModuleComponent
{

	friend class iDataHandler;
	friend class iSyncWithDataRequest;
	friend class iSyncWithLimitsRequest;
	//friend class iViewModule;

public:

	static void SetGlobalOptimizationMode(int mode);
	static int GetGlobalOptimizationMode(){ return mModeGlobal; }

	void SetOptimizationMode(int mode);
	int GetOptimizationMode() const;

	inline bool IsOptimizedForSpeed() const { return this->GetOptimizationMode() == iParameter::DataConsumerOptimizationMode::OptimizeForSpeed; }
	inline bool IsOptimizedForMemory() const { return this->GetOptimizationMode() == iParameter::DataConsumerOptimizationMode::OptimizeForMemory; }
	inline bool IsOptimizedForQuality() const { return this->GetOptimizationMode() == iParameter::DataConsumerOptimizationMode::OptimizeForQuality; }

	virtual float GetMemorySize();
	virtual void RemoveInternalData();

	inline const iDataType& GetDataType() const { return mDataType; }
	virtual bool IsThereData() const;

	void AddSecondaryDataType(const iDataType &type);

	inline const iDataInfo& GetPrimaryDataInfo() const { return *mPrimaryDataInfo; }
	inline const iDataInfo& GetSecondaryDataInfo() const { return *mSecondaryDataInfo; }

	iDataLimits* GetLimits() const;
	iDataSubject* GetSubject() const;
	virtual bool IsUsingData(const iDataType &type, bool onlyprimary) const;

	bool SyncWithLimits(int var, int mode);

protected:

    iDataConsumer(iViewModule *vm, const iDataType &type);
    virtual ~iDataConsumer();

	void RegisterDataHandler(iDataHandler *c);
	void UnRegisterDataHandler(iDataHandler *c);

	virtual bool SyncWithLimitsBody(int var, int mode); // by default do nothing
	virtual bool SyncWithData() = 0;

private:

	static int mModeGlobal;
	int mModeLocal;
	bool mOverrideGlobal;
	iLookupArray<iDataHandler*> mDataHandlers;
	const iDataType& mDataType;
	iDataInfo *mPrimaryDataInfo, *mSecondaryDataInfo;
};


#define iDataConsumerTypeMacro(_type_,_parent_) \
	protected: \
	virtual bool SyncWithDataBody(); \
	virtual bool SyncWithData() \
	{ \
		if(!this->_type_::SyncWithDataBody()) return false; \
		return this->_parent_::SyncWithData(); \
	} \
	vtkTypeMacro(_type_,_parent_)

#define iDataConsumerTypeMacroPass(_type_,_parent_) \
	protected: \
	virtual bool SyncWithData() \
	{ \
		return this->_parent_::SyncWithData(); \
	} \
	vtkTypeMacro(_type_,_parent_)

#define iDataConsumerTypeMacroBase(_type_,_parent_) \
	protected: \
	virtual bool SyncWithDataBody(); \
	virtual bool SyncWithData() \
	{ \
		return this->_type_::SyncWithDataBody(); \
	} \
	vtkTypeMacro(_type_,_parent_)

#endif // IDATACONSUMER_H

