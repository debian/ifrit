/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ifile.h"

#include "ierror.h"

#include <vtkIOStream.h>

//
//  VTK guys do that - so, there must be a good reason
//
#ifdef read
#undef read
#endif

#ifdef write
#undef write
#endif

#ifdef close
#undef close
#endif


struct iFilePosition
{
#ifndef VTK_USE_ANSI_STDLIB
#ifdef _WIN32
	streampos Value;
#else
	istream::pos_type Value;
#endif
#else
	istream::pos_type Value;
#endif
};


iFile::iFile(const iString &n)
{
	mName = n;
	mStream = new fstream(); IERROR_CHECK_MEMORY(mStream);
	mMaxMarker = 0;
	mNumMarker = 0;
	mMarker = 0;
}

iFile::~iFile()
{
	delete mStream;
	if(mMarker != 0) delete mMarker;
}


void iFile::SetName(const iString &n)
{
	this->Close();
	mName = n;
}


bool iFile::Open(OpenMode mode, OpenType type)
{
	int om = 0; 

	if(mode & _Read)
	{
		om |= ios::in;
#ifndef VTK_USE_ANSI_STDLIB
		om |= ios::nocreate;
#endif
	}

	if(mode & _Write)
	{
		om |= ios::out;
#ifndef VTK_USE_ANSI_STDLIB
		om |= !ios::nocreate;
#endif
	}

	if(mode & _Append) 
	{
		om = ios::out | ios::app;  // append is exclusive
#ifndef VTK_USE_ANSI_STDLIB
		om |= !ios::nocreate;
#endif
	}

	if(type == _Binary)
	{
		om |= ios::binary;
	}

	mStream->clear();
	mStream->open(mName.ToCharPointer(),ios::openmode(om));

	return (mStream->fail()==0);
}


void iFile::Close()
{
	mStream->clear();
	mStream->close();
}


bool iFile::ReadLine(iString &s, int max)
{
	const int nbuf = 256;
	char buf[nbuf];
	int n, r = 0;
	bool cont;
	
	mStream->clear();
	s.Clear();
	if(max < 1) return true;

	do
	{
		n = nbuf;
		if(n > max-r+1) n = max - r + 1;
		mStream->getline(buf,n);
		if(mStream->gcount() < n-1) 
		{
			cont = false;
		}
		else
		{
			if(mStream->fail()!=0)
			{
				mStream->clear();
				cont = true;
			}
			else
			{
				cont = false;
			}
		}
		if(mStream->fail()!=0 || mStream->eof()!=0) return false;
		s += buf;
		r += n-1;
	}
	while(cont && r<max);

	return true;
}


bool iFile::ReadBlock(void *p, size_t length)
{
	mStream->clear();
	mStream->read((char *)p,length);
#ifdef I_DEBUG
	bool f = (mStream->fail() == 0);
	bool e = (mStream->eof() == 0);
#endif
	return (mStream->fail()==0 && mStream->eof()==0);
}


bool iFile::SkipBlock(size_t length)
{ 
	mStream->clear();
	mStream->seekg(length,ios::cur); 
	return (mStream->fail()==0 && mStream->eof()==0);
}


bool iFile::WriteLine(const iString &s)
{
	mStream->clear();
	*mStream << s.ToCharPointer() << endl;
	bool ret = (mStream->fail()==0 && mStream->eof()==0);
	mStream->flush();
	return ret;
}


bool iFile::Rewind()
{
	mStream->clear();
	mStream->seekg(0);
	return (mStream->fail()==0 && mStream->eof()==0);
}


bool iFile::IsReadable(const iString &name)
{
	iFile F(name);
	if(!F.Open(_Read,_Text)) return false;
	F.Close();
	return true;
}


bool iFile::IsWritable(const iString &name)
{
	iFile F(name);
	if(!F.Open(_ReadWrite,_Text)) return false;
	F.Close();
	return true;
}


int iFile::SetMarker()
{
	if(mNumMarker == mMaxMarker)
	{
		mMaxMarker += 100;
		iFilePosition *tmp = new iFilePosition[mMaxMarker]; IERROR_CHECK_MEMORY(tmp);
		if(mMarker != 0)
		{
			for(int i=0; i<mNumMarker; i++) tmp[i] = mMarker[i];
			delete [] mMarker;
		}
		mMarker = tmp;
	}
	mMarker[mNumMarker].Value = mStream->tellg();
	return mNumMarker++;
}


bool iFile::ReturnToMarker(int m, bool remove)
{
	if(m<0 || m>=mNumMarker) return false;
	mStream->clear();
	mStream->seekg(mMarker[m].Value);
	bool ret = (mStream->fail()==0 && mStream->eof()==0);
	if(remove)
	{
		for(int i=m; i<mNumMarker-1; i++)
		{
			mMarker[i] = mMarker[i+1];
		}
		mNumMarker--;
	}
	return ret;
}
