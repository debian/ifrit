/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IPAINTEDVIEWSUBJECT_H
#define IPAINTEDVIEWSUBJECT_H


#include "iactorviewsubject.h"


class iDataLimits;
class iPaintedViewSubject;


class iPaintedViewInstance : public iActorViewInstance
{

	friend class iPaintedViewSubject;
	
public:
	
	iDataConsumerTypeMacro(iPaintedViewInstance,iActorViewInstance);
		
	iObjectSlotMacro1SV(Palette,int);
	iObjectSlotMacro1SV(PaintVar,int);

protected:
	
	iPaintedViewInstance(iPaintedViewSubject *owner, int numactors, float maxopacity, int numpaintedactors, int priority, bool alwayspainted);
	virtual ~iPaintedViewInstance();

	iPaintedViewSubject* Owner() const;

	virtual void UpdatePainting();
	virtual void UpdateColorBars();

	virtual bool IsPainted() const;
	virtual iDataLimits* GetPaintingLimits() const;
	virtual bool SyncWithLimitsBody(int var, int mode);

	virtual bool IsConnectedToPaintingData() const = 0;

	int mPaintOffset;

private:

	const int mNumPaintedActors, mColorBarPriority;
	const bool mAlwaysPainted;
};


class iPaintedViewSubject : public iActorViewSubject
{
	
public:
	
	iDataConsumerTypeMacroPass(iPaintedViewSubject,iActorViewSubject);

	iType::vsp_int Palette;
	iType::vsp_int PaintVar;

	const iDataType& GetPaintingDataType() const { return mPaintingDataType; }

protected:
	
	iPaintedViewSubject(iObject *parent, const iString &fname, const iString &sname, iViewModule *vm, const iDataType &type, const iDataType &pdt, unsigned int flags, int minsize, int maxsize);
	virtual ~iPaintedViewSubject();

	const iDataType &mPaintingDataType;
};

#endif // IPAINTEDVIEWSUBJECT_H

