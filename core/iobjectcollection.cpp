/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iobjectcollection.h"


#include "ierror.h"
#include "ihelpfactory.h"
#include "istring.h"

//
//  Templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


//
//  Main class
//
iObjectCollection::iObjectCollection(iObject *parent, const iString &fname, const iString &sname) : iObject(parent,fname,sname)
{
}


iObject* iObjectCollection::GetMember(int i) const
{
	if(i>=0 && i<mChildren.Size()) return mChildren[i]; else return 0;
}


bool iObjectCollection::AutoCreate = false;


const iObject* iObjectCollection::FindByName(const iString &name) const
{
	if(name==this->ShortName() || name==this->LongName()) return this;

	//
	//  Require an index for a member.
	//
	bool ok;
	iString prefix = name.Section(".", 0, 0);
	if (prefix.BeginsWith(this->ShortName() + "[") || prefix.BeginsWith(this->LongName() + "["))
	{
		if (!prefix.EndsWith("]")) return 0; // incorrect index
		int i = prefix.Find('[');
		iString sidx = prefix.Part(i, prefix.Length() - 1 - i);
		int idx = sidx.ToInt(ok);
		if (ok && idx >= 0 && idx < this->Size())
		{
			//
			//  There is an index specifier
			//
			const iObject *obj = this->GetMember(idx); IASSERT(obj);
			if (name.Contains(".") > 0) obj = obj->FindByName(name.Section(".", 1));
			return obj;
		}
		else return 0; // incorrect index
	}
	else return 0;
}


void iObjectCollection::RegisterWithHelpFactory() const
{
	this->RegisterPropertiesWithHelpFactory();

	int i;
	for(i=0; i<mChildren.Size(); i++)
	{
		mChildren[i]->RegisterWithHelpFactory();
	}
}

