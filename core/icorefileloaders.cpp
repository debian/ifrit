/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "icorefileloaders.h"


#include "iboundingbox.h"
#include "idatasubject.h"
#include "ifile.h"
#include "imath.h"
#include "imonitor.h"
#include "iparallel.h"
#include "iparallelfft.h"
#include "iparallelmanager.h"
#include "iparallelworker.h"
#include "ishell.h"
#include "ishelleventobservers.h"
#include "iuniformgriddata.h"
#include "iviewmodule.h"

#include <vtkFloatArray.h>
#include <vtkInformation.h>
#include <vtkPointData.h>
#include <vtkPoints.h>

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"
#include "ibuffer.tlh"


using namespace iParameter;


namespace iCoreData
{
	//
	//  Helper class for parallel executions
	//
	class ScalarHelper : protected iParallelWorker
	{

	public:

		ScalarHelper(ScalarFileLoader *loader);

		/*= void OperateOnData1(int nvar, int dims[3], const iString &calculatorExpression, bool doVector, float ps, float dp); =*/
		bool OperateOnData2(int nvar, int dims[3], int ivar, float Rsmooth, float ps, float dp);

	protected:

		virtual int ExecuteStep(int step, iParallel::ProcessorInfo &p);

		/*= void ExecuteOperateOnData1(iParallel::ProcessorInfo &p); =*/
		void ExecuteOperateOnData2(iParallel::ProcessorInfo &p);
		void ExecuteOperateOnData3(iParallel::ProcessorInfo &p);

		ScalarFileLoader *mLoader;

		iString mCalculatorExpression;

		int mNumVars, mNumProcs, *mDims, mVar;

		float *mDnyq, mRsmooth;
		float mProgStart, mProgStep;

		bool mDoVector, mOverflow;
	};


	//
	//  Main classes
	//
	//  ********************************************************************
	//
	//  VectorFileLoader class
	//
	//  ********************************************************************
	//
	VectorFileLoader::VectorFileLoader(iDataReader *r) : iUniformGridFileLoader(r,100,false)
	{
	}


	void VectorFileLoader::AssignBinData(int ncoms, vtkIdType ntot, int com, float *d)
	{
		if(ncoms != 3)
		{
			IBUG_FATAL("Invalid number of components in VectorFileLoader");
		}

		vtkIdType l;
		for(l=0; l<ntot; l++) mBuffer[com+3*l] = d[l];
	}


	void VectorFileLoader::AssignTxtData(int ncoms, vtkIdType ind, float *f)
	{
		if(ncoms != 3)
		{
			IBUG_FATAL("Invalid number of components in VectorFileLoader");
		}

		int n;
		for(n=0; n<3; n++) mBuffer[n+3*ind] = f[n];
	}


	int VectorFileLoader::GetNumComponents(int nvars) const
	{
		return 3;
	}


	void VectorFileLoader::AttachBuffer(vtkImageData *data)
	{
		//
		//  Attach the actual data
		//
		vtkIdType size = (vtkIdType)mDataDims[0]*mDataDims[1]*mDataDims[2]*3;
		vtkFloatArray *array = vtkFloatArray::New(); IERROR_CHECK_MEMORY(array);
		array->SetNumberOfComponents(3);
		array->SetArray(mBuffer,size,1);
		array->SetName("Vectors");
		data->GetPointData()->SetVectors(array);
		array->Delete();
	}


	//
	//  ********************************************************************
	//
	//  TensorFileLoader class
	//
	//  ********************************************************************
	//
	TensorFileLoader::TensorFileLoader(iDataReader *r) : iUniformGridFileLoader(r,200,false)
	{
	}


	void TensorFileLoader::AssignBinData(int ncoms, vtkIdType ntot, int com, float *d)
	{
		if(ncoms != 9)
		{
			IBUG_FATAL("Invalid number of components in TensorFileLoader");
		}

		int noff1, noff2;
		vtkIdType l;

		switch(com)
		{
		case 0: { noff1 = 0; noff2 = -1; break; }
		case 1: { noff1 = 1; noff2 = 3; break; }
		case 2: { noff1 = 2; noff2 = 6; break; }
		case 3: { noff1 = 4; noff2 = -1; break; }
		case 4: { noff1 = 5; noff2 = 7; break; }
		case 5: { noff1 = 8; noff2 = -1; break; }
		default: 
			{
				noff1 = 0; noff2 = -1;
				IBUG_WARN("Bug detected.");
			}
		}

		for(l=0; l<ntot; l++) mBuffer[noff1+9*l] = d[l];
		if(noff2 > 0) for(l=0; l<ntot; l++) mBuffer[noff2+9*l] = d[l];
	}


	void TensorFileLoader::AssignTxtData(int ncoms, vtkIdType ind, float *f)
	{
		if(ncoms != 9)
		{
			IBUG_FATAL("Invalid number of components in TensorFileLoader");
		}

		//
		//  Fill in 3x3 tensor with 6 components
		//
		mBuffer[0+9*ind] = f[0];
		mBuffer[1+9*ind] = f[1];
		mBuffer[2+9*ind] = f[2];
		mBuffer[3+9*ind] = f[1];
		mBuffer[4+9*ind] = f[3];
		mBuffer[5+9*ind] = f[4];
		mBuffer[6+9*ind] = f[2];
		mBuffer[7+9*ind] = f[4];
		mBuffer[8+9*ind] = f[5];
	}


	int TensorFileLoader::GetNumComponents(int nvars) const
	{
		return 9;
	}


	void TensorFileLoader::AttachBuffer(vtkImageData *data)
	{
		//
		//  Attach the actual data
		//
		vtkIdType size = (vtkIdType)mDataDims[0]*mDataDims[1]*mDataDims[2]*9;
		vtkFloatArray *array = vtkFloatArray::New(); IERROR_CHECK_MEMORY(array);
		array->SetNumberOfComponents(9);
		array->SetArray(mBuffer,size,1);
		array->SetName("Tensors");
		data->GetPointData()->SetTensors(array);
		array->Delete();
	}


	//
	//  ********************************************************************
	//
	//  ScalarFileLoader class
	//
	//  ********************************************************************
	//
	ScalarFileLoader::ScalarFileLoader(iDataReader *r, VectorFileLoader *vs) : iUniformGridFileLoader(r,0,true)
	{
		mVectorFileLoader = vs;

		mCalculatorOutput = 0;
		//	mCalculatorExpression = "";

		mHelper2 = new ScalarHelper(this); IERROR_CHECK_MEMORY(mHelper2);
	}


	ScalarFileLoader::~ScalarFileLoader()
	{
		delete mHelper2;
	}


	void ScalarFileLoader::AssignBinData(int ncoms, vtkIdType ntot, int com, float *d)
	{
		vtkIdType l;
		for(l=0; l<ntot; l++) mBuffer[com+ncoms*l] = d[l];
	}


	void ScalarFileLoader::AssignTxtData(int ncoms, vtkIdType ind, float *f)
	{
		int n;
		for(n=0; n<ncoms; n++) mBuffer[n+ncoms*ind] = f[n];
	}


	void ScalarFileLoader::AttachBuffer(vtkImageData *data)
	{
		//
		//  Attach the actual data
		//
		vtkIdType size = (vtkIdType)mDataDims[0]*mDataDims[1]*mDataDims[2]*mNumVars;
		vtkFloatArray *array = vtkFloatArray::New(); IERROR_CHECK_MEMORY(array);
		array->SetNumberOfComponents(mNumVars);
		array->SetArray(mBuffer,size,1);
		array->SetName("Scalars");
		data->GetPointData()->SetScalars(array);
		array->Delete();
	}


	int ScalarFileLoader::GetNumComponents(int nvars) const
	{
		return nvars;
	}


	void ScalarFileLoader::SetCalculatorOutput(int n)
	{
		if(n>=0 && n<this->GetStream(0)->Subject->GetLimits()->GetNumVars()) 
		{
			mCalculatorOutput = n;
		}
	}


	void ScalarFileLoader::SetCalculatorExpression(const iString &s)
	{
		mCalculatorExpression = s;
	}

		
	bool ScalarFileLoader::Smooth(int var, float r)
	{
		if(r>=0.0 && var>=0 && var<this->GetStream(0)->Subject->GetLimits()->GetNumVars())
		{
			iMonitor h(this,"Computing",1.0);
			return mHelper2->OperateOnData2(this->GetStream(0)->Subject->GetLimits()->GetNumVars(),mFileDims,var,r,0.0,1.0);
		}
		else return false;
	}


	//
	//  ********************************************************************
	//
	//  ParticleFileLoader class
	//
	//  ********************************************************************
	//
	ParticleFileLoader::ParticleFileLoader(iDataReader *r) : iParticleFileLoader(r)
	{
		//mHelper2 = new ScalarHelper(this); IERROR_CHECK_MEMORY(mHelper2);
	}


	ParticleFileLoader::~ParticleFileLoader()
	{
		//delete mHelper2;
	}


	void ParticleFileLoader::LoadParticleFileBody(const iString &suffix, const iString &fname)
	{
		//
		//  is the suffix valid?
		//
		iMonitor h(this);

		if(suffix.Lower()!="txt" && suffix.Lower()!="bin")
		{
			h.PostError("Incorrect file suffix.");
			return;
		}

		bool err, isBin = (suffix.Lower() == "bin");

		//
		//  Open the file
		//
		iFile F(fname);
		if(!F.Open(iFile::_Read,isBin?iFile::_Binary:iFile::_Text))
		{
			h.PostError("File "+fname+" does not exist.");
			return;
		}

		//
		//  Read the header
		//
		int i, nvar;
		bool paf;
		vtkIdType ntot0;
		float ll[3], ur[3];
		iString buffer;
		if(isBin)
		{
			err = !this->ReadBinFileHeader(F,ntot0,ll,ur,paf,nvar);
		}
		else
		{
			err = !this->ReadTxtFileHeader(F,ntot0,ll,ur,paf,nvar,buffer);
		}
		if(ntot0 <= 0) err = true;

		if(err || h.IsStopped())
		{
			F.Close();
			h.PostError("Corrupted header.");
			return;
		}

		//
		//  Compute scale and offset
		//
		float offsetF[3], scaleF[3];
		double offsetD[3], scaleD[3];
		if(paf)
		{
			for(i=0; i<3; i++)
			{
				offsetF[i] = ll[i];
				scaleF[i] = 2.0/(ur[i]-ll[i]);
			}
		}
		else
		{
			for(i=0; i<3; i++)
			{
				offsetD[i] = ll[i];
				scaleD[i] = 2.0/(ur[i]-ll[i]);
			}
		}

		//
		//  Configure streams
		//
		this->ConfigureStreams(&ntot0,&nvar,paf);
		h.SetProgress(0.01);

		//
		//  Read actual data
		//
		if(isBin)
		{
			err = !this->ReadBinFileContents(F,paf,scaleF,offsetF,scaleD,offsetD);
		}
		else
		{
			err = !this->ReadTxtFileContents(F,paf,scaleF,offsetF,scaleD,offsetD,buffer);
		}

		if(err || h.IsAborted())
		{
			if(err) h.PostError("Corrupted data.");
			F.Close();
			return;
		}

		h.SetProgress(1.0);
		F.Close();
	}


	bool ParticleFileLoader::ReadBinFileHeader(iFile &F, vtkIdType &ntot0, float *ll, float *ur, bool &paf, int &nvar)
	{
		//
		//  Read the header
		//
		iMonitor h(this);

		if(this->DetectFortranFileStructure(F,sizeof(int)))
		{
			int ntot1;
			if(!this->ReadFortranRecord(F,Buffer(ntot1))) return false;
			ntot0 = ntot1;
		}
		else if(this->DetectFortranFileStructure(F,sizeof(vtkIdType)))
		{
			vtkIdType ntot1;
			if(!this->ReadFortranRecord(F,Buffer(ntot1))) return false;
			ntot0 = ntot1;
		}
		else return false;

		float bounds[6];
		if(!this->ReadFortranRecord(F,Buffer(bounds,6))) return false;

		int i;
		for(i=0; i<3; i++)
		{
			ll[i] = bounds[i];
			ur[i] = bounds[3+i];
			if(!(ll[i] < ur[i]))
			{
				h.PostError("Bounding box is corrupted.");
				return false;
			}
		}

		//
		//  Auto-detect whether points are float or double
		//
		vtkIdType nrec = sizeof(float)*ntot0;
		int mar = F.SetMarker();
		if(!this->SkipFortranRecord(F,nrec))  // X
		{
			//
			//  not float - try double
			//
			paf = false;
			nrec = sizeof(double)*ntot0;
			F.ReturnToMarker(mar);
			if(!this->SkipFortranRecord(F,nrec)) return false;  // X
			if(!this->SkipFortranRecord(F,nrec)) return false;  // Y
			if(!this->SkipFortranRecord(F,nrec)) return false;  // Z
		}
		else
		{
			if(!this->SkipFortranRecord(F,nrec)) return false;  // Y
			if(!this->SkipFortranRecord(F,nrec)) return false;  // Z
			paf = true;
		}
		//
		//  Measure the file size to find out the number of variables
		//
		nrec = sizeof(float)*ntot0;
		for(i=0; i<999 && this->SkipFortranRecord(F,nrec); i++);
		F.ReturnToMarker(mar,true);
		nvar = i;

		return true;
	}



	bool ParticleFileLoader::ReadTxtFileHeader(iFile &F, vtkIdType &ntot0, float *ll, float *ur, bool &paf, int &nvar, iString &buffer)
	{
		iString s;

		//
		//  TxT file coordinates are float - do we need to check whether they are long enough to be double?
		//
		paf = true;

		//
		// First line
		//
		if(!F.ReadLine(s)) return false;
		unsigned long ntotRead;
		int ret = sscanf(s.ToCharPointer(),"%ld",&ntotRead);
		if(ret != 1) return false;
		if(ntotRead <= 0) return false;
		ntot0 = ntotRead;

		//
		//  Second line
		//
		if(!F.ReadLine(s)) return false;

		char *axisName[3];
		int i;
		for(i=0; i<3; i++) 
		{
			axisName[i] = 0;
			axisName[i] = new char[8192]; if(axisName[i] == 0) break;
		}
		if(i < 3 )
		{
			for(i=0; i<3; i++) if(axisName[i] != 0) delete [] axisName[i];
			return false;
		}

		ret = sscanf(s.ToCharPointer(),"%g %g %g %g %g %g %s %s %s",&ll[0],&ll[1],&ll[2],&ur[0],&ur[1],&ur[2],axisName[0],axisName[1],axisName[2]);
		if(ret == 9)
		{
			this->GetViewModule()->GetBoundingBox()->SetAxesLabels(0,axisName[0]);
			this->GetViewModule()->GetBoundingBox()->SetAxesLabels(1,axisName[1]);
			this->GetViewModule()->GetBoundingBox()->SetAxesLabels(2,axisName[2]);
		}
		for(i=0; i<3; i++) if(axisName[i] != 0) delete [] axisName[i];

		if(ret!=6 && ret!=9) return false;

		//
		//  Find out the number of variables
		//
		if(!F.ReadLine(s)) return false;

		double xyz[3];
		float f[10];
		ret = sscanf(s.ToCharPointer(),"%lg %lg %lg %g %g %g %g %g %g %g %g %g %g",&xyz[0],&xyz[1],&xyz[2],&f[0],&f[1],&f[2],&f[3],&f[4],&f[5],&f[6],&f[7],&f[8],&f[9]);
		if(ret<3 || ret>12) return false;

		nvar = ret - 3;
		buffer = s;

		return true;
	}


	bool ParticleFileLoader::ReadBinFileContents(iFile &F, bool paf, float *scaleF, float *offsetF, double *scaleD, double *offsetD)
	{
		ParticleStream *s = this->GetStream(0);

		//
		//  parameters for the Progress Bar
		//
		iMonitor h(this);
		float updateStart, updateDuration = 0.99/(s->NumVariablesInFile+3);

		//
		//  Read coordinates
		//
		int n;
		bool err = false;
		for(n=0; !err && n<3; n++)
		{
			updateStart = 0.01 + updateDuration*n;
			if(paf)
			{
				err = !this->ReadPositions(F,1,n,updateStart,updateDuration,scaleF+n,offsetF+n);
			}
			else
			{
				err = !this->ReadPositions(F,1,n,updateStart,updateDuration,scaleD+n,offsetD+n);
			}
		}

		if(err || h.IsAborted())
		{
			if(err) h.PostError("Corrupted data.");
			return false;
		}


		//
		//  Read variables
		//
		for(n=0; !err && n<s->NumVariablesInFile; n++) 
		{
			updateStart = 0.01 + updateDuration*(n+3);
			err = !this->ReadVariables(F,1,n,updateStart,updateDuration);
		}

		if(err || h.IsAborted())
		{
			if(err) h.PostError("Corrupted data.");
			return false;
		}

		return true;
	}


	bool ParticleFileLoader::ReadTxtFileContents(iFile &F, bool paf, float *scaleF, float *offsetF, double *scaleD, double *offsetD, iString buffer)
	{
		ParticleStream *s = this->GetStream(0);

		//
		//  Use the buffer
		//
		double xyz[3], xyzD[3];
		float xyzF[3], f[10];

		int i, ret;
		vtkIdType l;
		vtkPoints *pts = s->Points();
		vtkFloatArray *vars = s->Scalars();

		iMonitor h(this);
		mIterator.Start();
		for(l=0; l<s->NumTotal; l++) 
		{
			if(l > 0)
			{
				if(!F.ReadLine(buffer))
				{
					h.PostError("Truncated file.");
					return false;
				}
			}
			ret = sscanf(buffer.ToCharPointer(),"%lg %lg %lg %g %g %g %g %g %g %g %g %g %g",&xyz[0],&xyz[1],&xyz[2],&f[0],&f[1],&f[2],&f[3],&f[4],&f[5],&f[6],&f[7],&f[8],&f[9]);

			if(ret < s->NumVariablesInFile+3)
			{
				h.PostError("Corrupted data.");
				return false;
			}
			if((100*l)/s->NumTotal < (100*(l+1))/s->NumTotal) 
			{
				h.SetProgress(0.01+0.99*double(l)/s->NumTotal);
				if(h.IsAborted())
				{
					return false;
				}
			}
			if(mIterator.IsSelected())
			{
				if(paf)
				{
					for(i=0; i<3; i++) xyzF[i] = -1.0 + scaleF[i]*(xyz[i]-offsetF[i]);
					pts->SetPoint(mIterator.GlobalIndex(),xyzF);
				}
				else
				{
					for(i=0; i<3; i++) xyzD[i] = -1.0 + scaleD[i]*(xyz[i]-offsetD[i]);
					pts->SetPoint(mIterator.GlobalIndex(),xyzD);
				}
				if(vars != 0) vars->SetTuple(mIterator.GlobalIndex(),f);
			}
		}
		mIterator.Stop();

		return true;
	}


	//
	//  Helper class
	//
	ScalarHelper::ScalarHelper(ScalarFileLoader *loader) : iParallelWorker(loader->GetViewModule()->GetParallelManager())
	{
		mLoader = loader; IASSERT(loader);
	}

	/*=
	void ScalarHelper::OperateOnData1(int nvar, int dims[3], const iString &calculatorExpression, bool doVector, float ps, float dp)
	{
		mNumVars = nvar;
		mDims = dims;
		mCalculatorExpression = calculatorExpression;
		mDoVector = doVector;

		mProgStart = ps;
		mProgStep = dp;

		this->ParallelExecute(1);
	}
	=*/

	bool ScalarHelper::OperateOnData2(int nvar, int dims[3], int var, float Rsmooth, float ps, float dp)
	{
		mNumVars = nvar;
		mDims = dims;
		mVar = var;
		mRsmooth = Rsmooth;

		mProgStart = ps + 0.4*dp;
		mProgStep = 0.2*dp;

		iMonitor h(mLoader);

		iParallelFFT *fft = iParallelFFT::New(this->GetManager());
		mDnyq = new float[2*dims[1]*dims[2]];
		if(fft!=0 && mDnyq!=0)
		{
			h.SetInterval("Computing",0.0,0.4);
			if(fft->Transform(true,var,nvar,dims[0],dims[1],dims[2],mLoader->GetDataPointer(),mDnyq) != 0)
			{
				delete [] mDnyq;
				h.PostError("Forward FFT failed for unknown reason.");
				return false;
			}
			h.SetInterval(0.4,0.2);
			this->ParallelExecute(2);
			h.SetInterval(0.6,0.4);
			if(fft->Transform(false,var,nvar,dims[0],dims[1],dims[2],mLoader->GetDataPointer(),mDnyq) != 0)
			{
				delete [] mDnyq;
				h.PostError("Inverse FFT failed for unknown reason.");
				return false;
			}

			mLoader->GetData(0)->Modified();

			delete [] mDnyq;
		}
		else
		{
			h.PostError("Unable to allocate temporary work storage.");
			if(fft != 0) fft->Delete();
			return false;
		}

		fft->Delete();

		return true;
	}


	int ScalarHelper::ExecuteStep(int step, iParallel::ProcessorInfo &p)
	{
		switch(step)
		{
		case 1:
			{
				if(mNumProcs != p.NumProcs) return 3;
				/*= this->ExecuteOperateOnData1(p); =*/
				return 0;
			}
		case 2:
			{
				if(mNumProcs != p.NumProcs) return 3;
				this->ExecuteOperateOnData2(p);
				return 0;
			}
		default: 
			{
#ifndef I_NO_CHECK
				IBUG_WARN("Internal check failed.");
#endif
				return 2;
			}
		}
	}

	/*=
	//
	//  Operate on mesh data
	//
	void ScalarHelper::ExecuteOperateOnData1(iParallel::ProcessorInfo &p)
	{
		float *arr1 = mFileLoader->GetDataPointer();
		float *arr2 = mFileLoader->mVectorFileLoader->GetDataPointer();

		if(arr1==0 || (mDoVector && arr2==0)) return;

		vtkIdType size = (vtkIdType)mDims[0]*mDims[1]*mDims[2];
		int nout = mFileLoader->GetCalculatorOutput();
		vtkIdType l, kbeg, kend, kstp;
		iParallel::SplitRange(p,size,kbeg,kend,kstp);

		float progScale = mProgStep/kstp;

		//
		// Create our calculator
		//
		iCalculator::Stack stack(0);
		iCalculator::calc_t calc;
		iCalculator::ExternalDataVariable<float,false,false> var(arr1+kbeg*mNumVars,"Var",mNumVars);
		iCalculator::ExternalDataVariable<float,false,false> vec(arr2==0?arr1:arr2+kbeg*3,"Vec",3);
		stack.Variables()->Add(&var);
		if(mDoVector) stack.Variables()->Add(&vec);

		const iCalculator::Executable *exe = calc.Compile(mCalculatorExpression,&stack);
		if(exe == 0) return;

		for(l=kbeg; l<kend; l++) 
		{
			if(l%1000 == 0)
			{
				if(this->IsMaster(p)) this->Observer()->SetProgress(mProgStart+progScale*(l-kbeg));
				if(this->Observer()->IsAborted()) return;
			}
			if(!calc.Execute(exe)) break;		
			const iCalculator::DataVariable<float> *res = dynamic_cast<const iCalculator::DataVariable<float>*>(exe->GetResult());
			if(res==0 || res->Data()==0) break;
			arr1[nout+mNumVars*l] = res->Data()[0];
			++var;
			++vec;
		}
	}
	=*/

	void ScalarHelper::ExecuteOperateOnData2(iParallel::ProcessorInfo &p)
	{
		const float pi = 3.1415926535;

		vtkIdType size = mDims[2];
		vtkIdType kbeg, kend, kstp;
		iParallel::SplitRange(p,size,kbeg,kend,kstp);

		//
		//  Format everything
		//
		float *arr = mLoader->GetDataPointer();
		if(arr == 0) return;

		arr += mVar;

		float progScale = mProgStep/kstp;

		int i, j, k;
		float wz, wy, wx, fac;
		vtkIdType l;
		for(k=kbeg; k<kend; k++)
		{
			if(k%10 == 0)
			{
				if(p.IsMaster()) mLoader->GetShell()->GetExecutionEventObserver()->SetProgress(mProgStart+progScale*(k-kbeg));
				if(mLoader->GetShell()->GetExecutionEventObserver()->IsAborted()) return;
			}

			wz = 2*pi/mDims[2]*k;
			if(wz > pi) wz -= 2*pi;

			for(j=0; j<mDims[1]; j++)
			{
				wy = 2*pi/mDims[1]*j;
				if(wy > pi) wy -= 2*pi;

				l = mDims[0]*(j+mDims[1]*k);

				for(i=0; i<mDims[0]/2; i++)
				{
					wx = 2*pi/mDims[0]*i;

					fac = exp(-sqrt(wx*wx+wy*wy+wz*wz)*mRsmooth);

					arr[mNumVars*(2*i+0+l)] *= fac;
					arr[mNumVars*(2*i+1+l)] *= fac;
				}

				//
				//  Nyquist
				//
				wx = pi;

				fac = exp(-sqrt(wx*wx+wy*wy+wz*wz)*mRsmooth);

				mDnyq[0+2*(j+mDims[1]*k)] *= fac;
				mDnyq[1+2*(j+mDims[1]*k)] *= fac;
			}
		}
	}
};


