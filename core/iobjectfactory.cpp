/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iobjectfactory.h"


//
//  This file is documented only partially.
//
#include "iabstractextension.h"
#include "icoredatasubjects.h"
#include "icrosssectionviewsubject.h"
#include "idatareader.h"
#include "idatalimits.h"
#include "iedition.h"
#include "iparticleviewsubject.h"
#include "ipicker.h"
#include "ipointer.h"
#include "isurfaceviewsubject.h"
#include "itensorfieldviewsubject.h"
#include "ivectorfieldviewsubject.h"
#include "iviewmodule.h"
#include "iviewobject.h"
#include "ivolumeviewsubject.h"


//
//  Templates
//
#include "iarray.tlh"


using namespace iParameter;


//
//  Helper class
//
class iAbstractExtensionPointer : public iPointer::Ordered<iAbstractExtension>
{

public:

	iAbstractExtensionPointer(iAbstractExtension *ptr = 0) : iPointer::Ordered<iAbstractExtension>(ptr){}
};


//
//  Main class
//
iArray<iAbstractExtensionPointer>& iObjectFactory::Extensions()
{
	static iOrderedArray<iAbstractExtensionPointer> ext;
	return ext;
}


void iObjectFactory::AttachExtension(iAbstractExtension *ext)
{
	if(ext != 0) Extensions().Add(ext);
}


//
// **************************************************************************
//
//  IMPORTANT: Factory methods do not need to be checked for null pointers
//             since they are only called from within New() functions
//
// **************************************************************************
//
iViewObject* iObjectFactory::CreateViewObject(iViewModule *parent, const iString& name)
{
	IASSERT(parent);

	int i, j;
	iViewObject *obj = 0;
	iViewSubject *sub;
	iArray<iAbstractExtensionPointer> &ext(Extensions());

	//
	//  View objects
	//
	if(obj == 0)
	{
		static iString LongName = "CrossSection";
		static iString ShortName = "x";

		if(name==ShortName || name==LongName)
		{
			obj = iViewObject::New(parent,LongName,ShortName); IERROR_CHECK_MEMORY(obj);
			obj->AddSubject(iCrossSectionViewSubject::New(obj,iCoreData::ScalarDataSubject::DataType()));
			for(i=0; i<ext.Size(); i++)
			{
				j = 0;
				while((sub = ext[i]->CreateCrossSectionSubject(obj,j++)) != 0) obj->AddSubject(sub);
			}
		}
	}

	if(obj == 0)
	{
		static iString LongName = "Particles";
		static iString ShortName = "p";

		if(name==ShortName || name==LongName)
		{
			obj = iViewObject::New(parent,LongName,ShortName);
			if(obj == 0) return 0;
			obj->AddSubject(iParticleViewSubject::New(obj,iCoreData::ParticleDataSubject::DataType()));
			for(i=0; i<ext.Size(); i++)
			{
				j = 0;
				while((sub = ext[i]->CreateParticleSubject(obj,j++)) != 0) obj->AddSubject(sub);
			}
		}
	}

	if(obj == 0)
	{
		static iString LongName = "Surface";
		static iString ShortName = "s";

		if(name==ShortName || name==LongName)
		{
			obj = iViewObject::New(parent,LongName,ShortName);
			if(obj == 0) return 0;
			obj->AddSubject(iSurfaceViewSubject::New(obj,iCoreData::ScalarDataSubject::DataType()));
			for(i=0; i<ext.Size(); i++)
			{
				j = 0;
				while((sub = ext[i]->CreateSurfaceSubject(obj,j++)) != 0) obj->AddSubject(sub);
			}
		}
	}

	if(obj == 0)
	{
		static iString LongName = "TensorField";
		static iString ShortName = "t";

		if(name==ShortName || name==LongName)
		{
			obj = iViewObject::New(parent,LongName,ShortName);
			if(obj == 0) return 0;
			obj->AddSubject(iTensorFieldViewSubject::New(obj,iCoreData::TensorDataSubject::DataType(),iCoreData::ScalarDataSubject::DataType()));
			for(i=0; i<ext.Size(); i++)
			{
				j = 0;
				while((sub = ext[i]->CreateTensorFieldSubject(obj,j++)) != 0) obj->AddSubject(sub);
			}
		}
	}

	if(obj == 0)
	{
		static iString LongName = "VectorField";
		static iString ShortName = "u";

		if(name==ShortName || name==LongName)
		{
			obj = iViewObject::New(parent,LongName,ShortName);
			if(obj == 0) return 0;
			obj->AddSubject(iVectorFieldViewSubject::New(obj,iCoreData::VectorDataSubject::DataType(),iCoreData::ScalarDataSubject::DataType()));
			for(i=0; i<ext.Size(); i++)
			{
				j = 0;
				while((sub = ext[i]->CreateVectorFieldSubject(obj,j++)) != 0) obj->AddSubject(sub);
			}
		}
	}

	if(obj == 0)
	{
		static iString LongName = "Volume";
		static iString ShortName = "v";

		if(name==ShortName || name==LongName)
		{
			obj = iViewObject::New(parent,LongName,ShortName);
			if(obj == 0) return 0;
			obj->AddSubject(iVolumeViewSubject::New(obj,iCoreData::ScalarDataSubject::DataType()));
			for(i=0; i<ext.Size(); i++)
			{
				j = 0;
				while((sub = ext[i]->CreateVolumeSubject(obj,j++)) != 0) obj->AddSubject(sub);
			}
		}
	}

	for(i=0; obj==0 && i<ext.Size(); i++)
	{
		obj = ext[i]->CreateSpecialObject(parent,name);
	}

	iEdition::ApplySettings(obj);

	return obj;
}


//
//  These functions install extensions on various extendable classes.
//
void iObjectFactory::InstallExtensions(iDataReader *reader)
{
	int i;
	iArray<iAbstractExtensionPointer> &ext(Extensions());

	iDataReaderExtension *s;
	for(i=0; i<ext.Size(); i++)
	{
		s = ext[i]->CreateDataReaderExtension(reader);
		if(s != 0) reader->InstallExtension(s);
	}
}


void iObjectFactory::InstallExtensions(iPicker *picker)
{
	int i;
	iArray<iAbstractExtensionPointer> &ext(Extensions());

	iPickerExtension *s;
	for(i=0; i<ext.Size(); i++)
	{
		s = ext[i]->CreatePickerExtension(picker);
		if(s != 0) picker->InstallExtension(s);
	}
}


void iObjectFactory::InstallExtensions(iViewModule *vm)
{
	int i;
	iArray<iAbstractExtensionPointer> &ext(Extensions());

	iViewModuleExtension *s;
	for(i=0; i<ext.Size(); i++)
	{
		s = ext[i]->CreateViewModuleExtension(vm);
		if(s != 0) vm->InstallExtension(s);
	}
}

