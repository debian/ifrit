/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IPALETTE_H
#define IPALETTE_H


#include "iobject.h"


#include "icolor.h"
#include "ipair.h"
#include "istring.h"

class iImage;
class iPaletteCollection;
class iPiecewiseFunction;

class vtkColorTransferFunction;
class vtkLookupTable;


class iPalette : public iObject
{
	
	friend class iPaletteCollection;

public:
	
	vtkTypeMacro(iPalette,iObject);

	iObjectPropertyMacro1S(Name,String);
	iPropertyPiecewiseFunction Red, Green, Blue;

	virtual bool CopyState(const iObject *p);

	iColor GetColor(int n) const;
	const iImage* GetImage(int shape = 0);
	
	void SetComponents(const iPiecewiseFunction *r, const iPiecewiseFunction *g, const iPiecewiseFunction *b);
	iPiecewiseFunction* GetComponent(int n) const;

	inline vtkLookupTable* GetLookupTable(bool reverse) const { return mLT[reverse?1:0]; }
	inline vtkColorTransferFunction* GetColorTransferFunction(bool reverse) const { return mCTF[reverse?1:0]; }

	void Update();

protected:
	
	iPalette(iObject *parent, const iString &fname, const iString &sname);
	virtual ~iPalette();

private:

	iPiecewiseFunction *mRed, *mGreen, *mBlue;
	vtkColorTransferFunction *mCTF[2];
	vtkLookupTable *mLT[2];

	int mImageWidth, mImageHeight;
	bool mImageNeedsUpdate;
	iImage *mImage[3];
};

#endif // IPALETTE_H
