/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  EventObservers used by Shell
//

#ifndef ISHELLEVENTOBSERVERS_H
#define ISHELLEVENTOBSERVERS_H


#include "ieventobserver.h"
#include "ishellcomponent.h"


#include "iarray.h"
#include "istring.h"


class iExecutionEventObserver : public iEventObserver, public iShellComponent
{

public:
	
	vtkTypeMacro(iExecutionEventObserver,iEventObserver);

	//
	//  Public for manual driving
	//
	void Start();
	void Finish();
	void SetProgress(double fraction);
	void SetLabel(const iString &label);

	void SetInterval(double start, double length);

	void PostError(const iString &text);
	void Reset();

	const iString& LastErrorMessage() const;

	inline bool IsAborted() const { return mAborted; }
	inline bool IsInError() const { return mInError; }

protected:

	iExecutionEventObserver(iShell *s);

	virtual void ExecuteBody(vtkObject *caller, unsigned long event, void *data);

	virtual void OnStart() = 0;
	virtual void OnFinish() = 0;
	virtual void OnProgress(double fraction, bool &abort) = 0;
	virtual void OnSetLabel(const iString &label) = 0;

	static iExecutionEventObserver* New(); // not implemented

private:

	struct Item
	{
		double Start, Length, PrevValue;
		iString ErrorMessages;
		Item();
	};

	iArray<Item> mStack;
	bool mAborted, mInError;

#ifdef I_DEBUG
	double prev;
	double time[10];
	iString label;
#endif
};


class iParallelUpdateEventObserver : public iEventObserver, public iShellComponent
{
	
public:
	
	vtkTypeMacro(iParallelUpdateEventObserver,iEventObserver);

	virtual void UpdateInformation() = 0;

protected:

	iParallelUpdateEventObserver(iShell *s);

	virtual void ExecuteBody(vtkObject *caller, unsigned long event, void *data);

	static iParallelUpdateEventObserver* New(); // not implemented (created directly bu shell)
};

#endif // ISHELLEVENTOBSERVERS_H

