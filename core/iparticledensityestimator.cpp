/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iparticledensityestimator.h"


#include "ierror.h"
#include "iparallel.h"
#include "iparticlefileloader.h"
#include "iviewmodule.h"

#include <vtkCommand.h>
#include <vtkFloatArray.h>
#include <vtkIdList.h>
#include <vtkPointData.h>
#include <vtkPointLocator.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>



iParticleDensityEstimator* iParticleDensityEstimator::New(iParticleFileLoader *loader)
{
	IASSERT(loader);
	return new iParticleDensityEstimator(loader);
}


iParticleDensityEstimator::iParticleDensityEstimator(iParticleFileLoader *loader) : iParallelWorker(loader->GetViewModule()->GetParallelManager())
{
	mNumNeighbors = 16;
	mLocator = vtkPointLocator::New(); IERROR_CHECK_MEMORY(mLocator);
	mData = vtkPolyData::New(); IERROR_CHECK_MEMORY(mData);

	mLocator->SetDataSet(mData);
}


iParticleDensityEstimator::~iParticleDensityEstimator()
{
	mLocator->Delete();
	mData->Delete();
}


void iParticleDensityEstimator::SetNumNeighbors(int num)
{
	if(num > 1)
	{
		mNumNeighbors = num;
	}
}



void iParticleDensityEstimator::ComputeDensity(vtkPoints *points, vtkFloatArray *scalars, int varIn, int varOut)
{
	if(points==0 || scalars==0 || scalars->GetDataType()!=VTK_FLOAT || varIn<-1 || varIn>=scalars->GetNumberOfComponents() || varOut<0 || varOut>=scalars->GetNumberOfComponents()) return;

	wVarIn = varIn;
	wVarOut = varOut;

	mData->SetPoints(points);
	mData->GetPointData()->SetScalars(scalars);

	this->InvokeEvent(vtkCommand::StartEvent);
	this->PrepareForStep();	
	this->ParallelExecute(1);
	this->InvokeEvent(vtkCommand::EndEvent);

	mData->SetPoints(0);
	mData->GetPointData()->SetScalars(0);
}


void iParticleDensityEstimator::PrepareForStep()
{
	mLocator->BuildLocator();
}


int iParticleDensityEstimator::ExecuteStep(int step, iParallel::ProcessorInfo &p)
{
	vtkIdType l, kstp, kbeg, kend;
	vtkIdType id;
	int i, n;

	iParallel::SplitRange(p,mData->GetNumberOfPoints(),kbeg,kend,kstp);

	double r2, sr0, sr2, r2max, x[3], x1[3];
	vtkPoints *points = mData->GetPoints();
	int nc = mData->GetPointData()->GetScalars()->GetNumberOfComponents();
	vtkIdList *result = vtkIdList::New();
	if(result == 0) return 1;

	float v;
	float *ptrIn = (float *)mData->GetPointData()->GetScalars()->GetVoidPointer(0) + wVarIn;
	float *ptrOut = (float *)mData->GetPointData()->GetScalars()->GetVoidPointer(0) + wVarOut;
	for(l=kbeg; l<kend; l++)
	{
		if(l%1000 == 0)
		{
			if(p.IsMaster()) this->UpdateProgress(double(l-kbeg)/(kend-kbeg));
			if(this->GetAbortExecute()) break;
		}

		points->GetPoint(l,x);
		mLocator->FindClosestNPoints(mNumNeighbors,x,result);

		n = result->GetNumberOfIds();
		sr0 = sr2 = r2max = 0.0;
		for(i=0; i<n; i++)
		{
			id = result->GetId(i);
			points->GetPoint(id,x1);
			r2 = vtkMath::Distance2BetweenPoints(x,x1);
			v = (wVarIn == -1) ? 1.0 : ptrIn[nc*id];
			sr0 += v;
			sr2 += v*r2;
			if(r2 > r2max) r2max = r2;
		}

		if(n>0 && r2max>0.0)
		{
			ptrOut[nc*l] = 2.5*(sr0-sr2/r2max)/(12.566*n*r2max*sqrt(r2max));
		}
		else
		{
			ptrOut[nc*l] = (wVarIn == -1) ? 1.0 : ptrIn[nc*l];
		}
	}

	result->Delete();

	return 0;
}

