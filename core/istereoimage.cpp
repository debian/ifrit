/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "istereoimage.h"


//
//  StereoImage class
//
iStereoImage::iStereoImage()
{
	mIsStereo = false;
}


iStereoImage::iStereoImage(const iImage &img)
{
	this->operator=(img);
}


iStereoImage::iStereoImage(const iStereoImage &img)
{
	this->operator=(img);
}


iStereoImage& iStereoImage::operator=(const iStereoImage &img)
{
	mEyeImage[0] = img.mEyeImage[0];
	mEyeImage[1] = img.mEyeImage[1];
	mIsStereo = img.mIsStereo;
	mTag = img.mTag;
	return *this;
}


iStereoImage& iStereoImage::operator=(const iImage &img)
{
	mEyeImage[0] = img;
	mEyeImage[1].Clear();
	mIsStereo = false;
	mTag.Clear();
	return *this;
}


void iStereoImage::SetStereo(bool s)
{
	if(mIsStereo != s)
	{
		mIsStereo = s;
		if(s) this->CopyLeftEyeToRightEye(); else mEyeImage[1].Clear();
	}
}


void iStereoImage::CopyLeftEyeToRightEye()
{
	int w = mEyeImage[0].Width();
	int h = mEyeImage[0].Height();

	mEyeImage[1].Scale(w,h);
	memcpy(mEyeImage[1].DataPointer(),mEyeImage[0].DataPointer(),mEyeImage[0].Depth()*w*h);
	mIsStereo = true;
}


//
//  iImage functions
//
void iStereoImage::Scale(int w, int h, iImage::ScaleMode m, iImage::ScaleFilter f)
{
	mEyeImage[0].Scale(w,h,m,f);
	if(mIsStereo) mEyeImage[1].Scale(w,h,m,f);
}


void iStereoImage::ReplaceData(int n, vtkImageData *data)
{
	switch(n)
	{
	case 0:
		{
			mEyeImage[0].ReplaceData(data);
			if(mIsStereo)
			{
				mEyeImage[1].Clear();
				mIsStereo = false;
			}
			break;
		}
	case 1:
		{
			mIsStereo = true;
			mEyeImage[1].ReplaceData(data);
			mEyeImage[1].Scale(mEyeImage[0].Width(),mEyeImage[0].Height());
			break;
		}
	}
}


void iStereoImage::Blend(const iImage& im, float op)
{
	mEyeImage[0].Blend(im,op);
	if(mIsStereo) mEyeImage[1].Blend(im,op);
}


void iStereoImage::Blend(const iStereoImage& im, float op)
{
	mEyeImage[0].Blend(im.Eye(0),op);
	if(mIsStereo && im.mIsStereo) mEyeImage[1].Blend(im.Eye(1),op);
}


void iStereoImage::Overlay(int x, int y, const iImage& ovr, float opacity, bool masking)
{
	mEyeImage[0].Overlay(x,y,ovr,opacity,masking);
	if(mIsStereo) mEyeImage[1].Overlay(x,y,ovr,opacity,masking);
}


void iStereoImage::Overlay(int x, int y, const iStereoImage& ovr, float opacity, bool masking)
{
	mEyeImage[0].Overlay(x,y,ovr.LeftEye(),opacity,masking);
	if(mIsStereo) mEyeImage[1].Overlay(x,y,ovr.RightEye(),opacity,masking);
}


bool iStereoImage::CombineInPseudoColor(const iStereoImage &imRed, const iStereoImage &imGreen, const iStereoImage &imBlue)
{
	if(!mEyeImage[0].CombineInPseudoColor(imRed.Eye(0),imGreen.Eye(0),imBlue.Eye(0))) return false;
	if(mIsStereo) return mEyeImage[1].CombineInPseudoColor(imRed.Eye(1),imGreen.Eye(1),imBlue.Eye(1)); else return true;
}


void iStereoImage::DrawFrame(int width, const iColor &color)
{
	mEyeImage[0].DrawFrame(width,color);
	if(mIsStereo) mEyeImage[1].DrawFrame(width,color);
}


void iStereoImage::SetTag(const iString& tag)
{
	mTag = tag;
}

