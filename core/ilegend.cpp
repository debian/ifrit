/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ilegend.h"


#include "ierror.h"
#include "ioverlayhelper.h"

#include <vtkCamera.h>
#include <vtkMath.h>
#include <vtkProperty2D.h>
#include <vtkRenderer.h>
#include <vtkTextMapper.h>
#include <vtkTextProperty.h>

//
//  Templates
//
#include "iarray.tlh"
#include "igenericprop.tlh"


iLegend* iLegend::New(iRenderTool *rv)
{
	IASSERT(rv);
	return new iLegend(rv);
}


iLegend::iLegend(iRenderTool *rv) : iGenericProp<vtkLegendBoxActor>(true), mOverlayHelper(rv)
{
	this->BorderOn(); 
	this->GetPositionCoordinate()->SetCoordinateSystemToNormalizedViewport();
	this->GetPosition2Coordinate()->SetCoordinateSystemToNormalizedViewport();
	this->GetPosition2Coordinate()->SetReferenceCoordinate(this->GetPositionCoordinate());
	this->SetPosition2(0.25,0.08);
	this->BorderOn();
	this->LockBorderOn();
	this->GetProperty()->SetColor(0.0,0.0,0.0);
	this->GetProperty()->SetLineWidth(2.0);
	this->GetEntryTextProperty()->SetJustificationToLeft(); 
	this->GetEntryTextProperty()->SetVerticalJustificationToCentered();
	this->GetEntryTextProperty()->SetBold(1);

	this->PickableOff();
}


iLegend::~iLegend()
{
}


void iLegend::UpdateGeometry(vtkViewport *viewport)
{
	//
	//  Maintain the font size
	//
	int mag = mOverlayHelper->GetRenderingMagnification();
	if(mag == 1)
	{
		double *x1 = this->GetPosition();
		double *x2 = this->GetPosition2();
		int *size = viewport->GetSize();
		float s = (mOverlayHelper->GetFontSize(viewport)*NumberOfEntries+(NumberOfEntries+1)*Padding)/fabs(size[1]*x2[1]);

		mOverlayHelper->UpdateTextProperty(viewport,this->EntryTextProperty);
		this->EntryTextProperty->Modified();

		mUnmagx1[0] = x1[0];
		mUnmagx1[1] = x1[1];
		mUnmagx2[0] = s*x2[0];
		mUnmagx2[1] = s*x2[1];
		//
		//  Corrections
		//
		if(mUnmagx2[0] > 0.25) mUnmagx2[0] = 0.25;
		if(mUnmagx2[1] > 0.16) mUnmagx2[1] = 0.16;
		if(mUnmagx1[0] > 0.5) 
		{
			mUnmagx1[0] = 0.98 - mUnmagx2[0];
			this->SetPosition(mUnmagx1[0],mUnmagx1[1]);
		}
	}
	else
	{
		int winij[2];
		mOverlayHelper->ComputePositionShiftsUnderMagnification(winij);
		
		this->SetPosition(mag*mUnmagx1[0]-winij[0],mag*mUnmagx1[1]-winij[1]);
		this->Padding *= mag;
	}
	
	this->SetPosition2(mUnmagx2[0]*mag,mUnmagx2[1]*mag);

	//
	//  Scale lines
	//
	this->GetProperty()->SetLineWidth(2*mag);
	this->GetProperty()->SetColor(this->GetOverlayHelper()->GetColor(viewport).ToVTK());
}


void iLegend::UpdateOverlay(vtkViewport *viewport)
{
	int i;

	//
	//  Redo text colors and rerender them
	//
    for(i=0; i<this->NumberOfEntries; i++)
	{
		//this->TextMapper[i]->GetTextProperty()->ShallowCopy(this->GetEntryTextProperty());
		this->TextMapper[i]->GetTextProperty()->SetColor(this->GetOverlayHelper()->GetColor(viewport).ToVTK());
    }
}


void iLegend::Reset()
{
	int mag = mOverlayHelper->GetRenderingMagnification();

	//
	//  Reset to the unmagnified state
	//
	if(mag > 1)
	{
		this->SetPosition(mUnmagx1[0],mUnmagx1[1]);
		this->SetPosition2(mUnmagx2[0],mUnmagx2[1]);
		this->Padding /= mag;
	}
}

