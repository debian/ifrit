/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iparticleviewsubject.h"


#include "iactor.h"
#include "icolorbar.h"
#include "icommondatadistributors.h"
#include "icoredatasubjects.h"
#include "idatalimits.h"
#include "idatareader.h"
#include "idatasubject.h"
#include "ierror.h"
#include "ilookuptable.h"
#include "imath.h"
#include "iparticleconnector.h"
#include "iparticlepipeline.h"
#include "iparticlesplitter.h"
#include "ipointglyph.h"
#include "irendertool.h"
#include "istretch.h"
#include "iviewmodule.h"
#include "iviewmoduleeventobservers.h"
#include "iviewobject.h"
#include "iviewsubjectparallelpipeline.h"

#include <vtkCamera.h>
#include <vtkPolyData.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"
#include "igenericfilter.tlh"
#include "iproperty.tlh"


using namespace iParameter;


namespace iParticleViewSubject_Private
{
	class CameraEventObserver : public iEventObserver
	{

	public:

		vtkTypeMacro(CameraEventObserver,iEventObserver);
		static CameraEventObserver* New(iParticleViewInstance *owner = 0)
		{
			IASSERT(owner);
			return new CameraEventObserver(owner);
		}

		void Activate(bool s)
		{
			if(s)
			{
				mReferenceSize = mOwner->GetFixedSize();
				mParallelScale = mCamera->GetParallelScale();
				mDistance = mCamera->GetDistance();
			}
			else
			{
				mReferenceSize = -1.0;
			}
		}

	protected:

		CameraEventObserver(iParticleViewInstance *owner) : iEventObserver()
		{
			mOwner = owner;

			mReferenceSize = -1.0;
			mParallelScale = mDistance = 0.0;

			mCamera = mOwner->GetViewModule()->GetRenderer()->GetActiveCamera();
			mCamera->AddObserver(vtkCommand::ModifiedEvent,this);
			mCamera->Register(this);
		}

		~CameraEventObserver()
		{
			mCamera->RemoveObserver(this);
			mCamera->UnRegister(this);
		}

		virtual void ExecuteBody(vtkObject *caller, unsigned long event, void *)
		{
			if(caller==mCamera && event==vtkCommand::ModifiedEvent)
			{
				if(mReferenceSize > 0.0)
				{
					if(mCamera->GetParallelProjection() != 0)
					{
						mOwner->SetFixedSize(mReferenceSize*mCamera->GetParallelScale()/mParallelScale);
					}
					else
					{
						mOwner->SetFixedSize(mReferenceSize*mCamera->GetDistance()/mDistance);
					}
				}
			}
		}

	private:

		vtkCamera *mCamera;
		iParticleViewInstance *mOwner;
		double mReferenceSize, mParallelScale, mDistance;
	};
};


using namespace iParticleViewSubject_Private;


//
//  Single instance class
//
iParticleViewInstance::iParticleViewInstance(iParticleViewSubject *owner) : iPaintedViewInstance(owner,2,0.99,2,ColorBarPriority::Particles,false)
{
	mRealOwner = owner;

	mType = PointGlyphType::Point;

	mScaleVar = -1;

	mFixedSize = 1.0;
	mScaleFactor = 1.0;

	mLineWidth = 1;
	mAutoScaled = false;

	mConnector = iParticleConnector::New(this); IERROR_CHECK_MEMORY(mConnector);
	mCameraEventObserver = CameraEventObserver::New(this); IERROR_CHECK_MEMORY(mCameraEventObserver);

	mConnector->AddObserver(vtkCommand::ProgressEvent,this->GetViewModule()->GetRenderEventObserver());

	mActors[0]->GetProperty()->SetPointSize(mFixedSize);
	mActors[0]->GetProperty()->SetLineWidth(mLineWidth);
	mActors[1]->GetProperty()->SetPointSize(1);
	mActors[1]->GetProperty()->SetLineWidth(1);
	mActors[1]->VisibilityOff();
	mActors[1]->PickableOff();

	this->SetColor(owner->GetDefaultColor());
}


iParticleViewInstance::~iParticleViewInstance()
{
	mCameraEventObserver->Delete();
	mConnector->Delete();
}


void iParticleViewInstance::ConfigureBody()
{
	//
	//  Create pipeline (must be created after the object is fully created)
	//	
	this->AddMainPipeline(1);
	mActors[0]->SetInputConnection(mConnector->GetOutputPort());
	mActors[1]->SetInputConnection(this->Pipeline()->GetOutputPort());
	this->Pipeline()->SetInputConnection(mConnector->GetOutputPort());
}


void iParticleViewInstance::SetInputConnection(vtkAlgorithmOutput *port)
{
	mConnector->SetInputConnection(port);
}


iViewSubjectPipeline* iParticleViewInstance::CreatePipeline(int)
{
	return new iParticlePipeline(this);
}


void iParticleViewInstance::ConfigureMainPipeline(iViewSubjectPipeline *p, int)
{
	iViewSubjectParallelPipeline *pp = iRequiredCast<iViewSubjectParallelPipeline>(INFO,p);

	pp->GetDataManager()->AddDistributor(new iPolyDataDistributor(pp->GetDataManager()));
	pp->GetDataManager()->AddCollector(new iPolyDataCollector(pp->GetDataManager(),0));
}


void iParticleViewInstance::ShowBody(bool show)
{
	if(show)
	{
		mActors[0]->VisibilityOn();
		if(mType != PointGlyphType::Point) mActors[1]->VisibilityOn();
	} 
	else 
	{
		mActors[0]->VisibilityOff();
		mActors[1]->VisibilityOff();
	}
}


bool iParticleViewInstance::SetType(int t)
{
	if(t<0 || t>=PointGlyphType::SIZE) return false;

	mType = t;
	this->Pipeline()->UpdateContents(iParticlePipeline::KeyType);

	if(mType == PointGlyphType::Point)
	{
		mActors[0]->GetProperty()->SetPointSize(mFixedSize);
		mActors[1]->VisibilityOff();
	}
	else
	{
		mActors[0]->GetProperty()->SetPointSize(1.0); 
		mActors[1]->SetVisibility(mActors[0]->GetVisibility());
	}

	return true;
}


bool iParticleViewInstance::SetScaleVar(int v)
{ 
	if(v<-1 || (this->IsThereData() && v>=this->GetLimits()->GetNumVars())) return false;

	if(mScaleVar != v)
	{
		mScaleVar = v;
		this->Pipeline()->UpdateContents(iParticlePipeline::KeyScaling);
	}
	return true;
}


bool iParticleViewInstance::SetFixedSize(float s)
{ 
	if(s>0.0 && s<1000.1)
	{
		mFixedSize = s;
		if(mType == PointGlyphType::Point) mActors[0]->GetProperty()->SetPointSize(mFixedSize); 
		this->Pipeline()->UpdateContents(iParticlePipeline::KeyFixedSize);
		return true;
	}
	else return false;
}


bool iParticleViewInstance::SetScaleFactor(float f)
{
	if(f > 0.0)
	{
		mScaleFactor = f;
		this->Pipeline()->UpdateContents(iParticlePipeline::KeyScaling);
		return true;
	}
	else return false;
}


bool iParticleViewInstance::SetLineWidth(int w)
{
	if(w>=1 && w<=100)
	{
		mLineWidth = w;
		mActors[0]->GetProperty()->SetLineWidth(mLineWidth);
		return true;
	}
	else return false;
}


bool iParticleViewInstance::SetAutoScaled(bool s)
{
	if(s != mAutoScaled)
	{
		mAutoScaled = s;
		iRequiredCast<CameraEventObserver>(INFO,mCameraEventObserver)->Activate(s);
	}
	return true;
}


bool iParticleViewInstance::SetConnectVar(int a)
{
	mConnector->SetConnectVariable(a);
	return true;
}


bool iParticleViewInstance::SetConnectionBreakVar(int a)
{
	mConnector->SetConnectionBreakVariable(a);
	return true;
}


int iParticleViewInstance::GetConnectVar() const
{
	return mConnector->GetConnectVariable();
}


int iParticleViewInstance::GetConnectionBreakVar() const
{
	return mConnector->GetConnectionBreakVariable();
}


bool iParticleViewInstance::SyncWithDataBody()
{
	if(mScaleVar >= this->GetLimits()->GetNumVars()) this->SetScaleVar(-1);
	if(this->GetConnectVar() >= this->GetLimits()->GetNumVars()) this->SetConnectVar(-1);
	if(this->GetConnectionBreakVar() >= this->GetLimits()->GetNumVars()) this->SetConnectionBreakVar(-1);
	return true;
}


bool iParticleViewInstance::CanBeShown() const
{
	return this->IsThereData();
}


bool iParticleViewInstance::IsConnectedToPaintingData() const
{
	return this->IsThereData();
}


//
//  Main class
//
iParticleViewSubject* iParticleViewSubject::New(iViewObject *parent,const iDataType &type,const iColor &color)
{
	iParticleViewSubject *obj = new iParticleViewSubject(parent,type,color); IERROR_CHECK_MEMORY(obj);
	obj->InitInstances();
	return obj;
}


iParticleViewSubject::iParticleViewSubject(iViewObject *parent, const iDataType &type, const iColor &color) : iPaintedViewSubject(parent,parent->LongName(),parent->ShortName(),parent->GetViewModule(),type,type,ViewObject::Flag::IsSameColor|ViewObject::Flag::IsSameOpacity,1,iMath::_IntMax),
	SplitVar(iObject::Self(),static_cast<iType::ps_int::SetterType>(&iParticleViewSubject::SetSplitVar),static_cast<iType::ps_int::GetterType>(&iParticleViewSubject::GetSplitVar),"SplitVar","v",9),
	SplitRanges(iObject::Self(),static_cast<iType::pv_pair::SetterType>(&iParticleViewSubject::SetSplitRanges),static_cast<iType::pv_pair::GetterType>(&iParticleViewSubject::GetSplitRanges),"SplitRanges","r",static_cast<iType::pv_pair::SizeType>(&iParticleViewSubject::SizeSplitRanges),static_cast<iType::pv_pair::ResizeType>(&iParticleViewSubject::ResizeSplitRanges),8),
	SplitRangesTiled(iObject::Self(),static_cast<iType::ps_bool::SetterType>(&iParticleViewSubject::SetSplitRangesTiled),static_cast<iType::ps_bool::GetterType>(&iParticleViewSubject::GetSplitRangesTiled),"SplitRangesTiled","rt",7),
	iViewSubjectPropertyConstructMacro(Int,iParticleViewInstance,Type,t),
	iViewSubjectPropertyConstructMacro(Int,iParticleViewInstance,ScaleVar,sv),
	iViewSubjectPropertyConstructMacro(Float,iParticleViewInstance,ScaleFactor,sf),
	iViewSubjectPropertyConstructMacro(Float,iParticleViewInstance,FixedSize,s),
	iViewSubjectPropertyConstructMacro(Int,iParticleViewInstance,LineWidth,lw),
	iViewSubjectPropertyConstructMacro(Bool,iParticleViewInstance,AutoScaled,au),
	iViewSubjectPropertyConstructMacro(Int,iParticleViewInstance,ConnectVar,cv),
	iViewSubjectPropertyConstructMacro(Int,iParticleViewInstance,ConnectionBreakVar,cbv),
	iObjectConstructPropertyMacroS(Int,iParticleViewSubject,RenderSortVar,rsv)
{
	SplitVar.AddFlag(iProperty::_FunctionsAsIndex);
	ScaleVar.AddFlag(iProperty::_FunctionsAsIndex);
	ConnectVar.AddFlag(iProperty::_FunctionsAsIndex);
	ConnectionBreakVar.AddFlag(iProperty::_FunctionsAsIndex);

	mSplitter = iParticleSplitter::New(this); IERROR_CHECK_MEMORY(mSplitter);

	mSplitVar = mRenderSortVar = -1;
	mSplitRangesTiled = false;

	if(color.IsValid()) mDefaultColor = color;
}


iParticleViewSubject::~iParticleViewSubject()
{
	mSplitter->Delete();
}


void iParticleViewSubject::InitInstances()
{
	this->iViewSubject::InitInstances();

	iRequiredCast<iParticleViewInstance>(INFO,mInstances[0])->SetInputConnection(mSplitter->GetOutputPort());
}


bool iParticleViewSubject::CreateInstance()
{
	int oldLast = mSplitter->GetNumberOfPieces() - 1;
	iPair pold(mSplitter->GetPieceRange(oldLast));
	iPair pnew(0.5*(pold.Min+pold.Max),pold.Max);
	pold.Max = pnew.Min;

	if(!mSplitter->CreatePiece(pold)) return false;

	//
	//  Create a new piece
	//
	iParticleViewInstance *ins = new iParticleViewInstance(this);
	if(ins == 0)
	{
		mSplitter->DeletePiece(mSplitter->GetNumberOfPieces()-1);
		return false;
	}
	ins->Configure();
	int newLast = mInstances.Size();
	mInstances.Add(ins);

	//
	//  Set the correct piece number
	//
	this->CopyInstances(newLast,oldLast);
	ins->SetInputConnection(mSplitter->GetOutputPort(mSplitter->GetNumberOfPieces()-1));

	this->SetSplitRanges(oldLast,pold);
	this->SetSplitRanges(newLast,pnew);

	ins->Show(this->IsVisible());
	
	return true;
}


bool iParticleViewSubject::RemoveInstance(int n)
{
	if(mSplitter->DeletePiece(n))
	{
		mInstances[n]->Delete();
		mInstances.Remove(n);
		return true;
	}
	else return false;
}


void iParticleViewSubject::BecomeClone(iViewSubject *v)
{
	mSplitter->TakeOverData(v != 0);
}


bool iParticleViewSubject::SyncWithDataBody()
{
	mSplitter->SetInputData(vtkPolyData::SafeDownCast(this->GetSubject()->GetData()));
	this->SetSplitRangesTiled(mSplitRangesTiled);
	return true;
}


bool iParticleViewSubject::SetSplitVar(int v)
{
	if(v!=-1 && mSplitter->GetSorterMode()) return false;
	if(v == mSplitVar) return true;

	mSplitter->SetSplitVariable(v);
	if(mSplitVar != mSplitter->GetSplitVariable())
	{
		mSplitVar = mSplitter->GetSplitVariable();
		while(mInstances.MaxIndex() > 0)
		{
			mInstances[mInstances.MaxIndex()]->Delete();
			mInstances.Remove(mInstances.MaxIndex());
		}
		mSplitRangesTiled = false;

		return true;
	}
	else return false;
}


bool iParticleViewSubject::SetRenderSortVar(int v)
{
	if(v<-1 || (this->IsThereData() && v>=this->GetLimits()->GetNumVars())) return false;

	if(mRenderSortVar != v)
	{
		mRenderSortVar = v;

		if(v > -1)
		{
			this->SetSplitVar(-1);
			mSplitter->SetSplitVariable(mRenderSortVar);
			mSplitter->SetSorterMode(true);
		}
		else
		{
			mSplitter->SetSorterMode(false);
		}
	}

	return true;
}


bool iParticleViewSubject::SetSplitRangesTiled(bool s)
{
	mSplitRangesTiled = s;
	if(mSplitVar==-1 || !s) return true;

	float min = iStretch::Apply(this->GetLimits()->GetMin(mSplitVar),this->GetLimits()->GetStretchId(mSplitVar),false);
	float max = iStretch::Apply(this->GetLimits()->GetMax(mSplitVar),this->GetLimits()->GetStretchId(mSplitVar),true);

	iArray<iPair> &arr(mSplitter->mRanges);  // cache for speed

	//
	//  arange all ranges in a tiled sequence
	//
	int i;
	float d = (max-min)/arr.Size();
	for(i=0; i<arr.Size(); i++)
	{
		arr[i].Min = iStretch::Reset(min+d*i,this->GetLimits()->GetStretchId(mSplitVar));
		arr[i].Max = iStretch::Reset(min+d*(i+1),this->GetLimits()->GetStretchId(mSplitVar));
	}

	mSplitter->Modified();

	return true;
}


int iParticleViewSubject::SizeSplitRanges() const
{
	return mInstances.Size();
}


bool iParticleViewSubject::ResizeSplitRanges(int n)
{
	if(n < 1) return false;

	while(n < mInstances.Size())
	{
		if(!this->RemoveInstance(mInstances.MaxIndex()))
		{
			mSplitRangesTiled = false;
			return false;
		}
	}

	while(n > mInstances.Size())
	{
		if(!this->CreateInstance())
		{
			mSplitRangesTiled = false;
			return false;
		}
	}

	this->SetSplitRangesTiled(mSplitRangesTiled);
	return true;
}


bool iParticleViewSubject::SetSplitRanges(int i, const iPair& p)
{
	//
	//  Special case - one range changed in a tiled mode
	//
	iPair p1(p);
	if(mSplitRangesTiled)
	{
		iArray<iPair> &arr(mSplitter->mRanges);  // cache for speed
		if(i == 0)
		{
			p1.Min = arr[0].Min;
		}
		else
		{
			arr[i-1].Max = p1.Min;
		}
		if(i == arr.MaxIndex())
		{
			p1.Max = arr[arr.MaxIndex()].Max;
		}
		else
		{
			arr[i+1].Min = p1.Max;
		}
	}

	mSplitter->SetPieceRange(i,p1);

	return true;
}


const iPair& iParticleViewSubject::GetSplitRanges(int i) const
{
	return mSplitter->GetPieceRange(i);
}


int iParticleViewSubject::DepthPeelingRequestBody() const
{
	if(mRenderSortVar > -1) return -1;

	int i;
	for(i=0; i<mInstances.Size(); i++)
	{
		if(PaintVar.GetValue(i) > -1) return 1;
	}

	return 0;
}

