/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iparticleprobefilter.h"


#include "ierror.h"
#include "iviewsubject.h"

#include <vtkPointData.h>
#include <vtkPointLocator.h>

//
//  Templates
//
#include "igenericfilter.tlh"


iParticleProbeFilter::iParticleProbeFilter(iDataConsumer *consumer) : iProbeFilter(consumer)
{
}


void iParticleProbeFilter::ProvideOutput()
{
	vtkDataSet *input = this->InputData();
	vtkDataSet *output = this->OutputData();
	vtkDataSet *source = this->SourceData();

	output->ShallowCopy(input);

	if(input->IsA("vtkPointSet") && output->IsA("vtkPointSet"))
	{
		vtkDataArray *atts = vtkDataSet::SafeDownCast(source)->GetPointData()->GetScalars();
		if(atts!=0 && atts->GetSize()>0)
		{
			vtkFloatArray *outScalars = vtkFloatArray::New(); IERROR_CHECK_MEMORY(outScalars);
			outScalars->SetNumberOfComponents(atts->GetNumberOfComponents());
			
			vtkPointLocator *locator = vtkPointLocator::New(); IERROR_CHECK_MEMORY(locator);
			locator->SetDataSet(vtkDataSet::SafeDownCast(source));
			locator->BuildLocator();

			vtkPoints *pts = vtkPointSet::SafeDownCast(input)->GetPoints();
			vtkIdType i, l, n = pts->GetNumberOfPoints();
			for(i=0; i<n; i++)
			{
				l = locator->FindClosestPoint(pts->GetPoint(i)); 
				if(l != -1) outScalars->InsertNextTuple(atts->GetTuple(l)); else outScalars->InsertNextTuple(atts->GetTuple(0));
			}

			output->GetPointData()->SetScalars(outScalars);
			outScalars->Delete();
			locator->Delete();
		}
	}
}


