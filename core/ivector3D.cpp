/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ivector3D.h"


//
//  3D vector as a type for the Calculator use
//
iVector3D::iVector3D()
{
	int i;
	for(i=0; i<3; i++) mData[i] = 0.0;  //  fastest initialization!!! (faster than memcpy or unrolling the loop)
}


iVector3D::iVector3D(double x, double y, double z)
{
	mData[0] = x;
	mData[1] = y;
	mData[2] = z;
}


iVector3D::iVector3D(const double *pos)
{
	int i;
	if(pos != 0)
	{
		for(i=0; i<3; i++) mData[i] = pos[i];  //  fastest initialization!!! (faster than memcpy or unrolling the loop)
	}
	else
	{
		for(i=0; i<3; i++) mData[i] = 0.0;  //  fastest initialization!!! (faster than memcpy or unrolling the loop)
	}
}


iVector3D::iVector3D(const iVector3D &v)
{
	int i;
	for(i=0; i<3; i++) mData[i] = v.mData[i];  //  fastest initialization!!! (faster than memcpy or unrolling the loop)
}


iVector3D::~iVector3D()
{
}


iVector3D& iVector3D::operator=(const double *pos)
{
	int i;
	if(pos != 0)
	{
		for(i=0; i<3; i++) mData[i] = pos[i];  //  fastest initialization!!! (faster than memcpy or unrolling the loop)
	}
	else
	{
		for(i=0; i<3; i++) mData[i] = 0.0;  //  fastest initialization!!! (faster than memcpy or unrolling the loop)
	}
	return *this;
}


iVector3D& iVector3D::operator=(const iVector3D &v)
{
	int i;
	for(i=0; i<3; i++) mData[i] = v.mData[i];
	return *this;
}


iVector3D& iVector3D::operator+=(const iVector3D &v)
{
	int i;
	for(i=0; i<3; i++) mData[i] += v.mData[i];
	return *this;
}


iVector3D& iVector3D::operator-=(const iVector3D &v)
{
	int i;
	for(i=0; i<3; i++) mData[i] -= v.mData[i];
	return *this;
}


iVector3D operator+(const iVector3D &v1, const iVector3D &v2)
{
	int i;
	iVector3D tmp;
	for(i=0; i<3; i++) tmp.mData[i] = v1.mData[i] + v2.mData[i];
	return tmp;
}


iVector3D operator-(const iVector3D &v1, const iVector3D &v2)
{
	int i;
	iVector3D tmp;
	for(i=0; i<3; i++) tmp.mData[i] = v1.mData[i] - v2.mData[i];
	return tmp;
}


iVector3D operator*(const iVector3D &v, double s)
{
	int i;
	iVector3D tmp;
	for(i=0; i<3; i++) tmp.mData[i] = v.mData[i]*s;
	return tmp;
}

