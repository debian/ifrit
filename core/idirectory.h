/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A class that accesses file names in a directory. It uses vtkDirectory
//  class internally, but changes interface to more convenient.
//

#ifndef IDIRECTORY_H
#define IDIRECTORY_H


#include "iarray.h"
#include "istring.h"


class iDirectory
{

public:

	iDirectory();

	bool Open(const iString &name);
	void Close();
	inline bool IsOpened() const { return !mDirectory.IsEmpty(); }
	inline const iString& GetDirectoryPrefix() const { return mDirectory; }
	
	inline int GetNumberOfFiles() const { return mFiles.Size(); }
	const iString GetFile(int i, bool prefixed = false) const;
	const iString GetFile(const iString & file, int skip) const;
 
	bool Make(const iString &name);

	//
	//  Directory naming convention for portability
	//
	static const iString& Separator();
	static const iString& Current();
	static void ExpandFileName(iString& name, const iString &path = "");

private:

	iString mDirectory;
	iOrderedArray<iString> mFiles;
};

#endif // IDIRECTORY_H

