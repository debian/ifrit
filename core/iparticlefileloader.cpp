/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iparticlefileloader.h"


#include "ibuffer.h"
#include "idata.h"
#include "idatalimits.h"
#include "idatareader.h"
#include "idatasubject.h"
#include "ierror.h"
#include "imonitor.h"
#include "iparallel.h"
#include "iparallelmanager.h"
#include "iparallelworker.h"
#include "iparticledensityestimator.h"
#include "ishell.h"
#include "ishelleventobservers.h"
#include "iviewmodule.h"

#include <vtkCellArray.h>
#include <vtkDoubleArray.h>
#include <vtkFloatArray.h>
#include <vtkMath.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>

//
//  Templates
//
#include "iarray.tlh"
#include "ibuffer.tlh"


//
//  Helper classes for parallel execution & templates
//
class iParticleHelper : protected iParallelWorker
{

public:

	iParticleHelper(iParticleFileLoader *loader);

	void ShiftData(bool paf, int dim, vtkIdType n, bool per[3], double dr, float *pf, double *pd);
	void AddOrderAsVar(int var, int nc, vtkIdType n, float *pf);

	template<class T, class D>
	bool ReadFortranRecordWithMaskTemplate(iFile &F, int dest, int inNum, int outOffset, double updateStart, double updateDuration, T *scale, T *offset);

	template<class T>
	void AutoScalePositions();

protected:

	virtual int ExecuteStep(int step, iParallel::ProcessorInfo &p);

	iParticleFileLoader *mLoader;

	bool *mPeriodic;
	float *mFarr;
	double *mDarr;

	int mDim, mCom, mNumProcs;
	bool mPaf;
	vtkIdType mTot;
	double mDr;
};


//
//  Main class
//
iParticleFileLoader::iParticleFileLoader(iDataReader *r, int priority, bool useExtras) : iFileLoader(r,priority), mUseExtras(useExtras)
{
	this->Define();
}


iParticleFileLoader::iParticleFileLoader(iDataReaderExtension *ext, int priority, bool useExtras) : iFileLoader(ext,priority), mUseExtras(useExtras)
{
	this->Define();
}


void iParticleFileLoader::Define()
{
	mHaveNormals = false;

	mHelper = new iParticleHelper(this); IERROR_CHECK_MEMORY(mHelper);
	mDensityEstimator = iParticleDensityEstimator::New(this); IERROR_CHECK_MEMORY(mDensityEstimator);
	mDensityEstimator->AddObserver(vtkCommand::StartEvent,this->GetShell()->GetExecutionEventObserver());
	mDensityEstimator->AddObserver(vtkCommand::ProgressEvent,this->GetShell()->GetExecutionEventObserver());
	mDensityEstimator->AddObserver(vtkCommand::EndEvent,this->GetShell()->GetExecutionEventObserver());
}


iParticleFileLoader::~iParticleFileLoader()
{
	delete mHelper;
	mDensityEstimator->Delete();
}


void iParticleFileLoader::SetDownsampleMode(int v)
{
	mIterator.SetDownsampleMode(v);
	this->GetReader()->RequestDataReload(this->GetDataInfo());
}


int iParticleFileLoader::GetDownsampleMode() const
{
	return mIterator.GetDownsampleMode();
}


void iParticleFileLoader::SetNumNeighbors(int num)
{
	mDensityEstimator->SetNumNeighbors(num);
	this->GetReader()->RequestDataReload(this->GetDataInfo());
}


int iParticleFileLoader::GetNumNeighbors() const
{
	return mDensityEstimator->GetNumNeighbors();
}


void iParticleFileLoader::SetTypeIncluded(int type, bool s)
{
	if(type>=0 && type<this->NumStreams())
	{
		this->GetStream(type)->Included = s;
		this->GetReader()->RequestDataReload(this->GetDataType(type));
	}
}


void iParticleFileLoader::SetDownsampleFactor(int type, int value)
{
	if(value > 0)
	{
		if(type>=0 && type<this->NumStreams())
		{
			this->GetStream(type)->DownsampleFactor = value;
		}
		else if(type == -1)
		{
			int i;
			for(i=0; i<this->NumStreams(); i++) this->GetStream(i)->DownsampleFactor = value;
		}
		this->GetReader()->RequestDataReload(this->GetDataInfo());
	}
}


void iParticleFileLoader::SetDensityVariable(int type, int var)
{
	if(mUseExtras && type>=0 && type<this->NumStreams())
	{
		this->GetStream(type)->Extras.DensityVariable = (var > -2) ? var : -2;
		this->GetReader()->RequestDataReload(this->GetDataInfo());
	}
}


void iParticleFileLoader::SetAddOrderAsVariable(int type, bool s)
{
	if(mUseExtras && type>=0 && type<this->NumStreams())
	{
		this->GetStream(type)->Extras.AddOrderAsVariable = s;
		this->GetReader()->RequestDataReload(this->GetDataInfo());
	}
}


void iParticleFileLoader::AddExtras(ParticleStream *stream)
{
	int nvar = stream->NumVariablesInFile;

	//
	//  If we save the order in file as a variable, fill it in.
	//
	if(stream->Extras.AddOrderAsVariable && nvar<stream->NumVariablesInData)
	{
		mHelper->AddOrderAsVar(nvar,stream->Scalars()->GetNumberOfComponents(),stream->Scalars()->GetNumberOfTuples(),stream->Scalars()->GetPointer(0));
		nvar++;
	}

	//
	//  Check if we need to add density
	//
	iMonitor h(this,"Operating",1.0);
	if(stream->Extras.DensityVariable>-2 && nvar<stream->NumVariablesInData && stream->Extras.DensityVariable<nvar)
	{

		mDensityEstimator->ComputeDensity(stream->Points(),stream->Scalars(),stream->Extras.DensityVariable,nvar);
		nvar++;
	}

	//
	//  Final check
	//
	if(nvar != stream->NumVariablesInData)
	{
		IBUG_ERROR("iParticleFileLoader is configured icorrectly: unfilled variables remained.");
		h.PostError("Unable to read the data due to a bug.");
	}
}


void iParticleFileLoader::WrapPositions(vtkPoints *points)
{
	int i;
	vtkIdType l, loff, ntot = points->GetNumberOfPoints();

	//
	//  Boundary condition
	//
	for(i=0; i<3; i++) this->SetDirectionPeriodic(i,true);

	if(this->GetBoundaryConditions() == iParameter::BoundaryConditions::Periodic)
	{
		if(points->GetDataType() == VTK_FLOAT)
		{
			float *xptrF = (float *)points->GetVoidPointer(0);
			for(i=0; i<3; i++) if(this->IsDirectionPeriodic(i))
			{
				for(l=0; l<ntot; l++)
				{
					loff = i + 3*l;
					xptrF[loff] -= 2.0*floor(0.5*(1.0+xptrF[loff]));
				}
			}
		}
		else if(points->GetDataType() == VTK_DOUBLE)
		{
			double *xptrD = (double *)points->GetVoidPointer(0);
			for(i=0; i<3; i++) if(this->IsDirectionPeriodic(i))
			{
				for(l=0; l<ntot; l++)
				{
					loff = i + 3*l;
					xptrD[loff] -= 2.0*floor(0.5*(1.0+xptrD[loff]));
				}
			}
		}
		else
		{
			IBUG_ERROR("Internal bug: ivalid points data type.");
		}
	}
}


void iParticleFileLoader::LoadFileBody(const iString &suffix, const iString &fname)
{
	int i;

	iMonitor h(this);
	h.SetInterval(0.0,0.8);

	this->LoadParticleFileBody(suffix,fname);
	if(h.IsStopped()) return;

	h.SetInterval(0.8,0.2);

	//
	//  Apply extras
	//
	for(i=0; i<this->NumStreams(); i++)
	{
		this->WrapPositions(this->GetStream(i)->Points());
		this->AddExtras(this->GetStream(i));
	}
		
	for(i=0; i<this->NumStreams(); i++)
	{
		this->AttachDataToStream(i,this->GetStream(i)->Data);
	}

	for(i=0; i<this->NumStreams(); i++)
	{
		this->FinalizePolyData(this->GetStream(i)->Data);
	}
}


void iParticleFileLoader::ShiftDataBody(vtkDataSet *data, const double *dx)
{
	int i;
	iMonitor h(this,"Shifting",1.0);
	for(i=0; i<3; i++) 
	{
		h.SetInterval(i/3.0,1.0/3.0);
		if(fabs(dx[i]) > 1.0e-100)
		{
			this->ShiftPolyData(iRequiredCast<vtkPolyData>(INFO,data),i,dx[i]);
		}
	}
}


void iParticleFileLoader::FinalizePolyData(vtkPolyData *data)
{
	if(data==0 || data->GetPoints()==0) return;

	vtkIdType l, ntot = data->GetNumberOfPoints();

	if(mHaveNormals)
	{
		vtkFloatArray *newNormals;
		newNormals = vtkFloatArray::New(); IERROR_CHECK_MEMORY(newNormals);
		newNormals->SetNumberOfComponents(3);
		// Allocates and Sets MaxId
		newNormals->SetNumberOfTuples(ntot);
		float *p = (float *)newNormals->GetVoidPointer(0);
		if(p != 0)
		{
			for(l=0; l<ntot; l++)
			{
				p[3*l+0] = p[3*l+1] = 0.0f;
				p[3*l+2] = 1.0f;
			}
			data->GetPointData()->SetNormals(newNormals);
		}
		newNormals->Delete();
	}
}


void iParticleFileLoader::ShiftPolyData(vtkPolyData *data, int d, double dx)
{
	if(data==0 || data->GetPoints()==0 || d<0 || d>2) return;

	vtkIdType n = data->GetPoints()->GetNumberOfPoints();
	float *pf = (float *)data->GetPoints()->GetVoidPointer(0);
	double *pd = (double *)data->GetPoints()->GetVoidPointer(0);
		
	if(pf!=0 && pd!=0)
	{
		mHelper->ShiftData(data->GetPoints()->GetDataType()==VTK_FLOAT,d,n,mPeriodic,dx,pf,pd);
		data->Modified();
	}
}


bool iParticleFileLoader::ReadPositions(iFile &F, int inNum, int outOffset, double updateStart, double updateDuration, float *scale, float *offset)
{
	return mHelper->ReadFortranRecordWithMaskTemplate<float,float>(F,0,inNum,outOffset,updateStart,updateDuration,scale,offset);
}


bool iParticleFileLoader::ReadPositions(iFile &F, int inNum, int outOffset, double updateStart, double updateDuration, double *scale, double *offset)
{
	return mHelper->ReadFortranRecordWithMaskTemplate<double,double>(F,0,inNum,outOffset,updateStart,updateDuration,scale,offset);
}


bool iParticleFileLoader::ReadVariables(iFile &F, int inNum, int outOffset, double updateStart, double updateDuration, float *scale)
{
	return mHelper->ReadFortranRecordWithMaskTemplate<float,float>(F,1,inNum,outOffset,updateStart,updateDuration,scale,(float *)0);
}


bool iParticleFileLoader::ReadIntVariables(iFile &F, int inNum, int outOffset, double updateStart, double updateDuration)
{
	return mHelper->ReadFortranRecordWithMaskTemplate<float,int>(F,1,inNum,outOffset,updateStart,updateDuration,(float *)0,(float *)0);
}


void iParticleFileLoader::ConfigureStreams(vtkIdType *ntot, int *nvar, bool paf)
{
	int n;
	ParticleStream *stream;

	for(n=0; n<this->NumStreams(); n++)
	{
		stream = this->GetStream(n);

		if(stream->Data == 0)
		{
			stream->Data = vtkPolyData::New(); IERROR_CHECK_MEMORY(stream->Data);
		}

		vtkIdType nsel = mIterator.CreateOneMask(n,ntot[n],stream->Included?stream->DownsampleFactor:0);
		//
		//  Determine how many extra (derived) variables we need.
		//
		int nvarIn = nvar[n];
		if(stream->Extras.DensityVariable>-2 && stream->Extras.DensityVariable<nvar[n]) nvar[n]++;
		if(stream->Extras.AddOrderAsVariable) nvar[n]++;

		//
		//  Fill in stream info
		//
		stream->NumTotal = ntot[n];
		stream->NumSelected = nsel;
		stream->NumVariablesInFile = nvarIn;
		stream->NumVariablesInData = nvar[n];
		stream->InFile = true;

		//
		//  Create data arrays
		//
		vtkPoints *oldPoints = stream->Data->GetPoints();
		vtkCellArray *oldVerts = stream->Data->GetVerts();
		vtkFloatArray *oldScalars = iRequiredCast<vtkFloatArray>(INFO,stream->Data->GetPointData()->GetScalars());

		int type = paf ? VTK_FLOAT : VTK_DOUBLE;
		if(oldPoints==0 || oldPoints->GetNumberOfPoints()!=nsel || oldPoints->GetDataType()!=type)
		{
			stream->Data->SetPoints(0); // delete the old data first
			vtkPoints *points = vtkPoints::New(type); IERROR_CHECK_MEMORY(points);
			// Allocates and Sets MaxId
			points->SetNumberOfPoints(nsel);
			stream->Data->SetPoints(points);
			points->Delete();
		}

		if(oldVerts==0 || oldVerts->GetNumberOfCells()!=nsel)
		{
			stream->Data->SetVerts(0); // delete the old data first
			vtkCellArray *verts = vtkCellArray::New(); IERROR_CHECK_MEMORY(verts);
			// This allocates but does not Set Max Id
			verts->Allocate(verts->EstimateSize(nsel,1));
			vtkIdType l;
			for(l=0; l<nsel; l++)
			{
				verts->InsertNextCell(1);
				verts->InsertCellPoint(l);
			}
			stream->Data->SetVerts(verts);
			verts->Delete();
		}

		if(oldScalars==0 || oldScalars->GetNumberOfTuples()!=nsel || oldScalars->GetNumberOfComponents()!=nvar[n])
		{
			stream->Data->GetPointData()->SetScalars(0); // delete the old data first
			if(nvar[n] > 0)
			{
				vtkFloatArray *scalars = vtkFloatArray::New(); IERROR_CHECK_MEMORY(scalars);
				scalars->SetNumberOfComponents(nvar[n]);
				scalars->SetNumberOfTuples(nsel);
				stream->Data->GetPointData()->SetScalars(scalars);
				scalars->Delete();
			}
		}
	}
}


void iParticleFileLoader::AutoScalePositions()
{
	//
	//  Scale positions automatically
	//
	vtkPoints *p = 0;
	int i;
	for(i=0; p==0 && i<this->NumStreams(); i++)
	{
		if(this->GetStream(i)->Data->GetNumberOfPoints() > 0)
		{
			p = this->GetStream(i)->Points();
		}
	}
	if(p == 0) return;

	switch(p->GetDataType())
	{
	case VTK_FLOAT:
		{
			mHelper->AutoScalePositions<float>();
			break;
		}
	case VTK_DOUBLE:
		{
			mHelper->AutoScalePositions<double>();
			break;
		}
	}
}


//
//  Helper class
//
iParticleHelper::iParticleHelper(iParticleFileLoader *loader) : iParallelWorker(loader->GetViewModule()->GetParallelManager())
{
	mLoader = loader; IASSERT(loader);
}


void iParticleHelper::ShiftData(bool paf, int dim, vtkIdType n, bool per[3], double dr, float *pf, double *pd)
{
	mPaf = paf;
	mDim = dim;
	mTot = n;
	mDr = dr;
	mFarr = pf;
	mDarr = pd;
	mPeriodic = per;

	iMonitor h(mLoader,"Shifting",1.0);
	this->ParallelExecute(1);
}


void iParticleHelper::AddOrderAsVar(int dim, int com, vtkIdType tot, float *pf)
{
	mDim = dim;
	mCom = com;
	mTot = tot;
	mFarr = pf;

	iMonitor h(mLoader,"Formatting",1.0);
	this->ParallelExecute(2);
}


int iParticleHelper::ExecuteStep(int step, iParallel::ProcessorInfo &p)
{
	vtkIdType l, kstp, kbeg, kend;
	int d = mDim;
	int nc = mCom;

	iParallel::SplitRange(p,mTot,kbeg,kend,kstp);

	switch(step)
	{
	case 1:
		{
			float dr = mDr;

			if(mPaf)
			{
				float *x = mFarr + 3*kbeg;
				for(l=kbeg; l<kend; l++)
				{
					if(l%1000 == 0)
					{
						if(p.IsMaster()) mLoader->GetShell()->GetExecutionEventObserver()->SetProgress(double(l-kbeg)/(kend-kbeg));
						if(mLoader->GetShell()->GetExecutionEventObserver()->IsAborted()) return 2;
					}
					x[d] += 2.0*dr;
					if(mPeriodic[d])
					{
						if(x[d] >  1.0) x[d] -= 2.0;
						if(x[d] < -1.0) x[d] += 2.0;
					}
					x += 3;
				}
			}
			else
			{
				double *x = mDarr + 3*kbeg;
				for(l=kbeg; l<kend; l++)
				{
					if(l%1000 == 0)
					{
						if(p.IsMaster()) mLoader->GetShell()->GetExecutionEventObserver()->SetProgress(double(l-kbeg)/(kend-kbeg));
						if(mLoader->GetShell()->GetExecutionEventObserver()->IsAborted()) return 2;
					}
					x[d] += 2.0*dr;
					if(mPeriodic[d])
					{
						if(x[d] >  1.0) x[d] -= 2.0;
						if(x[d] < -1.0) x[d] += 2.0;
					}
					x += 3;
				}
			}
			return 0;
		}
	case 2:
		{
			float *f = mFarr + mDim;
			for(l=kbeg; l<kend; l++)
			{
				if(l%1000 == 0)
				{
					if(p.IsMaster()) mLoader->GetShell()->GetExecutionEventObserver()->SetProgress(double(l-kbeg)/(kend-kbeg));
					if(mLoader->GetShell()->GetExecutionEventObserver()->IsAborted()) return 2;
				}
				f[nc*l] = l;
			}
			return 0;
		}
	default:
		{
			return 1;
		}
	}
}


template<class T, class D>
bool iParticleHelper::ReadFortranRecordWithMaskTemplate(iFile &F, int dest, int inNum, int outOffset, double updateStart, double updateDuration, T *scale, T *offset)
{
	iParticleDownsampleIterator &it(this->mLoader->mIterator);

	iMonitor h(this->mLoader);
	if(this->mLoader->NumStreams() == 0)
	{
		IBUG_ERROR("Streams must be configured prior to a call to iParticleFileLoader::ReadFortranRecordWithMask.");
		h.PostError("Unable to read the data due to a bug.");
		return false;
	}

	if(this->mLoader->NumStreams() != it.GetNumMasks())
	{
		IBUG_ERROR("Masks must be created prior to a call to iParticleFileLoader::ReadFortranRecordWithMask.");
		h.PostError("Unable to read the data due to a bug.");
		return false;
	}

	//
	//  Is component valid?
	//
	if(inNum<1 || outOffset<0)
	{
		IBUG_ERROR("Invalid call to iParticleFileLoader::ReadFortranRecordWithMask.");
		h.PostError("Unable to read the data due to a bug.");
		return false;
	}

	int j;
	vtkDataArray *arr;
	for(j=0; j<this->mLoader->NumStreams(); j++)
	{
		if(this->mLoader->GetStream(j)->InFile)
		{
			switch(dest)
			{
			case 0:
				{
					arr = this->mLoader->GetStream(j)->Points()->GetData();
					break;
				}
			case 1:
				{
					arr = this->mLoader->GetStream(j)->Scalars();
					break;
				}
			default:
				{
					arr = 0;
				}
			}
			if((arr==0 && it.GetNumSelected(j)>0) || !it.AttachBuffer(j,arr,outOffset))
			{
				IBUG_ERROR("Invalid call to iParticleFileLoader::ReadFortranRecordWithMask.");
				return false;
			}
			it.SkipMask(j,false);
		}
		else it.SkipMask(j,true);
	}

	//
	//  Read the header
	//
	vtkIdType lrec1, lrec2;
	if(!this->mLoader->ReadFortranHeaderFooter(F,lrec1))
	{
		h.PostError("Corrupted data.");
		return false;
	}

	//
	//  Find # of components in the record
	//
	int nCompRec = lrec1/((vtkIdType)sizeof(T)*it.GetNumGlobalTotal());
	if(lrec1 != nCompRec*sizeof(T)*it.GetNumGlobalTotal())
	{
		h.PostError("Corrupted data.");
		return false;
	}

	//
	// Decide how to read
	//
	vtkIdType l, lpiece1, lpiece = it.GetNumGlobalTotal()/1000;
	if(lpiece < 1000) lpiece = 1000;
	int npieces = (it.GetNumGlobalTotal()+lpiece-1)/lpiece;

	//
	//  Create tmp array
	//
	T *ptr, *ptmp;
	D *d = new D[lpiece*nCompRec];
	if(d == 0) 
	{ 
		h.PostError("Not enough memory to create the data.");
		return false;
	}

	//
	//  parameters for the Progress Bar
	//
	updateDuration /= npieces;

	//
	//  Read piece by piece
	//
	int i;
	it.Start();
	for(j=0; j<npieces; j++)
	{
		if(j < npieces-1)
		{
			lpiece1 = lpiece;
		}
		else
		{
			//
			//  Correct for the last record
			//
			lpiece1 = it.GetNumGlobalTotal() - j*lpiece;
		}
		if(!this->mLoader->ReadBlock(F,d,inNum*lpiece1,updateStart+j*updateDuration,updateDuration))
		{
			h.PostError("Corrupted data.");
			delete [] d;
			return false;
		}
		if(h.IsAborted())
		{
			delete [] d;
			return false;
		}
		for(l=0; l<lpiece1; l++) if(it.IsSelected())
		{
			//
			//  Direct access to data
			//
			ptr = (T *)it.BufferPtr();
			for(i=0; i<inNum; i++)
			{
				ptr[i] = d[nCompRec*l+i];
			}
		}
	}
	it.Stop();

	delete [] d;

	//
	//  Read the footer
	//
	if(!this->mLoader->ReadFortranHeaderFooter(F,lrec2) || lrec1!=lrec2)
	{
		h.PostError("Corrupted data.");
		return false;
	}

	//
	//  Do we need to scale?
	//
	int nCompArr;
	vtkIdType nSizeArr;
	if(scale != 0)
	{
		if(offset != 0)
		{
			//
			//  Scale as positions
			//
			for(j=0; j<it.GetNumMasks(); j++)
			{
				ptr = (T *)it.GetBuffer(j);
				nSizeArr = it.GetNumSelected(j);
				nCompArr = it.GetBufferWidth(j);
				for(l=0; l<nSizeArr; l++)
				{
					ptmp = ptr + nCompArr*l;
					for(i=0; i<inNum; i++)
					{
						ptmp[i] = -1.0 + scale[i]*(ptmp[i]-offset[i]);
					}
				}
			}
		}
		else
		{
			//
			//  Just scale
			//
			for(j=0; j<it.GetNumMasks(); j++)
			{
				ptr = (T *)it.GetBuffer(j);
				nSizeArr = it.GetNumSelected(j);
				nCompArr = it.GetBufferWidth(j);
				for(l=0; l<nSizeArr; l++)
				{
					ptmp = ptr + nCompArr*l;
					for(i=0; i<inNum; i++)
					{
						ptmp[i] *= scale[i];
					}
				}
			}
		}
	}

	if(h.IsAborted())
	{
		return false;
	}

	return true;
}


template<class T>
void iParticleHelper::AutoScalePositions()
{
	//
	//  Scale positions automatically
	//
	int i, j;
	vtkIdType l, np;
	T min[3], max[3], r = 0.0, *ptr; 
	bool start = true;

	//
	//  Find min/max
	//
	for(j=0; j<this->mLoader->NumStreams(); j++) if(this->mLoader->GetStream(j)->Data->GetNumberOfPoints() > 0)
	{
		ptr = (T *)this->mLoader->GetStream(j)->Points()->GetVoidPointer(0);
		if(ptr == 0) continue;

		if(start)
		{
			start = false;
			for(i=0; i<3; i++)
			{
				min[i] = ptr[i];
				max[i] = ptr[i];
			}
		}

		np = this->mLoader->GetStream(j)->Points()->GetNumberOfPoints();
		for(l=0; l<np; l++)
		{
			for(i=0; i<3; i++)
			{
				if(ptr[3*l+i] < min[i]) min[i] = ptr[3*l+i];
				if(ptr[3*l+i] > max[i]) max[i] = ptr[3*l+i];
			}
		}

		for(i=0; i<3; i++)
		{
			if(r < (max[i]-min[i])) r = max[i] - min[i];
		}
	}

	if(!(r > 0.0)) return;

	r = 2.0/r;

	for(j=0; j<this->mLoader->NumStreams(); j++) if(this->mLoader->GetStream(j)->Data->GetNumberOfPoints() > 0)
	{
		ptr = (T *)this->mLoader->GetStream(j)->Points()->GetVoidPointer(0);
		if(ptr == 0) continue;

		np = this->mLoader->GetStream(j)->Points()->GetNumberOfPoints();
		for(l=0; l<np; l++)
		{
			for(i=0; i<3; i++)
			{
				ptr[3*l+i] = -1.0 + r*(ptr[3*l+i]-min[i]);
			}
		}
	}
}


//
//  Out stream
//
iParticleFileLoader::ParticleStream::ParticleStream()
{
	Included = InFile = true;
	DownsampleFactor = 1;
	NumVariablesInFile = NumVariablesInData = 0;
	NumSelected = 0;
	Data = 0;
}


iParticleFileLoader::ParticleStream::~ParticleStream()
{
	if(Data != 0) Data->Delete();
}


vtkPoints* iParticleFileLoader::ParticleStream::Points() const
{
	IASSERT(Data);
	vtkPoints *p = Data->GetPoints(); IASSERT(p);
	return p;
}


vtkCellArray* iParticleFileLoader::ParticleStream::Verts() const
{
	IASSERT(Data);
	vtkCellArray *c = Data->GetVerts(); IASSERT(c);
	return c;
}


vtkFloatArray* iParticleFileLoader::ParticleStream::Scalars() const
{
	IASSERT(Data);
	IASSERT(Data->GetPointData());
	return vtkFloatArray::SafeDownCast(Data->GetPointData()->GetScalars());
}


iParticleFileLoader::Stream* iParticleFileLoader::CreateNewStream() const
{
	return new ParticleStream;
}


iParticleFileLoader::ParticleStreamExtras::ParticleStreamExtras()
{
	DensityVariable = -2;
	AddOrderAsVariable = false;
}
