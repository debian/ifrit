/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ishelleventobservers.h"


#include "ierror.h"
#include "ioutputchannel.h"
#include "iparallel.h"
#include "ishell.h"

#include <vtkAlgorithm.h>

//
//  Templates
//
#include "iarray.tlh"

//#define DEBUG_THIS

#if defined(I_DEBUG) && defined(DEBUG_THIS)
#include "isystem.h"
#include <vtkTimerLog.h>
#endif


using namespace iParameter;


//
//  iExecutionEventObserver class
//
iExecutionEventObserver* iExecutionEventObserver::New()
{
	IBUG_FATAL("This function should never be called.");
	return 0;
}


iExecutionEventObserver::iExecutionEventObserver(iShell *s) : iEventObserver(), iShellComponent(s)
{
	mAborted = mInError = false;
}


void iExecutionEventObserver::ExecuteBody(vtkObject *caller, unsigned long event, void *data)
{
	switch(event)
	{
	case vtkCommand::StartEvent:
		{
			this->Start();
			break;
		}
	case vtkCommand::ProgressEvent:
		{
			double f;
			if(data == 0) f = 0.0; else f = static_cast<double*>(data)[0];
			this->SetProgress(f);
			break;
		}
	case vtkCommand::AbortCheckEvent:
		{
			if(mAborted || mInError)
			{
				vtkAlgorithm *alg = vtkAlgorithm::SafeDownCast(caller);
				if(alg != 0) alg->SetAbortExecute(1);
			}
			break;
		}
	case vtkCommand::EndEvent:
		{
			this->Finish();
			break;
		}
	}
}


void iExecutionEventObserver::Start()
{
	if(mAborted || mInError)
	{
		IBUG_WARN("Incorrect order of calls to iExecutionEventObserver.");
	}

	if(mStack.Size() == 0)
	{
		this->OnStart();
#if defined(I_DEBUG) && defined(DEBUG_THIS)
		prev = 0.0;
#endif
	}

	Item item;
	mStack.Add(item);

#if defined(I_DEBUG) && defined(DEBUG_THIS)
	if(mStack.Size() < 10) time[mStack.Size()] = vtkTimerLog::GetUniversalTime();
	iOutput::DisplayDebugMessage("Progress("+iString::FromNumber(mStack.Size())+"): start");
#endif
}


void iExecutionEventObserver::Finish()
{
	if(mStack.Size() == 0)
	{
		IBUG_WARN("Incorrect order of calls to iExecutionEventObserver.");
		return;
	}

#if defined(I_DEBUG) && defined(DEBUG_THIS)
	int i;
	double min = 0,max = 1;
	for(i=mStack.Size()-1; i>=0; i--)
	{
		min = mStack[i].Start + mStack[i].Length*min;
		max = mStack[i].Start + mStack[i].Length*max;
	}
	iOutput::DisplayDebugMessage("Progress("+iString::FromNumber(mStack.Size())+"): '"+label+"' ["+iString::FromNumber(mStack.Last().Start)+","+iString::FromNumber(mStack.Last().Start+mStack.Last().Length)+"] "+" ["+iString::FromNumber(min)+","+iString::FromNumber(max)+"] finish t="+iString::FromNumber(vtkTimerLog::GetUniversalTime()-time[mStack.Size()]));
#endif

	mAborted = false;
	Item last = mStack.RemoveLast();

	if(last.Start+last.Length > 1.001)
	{
		IBUG_WARN("Overflowing last segment call to iExecutionEventObserver at level "+iString::FromNumber(mStack.Size()+1)+".");
	}
	
	if(mStack.Size() == 0)
	{
		this->OnFinish();

		if(mInError) // error flag could have been cleared, in which case we ignore all error messages
		{
			this->OutputText(MessageType::Error,last.ErrorMessages);
			mInError = false;
		}
	}
	else
	{
		//
		//  Pass the error messages if present
		//
		if(!last.ErrorMessages.IsEmpty())
		{
			mStack.Last().ErrorMessages += last.ErrorMessages;
		}
	}
}


void iExecutionEventObserver::SetInterval(double start, double length)
{
	if(mStack.Size() == 0)
	{
		IBUG_WARN("Incorrect order of calls to iExecutionEventObserver.");
		return;
	}

#ifdef I_DEBUG
	if(start>0.0 && fabs(start-mStack.Last().Start-mStack.Last().Length)>1.0e-10)
	{
		IBUG_WARN("Incorrect segment ordering in iExecutionEventObserver: "+label+", previous segment=["+iString::FromNumber(mStack.Last().Start)+","+iString::FromNumber(mStack.Last().Start+mStack.Last().Length)+"] is not adjacent to the new segment=["+iString::FromNumber(start)+","+iString::FromNumber(length)+"].");
	}
#endif

	if(length>0.0 && start+length<1.001)
	{
		mStack.Last().Start = start;
		mStack.Last().Length = length;
		mStack.Last().PrevValue = 0.0;

#if defined(I_DEBUG) && defined(DEBUG_THIS)
		iOutput::DisplayDebugMessage("Progress("+iString::FromNumber(mStack.Size())+"): '"+label+"' ["+iString::FromNumber(mStack.Last().Start)+","+iString::FromNumber(mStack.Last().Start+mStack.Last().Length)+"] segment");
#endif

		this->SetProgress(0.0);
	}
#ifdef I_DEBUG
	else
	{
		IBUG_WARN("Incorrect segment ordering in iExecutionEventObserver: "+label+"["+iString::FromNumber(mStack.Last().Start)+","+iString::FromNumber(mStack.Last().Start+mStack.Last().Length)+"] + "+iString::FromNumber(length)+" > 1.");
	}
#endif
}


void iExecutionEventObserver::PostError(const iString &text)
{
	if(!text.IsEmpty())
	{
		mInError = true;
		mStack.Last().ErrorMessages += (text+"\n");
	}
}


const iString& iExecutionEventObserver::LastErrorMessage() const
{
	return mStack.Last().ErrorMessages;
}


void iExecutionEventObserver::Reset()
{
	mInError = false;
	mStack.Last().PrevValue = 0.0;

	int i;
	for(i=0; i<mStack.Size(); i++)
	{
		mStack[i].ErrorMessages.Clear();
	}
#if defined(I_DEBUG) && defined(DEBUG_THIS)
	prev = 0.0;
#endif
}


void iExecutionEventObserver::SetProgress(double f)
{
	if(mStack.Size()==0 || mStack.Last().Length==0.0)
	{
		IBUG_WARN("Incorrect order of calls to iExecutionEventObserver.");
		return;
	}

	if(!mAborted && !mInError && ((mStack.Last().PrevValue<1.0 && f==1.0) || f>mStack.Last().PrevValue+0.00099))
	{
		//
		//  Unwound the stack (can be optimized, but is not worth it)
		//
		int i;
		for(i=mStack.Size()-1; i>=0; i--)
		{
			mStack[i].PrevValue = f;
			f = mStack[i].Start + mStack[i].Length*f;
		}

		this->OnProgress(f,mAborted);
#if defined(I_DEBUG) && defined(DEBUG_THIS)
		/*
		double min = 0, max = 1;
		for(i=mStack.Size()-1; i>=0; i--)
		{
			min = mStack[i].Start + mStack[i].Length*min;
			max = mStack[i].Start + mStack[i].Length*max;
		}
		iOutput::DisplayDebugMessage("Progress("+iString::FromNumber(mStack.Size())+"): '"+label+"' ["+iString::FromNumber(mStack.Last().Start)+","+iString::FromNumber(mStack.Last().Start+mStack.Last().Length)+"] "+" ["+iString::FromNumber(min)+","+iString::FromNumber(max)+"] "+iString::FromNumber(f));
		*/
		if(f<0.0 || f>1.0)
		{
			IBUG_WARN("Out of range call.");
		}
		if(f < prev)
		{
			IBUG_WARN("Out of order call.");
		}
		prev = f;
#endif
	}
}


void iExecutionEventObserver::SetLabel(const iString& s)
{
#ifdef I_DEBUG
	label = s;
#endif
	this->OnSetLabel(s);
}


iExecutionEventObserver::Item::Item()
{
	Start = Length = PrevValue = 0.0;
}


//
//  iParallelUpdateEventObserver class
//
iParallelUpdateEventObserver* iParallelUpdateEventObserver::New()
{
	IBUG_FATAL("This function should never be called.");
	return 0;
}


iParallelUpdateEventObserver::iParallelUpdateEventObserver(iShell *s) : iEventObserver(), iShellComponent(s)
{
}


void iParallelUpdateEventObserver::ExecuteBody(vtkObject *, unsigned long eventId, void *)
{
	if(eventId == iParallel::InformationEvent) this->UpdateInformation(); 
}

