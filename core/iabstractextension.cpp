/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iabstractextension.h"


#include "icrosssectionviewsubject.h"
#include "ierror.h"
#include "iobjectfactory.h"
#include "iparticleviewsubject.h"
#include "isurfaceviewsubject.h"
#include "itensorfieldviewsubject.h"
#include "ivectorfieldviewsubject.h"
#include "ivolumeviewsubject.h"


int iAbstractExtension::DefineToId(int n)
{
	int id = -1;
	unsigned int k = (unsigned int)n;
	while(k != 0U)
	{
		id++;
		k = k >> 1;
	}
	return id;
}


iAbstractExtension::iAbstractExtension(int n, const iString& name) : mId(iAbstractExtension::DefineToId(n)), mName(name), mSubjectCounter(0)
{
	//
	//  Check that we configured correctly
	//
	if(n != (1<<mId))
	{
		IBUG_FATAL("Incorrectly configured extension.")
	}

	iObjectFactory::AttachExtension(this);
}


iCrossSectionViewSubject* iAbstractExtension::CreateCrossSectionSubject(iViewObject *, int) const
{
	return 0;
}


iParticleViewSubject* iAbstractExtension::CreateParticleSubject(iViewObject *, int) const
{
	return 0;
}


iSurfaceViewSubject* iAbstractExtension::CreateSurfaceSubject(iViewObject *, int) const
{
	return 0;
}


iTensorFieldViewSubject* iAbstractExtension::CreateTensorFieldSubject(iViewObject *, int) const
{
	return 0;
}


iVectorFieldViewSubject* iAbstractExtension::CreateVectorFieldSubject(iViewObject *, int) const
{
	return 0;
}


iVolumeViewSubject* iAbstractExtension::CreateVolumeSubject(iViewObject *, int) const
{
	return 0;
}


iViewObject* iAbstractExtension::CreateSpecialObject(iViewModule*, const iString &) const
{
	return 0;
}


iPickerExtension* iAbstractExtension::CreatePickerExtension(iPicker * ) const
{
	return 0;
}


iViewModuleExtension* iAbstractExtension::CreateViewModuleExtension(iViewModule * ) const
{
	return 0;
}




//
// Creator helpers
//
iCrossSectionViewSubject* iAbstractExtension::NewCrossSectionSubject(iViewObject *obj, const iDataType& type) const
{
	return iCrossSectionViewSubject::New(obj,type);
}


iParticleViewSubject* iAbstractExtension::NewParticleSubject(iViewObject *obj, const iDataType& type, const iColor& color) const
{
	return iParticleViewSubject::New(obj,type,color);
}


iSurfaceViewSubject* iAbstractExtension::NewSurfaceSubject(iViewObject *obj, const iDataType& type) const
{
	return iSurfaceViewSubject::New(obj,type);
}


iTensorFieldViewSubject* iAbstractExtension::NewTensorFieldSubject(iViewObject *obj, const iDataType& type, const iDataType& stype) const
{
	return iTensorFieldViewSubject::New(obj,type,stype);
}


iVectorFieldViewSubject* iAbstractExtension::NewVectorFieldSubject(iViewObject *obj, const iDataType& type, const iDataType& stype) const
{
	return iVectorFieldViewSubject::New(obj,type,stype);
}


iVolumeViewSubject* iAbstractExtension::NewVolumeSubject(iViewObject *obj, const iDataType& type) const
{
	return iVolumeViewSubject::New(obj,type);
}

