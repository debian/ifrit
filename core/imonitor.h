/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


//
//  A helper class that monitors execution of many IFrIT components
//  (starts and finishes execution observer automatically, reports
//  error and abort statuses, etc). It is created by value as a 
//  local variable, with most of th work done by constructor  and
//  destructor as the monitor goes in and out of scope.
//
#ifndef IMONITOR_H
#define IMONITOR_H


class iExecutionEventObserver;
class iShellComponent;
class iString;


class iMonitor
{

public:

	iMonitor(const iShellComponent *user);
	iMonitor(const iShellComponent *user, const iString& label, double length);
	~iMonitor();

	void SetInterval(const iString& label, double start, double length);
	void SetInterval(double start, double length);
	void SetProgress(double prog);

	void PostError(const iString &text);
	void Reset();

	const iString& LastErrorMessage() const;

	bool IsStopped() const;
	bool IsAborted() const;

private:

	iExecutionEventObserver* mObserver;
};

#endif  // IMONITOR_H

