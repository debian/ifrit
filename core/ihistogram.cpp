/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ihistogram.h"


#include "ierror.h"
#include "imath.h"
#include "istretch.h"

#include <string.h>

//
//  Templates
//
#include "iarray.tlh"


iHistogram::iHistogram()
{
	this->Clear();
}


iHistogram::~iHistogram()
{
}


const iHistogram& iHistogram::Null()
{
	static iHistogram null;
	return null;
}


void iHistogram::Clear()
{
	mValues.Clear();
	mBins.Clear();
	mBins.Add(0);

	mStretchId = iStretch::Lin;
	mStretchedMin = 0;
	mStretchedMax = 0;
	mValueToBinScale = 0;
}


void iHistogram::Reset(int nbins, float min, float max, int sid)
{
	int i;

	if(nbins<1 || min>=max || sid<0 || sid>=iStretch::Count)
	{
		this->Clear();
		return;
	}

	mBins.Resize(nbins+1);
	mBins[0] = min;
	mBins[nbins] = max;
	mStretchId = sid;

	mStretchedMin = iStretch::Apply(min,sid,false);
	mStretchedMax = iStretch::Apply(max,sid,true);
	float s = (mStretchedMax-mStretchedMin)/nbins;
	for(i=1; i<nbins; i++)
	{
		mBins[i] = iStretch::Reset(mStretchedMin+s*i,sid);
	}
	mValueToBinScale = 0.99999f*nbins/(mStretchedMax-mStretchedMin);

	mValues.Resize(nbins);
	memset((void *)mValues.Data(),0,nbins*sizeof(float));
}


int iHistogram::Bin(float v) const
{
#ifdef I_CHECK
	this->Check();
#endif
	if(mValues.Size()==0 || v<mBins[0] || v>mBins[mValues.Size()]) return -1;
	return int(mValueToBinScale*(iStretch::Apply(v,mStretchId,false)-mStretchedMin));
}


float iHistogram::GetMin() const
{
#ifdef I_CHECK
	this->Check();
#endif
	return mBins[0];
}


float iHistogram::GetMax() const
{
#ifdef I_CHECK
	this->Check();
#endif
	return mBins[mValues.Size()];
}


void iHistogram::GetRange(float range[2]) const
{
#ifdef I_CHECK
	this->Check();
#endif
	range[0] = mBins[0];
	range[1] = mBins[mValues.Size()];
}


float iHistogram::GetBinMin(int i) const
{
#ifdef I_CHECK
	this->Check();
#endif
	if(i>=0 && i<mValues.Size())
	{
		return mBins[i];
	}
	else return 0.0;
}


float iHistogram::GetBinMax(int i) const
{
#ifdef I_CHECK
	this->Check();
#endif
	if(i>=0 && i<mValues.Size())
	{
		return mBins[i+1];
	}
	else return 0.0;
}


void iHistogram::GetBinRange(int i, float range[2]) const
{
#ifdef I_CHECK
	this->Check();
#endif
	if(i>=0 && i<mValues.Size())
	{
		range[0] = mBins[i];
		range[1] = mBins[i+1];
	}
}


float iHistogram::Value(int i) const
{
#ifdef I_CHECK
	this->Check();
#endif
	if(i>=0 && i<mValues.Size())
	{
		return mValues[i];
	}
	else return -1;
}


#ifdef I_CHECK
void iHistogram::Check() const
{
	if(mValues.Size()+1 != mBins.Size())
	{
		IBUG_FATAL("Incorrectly configure iHistogram");
	}
}
#endif
