/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "icubeaxesactor.h"


#include "iaxis.h"
#include "ierror.h"
#include "ioverlayhelper.h"
#include "irendertool.h"
#include "iviewmodule.h"

#include <vtkAxisActor2D.h>
#include <vtkProperty2D.h>
#include <vtkRenderer.h>
#include <vtkTextMapper.h>
#include <vtkTextProperty.h>
#include <vtkViewport.h>

//
//  Templates
//
#include "iarray.tlh"
#include "igenericprop.tlh"


iCubeAxesActor* iCubeAxesActor::New(iRenderTool *rt)
{
	IASSERT(rt);
	return new iCubeAxesActor(rt);
}


iCubeAxesActor::iCubeAxesActor(iRenderTool *rt) : iGenericProp<vtkCubeAxesActor2D>(true), mOverlayHelper(rt)
{
	//
	//  Replace axes
	//
	this->XAxis->Delete();
	this->YAxis->Delete();
	this->ZAxis->Delete();

	this->XAxis = iAxis::New(rt);
	this->XAxis->GetPositionCoordinate()->SetCoordinateSystemToDisplay();
	this->XAxis->GetPosition2Coordinate()->SetCoordinateSystemToDisplay();
	this->XAxis->AdjustLabelsOff();

	this->YAxis = iAxis::New(rt);
	this->YAxis->GetPositionCoordinate()->SetCoordinateSystemToDisplay();
	this->YAxis->GetPosition2Coordinate()->SetCoordinateSystemToDisplay();
	this->YAxis->AdjustLabelsOff();

	this->ZAxis = iAxis::New(rt);
	this->ZAxis->GetPositionCoordinate()->SetCoordinateSystemToDisplay();
	this->ZAxis->GetPosition2Coordinate()->SetCoordinateSystemToDisplay();
	this->ZAxis->AdjustLabelsOff();

	this->XAxis->SetLabelTextProperty(this->AxisLabelTextProperty);
	this->YAxis->SetLabelTextProperty(this->AxisLabelTextProperty);
	this->ZAxis->SetLabelTextProperty(this->AxisLabelTextProperty);
	this->XAxis->SetTitleTextProperty(this->AxisTitleTextProperty);
	this->YAxis->SetTitleTextProperty(this->AxisTitleTextProperty);
	this->ZAxis->SetTitleTextProperty(this->AxisTitleTextProperty);

	this->SetFlyModeToOuterEdges();
	this->SetLabelFormat("%6.2g");
	this->ScalingOff();
	float b = 1.0;
	this->SetBounds(-b,b,-b,b,-b,b);
	this->SetNumberOfLabels(5);
	this->UseRangesOn();
	this->GetProperty()->SetColor(0.0,0.0,0.0);
}


iCubeAxesActor::~iCubeAxesActor()
{
}


void iCubeAxesActor::UpdateGeometry(vtkViewport *)
{
	//
	//  Do nothing here, we work entirely by replacing axes
	//
}


int iCubeAxesActor::RenderOpaqueGeometry(vtkViewport *vp)
{
 	int mag = mOverlayHelper->GetRenderingMagnification();

	if(strcmp(this->GetXLabel(),"X")==0 && strcmp(this->GetYLabel(),"Y")==0 && strcmp(this->GetZLabel(),"Z")==0)
	{
		if(mOverlayHelper->GetRenderTool()->GetViewModule()->InOpenGLCoordinates())
		{
			this->SetRanges(-1.0,1.0,-1.0,1.0,-1.0,1.0);
		}
		else 
		{
			float bs = mOverlayHelper->GetRenderTool()->GetViewModule()->GetBoxSize();
			this->SetRanges(0.0,bs,0.0,bs,0.0,bs);
		}
	}

	if(mag == 1)
	{
		return this->vtkCubeAxesActor2D::RenderOpaqueGeometry(vp);
	}
	else
	{
		//
		// Block rebuilding when working under magnification
		//
		int ret = 0;
	
		if(this->XAxisVisibility != 0) ret += this->XAxis->RenderOpaqueGeometry(vp);
		if(this->YAxisVisibility != 0) ret += this->YAxis->RenderOpaqueGeometry(vp);
		if(this->ZAxisVisibility != 0) ret += this->ZAxis->RenderOpaqueGeometry(vp);

		return ret;
	}
}

