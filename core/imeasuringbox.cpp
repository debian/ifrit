/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "imeasuringbox.h"


#include "iactor.h"
#include "ierror.h"
#include "irendertool.h"
#include "itextactor.h"
#include "iviewmodule.h"

#include <vtkCamera.h>
#include <vtkCommand.h>
#include <vtkCubeSource.h>
#include <vtkInteractorStyle.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>

#include <stdio.h>

//
//  Templates
//
#include "iarray.tlh"
#include "igenericprop.tlh"
#include "iproperty.tlh"


using namespace iParameter;


namespace iMeasuringBoxActor_Private
{
	class KeyboardObserver : public vtkCommand
	{

	public:

		vtkTypeMacro(KeyboardObserver,vtkCommand);
		static KeyboardObserver* New(iMeasuringBoxActor *parent = 0)
		{
			IASSERT(parent);
			return new KeyboardObserver(parent);
		}

		virtual void Execute(vtkObject *caller, unsigned long eventId, void *callData)
		{
			vtkInteractorStyle *s = vtkInteractorStyle::SafeDownCast(caller);

			if(eventId==vtkCommand::CharEvent && mParent->GetVisibility()!=0 && mParent->GetViewModule()->GetRenderWindowInteractor()!=0)
			{
				// catch keycodes
				switch(mParent->GetViewModule()->GetRenderWindowInteractor()->GetKeyCode())
				{
				case '-':
				case 'a':
				case 'A':
					{
						mParent->SetSize(mParent->GetSize()*0.95);
						break;
					}
				case '+':
				case 'z':
				case 'Z':
					{
						mParent->SetSize(mParent->GetSize()/0.95);
						break;
					}
				case '<':
				case 's':
				case 'S':
					{
						mParent->SetOpacity(mParent->GetOpacity()*0.8);
						break;
					}
				case '>':
				case 'x':
				case 'X':
					{
						mParent->SetOpacity(mParent->GetOpacity()/0.8);
						break;
					}
				}
				mParent->GetViewModule()->GetRenderWindowInteractor()->Render();
			}
			if(s != 0) s->OnChar();
		}

	private:

		KeyboardObserver(iMeasuringBoxActor *parent) : mParent(parent)
		{
		}

		iMeasuringBoxActor *mParent;
	};
};


using namespace iMeasuringBoxActor_Private;


iMeasuringBoxActor::iMeasuringBoxActor(iViewModule *vm) : iGenericProp<iActor>(true), iViewModuleComponent(vm)
{
	mStarted = false;
	mSize = 0.25;
	mBaseScale = 1.0;

	mFactor1r = 0.78; mFactor1g = 0.89; mFactor1b = 0.82; mFactor2 = 0.0;

	vtkCubeSource *cube = vtkCubeSource::New(); IERROR_CHECK_MEMORY(cube);
	cube->SetCenter(0.0,0.0,0.0);
	cube->SetXLength(2.0);
	cube->SetYLength(2.0);
	cube->SetZLength(2.0);

	mWorkerActor = iActor::New(); IERROR_CHECK_MEMORY(mWorkerActor);
	this->AppendComponent(mWorkerActor);
	
	mBoxActor = mWorkerActor;
	mFrameActor = this;

	mFrameActor->SetInputConnection(cube->GetOutputPort());
	mFrameActor->GetProperty()->SetRepresentationToWireframe();
	mFrameActor->GetProperty()->SetLineWidth(2);

	mBoxActor->SetInputConnection(cube->GetOutputPort());
	mBoxActor->GetProperty()->SetOpacity(0.5);
	mBoxActor->GetProperty()->SetAmbient(0.5);
	mBoxActor->GetProperty()->SetDiffuse(0.5);
	mBoxActor->GetProperty()->SetSpecular(0.7);
	mBoxActor->GetProperty()->SetSpecularPower(50);

	cube->Delete();

	mText = iTextActor::New(this->GetViewModule()->GetRenderTool()); IERROR_CHECK_MEMORY(mText);
	this->AppendComponent(mText);
	mText->SetBold(true);
	mText->SetPosition(0.5,0.03);
	mText->SetJustification(0);

	iColor black;
	this->SetColor(black);

	mObserver = KeyboardObserver::New(this); 

}


iMeasuringBoxActor::~iMeasuringBoxActor()
{
	mObserver->Delete();

	mWorkerActor->Delete();
	mText->Delete();
}


void iMeasuringBoxActor::AttachToInteractorStyle(vtkInteractorStyle *s)
{
	if(s != 0) s->AddObserver(vtkCommand::CharEvent,mObserver);
}


void iMeasuringBoxActor::SetSize(float s)
{
	if(s > 0.0)
	{
		mSize = s;
		this->Modified();
	}
}


void iMeasuringBoxActor::SetColor(iColor &c)
{
	mBoxActor->GetProperty()->SetColor(mFactor1r+mFactor2*c.ToVTK()[0],mFactor1g+mFactor2*c.ToVTK()[1],mFactor1b+mFactor2*c.ToVTK()[2]);
	mFrameActor->GetProperty()->SetColor(c.ToVTK());
	this->Modified();
}

	
void iMeasuringBoxActor::SetOpacity(float o)
{
	if(o > 0.0)
	{
		if(o > 1.0) o = 1.0;
		mBoxActor->GetProperty()->SetOpacity(o);
		this->Modified();
	}
}

	
void iMeasuringBoxActor::UpdateGeometry(vtkViewport* viewport)
{
	vtkRenderer *ren = vtkRenderer::SafeDownCast(viewport);
	if(ren == 0)
	{
		this->Disable();
		return;
	}

	vtkCamera *cam = ren->GetActiveCamera();
	if(cam == 0)
	{
		this->Disable();
		return;
	}
	
	if(!mStarted)
	{
		mStarted = true;
		mBaseScale = cam->GetParallelScale();
	}
	
	if(this->GetViewModule()->GetRenderTool()->GetRenderingMagnification() == 1)
	{
		float s = mSize*cam->GetParallelScale()/mBaseScale;
		char t[256];
		sprintf(t,"Box size: %6.2g",s);
		
		mText->SetText(t);
		
		this->SetScale(s);

		mBoxActor->SetAxisScale(s,s,s);
		mBoxActor->SetPosition(cam->GetFocalPoint());
		mFrameActor->SetAxisScale(s,s,s);
		mFrameActor->SetPosition(cam->GetFocalPoint());
	}
}


void iMeasuringBoxActor::SetBaseScale(float s)
{
	mStarted = true;
	mBaseScale = s;
}


//
//  Main class
//
iMeasuringBox* iMeasuringBox::New(iViewModule *vm)
{
	static iString LongName("MeasuringBox");
	static iString ShortName("mb");

	IASSERT(vm);
	return new iMeasuringBox(vm,LongName,ShortName);
}


iMeasuringBox::iMeasuringBox(iViewModule *vm, const iString &fname, const iString &sname) : iViewModuleTool(vm,fname,sname)
{
	mActor = new iMeasuringBoxActor(vm); IERROR_CHECK_MEMORY(mActor);
	mActor->VisibilityOff();
	mActor->PickableOff();

	this->GetViewModule()->GetRenderTool()->AddObject(mActor);
}


iMeasuringBox::~iMeasuringBox()
{
	this->GetViewModule()->GetRenderTool()->RemoveObject(mActor);
	mActor->Delete();
}


bool iMeasuringBox::ShowBody(bool s)
{
	mActor->SetVisibility(s?1:0); 
	return true;
}
