/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


//
//  Header file for iCrossSectionViewSubject class
//
#ifndef ICROSSSECTIONVIEWSUBJECT_H
#define ICROSSSECTIONVIEWSUBJECT_H


#include "imultiviewsubject.h"


class iCrossSectionViewSubject;
class iViewObject;

class vtkImageData;
class vtkTexture;


namespace iParameter
{
	namespace SpecialLocation
	{
		const int MaxData =	-4;
		const int MinData =	-3;
	};
};


class iCrossSectionViewInstance : public iMultiViewInstance
{
	
	friend class iCrossSectionViewSubject;

public:
	
	iDataConsumerTypeMacro(iCrossSectionViewInstance,iMultiViewInstance);
		
	iObjectSlotMacro1SV(Method,int);
	iObjectSlotMacro1SV(Direction,int);
	iObjectSlotMacro1SV(SampleRate,int);
	iObjectSlotMacro1SV(InterpolateData,bool);

	virtual bool SetBoxLocation(double p);
	double GetBoxLocation() const;

	void SetLocation(double p);
	void SetLocation(const iCoordinate &p);
	inline double GetLocation() const { return mLocation[mDirection]; }
	
	bool SetToSpecialLocation(int n);

	inline vtkImageData* GetTextureData() const { return mTextureData; }
	
	inline bool GetOverTheEdgeFlag() const { return mOverTheEdgeFlag; }
	int GetActualMethod() const;

	virtual float GetMemorySize();
	virtual void RemoveInternalData();	

protected:
	
	iCrossSectionViewInstance(iCrossSectionViewSubject *owner);
	virtual ~iCrossSectionViewInstance();
	virtual void ConfigureBody();
	virtual void FinishInitialization();
	virtual void ConfigureMainPipeline(iViewSubjectPipeline *p, int id);

	virtual void ShowBody(bool s);
	virtual bool CanBeShown() const;

	virtual void UpdatePainting();

	virtual iViewSubjectPipeline* CreatePipeline(int id);
	void UpdateLocation();
	void UpdateTextureSize();
	
	bool mOverTheEdgeFlag;
	iPosition mLocation;
	//
	//  VTK stuff
	//
	vtkTexture *mTexture;
	vtkImageData *mTextureData;
};


class iCrossSectionViewSubject : public iMultiViewSubject 
{
	
	friend class iCrossSectionViewInstance;

public:
	
	iViewSubjectTypeMacro(iCrossSectionView,iMultiViewSubject);

	iType::vsp_int Method;
	iType::vsp_int Direction;
	iType::vsp_int SampleRate;
	iType::vsp_bool InterpolateData;
	iType::vsp_double Location;

	iCrossSectionViewInstance* GetInstance(int i = 0) const;
	static void ForcePolygonalMethod(bool s);

protected:
	
	iCrossSectionViewSubject(iViewObject *parent, const iDataType &type);
	virtual ~iCrossSectionViewSubject();

	virtual void UpdateAutomaticShadingBody();

	static bool mForcePolygonalMethod;
};

#endif // ICROSSSECTIONVIEWSUBJECT_H

