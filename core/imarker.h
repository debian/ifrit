/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IMARKER_H
#define IMARKER_H


#include "imaterialobject.h"
#include "iviewmodulecomponent.h"


#include "iposition.h"
#include "ipropertymultiobject.h"

class iActor;
class iCaption;
class iLegend;
class iMarkerObject;
class iPointGlyph;
class iViewSubject;


class iMarkerInstance : public vtkObjectBase, public iViewModuleComponent
{
	
	friend class iMarkerObject;

public:
	
	vtkTypeMacro(iMarkerInstance,vtkObjectBase);
		
	iObjectSlotMacro1SV(Type,int);
	iObjectSlotMacro1SV(Size,double);
	iObjectSlotMacro1SV(Scaled,bool);
	iObjectSlotMacro1SR(Color,iColor);
	iObjectSlotMacro1SV(Opacity,float);
	iObjectSlotMacro2S(BoxPosition,const iVector3D&);

	iObjectSlotMacro1SR(CaptionText,iString);
	iObjectSlotMacro1SR(CaptionPosition,iPair);

	const iString GetTypeAsString() const;
	void SetPosition(const iPosition &v);
	const iPosition& GetPosition() const { return mPosition; }

	inline iPointGlyph* GetMarkerProp() const { return mProp; }
	inline iCaption* GetMarkerCaption() const { return mCaption; }

protected:
	
	virtual ~iMarkerInstance();

private:

	iMarkerInstance(iMarkerObject *owner);

	iPosition mPosition;

	//
	//  VTK stuff
	//
	iActor *mActor;
	iPointGlyph *mProp;
	iCaption *mCaption;
	iMarkerObject *mOwner;
};


class iMarkerObject : public iMaterialObject, public iViewModuleComponent
{
	
	friend class iMarkerInstance;
	template<class Object, class Instance, iType::TypeId id> friend class iPropertyMultiObject;

public:
	
	vtkTypeMacro(iMarkerObject,iMaterialObject);
	static iMarkerObject* New(iViewModule *parent = 0);
		
	iObjectPropertyMacro2S(Number,Int);
	iPropertyAction Create;
	iPropertyAction1<iType::Int> Remove;

	iPropertyMultiObject<iMarkerObject,iMarkerInstance,iType::Int> Type;
	iPropertyMultiObject<iMarkerObject,iMarkerInstance,iType::Double> Size;
	iPropertyMultiObject<iMarkerObject,iMarkerInstance,iType::Bool> Scaled;
	iPropertyMultiObject<iMarkerObject,iMarkerInstance,iType::Color> Color;
	iPropertyMultiObject<iMarkerObject,iMarkerInstance,iType::Float> Opacity;
	iPropertyMultiObject<iMarkerObject,iMarkerInstance,iType::Vector> Position;

	iPropertyMultiObject<iMarkerObject,iMarkerInstance,iType::String> CaptionText;
	iPropertyMultiObject<iMarkerObject,iMarkerInstance,iType::Pair> CaptionPosition;

	bool ShowLegend(bool s);
	bool IsLegendVisible() const;
	void UpdateLegend();
	iType::ps_bool Legend;
	iObjectPropertyMacro1S(LegendPosition,Int);
	iObjectPropertyMacro2S(TransparentCaptions,Bool);

	iObjectPropertyMacroA(MoveCaptions);

	iMarkerInstance* GetMarker(int i = -1) const;

	void AddUser(iViewSubject *);
	void RemoveUser(iViewSubject *);

protected:
	
	virtual ~iMarkerObject();

	bool CreateInstance();
	bool RemoveInstance(int n);

	iArray<iMarkerInstance*> mInstances;

private:

	iMarkerObject(iViewModule *parent, const iString &fname, const iString &sname);

	void BuildLegend(int i = -2);
	void UpdateEntry(int i);

	void Modified();

	iMarkerObject* Self(){ return this; }  // needed to avoid compiler warning

	iLegend *mLegend;
	iLookupArray<iViewSubject*> mUsers;
};

#endif // IMARKER_H

