/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "imarker.h"


#include "iactor.h"
#include "icamera.h"
#include "icaption.h"
#include "iclipplane.h"
#include "ierror.h"
#include "ilegend.h"
#include "imaterial.h"
#include "ipointglyph.h"
#include "irendertool.h"
#include "iviewmodule.h"
#include "iviewsubject.h"

#include <vtkProperty.h>
#include <vtkProperty2D.h>
#include <vtkTextProperty.h>

//
//  Templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


using namespace iParameter;


iMarkerInstance::iMarkerInstance(iMarkerObject *owner) : iViewModuleComponent(owner->GetViewModule()), mPosition(owner->GetViewModule())
{
	mOwner = owner;

	mSize = 0.08;
	mScaled = true;
	mType = PointGlyphType::Sphere;

	mColor = iColor(255,0,0);
	mOpacity = 1.0;

	//
	//  Do VTK stuff
	//	
	mProp = iPointGlyph::New(); IERROR_CHECK_MEMORY(mProp);
	mProp->SetType(PointGlyphType::Sphere);

	mActor = iActor::New(this->GetViewModule()->GetClipPlane()->GetPlanes()); IERROR_CHECK_MEMORY(mActor);
	mActor->SetScalarVisibility(false);
	mActor->SetPosition(0.0,0.0,0.0);
	mActor->VisibilityOn();
	mActor->DragableOff();  // use Dragable to mark Actors of ViewSubjects
	mActor->GetProperty()->SetOpacity(mOpacity);
	mActor->GetProperty()->SetInterpolationToGouraud();
	mActor->GetProperty()->SetColor(mColor.ToVTK());
	mActor->SetScaled(true);
	mActor->SetInputConnection(mProp->GetOutputPort());
	this->GetViewModule()->AddObject(mActor);
	mOwner->GetMaterial()->AddProperty(mActor->GetProperty());
	
	this->SetSize(mSize);

	mCaption = iCaption::New(this->GetViewModule()->GetRenderTool()); IERROR_CHECK_MEMORY(mCaption);
	this->GetViewModule()->AddObject(mCaption);
	mCaption->VisibilityOff();

	mOwner->Modified();
}


iMarkerInstance::~iMarkerInstance()
{
	this->GetViewModule()->RemoveObject(mCaption);
	mCaption->Delete();

	mOwner->GetMaterial()->RemoveProperty(mActor->GetProperty());
	this->GetViewModule()->RemoveObject(mActor);
	mActor->Delete();

	mProp->Delete();

	mOwner->Modified();
}


bool iMarkerInstance::SetType(int m)
{
	if(m>=0 && m<PointGlyphType::SIZE)
	{
		mProp->SetType(m);
		mType = m;
		return true;
	}
	else return false;
}


bool iMarkerInstance::SetSize(double s)
{
	if(s>0.0 && s<=10.0)
	{
		mSize = s;
		mActor->SetBasicScale(mSize);
		float ps = 10.0*pow(s,0.3);
		if(ps < 1.0) ps = 1.0;
		if(ps > 10.0) ps = 10.0;
		mActor->GetProperty()->SetPointSize(ps);
		return true;
	}
	else return false;
}


bool iMarkerInstance::SetColor(const iColor& c)
{
	if(!c.IsValid()) return false;

	mColor = c;
	mActor->GetProperty()->SetColor(c.ToVTK());
	return true;
}


bool iMarkerInstance::SetOpacity(float op)
{
	if(op>=0.0 && op<=1.0)
	{
		mOpacity = op;
		mActor->GetProperty()->SetOpacity(op);
		return true;
	}
	else return false;
}


bool iMarkerInstance::SetScaled(bool s)
{
	mActor->SetScaled(s);
	mScaled = s;
	return true;
}


bool iMarkerInstance::SetBoxPosition(const iVector3D& v)
{
	mPosition.SetBoxValue(v);
	this->SetPosition(mPosition);
	return true;
}


const iVector3D& iMarkerInstance::GetBoxPosition() const
{
	return mPosition.BoxValue();
}


bool iMarkerInstance::SetCaptionText(const iString& s)
{
	mCaptionText = s;
	mCaption->SetText(s);
	if(s.IsEmpty()) mCaption->VisibilityOff(); else mCaption->VisibilityOn();
	return true;
}


bool iMarkerInstance::SetCaptionPosition(const iPair& p)
{
	mCaptionPosition = p;
	mCaption->SetPosition(p.X,p.Y);
	return true;
}


const iString iMarkerInstance::GetTypeAsString() const
{
	return mProp->GetName();
}


void iMarkerInstance::SetPosition(const iPosition &p)
{
	mPosition = p;
	mActor->SetPosition(mPosition);
	mCaption->SetAttachmentPoint(mPosition);

	mOwner->Modified();
}


//
// Main class
//
iMarkerObject* iMarkerObject::New(iViewModule *parent)
{
	static iString LongName("Marker");
	static iString ShortName("m");

	iMarkerObject *ret = new iMarkerObject(parent,LongName,ShortName); IERROR_CHECK_MEMORY(ret);
	return ret;
}


#define iMarkerObjectPropertyConstructMacroGeneric(_id_,_fname_,_sname_,_setter_,_getter_) \
	_fname_(Self(),static_cast<iPropertyMultiObject<iMarkerObject,iMarkerInstance,iType::_id_>::SetterType>(&iMarkerInstance::_setter_),static_cast<iPropertyMultiObject<iMarkerObject,iMarkerInstance,iType::_id_>::GetterType>(&iMarkerInstance::_getter_),#_fname_,#_sname_)

#define iMarkerObjectPropertyConstructMacro(_id_,_fname_,_sname_) iMarkerObjectPropertyConstructMacroGeneric(_id_,_fname_,_sname_,Set##_fname_,Get##_fname_)


iMarkerObject::iMarkerObject(iViewModule *parent, const iString &fname, const iString &sname) : iMaterialObject(parent,fname,sname,1), iViewModuleComponent(parent),
	Number(iObject::Self(),static_cast<iType::ps_int::SetterType>(&iMarkerObject::SetNumber),static_cast<iType::ps_int::GetterType>(&iMarkerObject::GetNumber),"Number","num",9),
	Create(iObject::Self(),static_cast<iPropertyAction::CallerType>(&iMarkerObject::CreateInstance),"New","new"),
	Remove(iObject::Self(),static_cast<iPropertyAction1<iType::Int>::CallerType>(&iMarkerObject::RemoveInstance),"Delete","dlt"),
	iMarkerObjectPropertyConstructMacro(Int,Type,t),
	iMarkerObjectPropertyConstructMacro(Double,Size,s),
	iMarkerObjectPropertyConstructMacro(Bool,Scaled,sc),
	iMarkerObjectPropertyConstructMacro(Color,Color,c),
	iMarkerObjectPropertyConstructMacro(Float,Opacity,o),
	iMarkerObjectPropertyConstructMacroGeneric(Vector,Position,x,SetBoxPosition,GetBoxPosition),
	iMarkerObjectPropertyConstructMacro(String,CaptionText,ct),
	iMarkerObjectPropertyConstructMacro(Pair,CaptionPosition,cx),
	iObjectConstructPropertyMacroGenericS(Bool,iMarkerObject,Legend,l,ShowLegend,IsLegendVisible),
	iObjectConstructPropertyMacroS(Int,iMarkerObject,LegendPosition,lp),
	iObjectConstructPropertyMacroS(Bool,iMarkerObject,TransparentCaptions,tc),
	iObjectConstructPropertyMacroA(iMarkerObject,MoveCaptions,mc)
{
	mLegendPosition = 0;

	mLegend = iLegend::New(this->GetViewModule()->GetRenderTool()); IERROR_CHECK_MEMORY(mLegend);
	this->SetLegendPosition(mLegendPosition);
	mLegend->VisibilityOff();
	this->GetViewModule()->GetRenderTool()->AddObject(mLegend);
}


iMarkerObject::~iMarkerObject()
{
	//
	//  Legend needs to be deleted before markers - otherwise, markers get stuck somewhere and GarbareCollector crashes.
	//
	this->GetViewModule()->GetRenderTool()->RemoveObject(mLegend);
	mLegend->Delete();
}

	
iMarkerInstance* iMarkerObject::GetMarker(int i) const
{
	if(i>=0 && i<mInstances.Size())
	{
		return mInstances[i];
	}
	else return 0;
}


bool iMarkerObject::CreateInstance()
{
	//
	//  Create a new piece
	//
	iMarkerInstance *m = new iMarkerInstance(this);
	if(m == 0)
	{
		return false;
	}
	m->SetPosition(this->GetViewModule()->GetRenderTool()->GetCamera()->GetFocalPoint());
	mInstances.Add(m);
	this->BuildLegend();
	
	return true;
}


bool iMarkerObject::RemoveInstance(int n)
{
	if(n>=0 && n<mInstances.Size())
	{
		mInstances[n]->Delete();
		mInstances.Remove(n);
		this->BuildLegend();
		return true;
	}
	else return false;
}


bool iMarkerObject::ShowLegend(bool s)
{
	if(s) 
	{
		this->BuildLegend();
		mLegend->VisibilityOn(); 
	}
	else mLegend->VisibilityOff();
	return true;
}


bool iMarkerObject::IsLegendVisible() const
{
	return (mLegend->GetVisibility() != 0);
}


void iMarkerObject::UpdateLegend()
{
	this->BuildLegend(-1);
}


void iMarkerObject::BuildLegend(int n)
{
	int i;
	if(n<0 || n>mInstances.MaxIndex())
	{
		mLegend->SetNumberOfEntries(mInstances.Size());
		for(i=0; i<mInstances.Size(); i++)
		{
			this->UpdateEntry(i);
		}
	}
	else this->UpdateEntry(n);
}


void iMarkerObject::UpdateEntry(int n)
{
	//
	//  Fix a bug in vtkLegendBoxActor
	//
	iMarkerInstance *m = this->GetMarker(n);
	if(m!=0 && m->GetCaptionText().Length()>0)
	{
		mLegend->SetEntry(n,m->GetMarkerProp()->GetData(),m->GetCaptionText().ToCharPointer(),m->GetColor().ToVTK());
	}
	else
	{
		mLegend->SetEntry(n,m->GetMarkerProp()->GetData()," ",m->GetColor().ToVTK());
	}
}


bool iMarkerObject::SetLegendPosition(int p)
{
	switch(p)
	{
	case 0:
		{
			mLegendPosition = 0;
			mLegend->SetPosition(0.73,0.02);
			return true;
		}
	case 1:
		{
			mLegendPosition = 1;
			mLegend->SetPosition(0.02,0.02);
			return true;
		}
	default:
		{
			return false;
		}
	}
}


bool iMarkerObject::GetTransparentCaptions() const
{
	return iCaption::Transparent;
}


bool iMarkerObject::SetTransparentCaptions(bool s)
{
	iCaption::Transparent = s;
	return true;
}


bool iMarkerObject::CallMoveCaptions()
{
	this->GetViewModule()->LaunchInteractorStyleCaption();
	return true;
}


void iMarkerObject::AddUser(iViewSubject *subject)
{
	mUsers.AddUnique(subject);
}


void iMarkerObject::RemoveUser(iViewSubject *subject)
{
	mUsers.Remove(subject);
}


void iMarkerObject::Modified()
{
	int i;

	for(i=0; i<mUsers.Size(); i++)
	{
		mUsers[i]->UpdateOnMarkerChange();
	}
}


int iMarkerObject::GetNumber() const
{
	return mInstances.Size();
}


bool iMarkerObject::SetNumber(int n)
{
	if(n >= 0)
	{
		while(mInstances.Size() > n)
		{
			if(!this->RemoveInstance(mInstances.MaxIndex())) return false;
		}
		while(mInstances.Size() < n)
		{
			if(!this->CreateInstance()) return false;
		}
		return true;
	}
	else return false;
}

