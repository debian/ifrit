/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  vtkPostScriptWriter with different paper formats
//

#ifndef IPOSTSCRIPTWRITER_H
#define IPOSTSCRIPTWRITER_H


#define NFORMATS	11


#include <vtkPostScriptWriter.h>



class iPostScriptWriter : public vtkPostScriptWriter
{

public:

	vtkTypeMacro(iPostScriptWriter,vtkPostScriptWriter);
	static iPostScriptWriter* New(){ return new iPostScriptWriter; }
	
	virtual void SetPaperFormat(int f);
	inline int GetPaperFormat(){ return mFormat; }
	
	virtual void SetOrientation(int o);
	inline int GetOrientation(){ return mOrient; }
	
	static const char* GetPaperFormatName(int n){ if(n>=0 && n<NFORMATS) return mPaper[n].Name; else return ""; }

protected:

	virtual ~iPostScriptWriter();

	virtual void WriteFileHeader(ofstream *os, vtkImageData *data);
	virtual void WriteFile(ofstream *file, vtkImageData *data, int extent[6]);

private:

	struct Paper
	{
		const char *Name;
		float Width, Height;
	};

	iPostScriptWriter();

	int mFormat, mOrient;
	static Paper mPaper[NFORMATS];
};

#endif  // IPOSTSCRIPTWRITER_H


