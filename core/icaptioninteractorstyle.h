/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef ICAPTIONINTERACTORSTYLE_H
#define ICAPTIONINTERACTORSTYLE_H


#include <vtkInteractorStyleTrackballActor.h>
#include "iviewmodulecomponent.h"


class iMarkerInstance;

class vtkActor2D;


class iCaptionInteractorStyle: public vtkInteractorStyleTrackballActor, public iViewModuleComponent
{
	
public:
	
	vtkTypeMacro(iCaptionInteractorStyle,vtkInteractorStyleTrackballActor);
	static iCaptionInteractorStyle* New(iViewModule *vm = 0);
	
	void Launch(vtkInteractorStyle *next);

	void FindPickedActor(int x, int y);
	
	virtual void Rotate(); 
	virtual void OnLeftButtonUp();
	virtual void OnLeftButtonDown();
	
	//
	//  Disable all interactions except left mouse moving the caption around
	//
	virtual void Spin(){} 
	virtual void Pan(){}
	virtual void Dolly(){} 
	virtual void UniformScale(){}
	
	virtual void OnLeave(){ this->Finish(); }
	virtual void OnChar(){ this->Finish(); }
	virtual void OnMiddleButtonDown(){ this->Finish(); }
	virtual void OnMiddleButtonUp(){}
	virtual void OnRightButtonDown(){ this->Finish(); }
	virtual void OnRightButtonUp(){}

protected:
	
	virtual ~iCaptionInteractorStyle();

private:

	iCaptionInteractorStyle(iViewModule *vm);

	void Finish();

	iMarkerInstance *mInteractionMarker;
	vtkActor2D *mScreen;

	vtkInteractorStyle *wNextStyle;
	bool wTransparentCaptions;
};

#endif // ICAPTIONINTERACTORSTYLE_H
