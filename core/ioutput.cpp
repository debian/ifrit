/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ioutput.h"


#include "iarray.h"
#include "ierror.h"
#include "iexception.h"
#include "ifile.h"
#include "imath.h"
#include "ioutputchannel.h"
#include "ishell.h"
#include "istring.h"

#include <vtkOutputWindow.h>

//
//  Templates
//
#include "iarray.tlh"


using namespace iParameter;


namespace iOutput_Private
{
	//
	//  Take over VTK display function to prevent it from displaying extra windows
	//
	class OutputWindow : public vtkOutputWindow
	{

	public:

		vtkTypeMacro(OutputWindow,vtkOutputWindow);

		static OutputWindow* New()
		{
			return new OutputWindow();
		}

		//
		//  Don't overwrite those, they ARE thread-safe
		//
		virtual void DisplayText(const char* message)
		{
			iOutput::Display(MessageType::Information,message);
		}

		virtual void DisplayErrorText(const char* message)
		{
			const char* fm = this->FormattedErrorMessage(message);
			if(fm != 0)
			{
				iOutput::Display(MessageType::Warning,fm);  // display VTK messages as warning to avoid interrupting the rendering stream
			}
		}

		virtual void DisplayWarningText(const char* message)
		{
			const char* fm = this->FormattedErrorMessage(message);
			if(fm != 0)
			{
				iOutput::Display(MessageType::Warning,message);
			}
		}

		virtual void DisplayGenericWarningText(const char* message)
		{
			const char* fm = this->FormattedErrorMessage(message);
			if(fm != 0)
			{
				this->DisplayWarningText(fm);
			}
		}

		virtual void DisplayDebugText(const char* message)
		{
#ifdef I_DEBUG
			this->DisplayText(message);
#endif
		}

	private:

		OutputWindow()
		{
			this->PromptUserOff();
		}

		const char* FormattedErrorMessage(const char* message) const
		{
			//
			//  Block some of the VTK error messages - not all are useful
			//
			static const char *classBlockError[] = { 
				"vtkDecimate",
				"vtkPolyDataNormals",
				"vtkWindowedSincPolyDataFilter",
				"vtkStreamingDemandDrivenPipeline",
				"vtkDataSetToDataSetFilter",
				"vtkOpenGLExtensionManager",
				"vtkStreamingDemandDrivenPipeline"
			};
			static const char *messgBlockError[] = {
				"No data to decimate!",
				"No data to generate normals for!",
				"No data to smooth!",
				"The update extent specified in the information for output port",
				"Changing input type.  Deleting output",
				"Extension GL_VERSION_1_2 could not be loaded.",
				"returned failure for request: vtkInformation"
			};
			static const int nBlockError = sizeof(classBlockError)/sizeof(char*);

			int i;
			for(i=0; i<nBlockError; i++)
			{
				if(strstr(message,classBlockError[i])!=0 && strstr(message,messgBlockError[i])!=0) return 0;
			}

			//
			//  Cut extra parts of VTK error messages
			//
			static const char *markers[] = { "MESSAGE:", "ERROR:", "Warning:" };
			for(i=0; i<sizeof(markers)/sizeof(char*); i++)
			{
				const char *str = strstr(message,markers[i]);
				if(str != 0) message += strlen(markers[i]);
			}

			return message;
		}
	};

	//
	//  Channel registry
	//
	iLookupArray<iOutputChannel*> Stack;

	//
	//  Error log
	//
	iArray<iString> ErrorLog;
};


using namespace iOutput_Private;


void iOutput::Display(int type, const iString &text)
{
	try
	{
		if(Stack.Size() > 0)
		{
			Stack.Last()->Display(type,text);
		}
		else
		{
			//
			//  Send everything to the standard vtkOutputWindow
			//
			iString s = text + "\n";
			if(type == MessageType::Error) vtkOutputWindow::GetInstance()->PromptUserOff();
			vtkOutputWindow::GetInstance()->DisplayText(s.ToCharPointer());
			vtkOutputWindow::GetInstance()->PromptUserOff();
		}
	}
	catch(iException::GlobalExit)
	{
		cerr << "Unable to display output - something is awfully wrong." << endl;
		throw iException::UnableToOutput(); // this exception is not supposed to be caught
		exit(-1);
	}
}


void iOutput::ReportBug(const char* text, const char* file, int line, int severity)
{
	ReportBug(iString(text),file,line,severity);
}


void iOutput::ReportBug(const iString& text, const char* file, int line, int severity)
{
	iString s;
	if(severity >= 0) s += "BUG: ";
	s += text + "\n     FILE: " + file + "\n     LINE: " + iString::FromNumber(line);

	switch(severity)
	{
	case 0:
		{
			s += "\n     This problem is fatal. IFrIT will now exit.";
			break;
		}
	case 1:
		{
			s += "\n     Current operation is aborted.";
			break;
		}
	case 2:
		{
			int o = 0;  // just a placeholder for a breakpoint
			break;
		}
	}

	Display(MessageType::Error,s);

	if(severity <= 0)
	{
		throw iException::GlobalExit();
	}
}


void iOutput::AddChannel(iOutputChannel *channel)
{
	IASSERT(channel);
	Stack.Add(channel);
	channel->Register(0);

	//
	//  Take over the VTK output window
	//
	OutputWindow *ow = OutputWindow::New();
	if(ow == 0)
	{
		vtkOutputWindow::GetInstance()->DisplayErrorText("FATAL ERROR: unable to initialize output channel.");
		exit(-1);
	}

	vtkOutputWindow::SetInstance(ow);
	ow->Delete();
}


void iOutput::RemoveChannel(iOutputChannel *channel)
{
	if(Stack.Remove(channel))
	{
		channel->UnRegister(0);
	}

	if(Stack.Size() == 0)
	{
		//
		//  Restore the VTK output window
		//
		vtkOutputWindow::SetInstance(0);
	}
}

	
void iOutput::LogError(const iString &text)
{
	ErrorLog.Add(text);
}


#ifdef I_DEBUG
void iOutput::DisplayDebugMessage(const iString& text)
{
	static int count = 0;
	iString s = iString::FromNumber(++count,"...%6d ") + text;
	
	Display(MessageType::DebugMessage,s);
}
#endif

