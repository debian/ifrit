/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ibitmaptextsubject.h"


#include "ierror.h"
#include "ioverlayhelper.h"
#include "itextactor.h"

#include <vtkActor2D.h>
#include <vtkTextMapper.h>
#include <vtkTextProperty.h>
#include <vtkViewport.h>

//
//  Templates
//
#include "iarray.tlh"
#include "igenericprop.tlh"


iBitmapTextSubject* iBitmapTextSubject::New(iTextActor *parent, iRenderTool *rt)
{
	IASSERT(rt);
	return new iBitmapTextSubject(parent,rt);
}


iBitmapTextSubject::iBitmapTextSubject(iTextActor *parent, iRenderTool *rt) : iTextSubject(parent,rt)
{
	mMapper = vtkTextMapper::New(); IERROR_CHECK_MEMORY(mMapper);
	mActor = vtkActor2D::New(); IERROR_CHECK_MEMORY(mActor);
	this->AppendComponent(mActor);
	mActor->SetMapper(mMapper);
	mActor->GetPositionCoordinate()->SetCoordinateSystemToNormalizedViewport();

	mMapper->SetTextProperty(mParent->GetTextProperty()); 
}


iBitmapTextSubject::~iBitmapTextSubject()
{
	mMapper->Delete();
	mActor->Delete();
}


void iBitmapTextSubject::ComputeSize(vtkViewport *viewport, float s[2])
{
	mMapper->SetInput(mParent->GetText().ToCharPointer());

	double w = mMapper->GetWidth(viewport);
	double h = mMapper->GetHeight(viewport);
	viewport->ViewportToNormalizedViewport (w,h);

	s[0] = w;
	s[1] = h;
}


void iBitmapTextSubject::UpdateGeometryBody(vtkViewport* viewport, int mag)
{
	if(mag == 1)
	{
		//mMapper->GetTextProperty()->SetOrientation(mParent->GetAngle());
		mActor->SetPosition(mPos[0],mPos[1]);
		mActor->SetPosition2(mSize[0],mSize[1]);
	}
	else
	{
		int winij[2];
		this->GetOverlayHelper()->ComputePositionShiftsUnderMagnification(winij);
		
		mActor->SetPosition(mag*mPos[0]-winij[0],mag*mPos[1]-winij[1]);
		mActor->SetPosition2(mag*mSize[0],mag*mSize[1]);
	}
}

