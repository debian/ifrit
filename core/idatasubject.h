/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A base class for all DataSubjects. One DataSubject provides data for a single unique DataType.
//
#ifndef IDATASUBJECT_H
#define IDATASUBJECT_H


#include "iobject.h"
#include "iviewmodulecomponent.h"


#include "idatalimits.h"

class iDataConsumer;
class iDataLimits;
class iDataSubject;
class iDataType;
class iFileLoader;
class iHistogramMaker;
class iProbeFilter;

class vtkDataSet;


//
//  Property class that connect to iDataLimits
//
template<iType::TypeId id>
class iDataSubjectProperty : public iPropertyDataVariable<id>
{

public:

	typedef typename iType::Traits<id>::Type ArgT;
	typedef bool (iDataLimits::*SetterType)(int i, ArgT val);
	typedef ArgT (iDataLimits::*GetterType)(int i) const;

	iDataSubjectProperty(iDataSubject *owner, SetterType setter, GetterType getter, const iString &fname, const iString &sname, int rank = 0) : iPropertyDataVariable<id>(owner,fname,sname,rank)
	{
		this->mOwner = owner;
		this->mSetter = setter;
		this->mGetter = getter;
	}

	virtual int Size() const;
	virtual bool IsFixedSize() const { return false; }

	virtual bool Copy(const iPropertyVariable *p);

	virtual ArgT GetValue(int i) const;
	virtual bool SetValue(int i, ArgT v) const;

protected:

	virtual bool Resize(int num, bool force = false);

	iDataSubject *mOwner;
	SetterType mSetter;
	GetterType mGetter;
};


class iDataSubject : public iObject, public iViewModuleComponent
{

	friend class iFileLoader;

public:

	vtkTypeMacro(iDataSubject,iObject);

	iPropertyQuery<iType::Bool> DataPresent;
	iObjectPropertyMacro2Q(FileName,String);
	iObjectPropertyMacro1S(ResetOnLoad,Bool);

	iDataSubjectProperty<iType::Pair> Range;
	iDataSubjectProperty<iType::String> Name;
	iDataSubjectProperty<iType::String> Stretch;

	iObjectPropertyMacroA1(Load,String);
	iObjectPropertyMacroA(Erase);

	virtual bool IsThereData() const;
	void RequestDataReload() const;
	void SyncWithLimits(int var, int mode) const; // just pass a request to ViewModule

	//
	//  Access to components
	//
	inline iFileLoader* GetLoader() const { return mLoader; }
	iHistogramMaker* GetHistogramMaker() const;
	iDataLimits* GetLimits() const;
	vtkDataSet* GetData() const;
	virtual const iDataType& GetDataType() const = 0;

	//
	//  Boundary conditions
	//
	bool IsDirectionPeriodic(int d) const;

	//
	//  We need to know what ProbeFilter would work with this Subject
	//
	virtual iProbeFilter* CreateProbeFilter(iDataConsumer *consumer) const;

	//
	//  Provide some additional information about the data.
	//
	unsigned int GetDataRank() const;
	bool IsThereScalarData() const;
	bool IsThereVectorData() const;
	bool IsThereTensorData() const;

	//
	//  Data subjects are treated by help differently from other objects
	//
	virtual const iString HelpTag() const;

protected:

	iDataSubject(iFileLoader *fl, const iDataType& type);
	virtual ~iDataSubject();

	iDataSubject* Self(){ return this; }  // needed to avoid compiler warning
	inline int GetId() const { return mId; }

	virtual iHistogramMaker* CreateHistogramMaker() const;
	virtual iDataLimits* CreateLimits() const;

	virtual void SavePropertiesToString(iString &str) const;

private:

	//
	//  Components
	//
	const int mId;
	iFileLoader *mLoader;

	mutable iDataLimits *mLimits; // must be accessed via GetLimits() only
	mutable iHistogramMaker *mHistogramMaker; // must be accessed via GetHistogramMaker() only
};


template<iType::TypeId id>
int iDataSubjectProperty<id>::Size() const
{
	return this->mOwner->GetLimits()->GetNumVars();
}


template<iType::TypeId id>
typename iDataSubjectProperty<id>::ArgT iDataSubjectProperty<id>::GetValue(int i) const
{
	return (this->mOwner->GetLimits()->*this->mGetter)(i);
}


template<iType::TypeId id>
bool iDataSubjectProperty<id>::SetValue(int i, ArgT v) const
{
	return (this->mOwner->GetLimits()->*this->mSetter)(i,v);
}


template<iType::TypeId id>
bool iDataSubjectProperty<id>::Resize(int num, bool force)
{
	return this->mOwner->GetLimits()->Resize(num);
}


template<iType::TypeId id>
bool iDataSubjectProperty<id>::Copy(const iPropertyVariable *p)
{
	const iDataSubjectProperty<id> *other = dynamic_cast<const iDataSubjectProperty<id>*>(p);
	if(other != 0)
	{
		int i, n = this->Size();
		if(other->Size() < n) n = other->Size();

		for(i=0; i<n; i++)
		{
			if(!this->SetValue(i,other->GetValue(i))) return false;
		}

		return true;
	}
	else return false;
}


//
//  Useful macros to declare all members that have to be overwritten in children
//
#define iDataSubjectDefineTypeMacro(_object_,_id_,_fname_,_sname_,_rank_,_keywords_,_environment_) \
	const iDataType& _object_::DataType() \
	{ \
		static const iDataType tmp(_id_,_fname_,_sname_,_rank_,_keywords_,_environment_); \
		return tmp; \
	} \
	const iDataType& _object_::GetDataType() const { return _object_::DataType(); }

#define iDataSubjectDeclareClassMacro(_prefix_,_name_) \
class _prefix_##_name_##DataSubject : public iDataSubject \
{ \
public: \
	vtkTypeMacro(_prefix_##_name_##DataSubject,iDataSubject); \
	static const iDataType& DataType(); \
	const iDataType& GetDataType() const; \
	_prefix_##_name_##DataSubject(iFileLoader *fl); \
}

#define iDataSubjectDefineClassMacro(_prefix_,_name_,_fname_,_sname_,_rank_,_keywords_,_environment_) \
iDataSubjectDefineTypeMacro(_prefix_##_name_##DataSubject,_prefix_##Extension::SubjectId(),_fname_,_sname_,_rank_,_keywords_,_environment_); \
_prefix_##_name_##DataSubject::_prefix_##_name_##DataSubject(iFileLoader *fl) : iDataSubject(fl,_prefix_##_name_##DataSubject::DataType()){}

#endif

