/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iaxis.h"


#include "ioverlayhelper.h"
#include "irendertool.h"

#include <vtkCamera.h>
#include <vtkProperty2D.h>
#include <vtkRenderer.h>
#include <vtkTextMapper.h>
#include <vtkTextProperty.h>

//
//  Templates
//
#include "iarray.tlh"
#include "igenericprop.tlh"


iAxis* iAxis::New(iRenderTool *rt)
{
	IASSERT(rt);
	return new iAxis(rt);
}


iAxis::iAxis(iRenderTool *rt) : iGenericProp<vtkAxisActor2D>(true), mOverlayHelper(rt)
{
	mLineWidth = 2;
	mTickLength = TickLength;

	this->PickableOff();
}


iAxis::~iAxis()
{
}


void iAxis::UpdateGeometry(vtkViewport *vp)
{
	int mag = mOverlayHelper->GetRenderingMagnification();

	if(mag == 1)
	{
		this->AxisActor->GetProperty()->SetLineWidth(mLineWidth);
		this->SetTickLength(mTickLength);

		mPos1 = this->GetPosition();
		mPos2 = this->GetPosition2();

		this->GetProperty()->SetColor(this->GetOverlayHelper()->GetColor(vp).ToVTK());
		this->GetOverlayHelper()->UpdateTextProperty(vp,this->LabelTextProperty);
		this->GetOverlayHelper()->UpdateTextProperty(vp,this->TitleTextProperty);
	}
	else
	{
		int winij[2];
		mOverlayHelper->ComputePositionShiftsUnderMagnification(winij);
		
		int *sz = vp->GetSize();
		if(this->GetPoint1Coordinate()->GetCoordinateSystem() == VTK_NORMALIZED_VIEWPORT)
		{
			this->SetPosition(mag*mPos1.X-winij[0],mag*mPos1.Y-winij[1]);
		}
		else
		{
			this->SetPosition(mag*mPos1.X-sz[0]*winij[0],mag*mPos1.Y-sz[1]*winij[1]);
		}

		if(this->GetPoint2Coordinate()->GetCoordinateSystem() == VTK_NORMALIZED_VIEWPORT)
		{
			this->SetPosition2(mag*mPos2.X-winij[0],mag*mPos2.Y-winij[1]);
		}
		else
		{
			this->SetPosition2(mag*mPos2.X-sz[0]*winij[0],mag*mPos2.Y-sz[1]*winij[1]);
		}

		this->AxisActor->GetProperty()->SetLineWidth(mLineWidth*mag);
		this->SetTickLength(mag*mTickLength);
	}
}


void iAxis::UpdateOverlay(vtkViewport *vp)
{
	int i, mag = mOverlayHelper->GetRenderingMagnification();

	//
	//  This must be after RenderOpaqueGeometry
	//
	this->TitleTextProperty->SetFontSize(mOverlayHelper->GetFontSize(vp,mag));
	this->TitleMapper->GetTextProperty()->ShallowCopy(this->TitleTextProperty);

	this->LabelTextProperty->SetFontSize(mOverlayHelper->GetFontSize(vp,mag));
	for(i=0; i<this->AdjustedNumberOfLabels; i++)
	{
		this->LabelMappers[i]->GetTextProperty()->ShallowCopy(this->LabelTextProperty);
	}

	int *s = vp->GetSize();
	if(mag == 1)
	{
		mPosT = this->TitleActor->GetPosition();
		mPosL.Resize(this->AdjustedNumberOfLabels);
		for(i=0; i<this->AdjustedNumberOfLabels; i++)
		{
			mPosL[i] = this->LabelActors[i]->GetPosition();
		}
	}
	else
	{
		int winij[2];
		mOverlayHelper->ComputePositionShiftsUnderMagnification(winij);
		this->TitleActor->SetPosition(mag*mPosT.X-s[0]*winij[0],mag*mPosT.Y-s[1]*winij[1]);
		for(i=0; i<this->AdjustedNumberOfLabels; i++)
		{
			this->LabelActors[i]->SetPosition(mag*mPosL[i].X-s[0]*winij[0],mag*mPosL[i].Y-s[1]*winij[1]);
		}
	}
}


iAxis::Pos& iAxis::Pos::operator=(const double *ptr)
{
	this->X = ptr[0];
	this->Y = ptr[1];
	return *this;
}
