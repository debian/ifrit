/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "idatareaderobserver.h"


#include "idata.h"
#include "ierror.h"
#include "ioutput.h"
#include "istring.h"


using namespace iParameter;


//
//  iDataReaderObserver class
//
iDataReaderObserver::iDataReaderObserver(iShell *s) : iShellComponent(s)
{
	mStarted = false;
}


void iDataReaderObserver::StartSet()
{
	if(mStarted)
	{
		IBUG_ERROR("iDataReaderObserver cannot be called recursively.");
		return;
	}
	mStarted = true;
	this->OnSetStart();
}


void iDataReaderObserver::StartFile(const iString &fname, const iDataType &type)
{
	this->OnFileStart(fname,type);
}


void iDataReaderObserver::FinishFile(const iString &fname, const iDataInfo &info)
{
	this->OnFileFinish(fname,info);
}


void iDataReaderObserver::FinishSet(bool error)
{
	this->OnSetFinish(error);
	mStarted = false;
}


bool iDataReaderObserver::AllowReload()
{
	this->OutputText(MessageType::Warning,"This property only takes effect when the data set is reloaded. Use <DataReader:Reload()> function call to reload the data as needed.");
	return false;
}



void iDataReaderObserver::OnSetStart()
{
}


void iDataReaderObserver::OnFileFinish(const iString &fname, const iDataInfo &info)
{
}


void iDataReaderObserver::OnSetFinish(bool error)
{
}


void iDataReaderObserver::OnFileStart(const iString &fname, const iDataType &type)
{
	this->OutputText(MessageType::Information,"Loading file: "+fname+" of data type "+type.LongName());
}

