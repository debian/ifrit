/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iposition.h"

#include "icamera.h"
#include "ierror.h"
#include "imarker.h"
#include "ipicker.h"
#include "irendertool.h"
#include "iviewmodule.h"


using namespace iParameter;


//
//  iCoordinate functions
//
iCoordinate::iCoordinate(iViewModule *vm) : iViewModuleComponent(vm), mOffset(1)
{
	IASSERT(vm);
	mOGLValue = 0.0;
}


iCoordinate::iCoordinate(iViewModule *vm, int offset) : iViewModuleComponent(vm), mOffset(offset)
{
	IASSERT(vm);
	mOGLValue = 0.0;
}


void iCoordinate::UpdateBoxValue() const
{
	if(this->GetViewModule()->InOpenGLCoordinates())
	{
		mBoxValue = mOGLValue;
	}
	else
	{
		mBoxValue = 0.5*(mOffset+mOGLValue)*this->GetViewModule()->GetBoxSize();
	}

}


void iCoordinate::UpdateOGLValue()
{
	if(this->GetViewModule()->InOpenGLCoordinates())
	{
		mOGLValue = mBoxValue;
	}
	else
	{
		mOGLValue = -mOffset + 2.0*mBoxValue/this->GetViewModule()->GetBoxSize();
	}
}


void iCoordinate::PeriodicWrap()
{
	mOGLValue -= 2.0*floor(0.5*(1.0+mOGLValue));
}


void iCoordinate::CutToBounds()
{
	if(mOGLValue < -1.0) mOGLValue = -1.0;
	if(mOGLValue >  1.0) mOGLValue =  1.0;
}

//
//  iDistance functions
//
iDistance::iDistance(iViewModule *vm) : iCoordinate(vm,0)
{
	mOGLValue = 1.0;
}


void iDistance::PeriodicWrap()
{
}


//
//  iPosition functions
//
iPosition::iPosition(iViewModule *vm) : iViewModuleComponent(vm)
{
	IASSERT(vm);
}


bool iPosition::SetToSpecialLocation(int v)
{
	switch(v)
	{
	case SpecialLocation::PickedPoint:
		{
			if(!this->GetViewModule()->GetPicker()->GetObjectName().IsEmpty())
			{
				*this = this->GetViewModule()->GetPicker()->GetPosition();
				return true;
			}
			break;
		}
	case SpecialLocation::FocalPoint:
		{
			*this = this->GetViewModule()->GetRenderTool()->GetCamera()->GetFocalPoint();
			return true;
		}
	case SpecialLocation::BoxCenter:
		{
			*this = 0;
			return true;
		}
	default:
		{
			if(v>0 && v<=this->GetViewModule()->GetNumberOfMarkers())
			{
				*this = this->GetViewModule()->GetMarker(v-1)->GetPosition();
				return true;
			}
		}
	}

	return false;
}


void iPosition::UpdateBoxValue() const
{
	if(this->GetViewModule()->InOpenGLCoordinates())
	{
		mBoxValue = mOGLValue;
	}
	else
	{
		int i;
		float boxSize = this->GetViewModule()->GetBoxSize();
		for(i=0; i<3; i++) mBoxValue[i] = 0.5*(1.0+mOGLValue[i])*boxSize;
	}

}


void iPosition::UpdateOGLValue()
{
	if(this->GetViewModule()->InOpenGLCoordinates())
	{
		mOGLValue = mBoxValue;
	}
	else
	{
		int i;
		float boxSize = this->GetViewModule()->GetBoxSize();
		for(i=0; i<3; i++) mOGLValue[i] = -1.0 + 2.0*mBoxValue[i]/boxSize;
	}
}


void iPosition::PeriodicWrap()
{
	int i;
	//
	//  If x is in between -1 and 1, do nothing - this way x=1 remains 1, and not wrapped into x=-1
	//
	for(i=0; i<3; i++) if(mOGLValue[i]<-1.0 || mOGLValue[i]>1.0) mOGLValue[i] -= 2.0*floor(0.5*(1.0+mOGLValue[i]));
}


void iPosition::CutToBounds()
{
	int i;
	for(i=0; i<3; i++)
	{
		if(mOGLValue[i] < -1.0) mOGLValue[i] = -1.0;
		if(mOGLValue[i] >  1.0) mOGLValue[i] =  1.0;
	}
}

