/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "imultiviewsubject.h"


#include "imath.h"
#include "iviewmodule.h"

//
//  Templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


using namespace iParameter;


//
//  Single instance class
//
iMultiViewInstance::iMultiViewInstance(iMultiViewSubject *owner, int numactors, int numpaintedactors, int priority, bool alwayspainted) : iPaintedViewInstance(owner,numactors,1.0,numpaintedactors,priority,alwayspainted)
{
}


iMultiViewInstance::~iMultiViewInstance()
{
}


bool iMultiViewInstance::IsConnectedToPaintingData() const
{
	return this->IsThereData();
}


//
// Main class
//
iMultiViewSubject::iMultiViewSubject(iObject *parent, const iString &fname, const iString &sname, iViewModule *vm, const iDataType &type, unsigned int flags, int minsize) : iPaintedViewSubject(parent,fname,sname,vm,type,type,flags,minsize,iMath::_IntMax),
	Number(iObject::Self(),static_cast<iType::ps_int::SetterType>(&iViewSubject::SetNumberOfInstances),static_cast<iType::ps_int::GetterType>(&iViewSubject::GetNumberOfInstances),"Number","num",9),
	Create(iObject::Self(),static_cast<iPropertyAction::CallerType>(&iViewSubject::CreateInstance),"New","new"),
	Remove(iObject::Self(),static_cast<iPropertyAction1<iType::Int>::CallerType>(&iViewSubject::RemoveInstance),"Delete","dlt")
{
	Remove.AddFlag(iProperty::_FunctionsAsIndex);
}


iMultiViewSubject::~iMultiViewSubject()
{
}

