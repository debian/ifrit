/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A few functions and macros for easier math and template creation. 
//
#ifndef IMATH_H
#define IMATH_H


#include <math.h>
#include <float.h>
#include <limits.h>


class vtkMinimalStandardRandomSequence;


namespace iMath
{
#ifdef INT_MAX
	const int _IntMax = INT_MAX;
#else
	const int _IntMax = 2147483647;
#endif

#ifdef INT_MIN
	const int _IntMin = INT_MIN;
#else
	const int _IntMin = -2147483648;
#endif

#ifdef UINT_MAX
	const unsigned int _UnsignedIntMax = UINT_MAX;
#else
	const unsigned int _UnsignedIntMax = 4294967295U;
#endif

#ifdef FLT_MAX
	const float _FloatMax = FLT_MAX;
#else
	const float _FloatMax = 3.402823466e+38F;
#endif

#ifdef FLT_MIN
	const float _FloatMin = FLT_MIN;
#else
	const float _FloatMin = 1.175494351e-38F;
#endif

#ifdef FLT_EPSILON
	const float	_FloatRes = FLT_EPSILON;
#else
	const float _FloatRes = 1.192092896e-07F;
#endif

#ifdef DBL_MAX
	const double _DoubleMax = DBL_MAX;
#else
	const double _DoubleMax = 1.7976931348623158e+308;
#endif

#ifdef DBL_MIN
	const double _DoubleMin = DBL_MIN;
#else
	const double _DoubleMin = 2.2250738585072014e-308;
#endif

#ifdef DBL_EPSILON
	const double _DoubleRes = DBL_EPSILON;
#else
	const double _DoubleRes = 2.2204460492503131e-016;
#endif

	const float _LargeFloat = 1.0e36f;
	const int _LargeInt = _IntMax - 777;
	const float _FloatTolerance = 10.0f*_FloatRes;
	const double _DoubleTolerance = 30.0*_DoubleRes;


	inline float Deg2Rad(float x)
	{
		return 3.1415927f/180.0f*x;
	}


	inline double Deg2Rad(double x)
	{
		return 3.1415926535897932384626433832795/180.0*x;
	}


	inline int Round2Int(float x)
	{
		return (int)floorf(0.5f+x);
	}


	inline int Round2Int(double x)
	{
		return (int)floor(0.5+x);
	}


	inline double Pow10(double x)
	{
		return pow(10.0,x);
	}


	inline float Pow10(float x)
	{
		return powf(10.0f,x);
	}


	inline double Log10(double x)
	{
		return log10(x);
	}


	inline float Log10(float x)
	{
		return log10f(x);
	}


	inline double PowN(double x, int n)
	{
		return pow(x,double(n));
	}


	inline float PowN(float x, int n)
	{
		return powf(x,float(n));
	}


	inline int Abs(int v)
	{
		return (v < 0) ? -v : v;
	}


	bool Intersect3Planes(double *p1, double *p2, double *p3, double *x);

	class Random
	{

	public:

		Random(int seed);
		~Random();

		double NextValue();

	private:

		vtkMinimalStandardRandomSequence *mGenerator;
	};
}

#endif

