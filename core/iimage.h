/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IIMAGE_H
#define IIMAGE_H


#include "istring.h"


class iColor;

class vtkImageData;


class iImage
{
	
public:

	//
	//  Fit both directions when scaling, or fit into the requested size/include the requested size
	//  while preserving the original axes ratio.
	//
	enum ScaleMode
	{
		_Both = 0,
		_Fit = 1,
		_Include = 2,
		_FitMin = 3
	};

	enum ScaleFilter
	{
		_None = 0,
		_Smooth = 1
	};
	
	iImage(int depth = 3);
	iImage(const iImage& img);
	~iImage();
	
	//
	//  Operators
	//
    iImage& operator=(const iImage &img);   // impl-shared copy
    friend bool operator==(const iImage &s1, const iImage &s2);
    friend bool operator!=(const iImage &s1, const iImage &s2);
	
	//
	//  Loading from file
	//
	static bool LoadFromFile(const iString &filename, iImage &image);
	bool LoadFromFile(const iString &filename);
	
	//
	//  Useful functions for manipulating images
	//
	void Scale(int w, int h, ScaleMode m = _Both, ScaleFilter f = _None);
	void Smooth(float dx, float dy);
	void Clear();
	void ReplaceData(vtkImageData *n);
	void Blend(const iImage& im, float op);
	void Overlay(int x, int y, const iImage& ovr, float opacity = 1.0, bool masking = true);
	bool CombineInPseudoColor(const iImage &imRed, const iImage &imGreen, const iImage &imBlue);
	void Crop(int x, int y, int w, int h);
	void DrawFrame(int width, const iColor &color);

	//
	//  Quering the data
	//
	inline bool IsEmpty() const { return mData == 0; }
	inline int Depth() const { return mDepth; }
	inline int Width() const { return mWidth; }
	inline int Height() const { return mHeight; }
	inline void Dimensions(int d[2]) const { d[0] = mWidth; d[1] = mHeight; }

	//
	//  Direct access to underlying data: need to be very careful with direct data access!!!
	//
	unsigned char* DataPointer() const;
	inline vtkImageData* DataObject() const { return mData; }
	void SetDataPointer(unsigned char* array, int w, int h, int d);

private:

	//
	//  Helper function: if depth is set to 0, then the data are not actually allocated
	//
	vtkImageData* CreateData(int w, int h, int depth = 0);

	vtkImageData *mData;
	int mDepth, mWidth, mHeight;
};


inline bool operator!=(const iImage &s1, const iImage &s2)
{
	return !(s1==s2);
}

#endif // IIMAGE_H

