/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iimagecomposer.h"


#include "ierror.h"
#include "iimagecomposerwindows.h"
#include "imath.h"
#include "imonitor.h"
#include "ishell.h"
#include "istereoimagearray.h"
#include "iviewmodule.h"

//
//  Templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


using namespace iType;
using namespace iParameter;


//
//  Main class
//
iImageComposer* iImageComposer::New(iShell *s)
{
	static iString LongName("ImageComposer");
	static iString ShortName("ic");

	IASSERT(s);

	return new iImageComposer(s,LongName,ShortName);
}


#define iImageComposerConstructMacroB(_class_,_fname_,_sname_) \
	_fname_(Self(),static_cast<_class_::SetterType>(&iImageComposer::Set##_fname_),static_cast<_class_::GetterType>(&iImageComposer::Get##_fname_),#_fname_,#_sname_,static_cast<_class_::SizeType>(&iImageComposer::GetNumberOfBackgroundWindows))

#define iImageComposerConstructMacroF(_class_,_fname_,_sname_) \
	_fname_(Self(),static_cast<_class_::SetterType>(&iImageComposer::Set##_fname_),static_cast<_class_::GetterType>(&iImageComposer::Get##_fname_),#_fname_,#_sname_,static_cast<_class_::SizeType>(&iImageComposer::GetNumberOfForegroundWindows))

iImageComposer::iImageComposer(iShell *s, const iString &fname, const iString &sname) : iObject(s,fname,sname), iShellComponent(s),
	iObjectConstructPropertyMacroV(Int,iImageComposer,NumTiles,nt,2),
	iObjectConstructPropertyMacroS(Int,iImageComposer,NumForegroundWindows,nfg),
	iObjectConstructPropertyMacroS(Bool,iImageComposer,ScaleBackground,sb),
	iObjectConstructPropertyMacroS(Bool,iImageComposer,InnerBorder,ib),
	iObjectConstructPropertyMacroS(Color,iImageComposer,BorderColor,bc),
	iObjectConstructPropertyMacroS(Int,iImageComposer,BorderWidth,bw),
	iImageComposerConstructMacroB(pv_int,BackgroundWindowIndex,bgi),
	iImageComposerConstructMacroB(pv_int,BackgroundWindowIndex2,bgi2),
	iImageComposerConstructMacroB(pv_int,BackgroundWindowIndex3,bgi3),
	iImageComposerConstructMacroB(pv_string,BackgroundWindowWallpaperFile,bgw),
	iImageComposerConstructMacroF(pv_color,ForegroundWindowBorderColor,fgc),
	iImageComposerConstructMacroF(pv_float,ForegroundWindowScale,fgs),
	iImageComposerConstructMacroF(pv_int,ForegroundWindowIndex,fgi),
	iImageComposerConstructMacroF(pv_int,ForegroundWindowIndex2,fgi2),
	iImageComposerConstructMacroF(pv_int,ForegroundWindowIndex3,fgi3),
	iImageComposerConstructMacroF(pv_int,ForegroundWindowBorderWidth,fgw),
	iImageComposerConstructMacroF(pv_int,ForegroundWindowPositionX,fgx),
	iImageComposerConstructMacroF(pv_int,ForegroundWindowPositionY,fgy),
	iImageComposerConstructMacroF(pv_int,ForegroundWindowZoomSource,fgz),
	iImageComposerConstructMacroF(pv_bool,ForegroundWindowZoom4Line,fgz4),
	iImageComposerConstructMacroF(pv_float,ForegroundWindowZoomFactor,fgzf),
	iImageComposerConstructMacroF(pv_float,ForegroundWindowZoomX,fgzx),
	iImageComposerConstructMacroF(pv_float,ForegroundWindowZoomY,fgzy),
	iObjectConstructPropertyMacroQ(Int,iImageComposer,ImageWidth,w),
	iObjectConstructPropertyMacroQ(Int,iImageComposer,ImageHeight,h)
{
	mRenderMode = RenderMode::NoRender;

	BackgroundWindowIndex.AddFlag(iProperty::_FunctionsAsIndex);
	BackgroundWindowIndex2.AddFlag(iProperty::_FunctionsAsIndex);
	BackgroundWindowIndex3.AddFlag(iProperty::_FunctionsAsIndex);

	ForegroundWindowIndex.AddFlag(iProperty::_FunctionsAsIndex);
	ForegroundWindowIndex2.AddFlag(iProperty::_FunctionsAsIndex);
	ForegroundWindowIndex3.AddFlag(iProperty::_FunctionsAsIndex);

	mBlockUpdate = mInComposing = false;

	mNumTiles[0] = mNumTiles[1] = 1;
	mBorderWidth = 0;
	mBorderColor = iColor(0,0,0);
	mInnerBorder = false;
	mFullWidth = mTileWidth = 640;
	mFullHeight = mTileHeight = 480;

	mBackgroundWindows.Add(iImageComposerBackgroundWindow::New(0,0,this));
}


iImageComposer::~iImageComposer()
{
	int i;

	for(i=0; i<mForegroundWindows.Size(); i++)
	{
		delete mForegroundWindows[i];
	}

	for(i=0; i<mBackgroundWindows.Size(); i++)
	{
		delete mBackgroundWindows[i];
	}
}


bool iImageComposer::SetNumTiles(int k, int n)
{
	if(k<0 || k>1 || n<1) return false;
	if(n == mNumTiles[k]) return true;

	int i, j;

	//
	//  CreateBackgroundImage checks for out-of-boundary requests, so we need to have
	//  mNumTiles[X,Y] Set before creating new windows
	//
	int oldNumTiles[2];
	oldNumTiles[0] = mNumTiles[0];
	oldNumTiles[1] = mNumTiles[1];
	mNumTiles[k] = n; 

	iArray<iImageComposerBackgroundWindow*> tmp;
	for(j=0; j<mBackgroundWindows.Size(); j++) tmp.Add(mBackgroundWindows[j]);
	mBackgroundWindows.Clear();
	mBackgroundWindows.Resize(mNumTiles[0]*mNumTiles[1]);
	
	for(j=0; j<mNumTiles[1] && j<oldNumTiles[1]; j++)
	{
		for(i=0; i<mNumTiles[0] && i<oldNumTiles[0]; i++)
		{
			mBackgroundWindows[i+mNumTiles[0]*j] = tmp[i+oldNumTiles[0]*j];
		}
	}

	for(j=mNumTiles[1]; j<oldNumTiles[1]; j++)
	{
		for(i=0; i<oldNumTiles[0]; i++)
		{
			delete tmp[i+oldNumTiles[0]*j];
		}
	}

	for(j=0; j<mNumTiles[1]; j++)
	{
		for(i=mNumTiles[0]; i<oldNumTiles[0]; i++)
		{
			delete tmp[i+oldNumTiles[0]*j];
		}
	}

	for(j=oldNumTiles[1]; j<mNumTiles[1]; j++)
	{
		for(i=0; i<mNumTiles[0]; i++)
		{
			mBackgroundWindows[i+mNumTiles[0]*j] = iImageComposerBackgroundWindow::New(i,j,this);
		}
	}

	for(j=0; j<oldNumTiles[1]; j++)
	{
		for(i=oldNumTiles[0]; i<mNumTiles[0]; i++)
		{
			mBackgroundWindows[i+mNumTiles[0]*j] = iImageComposerBackgroundWindow::New(i,j,this);
		}
	}

	this->UpdateSize();
	return true;
}


void iImageComposer::AddForegroundWindow(iViewModule *v)
{
	if(v == 0) return;

	mForegroundWindows.Add(iImageComposerForegroundWindow::New(v,this));
}


void iImageComposer::RemoveForegroundWindow(int n)
{
	if(n<0 || n>=mForegroundWindows.Size()) return;

	delete mForegroundWindows[n];
	mForegroundWindows.Remove(n);
}


bool iImageComposer::SetScaleBackground(bool s)
{
	if(s != mScaleBackground)
	{
		mScaleBackground = s;
		this->UpdateSize();
	}
	return true;
}


bool iImageComposer::SetInnerBorder(bool s)
{
	if(s != mInnerBorder)
	{
		mInnerBorder = s;
		this->UpdateSize(); 
	}
	return true;
}


bool iImageComposer::SetBorderWidth(int s) 
{ 
	if(s >= 0) 
	{ 
		mBorderWidth = s; 
		this->UpdateSize(); 
	} 
	return true;
}


bool iImageComposer::SetBorderColor(const iColor& c) 
{ 
	if(c != mBorderColor) 
	{ 
		mBorderColor = c; 
	} 
	return true;
}


void iImageComposer::Update()
{
	this->UpdateWindowList();
	this->UpdateSize();
}


void iImageComposer::UpdateSize()
{
	if(mBlockUpdate) return;
	
	int i;
	//
	//  Compute real size
	//
	mTileWidth = mTileHeight = 0;
	for(i=0; i<mBackgroundWindows.Size(); i++)
	{
		if(mTileWidth < mBackgroundWindows[i]->GetWindowWidth()) mTileWidth = mBackgroundWindows[i]->GetWindowWidth();
		if(mTileHeight < mBackgroundWindows[i]->GetWindowHeight()) mTileHeight = mBackgroundWindows[i]->GetWindowHeight();
	}
	if(mTileWidth==0 || mTileHeight==0) 
	{
		for(i=0; i<this->GetShell()->GetNumberOfViewModules(); i++)
		{
			if(mTileWidth < this->GetShell()->GetViewModule(i)->GetThisImageWidth()) mTileWidth = this->GetShell()->GetViewModule(i)->GetThisImageWidth(); 
			if(mTileHeight < this->GetShell()->GetViewModule(i)->GetThisImageHeight()) mTileHeight = this->GetShell()->GetViewModule(i)->GetThisImageHeight();
		}
	}

	mFullWidth = mNumTiles[0]*mTileWidth + 2*mBorderWidth;
	mFullHeight = mNumTiles[1]*mTileHeight + 2*mBorderWidth;
	if(mInnerBorder)
	{
		mFullWidth += (mNumTiles[0]-1)*mBorderWidth;
		mFullHeight += (mNumTiles[1]-1)*mBorderWidth;
	}

	//
	//  Make sure no foreground window moves out of image area
	//
	for(i=0; i<mForegroundWindows.Size(); i++)
	{
		mForegroundWindows[i]->CorrectPosition();
	}
}


int iImageComposer::GetImageWidth() const
{
	return mFullWidth;
}


int iImageComposer::GetImageHeight() const
{
	return mFullHeight;
}


void iImageComposer::UpdateWindowList()
{
	int i;

	for(i=0; i<mBackgroundWindows.Size(); i++) mBackgroundWindows[i]->UpdateWindow();
	for(i=0; i<mForegroundWindows.Size(); i++) 
	{
		mForegroundWindows[i]->UpdateWindow();
		if(mForegroundWindows[i]->GetViewModule() == 0) this->RemoveForegroundWindow(i);
	}
}


iImageComposerBackgroundWindow* iImageComposer::GetBackgroundWindow(int i) const
{
	if(i>=0 && i<mBackgroundWindows.Size()) return mBackgroundWindows[i]; else return 0;
}


iImageComposerForegroundWindow* iImageComposer::GetForegroundWindow(int i) const
{
	if(i>=0 && i<mForegroundWindows.Size()) return mForegroundWindows[i]; else return 0;
}


bool iImageComposer::IsActive() 
{
	int i;
	
	//
	//  Is there an active background window?
	//
	for(i=0; i<mBackgroundWindows.Size(); i++)
	{
		if(!mBackgroundWindows[i]->IsEmpty()) return true;
	}

	//
	//  Is there an active foreground window?
	//
	if(mForegroundWindows.Size() > 0) return true;

	//
	// At the very least, is there a border?
	//
	return mBorderWidth > 0;
}

										  
bool iImageComposer::Compose(iViewModule *vm, iStereoImageArray &images) 
{ 
	if(vm == 0)
	{
		IBUG_ERROR("No ViewModule is specified");
		return false;
	}

	iMonitor h(this);

	mInComposing = true;

	//
	//  We received a request to compose the images and put the data into images.
	//  First, make sure that Composer is up to date
	//
	this->UpdateWindowList();

	iStereoImage outIm;
	char rgb[3];
	int i, j, k, xoff;
	unsigned char *dPtr, *dPtr1;
	
	outIm.Scale(mFullWidth,mFullHeight);
	dPtr = outIm.LeftEye().DataPointer();
	int d = outIm.Depth();
	bool work = false;

	if(mBorderWidth > 0)
	{
		work = true;
		//
		//  Create the border
		//
		mBorderColor.GetRGB(rgb);
		//
		//  Vertical lines
		//
		for(j=0; j<mFullHeight; j++)  // sort of a waste, but it is much simpler this way
		{
			for(i=0; i<mBorderWidth; i++)
			{
				dPtr1 = dPtr + d*(i+mFullWidth*j);
				for(k=0; k<d; k++) dPtr1[k] = rgb[k];  // fastest assignment!!
			}
			for(i=mFullWidth-mBorderWidth; i<mFullWidth; i++)
			{
				dPtr1 = dPtr + d*(i+mFullWidth*j);
				for(k=0; k<d; k++) dPtr1[k] = rgb[k];  // fastest assignment!!
			}

			if(mInnerBorder)
			{
				for(k=1; k<mNumTiles[0]; k++)
				{
					xoff = k*(mBorderWidth+mTileWidth);
					for(i=xoff; i<xoff+mBorderWidth; i++)
					{
						dPtr1 = dPtr + d*(i+mFullWidth*j);
						for(k=0; k<d; k++) dPtr1[k] = rgb[k];  // fastest assignment!!
					}
				}
			}
		}
		//
		//  Horizontal lines
		//
		for(j=0; j<mBorderWidth; j++)  // sort of a waste, but it is much simpler this way
		{
			for(i=0; i<mFullWidth; i++)
			{
				dPtr1 = dPtr + d*(i+mFullWidth*j);
				for(k=0; k<d; k++) dPtr1[k] = rgb[k];  // fastest assignment!!
			}
		}

		for(j=mFullHeight-mBorderWidth; j<mFullHeight; j++)
		{
			for(i=0; i<mFullWidth; i++)
			{
				dPtr1 = dPtr + d*(i+mFullWidth*j);
				for(k=0; k<d; k++) dPtr1[k] = rgb[k];  // fastest assignment!!
			}
		}

		if(mInnerBorder)
		{
			for(k=1; k<mNumTiles[1]; k++)
			{
				xoff = k*(mBorderWidth+mTileHeight);
				for(j=xoff; j<xoff+mBorderWidth; j++)
				{
					for(i=0; i<mFullWidth; i++)
					{
						dPtr1 = dPtr + d*(i+mFullWidth*j);
						for(k=0; k<d; k++) dPtr1[k] = rgb[k];  // fastest assignment!!
					}
				}
			}
		}
	}

	//
	//  Background windows 
	//
	for(i=0; i<mBackgroundWindows.Size(); i++)
	{
		if(!mBackgroundWindows[i]->IsEmpty())
		{
			work = true;
			mBackgroundWindows[i]->Draw(outIm);
		}
	}

	if(h.IsStopped()) return false;

	//
	//  Foreground windows are rendered in the inverse order
	//
	for(i=0; i<mForegroundWindows.Size(); i++)
	{
		if(!mForegroundWindows[i]->IsEmpty())
		{
			work = true;
			mForegroundWindows[i]->Draw(outIm);
		}
	}

	if(h.IsStopped()) return false;

	if(work)
	{
		images.Clear();
		images.Add(outIm);
	}
	else
	{
		vm->RenderImages(images);
	}

	mInComposing = false;
	return !h.IsStopped();
}


//
//  Getters
//
int iImageComposer::GetNumForegroundWindows() const
{
	return mForegroundWindows.Size();
}


int iImageComposer::GetBackgroundWindowIndex(int i) const
{
	return mBackgroundWindows[i]->GetWindowNumber(0);
}


int iImageComposer::GetBackgroundWindowIndex2(int i) const
{
	return mBackgroundWindows[i]->GetWindowNumber(1);
}


int iImageComposer::GetBackgroundWindowIndex3(int i) const
{
	return mBackgroundWindows[i]->GetWindowNumber(2);
}


const iString& iImageComposer::GetBackgroundWindowWallpaperFile(int i) const
{
	return mBackgroundWindows[i]->GetWallpaperFile();
}


int iImageComposer::GetForegroundWindowIndex(int i) const
{
	return mForegroundWindows[i]->GetWindowNumber(0);
}


int iImageComposer::GetForegroundWindowIndex2(int i) const
{
	return mForegroundWindows[i]->GetWindowNumber(1);
}


int iImageComposer::GetForegroundWindowIndex3(int i) const
{
	return mForegroundWindows[i]->GetWindowNumber(2);
}


int iImageComposer::GetForegroundWindowPositionX(int i) const
{
	return mForegroundWindows[i]->GetPositionX();
}


int iImageComposer::GetForegroundWindowPositionY(int i) const
{
	return mForegroundWindows[i]->GetPositionY();
}


int iImageComposer::GetForegroundWindowBorderWidth(int i) const
{
	return mForegroundWindows[i]->GetBorderWidth();
}


const iColor& iImageComposer::GetForegroundWindowBorderColor(int i) const
{
	return mForegroundWindows[i]->GetBorderColor();
}


float iImageComposer::GetForegroundWindowScale(int i) const
{
	return mForegroundWindows[i]->GetScale();
}


int iImageComposer::GetForegroundWindowZoomSource(int i) const
{
	if(mForegroundWindows[i]->GetZoomSource() == 0)
	{
		return 0;
	}
	else
	{
		return mForegroundWindows[i]->GetZoomSource()->GetId();
	}
}


bool iImageComposer::GetForegroundWindowZoom4Line(int i) const
{
	return (mForegroundWindows[i]->GetZoomFlag() == iImageComposerForegroundWindow::_4Lines);
}


float iImageComposer::GetForegroundWindowZoomX(int i) const
{
	return mForegroundWindows[i]->GetZoomX();
}


float iImageComposer::GetForegroundWindowZoomY(int i) const
{
	return mForegroundWindows[i]->GetZoomY();
}


float iImageComposer::GetForegroundWindowZoomFactor(int i) const
{
	return mForegroundWindows[i]->GetZoomFactor();
}


//
//  Setters
//
bool iImageComposer::SetNumForegroundWindows(int n)
{
	while(n < mForegroundWindows.Size()) this->RemoveForegroundWindow(n);
	while(n > mForegroundWindows.Size()) this->AddForegroundWindow(this->GetShell()->GetViewModule(0));
	this->UpdateWindowList();
	return true;
}


bool iImageComposer::SetBackgroundWindowIndex(int i, int k)
{
	mBackgroundWindows[i]->SetViewModule((k>=0)?this->GetShell()->GetViewModule(k):0,0);
	this->Update();
	return true;
}


bool iImageComposer::SetBackgroundWindowIndex2(int i, int k)
{
	mBackgroundWindows[i]->SetViewModule((k>=0)?this->GetShell()->GetViewModule(k):0,1);
	this->Update();
	return true;
}


bool iImageComposer::SetBackgroundWindowIndex3(int i, int k)
{
	mBackgroundWindows[i]->SetViewModule((k>=0)?this->GetShell()->GetViewModule(k):0,2);
	this->Update();
	return true;
}


bool iImageComposer::SetBackgroundWindowWallpaperFile(int i, const iString& s)
{
	if(mBackgroundWindows[i]->LoadWallpaperImage(s))
	{
		this->Update();
		return true;
	}
	else return false;
}



bool iImageComposer::SetForegroundWindowIndex(int i, int k)
{
	mForegroundWindows[i]->SetViewModule(this->GetShell()->GetViewModule(k),0);
	return true;
}


bool iImageComposer::SetForegroundWindowIndex2(int i, int k)
{
	mForegroundWindows[i]->SetViewModule(this->GetShell()->GetViewModule(k),1);
	return true;
}


bool iImageComposer::SetForegroundWindowIndex3(int i, int k)
{
	mForegroundWindows[i]->SetViewModule(this->GetShell()->GetViewModule(k),2);
	return true;
}


bool iImageComposer::SetForegroundWindowBorderWidth(int i, int w)
{
	mForegroundWindows[i]->SetBorderWidth(w);
	return true;
}

bool iImageComposer::SetForegroundWindowBorderColor(int i, const iColor& c)
{
	mForegroundWindows[i]->SetBorderColor(c);
	return true;
}


bool iImageComposer::SetForegroundWindowScale(int i, float v)
{
	mForegroundWindows[i]->SetScale(v);
	return true;
}


bool iImageComposer::SetForegroundWindowPositionX(int i, int v)
{
	mForegroundWindows[i]->SetPositionX(v);
	return true;
}


bool iImageComposer::SetForegroundWindowPositionY(int i, int v)
{
	mForegroundWindows[i]->SetPositionY(v);
	return true;
}


bool iImageComposer::SetForegroundWindowZoom4Line(int i, bool b)
{
	mForegroundWindows[i]->SetZoomFlag(b?iImageComposerForegroundWindow::_4Lines:1);
	return true;
}


bool iImageComposer::SetForegroundWindowZoomX(int i, float v)
{
	mForegroundWindows[i]->SetZoomPosition(v,mForegroundWindows[i]->GetZoomY());
	return true;
}

bool iImageComposer::SetForegroundWindowZoomY(int i, float v)
{
	mForegroundWindows[i]->SetZoomPosition(mForegroundWindows[i]->GetZoomX(),v);
	return true;
}

bool iImageComposer::SetForegroundWindowZoomSource(int i, int k)
{
	if(k < 0)
	{
		mForegroundWindows[i]->SetZoomSource(this->GetBackgroundWindow(-k-1));
	}
	else if(k > 0)
	{
		mForegroundWindows[i]->SetZoomSource(this->GetForegroundWindow(k-1));
	}
	else
	{
		mForegroundWindows[i]->SetZoomSource(0);
	}
	return true;
}


bool iImageComposer::SetForegroundWindowZoomFactor(int i, float v)
{
	mForegroundWindows[i]->SetZoomFactor(v);
	return true;
}
