/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iactorviewsubject.h"


#include "iclipplane.h"
#include "ierror.h"
#include "ilookuptable.h"
#include "imaterial.h"
#include "ireplicatedactor.h"
#include "iviewmodule.h"
#include "iviewmoduleeventobservers.h"
#include "iviewsubjectobserver.h"

#include <vtkMath.h>
#include <vtkProperty.h>

//
//  Templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


using namespace iParameter;


//
//  Single instance class
//
iActorViewInstance::iActorViewInstance(iActorViewSubject *owner, int num_actors, float max_opacity) : iViewInstance(owner), mMaxOpacity(max_opacity)
{
	int i;
	iActor *a;

	if(num_actors < 1)
	{
		IBUG_FATAL("SolidViewSubject must have at least 1 Actor.");
	}

	mColor = owner->GetDefaultColor();
	mOpacity = mMaxOpacity;

	for(i=0; i<num_actors; i++)
	{
		if(owner->IsNonReplicating)
		{
			a = iActor::New(owner->GetViewModule()->GetClipPlane()->GetPlanes()); IERROR_CHECK_MEMORY(a);
		}
		else
		{
			a = iReplicatedActor::New(owner); IERROR_CHECK_MEMORY(a);
		}
		a->SetScalarVisibility(false);
		a->GetLookupTable()->SetColor(mColor);
		a->SetPosition(0.0,0.0,0.0);
		a->VisibilityOff();
		a->DragableOff();  // use Dragable to mark Actors of ViewSubjects
		a->GetProperty()->SetOpacity(mOpacity);
		a->GetProperty()->SetInterpolationToGouraud();
		a->GetProperty()->SetColor(owner->GetDefaultColor().ToVTK());
		a->AddMapperObserver(vtkCommand::ProgressEvent,this->GetViewModule()->GetRenderEventObserver());
		this->GetViewModule()->AddObject(a);
		this->Owner()->GetMaterial()->AddProperty(a->GetProperty());
		mActors.Add(a);
	}

	//
	//  Add observer to keep information about this object
	//
	mActors[0]->AddObserver(vtkCommand::UserEvent,owner->mObjectObserver);
}


iActorViewInstance::~iActorViewInstance()
{
	int i;
	for(i=0; i<mActors.Size(); i++)
	{
		this->iViewInstance::Owner()->GetMaterial()->RemoveProperty(mActors[i]->GetProperty());
		this->GetViewModule()->RemoveObject(mActors[i]);
		mActors[i]->Delete();
	}
}


iActorViewSubject* iActorViewInstance::Owner() const
{
	return iRequiredCast<iActorViewSubject>(INFO,this->iViewInstance::Owner());
}


void iActorViewInstance::ShowBody(bool s)
{
	int i;
	for(i=0; i<mActors.Size(); i++) mActors[i]->SetVisibility(s?1:0);
}


void iActorViewInstance::ResetBody()
{
	int i;
	for(i=0; i<mActors.Size(); i++) mActors[i]->VisibilityOff();
}


bool iActorViewInstance::SetColor(const iColor& c)
{
	if(!c.IsValid()) return false;

	mColor = c;
	int i, n = this->Owner()->IsSameColor ? mActors.Size() : 1;
	for(i=0; i<n; i++)
	{
		mActors[i]->GetProperty()->SetColor(c.ToVTK());
		mActors[i]->GetLookupTable()->SetColor(c);
	}
	return true;
}


bool iActorViewInstance::SetOpacity(float o)
{ 
	if(o<0 || o>1) return false;

	mOpacity = (o > mMaxOpacity) ? mMaxOpacity : o;
	int i, n = this->Owner()->IsSameOpacity ? mActors.Size() : 1;
	for(i=0; i<n; i++)
	{
		mActors[i]->GetProperty()->SetOpacity(mOpacity);
	}
	return true;
}


//
// Main class
//
iActorViewSubject::iActorViewSubject(iObject *parent, const iString &fname, const iString &sname, iViewModule *vm, const iDataType &type, unsigned int flags, int minsize, int maxsize) : iViewSubject(parent,fname,sname,vm,type,flags,minsize,maxsize), 
	HasNoColor((flags & ViewObject::Flag::HasNoColor) != 0U), 
	HasNoOpacity((flags & ViewObject::Flag::HasNoOpacity) != 0U), 
	IsSameColor((flags & ViewObject::Flag::IsSameColor) != 0U), 
	IsSameOpacity((flags & ViewObject::Flag::IsSameOpacity) != 0U), 
	iViewSubjectPropertyConstructMacroOP(!HasNoColor,Color,iActorViewInstance,Color,c),
	iViewSubjectPropertyConstructMacroOP(!HasNoOpacity,Float,iActorViewInstance,Opacity,o)
{
	mDefaultColor = iColor::IFrIT();
}


iActorViewSubject::~iActorViewSubject()
{
}


//
//  Helper class
//
iPropPlacementHelper::iPropPlacementHelper(iViewModule *vm) : mPosition(vm), mSize(vm)
{
	mDirection[0] = 0.0;
	mDirection[1] = 0.0;
	mDirection[2] = 1.0;
}


iPropPlacementHelper::~iPropPlacementHelper()
{
}


bool iPropPlacementHelper::SetDirection(const iVector3D& n)
{
	if(n[0]!=0.0 || n[1]!=0.0 || n[2]!=0.0)
	{
		mDirection = n;
		vtkMath::Normalize(mDirection);
		this->UpdateDirection();
		return true;
	}
	else return false;
}


bool iPropPlacementHelper::SetBoxPosition(const iVector3D &v)
{
	mPosition.SetBoxValue(v);
	this->SetPosition(mPosition);
	return true;
}


const iVector3D& iPropPlacementHelper::GetBoxPosition() const
{
	return mPosition.BoxValue();
}


void iPropPlacementHelper::SetPosition(const iPosition &p)
{
	mPosition = p;
	this->UpdatePosition();
}


bool iPropPlacementHelper::SetBoxSize(double p)
{
	mSize.SetBoxValue(p);
	if(mSize > 0.0)
	{
		this->UpdateSize();
		return true;
	}
	else return false;
}


double iPropPlacementHelper::GetBoxSize() const
{
	return mSize.BoxValue();
}


void iPropPlacementHelper::SetSize(const iDistance &s)
{
	double v = s;
	this->SetSize(v);
}


void iPropPlacementHelper::SetSize(double s)
{
	if(s < 1.1)
	{
		mSize = s;
		this->UpdateSize();
	}
}

