/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iviewsubjectpipeline.h"


#include "ierror.h"
#include "iviewsubject.h"

#include <vtkDataSet.h>
#include <vtkPolyData.h>

//
//  Templates
//
#include "iarray.tlh"
#include "igenericfilter.tlh"


iViewSubjectPipeline::iViewSubjectPipeline(iViewInstance *owner, int numInputs) : iGenericFilter<vtkDataSetAlgorithm,vtkDataSet,vtkPolyData>(owner,numInputs,true)
{
	mOwner = owner;
	mGlobalInput = 0;
}


iViewSubjectPipeline::~iViewSubjectPipeline()
{
	this->SetGlobalInput(0);
	while(mInternalFilters.Size() > 0) mInternalFilters.RemoveLast()->Delete();
}


void iViewSubjectPipeline::SetGlobalInput(vtkDataSet *input)
{
	if(mGlobalInput != 0)
	{
		mGlobalInput->UnRegister(this);
	}

	mGlobalInput = input;
	
	if(mGlobalInput != 0)
	{
		mGlobalInput->Register(this);
	}
	
	this->Modified();
}


float iViewSubjectPipeline::GetMemorySize()
{
	int i, j;
	float s = this->iGenericFilter<vtkDataSetAlgorithm,vtkDataSet,vtkPolyData>::GetMemorySize();

	for(i=0; i<mInternalFilters.Size(); i++)
	{
		for(j=0; j<mInternalFilters[i]->GetNumberOfOutputPorts(); j++)
		{
			vtkDataObject *d = this->GetFilterOutputIfPresent(mInternalFilters[i],j);
			if(d != 0) s += d->GetActualMemorySize();
		}
	}

	return s;
}


void iViewSubjectPipeline::RemoveInternalData()
{
	int i, j;
	for(i=0; i<mInternalFilters.Size(); i++)
	{
		for(j=0; j<mInternalFilters[i]->GetNumberOfOutputPorts(); j++)
		{
			vtkDataObject *d = this->GetFilterOutputIfPresent(mInternalFilters[i],j);
			if(d != 0) d->Initialize();
		}
	}
}


void iViewSubjectPipeline::RemoveInternalDataAndOutputs()
{
	int i;

	this->RemoveInternalData();
	for(i=0; this->GetNumberOfOutputPorts(); i++)
	{
		vtkDataObject *d = this->GetFilterOutputIfPresent(this,i);
		if(d != 0) d->Initialize();
	}
}


void iViewSubjectPipeline::UpdateContents()
{
	int i;
	Key::RegistryType &reg(Key::Registry()); // cache

	for(i=0; i<reg.Size(); i++)
	{
		if(this->IsA(reg[i]->ClassName()) != 0)
		{
			reg[i]->Update(this);
		}
	}
}


void iViewSubjectPipeline::UpdateContents(const Key &key)
{
	Key::RegistryType &reg(Key::Registry()); // cache

#ifndef I_NO_CHECK
	if(reg.Find(&key) == -1)
	{
		IBUG_FATAL("Using an unregistered key.");
	}
	if(this->IsA(key.ClassName()) == 0)
	{
		IBUG_FATAL("Wrong key for this class.");
	}
#endif
	key.Update(this);
}


//
//  iViewSubjectPipeline::Key class
//
iGenericOrderedArray<const iViewSubjectPipeline::Key*,iString>& iViewSubjectPipeline::Key::Registry()
{
	static iGenericOrderedArray<const iViewSubjectPipeline::Key*,iString> arr(iViewSubjectPipeline::Key::Lookup);
	return arr;
}


const iString& iViewSubjectPipeline::Key::Lookup(const iViewSubjectPipeline::Key* const &ptr)
{
	return ptr->Tag();
}



iViewSubjectPipeline::Key::Key(UpdaterType updater, const char *cname, const char *mname) : mUpdater(updater), mClassName(cname), mMethodName(mname), mTag(cname+iString("::")+mname)
{
	IASSERT(mUpdater);

#ifndef I_NO_CHECK
	if(Registry().Find(this) != -1)
	{
		IBUG_ERROR("Registering the key that has been already registered (probably, incorrect tag).");
	}
#endif
	Registry().AddUnique(this);
}


iViewSubjectPipeline::Key::~Key()
{
#ifndef I_NO_CHECK
	if(Registry().Find(this) == -1)
	{
		IBUG_ERROR("Unregistering a key that has never been registered (probably, incorrect tag).");
	}
#endif
	Registry().Remove(this);
}


void iViewSubjectPipeline::Key::Update(iViewSubjectPipeline *pipeline) const
{
	if(pipeline != 0)
	{
		(pipeline->*mUpdater)();
	}
}

