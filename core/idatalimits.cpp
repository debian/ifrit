/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
// Class containing data limits for a single DataSubject
//
#include "idatalimits.h"


#include "idatasubject.h"
#include "ifileloader.h"
#include "ihistogrammaker.h"
#include "istretch.h"
#include "iviewmodule.h"

//
//  templates
//
#include "iarray.tlh"


using namespace iParameter;


iDataLimits* iDataLimits::New(const iDataSubject *subject)
{
	IASSERT(subject);
	return new iDataLimits(subject,false);
}


iDataLimits::iDataLimits(const iDataSubject *subject, bool isfrozen) : mSubject(subject), mIsFrozen(isfrozen)
{
	IASSERT(subject);

	mBlockNotifications = mIsActive = false;

	if(!mIsFrozen)
	{
		//
		//  By default add just 1 var
		//
		this->DeclareVars(1);
	}
}


iDataLimits::~iDataLimits()
{
}


const iDataType& iDataLimits::GetDataType() const
{
	return mSubject->GetDataType();
}


bool iDataLimits::Activate(int nvars)
{
	mVars.Clear();
	if(nvars >= 0)
	{
		mIsActive = true;
		return this->AddVars(nvars);
	}
	else
	{
		mIsActive = false;
		return true;
	}
}


bool iDataLimits::AddVars(int nvars) // add first nvars Vars
{
	int var;
	for(var=0; var<nvars; var++)
	{
		this->AddOneVar();
	}
	return true;
}


void iDataLimits::AddOneVar(int ndecl)
{
	if(ndecl<-1 || ndecl>=mDeclaredVars.Size())
	{
		IBUG_ERROR("Invalid index for a declared variable.");
		return;
	}

	if(ndecl == -1)
	{
		//
		//  Auto-expand the declared vars if needed
		//
		if(mVars.Size() == mDeclaredVars.Size())
		{
			if(mIsFrozen)
			{
				IBUG_WARN("Unable to modify variables for frozen Limits.");
				return;
			}
			else
			{
				Var tmp(mDeclaredVars.Size());
				mDeclaredVars.Add(tmp);
			}
		}
		ndecl = mVars.Size();
	}

	mVars.AddUnique(&mDeclaredVars[ndecl]);
}


bool iDataLimits::Resize(int n)
{
	if(mIsFrozen) return false; else
	{
		this->DeclareVars(n);
		return true;
	}
}


void iDataLimits::DeclareVars(int n)
{
	if(n < 1) n = 1;

	while(mDeclaredVars.Size() > n) mDeclaredVars.RemoveLast();
	while(mDeclaredVars.Size() < n)
	{
		Var tmp(mDeclaredVars.Size());
		mDeclaredVars.Add(tmp);
	}
}


void iDataLimits::AddAllDeclared(bool s)
{
	int i;

	mVars.Clear();

	if(s)
	{
		mVarsBackup.Clear();
		for(i=0; i<mVars.Size(); i++) mVarsBackup.Add(mVars[i]);
		
		for(i=0; i<mDeclaredVars.Size(); i++) mVars.Add(&mDeclaredVars[i]);
	}
	else
	{
		mVars.Clear();
		for(i=0; i<mVarsBackup.Size(); i++) mVars.Add(mVarsBackup[i]);
	}
}


int iDataLimits::GetNumVars() const 
{ 
	if(mIsActive) return mVars.Size(); else return mDeclaredVars.Size();
}


iDataLimits::Var* iDataLimits::CacheVar(int n)
{
	if(mIsActive)
	{
		if(n>=0 && n<mVars.Size()) return mVars[n];
	}
	else
	{
		if(n>=0 && n<mDeclaredVars.Size()) return &(mDeclaredVars[n]);
	}
	return 0;
}


const iDataLimits::Var* iDataLimits::CacheVar(int n) const
{
	if(mIsActive)
	{
		if(n>=0 && n<mVars.Size()) return mVars[n];
	}
	else
	{
		if(n>=0 && n<mDeclaredVars.Size()) return &(mDeclaredVars[n]);
	}
	return 0;
}


bool iDataLimits::SetRange(int n, const iPair& p)
{
	Var *v = this->CacheVar(n);
	if(v != 0)
	{
		v->Range = p;

		if(v->Range.Max < v->Range.Min)
		{
			float tmp = v->Range.Max;
			v->Range.Max = v->Range.Min;
			v->Range.Min = tmp;
		}
		else if(v->Range.Max == v->Range.Min)
		{
			float q = v->Range.Max;
			if(q > 0.0)
			{
				v->Range.Min = q*0.99;
				v->Range.Max = q*1.01;
			}
			else if(q < 0.0)
			{
				v->Range.Min = q*1.01;
				v->Range.Max = q*0.99;
			}
			else
			{
				v->Range.Min = -0.01;
				v->Range.Max = 0.01;
			}
		}

		if(!mBlockNotifications)
		{
			mSubject->SyncWithLimits(n,LimitsChangeMode::Range);
		}
		return true;
	}
	else return false;
}


bool iDataLimits::SetName(int n, const iString& name)
{
	Var *v = this->CacheVar(n);
	if(v != 0)
	{
		if(mIsFrozen) return (v->Name == name);
		v->Name = name;
		if(!mBlockNotifications)
		{
			mSubject->SyncWithLimits(n,LimitsChangeMode::Name);
		}
		return true;
	}
	else return false;
}


bool iDataLimits::SetStretch(int n, const iString& str)
{
	Var *v = this->CacheVar(n);
	if(v != 0)
	{
		if(!v->StretchFixed)
		{
			int id = iStretch::GetId(str);
			if(id != -1)
			{
				v->StretchId = id;
				if(!mBlockNotifications)
				{
					mSubject->SyncWithLimits(n,LimitsChangeMode::Stretch);
				}
				return true;
			}
			return false;
		}
		else return (iStretch::GetName(v->StretchId).Lower() == str.Lower());
	}
	else return false;
}


const iString& iDataLimits::GetStretch(int n) const
{
	static const iString null;
	const Var *v = this->CacheVar(n);
	if(v != 0) return iStretch::GetName(v->StretchId); else return null;
}


float iDataLimits::GetStretchedMax(int n) const
{
	const Var *v = this->CacheVar(n);
	if(v != 0) return iStretch::Apply(v->Range.Max,v->StretchId,true); else return iMath::_LargeFloat;
}


float iDataLimits::GetStretchedMin(int n) const
{
	const Var *v = this->CacheVar(n);
	if(v != 0) return iStretch::Apply(v->Range.Min,v->StretchId,false); else return -iMath::_LargeFloat;
}


const iPair iDataLimits::GetDataRange(int n) const
{
	static const iPair none(-iMath::_LargeFloat,iMath::_LargeFloat);

	if(n>=0 && n<mVars.Size())
	{
		if(mSubject->IsThereData())
		{
			return mSubject->GetHistogramMaker()->GetRange(n);
		}
		else return this->GetRange(n);
	}
	else return none;
}


float iDataLimits::GetDataMax(int n) const
{
	if(n>=0 && n<mVars.Size())
	{
		if(mSubject->IsThereData())
		{
			return mSubject->GetHistogramMaker()->GetMax(n).Value;
		}
		else return this->GetMax(n);
	}
	else return iMath::_LargeFloat;
}


float iDataLimits::GetDataMin(int n) const
{
	if(n>=0 && n<mVars.Size())
	{
		if(mSubject->IsThereData())
		{
			return mSubject->GetHistogramMaker()->GetMin(n).Value;
		}
		else return this->GetMin(n);
	}
	else return -iMath::_LargeFloat;
}


iDataLimits::Var::Var(int n)
{
	Range = iPair(0.1,10);
	StretchId = iStretch::Lin;
	Rank = 0U;
	Included = true;
	StretchFixed = false;

	Name = "Variable";
	if(n >= 0) Name += (" #"+iString::FromNumber(n+1));
}

