/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "imath.h"


#include "ierror.h"

#include <vtkMath.h>
#include <vtkMinimalStandardRandomSequence.h>


namespace iMath
{
	bool Intersect3Planes(double *p1, double *p2, double *p3, double *x)
	{
		int i;
		double atmp[9], *a[3];

		a[0] = atmp + 0;
		a[1] = atmp + 3;
		a[2] = atmp + 6;

		for(i=0; i<3; i++)
		{
			a[0][i] = p1[i];
			a[1][i] = p2[i];
			a[2][i] = p3[i];
		}
		x[0] = -p1[3];
		x[1] = -p2[3];
		x[2] = -p3[3];

		return (vtkMath::SolveLinearSystem(a,x,3) != 0);
	}

	Random::Random(int seed)
	{
		mGenerator = vtkMinimalStandardRandomSequence::New(); IERROR_CHECK_MEMORY(mGenerator);
		mGenerator->SetSeed(seed);
	}

	Random::~Random()
	{
		mGenerator->Delete();
	}
	
	double Random::NextValue()
	{
		mGenerator->Next();
		return mGenerator->GetValue();
	}
};

