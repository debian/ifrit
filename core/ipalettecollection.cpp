/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ipalettecollection.h"


#include "iimage.h"
#include "ipalette.h"
#include "ipointer.h"
#include "ipiecewisefunction.h"
#include "ishell.h"
#include "istring.h"
#include "ivtk.h"

#include <vtkImageData.h>
#include <vtkPNGWriter.h>

//
//  templates
//
#include "iarray.tlh"
#include "iobjectcollection.tlh"
#include "iproperty.tlh"


iPaletteCollection* iPaletteCollection::mGlobal = 0;


//
//  Helper functions hidden in a private namespace
//
namespace iPaletteCollection_Private
{
	int Decode(int id, bool &r)
	{
		if(id < 0)
		{
			r = true;
			return -id - 1;
		}
		else
		{
			r = false;
			return id - 1;
		}
	}
};


using namespace iPaletteCollection_Private;


//
//  Main class
//
iPaletteCollection* iPaletteCollection::New(iShell *s)
{
	static iString LongName("Palette");
	static iString ShortName("pal");

	IASSERT(s);

	iPaletteCollection *tmp = new iPaletteCollection(s,LongName,ShortName);
	tmp->CreateDefaultPalettes();

	if(mGlobal == 0) mGlobal = tmp;

	return tmp;
}


iPaletteCollection::iPaletteCollection(iShell *s, const iString &fname, const iString &sname) : iObjectCollectionFixed<iPalette>(s,fname,sname,0), iShellComponent(s),
		Create(iObject::Self(),static_cast<iPropertyAction::CallerType>(&iPaletteCollection::CreateMember),"New","new")
{
	mRenderMode = iParameter::RenderMode::NoRender;
}


iPaletteCollection::~iPaletteCollection()
{
	mGlobal = 0;
}


iPalette* iPaletteCollection::NewObject()
{
	return new iPalette(this,this->LongName(),this->ShortName());
}


iPalette* iPaletteCollection::GetPalette(int n) const
{
	return iRequiredCast<iPalette>(INFO,this->GetMember(n));
}

	
vtkImageData* iPaletteCollection::GetImageData(int id) const
{
	bool r;
	int n = Decode(id,r);
	if(n>=0 && n<this->Size())
	{
		const iImage *im = this->GetPalette(n)->GetImage(r?2:1);
		if(im == 0) return 0;
		return im->DataObject();
	}
	else return 0;
}


vtkLookupTable* iPaletteCollection::GetLookupTable(int id) const
{
	bool r;
	int n = Decode(id,r);
	if(n>=0 && n<this->Size()) return this->GetPalette(n)->GetLookupTable(r); else return 0;
}


vtkColorTransferFunction* iPaletteCollection::GetColorTransferFunction(int id) const
{
	bool r;
	int n = Decode(id,r);
	if(n>=0 && n<this->Size()) return this->GetPalette(n)->GetColorTransferFunction(r); else return 0;
}


void iPaletteCollection::WritePaletteImages(iShell *s, const iString& dirname)
{
	int i;
	iString fname;
	vtkPNGWriter *w = vtkPNGWriter::New(); IERROR_CHECK_MEMORY(w);
	iPaletteCollection *c = iPaletteCollection::New(s); IERROR_CHECK_MEMORY(c);

	for(i=0; i<c->Size(); i++)
	{
		vtkImageData *im = vtkImageData::SafeDownCast(c->GetPalette(i)->GetImage()->DataObject()); IERROR_CHECK_MEMORY(im);

		fname = dirname + "/pal_" + c->GetPalette(i)->GetName() + ".png";
		fname.Replace(" ","");
		fname.Replace("-","");
		w->SetFileName(fname.ToCharPointer());
		w->SetInputData(im);
		w->Write();
	}

	c->Delete();
	w->Delete();
}


void iPaletteCollection::CreateDefaultPalettes()
{
	iPalette* tmp;
	iPiecewiseFunction *r, *g, *b;

	if(this->CreateMember())
	{
		tmp = this->GetPalette(mChildren.MaxIndex());
		tmp->SetName("Rainbow");
		r = tmp->mRed;
		g = tmp->mGreen;
		b = tmp->mBlue;
		r->MovePoint(0,0.0,0.0); g->MovePoint(0,0.0,0.0); b->MovePoint(0,0.0,0.1);
		r->MovePoint(1,1.0,1.0); g->MovePoint(1,1.0,0.9); b->MovePoint(1,1.0,0.9);
		r->AddPoint(1.0/6.0,0.0); g->AddPoint(1.0/6.0,0.0); b->AddPoint(1.0/6.0,1.0);
		r->AddPoint(2.0/6.0,0.0); g->AddPoint(2.0/6.0,1.0); b->AddPoint(2.0/6.0,1.0);
		r->AddPoint(3.0/6.0,0.0); g->AddPoint(3.0/6.0,1.0); b->AddPoint(3.0/6.0,0.0);
		r->AddPoint(4.0/6.0,1.0); g->AddPoint(4.0/6.0,1.0); b->AddPoint(4.0/6.0,0.0);
		r->AddPoint(5.0/6.0,1.0); g->AddPoint(5.0/6.0,0.0); b->AddPoint(5.0/6.0,0.0);
		tmp->Update();
	}

	if(this->CreateMember())
	{
		tmp = this->GetPalette(mChildren.MaxIndex());
		tmp->SetName("Isolum");
		r = tmp->mRed;
		g = tmp->mGreen;
		b = tmp->mBlue;
		r->MovePoint(0,0.0,90.0/255); g->MovePoint(0,0.0,190.0/255); b->MovePoint(0,0.0,245.0/255);
		r->MovePoint(1,1.0,180.0/255); g->MovePoint(1,1.0,180.0/255); b->MovePoint(1,1.0,0);
		r->AddPoint(0.25,157.0/255); g->AddPoint(0.25,157.0/255); b->AddPoint(0.25,200.0/255);
		r->AddPoint(0.50,220.0/255); g->AddPoint(0.50,150.0/255); b->AddPoint(0.50,130.0/255);
		r->AddPoint(0.67,245.0/255); g->AddPoint(0.67,120.0/255); b->AddPoint(0.67, 80.0/255);
		tmp->Update();
	}

	if(this->CreateMember())
	{
		tmp = this->GetPalette(mChildren.MaxIndex());
		tmp->SetName("Ametrine");
		r = tmp->mRed;
		g = tmp->mGreen;
		b = tmp->mBlue;
		r->MovePoint(0,0.0,30.0/255); g->MovePoint(0,0.0,60.0/255); b->MovePoint(0,0.0,150.0/255);
		r->MovePoint(1,1.0,220.0/255); g->MovePoint(1,1.0,220.0/255); b->MovePoint(1,1.0,0);
		r->AddPoint(0.27,180.0/255); g->AddPoint(0.27,90.0/255); b->AddPoint(0.27,155.0/255);
		r->AddPoint(0.50,230.0/255); g->AddPoint(0.50,85.0/255); b->AddPoint(0.50,65.0/255);
		tmp->Update();
	}

	if(this->CreateMember())
	{
		tmp = this->GetPalette(mChildren.MaxIndex());
		tmp->SetName("Temperature");
		r = tmp->mRed;
		g = tmp->mGreen;
		b = tmp->mBlue;
		r->MovePoint(0,0.0,0.3); g->MovePoint(0,0.0,0.0); b->MovePoint(0,0.0,0.0);
		r->MovePoint(1,1.0,1.0); g->MovePoint(1,1.0,0.8); b->MovePoint(1,1.0,0.8);
		r->AddPoint(0.7,1.0); g->AddPoint(0.6,0.0); b->AddPoint(0.8,0.0);
		tmp->Update();
	}

	if(this->CreateMember())
	{
		tmp = this->GetPalette(mChildren.MaxIndex());
		tmp->SetName("Greyscale");
		r = tmp->mRed;
		g = tmp->mGreen;
		b = tmp->mBlue;
		r->MovePoint(0,0.0,0.0); g->MovePoint(0,0.0,0.0); b->MovePoint(0,0.0,0.0);
		r->MovePoint(1,1.0,1.0); g->MovePoint(1,1.0,1.0); b->MovePoint(1,1.0,1.0);
		tmp->Update();
	}

	if(this->CreateMember())
	{
		tmp = this->GetPalette(mChildren.MaxIndex());
		tmp->SetName("Blue-white");
		r = tmp->mRed;
		g = tmp->mGreen;
		b = tmp->mBlue;
		r->MovePoint(0,0.0,0.0); g->MovePoint(0,0.0,0.0); b->MovePoint(0,0.0,0.1);
		r->MovePoint(1,1.0,0.9); g->MovePoint(1,1.0,0.9); b->MovePoint(1,1.0,1.0);
		r->AddPoint(0.75,0.0); g->AddPoint(0.38,0.0); b->AddPoint(0.78,1.0);
		tmp->Update();
	}

	if(this->CreateMember())
	{
		tmp = this->GetPalette(mChildren.MaxIndex());
		tmp->SetName("Prizm");
		r = tmp->mRed;
		g = tmp->mGreen;
		b = tmp->mBlue;
		r->MovePoint(0,0.0,0.0); g->MovePoint(0,0.0,0.0); b->MovePoint(0,0.0,0.0);
		r->MovePoint(1,1.0,0.0); g->MovePoint(1,1.0,0.0); b->MovePoint(1,1.0,0.0);
		r->AddPoint(0.25,1.0); g->AddPoint(0.25,0.0); b->AddPoint(0.25,0.0);
		r->AddPoint(0.50,0.0); g->AddPoint(0.50,1.0); b->AddPoint(0.50,0.0);
		r->AddPoint(0.75,0.0); g->AddPoint(0.75,0.0); b->AddPoint(0.75,1.0);
		tmp->Update();
	}

	if(this->CreateMember())
	{
		tmp = this->GetPalette(mChildren.MaxIndex());
		tmp->SetName("Green-white");
		r = tmp->mRed;
		g = tmp->mGreen;
		b = tmp->mBlue;
		r->MovePoint(0,0.0,0.0); g->MovePoint(0,0.0,0.1); b->MovePoint(0,0.0,0.0);
		r->MovePoint(1,1.0,1.0); g->MovePoint(1,1.0,1.0); b->MovePoint(1,1.0,1.0);
		r->AddPoint(0.375,0.0); 
		b->AddPoint(0.750,0.0); 
		tmp->Update();
	}

	if(this->CreateMember())
	{
		tmp = this->GetPalette(mChildren.MaxIndex());
		tmp->SetName("Blue-red");
		r = tmp->mRed;
		g = tmp->mGreen;
		b = tmp->mBlue;
		r->MovePoint(0,0.0,0.0); g->MovePoint(0,0.0,0.0); b->MovePoint(0,0.0,0.0);
		r->MovePoint(1,1.0,1.0); g->MovePoint(1,1.0,0.0); b->MovePoint(1,1.0,0.0);
		r->AddPoint(0.25,0.0); g->AddPoint(0.25,1.0); b->AddPoint(0.25,1.0);
		r->AddPoint(0.5,0.0); g->AddPoint(0.5,0.0); b->AddPoint(0.5,1.0);
		r->AddPoint(0.75,1.0); g->AddPoint(0.75,0.0); b->AddPoint(0.75,1.0);
		tmp->Update();
	}

	if(this->CreateMember())
	{
		tmp = this->GetPalette(mChildren.MaxIndex());
		tmp->SetName("Stern");
		r = tmp->mRed;
		g = tmp->mGreen;
		b = tmp->mBlue;
		r->MovePoint(0,0.0,0.0); g->MovePoint(0,0.0,0.0); b->MovePoint(0,0.0,0.0);
		r->MovePoint(1,1.0,1.0); g->MovePoint(1,1.0,1.0); b->MovePoint(1,1.0,1.0);
		r->AddPoint(0.05,1.0);
		r->AddPoint(0.25,0.0);
		b->AddPoint(0.50,1.0);
		b->AddPoint(0.75,0.0);
		tmp->Update();
	}

	if(this->CreateMember())
	{
		tmp = this->GetPalette(mChildren.MaxIndex());
		tmp->SetName("Haze");
		r = tmp->mRed;
		g = tmp->mGreen;
		b = tmp->mBlue;
		r->MovePoint(0,0.0,1.0); g->MovePoint(0,0.0,0.8); b->MovePoint(0,0.0,1.0);
		r->MovePoint(1,1.0,1.0); g->MovePoint(1,1.0,0.8); b->MovePoint(1,1.0,0.0);
		r->AddPoint(0.5,0.0); g->AddPoint(0.5,0.1); b->AddPoint(0.5,0.5);
		tmp->Update();
	}

	if(this->CreateMember())
	{
		tmp = this->GetPalette(mChildren.MaxIndex());
		tmp->SetName("Starlight");
		r = tmp->mRed;
		g = tmp->mGreen;
		b = tmp->mBlue;
		r->MovePoint(0,0.0,0.5); g->MovePoint(0,0.0,0.0); b->MovePoint(0,0.0,0.0);
		r->MovePoint(1,1.0,1.0); g->MovePoint(1,1.0,1.0); b->MovePoint(1,1.0,0.7);
		r->AddPoint(0.5,0.9); g->AddPoint(0.5,0.7); b->AddPoint(0.5,0.2);
		tmp->Update();
	}

	if(this->CreateMember())
	{
		tmp = this->GetPalette(mChildren.MaxIndex());
		tmp->SetName("2 color");
		r = tmp->mRed;
		g = tmp->mGreen;
		b = tmp->mBlue;
		r->MovePoint(0,0.0,1.0); g->MovePoint(0,0.0,0.0); b->MovePoint(0,0.0,0.0);
		r->MovePoint(1,1.0,0.0); g->MovePoint(1,1.0,0.0); b->MovePoint(1,1.0,1.0);
		r->AddPoint(0.4999,1.0); g->AddPoint(0.4999,0.0); b->AddPoint(0.4999,0.0);
		r->AddPoint(0.5001,0.0); g->AddPoint(0.5001,0.0); b->AddPoint(0.5001,1.0);
		tmp->Update();
	}

	if(this->CreateMember())
	{
		tmp = this->GetPalette(mChildren.MaxIndex());
		tmp->SetName("3 color");
		r = tmp->mRed;
		g = tmp->mGreen;
		b = tmp->mBlue;
		r->MovePoint(0,0.0,1.0); g->MovePoint(0,0.0,0.0); b->MovePoint(0,0.0,0.0);
		r->MovePoint(1,1.0,0.0); g->MovePoint(1,1.0,0.0); b->MovePoint(1,1.0,1.0);
		r->AddPoint(0.3332,1.0); g->AddPoint(0.3332,0.0); b->AddPoint(0.3332,0.0);
		r->AddPoint(0.3334,0.0); g->AddPoint(0.3334,1.0); b->AddPoint(0.3334,0.0);
		r->AddPoint(0.6665,0.0); g->AddPoint(0.6665,1.0); b->AddPoint(0.6665,0.0);
		r->AddPoint(0.6667,0.0); g->AddPoint(0.6667,0.0); b->AddPoint(0.6667,1.0);
		tmp->Update();
	}

	if(this->CreateMember())
	{
		tmp = this->GetPalette(mChildren.MaxIndex());
		tmp->SetName("4 color");
		r = tmp->mRed;
		g = tmp->mGreen;
		b = tmp->mBlue;
		r->MovePoint(0,0.0,1.0); g->MovePoint(0,0.0,0.0); b->MovePoint(0,0.0,0.0);
		r->MovePoint(1,1.0,0.0); g->MovePoint(1,1.0,0.0); b->MovePoint(1,1.0,1.0);
		r->AddPoint(0.2499,1.0); g->AddPoint(0.2499,0.0); b->AddPoint(0.2499,0.0);
		r->AddPoint(0.2501,1.0); g->AddPoint(0.2501,1.0); b->AddPoint(0.2501,0.0);
		r->AddPoint(0.4999,1.0); g->AddPoint(0.4999,1.0); b->AddPoint(0.4999,0.0);
		r->AddPoint(0.5001,0.0); g->AddPoint(0.5001,1.0); b->AddPoint(0.5001,0.0);
		r->AddPoint(0.7499,0.0); g->AddPoint(0.7499,1.0); b->AddPoint(0.7499,0.0);
		r->AddPoint(0.7501,0.0); g->AddPoint(0.7501,0.0); b->AddPoint(0.7501,1.0);
		tmp->Update();
	}

	if(this->CreateMember())
	{
		tmp = this->GetPalette(mChildren.MaxIndex());
		tmp->SetName("8 color");
		r = tmp->mRed;
		g = tmp->mGreen;
		b = tmp->mBlue;
		r->MovePoint(0,0.0,1.0); g->MovePoint(0,0.0,0.0); b->MovePoint(0,0.0,0.0);
		r->MovePoint(1,1.0,1.0); g->MovePoint(1,1.0,0.0); b->MovePoint(1,1.0,1.0);
		r->AddPoint(0.1249,1.0); g->AddPoint(0.1249,0.0); b->AddPoint(0.1249,0.0);
		r->AddPoint(0.1251,1.0); g->AddPoint(0.1251,0.5); b->AddPoint(0.1251,0.0);
		r->AddPoint(0.2499,1.0); g->AddPoint(0.2499,0.5); b->AddPoint(0.2499,0.0);
		r->AddPoint(0.2501,1.0); g->AddPoint(0.2501,1.0); b->AddPoint(0.2501,0.0);
		r->AddPoint(0.3749,1.0); g->AddPoint(0.3749,1.0); b->AddPoint(0.3749,0.0);
		r->AddPoint(0.3751,0.0); g->AddPoint(0.3751,1.0); b->AddPoint(0.3751,0.0);
		r->AddPoint(0.4999,0.0); g->AddPoint(0.4999,1.0); b->AddPoint(0.4999,0.0);
		r->AddPoint(0.5001,0.0); g->AddPoint(0.5001,1.0); b->AddPoint(0.5001,1.0);
		r->AddPoint(0.6249,0.0); g->AddPoint(0.6249,1.0); b->AddPoint(0.6249,1.0);
		r->AddPoint(0.6251,0.0); g->AddPoint(0.6251,0.5); b->AddPoint(0.6251,1.0);
		r->AddPoint(0.7499,0.0); g->AddPoint(0.7499,0.5); b->AddPoint(0.7499,1.0);
		r->AddPoint(0.7501,0.0); g->AddPoint(0.7501,0.0); b->AddPoint(0.7501,1.0);
		r->AddPoint(0.8749,0.0); g->AddPoint(0.8749,0.0); b->AddPoint(0.8749,1.0);
		r->AddPoint(0.8751,1.0); g->AddPoint(0.8751,0.0); b->AddPoint(0.8751,1.0);
		tmp->Update();
	}
}
