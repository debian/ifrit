/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "icameraorthoactor.h"


#include "ierror.h"
#include "ioverlayhelper.h"
#include "itextactor.h"

#include <vtkAxisActor2D.h>
#include <vtkCamera.h>
#include <vtkMath.h>
#include <vtkRenderer.h>
#include <vtkProperty2D.h>
#include <vtkTextProperty.h>

//
//  Templates
//
#include "iarray.tlh"
#include "igenericprop.tlh"


iCameraOrthoActor* iCameraOrthoActor::New(iRenderTool *rt)
{
	IASSERT(rt);
	return new iCameraOrthoActor(rt);
}


iCameraOrthoActor::iCameraOrthoActor(iRenderTool *rt) : iGenericProp<vtkActor2D>(false), mOverlayHelper(rt)
{
	int i;
	for(i=0; i<2; i++)
	{
		mLabels[i] = iTextActor::New(rt); IERROR_CHECK_MEMORY(mLabels[i]);
		this->AppendComponent(mLabels[i]);
		mLabels[i]->SetBold(true);
		mActors[i] = vtkAxisActor2D::New(); IERROR_CHECK_MEMORY(mActors[i]);
		this->AppendComponent(mActors[i]);
        mActors[i]->GetPoint1Coordinate()->SetCoordinateSystemToNormalizedViewport(); 
        mActors[i]->GetPoint2Coordinate()->SetCoordinateSystemToNormalizedViewport(); 
		mActors[i]->SetPoint1(0.05,0.05);
		mActors[i]->TitleVisibilityOff();
		mActors[i]->LabelVisibilityOff();
		mActors[i]->TickVisibilityOff();
		mActors[i]->GetProperty()->SetColor(0.0,0.0,0.0);
		mActors[i]->GetProperty()->SetLineWidth(2);
	}
	mActors[0]->SetPoint2(0.15,0.05);
	mActors[1]->SetPoint2(0.05,0.15);
}


iCameraOrthoActor::~iCameraOrthoActor()
{
	int i;
	for(i=0; i<2; i++)
	{
        mActors[i]->Delete();
		mLabels[i]->Delete();
	}
}


void iCameraOrthoActor::UpdateGeometry(vtkViewport* vp)
{
	static const double orts[6][3] = 
	{
		{ -1.0,  0.0,  0.0 },
		{  1.0,  0.0,  0.0 },
		{  0.0, -1.0,  0.0 },
		{  0.0,  1.0,  0.0 },
		{  0.0,  0.0, -1.0 },
		{  0.0,  0.0,  1.0 } 
	};

	vtkCamera *cam = mOverlayHelper->GetCamera(vp);
	if(cam == 0)
	{
		this->Disable();
		return;
	}

	//
	//  Check direction of projection
	//
	double d, vx[3], *vy, dmax;
	int i, imax = 5, jmax = 3;

	dmax = 0.0;
	vtkMath::Cross(cam->GetViewUp(),cam->GetViewPlaneNormal(),vx);
	for(i=0; i<6; i++)
	{
		d = vtkMath::Dot(orts[i],vx);
		if(d > dmax)
		{
			dmax = d;
			imax = i;
		}
	}
	if(dmax < 0.99999)
	{
		this->Disable();
		return;
	}

	//
	//  Check view up.
	//
	dmax = 0.0;
	vy = cam->GetViewUp();
	for(i=0; i<6; i++)
	{
		d = vtkMath::Dot(orts[i],vy);
		if(d > dmax)
		{
			dmax = d;
			jmax = i;
		}
	}
	if(dmax < 0.99999)
	{
		this->Disable();
		return;
	}

	//
	//  We are orthogonal
	//
	double as[2];
	vp->GetAspect(as);
	if(as[0] > 1.0) 
	{
		as[1] /= as[0];
		as[0] = 1.0;
	}
	static const double off1 = 0.02;
	static const double off2 = 0.12;
	double pos1[2], pos2[2];
	for(i=0; i<2; i++)
	{
		pos1[i] = off1/as[i];
		pos2[i] = off2/as[i];
	}

	int mag = mOverlayHelper->GetRenderingMagnification();
	if(mag > 1)
	{
		int winij[2];
		mOverlayHelper->ComputePositionShiftsUnderMagnification(winij);
		for(i=0; i<2; i++)
		{
			pos1[i] = mag*pos1[i]-winij[i];
			pos2[i] = mag*pos2[i]-winij[i];
		}
	}

	if(jmax%2 == 0) // y-neg
	{
		if(imax%2 == 0) // x-neg
		{
			mActors[0]->SetPoint1(pos2[0],pos2[1]);
			mActors[0]->SetPoint2(pos1[0]+0.2*(pos2[0]-pos1[0]),pos2[1]);
			mActors[1]->SetPoint1(pos2[0],pos2[1]);
			mActors[1]->SetPoint2(pos2[0],pos1[1]+0.2*(pos2[1]-pos1[1]));
			mLabels[0]->SetPosition(pos1[0],pos2[1]);
			mLabels[1]->SetPosition(pos2[0],pos1[1]);
		}
		else // x-pos
		{
			mActors[0]->SetPoint1(pos1[0],pos2[1]);
			mActors[0]->SetPoint2(pos2[0]-0.2*(pos2[0]-pos1[0]),pos2[1]);
			mActors[1]->SetPoint1(pos1[0],pos2[1]);
			mActors[1]->SetPoint2(pos1[0],pos1[1]+0.2*(pos2[1]-pos1[1]));
			mLabels[0]->SetPosition(pos2[0],pos2[1]);
			mLabels[1]->SetPosition(pos1[0],pos1[1]);
		}
	}
	else // y-pos
	{
		if(imax%2 == 0) // x-neg
		{
			mActors[0]->SetPoint1(pos2[0],pos1[1]);
			mActors[0]->SetPoint2(pos1[0]+0.2*(pos2[0]-pos1[0]),pos1[1]);
			mActors[1]->SetPoint1(pos2[0],pos1[1]);
			mActors[1]->SetPoint2(pos2[0],pos2[1]-0.2*(pos2[1]-pos1[1]));
			mLabels[0]->SetPosition(pos1[0],pos1[1]);
			mLabels[1]->SetPosition(pos2[0],pos2[1]);
		}
		else // x-pos
		{
			mActors[0]->SetPoint1(pos1[0],pos1[1]);
			mActors[0]->SetPoint2(pos2[0]-0.2*(pos2[0]-pos1[0]),pos1[1]);
			mActors[1]->SetPoint1(pos1[0],pos1[1]);
			mActors[1]->SetPoint2(pos1[0],pos2[1]-0.2*(pos2[1]-pos1[1]));
			mLabels[0]->SetPosition(pos2[0],pos1[1]);
			mLabels[1]->SetPosition(pos1[0],pos2[1]);
		}
	}

	switch(imax/2)
	{
	case  0:
		{
			mLabels[0]->SetText("X");
			break;
		}
	case  1:
		{
			mLabels[0]->SetText("Y");
			break;
		}
	case  2:
		{
			mLabels[0]->SetText("Z");
			break;
		}
	};

	switch(jmax/2)
	{
	case  0:
		{
			mLabels[1]->SetText("X");
			break;
		}
	case  1:
		{
			mLabels[1]->SetText("Y");
			break;
		}
	case  2:
		{
			mLabels[1]->SetText("Z");
			break;
		}
	};

	for(i=0; i<2; i++)
	{
		mActors[i]->GetProperty()->SetColor(this->GetOverlayHelper()->GetColor(vp).ToVTK());
		mActors[i]->GetProperty()->SetLineWidth(2*mag);
	}
}

