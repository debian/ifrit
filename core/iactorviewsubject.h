/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A ViewSubject that represents a fixed set of solid surfaces (iActor)
//
#ifndef IACTORVIEWSUBJECT_H
#define IACTORVIEWSUBJECT_H


#include "iviewsubject.h"


#include "iposition.h"

class iActor;
class iActorViewSubject;


namespace iParameter
{
	namespace ViewObject
	{
		namespace Flag
		{
			const unsigned int HasNoColor =		 2U;
			const unsigned int HasNoOpacity =	 4U;
			const unsigned int IsSameColor =	 8U;
			const unsigned int IsSameOpacity =	16U;
		};
	};
};


class iActorViewInstance : public iViewInstance
{
	
	friend class iActorViewSubject;

public:
	
	iDataConsumerTypeMacroPass(iActorViewInstance,iViewInstance);

	iObjectSlotMacro1SR(Color,iColor);
	iObjectSlotMacro1SV(Opacity,float);

protected:
	
	iActorViewInstance(iActorViewSubject *owner, int num_actors, float max_opacity);
	virtual ~iActorViewInstance();

	iActorViewSubject* Owner() const;

	virtual void ShowBody(bool s);
	virtual void ResetBody();

	const float mMaxOpacity;
	iArray<iActor*> mActors;
};


class iActorViewSubject : public iViewSubject
{

	friend class iActorViewInstance;
	friend class iObjectFactory;

public:
	
	iDataConsumerTypeMacroPass(iActorViewSubject,iViewSubject);

	const bool HasNoColor, HasNoOpacity;
	const bool IsSameColor, IsSameOpacity;

	iType::vsp_color Color;
	iType::vsp_float Opacity;
		
	inline const iColor& GetDefaultColor() const { return mDefaultColor; }

protected:
	
	iActorViewSubject(iObject *parent, const iString &fname, const iString &sname, iViewModule *vm, const iDataType &type, unsigned int flags, int minsize, int maxsize);
	virtual ~iActorViewSubject();

	iColor mDefaultColor;
};


class iPropPlacementHelper
{

public:

	virtual bool SetBoxSize(double p);
	double GetBoxSize() const;

	void SetSize(double p);
	void SetSize(const iDistance &p);
	inline double GetSize() const { return mSize; }

	virtual bool SetDirection(const iVector3D& pos);
	const iVector3D& GetDirection() const { return mDirection; }

	virtual bool SetBoxPosition(const iVector3D& pos);
	const iVector3D& GetBoxPosition() const;

	void SetPosition(const iPosition &pos);
	inline const iPosition& GetPosition() const { return mPosition; }

protected:
	
	iPropPlacementHelper(iViewModule *vm);
	virtual ~iPropPlacementHelper();

	virtual void UpdateDirection() = 0;
	virtual void UpdatePosition() = 0;
	virtual void UpdateSize() = 0;

	iVector3D mDirection;
	iPosition mPosition;
	iDistance mSize;
};

#endif // IACTORVIEWSUBJECT_H

