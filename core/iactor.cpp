/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iactor.h"


#include "idatalimits.h"
#include "ierror.h"
#include "ilookuptable.h"
#include "ipair.h"
#include "ipalette.h"

#include <vtkCamera.h>
#include <vtkLookupTable.h>
#include <vtkMapperCollection.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkPolyDataMapper.h>

//
//  Templates
//
#include "iarray.tlh"


#ifdef I_DEBUG
#include <vtkRandomAttributeGenerator.h>

bool iActor::mRandomColoring = false;
#endif


//
//  Actor class
//
iActor* iActor::New(vtkPlaneCollection *planes)
{
	return new iActor(planes);
}


iActor::iActor(vtkPlaneCollection *planes)
{
	mBasicScale = mAxisScale[0] = mAxisScale[1] = mAxisScale[2] = 1.0;
	mScaled = false;
	mCurrentSubject = 0;

	mDummySubject = new iActorSubject(this,0); IERROR_CHECK_MEMORY(mDummySubject);
	mDummySubjectPointer = mDummySubject;

	mDefaultMapper = vtkPolyDataMapper::New(); IERROR_CHECK_MEMORY(mDefaultMapper);
	if(planes != 0) mDefaultMapper->SetClippingPlanes(planes);
	this->SetMapper(mDefaultMapper);
	mDefaultMapper->Delete();

	mLookupTable = iLookupTable::New(); IERROR_CHECK_MEMORY(mLookupTable);
	mDefaultMapper->SetLookupTable(mLookupTable);
	mDefaultMapper->UseLookupTableScalarRangeOn();

	mDefaultLODMappers = 0;

#ifdef I_DEBUG
	mRandomizer = vtkRandomAttributeGenerator::New(); IERROR_CHECK_MEMORY(mRandomizer);
	mRandomizer->SetDataTypeToUnsignedChar();
	mRandomizer->SetNumberOfComponents(3);
	mRandomizer->GenerateCellScalarsOn();
	mRandomizer->SetMinimumComponentValue(0.0);
	mRandomizer->SetMaximumComponentValue(255.0);
	mSavedInterpolationMode = -1;
#endif
}


iActor::~iActor()
{
#ifdef I_DEBUG
	mRandomizer->Delete();
#endif

	while(mSubjects.Size() > 0) this->ReleaseGraphicsResources(mSubjects[mSubjects.MaxIndex()]->mWindow);
	delete mDummySubject;

	mLookupTable->Delete();

	this->Mapper = mDefaultMapper;
	if(mDefaultLODMappers != 0) this->LODMappers = mDefaultLODMappers;
}


bool iActor::SetCurrentWindow(vtkWindow *win)
{
	int i;

	IASSERT(win);

	if(mCurrentSubject!=0 && mCurrentSubject->mWindow==win) return true; // already current

	//
	//  Find this window
	//
	mDummySubject->mWindow = win;
	i = mSubjects.Find(mDummySubjectPointer);
	if(i<0 || i>=mSubjects.Size()) return false;

	mCurrentSubject = mSubjects[i].Pointer();
	this->Mapper = mCurrentSubject->mMapper;
	this->LODMappers = mCurrentSubject->mLODMappers;
	return true;
}


void iActor::ReleaseGraphicsResources(vtkWindow *win)
{
	if(win==0 || !this->SetCurrentWindow(win)) return; // already released

	this->vtkLODActor::ReleaseGraphicsResources(win);

	if(mSubjects.Remove(mCurrentSubject)) delete mCurrentSubject;
	mCurrentSubject = 0;
	this->Mapper = mDefaultMapper;
	this->LODMappers = mDefaultLODMappers;
}



void iActor::Render(vtkRenderer *ren, vtkMapper *)
{
	vtkWindow *win = ren->GetVTKWindow();

	if(win == 0) return; // nothing to do

	if(mDefaultLODMappers == 0)
	{
		this->CreateOwnLODs();
		mDefaultLODMappers = this->LODMappers; // saving for proper deletion
		vtkMapper *m;
		mDefaultLODMappers->InitTraversal();
		while((m = mDefaultLODMappers->GetNextItem()) != 0)
		{
			m->SetLookupTable(mLookupTable);
			m->UseLookupTableScalarRangeOn();
			m->SetClippingPlanes(mDefaultMapper->GetClippingPlanes());
		}
	}

	if(!this->SetCurrentWindow(win))
	{
		//
		//  This window has not been attached yet
		//
		mCurrentSubject = new iActorSubject(this,win); IERROR_CHECK_MEMORY(mCurrentSubject);
		mSubjects.Add(mCurrentSubject);
		mCurrentSubject->mMapper->SetInputConnection(mDefaultMapper->GetInputConnection(0,0));

		this->Mapper = mCurrentSubject->mMapper;
		this->LODMappers = mCurrentSubject->mLODMappers;
	}

	if(mScaled)
	{
		vtkCamera *cam = ren->GetActiveCamera();
		
		double ps;
		if(cam->GetParallelProjection() == 0)
		{
			ps = cam->GetDistance()*tan(cam->GetViewAngle()/2.0*0.017453292)/1.6;
		}
		else
		{
			ps = cam->GetParallelScale()/1.6;
		}
		ps *= mBasicScale;
		this->SetScale(ps*mAxisScale[0],ps*mAxisScale[1],ps*mAxisScale[2]);
	}
	else
	{
		this->SetScale(mAxisScale[0],mAxisScale[1],mAxisScale[2]);
	}
	
	//
	//  Fix VTK bug
	//
	if(this->GetProperty()->GetMTime() > this->Mapper->GetMTime())
	{
		this->Mapper->Modified();
	}

#ifdef I_DEBUG
	if(mRandomColoring)
	{
		if(mSavedInterpolationMode == -1)
		{
			mSavedInterpolationMode = this->GetProperty()->GetInterpolation();
			this->GetProperty()->SetInterpolationToFlat();
		}
		mRandomizer->SetGenerateCellScalars(1);
		this->Mapper->SetColorModeToDefault();
		this->Mapper->SetScalarModeToUseCellData();
	}
	else
	{
		mRandomizer->SetGenerateCellScalars(0);
		this->Mapper->SetColorMode(mDefaultMapper->GetColorMode());
		this->Mapper->SetScalarMode(mDefaultMapper->GetScalarMode());
		if(mSavedInterpolationMode != -1)
		{
			this->GetProperty()->SetInterpolation(mSavedInterpolationMode);
			mSavedInterpolationMode = -1;
		}
	}
#endif

	vtkLODActor::Render(ren,this->Mapper);
}


void iActor::SetScaled(bool s)
{
	if(s != mScaled)
	{
		mScaled = s;
		this->Modified();
	}
}


void iActor::SetBasicScale(double s)
{
	if(s > 0.0)
	{
		mBasicScale = s;
		this->Modified();
	}
}


void iActor::SetAxisScale(double sx, double sy, double sz)
{
	if(sx>0.0 && sy>0.0 && sz>0.0)
	{
		mAxisScale[0] = sx;
		mAxisScale[1] = sy;
		mAxisScale[2] = sz;
		this->Modified();
	}
}


//
//  Mapper functionality
//
void iActor::SetInputConnection(vtkAlgorithmOutput *port)
{
#ifdef I_DEBUG
	mRandomizer->SetInputConnection(port);
	port = mRandomizer->GetOutputPort();
#endif

	mDefaultMapper->SetInputConnection(port);

	vtkMapper *save = this->Mapper;
	this->Mapper = mDefaultMapper;
	this->UpdateOwnLODs();
	this->Mapper = save;

	this->Mapper->SetInputConnection(port);
}
	

//
//  Mapper functionality
//
void iActor::SetInputData(vtkPolyData *data)
{
	mDefaultMapper->SetInputData(data);

	vtkMapper *save = this->Mapper;
	this->Mapper = mDefaultMapper;
	this->UpdateOwnLODs();
	this->Mapper = save;

	vtkPolyDataMapper::SafeDownCast(this->Mapper)->SetInputData(data);
}
	

void iActor::SetScalarVisibility(bool s)
{
	int i;
	mDefaultMapper->SetScalarVisibility(s?1:0);
	for(i=0; i<mSubjects.Size(); i++) mSubjects[i]->mMapper->SetScalarVisibility(s?1:0);
}


void iActor::SetScalarRange(const iPair &p)
{
	this->SetScalarRange(p.Min,p.Max);
}


void iActor::SetScalarRange(float min, float max)
{
	int i;

	if(mDefaultMapper->GetUseLookupTableScalarRange() == 0)
	{
		mDefaultMapper->SetScalarRange(min,max);
		for(i=0; i<mSubjects.Size(); i++) mSubjects[i]->mMapper->SetScalarRange(min,max);
	}
	mLookupTable->SetTableRange(min,max);
}

	
void iActor::SyncWithLimits(iDataLimits *lims, int v)
{
	if(lims!=0 && v>=0 && v<lims->GetNumVars())
	{
		mLookupTable->SetStretchId(iStretch::Lin);
		this->SetScalarRange(lims->GetMin(v),lims->GetMax(v));
		mLookupTable->SetStretchId(lims->GetStretchId(v));
	}
}


void iActor::AddMapperObserver(unsigned long event, vtkCommand *obs)
{
	int i;
	mDefaultMapper->AddObserver(event,obs);
	for(i=0; i<mSubjects.Size(); i++) mSubjects[i]->mMapper->AddObserver(event,obs);
}


void iActor::ColorByArrayComponent(int arrayNum, int component)
{
	int i;
	mDefaultMapper->ColorByArrayComponent(arrayNum,component);
	for(i=0; i<mSubjects.Size(); i++) mSubjects[i]->mMapper->ColorByArrayComponent(arrayNum,component);
}


//
//  Helper classes
//
iActorSubject::iActorSubject(iActor *parent, vtkWindow *win)
{
	IASSERT(parent);
 
	mWindow = win;

	if(win == 0) // dummy
	{
		mMapper = 0;
		mLODMappers = 0;
	}
	else
	{
		mMapper = vtkPolyDataMapper::New(); IERROR_CHECK_MEMORY(mMapper);
		mMapper->ShallowCopy(parent->GetMapper());

		mLODMappers = vtkMapperCollection::New(); IERROR_CHECK_MEMORY(mLODMappers);

		vtkMapper *m1;
		vtkPolyDataMapper *m2;
		vtkMapperCollection *pm = parent->GetLODMappers();
		pm->InitTraversal();
		while((m1 = pm->GetNextItem()) != 0)
		{
			m2 = vtkPolyDataMapper::New(); IERROR_CHECK_MEMORY(m2);
			m2->ShallowCopy(m1);
			mLODMappers->AddItem(m2);
			m2->Delete();
		}
	}
}


iActorSubject::~iActorSubject()
{
	if(mMapper != 0) mMapper->Delete();
	if(mLODMappers != 0) mLODMappers->Delete();
}


iActorSubjectPointer::iActorSubjectPointer(iActorSubject *subject)
{
	mPointer = subject;
}


void iActorSubjectPointer::operator=(iActorSubject *p)
{
	mPointer = p;
}


void iActorSubjectPointer::operator=(const iActorSubjectPointer &p)
{
	mPointer = p.mPointer;
}


bool iActorSubjectPointer::operator==(const iActorSubjectPointer &p) const
{
	if(mPointer!=0 && p.mPointer!=0)
	{
		return (mPointer->mWindow == p.mPointer->mWindow); 
	}
	else return (mPointer == p.mPointer);
}
