/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "icaption.h"


#include "iactor.h"
#include "ierror.h"
#include "ioverlayhelper.h"

#include <vtkCamera.h>
#include <vtkCellArray.h>
#include <vtkCoordinate.h>
#include <vtkMapper.h>
#include <vtkMath.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>

//
//  Templates
//
#include "iarray.tlh"
#include "igenericprop.tlh"


bool iCaption::Transparent = true;


iCaption* iCaption::New(iRenderTool *rt)
{
	IASSERT(rt);
	return new iCaption(rt);
}


iCaption::iCaption(iRenderTool *rt) : iFramedTextActor(rt)
{
	this->SetJustification(-1,-1);

	//
	// This is the leader (line) from the attachment point to the caption
	//
	mLeaderInput = vtkPolyData::New(); IERROR_CHECK_MEMORY(mLeaderInput);

	vtkPoints *pts = vtkPoints::New(VTK_DOUBLE); IERROR_CHECK_MEMORY(pts);
	pts->SetNumberOfPoints(2);
	mLeaderInput->SetPoints(pts);
	pts->Delete();

	vtkCellArray *leader = vtkCellArray::New(); IERROR_CHECK_MEMORY(leader);
	leader->InsertNextCell(2); 
	leader->InsertCellPoint(0);
	leader->InsertCellPoint(1); //at the attachment point
	mLeaderInput->SetLines(leader);
	leader->Delete();
	
	mLeaderActor = iActor::New(); IERROR_CHECK_MEMORY(mLeaderActor);
	this->PrependComponent(mLeaderActor);
	mLeaderActor->SetInputData(mLeaderInput);

	mAttachmentPoint = vtkCoordinate::New(); IERROR_CHECK_MEMORY(mAttachmentPoint);
	mAttachmentPoint->SetCoordinateSystemToWorld();
	mAttachmentPoint->SetValue(0.0,0.0,0.0);
}


iCaption::~iCaption()
{
	mAttachmentPoint->Delete();
	mLeaderInput->Delete();
	mLeaderActor->Delete();
}

	
void iCaption::SetAttachmentPoint(double x[3])
{
	mAttachmentPoint->SetValue(x);
}
	

const double* iCaption::GetAttachmentPoint()
{
	return mAttachmentPoint->GetValue();
}


void iCaption::UpdateGeometry(vtkViewport* vp)
{
	static const int index[5][2] = { 
		{ 0, 2 },
		{ 1, 2 },
		{ 1, 3 },
		{ 0, 3 },
		{ 0, 2 }
	};

	iFramedTextActor::UpdateGeometry(vp);
	if(!this->IsEnabled()) return;
	
	int mag = this->GetOverlayHelper()->GetRenderingMagnification();
	if(mag == 1)
	{
		this->SetTransparent(Transparent);

		//
		//  Compute the screen projection of the attachment point
		//
		int *vl = mAttachmentPoint->GetComputedViewportValue(vp);
		int *vs = vp->GetSize();

		int j;
		float pa[3];
		for(j=0; j<2; j++) pa[j] = float(vl[j])/vs[j];

		//
		//  Define the leader. Have to find the closest point from the
		//  border to the attachment point. We look at the four vertices
		//  and four edge centers.
		//
		float d2, d2min, pt[3], ptmin[2];
		d2min = VTK_LARGE_FLOAT;
		pt[2] = pa[2] = 0.0;

		for(j=0; j<8; j++)
		{
			if(j%2 == 0)
			{
				pt[0] = wBounds[index[j/2][0]];
				pt[1] = wBounds[index[j/2][1]];
			}
			else
			{
				pt[0] = 0.5*(wBounds[index[j/2][0]]+wBounds[index[j/2+1][0]]);
				pt[1] = 0.5*(wBounds[index[j/2][1]]+wBounds[index[j/2+1][1]]);
			}
			d2 = vtkMath::Distance2BetweenPoints(pa,pt);
			if(d2 < d2min)
			{
				d2min = d2;
				ptmin[0] = pt[0];
				ptmin[1] = pt[1]; 
			}
		}

		//
		//  Find the world coordinates of the nearest point.
		//
		vtkCamera *cam = this->GetOverlayHelper()->GetCamera(vp);
		double x[4], d[3];
		double *clip = cam->GetClippingRange();
		double *cpos = cam->GetPosition();

		vp->SetDisplayPoint(ptmin[0]*vs[0],ptmin[1]*vs[1],0.0);
		vp->DisplayToWorld();
		vp->GetWorldPoint(x);

		for(j=0; j<3; j++) d[j] = x[j] - cpos[j];

		double cdop[3];
		cam->GetDirectionOfProjection(cdop);
		vtkMath::Normalize(cdop);
		float s = clip[0]/vtkMath::Dot(d,cdop);
		for(j=0; j<3; j++) x[j] = cpos[j] + s*d[j];


#ifdef I_DEBUG
		iOutput::DisplayDebugMessage(iString("Caption: ")+iString::FromNumber(d2min)+" ("+iString::FromNumber(ptmin[0])+","+iString::FromNumber(ptmin[1])+")");
		iOutput::DisplayDebugMessage(iString("         ")+iString::FromNumber(s)+" ("+iString::FromNumber(x[0])+","+iString::FromNumber(x[1])+","+iString::FromNumber(x[2])+","+iString::FromNumber(x[3])+")");
#endif

		vtkPoints *pts = mLeaderInput->GetPoints();
		pts->SetPoint(0,mAttachmentPoint->GetValue());
		pts->SetPoint(1,x);
		mLeaderInput->Modified();

		mLeaderActor->GetProperty()->SetColor(this->GetOverlayHelper()->GetColor(vp).ToVTK());
	}

	mLeaderActor->GetProperty()->SetLineWidth(2*mag);
}
