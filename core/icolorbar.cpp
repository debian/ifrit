/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "icolorbar.h"


#include "idata.h"
#include "idatalimits.h"
#include "idatareader.h"
#include "ierror.h"
#include "igenericprop.h"
#include "imath.h"
#include "ioverlayhelper.h"
#include "ipalette.h"
#include "ipalettecollection.h"
#include "istretch.h"
#include "irendertool.h"
#include "itextactor.h"
#include "iviewmodule.h"

#include <vtkActor2D.h>
#include <vtkCamera.h>
#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkFloatArray.h>
#include <vtkImageMapper.h>
#include <vtkLookupTable.h>
#include <vtkMath.h>
#include <vtkMatrix4x4.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper2D.h>
#include <vtkProperty2D.h>
#include <vtkRenderer.h>

using namespace iParameter;

//
//  Templates
//
#include "iarray.tlh"
#include "igenericprop.tlh"
#include "iproperty.tlh"


//
//  helper classes
//
class iColorBarLabel
{
	
	friend class iColorBarActor;

private:

	iColorBarLabel(int id, iRenderTool *parent)
	{
		Base = iTextActor::New(parent); IERROR_CHECK_MEMORY(Base);
		Power = iTextActor::New(parent); IERROR_CHECK_MEMORY(Power);
		Base->SetJustification((id==1)?1:-1);
		Power->SetJustification(-1);
	}

	~iColorBarLabel()
	{
		Base->Delete();
		Power->Delete();
	}

	iTextActor *Base, *Power;
};


class iColorBarActor : public iGenericProp<vtkActor2D>
{
	
	IPOINTER_AS_USER(OverlayHelper);

public:
	
	vtkTypeMacro(iColorBarActor,vtkActor2D);
	static iColorBarActor* New(bool left = false, iColorBar *parent = 0);

	void SetSideOffset(float v);
	inline float GetSideOffset() const { return mOffset; }

	void SetHeight(float v);
	inline float GetHeight() const { return mHeight*0.86; } // there is a 0.86 factor in vtkScalarBarActor

	static bool MethodToImage;

protected:

	virtual ~iColorBarActor();

	virtual void UpdateGeometry(vtkViewport *vp);

private:
	
	iColorBarActor(const int id, iColorBar *parent);

	void UpdatePlacement(bool force);
	void UpdateMapper2Data(int mag);

	const int mId;

	bool mStarted;

	float mHeight, mOffset, mWidth4Height;
	float mXOff, mYOff;
	float mLabelOff, mTitleOff;
	int mJmax;

	iColorBar *mParent;
	vtkActor2D *mBarActor;
	vtkImageMapper *mBarMapper;
	vtkPolyDataMapper2D *mBarMapper2;
	vtkPolyData *mBarMapper2Data;

	iTextActor *mTitleActor;
	iArray<iColorBarLabel*> mLabels;
};


bool iColorBarActor::MethodToImage = false;


iColorBarActor* iColorBarActor::New(bool left, iColorBar *parent)
{
	IASSERT(parent);
	return new iColorBarActor(left,parent);
}


iColorBarActor::iColorBarActor(int id, iColorBar *parent) : iGenericProp<vtkActor2D>(false), mOverlayHelper(parent->GetViewModule()->GetRenderTool()), mId(id)
{
	int i;

	mParent = parent;
	mStarted = false;

	mJmax = 0;
	mWidth4Height = 0.03/0.7;
	mHeight = 0.7;
	mOffset = 0.08;

	mBarActor = vtkActor2D::New(); IERROR_CHECK_MEMORY(mBarActor);
	this->AppendComponent(mBarActor);
	mBarActor->GetPositionCoordinate()->SetCoordinateSystemToNormalizedViewport();
	mBarActor->GetPosition2Coordinate()->SetCoordinateSystemToNormalizedViewport();
	mBarActor->SetWidth(mWidth4Height*mHeight*0.86);  // there was a 0.86 factor in vtkScalarBarActor
	mBarActor->SetHeight(mHeight*0.86);               // there was a 0.86 factor in vtkScalarBarActor
	mBarActor->PickableOff();
	mBarActor->GetProperty()->SetDisplayLocationToForeground();
	mBarActor->GetProperty()->SetColor(1,1,1);

	mBarMapper = vtkImageMapper::New(); IERROR_CHECK_MEMORY(mBarMapper);
	mBarMapper->RenderToRectangleOn();
	mBarMapper->SetColorWindow(255.0);
	mBarMapper->SetColorLevel(127.5);
	mBarMapper->UseCustomExtentsOff();

	mBarMapper2 = vtkPolyDataMapper2D::New(); IERROR_CHECK_MEMORY(mBarMapper2);
	vtkCoordinate *c = vtkCoordinate::New(); IERROR_CHECK_MEMORY(c);
	c->SetCoordinateSystemToNormalizedViewport();
	mBarMapper2->SetTransformCoordinate(c);
	c->Delete();
	mBarMapper2->SetScalarRange(0.0,1.0); 	

	mTitleActor = iTextActor::New(this->GetOverlayHelper()->GetRenderTool()); IERROR_CHECK_MEMORY(mTitleActor);
	this->AppendComponent(mTitleActor);
	mTitleActor->SetAngle(90.0);
	mTitleActor->SetJustification((mId==1)?-1:1,0);
	mTitleActor->SetBold(true);

	iColorBarLabel *l;
	for(i=0; i<10; i++)
	{
		l = new iColorBarLabel(mId,this->GetOverlayHelper()->GetRenderTool()); IERROR_CHECK_MEMORY(l);
		this->AppendComponent(l->Base);
		this->AppendComponent(l->Power);
		mLabels.Add(l);
	}

	//
	//  Data for type 2 mapper
	//
	mBarMapper2Data = vtkPolyData::New(); IERROR_CHECK_MEMORY(mBarMapper2Data);
	vtkPoints *points = vtkPoints::New(VTK_FLOAT); IERROR_CHECK_MEMORY(points);
	vtkCellArray *cells = vtkCellArray::New(); IERROR_CHECK_MEMORY(cells);
	vtkFloatArray *scalars = vtkFloatArray::New(); IERROR_CHECK_MEMORY(scalars);
	scalars->SetNumberOfComponents(1);

	//
	// We now assign the pieces to the vtkPolyData.
	//
	mBarMapper2Data->SetPolys(cells);
	cells->Delete();
	mBarMapper2Data->SetPoints(points);
	points->Delete();
	mBarMapper2Data->GetCellData()->SetScalars(scalars);
	scalars->Delete();
	
	mBarMapper2->SetInputData(mBarMapper2Data);

	//
	// Load the point, cell, and data attributes.
	//
	int n = 256;
	double x[3];
	vtkIdType verts[4];
	scalars->SetNumberOfTuples(n);
	float *arr = scalars->GetPointer(0);

	x[2] = 0.0;
	for(i=0; i<=n; i++)
	{
		x[1] = float(i)/n;
		x[0] = 0.0;
		points->InsertNextPoint(x);
		x[0] = 1.0;
		points->InsertNextPoint(x);

		if(i > 0)
		{
			verts[0] = 2*i - 2;
			verts[1] = 2*i - 1;
			verts[2] = 2*i + 1;
			verts[3] = 2*i + 0;
			cells->InsertNextCell(4,verts);
			arr[i-1] = float(i-1)/(n-1);
		}
	}

	this->UpdatePlacement(true);
}


iColorBarActor::~iColorBarActor()
{
	int i;

	mBarMapper2Data->Delete();
	mBarMapper2->Delete();
	mBarMapper->Delete();
	mBarActor->Delete();
	mTitleActor->Delete();
	for(i=0; i<mLabels.Size(); i++) delete mLabels[i];
}


void iColorBarActor::UpdatePlacement(bool force)
{
	double w = mWidth4Height*mHeight*0.86;
	double h = mHeight*0.86;

	if(force || fabs(mBarActor->GetWidth()-w) > 1.0e-10 || fabs(mBarActor->GetHeight()-h) > 1.0e-10)
	{
		if(mId == 1)
		{
			mXOff = mOffset;
			mLabelOff = mXOff - 0.01;
			mTitleOff = mXOff + mWidth4Height*mHeight;
		}
		else
		{
			mXOff = 1.0 - mWidth4Height*mHeight - mOffset;
			mLabelOff = mXOff + mWidth4Height*mHeight;
			mTitleOff = mXOff - 0.3*mWidth4Height*mHeight;
		}
		mYOff = 0.01 + 0.5*(1-mHeight*0.86);  // there was a 0.86 factor in vtkScalarBarActor

		this->UpdateMapper2Data(1);

		mBarActor->SetPosition(mXOff,mYOff);
		mBarActor->SetWidth(mWidth4Height*mHeight*0.86);  // there was a 0.86 factor in vtkScalarBarActor
		mBarActor->SetHeight(mHeight*0.86);               // there was a 0.86 factor in vtkScalarBarActor
		mTitleActor->SetPosition(mTitleOff,0.5);
		this->Modified();
	}
}


void iColorBarActor::UpdateMapper2Data(int mag)
{
	if(MethodToImage) return;

	double w = mWidth4Height*mHeight*0.86;
	double h = mHeight*0.86;

	vtkPoints *p = mBarMapper2Data->GetPoints();

	int i, n = p->GetNumberOfPoints();
	float x[3];
	x[2] = 0;
	for(i=0; i<n; i+=2)
	{
		x[1] = mag*h*float(i)/(n-2);
		x[0] = 0.0;
		p->SetPoint(i,x);
		x[0] = mag*w;
		p->SetPoint(i+1,x);
	}
}


void iColorBarActor::SetSideOffset(float v)
{
	mOffset = v;
	this->UpdatePlacement(true);
}


void iColorBarActor::SetHeight(float v)
{
	mHeight = v/0.86;       // there is a 0.86 factor in vtkScalarBarActor
	this->UpdatePlacement(true);
}


void iColorBarActor::UpdateGeometry(vtkViewport* viewport)
{
	const iColorBarItem *bar = mParent->GetBar(mId);

	if(bar==0 || bar->Palette==0)
	{
		this->Disable();
		return;
	}

	iDataLimits *lim = mParent->GetViewModule()->GetReader()->GetLimits(bar->DataTypeId.Type());
	if(lim==0 || bar->Var>=lim->GetNumVars())
	{
		this->Disable();
		return;
	}

	int j;
	float w1 = 0.0, h1 = 0.0, w2 = 0.0, h2 = 0.0;
	if(!mStarted) mStarted = true;

	int mag = this->GetOverlayHelper()->GetRenderingMagnification();
	if(mag == 1)
	{
		int nstep, pow;
		float dv, voff, vstep, vbound, v;
		float s1[2], s2[2];
		char s[100];
		const char *fmt;

		float vl = lim->GetStretchedMin(bar->Var);
		float vu = lim->GetStretchedMax(bar->Var);
		switch(lim->GetStretchId(bar->Var))
		{
		case iStretch::Log:
			{
				//
				//  Log table
				//
				int ivl = 1 + (int)floor(vl-0.01);
				int ivu = 0 + (int)floor(vu+0.01);
				dv = vu - vl;

				if(dv<=0.0 || ivl>vu)
				{
					this->Disable();
					return;
				}

				voff = ivl - vl;
				int ndex = ivu - ivl;
				if(ndex > 1) nstep = 1 + ndex/mLabels.Size(); else nstep = 1;

				mJmax = 1 + ndex/nstep;
				vstep = nstep;

				for(j=0; j<mJmax; j++)
				{
					pow = ivl + nstep*j;
					sprintf(s,"%d",pow);

					if(pow == 0)
					{
						mLabels[j]->Base->SetText(" 1"); 
						mLabels[j]->Power->SetText(""); 
					} 
					else 
					{
						mLabels[j]->Base->SetText("10"); 
						mLabels[j]->Power->SetText(s);
					}

					mLabels[j]->Base->GetSize(viewport,s1);
					mLabels[j]->Power->GetSize(viewport,s2);
					if(w1 < s1[0]) w1 = s1[0];
					if(h1 < s1[1]) h1 = s1[1];
					if(w2 < s2[0]) w2 = s2[0];
					if(h2 < s2[1]) h2 = s2[1];	
				}
				break;
			}
		default:
			{
				//
				//  Lin table
				//
				mJmax = 5;
				voff = 0.0;
				if(fabs(vl) < 1.0e-3*fabs(vu)) vl = 0.0;
				if(fabs(vu) < 1.0e-3*fabs(vl)) vu = 0.0;
				dv = vu - vl;
				if(fabs(dv) < 1.0e-30*fabs(vu)) // dv is zero
				{
					if(vu > 0.0)
					{
						vu = vu*1.001;
						vl = vl*0.999;
					}
					else if(vu < 0.0)
					{
						vu = vu*0.999;
						vl = vl*1.001;
					}
					else
					{
						vu = 1.0;
						vl = -1.0;
					}
					dv = vu - vl;
				}
				vstep = dv/(mJmax-1);
				vbound = (fabs(vl) > fabs(vu)) ? fabs(vl) : fabs(vu);
				if(fabs(vstep) > 0.03*vbound)
				{
					fmt = "%.2g";
				}
				else if(fabs(vstep) > 0.003*vbound)
				{
					fmt = "%.3g";
				}
				else
				{
					fmt = "%g";
				}

				for(j=0; j<mJmax; j++)
				{
					v = vl + vstep*j;
					sprintf(s,fmt,v);

					mLabels[j]->Base->SetText(s); 
					mLabels[j]->Power->SetText(""); 

					mLabels[j]->Base->GetSize(viewport,s1);
					mLabels[j]->Power->GetSize(viewport,s2);
					if(w1 < s1[0]) w1 = s1[0];
					if(h1 < s1[1]) h1 = s1[1];
					if(w2 < s2[0]) w2 = s2[0];
					if(h2 < s2[1]) h2 = s2[1];
				}
			}
		}

		w1 *= 0.8;
		w2 *= 0.8;
		h1 *= 0.8;

		float xpos, ypos;
		xpos = mLabelOff;
		for(j=0; j<mJmax; j++)
		{
			if(dv > 0.0)
			{
				ypos = mYOff + 0.86*mHeight*(voff+vstep*j)/dv;
			}
			else
			{
				ypos = mYOff + 0.86*mHeight*0.5;
			}

			if(mId == 0) 
			{
				mLabels[j]->Base->SetPosition(xpos,ypos-0.5*h1);
				mLabels[j]->Power->SetPosition(w1+xpos,ypos+0.5*h1);
			} 
			else 
			{
				mLabels[j]->Base->SetPosition(xpos-w2,ypos-0.5*h1);
				mLabels[j]->Power->SetPosition(xpos-w2,ypos+0.5*h1);
			}
		}

		for(j=mJmax; j<mLabels.Size(); j++)
		{
			mLabels[j]->Base->SetText(""); 
			mLabels[j]->Power->SetText(""); 
		}

		mTitleActor->SetText(lim->GetName(bar->Var).ToCharPointer());

		if(MethodToImage)
		{
			mBarActor->SetMapper(mBarMapper);
			mBarMapper->SetInputData(iPaletteCollection::Global()->GetImageData(bar->Palette));
		}
		else
		{
			mBarActor->SetMapper(mBarMapper2);
			mBarMapper2->SetLookupTable(iPaletteCollection::Global()->GetLookupTable(bar->Palette));
		}

		this->UpdatePlacement(false);
	}
	else
	{
		mBarActor->SetWidth(mag*mWidth4Height*mHeight);
		mBarActor->SetHeight(mag*mHeight);

		//
		//  Shift positions if under magnification - 
		//
		int winij[2];
		this->GetOverlayHelper()->ComputePositionShiftsUnderMagnification(winij);

		mBarActor->SetPosition(mag*mXOff-winij[0],mag*mYOff-winij[1]);

		if(winij[0]==0 && winij[1]==0) this->UpdateMapper2Data(mag);
	}
}


//
//  Main class
//
iColorBar* iColorBar::New(iViewModule *vm)
{
	static iString LongName("ColorBar");
	static iString ShortName("cb");

	IASSERT(vm);
	return new iColorBar(vm,LongName,ShortName);
}


iColorBar::iColorBar(iViewModule *vm, const iString &fname, const iString &sname) : iViewModuleTool(vm,fname,sname),
	iObjectConstructPropertyMacroS(Bool,iColorBar,Automatic,a),
	iObjectConstructPropertyMacroS(Color,iColorBar,Color,c),
	iObjectConstructPropertyMacroS(Float,iColorBar,SideOffset,so),
	iObjectConstructPropertyMacroS(Float,iColorBar,Size,s),
	iObjectConstructPropertyMacroV(Int,iColorBar,Var,v,2),
	iObjectConstructPropertyMacroV(Int,iColorBar,Palette,p,2),
	iObjectConstructPropertyMacroV(DataType,iColorBar,DataType,dt,2)
{
	Var.AddFlag(iProperty::_FunctionsAsIndex);

	mAutomatic = true;

	//
	//  Create manual bars
	//
	mManualQueue[0].Bar.Palette = mManualQueue[1].Bar.Palette = 0;
	mManualQueue[0].Bar.DataTypeId = 1;
	mManualQueue[1].Bar.DataTypeId = 1;
	mManualQueue[0].Count = mManualQueue[1].Count = 1;

	mActors[0] = iColorBarActor::New(0,this); IERROR_CHECK_MEMORY(mActors[0]);
	mActors[1] = iColorBarActor::New(1,this); IERROR_CHECK_MEMORY(mActors[1]);

	this->GetViewModule()->GetRenderTool()->AddObject(mActors[0]);
	this->GetViewModule()->GetRenderTool()->AddObject(mActors[1]);

	mColor = iColor::Invalid();
}


iColorBar::~iColorBar()
{
	int i;
	for(i=0; i<2; i++)
	{
		this->GetViewModule()->GetRenderTool()->RemoveObject(mActors[i]);
		mActors[i]->Delete();
	}
}


void iColorBar::SetMethodToImage(bool s)
{
	iColorBarActor::MethodToImage = s;
}


bool iColorBar::SetMethodToImage()
{
	return iColorBarActor::MethodToImage;
}


bool iColorBar::ShowBody(bool s)
{
	int i;
	for(i=0; i<2; i++) mActors[i]->SetVisibility(s?1:0);
	return true;
}


bool iColorBar::Add(int priority, const iColorBarItem &bar)
{
	if(bar.Var < 0) return false;

	iDataLimits *lim = this->GetViewModule()->GetReader()->GetLimits(bar.DataTypeId.Type());
	if(lim==0 || bar.Var>=lim->GetNumVars()) return false;

	Item item;
	item.Bar = bar;
	item.Priority = priority;
	item.Count = 0;

	int idx = mAutomaticQueue.Find(item);
	if(idx >= 0) 
	{
		mAutomaticQueue[idx].Count++;
	}
	else 
	{
		//
		//  Insert a new one keeping the list ordered by priority
		//
		item.Count = 1;
		mAutomaticQueue.Add(item);
	}

	int i;
	for(i=0; i<2; i++) mActors[i]->Modified();

	return true;
}


void iColorBar::Remove(const iColorBarItem &bar)
{
	Item item;
	item.Bar = bar;
	item.Priority = 0;
	item.Count = 0;

	int idx = mAutomaticQueue.Find(item);
	if(idx >= 0)
	{
		mAutomaticQueue[idx].Count--;
		if(mAutomaticQueue[idx].Count == 0) // delete the entry
		{
			mAutomaticQueue.iArray<Item>::Remove(idx);
		}

		int i;
		for(i=0; i<2; i++) mActors[i]->Modified();
	}
}


//
//  Property slots
//
bool iColorBar::SetAutomatic(bool s)
{
	mAutomatic = s;

	int i;
	for(i=0; i<2; i++) mActors[i]->Modified();
	return true;
}


bool iColorBar::SetColor(const iColor& c)
{
	int i;
	for(i=0; i<2; i++)
	{
		mActors[i]->GetOverlayHelper()->SetFixedColor(c);
		mActors[i]->GetOverlayHelper()->SetAutoColor(!c.IsValid());
	}
	return true;
}


bool iColorBar::SetSideOffset(float v)
{
	if(v>=0.01 && v<=0.3)
	{
		int i;
		for(i=0; i<2; i++) mActors[i]->SetSideOffset(v);
		return true;
	}
	else return false;
}


float iColorBar::GetSideOffset() const
{
        return mActors[1]->GetSideOffset();
}


bool iColorBar::SetSize(float v)
{
	if(v>=0.1 && v<=1.0)
	{
		if(v > 0.95) v = 0.95;  //  make sure labels are visible;

		int i;
		for(i=0; i<2; i++) mActors[i]->SetHeight(v);
		return true;
	}
	else return false;
}


float iColorBar::GetSize() const
{
        return mActors[1]->GetHeight();
}


bool iColorBar::SetVar(int i, int v)
{
	if(i>=0 && i<2)
	{
		mManualQueue[i].Bar.Var = v;
		return true;
	}
	else return false;
}


int iColorBar::GetVar(int i) const
{
	if(i>=0 && i<2)
	{
		return mManualQueue[i].Bar.Var;
	}
	else return -1;
}


bool iColorBar::SetPalette(int i, int v)
{
	if(i>=0 && i<2)
	{
		mManualQueue[i].Bar.Palette = v;
		return true;
	}
	else return false;
}


int iColorBar::GetPalette(int i) const
{
	if(i>=0 && i<2)
	{
		return mManualQueue[i].Bar.Palette;
	}
	else return -1;
}


bool iColorBar::SetDataType(int i, const iDataType& v)
{
	if(i>=0 && i<2)
	{
		mManualQueue[i].Bar.DataTypeId = v.GetId();
		return true;
	}
	else return false;
}


const iDataType& iColorBar::GetDataType(int i) const
{
	if(i>=0 && i<2)
	{
		return mManualQueue[i].Bar.DataTypeId.Type();
	}
	else return iDataType::Null();
}


const iColorBarItem* iColorBar::GetBar(int id)
{
	if(mAutomatic)
	{
		if(mAutomaticQueue.Size() > id) return &(mAutomaticQueue[id].Bar); else return 0;
	}
	else
	{
		return &(mManualQueue[id].Bar);
	}
}


vtkActor2D* iColorBar::GetActor(int id) const
{
	return mActors[id];
}


bool iColorBar::Item::operator<(const iColorBar::Item &item) const
{
	return Priority < item.Priority;
}


bool iColorBar::Item::operator==(const iColorBar::Item &item) const
{
	return (Bar.Var==item.Bar.Var && Bar.DataTypeId==item.Bar.DataTypeId && Bar.Palette==item.Bar.Palette); 
}

