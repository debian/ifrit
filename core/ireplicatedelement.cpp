/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ireplicatedelement.h"


#include "ierror.h"
#include "iviewsubject.h"


iReplicatedElement::iReplicatedElement(bool isData) : mIsData(isData)
{
	int i;
	for(i=0; i<6; i++) mReplicationFactors[i] = mOldReplicationFactors[i] = 0;

	mParent = 0;
}


iReplicatedElement::~iReplicatedElement()
{
	if(mParent != 0)
	{
		mParent->UnRegisterReplicatedElement(this); 
	}
}


void iReplicatedElement::ReplicateAs(iViewSubject *parent)
{
	if(mParent != 0)
	{
		mParent->UnRegisterReplicatedElement(this); 
	}

	mParent = parent;

	if(mParent != 0)
	{
		mParent->RegisterReplicatedElement(this,mIsData);
	}

	this->UpdateReplicas();
}


void iReplicatedElement::UpdateReplicas(bool force)
{
	int i;
	bool work = force;

	this->UpdateReplicasHead();

	if(mParent != 0)
	{
		if(mIsData == mParent->IsOptimizedForQuality())
		{
			for(i=0; i<6; i++) mReplicationFactors[i] = mParent->mReplicationFactors[i];
		}
		else
		{
			for(i=0; i<6; i++) mReplicationFactors[i] = 0;
		}
	}

	for(i=0; !work && i<6; i++)
	{
		if(mOldReplicationFactors[i] != mReplicationFactors[i]) work = true;
	}

	if(work)
	{
		this->UpdateReplicasBody();
		for(i=0; i<6; i++) mOldReplicationFactors[i] = mReplicationFactors[i];
	}
}


void iReplicatedElement::SetReplicationFactors(const int n[6])
{
	int i;
	for(i=0; i<6; i++) mReplicationFactors[i] = n[i];
	this->UpdateReplicas();
}


void iReplicatedElement::GetReplicationFactors(int n[6]) const
{
	int i;
	for(i=0; i<6; i++) n[i] = mReplicationFactors[i];
}
