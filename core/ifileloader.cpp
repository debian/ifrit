/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ifileloader.h"


#include "idata.h"
#include "idataconsumer.h"
#include "idatalimits.h"
#include "idatareader.h"
#include "idatareaderobserver.h"
#include "idatasubject.h"
#include "ierror.h"
#include "ifile.h"
#include "ihistogrammaker.h"
#include "imonitor.h"
#include "iparallelmanager.h"
#include "ishell.h"
#include "ishelleventobservers.h"
#include "isystem.h"
#include "iviewmodule.h"
#include "ivtktype.h"

#include <vtkDataSet.h>

//
//  Templates
//
#include "iarray.tlh"


#define	IDATASUBJECT_DEFINE_SWAP_BYTES_FUNCTIONS(_type_) \
void iFileLoader::SwapBytes(_type_ &p) \
{ \
	iFileLoader_Private::SwapBytes(p); \
} \
void iFileLoader::SwapBytesRange(_type_ *p, vtkIdType count) \
{ \
	iFileLoader_Private::SwapBytesRange(p,count); \
}

#define	IDATASUBJECT_DEFINE_READ_BLOCK_FUNCTIONS(_type_) \
bool iFileLoader::ReadBlock(iFile& F, _type_ *p, vtkIdType len, double updateStart, double updateDuration) \
{ \
	return iFileLoader_Private::ReadBlockToArray(F,p,len,updateStart,updateDuration,mIsBigEndian!=iSystem::IsBigEndianMachine(),this->GetShell()->GetExecutionEventObserver()); \
} \
iFileLoader::ReadingBuffer iFileLoader::Buffer(_type_ &d) const \
{ \
	return ReadingBuffer(&d,1UL,iFileLoader_Private::TypeIndex(&d),sizeof(_type_)); \
} \
iFileLoader::ReadingBuffer iFileLoader::Buffer(_type_ *d, vtkIdType l) const \
{ \
	return ReadingBuffer(d,l,iFileLoader_Private::TypeIndex(d),sizeof(_type_)); \
}


namespace iFileLoader_Private
{
	//
	//  Byte swap helpers
	//
	inline void Swap4Bytes(char* data)
	{ 
		char one_byte; 
		one_byte = data[0]; data[0] = data[3]; data[3] = one_byte;
		one_byte = data[1]; data[1] = data[2]; data[2] = one_byte; 
	}


	inline void Swap8Bytes(char* data)
	{ 
		char one_byte;
		one_byte = data[0]; data[0] = data[7]; data[7] = one_byte;
		one_byte = data[1]; data[1] = data[6]; data[6] = one_byte;
		one_byte = data[2]; data[2] = data[5]; data[5] = one_byte;
		one_byte = data[3]; data[3] = data[4]; data[4] = one_byte; 
	}

	
	void Swap4BytesRange(char *data, vtkIdType count)
	{
		char *pos;
		vtkIdType i;

		pos = data;
		for(i = 0; i<count; i++)
		{
			Swap4Bytes(pos);
			pos += 4;
		}
	}


	void Swap8BytesRange(char *data, vtkIdType count)
	{
		char *pos;
		vtkIdType i;

		pos = data;
		for(i = 0; i<count; i++)
		{
			Swap8Bytes(pos);
			pos += 8;
		}
	}


	template<class T>
	inline void SwapBytes(T &p)
	{
		switch(sizeof(T))
		{
		case 4:
			{
				Swap4Bytes((char *)&p);
				break;
			}
		case 8:
			{
				Swap8Bytes((char *)&p);
				break;
			}
		default:
			{
				IBUG_FATAL("IFrIT was not ported to that platform.");
			}
		}
	}


	template<class T>
	inline void SwapBytesRange(T *p, vtkIdType count)
	{
		switch(sizeof(T))
		{
		case 4:
			{
				Swap4BytesRange((char *)p,count);
				break;
			}
		case 8:
			{
				Swap8BytesRange((char *)p,count);
				break;
			}
		default:
			{
				IBUG_FATAL("IFrIT was not ported to that platform.");
			}
		}
	}


	//
	//  Check validity of numbers
	//
	bool IsNumberValid(vtkTypeInt32 v){ return true; }
	bool IsNumberValid(vtkTypeInt64 v){ return true; }
	bool IsNumberValid(vtkTypeFloat32 v){ return (-iMath::_FloatMax<=v && v<=iMath::_FloatMax); }
	bool IsNumberValid(vtkTypeFloat64 v){ return (-iMath::_DoubleMax<=v && v<=iMath::_DoubleMax); }


	//
	//  Type indices
	//
	const int _Int32 = 1;
	const int _Int64 = 2;
	const int _Float32 = 3;
	const int _Float64 = 4;

	int TypeIndex(vtkTypeInt32 *){ return _Int32; }
	int TypeIndex(vtkTypeInt64 *){ return _Int64; }
	int TypeIndex(vtkTypeFloat32 *){ return _Float32; }
	int TypeIndex(vtkTypeFloat64 *){ return _Float64; }


	//
	//  Read blocks of data from a file
	//
	template<class T>
	bool ReadBlockToArray(iFile& F, T *p, vtkIdType len, double updateStart, double updateDuration, bool swapbytes, iExecutionEventObserver *observer)
	{
		const vtkIdType nline = 16384L;
		vtkIdType nread, l, nstep = (len+nline-1)/nline;
		double ustep = updateDuration/nstep;

		for(l=0; l<nstep; l++) 
		{
			if(observer->IsAborted()) return true;
			if(l < nstep-1) nread = nline; else nread  = len - nline*l;
			if(!F.ReadBlock(p+l*nline,nread*sizeof(T))) return false;
			if(ustep > 0.0) observer->SetProgress(updateStart+ustep*l);
		}

		if(swapbytes) 
		{
			iFileLoader_Private::SwapBytesRange(p,len);
		}

		bool ok = true;
		for(l=0; ok && l<len; l++)
		{
			ok = iFileLoader_Private::IsNumberValid(p[l]);
		}

		return ok;
	}


	bool ReadBlockToBuffer(iFile& F, const iFileLoader::ReadingBuffer &b, double updateStart, double updateDuration, bool swapbytes, iExecutionEventObserver *observer)
	{
		switch(b.Type)
		{
		case _Int32:
			{
				vtkTypeInt32 *d = (vtkTypeInt32 *)b.Data;
				return iFileLoader_Private::ReadBlockToArray(F,d,b.Length,updateStart,updateDuration,swapbytes,observer);
			}
		case _Int64:
			{
				vtkTypeInt64 *d = (vtkTypeInt64 *)b.Data;
				return iFileLoader_Private::ReadBlockToArray(F,d,b.Length,updateStart,updateDuration,swapbytes,observer);
			}
		case _Float32:
			{
				vtkTypeFloat32 *d = (vtkTypeFloat32 *)b.Data;
				return iFileLoader_Private::ReadBlockToArray(F,d,b.Length,updateStart,updateDuration,swapbytes,observer);
			}
		case _Float64:
			{
				vtkTypeFloat64 *d = (vtkTypeFloat64 *)b.Data;
				return iFileLoader_Private::ReadBlockToArray(F,d,b.Length,updateStart,updateDuration,swapbytes,observer);
			}
		default:
			{
				return false;
			}
		}
	}


	template<class T>
	bool AnalyzeFirstRecordForType(iFile &F, unsigned int len, bool &isBig)
	{
		T lrec1, lrec2;

		int m = F.SetMarker();
		if(!F.ReadBlock(&lrec1,sizeof(T)) || !F.SkipBlock(len) || !F.ReadBlock(&lrec2,sizeof(T)))
		{
			F.ReturnToMarker(m,true);
			return false; // unable even to read
		}
		F.ReturnToMarker(m,true);

		//
		//  Is the header/footer length ok?
		//
		if(lrec1 == lrec2) // succeeded
		{
			//
			// auto-detect data endiness
			//
			if(lrec1 != len)
			{
				SwapBytes(lrec1);
				if(lrec1 != len) return false; else isBig = !iSystem::IsBigEndianMachine();
			}
			else isBig = iSystem::IsBigEndianMachine();
			return true;
		}
		else return false;
	}
};


using namespace iFileLoader_Private;
using namespace iParameter;


iFileLoader::iFileLoader(iDataReader *r, int priority) : iViewModuleComponent(r?r->GetViewModule():0), mReader(r), mReaderExtension(0), mPriority(priority)
{
	this->Define();
}


iFileLoader::iFileLoader(iDataReaderExtension *ext, int priority) : iViewModuleComponent(ext?ext->GetReader()->GetViewModule():0), mReader(ext->GetReader()), mReaderExtension(ext), mPriority(priority)
{
	this->Define();
}


void iFileLoader::Define()
{
	IASSERT(mReader);

	mFortranHeaderFooterLength = -1;

	// mFileRoot = mFileSuffix = mLastFileName = "";
	mIsBigEndian = true;
	mTwoCopies = false;
	mRecord = -1;
	mShift[0] = mShift[1] = mShift[2] = 0.0f;
	mPeriodic[0] = mPeriodic[1] = mPeriodic[2] = false;

	mInfo = new iDataInfo; IERROR_CHECK_MEMORY(mInfo);
}


iFileLoader::~iFileLoader()
{
	delete mInfo;
	while(mStreams.Size() > 0) delete mStreams.RemoveLast();
}


bool iFileLoader::IsUsingData(const iDataType &type) const
{
	int i;
	for(i=0; i<mStreams.Size(); i++)
	{
		if(mStreams[i]->Subject->GetDataType() == type) return true;
	}
	return false;
}


const iDataInfo& iFileLoader::GetDataInfo() const
{
	if(mInfo->Size() != mStreams.Size())
	{
		mInfo->Clear();
		int i;
		for(i=0; i<mStreams.Size(); i++) *mInfo += mStreams[i]->Subject->GetDataType();
	}
	return *mInfo;
}


const iDataType& iFileLoader::GetDataType(int n) const
{
	if(n>=0 && n<mStreams.Size()) return mStreams[n]->Subject->GetDataType(); else return iDataType::Null();
}


vtkDataSet* iFileLoader::GetData(int n) const
{
	if(n>=0 && n<mStreams.Size()) return mStreams[n]->ReleasedData; else return 0;
}


vtkDataSet* iFileLoader::GetData(const iDataType &type) const
{
	int i;
	for(i=0; i<mStreams.Size(); i++)
	{
		if(mStreams[i]->Subject->GetDataType() == type) return this->GetData(i);
	}
	return 0;
}


iDataLimits* iFileLoader::GetLimits(int n) const
{
	if(n>=0 && n<mStreams.Size()) return mStreams[n]->Subject->GetLimits(); else return 0;
}


iDataSubject* iFileLoader::GetSubject(const iDataType &type) const
{
	int i;
	for(i=0; i<mStreams.Size(); i++)
	{
		if(mStreams[i]->Subject->GetDataType() == type) return mStreams[i]->Subject;
	}
	return 0;
}


int iFileLoader::GetBoundaryConditions() const
{
	return mReader->GetBoundaryConditions();
}


bool iFileLoader::IsDirectionPeriodic(int d) const
{
	if(d>=0 && d<3)
	{
		return (this->GetBoundaryConditions()==iParameter::BoundaryConditions::Periodic && mPeriodic[d]);
	}
	else return false;
}


void iFileLoader::SetDirectionPeriodic(int d, bool s)
{
	if(d>=0 && d<3)
	{
		mPeriodic[d] = s;
	}
}


void iFileLoader::LoadFile(const iString &fname)
{
	int i;
	double shift[3];

	for(i=0; i<3; i++) shift[i] = mReader->GetShift(i);

	this->LoadFile(fname,shift);
}


void iFileLoader::LoadFile(const iString &fname, const double *shift)
{
	int i;
	iMonitor h(this,"Loading file",0.8);

	mFortranHeaderFooterLength = -1; // must be set by a child class

	iString root, suffix;
	int record = this->DissectFileName(fname,root,suffix);

	this->GetShell()->GetDataReaderObserver()->StartFile(fname,this->GetDataType(0));

	for(i=0; i<mStreams.Size(); i++)
	{
		mStreams[i]->Subject->GetLimits()->BlockNotifications(true);
		if(mStreams[i]->ReleasedData!=0 && !mTwoCopies)
		{
			//
			//  The data can be referenced by other objects, so delete will not delete it. Thus, we first erase
			//  the actual data by initializing the data object
			//
			mStreams[i]->ReleasedData->Initialize();
			mStreams[i]->ReleasedData->Delete();
			mStreams[i]->ReleasedData = 0;
		}
	}

	this->LoadFileBody(suffix,fname);
	
	for(i=0; i<mStreams.Size(); i++)
	{
		mStreams[i]->Subject->GetLimits()->BlockNotifications(false);
	}

	if(h.IsStopped())
	{
		if(!mTwoCopies) this->EraseData();
		return;
	}

	//
	//  Check that we are properly configured
	//
	for(i=0; i<mStreams.Size(); i++)
	{
		if(mStreams[i]->ReleasedData == 0)
		{
			IBUG_ERROR("Stream #"+iString::FromNumber(i)+" was not assigned data. This is a bug, the file has not been read properly.");
			if(!mTwoCopies) this->EraseData();
			return;	
		}
	}

	h.SetInterval("Shifting",0.8,0.1);

	mShift[0] = mShift[1] = mShift[2] = 0.0f;
	this->ShiftData(shift);
	if(h.IsStopped())
	{
		if(!mTwoCopies) this->EraseData();
		return;
	}

	mLastFileName = fname;
	mFileSuffix = suffix;
	mFileRoot = root;
	mRecord = record;

	h.SetProgress(1.0);
	h.SetInterval("Computing",0.9,0.1);

	//
	//  Configure limits
	//
	bool overflow = false;
	iMonitor h1(this);
	for(i=0; i<mStreams.Size(); i++)
	{
		h1.SetInterval(double(i)/mStreams.Size(),1.0/mStreams.Size());

		if(mStreams[i]->Subject->IsThereData())
		{
			int var, nvars = mStreams[i]->Subject->GetHistogramMaker()->GetNumberOfComponents();
			if(nvars < 0)
			{
				IBUG_ERROR("Stream #"+iString::FromNumber(i)+" has invalid data. This is a bug, the file has not been read properly.");
				if(!mTwoCopies) this->EraseData();
				return;
			}
			mStreams[i]->Subject->GetLimits()->BlockNotifications(true);

			//
			//  Try to set the requested number of variables. DataLimits will either expand to accommodate 
			//  all of them or limit the allowed number to the number of listed records.
			//
			if(!mStreams[i]->Subject->GetLimits()->Activate(nvars) || nvars!=mStreams[i]->Subject->GetLimits()->GetNumVars())
			{
				IBUG_ERROR("Stream #"+iString::FromNumber(i)+" is unable to accommodate "+iString::FromNumber(nvars)+" fields - associated iDataLimits class has been configured incorrectly. This is a bug, the file has not been read properly.");
				if(!mTwoCopies) this->EraseData();
				return;
			}

			iMonitor h2(this);
			for(var=0; var<nvars; var++)
			{
				h2.SetInterval(double(var)/nvars,1.0/nvars);

				float range[2];
				mStreams[i]->Subject->GetHistogramMaker()->GetRange(var,range);
				if(mStreams[i]->Subject->GetHistogramMaker()->HasOverflow(var)) overflow = true;
			}

			if(mStreams[i]->Subject->GetResetOnLoad())
			{
				for(var=0; var<nvars; var++)
				{
					mStreams[i]->Subject->GetLimits()->SetRange(var,mStreams[i]->Subject->GetHistogramMaker()->GetRange(var));
				}
			}
			mStreams[i]->Subject->GetLimits()->BlockNotifications(false);

			iSyncWithDataRequest req(mStreams[i]->Subject->GetDataType());
			this->GetViewModule()->SyncWithData(req);
		}

		h1.SetProgress(1.0);
	}

	if(overflow)
	{
		this->OutputText(MessageType::Warning,"The data are read correctly, but some values in the file were outside of range ("+iString::FromNumber(-iMath::_LargeFloat)+","+iString::FromNumber(iMath::_LargeFloat)+").\n These values were clamped to be within the required range.");
	}

	this->OutputDataDescription();

	h.SetProgress(1.0);
	this->GetShell()->GetDataReaderObserver()->FinishFile(fname,this->GetDataInfo());
}


void iFileLoader::AttachDataToStream(int i, vtkDataSet *ds)
{
	if(i>=0 && i<mStreams.Size())
	{
		if(ds == 0)
		{
			IBUG_FATAL("Cannot attach a null data set.");
		}
		else
		{
			if(mStreams[i]->ReleasedData != 0)
			{
				mStreams[i]->ReleasedData->Delete();
			}
			mStreams[i]->ReleasedData = ds->NewInstance(); IERROR_CHECK_MEMORY(mStreams[i]->ReleasedData);
			if(mTwoCopies)
			{
				mStreams[i]->ReleasedData->DeepCopy(ds);
			}
			else
			{
				mStreams[i]->ReleasedData->ShallowCopy(ds);
			}
			this->Polish(mStreams[i]->ReleasedData);
		}
	}
}


bool iFileLoader::IsThereData(int n) const
{
	if(n>=0 && n<mStreams.Size())
	{
		return (this->GetData(n)!=0 && this->GetData(n)->GetNumberOfPoints()>0);
	}
	else return false;
}

bool iFileLoader::IsThereScalarData(int n) const
{
	if(n>=0 && n<mStreams.Size())
	{
		return ivtkType::IsThereScalarData(this->GetData(n));
	}
	else return false;
}


bool iFileLoader::IsThereVectorData(int n) const
{
	if(n>=0 && n<mStreams.Size())
	{
		return ivtkType::IsThereVectorData(this->GetData(n));
	}
	else return false;
}


bool iFileLoader::IsThereTensorData(int n) const
{
	if(n>=0 && n<mStreams.Size())
	{
		return ivtkType::IsThereTensorData(this->GetData(n));
	}
	else return false;
}

bool iFileLoader::IsThereData() const
{
	int i;
	for(i=0; i<mStreams.Size(); i++)
	{
		if(this->IsThereData(i)) return true;
	}
	return false;
}


bool iFileLoader::IsThereData(const iDataType &type) const
{
	int i;
	for(i=0; i<mStreams.Size(); i++)
	{
		if(mStreams[i]->Subject->GetDataType() == type) return this->IsThereData(i);
	}
	return false;
}


void iFileLoader::EraseData()
{
	int i;
	for(i=0; i<mStreams.Size(); i++)
	{
		if(mStreams[i]->ReleasedData != 0)
		{
			mStreams[i]->ReleasedData->Initialize();
			mStreams[i]->ReleasedData = 0;
			mStreams[i]->Subject->GetLimits()->Activate(-1);
		}

		//
		//  EraseData can be called by a loader if a file load failed; in that case the data
		//  may be in an invalid state, so send the request anyway, even if the data were not released
		//
		iSyncWithDataRequest req(mStreams[i]->Subject->GetDataType());
		this->GetViewModule()->SyncWithData(req);
	}

	mRecord = -1;
	mShift[0] = mShift[1] = mShift[2] = 0.0f;
}



void iFileLoader::SetTwoCopies(bool s)
{
	if(mTwoCopies != s)
	{
		mTwoCopies = s;
	}
}


void iFileLoader::Reset()
{
	int i;
	for(i=0; i<mStreams.Size(); i++) if(mStreams[i]->ReleasedData != 0)
	{
		mStreams[i]->ReleasedData->Modified();

		iSyncWithDataRequest req(mStreams[i]->Subject->GetDataType());
		this->GetViewModule()->SyncWithData(req);
	}
}


void iFileLoader::ShiftData(const double *shift)
{
	int i;
	double dx[3];

	for(i=0; i<3; i++) dx[i] = shift[i] - mShift[i];

	iMonitor h(this);
	for(i=0; i<this->NumStreams(); i++) if(mStreams[i]->ReleasedData != 0)
	{
		h.SetInterval("Shifting",double(i)/this->NumStreams(),1.0/this->NumStreams());
		this->ShiftDataBody(mStreams[i]->ReleasedData,dx);
	}

	for(i=0; i<3; i++) mShift[i] = shift[i];
}


float iFileLoader::GetMemorySize() const
{
	int i;
	float s = 0.0f;

	for(i=0; i<mStreams.Size(); i++) if(mStreams[i]->ReleasedData != 0)
	{
		s += mStreams[i]->ReleasedData->GetActualMemorySize();
	}
	if(mTwoCopies) s *= 2;
	return s;
}


iFileLoader::Stream* iFileLoader::CreateNewStream() const
{
	return new Stream();
}


//
//  Data reading helpers
//
bool iFileLoader::SkipFortranRecord(iFile& F, vtkIdType len)
{
	vtkIdType lrec1, lrec2;

	if(!this->ReadFortranHeaderFooter(F,lrec1)) return false;
	if(!F.SkipBlock(len)) return false;
	if(!this->ReadFortranHeaderFooter(F,lrec2)) return false;
	if(lrec1!=lrec2 || lrec1!=len) return false;

	return true;
}


bool iFileLoader::ReadFortranRecord(iFile& F, const ReadingBuffer &b, double updateStart, double updateDuration, bool autoswap, bool full)
{
	vtkIdType lrec1, lrec2;

	if(!this->ReadFortranHeaderFooter(F,lrec1)) return false;
	if(full && lrec1!=b.Length*b.Size) return false;

	if(!ReadBlockToBuffer(F,b,updateStart,updateDuration,autoswap && mIsBigEndian!=iSystem::IsBigEndianMachine(),this->GetShell()->GetExecutionEventObserver())) return false;
	if(this->GetShell()->GetExecutionEventObserver()->IsAborted()) return true;

	if(!full && b.Length*b.Size<lrec1)
	{
		if(!F.SkipBlock(lrec1-b.Length*b.Size)) return false;
	}

	if(!this->ReadFortranHeaderFooter(F,lrec2)) return false;
	if(lrec1 != lrec2) return false;

	return true;
}


bool iFileLoader::ReadFortranRecord(iFile& F, const ReadingBuffer &b1, const ReadingBuffer &b2, double updateStart, double updateDuration, bool autoswap)
{
	vtkIdType lrec1, lrec2;
	vtkIdType len = b1.Length*b1.Size + b2.Length*b2.Size;
	float f1 = float(b1.Length*b1.Size)/len;
	float f2 = float(b2.Length*b2.Size)/len;

	if(!this->ReadFortranHeaderFooter(F,lrec1)) return false;
	if(lrec1 != len) return false;

	if(!ReadBlockToBuffer(F,b1,updateStart,f1*updateDuration,autoswap && mIsBigEndian!=iSystem::IsBigEndianMachine(),this->GetShell()->GetExecutionEventObserver())) return false;
	if(this->GetShell()->GetExecutionEventObserver()->IsAborted()) return true;
	if(!ReadBlockToBuffer(F,b2,updateStart+f1*updateDuration,f2*updateDuration,autoswap && mIsBigEndian!=iSystem::IsBigEndianMachine(),this->GetShell()->GetExecutionEventObserver())) return false;
	if(this->GetShell()->GetExecutionEventObserver()->IsAborted()) return true;

	if(!this->ReadFortranHeaderFooter(F,lrec2)) return false;
	if(lrec1 != lrec2) return false;

	return true;
}


bool iFileLoader::ReadFortranRecord(iFile& F, const ReadingBuffer &b1, const ReadingBuffer &b2, const ReadingBuffer &b3, double updateStart, double updateDuration, bool autoswap)
{
	vtkIdType lrec1, lrec2;
	vtkIdType len = b1.Length*b1.Size + b2.Length*b2.Size + b3.Length*b3.Size;
	float f1 = float(b1.Length*b1.Size)/len;
	float f2 = float(b2.Length*b2.Size)/len;
	float f3 = float(b3.Length*b3.Size)/len;

	if(!this->ReadFortranHeaderFooter(F,lrec1)) return false;
	if(lrec1 != len) return false;

	if(!ReadBlockToBuffer(F,b1,updateStart,f1*updateDuration,autoswap && mIsBigEndian!=iSystem::IsBigEndianMachine(),this->GetShell()->GetExecutionEventObserver())) return false;
	if(this->GetShell()->GetExecutionEventObserver()->IsAborted()) return true;
	if(!ReadBlockToBuffer(F,b2,updateStart+f1*updateDuration,f2*updateDuration,autoswap && mIsBigEndian!=iSystem::IsBigEndianMachine(),this->GetShell()->GetExecutionEventObserver())) return false;
	if(this->GetShell()->GetExecutionEventObserver()->IsAborted()) return true;
	if(!ReadBlockToBuffer(F,b3,updateStart+(f1+f2)*updateDuration,f3*updateDuration,autoswap && mIsBigEndian!=iSystem::IsBigEndianMachine(),this->GetShell()->GetExecutionEventObserver())) return false;
	if(this->GetShell()->GetExecutionEventObserver()->IsAborted()) return true;

	if(!this->ReadFortranHeaderFooter(F,lrec2)) return false;
	if(lrec1 != lrec2) return false;

	return true;
}

	
bool iFileLoader::ReadFortranRecord(iFile& F, int nbufs, const ReadingBuffer *b, double updateStart, double updateDuration, bool autoswap) // read any number of buffers
{
	vtkIdType lrec1, lrec2;

	if(b==0 || nbufs<=0 || !this->ReadFortranHeaderFooter(F,lrec1)) return false;

	updateDuration /= nbufs;

	int i;
	vtkIdType len = 0L;
	for(i=0; i<nbufs; i++)
	{
		len += b[i].Length*b[i].Size;
		if(!ReadBlockToBuffer(F,b[i],updateStart+i*updateDuration,updateDuration,autoswap && mIsBigEndian!=iSystem::IsBigEndianMachine(),this->GetShell()->GetExecutionEventObserver())) return false;
		if(this->GetShell()->GetExecutionEventObserver()->IsAborted()) return true;
	}

	if(!this->ReadFortranHeaderFooter(F,lrec2)) return false;
	if(lrec1!=lrec2 || lrec1!=len) return false;

	return true;
}


bool iFileLoader::ReadFortranHeaderFooter(iFile& F, vtkIdType &lrec)
{
	bool ret = false;

	if(mFortranHeaderFooterLength == 8)
	{
		vtkTypeInt64 ltmp;
		ret = F.ReadBlock(&ltmp,8);
		if(ret)
		{
			if(mIsBigEndian != iSystem::IsBigEndianMachine()) this->SwapBytes(ltmp);
			if(sizeof(vtkIdType) < 8)
			{
				IBUG_WARN("VTK is compiled with 32-bit Ids. The record length of this file is 64-bits, it cannot be read with this VTK installation. To be able to read this file you will have to recompile VTK with the advanced option VTK_USE_64BIT_IDS set to ON and then recompile IFrIT.");
				return false;
			}
			lrec = (vtkTypeUInt64)ltmp;
		}
	}
	else if(mFortranHeaderFooterLength == 4)
	{
		vtkTypeInt32 ltmp;
		ret = F.ReadBlock(&ltmp,4);
		if(ret)
		{
			if(mIsBigEndian != iSystem::IsBigEndianMachine()) this->SwapBytes(ltmp);
			lrec = (vtkTypeUInt32)ltmp;
		}
	}
	else
	{
		IBUG_WARN("Invalid Fortran header/footer length.");
	}

	return ret;
}


bool iFileLoader::DetectFortranFileStructure(iFile &F, vtkIdType len)
{
	//
	//  Try 4-byte records
	//
	if(AnalyzeFirstRecordForType<vtkTypeInt32>(F,len,mIsBigEndian))
	{
		mFortranHeaderFooterLength = sizeof(vtkTypeInt32);
		return true;
	}
	//
	//  Try 8-byte records
	//
	if(AnalyzeFirstRecordForType<vtkTypeInt64>(F,len,mIsBigEndian))
	{
		mFortranHeaderFooterLength = sizeof(vtkTypeInt64);
		return true;
	}
	//
	//  Failed to figure it out
	//
	return false;
}


iString iFileLoader::GetRecordAsString(int rec) const
{
	const iString format = "%0" + iString::FromNumber(mReader->GetRecordLength()) + "d";
	return iString::FromNumber(rec,format.ToCharPointer());
}


int iFileLoader::DissectFileName(const iString &fname, iString &root, iString &suffix) const
{
	bool ok;
	int rec;

	if(fname.Contains('.') > 0)
	{
		suffix = fname.Section(".",-1);
		root = fname.Part(0,fname.Length()-suffix.Length()-1);
	}
	else
	{
		suffix.Clear();
		root = fname;
	}
	iString record = root.Part(-mReader->GetRecordLength());
	rec = record.ToInt(ok);
	if(!ok || record!=this->GetRecordAsString(rec) || rec<0) 
	{
		return -1;
	}
	else
	{
		root = root.Part(0,root.Length()-mReader->GetRecordLength());
		return rec;
	}
}


const iString iFileLoader::GetFileName(int rec) const
{
	if(mFileSuffix.IsEmpty())
	{
		return mFileRoot + this->GetRecordAsString(rec);
	}
	else
	{
		return mFileRoot + this->GetRecordAsString(rec) + "." + mFileSuffix;
	}
}


bool iFileLoader::IsSeriesFileName(const iString &fname) const
{
	iString root, suffix;
	int	rec = this->DissectFileName(fname,root,suffix);
	return (rec>=0 && root==mFileRoot && suffix==mFileSuffix);
}


//
//  Optional final touch
//
void iFileLoader::Polish(vtkDataSet *data)
{
}


//
//  Overloads
//
IDATASUBJECT_DEFINE_SWAP_BYTES_FUNCTIONS(vtkTypeInt32);
IDATASUBJECT_DEFINE_SWAP_BYTES_FUNCTIONS(vtkTypeInt64);
IDATASUBJECT_DEFINE_SWAP_BYTES_FUNCTIONS(vtkTypeFloat32);
IDATASUBJECT_DEFINE_SWAP_BYTES_FUNCTIONS(vtkTypeFloat64);

IDATASUBJECT_DEFINE_READ_BLOCK_FUNCTIONS(vtkTypeInt32);
IDATASUBJECT_DEFINE_READ_BLOCK_FUNCTIONS(vtkTypeInt64);
IDATASUBJECT_DEFINE_READ_BLOCK_FUNCTIONS(vtkTypeFloat32);
IDATASUBJECT_DEFINE_READ_BLOCK_FUNCTIONS(vtkTypeFloat64);


//
//  Helper class
//
iFileLoader::Stream::Stream()
{
	Subject = 0;
	ReleasedData = 0;
	Periodic[0] = Periodic[1] = Periodic[2] = false;
}


iFileLoader::Stream::~Stream()
{
	if(ReleasedData != 0) ReleasedData->Delete();
	if(Subject != 0) Subject->Delete();
}


#include <vtkDataSet.h>
#include <vtkImageData.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>


void iFileLoader::OutputDataDescription() const
{
	int j;

	this->OutputText(MessageType::Information,"File: "+mLastFileName);

	for(j=0; j<this->NumStreams(); j++)
	{
		const iDataType &type(mStreams[j]->Subject->GetDataType());
		vtkDataSet *data = mStreams[j]->ReleasedData;
		if(data == 0) continue;

		//
		//  DataType and VTK class names
		//
		this->OutputText(MessageType::Information,"\tStream: "+type.TextName()+" ( VTK class name: "+data->GetClassName()+")");

		if(mStreams[j]->Periodic[0] || mStreams[j]->Periodic[1] || mStreams[j]->Periodic[2])
		{
			this->OutputText(MessageType::Information,iString("\t\tPeriodic directions: ")+(mStreams[j]->Periodic[0]?"X ":"")+(mStreams[j]->Periodic[0]?"Y ":"")+(mStreams[j]->Periodic[2]?"Z":""));
		}

		//
		//  Are we ImageData?
		//
		vtkImageData *im = vtkImageData::SafeDownCast(data);
		if(im != 0)
		{
			int dims[3];
			im->GetDimensions(dims);
			this->OutputText(MessageType::Information,"\t\tDimensions = "+iString::FromNumber(dims[0])+" x "+iString::FromNumber(dims[1])+" x "+iString::FromNumber(dims[2]));
		}

		vtkPolyData *pl = vtkPolyData::SafeDownCast(data);
		if(pl != 0)
		{
			this->OutputText(MessageType::Information,"    Number of particles = "+iString::FromNumber(pl->GetNumberOfPoints()));
		}

		vtkPointData *pd = data->GetPointData();
		if(pd!=0 && pd->GetScalars() != 0)
		{
			this->OutputText(MessageType::Information,"    Number of scalar components = "+iString::FromNumber(pd->GetScalars()->GetNumberOfComponents()));
		}
		if(pd!=0 && pd->GetVectors() != 0)
		{
			this->OutputText(MessageType::Information,"    Number of vector components = "+iString::FromNumber(pd->GetVectors()->GetNumberOfComponents()));
		}
		if(pd!=0 && pd->GetTensors() != 0)
		{
			this->OutputText(MessageType::Information,"    Number of tensor components = "+iString::FromNumber(pd->GetTensors()->GetNumberOfComponents()));
		}

		int i;
		for(i=0; i<mStreams[j]->Subject->GetLimits()->GetNumVars(); i++)
		{
			float range[2];
			mStreams[j]->Subject->GetHistogramMaker()->GetRange(i,range);
			this->OutputText(MessageType::Information,"    "+mStreams[j]->Subject->GetLimits()->GetName(i)+" from "+iString::FromNumber(range[0])+" ("+iString::FromNumber(iMath::Log10(1.0e-30+fabs(range[0])))+" dex) to "+iString::FromNumber(range[1])+" ("+iString::FromNumber(iMath::Log10(1.0e-30+fabs(range[1])))+" dex)");
		}
	}
}
