/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IMEASURINGBOX_H
#define IMEASURINGBOX_H


#include "igenericprop.h"
#include "iactor.h"
#include "iviewmoduletool.h"


#include <vtkProperty.h>

class iColor;
class iMeasuringBox;
class iTextActor;

class vtkCommand;
class vtkCubeSource;
class vtkInteractorStyle;
class vtkPolyDataMapper;


class iMeasuringBoxActor : public iGenericProp<iActor>, public iViewModuleComponent
{
	
	friend class iMeasuringBox;

public:
	
	vtkTypeMacro(iMeasuringBoxActor,iActor);
	
	void AttachToInteractorStyle(vtkInteractorStyle *s);

	void SetSize(float s);
	void SetOpacity(float s);
	void SetColor(iColor &c);
	void SetBaseScale(float s);

	inline float GetOpacity(){ return mBoxActor->GetProperty()->GetOpacity(); }
	inline float GetSize(){ return mSize; }
	
	virtual double *GetBounds(){ return 0; }  // Set to zero so that the text is always displayed

protected:
	
	virtual ~iMeasuringBoxActor();
	
	virtual void UpdateGeometry(vtkViewport *vp);

private:

	iMeasuringBoxActor(iViewModule *vm);

	iTextActor *mText;
	iActor *mFrameActor, *mBoxActor, *mWorkerActor;

	vtkCommand *mObserver;

	bool mStarted;
	float mSize, mBaseScale;
	float mFactor1r, mFactor1g, mFactor1b, mFactor2;

};


class iMeasuringBox : public iViewModuleTool
{
	
public:

	vtkTypeMacro(iMeasuringBox,iViewModuleTool);
	static iMeasuringBox* New(iViewModule *vm = 0);
		
	inline iMeasuringBoxActor* GetActor() const { return mActor; }

protected:

	virtual ~iMeasuringBox();

private:
	
	iMeasuringBox(iViewModule *vm, const iString &fname, const iString &sname);

	virtual bool ShowBody(bool s);

	//
	//  Actors displayed by this class
	//
	iMeasuringBoxActor *mActor;
};

#endif // IMEASURINGBOX_H
