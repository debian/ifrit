/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IORTHOSLICER_H
#define IORTHOSLICER_H


#include "igenericfilter.h"
#include <vtkImageAlgorithm.h>


class iOrthoSlicer : public iGenericImageDataFilter<vtkImageAlgorithm>
{

	iGenericFilterTypeMacro(iOrthoSlicer,vtkImageAlgorithm);

public: 

	void SetCurrentVar(int n);
	inline int GetCurrentVar() const { return mCurVar; }

    void SetDir(int d);
    inline int GetDir() const { return mDir; }

	void SetPos(double p);
	inline double GetPos() const { return mPos[mDir]; }
    
	void SetInterpolation(bool s);
	inline bool GetInterpolation() const { return mInterpolate; }

	void SetSampleRate(int r){ mSampleRate = r; Modified(); }
	inline int GetSampleRate() const { return mSampleRate; }
	
	static void GetUV(int Axis, int &Uidx, int &Vidx);

protected:

	virtual ~iOrthoSlicer();

	virtual void ProvideOutput();

private:

	int mCurVar;
	double mPos[3];
    int mDir, mSampleRate;
	bool mInterpolate;
};

#endif // IORTHOSLICER_H
