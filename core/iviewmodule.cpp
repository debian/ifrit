/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iviewmodule.h"


#include "iactorcollection.h"
#include "ianimator.h"
#include "iboundingbox.h"
#include "icamera.h"
#include "icameraorthoactor.h"
#include "icaptioninteractorstyle.h"
#include "iclipplane.h"
#include "icolorbar.h"
#include "icrosssectionviewsubject.h"
#include "idataobject.h"
#include "idatareader.h"
#include "ierror.h"
#include "iimagecomposer.h"
#include "ikeyboardinteractorstyle.h"
#include "ilabel.h"
#include "ilights.h"
#include "imarker.h"
#include "imeasuringbox.h"
#include "imonitor.h"
#include "iobjectfactory.h"
#include "iparallelmanager.h"
#include "iparticleviewsubject.h"
#include "ipicker.h"
#include "irendertool.h"
#include "iruler.h"
#include "ishell.h"
#include "ishelleventobservers.h"
#include "isurfaceviewsubject.h"
#include "itensorfieldviewsubject.h"
#include "ivectorfieldviewsubject.h"
#include "iviewobject.h"
#include "iviewmodulecollection.h"
#include "iviewmoduleeventobservers.h"
#include "ivolumeviewsubject.h"
#include "iwriter.h"

#include <vtkCamera.h>
#include <vtkInteractorStyleFlight.h>
#include <vtkInteractorStyleJoystickCamera.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkMapper.h>
#include <vtkPicker.h>
#include <vtkProperty2D.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowCollection.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkTimerLog.h>

//
//  Templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


using namespace iParameter;


namespace iViewModule_Private
{
	//
	//  Helper functions
	//
	//vtkActor2D* CreateBackground(iRenderTool *rt);
	iActorCollection* CreateDebugProp(iRenderTool *rt);
};


using namespace iViewModule_Private;


//
//  Main class
//
iViewModule::iViewModule(iViewModuleCollection *parent, const iString &fname, const iString &sname) : iExtendableObject(parent,fname,sname), iShellComponent(parent->GetShell()),
	CloneOfWindow(iObject::Self(),static_cast<iType::ps_int::SetterType>(&iViewModule::SetCloneOfWindow),static_cast<iType::ps_int::GetterType>(&iViewModule::GetCloneOfWindow),"CloneOfWindow","co",9),
	iObjectConstructPropertyMacroS(Float,iViewModule,BoxSize,bs),
	iObjectConstructPropertyMacroV(Bool,iViewModule,Antialiasing,aa,3),
	iObjectConstructPropertyMacroS(Bool,iViewModule,BackgroundImageFixedAspect,bia),
	iObjectConstructPropertyMacroS(Color,iViewModule,BackgroundColor,bg),
	iObjectConstructPropertyMacroS(String,iViewModule,BackgroundImage,bi),
	iObjectConstructPropertyMacroS(Bool,iViewModule,CameraAlignmentLabel,ca),
	iObjectConstructPropertyMacroS(Int,iViewModule,CurrentInteractorStyle,is),
	iObjectConstructPropertyMacroS(Int,iViewModule,FontScale,fs),
	iObjectConstructPropertyMacroS(Int,iViewModule,FontType,ft),
	iObjectConstructPropertyMacroS(Int,iViewModule,ImageMagnification,ix),
	iObjectConstructPropertyMacroS(Bool,iViewModule,StereoAlignmentMarks,sam),
	iObjectConstructPropertyMacroS(Int,iViewModule,StereoMode,sm),
	iObjectConstructPropertyMacroS(Int,iViewModule,TrueRendering,tr),
	iObjectConstructPropertyMacroS(Int,iViewModule,UpdateRate,ur),
	iObjectConstructPropertyMacroV(Int,iViewModule,Position,sp,2),
	iObjectConstructPropertyMacroV(Int,iViewModule,Size,ss,2),
	iObjectConstructPropertyMacroA1(String,iViewModule,Export,exp),
	iObjectConstructPropertyMacroA(iViewModule,WriteImage,wi),
	iObjectConstructPropertyMacroA(iViewModule,Render,ren)
{
	mRenderMode = RenderMode::NoRender;

	CloneOfWindow.AddFlag(iProperty::_FunctionsAsIndex);
	CloneOfWindow.AddFlag(iProperty::_DoNotIncludeInCopy);
	Position.AddFlag(iProperty::_DoNotIncludeInCopy);
	
	mImageMagnification = 1;
	mSavedProjection = 0;

	mIsDebug = false;
	mAntialiasing[0] = mAntialiasing[2] = false;
	mAntialiasing[1] = true;
	mInImageRender = false;
	mBackgroundImageFixedAspect = false;
	mInAnimation = false;
	
	mTrueRendering = true;

	mUpdateRate = 10;
	mBoxSize = 0.0;
	mCloneOfModule = 0;

	mCameraAlignmentLabel = true;
	mStereoAlignmentMarks = true;

	mStereoType = VTK_STEREO_RED_BLUE;

	int bc = 255;
	mBackgroundColor = iColor(bc,bc,bc);
	
	//
	//  Create render view
	//
	mRenderTool = 0; // must have this zeroing - who knows what happens inside the call to iExtensionFactory::CreateRenderTool
	mRenderTool = iRenderTool::New(this); IERROR_CHECK_MEMORY(mRenderTool);
	mLights = iLights::New(this);

	//
	//  Attach to Interactor Observer
	//
	if(mRenderTool->GetRenderWindowInteractor() != 0)
	{
		mRenderTool->GetRenderWindowInteractor()->AddObserver(vtkCommand::LeftButtonPressEvent,this->GetShell()->GetInteractorObserver(),-1.0);
		mRenderTool->GetRenderWindowInteractor()->AddObserver(vtkCommand::LeftButtonReleaseEvent,this->GetShell()->GetInteractorObserver(),-1.0);
		mRenderTool->GetRenderWindowInteractor()->AddObserver(vtkCommand::MiddleButtonPressEvent,this->GetShell()->GetInteractorObserver());
		mRenderTool->GetRenderWindowInteractor()->AddObserver(vtkCommand::MiddleButtonReleaseEvent,this->GetShell()->GetInteractorObserver());
		mRenderTool->GetRenderWindowInteractor()->AddObserver(vtkCommand::RightButtonPressEvent,this->GetShell()->GetInteractorObserver());
		mRenderTool->GetRenderWindowInteractor()->AddObserver(vtkCommand::RightButtonReleaseEvent,this->GetShell()->GetInteractorObserver());
		mRenderTool->GetRenderWindowInteractor()->AddObserver(vtkCommand::MouseMoveEvent,this->GetShell()->GetInteractorObserver(),-1.0);
	}

	//
	//  File reader, gateway, volume converter
	//
	mDataObject = iDataObject::New(this); IERROR_CHECK_MEMORY(mDataObject);
	mDataReader = iDataReader::New(this); IERROR_CHECK_MEMORY(mDataReader);
	mAnimator = iAnimator::New(this); IERROR_CHECK_MEMORY(mAnimator);
	
	//
	//  Image writer
	//
	mWriter = iWriter::New(this); IERROR_CHECK_MEMORY(mWriter); 

	//
	//  Bounding box
	//	
	mBoundingBox = iBoundingBox::New(this); IERROR_CHECK_MEMORY(mBoundingBox);
	mBoundingBox->Show(true);

	//
	//  Ruler
	//
	mRuler = iRuler::New(this); IERROR_CHECK_MEMORY(mRuler);

	mCameraOrthoActor = iCameraOrthoActor::New(mRenderTool);
	mCameraOrthoActor->PickableOff();
	mCameraOrthoActor->SetVisibility(1);
	mRenderTool->AddObject(mCameraOrthoActor);

	//
	//  Text label actors
	//
	mLabel = iLabel::New(this); IERROR_CHECK_MEMORY(mLabel);
	mLabel->Show(true);

	//
	//  Measuring box
	//
	mMeasuringBox = iMeasuringBox::New(this); IERROR_CHECK_MEMORY(mMeasuringBox);

	//
	//  Color bars
	//
	mColorBar = iColorBar::New(this); IERROR_CHECK_MEMORY(mColorBar);
	mColorBar->Show(true);

	//
	//  Clipping plane
	//
	mClipPlane = iClipPlane::New(this); IERROR_CHECK_MEMORY(mClipPlane);

	//
	//  Other observers
	//
	mRenderObserver = iRenderEventObserver::New(this); IERROR_CHECK_MEMORY(mRenderObserver);
	mRenderTool->AddObserver(vtkCommand::AbortCheckEvent,mRenderObserver);
	mRenderTool->AddObserver(vtkCommand::StartEvent,mRenderObserver);
	mRenderTool->AddObserver(vtkCommand::EndEvent,mRenderObserver);

	//
	//  Interactor styles
	//
	mCurrentInteractorStyle = 0;

	vtkInteractorStyleFlight *isf = vtkInteractorStyleFlight::New();

	mInteractorStyle[InteractorStyle::Trackball] = vtkInteractorStyleTrackballCamera::New();
	mInteractorStyle[InteractorStyle::Joystick] = vtkInteractorStyleJoystickCamera::New();
	mInteractorStyle[InteractorStyle::Flight] = isf;
	mInteractorStyle[InteractorStyle::Keyboard] = iKeyboardInteractorStyle::New();
//	mInteractorStyle[InteractorStyle::Keyboard] = vtkInteractorStyleUnicam::New();

	int i;
	for(i=0; i<InteractorStyle::SIZE; i++)
	{
		IERROR_CHECK_MEMORY(mInteractorStyle[i]);
		mMeasuringBox->GetActor()->AttachToInteractorStyle(mInteractorStyle[i]);
	}

	isf->SetAngleStepSize(0.1);
	isf->SetMotionStepSize(0.001);

	if(mRenderTool->GetRenderWindowInteractor() != 0)
	{
		mRenderTool->GetRenderWindowInteractor()->SetInteractorStyle(mInteractorStyle[mCurrentInteractorStyle]);
	}

	//
	//  Debug props
	//
	mDebugActor = CreateDebugProp(mRenderTool);
	
	//
	//  Picker
	//
	mPicker = iPicker::New(this); IERROR_CHECK_MEMORY(mPicker);
	if(mRenderTool->GetRenderWindowInteractor() != 0)
	{
		mRenderTool->GetRenderWindowInteractor()->SetPicker(mPicker->GetHandler());
	}

	//
	//  Must be created before marker family
	//
	mInteractorStyleCaption = iCaptionInteractorStyle::New(this); IERROR_CHECK_MEMORY(mInteractorStyleCaption);

	//
	//  iViewSubjects
	//
	mMarkerObject = iMarkerObject::New(this); IERROR_CHECK_MEMORY(mMarkerObject);

	mViewObjects.Add(iObjectFactory::CreateViewObject(this,"x"));
	mViewObjects.Add(iObjectFactory::CreateViewObject(this,"p"));
	mViewObjects.Add(iObjectFactory::CreateViewObject(this,"s"));
	mViewObjects.Add(iObjectFactory::CreateViewObject(this,"t"));
	mViewObjects.Add(iObjectFactory::CreateViewObject(this,"u"));
	mViewObjects.Add(iObjectFactory::CreateViewObject(this,"v"));
	
	for(i=0; i<mViewObjects.Size(); i++) IERROR_CHECK_MEMORY(mViewObjects[i]);

	//
	//  Timer
	//
	mUpdateTimer = vtkTimerLog::New(); IERROR_CHECK_MEMORY(mUpdateTimer);
	mUpdateTime = 0.0f;

	//
	//  Render the scene
	//
	mRenderTool->GetCamera()->Reset();

	mRuler->GetActor()->SetBaseScale(mRenderTool->GetCamera()->GetDevice()->GetParallelScale());
	mMeasuringBox->GetActor()->SetBaseScale(mRenderTool->GetCamera()->GetDevice()->GetParallelScale());
	
	if(mRenderTool->GetRenderWindowInteractor() != 0)
	{
		mRenderTool->GetRenderWindowInteractor()->SetDesiredUpdateRate(mUpdateRate);
		mRenderTool->GetRenderWindowInteractor()->SetStillUpdateRate(0.001);
	}

	vtkMapper::SetResolveCoincidentTopologyToPolygonOffset();

	//
	//  Set the font size
	//
	for(i=0; i<3; i++) this->SetAntialiasing(i,mAntialiasing[i]);
	this->SetTrueRendering(mTrueRendering);
	this->SetBackgroundColor(mBackgroundColor);

	//
	//  Add extensions
	//
	iObjectFactory::InstallExtensions(this);

	iViewObject *obj;
	for(i=0; i<mExtensions.Size(); i++)
	{
		int n = 0;
		while((obj = static_cast<iViewModuleExtension*>(mExtensions[i])->CreateObject(n++)) != 0)
		{
			this->mViewObjects.Add(obj);
		}
	}

	mOldParentIndex = -1;
	mOldNumWindows = 0;

	//
	//  Initialize the interactor
	//
	//mRenderTool->GetInteractor()->Initialize();

	//
	//  Debug area
	//

	//
	//  With this we allow other objects to render us
	//
	mRenderMode = RenderMode::Self;
}


iViewModule::~iViewModule()
{
	// Clean up
	int i;

	if(this->IsClone())
	{
		//
		//  Unregister with the parent first
		//
		mCloneOfModule->mClones.Remove(this);
	}

	//
	//  Check that all clones are already deleted
	//
#ifndef I_NO_CHECK
	if(mClones.Size() > 0) IBUG_WARN("Deleting a window with undeleted clones.");
#endif

	//
	//  Delete picker first in case it holds onto an object
	//
	if(mRenderTool->GetRenderWindowInteractor() != 0)
	{
		mRenderTool->GetRenderWindowInteractor()->SetPicker(0);
	}
	mPicker->Delete();

	for(i=0; i<mViewObjects.Size(); i++) mViewObjects[i]->Delete();
	mMarkerObject->Delete();
	
	mRenderTool->RemoveObject(mCameraOrthoActor);
	mCameraOrthoActor->Delete();
	mRuler->Delete();
	mLabel->Delete();
	mMeasuringBox->Delete();
	mColorBar->Delete();
	mBoundingBox->Delete();

	mInteractorStyleCaption->Delete();

	mUpdateTimer->Delete();
	mAnimator->Delete();

	for(i=0; i<InteractorStyle::SIZE; i++) mInteractorStyle[i]->Delete();

	mDebugActor->Delete();

	mClipPlane->Delete();

	if(this->IsClone())
	{
		mChildren.Remove(mDataReader);
		mChildren.Remove(mDataObject);
	}
	else
	{
		mDataReader->Delete();
		mDataObject->Delete();
	}
	mWriter->Delete();

	mRenderObserver->Delete();

	//
	//  Extensions may use RenderTool, so we need to delete them before we delete mRenderTool
	//
	while(mExtensions.Size() > 0) mExtensions.RemoveLast()->Delete();

	mLights->Delete();
	mRenderTool->Delete();
}


bool iViewModule::BecomeClone(iViewModule *v)
{
	int i;

	if(v == this) return true;

	if(v == 0)
	{
		//
		//	Unmake the clone
		//
		v = mDataReader->GetViewModule(); 
		if(v != this)
		{
#ifndef I_NO_CHECK
			if(v != mCloneOfModule)
			{
				IBUG_WARN("Clones configured incorrectly.");
			}
#endif
			mCloneOfModule = 0;
			v->mClones.Remove(this);

			//
			//  Re-create data reader
			//
			mChildren.Remove(mDataObject);
			mChildren.Remove(mDataReader);
			mDataObject = iDataObject::New(this); IERROR_CHECK_MEMORY(mDataObject);
			mDataReader = iDataReader::New(this); IERROR_CHECK_MEMORY(mDataReader);
			mDataReader->CopyState(v->mDataReader);

			for(i=0; i<mViewObjects.Size(); i++)
			{
				mViewObjects[i]->BecomeClone(0);
			}

			//
			//  Un-show label
			//
			mLabel->Show(false); 
		}
	}
	else
	{
		//
		//	Make the clone
		//
		if(mDataReader->GetViewModule() == this)
		{
			mCloneOfModule = v;
			v->mClones.Add(this);

			//
			//  Link the Data Reader
			//
			mDataReader->Delete();
			mDataObject->Delete();
			mDataObject = v->mDataObject;
			mDataReader = v->mDataReader;
			mChildren.Add(mDataObject);
			mChildren.Add(mDataReader);

			if(!this->CopyState(v)) return false;

			for(i=0; i<mViewObjects.Size(); i++)
			{
				mViewObjects[i]->BecomeClone(v->mViewObjects[i]);
			}
		}
	}

	return true;
}


void iViewModule::SetDebugMode(bool s)
{
	int i;

	if(s != mIsDebug)
	{
		mIsDebug = s;
		if(s)
		{
			
			mVisibleProps.Clear();
			for(i=0; i<mProps.Size(); i++) if(mProps[i]->GetVisibility())
			{
				mVisibleProps.Add(mProps[i]);
				mProps[i]->VisibilityOff();
			}
			
			mDebugActor->VisibilityOn();
		}
		else
		{
			
			mAnimator->SetDebug(0);
			mDebugActor->VisibilityOff();
			
			for(i=0; i<mVisibleProps.Size(); i++)
			{
				mVisibleProps[i]->VisibilityOn();
			}
		}
		this->RequestRender();
	}
}


void iViewModule::AddObject(vtkProp* p)
{
	mRenderTool->AddObject(p);
}


void iViewModule::RemoveObject(vtkProp* p)
{
	mRenderTool->RemoveObject(p);
}


int iViewModule::GetViewObjectIndex(const iString &name) const
{
	int i;

	if(name[0] < 'a')
	{
		//
		//  Long name 
		//
		for(i=0; i<mViewObjects.Size(); i++)
		{
			if( mViewObjects[i]->LongName() == name) return i;
		}
	}
	else
	{
		//
		//  Short name
		//
		for(i=0; i<mViewObjects.Size(); i++)
		{
			if(mViewObjects[i]->ShortName() == name) return i;
		}
	}

	IBUG_FATAL("Invalid ViewObject name");

	return -1;
}


iViewObject* iViewModule::GetViewObjectByName(const iString &name) const
{
	int i;

	if(name[0] < 'a')
	{
		//
		//  Long name 
		//
		for(i=0; i<mViewObjects.Size(); i++)
		{
			if( mViewObjects[i]->LongName() == name) return mViewObjects[i];
		}
	}
	else
	{
		//
		//  Short name
		//
		for(i=0; i<mViewObjects.Size(); i++)
		{
			if(mViewObjects[i]->ShortName() == name) return mViewObjects[i];
		}
	}

	IBUG_FATAL("Invalid ViewObject name");

	return 0;
}


vtkRenderWindowCollection* iViewModule::GetRenderWindowCollection(bool inited) const
{
	vtkRenderWindowCollection *c = mRenderTool->GetRenderWindowCollection();
	if(inited) c->InitTraversal();
	return c;
}


iParallelManager* iViewModule::GetParallelManager() const
{
	return this->GetShell()->GetParallelManager();
}


void iViewModule::ActualRender()  
{
	//
	//  Activate counting for parallel performance
	//
	this->GetParallelManager()->StartCounters();
	mUpdateTimer->StartTimer();

	//
	//  Set depth peeling level
	//
	if(this->DepthPeelingRequest() > 0)
	{
		mRenderTool->SetDepthPeelingStyle(mTrueRendering);
	}
	else
	{
		mRenderTool->SetDepthPeelingStyle(0);
	}

	mRenderObserver->SetInteractive(false);
	mRenderTool->Render();
	mRenderObserver->SetInteractive(true);

	if(mOldParentIndex!=this->ParentIndex() || mOldNumWindows!=this->GetShell()->GetNumberOfViewModules())
	{
		mRenderTool->UpdateWindowName();
		mOldParentIndex = this->ParentIndex();
		mOldNumWindows = this->GetShell()->GetNumberOfViewModules();
	}

	mUpdateTimer->StopTimer();
	mUpdateTime = mUpdateTimer->GetElapsedTime();
	this->UpdatePerformance();
	this->GetParallelManager()->StopCounters();
}


void iViewModule::UpdatePerformance()
{
	mRenderObserver->PostFinished(); // manually driven
}


void iViewModule::UpdateAfterFileLoad()
{
	this->UpdateLabel();
	this->UpdatePerformance();
}



bool iViewModule::SetCameraAlignmentLabel(bool s)
{
	mCameraAlignmentLabel = s;
	mCameraOrthoActor->SetVisibility(s?1:0);
	return true;
}


bool iViewModule::SetStereoAlignmentMarks(bool s)
{
	mStereoAlignmentMarks = s;
	mRenderTool->ShowStereoAlignmentMarks(s);
	return true;
}


void iViewModule::UpdateLabel()
{
	mLabel->Update();
	//
	//  Update my clones as well
	//
	int i;
	for(i=0; i<mClones.Size(); i++) mClones[i]->UpdateLabel();
}


bool iViewModule::SetUpdateRate(int r)
{
	r = (r<0) ? 0 : r;
	r = (r>60) ? 60 : r;
	mUpdateRate = r;
	if(mRenderTool->GetRenderWindowInteractor() != 0)
	{
		mRenderTool->GetRenderWindowInteractor()->SetDesiredUpdateRate(mUpdateRate);
	}
	return true;
}


bool iViewModule::SetCurrentInteractorStyle(int n)
{
	if(n>=0 && n<InteractorStyle::SIZE)
	{
		//
		//  Specials
		//
		if(n == InteractorStyle::Flight)
		{
			mSavedProjection = mRenderTool->GetCamera()->GetDevice()->GetParallelProjection();
			mRenderTool->GetCamera()->GetDevice()->ParallelProjectionOff();
			//mRenderTool->GetInteractor()->SetLastEventPosition(mRenderTool->GetInteractor()->GetEventPosition());
		} 
		else if(mCurrentInteractorStyle == InteractorStyle::Flight)
		{
			mRenderTool->GetCamera()->GetDevice()->SetParallelProjection(mSavedProjection);
			mRenderTool->GetCamera()->Reset();
		}
		mCurrentInteractorStyle = n;
		if(mRenderTool->GetRenderWindowInteractor() != 0)
		{
			mRenderTool->GetRenderWindowInteractor()->SetInteractorStyle(mInteractorStyle[mCurrentInteractorStyle]);
		}
		return true;
	}
	else return false;
}


bool iViewModule::SetBackgroundColor(const iColor& bg)
{
	mBackgroundColor = bg;
	iColor fg = mBackgroundColor.Reverse();

	mRenderTool->SetBackground(mBackgroundColor);
	mMeasuringBox->GetActor()->SetColor(fg);
	mBoundingBox->SetColor(fg);
	return true;
}


bool iViewModule::SetBackgroundImage(const iString& name)
{
	iImage image;

	if(name.IsEmpty() || image.LoadFromFile(name))
	{
		mBackgroundImage = name;
		mRenderTool->SetBackground(image);
		return true;
	}
	else
	{
		image.Clear();
		mBackgroundImage.Clear();
		mRenderTool->SetBackground(image);
		return false;
	}
}


bool iViewModule::SetBackgroundImageFixedAspect(bool s)
{
	mRenderTool->SetBackgroundImageFixedAspect(s);
	mBackgroundImageFixedAspect = s;
	return true;
}


bool iViewModule::SetAntialiasing(int mode, bool s)
{
	if(mode>=0 && mode<3)
	{
		mAntialiasing[mode] = s;
		int mode = 0;
		if(mAntialiasing[0]) mode |= Antialiasing::Points;
		if(mAntialiasing[1]) mode |= Antialiasing::Lines;
		if(mAntialiasing[2]) mode |= Antialiasing::Polys;
		mRenderTool->SetAntialiasingMode(mode);
		return true;
	}
	else return false;
}


bool iViewModule::SetTrueRendering(int s)
{
	if(s>=0 && s<4)
	{
		mTrueRendering = s;
		mRenderTool->SetDepthPeelingStyle(s);
		return true;
	}
	else return false;
}


int iViewModule::DepthPeelingRequest() const
{
	int i, req = 0;
	bool ok = true;
	for(i=0; i<mViewObjects.Size(); i++)
	{
		switch(mViewObjects[i]->DepthPeelingRequest())
		{
		case 1:
			{
				if(req == -1) ok = false; else req = 1;
				break;
			}
		case -1:
			{
				if(req == 1) ok = false;
				req = -1;
				break;
			}
		}
	}

	if(!ok) this->OutputText(MessageType::Warning,"Rendered objects request incompatible rendering modes.");

	return req;
}


bool iViewModule::SetImageMagnification(int m)
{
	if(m>0 && m<101)
	{
		mImageMagnification = m;
		this->GetShell()->GetImageComposer()->UpdateSize();
		return true;
	}
	else return false;
}


int iViewModule::GetFullImageWidth()
{
	return this->GetShell()->GetImageComposer()->GetImageWidth();
}


int iViewModule::GetFullImageHeight()
{
	return this->GetShell()->GetImageComposer()->GetImageHeight();
}


int iViewModule::GetThisImageWidth()
{
	const int *s = mRenderTool->GetRenderWindowSize();
	return mImageMagnification*s[0];
}


int iViewModule::GetThisImageHeight()
{
	const int *s = mRenderTool->GetRenderWindowSize();
	return mImageMagnification*s[1];
}


void iViewModule::CreateImages(iStereoImageArray &images)
{
	images.Clear();
	//
	//  ask composer to composer the image
	//
	if(this->GetShell()->GetImageComposer()->Compose(this,mImages))
	{
		int i;
		images.Clear();
		for(i=0; i<mImages.Size(); i++) images.Add(mImages[i]);
	}
}


void iViewModule::RenderImages(iStereoImageArray &images)
{
	//
	//  Prepare for render; do a few more things for better representation
	//
	mBoundingBox->SetMagnification(mImageMagnification);
	
	//
	//  do the render
	//
	mInImageRender = true;		
	mRenderTool->RenderImages(mImageMagnification,images);
	mInImageRender = false;

	//
	//  Reset to the present state
	//
	mBoundingBox->SetMagnification(1);
		
	//
	// render again to restore the original view
	//
	mRenderTool->Render();
}


void iViewModule::RenderStereoImage(iStereoImage &image)
{
	//
	//  Prepare for render; do a few more things for better representation
	//
	mBoundingBox->SetMagnification(mImageMagnification);
	
	//
	//  do the render
	//
	mInImageRender = true;		
	mRenderTool->RenderStereoImage(mImageMagnification,image);
	mInImageRender = false;

	//
	//  Reset to the present state
	//
	mBoundingBox->SetMagnification(1);
		
	//
	// render again to restore the original view
	//
	mRenderTool->Render();
}


void iViewModule::WriteImages(int mode)
{
	static const iStereoImageArray dummy;
	this->WriteImages(dummy,mode);
}


void iViewModule::WriteImages(const iStereoImageArray &images, int mode)
{
	int i;
	iStereoImageArray tmp;
	iMonitor h(this,"Writing",1.0);

	if(images.Size() != mRenderTool->GetNumberOfActiveViews()) 
	{
		this->CreateImages(tmp);
		if(images.Size() != 0) IBUG_WARN("Incorrect number of images in ImageArray in WriteImages."); 
	}
	else
	{
		for(i=0; i<images.Size(); i++) tmp.Add(images[i]);
	}

	if(h.IsStopped()) return;

	//
	// Create and write files
	//
	switch(mode)
	{
	case ImageType::Image:
		{
			mWriter->WriteImageFrame(tmp);
			break;
		}
	case ImageType::MovieFrame:
		{
			if(mInAnimation) mWriter->WriteMovieFrame(tmp); else
			{
				IBUG_WARN("Trying to write a frame for an animation that has not started.");
			}
			break;
		}
	}
}


float iViewModule::GetMemorySize() const
{
	int i;
	float s = 0.0;

	for(i=0; i<mViewObjects.Size(); i++) s += mViewObjects[i]->GetMemorySize();
	s += mDataReader->GetMemorySize();

	return s;
}


//
//  Coordinate transformation functions
//
bool iViewModule::SetBoxSize(float s)
{
	if(s < 0.0) return false;

	mBoxSize = s;
	return true;
}


void iViewModule::SyncWithData(const iSyncRequest &request)
{
	if(mCloneOfModule != 0)
	{
		//
		//  If we are a clone, pass the request to the primary window.
		//
		mCloneOfModule->SyncWithData(request);
	}
	else
	{
		//
		//  If we an individual window, notify our consumers and all our clones
		//
		this->SyncWithDataBody(request);

		int i;
		for(i=0; i<mClones.Size(); i++)
		{
			mClones[i]->SyncWithDataBody(request);
		}
	}
}


void iViewModule::SyncWithDataBody(const iSyncRequest &request)
{
	int i;
	for(i=0; i<mViewObjects.Size(); i++)
	{
		mViewObjects[i]->SyncWithData(request);
	}
}


bool iViewModule::SetStereoMode(int m)
{
	mRenderTool->SetStereoMode(m);
	return true;
}


int iViewModule::GetStereoMode() const
{
	return 	mRenderTool->GetStereoMode();
}


bool iViewModule::SetFontScale(int s)
{
	mRenderTool->SetFontScale(s); 
	return true;
}


int iViewModule::GetFontScale() const
{
	return mRenderTool->GetFontScale();
}


bool iViewModule::SetFontType(int s)
{
	mRenderTool->SetFontType(s); 
	return true;
}


int iViewModule::GetFontType() const
{
	return mRenderTool->GetFontType();
}



void iViewModule::StartAnimation()
{
	if(mInAnimation)
	{
		IBUG_WARN("Previous animation has not completed properly.");
	}

	mWriter->StartMovie();
	mInAnimation = true;
}


void iViewModule::FinishAnimation()
{
	if(!mInAnimation)
	{
		IBUG_WARN("Animation is finishing without starting.");
	}

	mInAnimation = false;
}


//
//  Access to RenderTool components
//
bool iViewModule::SetPosition(int i, int v)
{
	if(i>=0 && i<2)
	{
		int s[2];
		s[0] = mRenderTool->GetRenderWindowPosition()[0];
		s[1] = mRenderTool->GetRenderWindowPosition()[1];
		s[i] = v;
		mRenderTool->SetRenderWindowPosition(s);
		return true;
	}
	else return false;
}

 
int iViewModule::GetPosition(int i) const
{ 
	return mRenderTool->GetRenderWindowPosition()[i]; 
}


bool iViewModule::SetSize(int i, int v)
{
	if(i>=0 && i<2 && v>10)
	{
		int s[2];
		s[0] = mRenderTool->GetRenderWindowSize()[0];
		s[1] = mRenderTool->GetRenderWindowSize()[1];
		s[i] = v;
		mRenderTool->SetRenderWindowSize(s);
		return true;
	}
	else return false;
}

 
int iViewModule::GetSize(int i) const
{ 
	return mRenderTool->GetRenderWindowSize()[i]; 
}


vtkRenderer* iViewModule::GetRenderer() const
{
	return mRenderTool->GetRenderer();
}


vtkRenderWindow* iViewModule::GetRenderWindow() const
{
	return mRenderTool->GetRenderWindow();
}


vtkRenderWindowInteractor* iViewModule::GetRenderWindowInteractor() const
{
	return mRenderTool->GetRenderWindowInteractor(); 
}


void iViewModule::ForceCrossSectionToUsePolygonalMethod(bool s)
{
	int k;

	iViewObject *cs = mViewObjects[0]; IASSERT(cs);
	for(k=0; k<cs->GetNumberOfSubjects(); k++)
	{
		static_cast<iCrossSectionViewSubject*>(cs->GetSubject(k))->ForcePolygonalMethod(s);
	}
}


iMarkerInstance* iViewModule::GetMarker(int n) const
{
	return mMarkerObject->GetMarker(n);
}


int iViewModule::GetNumberOfMarkers() const
{
	return mMarkerObject->GetNumber();
}


void iViewModule::LaunchInteractorStyleCaption()
{
	if(mRenderTool->GetRenderWindowInteractor() != 0)
	{
		mRenderTool->GetRenderWindowInteractor()->SetInteractorStyle(mInteractorStyleCaption);
		mInteractorStyleCaption->Launch(mInteractorStyle[mCurrentInteractorStyle]);
	}
}


bool iViewModule::CallRender()
{
	this->GetShell()->RequestRender(this,true);
	return true;
}


void iViewModule::PassRenderRequest(int mode)
{
	switch(mRenderMode)
	{
	case -1:
		{
			IBUG_FATAL("RenderMode for object "+this->LongName()+" was not set.");  // should never happen
			return;
		}
	case RenderMode::NoRender:
		{
			return; // we are not fully constructed yet
		}
	}

	switch(mode)
	{
	case RenderMode::NoRender:
		{
			break;
		}
	case RenderMode::Self:
		{
			this->GetShell()->RequestRender(this,false);
			break;
		}
	case RenderMode::Clones:
		{
			int i;
			this->GetShell()->RequestRender(this,false);
			for(i=0; i<mClones.Size(); i++)
			{
				this->GetShell()->RequestRender(mClones[i],false);
			}
			break;
		}
	default:
		{
			IBUG_FATAL("Invalid RenderMode");
			break;
		}
	}
}


bool iViewModule::CallWriteImage()
{
	this->WriteImages(ImageType::Image);
	return true;
}


bool iViewModule::SetCloneOfWindow(int w)
{
	if(w>=-1 && w<this->GetShell()->GetNumberOfViewModules() && w!=this->ParentIndex())
	{
		if(w >= 0)
		{
			return this->BecomeClone(this->GetShell()->GetViewModule(w));
		}
		else
		{
			return this->BecomeClone(0);
		}
	}
	else return false;
}


void iViewModule::FlyTo(const double *pos, float time) const
{
	if(pos == 0) return;
	//
	//  Gradual fly-to
	//
	int n = iMath::Round2Int(time/this->GetRenderer()->GetLastRenderTimeInSeconds());
	if(n > 60) n = 60;
	if(mRenderTool->GetRenderWindowInteractor() != 0)
	{
		mRenderTool->GetRenderWindowInteractor()->SetNumberOfFlyFrames(n);
		mRenderTool->GetRenderWindowInteractor()->FlyTo(this->GetRenderer(),pos[0],pos[1],pos[2]);
		mRenderTool->GetRenderer()->ResetCameraClippingRange();
	}
}


bool iViewModule::CallExport(const iString& fname)
{
	iMonitor h(this);
	mRenderTool->ExportScene(fname);
	return (!h.IsStopped());
}


//
//  Extension class
//
iObjectExtensionAbstractConstructorBeginMacro(iViewModule)
{
}


//
//  Helper functions in a private namespace
//
#include <vtkArrowSource.h>
#include <vtkCellArray.h>
#include <vtkPointData.h>
#include <vtkPolyDataMapper2D.h>

namespace iViewModule_Private
{
	iActorCollection* CreateDebugProp(iRenderTool *rt)
	{
		//
		//  Debug prop
		//
		float diam = 0.2;
		iActor *actor[3];
		vtkArrowSource *as;

		int i;
		for(i=0; i<3; i++) 
		{
			actor[i] = iActor::New(); IERROR_CHECK_MEMORY(actor[i]);
			as = vtkArrowSource::New(); IERROR_CHECK_MEMORY(as);
			as->SetShaftResolution(6);
			as->SetTipResolution(18);
			as->SetTipLength(0.5);
			as->SetShaftRadius(0.7*diam);
			as->SetTipRadius(1.3*diam);
			as->Update();
			actor[i]->SetInputConnection(as->GetOutputPort());
			as->Delete();
			actor[i]->SetScale(0.5);
			actor[i]->GetProperty()->SetAmbient(0.25);
			actor[i]->GetProperty()->SetDiffuse(0.25);
			actor[i]->GetProperty()->SetSpecular(0.5);
			actor[i]->PickableOff();
		}

		actor[0]->GetProperty()->SetColor(0.0,0.0,1.0);
		actor[0]->RotateY(-90.0);
		actor[1]->GetProperty()->SetColor(0.0,1.0,0.0);
		actor[1]->RotateZ(90.0);
		actor[2]->GetProperty()->SetColor(1.0,0.0,0.0);

		actor[ 0]->SetPosition(0.0,0.0,0.0);
		actor[ 1]->SetPosition(0.0,0.0,0.0);
		actor[ 2]->SetPosition(0.0,0.0,0.0);

		iActorCollection* debugActor = iActorCollection::New(); IERROR_CHECK_MEMORY(debugActor);
		for(i=0; i<3; i++) 
		{
			debugActor->AddActor(actor[i]);
			rt->AddObject(actor[i]);
			actor[i]->Delete();
		}
		debugActor->VisibilityOff();

		return debugActor;
	}
};

