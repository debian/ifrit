/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iboundedpolydatasource.h"


#include "idatalimits.h"
#include "ierror.h"
#include "itransform.h"
#include "iviewmodule.h"

#include <vtkClipPolyData.h>
#include <vtkMath.h>
#include <vtkPlanes.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkTransformPolyDataFilter.h>

//
//  Templates
//
#include "igenericfilter.tlh"


//
// iBoundedPolyDataSource class
//
iBoundedPolyDataSource::iBoundedPolyDataSource(iDataConsumer *consumer, bool normalCanChange, bool radiusCanChange) : iGenericPolyDataFilter<vtkPolyDataAlgorithm>(consumer,0,true), mNormalCanChange(normalCanChange), mRadiusCanChange(radiusCanChange)
{
	mTransform = iTransform::New(); IERROR_CHECK_MEMORY(mTransform);
	mClipper = vtkClipPolyData::New(); IERROR_CHECK_MEMORY(mClipper);
	mClipPlanes = vtkPlanes::New(); IERROR_CHECK_MEMORY(mClipPlanes);
	mFilter = vtkTransformPolyDataFilter::New(); IERROR_CHECK_MEMORY(mFilter);

	mClipper->SetClipFunction(mClipPlanes);
	mClipper->InsideOutOn();

	mFilter->SetTransform(mTransform);
	mTransform->PreMultiply();

	mRadius = 1.0;

	mCenter[0] = mCenter[1] = mCenter[2] = 0.0;
	mNormal[0] = mNormal[1] = 0.0f; mNormal[2] = 1.0f;
	mResolution = 2;

	mBounds[0] = mBounds[2] = mBounds[4] = -1.0;
	mBounds[1] = mBounds[3] = mBounds[5] = 1.0;
	mClipPlanes->SetBounds(mBounds);

	mNormalChanged = true;

	mClipper->SetInputConnection(mFilter->GetOutputPort());
}


iBoundedPolyDataSource::~iBoundedPolyDataSource()
{
	mClipper->Delete();
	mClipPlanes->Delete();
	mTransform->Delete();
	mFilter->Delete();
}


unsigned long iBoundedPolyDataSource::AddObserver(unsigned long e, vtkCommand *c, float p)
{
	this->AddObserverToSource(e,c,p);
	mFilter->AddObserver(e,c,p);
	return mClipper->AddObserver(e,c,p);
}


void iBoundedPolyDataSource::SetResolution(int r)
{
	if(r > 0)
	{
		mResolution = r;
		this->UpdateSourceResolution();
		this->Modified();
	}
}


void iBoundedPolyDataSource::SetCenter(const double x[3])
{
	int i;
	for(i=0; i<3; i++) mCenter[i] = x[i];
	this->Modified();
}


void iBoundedPolyDataSource::SetNormal(const double n[3])
{
	int i;
	for(i=0; i<3; i++) mNormal[i] = n[i];
	vtkMath::Normalize(mNormal);
	mNormalChanged = true;
	this->Modified();
}


void iBoundedPolyDataSource::SetRadius(double r)
{
	if(r > 0.0)
	{
		mRadius = r;
		this->Modified();
	}
}


void iBoundedPolyDataSource::SetBounds(double b0, double b1, double b2, double b3, double b4, double b5)
{
	mBounds[0] = b0;
	mBounds[1] = b1;
	mBounds[2] = b2;
	mBounds[3] = b3;
	mBounds[4] = b4;
	mBounds[5] = b5;
	mClipPlanes->SetBounds(mBounds);
	this->Modified();
}


void iBoundedPolyDataSource::ProvideOutput()
{
	mTransform->Identity();

	if(vtkMath::Norm(mCenter) > 1.0e-30)
	{
		mTransform->Translate(mCenter);
		mFilter->Modified();
	}

	if(mRadiusCanChange && fabs(mRadius-1.0)>1.0e-10)
	{
		mTransform->Scale(mRadius,mRadius,mRadius);
		mFilter->Modified();
	}

	if(mNormalCanChange && mNormalChanged)
	{
		mTransform->SetDirection(mNormal[0],mNormal[1],mNormal[2]);
	}

	this->UpdateBoundaryConditions();

	mClipper->Update();
	this->OutputData()->ShallowCopy(mClipper->GetOutputDataObject(0));
}


float iBoundedPolyDataSource::GetMemorySize() const
{
	float s = 0.0;
	s += mClipper->GetOutput()->GetActualMemorySize();
	s += mFilter->GetOutput()->GetActualMemorySize();
	s += this->GetSourceMemorySize();
	return s;
}

