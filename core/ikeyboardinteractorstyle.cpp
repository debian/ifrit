/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ikeyboardinteractorstyle.h"


#include <vtkRenderWindowInteractor.h>


iKeyboardInteractorStyle* iKeyboardInteractorStyle::New()
{
	return new iKeyboardInteractorStyle;
}


iKeyboardInteractorStyle::iKeyboardInteractorStyle()
{
}


iKeyboardInteractorStyle::~iKeyboardInteractorStyle()
{
}


void iKeyboardInteractorStyle::OnChar() 
{
	//
	//  Are these universal???
	//
	const char _Left = 18;
	const char _Down = 21;
	const char _Up = 19;
	const char _Right = 20;

	// catch keycodes
	switch (Interactor->GetKeyCode())
	{
    case 'a':
    case 'A': 
		{ 
			MotionFactor *= 0.5;
			break;
		}
    case 'z':
    case 'Z':
		{ 
			MotionFactor *= 2.0;
			break;
		}
    case '+':
		{
			this->DoZoom(0,1);
			break;
		}
    case '-':
		{
			this->DoZoom(0,-1);
			break;
		}
    case 'h':
    case 'H':
	case _Left:
		{
			this->DoMove(-1,0);
			break;
		}
    case 'j':
    case 'J':
	case _Down:
		{
			this->DoMove(0,-1);
			break;
		}
    case 'k':
    case 'K':
	case _Up:
		{
			this->DoMove(0,1);
			break;
		}
    case 'l':
    case 'L':
	case _Right:
		{
			this->DoMove(1,0);
			break;
		}
	default:
		{
			vtkInteractorStyleTrackballCamera::OnChar();
		}
	}

	Interactor->Render();
}


void iKeyboardInteractorStyle::FakeEvent(int dx, int dy)
{
	int x, y;
	Interactor->GetMousePosition(&x,&y);
	Interactor->SetEventPosition(x-dx,y-dy);
	Interactor->SetEventPosition(x,y);
	this->FindPokedRenderer(x,y);
}


void iKeyboardInteractorStyle::DoZoom(int dx, int dy)
{
	this->FakeEvent(dx,dy);

	this->Dolly();
}


void iKeyboardInteractorStyle::DoMove(int dx, int dy)
{
	this->FakeEvent(dx,dy);

	if(Interactor->GetShiftKey())
	{
		this->Pan();
	}
	else if(Interactor->GetControlKey())
	{
		this->Spin();
	}
	else
	{
		this->Rotate();
	}
}

