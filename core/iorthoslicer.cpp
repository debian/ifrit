/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iorthoslicer.h"


#include "idata.h"
#include "ierror.h"
#include "iuniformgriddata.h"
#include "iviewmodule.h"

#include <vtkCellData.h>
#include <vtkFloatArray.h>
#include <vtkPointData.h>

using namespace iParameter;

//
//  Templates
//
#include "igenericfilter.tlh"


iOrthoSlicer::iOrthoSlicer(iDataConsumer *consumer) : iGenericImageDataFilter<vtkImageAlgorithm>(consumer,1,true)
{
	mCurVar = 0;

	mPos[0] = mPos[1] = mPos[2] = 0;
	mDir = 2; 
	mSampleRate = 1;
	mInterpolate = true;
}


iOrthoSlicer::~iOrthoSlicer() 
{
}


void iOrthoSlicer::SetCurrentVar(int n)
{ 
	if(n>=0 && n!=mCurVar)
	{
		mCurVar = n;
		this->Modified();
	}
}


void iOrthoSlicer::SetInterpolation(bool s)
{
	if(s != mInterpolate)
	{
		mInterpolate = s;
		this->Modified();
	}
}


void iOrthoSlicer::SetPos(double p)
{
	mPos[mDir] = p;
	this->Modified();
}


void iOrthoSlicer::SetDir(int d)
{
	if(d>=0 && d<3 && d!=mDir)
	{
		mDir = d;
		this->Modified();
	}
}


void iOrthoSlicer::ProvideOutput()
{
    int numCellsIn[3], dimsIn[3], dimsOut[3];
    double orgIn[3], spaIn[3];
    double orgOut[3], spaOut[3];
    int u, v;

	iOrthoSlicer::GetUV(mDir,u,v);

	iUniformGridData *input = iUniformGridData::SafeDownCast(this->InputData());
	vtkImageData *output = this->OutputData();

	output->Initialize();

	if(input == 0) return;

	input->GetNumCells(numCellsIn);
	if(numCellsIn[u]<1 || numCellsIn[v]<1) return;

	//
	//  Get info about out configuration
	//
    input->GetSpacing(spaIn);
    input->GetGlobalOrigin(orgIn);
	input->GetDimensions(dimsIn);

	//
	//  Compute the sub-sampling factor
	//
	int step = mSampleRate;
	step = (numCellsIn[u]/8 > step) ? step : numCellsIn[u]/8;
	step = (numCellsIn[v]/8 > step) ? step : numCellsIn[v]/8;
	if(step < 1) step = 1;

	//
	//  Decide what we do: cell vs point data, how many points, etc.
	//
	if(input->GetVoxelLocation() == VoxelLocation::Vertex)
	{
		//
		//  Place points as we get them
		//
		dimsOut[u] = numCellsIn[u]/step + 1;
		dimsOut[v] = numCellsIn[v]/step + 1;

		if(dimsOut[u]<2 || dimsOut[v]<2) return;

		spaOut[u] = (spaIn[u]*numCellsIn[u])/(dimsOut[u]-1);
		spaOut[v] = (spaIn[v]*numCellsIn[v])/(dimsOut[v]-1);
	}
	else	
	{
		//
		//  Create points around cells, but throw away periodic offsets
		//
		dimsOut[u] = numCellsIn[u]/step;
		dimsOut[v] = numCellsIn[v]/step;

		if(dimsOut[u]<1 || dimsOut[v]<1) return;

		spaOut[u] = (spaIn[u]*numCellsIn[u])/dimsOut[u];
		spaOut[v] = (spaIn[v]*numCellsIn[v])/dimsOut[v];
	}

	dimsOut[mDir] = 1;
	spaOut[mDir] = 1.0;

	orgOut[u] = orgIn[u];
	orgOut[v] = orgIn[v];
	orgOut[mDir] = mPos[mDir];

	//
	//  Find the intersection
	//
	int ijk[3], ijk1[3], ijk2[3];
    float ht = (mPos[mDir]-orgIn[mDir])/spaIn[mDir];	//Normalized height

	//
	//  Must do that for parallel work
	//
	if(ht<-0.01 || ht>numCellsIn[mDir]+0.01)
	{
		return;
	}
	
	if(input->GetVoxelLocation() == VoxelLocation::Center) ht -= 0.5;

	//
	//  Add a little bit of a cushion
	//
	if(ht < 0.01) ht = 0.01;
	if(ht > numCellsIn[mDir]-0.01) ht = numCellsIn[mDir] - 0.01;

    ijk1[mDir] = ijk2[mDir] = int(ht); //Grid Point below
	if(ijk2[mDir] < dimsIn[mDir]-1) ijk2[mDir]++;

#ifndef I_NO_CHECK
	if(ijk1[mDir]<0 || ijk2[mDir]>=dimsIn[mDir])
	{
		IBUG_WARN("Internal check failed.");
	}
#endif

    float ss2 = ht - ijk1[mDir];
	float ss1 = 1.0 - ss2;

	output->SetDimensions(dimsOut);
	output->SetOrigin(orgOut);
	output->SetSpacing(spaOut);

	//
	//  Prepare output scalars
	//
    vtkIdType numPts = dimsOut[u]*dimsOut[v];
	if(!wArray.Init(input->GetPointData()->GetScalars(),numPts,1))
	{
		this->SetAbortExecute(1);
		return;
	}

	wArray.PtrIn += mCurVar;

	vtkIdType loffOut, loffIn1, loffIn2;
	for(ijk[v]=0; ijk[v]<dimsOut[v]; ijk[v]++)
	{
		this->UpdateProgress(double(ijk[v])/dimsOut[v]);
		if(this->GetAbortExecute()) break;

		for(ijk[u]=0; ijk[u]<dimsOut[u]; ijk[u]++)
		{
//			loffOut = wScalarDimOut*(ijk[u]+ijk[v]*(vtkIdType)dim[u]);
			loffOut = ijk[u] + ijk[v]*(vtkIdType)dimsOut[u];  //  wScalarDimOut = 1

			if(mInterpolate)
			{
				ijk1[u] = ijk[u]*step + step/2;
				ijk1[v] = ijk[v]*step + step/2;
				loffIn1 = wArray.DimIn*(ijk1[0]+(vtkIdType)dimsIn[0]*(ijk1[1]+(vtkIdType)dimsIn[1]*ijk1[2]));
				ijk2[u] = ijk1[u];
				ijk2[v] = ijk1[v];
				loffIn2 = wArray.DimIn*(ijk2[0]+(vtkIdType)dimsIn[0]*(ijk2[1]+(vtkIdType)dimsIn[1]*ijk2[2]));
//				for(j=0; j<wScalarDimOut; j++)
//				{
//					wScalarPtrOut[loffOut+j] = ss1*wScalarPtrIn[loffIn1+j] + ss2*wScalarPtrIn[loffIn2+j];
//				}
				wArray.PtrOut[loffOut] = ss1*wArray.PtrIn[loffIn1] + ss2*wArray.PtrIn[loffIn2];  //  wScalarDimOut = 1
			}
			else
			{
				if(ss2 < 0.5f)
				{
					ijk1[u] = ijk[u]*step + step/2;
					ijk1[v] = ijk[v]*step + step/2;
					loffIn1 = wArray.DimIn*(ijk1[0]+(vtkIdType)dimsIn[0]*(ijk1[1]+(vtkIdType)dimsIn[1]*ijk1[2]));
				}
				else
				{
					ijk2[u] = ijk[u]*step + step/2;
					ijk2[v] = ijk[v]*step + step/2;
					loffIn1 = wArray.DimIn*(ijk2[0]+(vtkIdType)dimsIn[0]*(ijk2[1]+(vtkIdType)dimsIn[1]*ijk2[2]));
				}
//				for(j=0; j<wScalarDimOut; j++)
//				{
//					wScalarPtrOut[loffOut+j] = wScalarPtrIn[loffIn1+j];
//				}
				wArray.PtrOut[loffOut] = wArray.PtrIn[loffIn1];  //  wScalarDimOut = 1
			}
		}
    }
    
	if(input->GetVoxelLocation() == VoxelLocation::Vertex)
	{
		output->GetPointData()->SetScalars(wArray.ArrOut);
		wArray.ArrOut->Delete();
	}
	else
	{
		output->GetCellData()->SetScalars(wArray.ArrOut);
		wArray.ArrOut->Delete();
	}
}


void iOrthoSlicer::GetUV(int Axis, int &u, int &v)
{
	switch(Axis)
	{
	case 0:
		{
			u = 1;
			v = 2;
			return;
		}
	case 1:
		{
			u = 0;
			v = 2;
			return;
		}
	case 2:
		{
			u = 0;
			v = 1;
			return;
		}
	}
}

