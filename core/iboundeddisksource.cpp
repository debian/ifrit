/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iboundeddisksource.h"


#include "ierror.h"

#include <vtkAppendPolyData.h>
#include <vtkArrowSource.h>
#include <vtkDiskSource.h>
#include <vtkPolyData.h>
#include <vtkPolyDataNormals.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>

//
//  Templates
//
#include "igenericfilter.tlh"


//
// iBoundedDiskSource class
//
iBoundedDiskSource* iBoundedDiskSource::New(iDataConsumer *consumer)
{
	IASSERT(consumer);
	return new iBoundedDiskSource(consumer);
}


iBoundedDiskSource::iBoundedDiskSource(iDataConsumer *consumer) : iBoundedPolyDataSource(consumer,true,true)
{
	mPointNormals = vtkPolyDataNormals::New(); IERROR_CHECK_MEMORY(mPointNormals);
	mSource = vtkDiskSource::New(); IERROR_CHECK_MEMORY(mSource);

	mPointNormals->SplittingOff();
	mPointNormals->ConsistencyOff();
	mPointNormals->ComputePointNormalsOn();
	mPointNormals->ComputeCellNormalsOff();
	mPointNormals->FlipNormalsOff();

	mSource->SetInnerRadius(0.0);
	mSource->SetOuterRadius(1.0);

	mAppendPolyData = vtkAppendPolyData::New(); IERROR_CHECK_MEMORY(mAppendPolyData);
	mArrowSource = vtkArrowSource::New(); IERROR_CHECK_MEMORY(mArrowSource);
	mTransformPolyDataFilter = vtkTransformPolyDataFilter::New(); IERROR_CHECK_MEMORY(mTransformPolyDataFilter);
	vtkTransform *t = vtkTransform::New(); IERROR_CHECK_MEMORY(t);
	t->Identity();
	t->RotateY(-90.0);
	mTransformPolyDataFilter->SetTransform(t);
	t->Delete();
	mTransformPolyDataFilter->SetInputConnection(mArrowSource->GetOutputPort());
	mAppendPolyData->AddInputConnection(mSource->GetOutputPort());
	mAppendPolyData->AddInputConnection(mTransformPolyDataFilter->GetOutputPort());

	mWithArrow = false;
	this->SetWithArrow(true);
}


iBoundedDiskSource::~iBoundedDiskSource()
{
	mSource->Delete();
	mPointNormals->Delete();
	mAppendPolyData->Delete();
	mArrowSource->Delete();
	mTransformPolyDataFilter->Delete();
}


void iBoundedDiskSource::SetWithArrow(bool s)
{
	if(s == mWithArrow) return;

	mWithArrow = s;
	if(s)
	{
		mFilter->SetInputConnection(mAppendPolyData->GetOutputPort());
	}
	else
	{
		mFilter->SetInputConnection(mSource->GetOutputPort());
	}
}


void iBoundedDiskSource::AddObserverToSource(unsigned long e, vtkCommand *c, float p)
{
	mSource->AddObserver(e,c,p);
	mPointNormals->AddObserver(e,c,p);
}


void iBoundedDiskSource::UpdateSourceResolution()
{
	mSource->SetRadialResolution(1+mResolution/2);
	mSource->SetCircumferentialResolution(3*mResolution);
}


float iBoundedDiskSource::GetSourceMemorySize() const
{
	float s = 0.0;
	s += mPointNormals->GetOutput()->GetActualMemorySize();
	s += mSource->GetOutput()->GetActualMemorySize();
	return s;
}


void iBoundedDiskSource::UpdateBoundaryConditions()
{
}

