/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IVECTORFIELDVIEWSUBJECT_H
#define IVECTORFIELDVIEWSUBJECT_H


#include "ifieldviewsubject.h"


#include "iposition.h"

class iBoundedDiskSource;
class iBoundedPlaneSource;
class iBoundedSphereSource;
class iMarker;
class iVectorFieldViewSubject;
class iViewObject;

class vtkCellArray;
class vtkFloatArray;
class vtkPoints;


namespace iParameter
{
	namespace VectorField
	{
		namespace Method
		{
			const int Glyph =		0;
			const int StreamLine =	1;
			const int StreamTube =	2;
			const int StreamBand =	3;

			inline bool IsValid(int m){ return m>=0 && m<=3; }
		};
	};
	namespace StreamLine
	{
		namespace Source
		{
			const int Disk =	0;
			const int Plane =	1;
			const int Sphere =	2;
			const int Marker =	3;

			inline bool IsValid(int m){ return m>=0 && m<=3; }
			//const int COUNT =	4;
		};
	};
};


class iVectorFieldViewInstance : public iFieldViewInstance, public iPropPlacementHelper
{
	
	friend class iVectorFieldViewSubject;

public:
	
	iDataConsumerTypeMacro(iVectorFieldViewInstance,iFieldViewInstance);
		
	iObjectSlotMacro1SV(Method,int);
	iObjectSlotMacro1SV(LineWidth,int);
	iObjectSlotMacro1SV(LineQuality,int);
	iObjectSlotMacro1SV(LineLength,float);
	iObjectSlotMacro1SV(LineDirection,int);
	iObjectSlotMacro1SV(TubeSize,int);
	iObjectSlotMacro1SV(TubeRangeFactor,float);
	iObjectSlotMacro1SV(TubeVariationFactor,float);
	iObjectSlotMacro1SV(NumberOfStreamLines,int);
	iObjectSlotMacro1SV(SourceType,int);
	iObjectSlotMacro1SV(SourceOpacity,float);  
	iObjectSlotMacro1SV(ShowSourceObject,bool); 
	iObjectSlotMacro1SV(GlyphBaseSize,int); 

	virtual bool CanBeShown() const;

	virtual void UpdateOnMarkerChange();

protected:
	
	iVectorFieldViewInstance(iVectorFieldViewSubject *owner);
	virtual ~iVectorFieldViewInstance();
	virtual void ConfigureBody();
	virtual void ConfigureMainPipeline(iViewSubjectPipeline *p, int id);

	virtual iViewSubjectPipeline* CreatePipeline(int id);
	virtual void ResetPipelineInput(vtkDataSet *input);
	virtual void ResetBody();

	virtual float GetMemorySize();
	virtual void RemoveInternalData();

	virtual void ShowBody(bool s);

	virtual void UpdateGlyphSize();
	virtual void UpdateGlyphSampleRate();
	virtual void UpdateStreamLineSource();
	virtual void UpdatePipelines();

	virtual void UpdateDirection();
	virtual void UpdatePosition();
	virtual void UpdateSize();

	//
	//  VTK stuff
	//
	vtkPoints *mStreamLineSourcePoints;
	vtkCellArray *mStreamLineSourceVerts;
	vtkFloatArray *mStreamLineSourceNorms;

	iBoundedDiskSource *mSourceDisk;
	iBoundedPlaneSource *mSourcePlane;
	iBoundedSphereSource *mSourceSphere;

private:
	//
	//  Slot wrappers
	//
	bool SetSourceDirection(const iVector3D& v){ return this->iPropPlacementHelper::SetDirection(v); }
	const iVector3D& GetSourceDirection() const { return this->iPropPlacementHelper::GetDirection(); }

	bool SetSourcePosition(const iVector3D& v){ return this->iPropPlacementHelper::SetBoxPosition(v); }
	const iVector3D& GetSourcePosition() const { return this->iPropPlacementHelper::GetBoxPosition(); }

	bool SetSourceSize(double p){ return this->iPropPlacementHelper::SetBoxSize(p); }
	double GetSourceSize() const { return this->iPropPlacementHelper::GetBoxSize(); }
};


class iVectorFieldViewSubject : public iFieldViewSubject
{
	
public:
	
	iViewSubjectTypeMacro2(iVectorFieldView,iFieldViewSubject);
		
	iType::vsp_int Method;
	iType::vsp_int LineWidth;
	iType::vsp_int LineQuality;
	iType::vsp_float LineLength;
	iType::vsp_int LineDirection;
	iType::vsp_int TubeSize;
	iType::vsp_float TubeRangeFactor;
	iType::vsp_float TubeVariationFactor;
	iType::vsp_int NumberOfStreamLines;
	iType::vsp_int SourceType;
	iType::vsp_float SourceOpacity;  
	iType::vsp_vector SourceDirection; 
	iType::vsp_vector SourcePosition; 
	iType::vsp_double SourceSize;
	iType::vsp_bool ShowSourceObject; 
	iType::vsp_int GlyphBaseSize;

protected:
	
	iVectorFieldViewSubject(iViewObject *parent, const iDataType &type, const iDataType &stype);
	virtual ~iVectorFieldViewSubject();
};

#endif // IVECTORFIELDVIEWSUBJECT_H

