/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IVIEWSUBJECTPIPELINEDATAMANAGER_H
#define IVIEWSUBJECTPIPELINEDATAMANAGER_H


#include <vtkObjectBase.h>
#include "idatahandler.h"


#include "iarray.h"
#include "istring.h"

#include <vtkSetGet.h>

class iViewSubjectPipelineDataManager;
class iViewInstance;

class vtkDataSet;


class iViewSubjectPipelineDataDistributor : public vtkObjectBase
{

public:

	vtkTypeMacro(iViewSubjectPipelineDataDistributor,vtkObjectBase);

	inline const iString& GetType() const { return mType; }

	bool DistributeData(vtkDataSet *globalInput, iArray<vtkDataSet*> &localInputs);

protected:

	iViewSubjectPipelineDataDistributor(iViewSubjectPipelineDataManager *manager, const iString &type);
	virtual ~iViewSubjectPipelineDataDistributor();

	//
	//  Inherit this to implement specific types
	//
	virtual bool DistributeDataBody(vtkDataSet *globalInput, iArray<vtkDataSet*> &localInputs) = 0;

	iViewSubjectPipelineDataManager *mManager;
	iString mType;
};


class iViewSubjectPipelineDataCollector : public vtkObjectBase
{

public:

	vtkTypeMacro(iViewSubjectPipelineDataCollector,vtkObjectBase);

	inline const iString& GetType() const { return mType; }

	bool CollectData(iArray<vtkDataSet*> &localOutputs, vtkDataSet *globalOutput);

protected:

	iViewSubjectPipelineDataCollector(iViewSubjectPipelineDataManager *manager, const iString &type);
	virtual ~iViewSubjectPipelineDataCollector();

	//
	//  Inherit this to implement specific types
	//
	virtual bool CollectDataBody(iArray<vtkDataSet*> &localOutputs, vtkDataSet *globalOutput) = 0;

	iViewSubjectPipelineDataManager *mManager;
	iString mType;
};


class iViewSubjectPipelineDataManager : public iDataHandler
{

public:

	static iViewSubjectPipelineDataManager* New(iViewInstance *owner);
	virtual void Delete(){ delete this; }

	void DistributeData(vtkDataSet *globalInput, iArray<vtkDataSet*> &localInputs);
	void CollectData(iArray<vtkDataSet*> &localOutputs, vtkDataSet *globalOutput);

	const iViewSubjectPipelineDataDistributor* GetDistributor(const iString &type) const;
	const iViewSubjectPipelineDataCollector* GetCollector(const iString &type) const;

	void AddDistributor(iViewSubjectPipelineDataDistributor *distributor);
	void AddCollector(iViewSubjectPipelineDataCollector *collector);

	iViewInstance* Owner() const { return mOwner; }

protected:

	virtual ~iViewSubjectPipelineDataManager();

	virtual float GetMemorySize(){ return 0.0; }
	virtual void RemoveInternalData(){}

private:

	iViewSubjectPipelineDataManager(iViewInstance *owner);

	iArray<iViewSubjectPipelineDataDistributor*> mDistributors;
	iArray<iViewSubjectPipelineDataCollector*> mCollectors;

	iViewInstance *mOwner;
};

#endif  // IVIEWSUBJECTPIPELINEDATAMANAGER_H
