/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Helper class for computing particle density in place (NOT a filter!!!) - there is no elegant way to add
//  a variable without replicating the data in the memory.
//  It can be inherited to implement a faster density assignment scheme.
//
#ifndef IPARTICLEDENSITYESTIMATOR_H
#define IPARTICLEDENSITYESTIMATOR_H


#include <vtkAlgorithm.h>
#include "iparallelworker.h"


class iParticleFileLoader;

class vtkFloatArray;
class vtkPointLocator;
class vtkPoints;
class vtkPolyData;


class iParticleDensityEstimator : public vtkAlgorithm, protected iParallelWorker
{

public:

	vtkTypeMacro(iParticleDensityEstimator,vtkAlgorithm);
	static iParticleDensityEstimator* New(iParticleFileLoader *loader = 0);

	void SetNumNeighbors(int num);
	inline int GetNumNeighbors() const { return mNumNeighbors; }

	void ComputeDensity(vtkPoints *points, vtkFloatArray *scalars, int varIn, int varOut);

protected:

	iParticleDensityEstimator(iParticleFileLoader *loader);
	virtual ~iParticleDensityEstimator();

	virtual int ExecuteStep(int step, iParallel::ProcessorInfo &p);
	virtual void PrepareForStep();

	int mNumNeighbors;
	vtkPolyData *mData;
	vtkPointLocator *mLocator;

	//
	//  Work variables
	//
	int wVarIn, wVarOut;
};

#endif  // IPARTICLEDENSITYESTIMATOR_H

