/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef ICOLOR_H
#define ICOLOR_H


class iColor
{

public:

	iColor();
	iColor(const iColor &c);
	iColor(int r, int g, int b, int a = 255);
	iColor(double c[3], double alpha = 1.0);

	inline int Red() const { return mRed; }
	inline int Green() const { return mGreen; }
	inline int Blue() const { return mBlue; }
	inline int Alpha() const { return mAlpha; }

	inline int Intensity() const { return (mRed+mGreen+mBlue)/3; }

	inline void GetRGB(char rgb[3]) const { rgb[0] = (char)mRed; rgb[1] = (char)mGreen; rgb[2] = (char)mBlue; }
	inline void GetRGB(unsigned char rgb[3]) const { rgb[0] = (unsigned char)mRed; rgb[1] = (unsigned char)mGreen; rgb[2] = (unsigned char)mBlue; }

	double* ToVTK() const;

	iColor Reverse() const;
	iColor Shadow() const;

    iColor& operator=(const iColor &c);   // impl-shared copy

	friend bool operator==(const iColor &s1, const iColor &s2);
    friend bool operator!=(const iColor &s1, const iColor &s2);

	int operator[](int i) const;
	int& operator[](int i);

	bool IsValid() const;

	//
	//  Some colors
	//
	static const iColor& IFrIT();
	static const iColor& Black();
	static const iColor& White();
	static const iColor& Invalid();

private:

	int mRed, mGreen, mBlue, mAlpha;
	mutable double mVTK[4];
};


inline bool operator==(const iColor &s1, const iColor &s2)
{
	return (s1.mRed==s2.mRed) && (s1.mGreen==s2.mGreen) && (s1.mBlue==s2.mBlue) && (s1.mAlpha==s2.mAlpha);
}


inline bool operator!=(const iColor &s1, const iColor &s2)
{
	return (s1.mRed!=s2.mRed) || (s1.mGreen!=s2.mGreen) || (s1.mBlue!=s2.mBlue) || (s1.mAlpha!=s2.mAlpha);
}


inline bool iColor::IsValid() const
{
	return (mRed>=0 && mRed<256 && mGreen>=0 && mGreen<256 && mBlue>=0 && mBlue<256 && mAlpha>=0 && mAlpha<256);
}


inline int iColor::operator[](int i) const
{
	switch(i)
	{
	case 0: return mRed;
	case 1: return mGreen;
	case 2: return mBlue;
	default: return mAlpha;
	}
}

inline int& iColor::operator[](int i)
{
	switch(i)
	{
	case 0: return mRed;
	case 1: return mGreen;
	case 2: return mBlue;
	default: return mAlpha;
	}
}

#endif // ICOLOR_H

