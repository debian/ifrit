/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
// A small set of common functionality for all IFrIT visualization objects
//

#ifndef IVIEWSUBJECT_H
#define IVIEWSUBJECT_H


#include "imaterialobject.h"
#include "idataconsumer.h"
#include "ireplicatedelement.h"


#include "icolorbaritem.h"
#include "ipropertymultiobject.h"

class iViewSubject;
class iViewSubjectObserver;
class iViewSubjectPipeline;


namespace iParameter
{
	namespace ViewObject
	{
		namespace Flag
		{
			const unsigned int IsNonReplicating = 1U;
		};
	};
};


class iViewInstance : public vtkObjectBase, public iDataConsumer, public iReplicatedElement
{
	
	friend class iViewSubject;
	friend class iViewSubjectParallelPipeline;

public:
	
	iDataConsumerTypeMacroBase(iViewInstance,vtkObjectBase);

	inline iViewSubject* Owner() const { return mOwner; }

	void Show(bool s);

	virtual bool CanBeShown() const = 0;

	virtual void UpdateOnMarkerChange();

protected:
	
	iViewInstance(iViewSubject *owner);
	virtual ~iViewInstance();

	void Configure();
	virtual void ConfigureBody() = 0; // Create and attach pipelines here. This will be called automatically by the factory.
	virtual bool IsInitialized() const { return mIsInitialized; }
	virtual void FinishInitialization(){}

	void Reset();
	virtual void ResetBody() = 0;

	virtual void ShowBody(bool s) = 0;

	void AddColorBar(int priority, int var, int palette, const iDataType &type);
	void RemoveColorBars();
	virtual void UpdateColorBars() = 0;

	virtual void UpdateReplicasBody();

	//
	//  Pipeline operation
	//
	void AddMainPipeline(int numInputs);
	virtual void ConfigureMainPipeline(iViewSubjectPipeline *p, int id);
	inline bool IsCreatingMainPipeline() const { return mCreatingMainPipeline; }
	virtual iViewSubjectPipeline* CreateMainPipeline(int numInputs, int id); // can be overwritten to create custom pipelines

	inline iViewSubjectPipeline* Pipeline(int n = 0) const { return mPipelines[n]; } // will automatically return zero if out-of-bounds
	virtual iViewSubjectPipeline* CreatePipeline(int id);

	//
	//  Members
	//
	bool mCreatingMainPipeline;
	bool mIsInitialized, mIsConfigured;

	iArray<iColorBarItem> mColorBars;

	inline int Index() const { return mIndex; }

private:

	iViewSubject *mOwner;
	int mIndex;

	//
	//  make it private so that it can only be created by AddMainPipeline calls
	//
	iArray<iViewSubjectPipeline*> mPipelines;

};


namespace iType
{
	typedef iPropertyMultiObject<iViewSubject,iViewInstance,Int>		vsp_int;
	typedef iPropertyMultiObject<iViewSubject,iViewInstance,Bool>		vsp_bool;
	typedef iPropertyMultiObject<iViewSubject,iViewInstance,Float>		vsp_float;
	typedef iPropertyMultiObject<iViewSubject,iViewInstance,Double>		vsp_double;
	typedef iPropertyMultiObject<iViewSubject,iViewInstance,String>		vsp_string;
	typedef iPropertyMultiObject<iViewSubject,iViewInstance,Pair>		vsp_pair;
	typedef iPropertyMultiObject<iViewSubject,iViewInstance,Color>		vsp_color;
	typedef iPropertyMultiObject<iViewSubject,iViewInstance,Vector>		vsp_vector;
};


//
//  Main class
//
class iViewSubject : public iMaterialObject, public iDataConsumer, public iReplicatedElement
{

	friend class iViewInstance;
	template<class Object, class Instance, iType::TypeId id> friend class iPropertyMultiObject;

public:

	const bool IsNonReplicating;

	iDataConsumerTypeMacroBase(iViewSubject,iMaterialObject);
	virtual void Delete();

	iType::pv_int ReplicationFactors;
	iType::ps_bool Visible;

	const iString GetQualifiedName() const;

	bool SetReplicationFactors(int d, int n);
	inline int GetReplicationFactors(int d) const { if(d>=0 && d<6) return mReplicationFactors[d]; else return 0; }

	inline bool IsVisible() const { return mIsVisible; }
	bool Show(bool s);
	void Reset();

	virtual float GetMemorySize();
	virtual void RemoveInternalData();

	virtual void BecomeClone(iViewSubject *v);

	inline int GetMinNumberOfInstances() const { return mMinSize; }
	inline int GetMaxNumberOfInstances() const { return mMaxSize; }
	inline int GetNumberOfInstances() const { return mInstances.Size(); }
	bool SetNumberOfInstances(int n);
	virtual bool CreateInstance();
	virtual bool RemoveInstance(int n);
	iViewInstance* GetInstance(int i) const;

	//
	//  Registry for replicated components
	//
	void RegisterReplicatedElement(iReplicatedElement *r, bool isData); 
	void UnRegisterReplicatedElement(iReplicatedElement *r); 
	const iReplicatedElement* GetDataReplicated() const { return (mDataReplicated.Size() > 0) ? mDataReplicated[0] : 0; }

	void UpdateOnMarkerChange();

	int DepthPeelingRequest() const;

protected:
	
	iViewSubject(iObject *parent, const iString &fname, const iString &sname, iViewModule *vm, const iDataType &type, unsigned int flags, int minsize, int maxsize);
	virtual ~iViewSubject();
	iViewSubject* Self(){ return this; }  // needed to avoid compiler warning

	virtual void InitInstances();
	virtual iViewInstance* MakeInstance() = 0;

	virtual bool SyncWithLimitsBody(int var, int mode); // just pass to instances

	virtual void UpdateReplicasHead();
	virtual void UpdateReplicasBody();

	virtual int DepthPeelingRequestBody() const;

	virtual bool CopyInstances(int iy, int ix);  // copy the internal state of mProps[ix] into mProps[iy] 

	virtual void WriteHead(iString &s) const;

	iViewSubjectObserver *mObjectObserver;
	iArray<iViewInstance*> mInstances;

private:

	bool mIsVisible;
	const int mMinSize, mMaxSize;

	//
	//  ReplicationElement members
	//
	iLookupArray<iReplicatedElement*> mDataReplicated, mPropReplicated;
};


#include "ierror.h"

//
//  Helper macros
//
#define iViewSubjectTypeMacro(_type_,_parent_) \
	protected: \
	virtual iViewInstance* MakeInstance() { return new _type_##Instance(this); } \
	public: \
	static _type_##Subject* New(iViewObject *parent, const iDataType &type) \
	{ \
		_type_##Subject *obj = new _type_##Subject(parent,type); IERROR_CHECK_MEMORY(obj); \
		obj->InitInstances(); \
		return obj; \
	} \
	private: \
	static _type_##Subject* New(){ return 0; } \
	iDataConsumerTypeMacroPass(_type_##Subject,_parent_)

#define iViewSubjectTypeMacro1(_type_,_parent_) \
	protected: \
	virtual iViewInstance* MakeInstance() { return new _type_##Instance(this); } \
	public: \
	static _type_##Subject* New(iViewObject *parent, const iDataType &type) \
	{ \
		_type_##Subject *obj = new _type_##Subject(parent,type); IERROR_CHECK_MEMORY(obj); \
		obj->InitInstances(); \
		return obj; \
	} \
	private: \
	static _type_##Subject* New(){ return 0; } \
	iDataConsumerTypeMacro(_type_##Subject,_parent_)

#define iViewSubjectTypeMacro2(_type_,_parent_) \
	protected: \
	virtual iViewInstance* MakeInstance() { return new _type_##Instance(this); } \
	public: \
	static _type_##Subject* New(iViewObject *parent, const iDataType &type, const iDataType &stype) \
	{ \
		_type_##Subject *obj = new _type_##Subject(parent,type,stype); IERROR_CHECK_MEMORY(obj); \
		obj->InitInstances(); \
		return obj; \
	} \
	private: \
	static _type_##Subject* New(){ return 0; } \
	iDataConsumerTypeMacroPass(_type_##Subject,_parent_)

#define iViewSubjectPropertyConstructMacroGeneric(_id_,_subject_,_fname_,_sname_,_setter_,_getter_) \
	_fname_(Self(),static_cast<iPropertyMultiObject<iViewSubject,iViewInstance,iType::_id_>::SetterType>(&_subject_::_setter_),static_cast<iPropertyMultiObject<iViewSubject,iViewInstance,iType::_id_>::GetterType>(&_subject_::_getter_),#_fname_,#_sname_)

#define iViewSubjectPropertyConstructMacroGenericOP(YES,_id_,_subject_,_fname_,_sname_,_setter_,_getter_) \
	_fname_(Self(),static_cast<iPropertyMultiObject<iViewSubject,iViewInstance,iType::_id_>::SetterType>(&_subject_::_setter_),static_cast<iPropertyMultiObject<iViewSubject,iViewInstance,iType::_id_>::GetterType>(&_subject_::_getter_),YES?#_fname_:"",YES?#_sname_:"")

#define iViewSubjectPropertyConstructMacro(_id_,_subject_,_fname_,_sname_)			iViewSubjectPropertyConstructMacroGeneric(_id_,_subject_,_fname_,_sname_,Set##_fname_,Get##_fname_)
#define iViewSubjectPropertyConstructMacroOP(YES,_id_,_subject_,_fname_,_sname_)	iViewSubjectPropertyConstructMacroGenericOP(YES,_id_,_subject_,_fname_,_sname_,Set##_fname_,Get##_fname_)

#endif // IVIEWSUBJECT_H

