/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iviewobject.h"


#include "idata.h"
#include "idatareader.h"
#include "ierror.h"
#include "iobjectfactory.h"
#include "ioutputchannel.h"
#include "ishell.h"
#include "iviewmodule.h"
#include "iviewsubject.h"

//
//  templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


using namespace iParameter;


//
//  Main class
//
iViewObject* iViewObject::New(iViewModule *parent, const iString &fname, const iString &sname)
{
	return new iViewObject(parent,fname,sname);
}


iViewObject::iViewObject(iViewModule *parent, const iString &fname, const iString &sname) : iObject(parent,fname,sname,1), iViewModuleComponent(parent)
{
	mRenderMode = iParameter::RenderMode::NoRender;

	//mIsVisible = false;
}


void iViewObject::AddSubject(iViewSubject *subject)
{
	IASSERT(subject);

	mSubjects.Add(subject);
}


iViewObject::~iViewObject()
{
	while(mSubjects.Size() > 0) mSubjects.RemoveLast()->Delete();
}


int iViewObject::GetNumberOfSubjects() const
{
	return mSubjects.Size();
}


bool iViewObject::IsUsingData(const iDataType &type, bool onlyprimary) const
{
	int i;
	for(i=0; i<mSubjects.Size(); i++)
	{
		if(mSubjects[i]->IsUsingData(type,onlyprimary)) return true;
	}
	return false;
}


void iViewObject::SyncWithData(const iSyncRequest &request)
{
	int i;
	for(i=0; i<mSubjects.Size(); i++) if(mSubjects[i]->IsUsingData(request.DataType(),false))
	{
		request.Sync(mSubjects[i]);
	}
}


iViewSubject* iViewObject::GetSubject(const iDataType &type) const
{
	int j;
	for(j=0; j<mSubjects.Size(); j++) if(mSubjects[j]->GetDataType() == type) return mSubjects[j];
	return 0;
}


iViewSubject* iViewObject::GetSubject(int id) const
{
	if(id>=0 && id<mSubjects.Size())
	{
		return mSubjects[id];
	}
	else
	{
		IBUG_FATAL("Invalid VO index");
		return 0;
	}
}

/*
void iViewObject::Show(bool s)
{
	int i;

	mIsVisible = s;
	for(i=0; i<mSubjects.Size(); i++) mSubjects[i]->Show(s);
}
*/

void iViewObject::Reset()
{
	int i;
	for(i=0; i<mSubjects.Size(); i++) mSubjects[i]->Reset();
}


float iViewObject::GetMemorySize() const
{
	int i;
	float s = 0.0f;

	for(i=0; i<mSubjects.Size(); i++) s += mSubjects[i]->GetMemorySize();

	return s;
}


int iViewObject::DepthPeelingRequest() const
{
	int i, req = 0;
	bool ok = true;
	for(i=0; i<mSubjects.Size(); i++)
	{
		switch(mSubjects[i]->DepthPeelingRequest())
		{
		case 1:
			{
				if(req == -1) ok = false; else req = 1;
				break;
			}
		case -1:
			{
				if(req == 1) ok = false;
				req = -1;
				break;
			}
		}
	}

	if(!ok) this->OutputText(MessageType::Warning,"Rendered objects request incompatible rendering modes.");

	return req;
}


void iViewObject::WriteHead(iString &s) const
{
	s = "Object: " + this->LongName() + "[]/" + this->ShortName() + "[]\n";
}


void iViewObject::WriteState(iString &s) const
{
	this->iObject::WriteState(s);

	int i;
	iString s1;
	for(i=0; i<mChildren.Size(); i++)
	{
		mChildren[i]->WriteState(s1);
		s += s1;
	}
}


void iViewObject::RegisterWithHelpFactory() const
{
	this->RegisterPropertiesWithHelpFactory();

	int i;
	for(i=0; i<mChildren.Size(); i++)
	{
		mChildren[i]->RegisterWithHelpFactory();
	}
}


void iViewObject::BecomeClone(iViewObject *v)
{
	int i;

	if(v!=0 && v->LongName()==this->LongName() && v->mSubjects.Size()==mSubjects.Size())
	{
		this->Reset();
		for(i=0; i<mSubjects.Size(); i++)
		{
			mSubjects[i]->BecomeClone(v->mSubjects[i]);
		}
		//this->Show(v->IsVisible());
	}
	else
	{
		for(i=0; i<mSubjects.Size(); i++)
		{
			mSubjects[i]->BecomeClone(0);
		}
	}
}


const iObject* iViewObject::FindByName(const iString &name) const
{
	if(name==this->ShortName() || name==this->LongName()) return this;

	//
	//  This is one of our children. Is there a data qualifier?
	//
	iString prefix = name.Section(".", 0, 0);
	if (prefix.BeginsWith(this->ShortName() + "[") || prefix.BeginsWith(this->LongName() + "["))
	{
		if (!prefix.EndsWith("]")) return 0; // incorrect index
		int i = prefix.Find('[');
		iString sidx = prefix.Part(i + 1, prefix.Length() - 2 - i);
		sidx.RemoveWhiteSpace();
		//
		//  Removes quotes if present
		//
		if (sidx[0] == '"' && sidx[sidx.Length() - 1] == '"')
		{
			sidx = sidx.Part(1, sidx.Length() - 2);
		}

		const iDataType &type = iDataType::FindTypeByName(sidx);
		if (!type.IsNull())
		{
			return this->GetSubject(type);
		}
		else return 0; // incorrect data type
	}
	else return 0;
}

