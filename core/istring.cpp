/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "istring.h"


#include "iexception.h"
#include "imath.h"

#include <math.h>
#include <stdio.h>


const int iString::MaxLength = iMath::_LargeInt;


//
//  A clever small trick: we can gain more in efficiency if we allocate memory in discrete chunks - 
//  this way appending small string would not require re-allocating memory with a huge speed-up.
//
#define ARRSIZE(len) (10*((len)/10+1))  // this automatically adds at least 1 byte for the terminating 0

//
//  vtkCharArray class is good, but its size is 200 bytes, and it contains many
//  members that I do not need. It is easy to write a shorter analog of that
//  class. Besides, by explicitly inlining all the functions, I gain in efficiency.
//
class iCharArray
{

public:

	inline static iCharArray* New(int s = 1) { return new iCharArray(s); }

	inline void Register()
	{ 
		mRef++; 
	}
	inline void UnRegister()
	{
		if(--mRef == 0) 
		{
			delete this;
		}
	}
	inline void SetSize(int s)
	{ 
		if(s<=0 || s==mLen || mRef>1) return; 
		delete [] mData;
		mData = new char[s]; if(mData == 0) throw iException::TotalDisaster();
		mLen = s;
	}
	inline void DeepCopy(iCharArray *arr)
	{
		if(mRef > 1) return;
		this->SetSize(arr->mLen);
		memcpy(arr->mData,mData,mLen);
	}

	inline char* GetPointer(int i) const { if(i>=0 && i<mLen) return mData+i; else return 0; }
	inline int GetSize() const { return mLen; }
	inline int GetReferenceCount() const { return mRef; }

private:

	iCharArray(int s)
	{ 
		if(s < 1) s = 1;
		mData = new char[s]; if(mData == 0) throw iException::TotalDisaster();
		mRef = 1; 
		mLen = s;
	}
	~iCharArray(){ delete [] mData; }

	char *mData;
	int mRef, mLen;
};


iString::iString(const iString &str)
{
	mArr = str.mArr;
	mArr->Register();
	mLen = str.mLen;
#ifdef I_DEBUG
	Ptr = mArr->GetPointer(0);
#endif
}


iString::iString(const char *s, int l)
{
	mArr = iCharArray::New(); if(mArr == 0) throw iException::TotalDisaster();

	if(s != 0) 
	{
		mLen = int(strlen(s)); 
		if(l>=0 && l<mLen) mLen = l;
		mArr->SetSize(ARRSIZE(mLen));
		memcpy(mArr->GetPointer(0),(const char *)s,mLen+1); 
	}
	else 
	{
		mLen = 0;
		if(l > 0) mArr->SetSize(ARRSIZE(l)); else mArr->SetSize(ARRSIZE(0));
	}
	*(mArr->GetPointer(mLen)) = 0;
#ifdef I_DEBUG
	Ptr = mArr->GetPointer(0);
#endif
}


iString::~iString()
{
	mArr->UnRegister();
}


void iString::Clear()
{
	if(mArr->GetReferenceCount() > 1)
	{
		iCharArray *tmp = iCharArray::New(); if(tmp == 0) throw iException::TotalDisaster();
		tmp->SetSize(ARRSIZE(0));
		mArr->UnRegister();
		mArr = tmp;
#ifdef I_DEBUG
		Ptr = mArr->GetPointer(0);
#endif
	}
	mLen = 0;
	*(mArr->GetPointer(mLen)) = 0;
}


void iString::Init(int size)
{
	if(size < 1) return;

	if(mArr->GetReferenceCount() > 1)
	{
		iCharArray *tmp = iCharArray::New(); if(tmp == 0) throw iException::TotalDisaster();
		mArr->UnRegister();
		mArr = tmp;
	}

	mArr->SetSize(ARRSIZE(size));
	mLen = 0;
	*(mArr->GetPointer(mLen)) = 0;
#ifdef I_DEBUG
	Ptr = mArr->GetPointer(0);
#endif
}


char* iString::GetWritePointer(int len)
{
	if(len <= 0) return 0;

	if(len<mArr->GetSize() && mArr->GetReferenceCount()==1)  
	{
		//
		//  Enough memory is already allocated, just return the pointer
		//
	}
	else
	{
		//
		//  Need to re-allocate
		//
		iCharArray *tmp;
		tmp = iCharArray::New(); if(tmp == 0) throw iException::TotalDisaster();
		tmp->SetSize(ARRSIZE(len));
		mArr->UnRegister();
		mArr = tmp;
#ifdef I_DEBUG
		Ptr = mArr->GetPointer(0);
#endif
	}
	mLen = len;
	*(mArr->GetPointer(mLen)) = 0;
	return mArr->GetPointer(0);
}


iString& iString::operator=(const iString &str)
{
	if(mArr != str.mArr)
	{
		mArr->UnRegister();
		mArr = str.mArr;
		mArr->Register();
		mLen = str.mLen;
#ifdef I_DEBUG
		Ptr = mArr->GetPointer(0);
#endif
	}
	return *this;
}


iString& iString::operator+=(const iString &str)
{
	int newlen = mLen + str.mLen;
	int newsize = ARRSIZE(newlen);

	if(newsize<=mArr->GetSize() && mArr->GetReferenceCount()==1)  
	{
		//
		//  Enough memory is already allocated, we save here
		//
		memcpy(mArr->GetPointer(mLen),str.mArr->GetPointer(0),str.mLen+1);
	}
	else
	{
		//
		//  Need to re-allocate
		//
		iCharArray *tmp;
		tmp = iCharArray::New(); if(tmp == 0) throw iException::TotalDisaster();
		tmp->SetSize(newsize);
		memcpy(tmp->GetPointer(0),mArr->GetPointer(0),mLen);
		memcpy(tmp->GetPointer(mLen),str.mArr->GetPointer(0),str.mLen+1);
		mArr->UnRegister();
		mArr = tmp;
#ifdef I_DEBUG
		Ptr = mArr->GetPointer(0);
#endif
	}

	mLen = newlen;

	return *this;
}


char iString::operator[](int i) const
{
	if(i>=0 && i<mLen) return *(mArr->GetPointer(i)); else return char(0);
}


bool iString::operator==(const iString &str) const
{
	if(mLen != str.mLen) return false;
	if(mArr == str.mArr) return true;
	return (strcmp(mArr->GetPointer(0),str.mArr->GetPointer(0)) == 0);
}


bool iString::operator<(const iString &str) const
{
	return (strcmp(mArr->GetPointer(0),str.mArr->GetPointer(0)) < 0);
}


bool iString::operator>(const iString &str) const
{
	return (strcmp(mArr->GetPointer(0),str.mArr->GetPointer(0)) > 0);
}


const char* iString::ToCharPointer(int i) const
{
	return mArr->GetPointer(i);
}


bool iString::IsAt(const iString &str, int index) const
{
	if(index>=0 && index<=mLen-str.mLen)
	{
		return (strncmp(mArr->GetPointer(index),str.mArr->GetPointer(0),str.mLen)==0);
	}
	else return false;
}


bool iString::BeginsWith(const iString &str) const
{
	return (mLen>=str.mLen) && (strncmp(mArr->GetPointer(0),str.mArr->GetPointer(0),str.mLen)==0);
}


bool iString::EndsWith(const iString &str) const
{
	return (mLen>=str.mLen) && (strcmp(mArr->GetPointer(mLen-str.mLen),str.mArr->GetPointer(0))==0);
}


int iString::Contains(char c, int index) const
{
	int n = 0;
	char *tmp;
	if(index < 0) index += mLen;
	if(index < 0) index = 0;
	tmp = mArr->GetPointer(index);
	while(tmp!=0 && *tmp!=0)
	{
		tmp = strchr(tmp,c);
		if(tmp != 0) 
		{
			tmp++;
			n++;
		}
	}
	return n;
}


int iString::Contains(const char *s, int index) const
{
	int n = 0;
	char *tmp;
	if(index < 0) index += mLen;
	if(index < 0) index = 0;
	tmp = mArr->GetPointer(index);
	while(tmp!=0 && *tmp!=0)
	{
		tmp = strstr(tmp,s);
		if(tmp != 0) 
		{
			tmp++;
			n++;
		}
	}
	return n;
}


int iString::Find(char c, int index) const
{
	if(index < 0) index += mLen;
	if(index < 0) index = 0;
	char *tmp = strchr(mArr->GetPointer(index),c);
	if(tmp != 0) return int(tmp-mArr->GetPointer(0)); else return -1;
}


int iString::FindLast(char c) const
{
	char *tmp = strrchr(mArr->GetPointer(0),c);
	if(tmp != 0) return int(tmp-mArr->GetPointer(0)); else return -1;
}


int iString::Find(const char *s, int index) const
{
	if(s==0 || s[0]==0 || index>=mLen) return -1;

	char *tmp;
	if(index < 0) index += mLen;
	if(index < 0) index = 0;
	tmp = strstr(mArr->GetPointer(index),s);
	if(tmp != 0) return int(tmp-mArr->GetPointer(0)); else return -1;
}


int iString::FindIgnoringWhiteSpace(const iString &str, int index, int *last) const
{
	if(str.IsEmpty() || index>=mLen) return -1;

	if(index < 0) index += mLen;
	if(index < 0) index = 0;

	int i, j, jstart = 0, len = str.Length();
	while(this->IsWhiteSpace(index)) index++;
	while(str.IsWhiteSpace(jstart)) jstart++;
	if(index==mLen || jstart==len) return -1; // if any one is just whitespace, return -1

	char *ptr = mArr->GetPointer(0);
	i = index;
	j = jstart;
	while(i < mLen)
	{
		if(ptr[i] == str[j])
		{
			//
			//  Partial match, advance indices
			//
			i++; j++;
			while(this->IsWhiteSpace(i)) i++;
			while(str.IsWhiteSpace(j)) j++;
			if(j == len)
			{
				//
				//  Hooray, we are done!
				//
				if(last != 0) *last = i;
				return index;
			}
		}
		else
		{
			//
			//  Mismatch, reset i and j
			//
			index = i + (j > jstart ? 0 : 1);
			while(this->IsWhiteSpace(index)) index++;
			i = index;
			j = jstart;
		}
	}
	//
	//  We did not find a match
	//
	return -1;
}


int iString::FindFromSet(const char *set, int index, int *length) const
{
	if(set==0 || set[0]==0 || index>=mLen) return -1;

	if(index < 0) index += mLen;
	if(index < 0) index = 0;
	char *tmp = mArr->GetPointer(0);
	int len;
	while(index<mLen && (len=int(strspn(tmp+index,set)))==0) index++;
	if(index < mLen)
	{
		if(length != 0) *length = len;
		return index;
	}
	else return -1;
}


void iString::Replace(int index, char c)
{
	if(index < 0) index += mLen;
	if(index>=0 && index<mLen)
	{
		if(mArr->GetReferenceCount() > 1)
		{
			//
			//  Need to re-allocate
			//
			iCharArray *tmp;
			tmp = iCharArray::New(); if(tmp == 0) throw iException::TotalDisaster();
			tmp->SetSize(ARRSIZE(mLen));
			memcpy(tmp->GetPointer(0),mArr->GetPointer(0),mLen+1);
			mArr->UnRegister();
			mArr = tmp;
#ifdef I_DEBUG
			Ptr = mArr->GetPointer(0);
#endif
		}
		*(mArr->GetPointer(index)) = c;
	}
}


void iString::Replace(int index, int length, const iString &str)
{
	if(index < 0) index += mLen;
	if(index < 0) index = 0;
	if(index+length > mLen) length = mLen - index; 

	int newlen = mLen + (str.mLen-length);
	int newsize = ARRSIZE(newlen);

	if(newsize<=mArr->GetSize() && mArr->GetReferenceCount()==1)  
	{
		//
		//  Enough memory is already allocated, we save here - but we need to use memmove
		//  rather than memcpy because the regions are overlapping
		//
		memmove(mArr->GetPointer(index+str.mLen),mArr->GetPointer(index+length),1+mLen-index-length);
		memmove(mArr->GetPointer(index),str.mArr->GetPointer(0),str.mLen);
	}
	else
	{
		//
		//  Need to re-allocate
		//
		iCharArray *tmp;
		tmp = iCharArray::New(); if(tmp == 0) throw iException::TotalDisaster();
		tmp->SetSize(newsize);
		if(index > 0) memcpy(tmp->GetPointer(0),mArr->GetPointer(0),index);
		memcpy(tmp->GetPointer(index),str.mArr->GetPointer(0),str.mLen);
		memcpy(tmp->GetPointer(index+str.mLen),mArr->GetPointer(index+length),1+mLen-index-length);
		mArr->UnRegister();
		mArr = tmp;
#ifdef I_DEBUG
		Ptr = mArr->GetPointer(0);
#endif
	}
	mLen = newlen;
}


iString iString::Section(const iString &str, int start, int end) const
{
	int nsym = -1;
	int i, ioff1, ioff2;
	char *s, *p0 = mArr->GetPointer(0), *p1 = str.mArr->GetPointer(0);

	if(start < 0) 
	{
		nsym = 1 + this->Contains(str);
		start += nsym;
	}
	if(end < 0)
	{
		if(nsym < 0) nsym = 1 + this->Contains(str);
		end += nsym;
	}
	if(start<0 || end<start || str.mLen==0) return iString();
	//
	//  Find starting section
	//
	s = p0;
	for(ioff1=i=0; i<start; i++)
	{
		s = strstr(p0+ioff1,p1);
		if(s == 0) return iString();
		ioff1 = int(s-p0+str.mLen);
	}
	ioff2 = ioff1;
	//
	//  Find ending section
	//
	for(i=0; i<=end-start && s!=0; i++)
	{
		s = strstr(p0+ioff2,p1);
		if(s != 0) ioff2 = int(s-p0+str.mLen); else ioff2 = int(mLen+str.mLen);
	}
	//
	//  Cut out the string
	//
	return this->Part(ioff1,ioff2-ioff1-str.mLen);

}


iString iString::Part(int index, int length) const
{
	if(length <= 0) return iString();
	if(index < 0) index += mLen;
	if(index < 0) index = 0;
	if(index+length > mLen) length = mLen - index; 

	return iString(mArr->GetPointer(index),length);
}


iString iString::FromNumber(int d, const char *format)
{
	const unsigned int n = 99;
	char s[n];
	sprintf(s,format,d);
#ifndef I_NO_CHECK
	if(strlen(s) > n)
	{
		throw iException::TotalDisaster();
	}
#endif
	return iString(s);
}


iString iString::FromNumber(long d, const char *format)
{
	const unsigned int n = 99;
	char s[n];
	sprintf(s,format,d);
#ifndef I_NO_CHECK
	if(strlen(s) > n)
	{
		throw iException::TotalDisaster();
	}
#endif
	return iString(s);
}


iString iString::FromNumber(size_t d, const char *format)
{
	const unsigned int n = 99;
	char s[n];
	sprintf(s,format,d);
#ifndef I_NO_CHECK
	if(strlen(s) > n)
	{
		throw iException::TotalDisaster();
	}
#endif
	return iString(s);
}

#ifndef I_NO_LONG_LONG
// 
//  Not all compilers may support long long type...
//
iString iString::FromNumber(long long d, const char *format)
{
	const unsigned int n = 99;
	char s[n];
	sprintf(s,format,d);
#ifndef I_NO_CHECK
	if(strlen(s) > n)
	{
		throw iException::TotalDisaster();
	}
#endif
	return iString(s);
}
#endif


iString iString::FromNumber(float d, const char *format)
{
	const unsigned int n = 99;
	char s[n];
	sprintf(s,format,d);
#ifndef I_NO_CHECK
	if(strlen(s) > n)
	{
		throw iException::TotalDisaster();
	}
#endif
	return iString(s);
}


iString iString::FromNumber(double d, const char *format)
{
	const unsigned int n = 99;
	char s[n];
	sprintf(s,format,d);
#ifndef I_NO_CHECK
	if(strlen(s) > n)
	{
		throw iException::TotalDisaster();
	}
#endif
	return iString(s);
}


iString iString::FromNumber(void *d)
{
	const unsigned int n = 99;
	char s[n];
	sprintf(s,"%p",d);
#ifndef I_NO_CHECK
	if(strlen(s) > n)
	{
		throw iException::TotalDisaster();
	}
#endif
	return iString(s);
}


iString iString::FromNumber(bool n)
{
	return (n ? "true" : "false");
}


int iString::ToInt(bool &ok) const
{
	int d = 0;
	char c;
	ok = (sscanf(mArr->GetPointer(0),"%d%c",&d,&c) == 1);
	return d;
}


float iString::ToFloat(bool &ok) const
{
	float d = 0.0;
	char c;
	switch(sscanf(mArr->GetPointer(0),"%g%c",&d,&c))
	{
	case 1:
		{
			//
			//  Perfect match
			//
			ok = true;
			return d;
		}
	case 2:
		{
			//
			//  Workaround for a MAC bug
			//
			ok = (c == '.');
			return d;
		}
	default:
		{
			ok = false;
			return d;
		}
	}
}


double iString::ToDouble(bool &ok) const
{
	double d = 0.0;
	char c;
	ok = (sscanf(mArr->GetPointer(0),"%lg%c",&d,&c) == 1);
	return d;
}


void* iString::ToPointer(bool &ok) const
{
	void *d = 0;
	char c;
	ok = (sscanf(mArr->GetPointer(0),"%p%c",&d,&c) == 1);
	return d;
}


iString iString::Lower() const
{
	const char d = 'a' - 'A';
	iString tmp;
	tmp.mArr->SetSize(mArr->GetSize());
	tmp.mLen = mLen;
	char *ps = mArr->GetPointer(0);
	char *pd = tmp.mArr->GetPointer(0);
	for(int i=0; i<mLen; i++)
	{
		pd[i] = (ps[i]>='A' && ps[i]<='Z') ? ps[i] + d : ps[i];
	}
	pd[mLen] = 0;
	return tmp;
}


iString iString::Upper() const
{
	const char d = 'a' - 'A';
	iString tmp;
	tmp.mArr->SetSize(mArr->GetSize());
	tmp.mLen = mLen;
	char *ps = mArr->GetPointer(0);
	char *pd = tmp.mArr->GetPointer(0);
	for(int i=0; i<mLen; i++)
	{
		pd[i] = (ps[i]>='a' && ps[i]<='z') ? ps[i] - d : ps[i];
	}
	pd[mLen] = 0;
	return tmp;
}


void iString::ReduceWhiteSpace(bool preserve_quotes)
{
	//
	//  Turn all whitespace characters (codes 9-13 and 32) into a single space (code 32) character, and
	//  strip white space at the beginning and the end.
	//  This function works in place, without re-allocating memory, for un-shared strings. Must be 
	//  better for cache coherence too. This is only possible because we only throw out characters, not add.
	//
	iCharArray *tmp = 0;
	char *pd, *ps = mArr->GetPointer(0);

	if(mArr->GetReferenceCount() > 1)
	{
		//
		//  Dereference
		//
		tmp = iCharArray::New(); if(tmp == 0) throw iException::TotalDisaster();
		tmp->SetSize(ARRSIZE(mLen));
		pd = tmp->GetPointer(0);
	}
	else
	{
		pd = mArr->GetPointer(0);
	}

	bool on = true;
	int is, id;
	bool in_quotes = false;
	bool is_nws;
	for(is=id=0; is<mLen; is++)
	{
		is_nws = (ps[is]!=32 && (ps[is]<9 || ps[is]>13));
		if(is_nws || (preserve_quotes && in_quotes))
		{
			//
			//  not a whitespace
			//
			if(ps[is]=='"' && (is==0 || ps[is-1]!='\\')) in_quotes = !in_quotes;
			pd[id++] = ps[is];
			on = false;
		}
		else
		{
			//
			//  a whitespace - keep the first one
			//
			if(!on)
			{
				pd[id++] = ' ';
				on = true;
			}
		}
	}
	//
	//  Remove the last whitespace
	//
	if(id>0 && pd[id-1]==32) id--;
	pd[id] = 0;

	mLen = id;

	if(mArr->GetReferenceCount() > 1)
	{
		mArr->UnRegister();
		mArr = tmp;
#ifdef I_DEBUG
		Ptr = mArr->GetPointer(0);
#endif
	}
}


void iString::RemoveWhiteSpace(bool preserve_quotes)
{
	//
	//  Remove all whitespace characters (codes 9-13 and 32).
	//  This function works in place, without re-allocating memory, for un-shared strings. Must be 
	//  better for cache coherence too. This is only possible because we only throw out characters, not add.
	//
	iCharArray *tmp = 0;
	char *pd, *ps = mArr->GetPointer(0);

	if(mArr->GetReferenceCount() > 1)
	{
		//
		//  Dereference
		//
		tmp = iCharArray::New(); if(tmp == 0) throw iException::TotalDisaster();
		tmp->SetSize(ARRSIZE(mLen));
		pd = tmp->GetPointer(0);
	}
	else
	{
		pd = mArr->GetPointer(0);
	}

	int is, id;
	bool in_quotes = false;
	bool is_nws;
	for(is=id=0; is<mLen; is++)
	{
		is_nws = (ps[is]!=32 && (ps[is]<9 || ps[is]>13));
		if(is_nws || (preserve_quotes && in_quotes))
		{
			//
			//  not a whitespace
			//
			if(ps[is]=='"' && (is==0 || ps[is-1]!='\\')) in_quotes = !in_quotes;
			pd[id++] = ps[is];
		}
	}
	pd[id] = 0;

	mLen = id;

	if(mArr->GetReferenceCount() > 1)
	{
		mArr->UnRegister();
		mArr = tmp;
#ifdef I_DEBUG
		Ptr = mArr->GetPointer(0);
#endif
	}
}


void iString::FindTokenMatch(const iString &tokBeg, int &indBeg, const iString &tokEnd, int &indEnd, int index, bool multiLevel) const
{
	//
	//  Find the first match
	//
	indBeg = this->Find(tokBeg,index);
	if(indBeg < 0)
	{
		indEnd = -1;
		return;
	}

	if(multiLevel)
	{
		//
		//  Count opening and closing tokens until we get a match
		//
		int i1, i2, fin, stack = 1;
		i1 = this->Find(tokBeg,indBeg+1);
		fin = i2 = this->Find(tokEnd,indBeg+1);
		while(i2!=-1 && stack>0)
		{
			if(i1!=-1 && i1<i2)
			{
				stack++;
				i1 = this->Find(tokBeg,i1+1);
			} 
			else
			{
				stack--;
				fin = i2;
				i2 = this->Find(tokEnd,i2+1);
			}
		}

		if(stack == 0) indEnd = fin; else indEnd = -1;
	}
	else
	{
		indEnd = this->Find(tokEnd,indBeg+1);
	}
}


void iString::RemoveFormatting(const iString &tokBeg, const iString &tokEnd, bool multiLevel)
{
	int iBeg, iEnd;
	do
	{
        this->FindTokenMatch(tokBeg,iBeg,tokEnd,iEnd,0,multiLevel);
		if(iEnd < 0) iEnd = mLen - 1; else iEnd += tokEnd.Length() - 1;
		if(iBeg > -1) this->Replace(iBeg,iEnd-iBeg+1,"");
	}
	while(iBeg != -1);
}


void iString::ReformatHTMLToText(int length)
{
	int i;
	char *p;

	//
	//  Replace <img> with references to images
	//
	p = mArr->GetPointer(0);
	while((i=this->Find("<center><img src=\"")) > -1)
	{
		this->Replace(i,18,"   (see image \"docs/");
		i = this->Find("><br>",i);
		if(i > -1)
		{
			p[i] = ':';
			p[i+1] = '\n';
			p[i+2] = ' ';
			p[i+3] = ' ';
			p[i+4] = ' ';
			this->Replace("</center>",")",i,true);
		}
	}

	//
	//  Replace HTML new-line commands with newlines
	//
	this->Replace("<li>","* ");
	this->Replace("</li>","\n");
	//this->Replace("<ul>","\n");
	this->Replace("<br>","\n");
	this->Replace("<p>","\n\n");

	//
	//  For pieces of code: replace <code> with ' (any better way?)
	//
	this->Replace("<code>","'");
	this->Replace("</code>","'");

	//
	//  Horizontal line
	//
	this->Replace("<hr>","----------------------------------------\n");

	//
	//  Remove other HTML formatting
	//
	this->RemoveFormatting("<",">");

	//
	//  Replace some of HTML entities with their ascii equivalents
	//
	this->Replace("&lt;","<");
	this->Replace("&gt;",">");
	this->Replace("&nbsp;"," ");

	//
	//  Insert line breaks each length spaces, but only in white spaces
	//
	if(length > 0)
	{
		int prev = 0, offset = 0;
		p = mArr->GetPointer(0);
		for(i=0; i<mLen; i++)
		{
			switch(p[i])
			{
			case '\n':
				{
					//
					//  A new line resets the counters
					//
					prev = offset = i;
					break;
				}
			case ' ':
				{
					//
					//  If this is a white space, check how far away we are from the counter
					//
					if(i > offset+length) 
					{
						p[prev] = '\n';
						offset = prev;
					}
					prev = i;
					break;
				}
			}
		}
	}
}


iString iString::Whitespace(int len)
{
	int i;
	iString str;
	char *ptr = str.GetWritePointer((len>0)?len:1);

	for(i=0; i<len; i++)
	{
		ptr[i] = ' ';
	}

	return str;
}
