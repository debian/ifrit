/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


//
//  A class that can be replicated periodically in all 3 directions.
//
#ifndef IREPLICATEDELEMENT_H
#define IREPLICATEDELEMENT_H


class iViewSubject;


class iReplicatedElement
{

	friend class iViewSubject;

public:
	
	void SetReplicationFactors(const int n[6]);
	void GetReplicationFactors(int n[6]) const;

	inline bool IsReplicated() const { return mReplicationFactors[0]>0 || mReplicationFactors[1]>0 || mReplicationFactors[2]>0 || mReplicationFactors[3]>0 || mReplicationFactors[4]>0 || mReplicationFactors[5]>0; }

	void ReplicateAs(iViewSubject *parent);

protected:
	
	iReplicatedElement(bool isData);
	virtual ~iReplicatedElement();
	
	void UpdateReplicas(bool force = false);
	virtual void UpdateReplicasHead(){}
	virtual void UpdateReplicasBody() = 0;

	const bool mIsData;
	int mReplicationFactors[6], mOldReplicationFactors[6];
	iViewSubject *mParent;
};

#endif // IREPLICATEDELEMENT_H
