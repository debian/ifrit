/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "istreamlinefilter.h"


#include "idata.h"
#include "ierror.h"
#include "imath.h"
#include "iuniformgriddata.h"
#include "iviewmodule.h"
#include "ivtk.h"

#include <vtkCellArray.h>
#include <vtkDoubleArray.h>
#include <vtkFloatArray.h>
#include <vtkImageData.h>
#include <vtkMath.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>

//
//  Templates
//
#include "ibuffer.tlh"
#include "igenericfilter.tlh"


using namespace iParameter;


iStreamLineFilter::iStreamLineFilter(iDataConsumer *consumer) : iGenericFilter<vtkStreamTracer,vtkDataSet,vtkPolyData>(consumer,2,true)
{
	mLength = 2.0;
	mVmin = 1.0e-5;
	mDir = StreamLine::Direction::BothWays;
	mSplitLines = 0;
	mQuality = 0;
	this->SetQuality(mQuality);
	mDmin = 1.0e-3;

	this->SetIntegratorTypeToRungeKutta45();
	this->SetMaximumPropagation(mLength);
	this->SetTerminalSpeed(mVmin);
	this->SetIntegrationDirectionToBoth();
}


vtkDataSet* iStreamLineFilter::SourceData()
{
	return this->GetSource();
}


void iStreamLineFilter::SetLength(float d)
{
	if(d > 0.0)
	{
		mLength = d;
		this->SetMaximumPropagation(mLength);
		this->Modified();
	}
}


void iStreamLineFilter::SetMinimumSpeed(float v)
{
	if(v > 1.0e-30)
	{
		mVmin = v;
		this->SetTerminalSpeed(mVmin);
		this->Modified();
	}
}


void iStreamLineFilter::SetDirection(int d)
{
	if(StreamLine::Direction::IsValid(d))
	{
		mDir = d;
		switch(mDir)
		{
		case StreamLine::Direction::Backward:
		case StreamLine::Direction::DownStream:
			{
				this->SetIntegrationDirectionToBackward();
				break;
			}
		case StreamLine::Direction::Forward:
		case StreamLine::Direction::UpStream:
			{
				this->SetIntegrationDirectionToForward();
				break;
			}
		case StreamLine::Direction::BothWays:
			{
				this->SetIntegrationDirectionToBoth();
				break;
			}
		}
		this->Modified();
	}
}


void iStreamLineFilter::SetSplitLines(bool s)
{
	if(s != mSplitLines)
	{
		mSplitLines = s;
		this->Modified();
	}
}


void iStreamLineFilter::SetQuality(int q)
{ 
	const float eps0 = 1.0e-3;
	const int itMax0 = 10000;

	if(mQuality>=0 && mQuality <=7)
	{ 
		mQuality = q; 
		mEps = eps0*pow(0.1,(double)mQuality);
		mItMax = itMax0*iMath::Round2Int(pow(10.0,0.25*q));
		this->Modified(); 
	} 
}


void iStreamLineFilter::ProvideOutput()
{
	vtkDataSet *input = this->InputData();
	vtkPolyData *output = this->OutputData();
	vtkDataSet *source = this->SourceData();

	if(input->IsA("vtkImageData") && source->IsA("vtkPointSet"))
	{
		this->ProvideOutputForImageData(vtkImageData::SafeDownCast(input),output,vtkPointSet::SafeDownCast(source));
	}
	else
	{
		this->ExecuteParent();
	}
}


void iStreamLineFilter::ProvideOutputForImageData(vtkImageData *input, vtkPolyData *output, vtkPointSet *source)
{
	int i,line;

	output->Initialize();

	vtkPoints *srcPoints = source->GetPoints();

	if(input==0 || srcPoints==0 || !this->DefinePointers(input)) return;	

	float *ptrNorms = 0;
	if(source->GetPointData() != 0) 
	{
		vtkFloatArray *sn = iRequiredCast<vtkFloatArray>(INFO,source->GetPointData()->GetNormals());
		ptrNorms = sn->GetPointer(0);
	}

	int nlines = srcPoints->GetNumberOfPoints();
	
	vtkPoints *poi = vtkPoints::New(VTK_DOUBLE); IERROR_CHECK_MEMORY(poi);
	vtkCellArray *lin = vtkCellArray::New(); IERROR_CHECK_MEMORY(lin);
	vtkFloatArray *sca = vtkFloatArray::New(); IERROR_CHECK_MEMORY(sca);

	input->GetDimensions(wDims);
	input->GetOrigin(wOrg);
	input->GetSpacing(wSpa);

	double cell = wSpa[0];
	if(cell > wSpa[1]) cell = wSpa[1];
	if(cell > wSpa[2]) cell = wSpa[2];
	
	this->GetPeriodicities(wPer);

	//
	//  Determine how many scalar components we need to carry with us
	//
	int nsca = wNsca + 1; // +1 is for 1/vv for tube scaling
	sca->SetNumberOfComponents(nsca);

	//
	//  Loop over all lines
	//
	int npoi;
	float v0[3], v1[3], t0[3], *vn = 0;
	double x0[3], x1[3];
	double d;
	float vv1, sdir, h;
	bool done;
	vtkIdType l, l0;

	iBuffer<float> bsca; 
	bsca.Extend(nsca);

	int idir, idirMin = 0, idirMax = -1;
	switch (mDir) 
	{
	case StreamLine::Direction::UpStream: { idirMin = 0; idirMax = 0; break; }
	case StreamLine::Direction::DownStream: { idirMin = 1; idirMax = 1; break; }
	case StreamLine::Direction::Forward:
	case StreamLine::Direction::Backward:
	case StreamLine::Direction::BothWays: { idirMin = 0; idirMax = 1; break; }
	}

	int isplit, isplitMax = 1;
	if(mSplitLines) isplitMax = 2;

	int it, ret, stopIndex;
	bool out;

	vtkIdType vertex = 0;
	for(line=0; line<nlines; line++) 
	{
		this->UpdateProgress(double(line)/nlines);
		if(this->GetAbortExecute()) break;
		
		srcPoints->GetPoint(line,x0);
		l0 = this->GetVector(x0,v0);
		if(ptrNorms != 0)
		{
			vn = ptrNorms + 3*line;
			if(mSplitLines)
			{
				vv1 = vtkMath::Dot(vn,v0)/(1.0e-30+v0[0]*v0[0]+v0[1]*v0[1]+v0[2]*v0[2]);
				for(i=0; i<3; i++) t0[i] = vn[i] - vv1*v0[i];
				vtkMath::Normalize(t0);
			}
		}

		for(idir=idirMin; idir<=idirMax; idir++)
		{
			sdir = 1.0 - 2.0*idir;
			if(ptrNorms!=0 && ((mDir==StreamLine::Direction::Forward && sdir*vtkMath::Dot(vn,v0)<0.0) || (mDir==StreamLine::Direction::Backward && sdir*vtkMath::Dot(vn,v0)>0.0)))
			{
				break;
			}

			for(isplit=0; isplit<isplitMax; isplit++)
			{			
				stopIndex = 10;
				d = 0.0;
				npoi = 0;
				done = false;
				
				//
				//  Locate the starting point
				//
				l = l0;
				for(i=0; i<3; i++)
				{
                    v1[i] = v0[i];
					x1[i] = x0[i];
				}
				 
				if(ptrNorms != 0)
				{
					if(mSplitLines)
					{
						for(i=0; i<3; i++) 
						{
							x1[i] += 0.25*cell*(2*isplit-1)*t0[i];
							if(x1[i] < -1.0) x1[i] = -1.0;
							if(x1[i] > 1.0) x1[i] = 1.0;
						}
						l = this->GetVector(x1,v1);
					}

					if((mDir==StreamLine::Direction::Forward && sdir*vtkMath::Dot(vn,v0)<0.0 || (mDir==StreamLine::Direction::Backward && sdir*vtkMath::Dot(vn,v0)>0.0)))
					{
						break;
					}
				}
				
				h = sdir*cell/(1.0e-30+sqrt(v1[0]*v1[0]+v1[1]*v1[1]+v1[2]*v1[2]));

				it = 0;
				ret = 1;

				do
				{
					if(ret >= 0)
					{
						//
						//  Save the point
						//
						l = this->GetVector(x1,v1);
						
						out = false;
						for(i=0; i<3; i++)
						{
							if(x1[i]<-1.0 && sdir*v1[i]<0.0) out = true;
							if(x1[i]>1.0 && sdir*v1[i]>0.0) out = true;
						}
						if(out) break;

						vv1 = sqrt(v1[0]*v1[0]+v1[1]*v1[1]+v1[2]*v1[2]);
						poi->InsertNextPoint(x1); npoi++;
						bsca[0] = 1.0/sqrt(1.0+vv1/mVmin);
						if(wNsca > 0) 
						{
							this->AssignScalars(bsca+1,l);
						}

						sca->InsertNextTuple(bsca);
					}
					//
					//  Compute the distance to the nearest edge
					//
					it++;

					ret = FollowLine(x1,d,h,mEps,l);
					if(ret == 0) stopIndex--;
				} 
				while(stopIndex>0 && d<mLength && it<mItMax);
				
				if(npoi > 0)
				{
					lin->InsertNextCell(npoi);
					for(i=0; i<npoi; i++) lin->InsertCellPoint(vertex+i);
					vertex += npoi;
				}
			}
		}			
	}
	
	output->SetPoints(poi);
	poi->Delete();
	output->SetLines(lin);
	lin->Delete();
	output->GetPointData()->SetScalars(sca);
	sca->Delete();

	this->Modified();
}


int iStreamLineFilter::FollowLine(double x[3], double &d, float &h, float eps, vtkIdType l)
{
	const float b21 = 2.0/9.0;
	const float b31 = 1.0/12.0;
	const float b32 = 1.0/4.0;
	const float b41 = 69.0/128.0;
	const float b42 = -243.0/128.0;
	const float b43 = 135.0/64.0;
	const float b51 = -17.0/12.0;
	const float b52 = 27.0/4.0;
	const float b53 = -54.0/10.0;
	const float b54 = 16.0/15.0;
	const float b61 = 65.0/432.0;
	const float b62 = -5.0/16.0;
	const float b63 = 13.0/16.0;
	const float b64 = 4.0/27.0;
	const float b65 = 5.0/144.0;
	const float c1 = 47.0/450.0;
	const float c2 = 0.0;
	const float c3 = 12.0/25.0;
	const float c4 = 32.0/225.0;
	const float c5 = 1.0/30.0;
	const float c6 = 6.0/25.0;
	const float d1 = 1.0/150.0;
	const float d2 = 0.0;
	const float d3 = -3.0/100.0;
	const float d4 = 16.0/75.0;
	const float d5 = 1.0/20.0;
	const float d6 = -6.0/25.0;

	int i, ret;
	float k1[3], k2[3], k3[3], k4[3], k5[3], k6[3], vtmp[3], err, errmax;
	double xtmp[3];
	float ddmin, dx;

	this->GetDmin(l,ddmin,dx);

	this->GetVector(x,vtmp);
	for(i=0; i<3; i++)
	{
		k1[i] = h*vtmp[i];
		xtmp[i] = x[i] + b21*k1[i];
	}

	this->GetVector(xtmp,vtmp);
	for(i=0; i<3; i++)
	{
		k2[i] = h*vtmp[i];
		xtmp[i] = x[i] + b31*k1[i] + b32*k2[i];
	}

	this->GetVector(xtmp,vtmp);
	for(i=0; i<3; i++)
	{
		k3[i] = h*vtmp[i];
		xtmp[i] = x[i] + b41*k1[i] + b42*k2[i] + b43*k3[i];
	}

	this->GetVector(xtmp,vtmp);
	for(i=0; i<3; i++)
	{
		k4[i] = h*vtmp[i];
		xtmp[i] = x[i] + b51*k1[i] + b52*k2[i] + b53*k3[i] + b54*k4[i];
	}

	this->GetVector(xtmp,vtmp);
	for(i=0; i<3; i++)
	{
		k5[i] = h*vtmp[i];
		xtmp[i] = x[i] + b61*k1[i] + b62*k2[i] + b63*k3[i] + b64*k4[i] + b65*k5[i];
	}

	errmax = 0.0;
	this->GetVector(xtmp,vtmp);
	for(i=0; i<3; i++)
	{
		k6[i] = h*vtmp[i];
		xtmp[i] = c1*k1[i] + c2*k2[i] + c3*k3[i] + c4*k4[i] + c5*k5[i] + c6*k6[i];
		err     = d1*k1[i] + d2*k2[i] + d3*k3[i] + d4*k4[i] + d5*k5[i] + d6*k6[i];
		err = fabs(err)/dx;
		if(err > errmax) errmax = err;
	}

	float dd = sqrt(xtmp[0]*xtmp[0]+xtmp[1]*xtmp[1]+xtmp[2]*xtmp[2]);
	errmax /= eps;

	if(errmax > 1.0)
	{
		h = 0.9*h*pow((double)errmax,-0.2);
		ret = -1;
	}
	else
	{
		d += dd;
		for(i=0; i<3; i++) x[i] += xtmp[i];
		h = 0.9*h*pow(1.0e-4+(double)errmax,-0.25);
		ret = 1;
	}

	if(dd < ddmin) 
	{
		if(ret == -1)
		{
			for(i=0; i<3; i++) x[i] += xtmp[i];
		}
		return 0;
	}
	else
	{
		return ret;
	}

}


void iStreamLineFilter::GetDmin(vtkIdType vtkNotUsed(l), float &ddmin, float &dx)
{
	ddmin = mDmin*wSpa[0];
	dx = wSpa[0];
}


bool iStreamLineFilter::DefinePointers(vtkImageData *input)
{
	if(input==0 || input->GetPointData()==0) return false; 

	vtkFloatArray *vec = vtkFloatArray::SafeDownCast(input->GetPointData()->GetVectors());
	if(vec==0 || vec->GetNumberOfComponents()!=3) return false;
	wPtrVec = (float *)vec->GetVoidPointer(0);

	wNsca = 0;
	input->GetDimensions(wDims);
	vtkIdType size = (vtkIdType)wDims[0]*wDims[1]*wDims[2];
	if(input->GetPointData()->GetScalars()!=0 && input->GetPointData()->GetScalars()->GetNumberOfTuples()==size)
	{
		wNsca = input->GetPointData()->GetScalars()->GetNumberOfComponents();
		wPtrSca = (float *)input->GetPointData()->GetScalars()->GetVoidPointer(0);
	}
	wLimits = this->GetLimits();

	return true;
}


void iStreamLineFilter::AssignScalars(float *p, vtkIdType l)
{
	int i;
	float *org = wPtrSca + wNsca*l;
	for(i=0; i<wNsca; i++) p[i] = org[i]; // # of scalars is likely to be small, no reason to do memcpy
}


vtkIdType iStreamLineFilter::GetVector(double x[3], float v[3])
{
	return iStreamLineFilter::GetVectorWithCIC(x,v,wPtrVec,wDims,wOrg,wSpa,wPer);
}


vtkIdType iStreamLineFilter::GetVectorWithCIC(double x[3], float v[3], float *ptrvec, int dims[3], double org[3], double spa[3], bool per[3])
{
	int ijk1[3], ijk2[3];
	int i;
	double w1[3], w2[3];
	float *v111, *v112, *v121, *v122, *v211, *v212, *v221, *v222;

	iUniformGridData::CIC(per,dims,org,spa,x,ijk1,ijk2,w1,w2);

	//
	//  Vectors of the CIC neigbors
	//
	v111 = ptrvec + 3*(ijk1[0]+dims[0]*(ijk1[1]+dims[1]*ijk1[2]));
	v112 = ptrvec + 3*(ijk1[0]+dims[0]*(ijk1[1]+dims[1]*ijk2[2]));
	v121 = ptrvec + 3*(ijk1[0]+dims[0]*(ijk2[1]+dims[1]*ijk1[2]));
	v122 = ptrvec + 3*(ijk1[0]+dims[0]*(ijk2[1]+dims[1]*ijk2[2]));
	v211 = ptrvec + 3*(ijk2[0]+dims[0]*(ijk1[1]+dims[1]*ijk1[2]));
	v212 = ptrvec + 3*(ijk2[0]+dims[0]*(ijk1[1]+dims[1]*ijk2[2]));
	v221 = ptrvec + 3*(ijk2[0]+dims[0]*(ijk2[1]+dims[1]*ijk1[2]));
	v222 = ptrvec + 3*(ijk2[0]+dims[0]*(ijk2[1]+dims[1]*ijk2[2]));

	for(i=0; i<3; i++)
	{
		v[i] =
			v111[i]*w1[0]*w1[1]*w1[2] +
			v211[i]*w2[0]*w1[1]*w1[2] +
			v121[i]*w1[0]*w2[1]*w1[2] +
			v221[i]*w2[0]*w2[1]*w1[2] +
			v112[i]*w1[0]*w1[1]*w2[2] +
			v212[i]*w2[0]*w1[1]*w2[2] +
			v122[i]*w1[0]*w2[1]*w2[2] +
			v222[i]*w2[0]*w2[1]*w2[2];
	}

	return ijk1[0] + dims[0]*(ijk1[1]+(vtkIdType)dims[1]*ijk1[2]);
}

