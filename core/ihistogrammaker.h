/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


//
//  Class that computes a histogram for DataSubject's data and also provides other general info about the DataSubject's data
//
#ifndef IHISTOGRAMMAKER_H
#define IHISTOGRAMMAKER_H


#include <vtkObject.h>
#include "iparallelworker.h"


#include "iarray.h"
#include "ihistogram.h"
#include "iposition.h"
#include "istretch.h"

class iDataSubject;
class iHistogram;
class iHistogramMakerWorker;
struct iPair;
class iViewModule;

class vtkDataSet;


class iHistogramMaker : public vtkObject, protected iParallelWorker
{
	
public:
	
	struct PointInfo
	{
		float Value;
		vtkIdType Cell;
		iPosition Position;
		PointInfo(iViewModule *vm);
	};

	vtkTypeMacro(iHistogramMaker,vtkObject);

	static iHistogramMaker* New(const iDataSubject *subject = 0);
	
	virtual int GetNumberOfComponents();
	bool HasOverflow(int var);

	const iPair GetRange(int var);
	void GetRange(int var, float &min, float &max);
	void GetRange(int var, float range[2]);

	const PointInfo& GetMin(int var);
	const PointInfo& GetMax(int var);

	const iHistogram& GetHistogram(int var, int sid, bool full);

protected:
	
	const iHistogram& GetHistogram(int var, int sid, float fbins); // scale number of bins by fbins factor

	iHistogramMaker(const iDataSubject *subject);
	virtual ~iHistogramMaker();

	virtual int ExecuteStep(int step, iParallel::ProcessorInfo &p);

	//
	//  Inherit these functions
	//
	virtual vtkIdType GetNumberOfCells();
	virtual void FindPositionForCell(vtkIdType cell, iPosition &pos);

private:

	bool ComputeRange(int var);
	bool ComputePoints(int var);
	bool ComputeHistogram(int var, int sid, float fbins, int step);

	struct VarItem
	{
		PointInfo Max;
		PointInfo Min;
		bool Overflow;
		int DataStep[iStretch::Count];
		iHistogram Histogram[iStretch::Count];
		vtkTimeStamp RangeMTime, PointsMTime, HistogramMTime[iStretch::Count];
		VarItem(iViewModule *vm);
	};

	iArray<VarItem*> mItems; // have to use pointers due to a non-trivial constructor

	iHistogramMakerWorker *mWorker;
	const PointInfo mNullPointInfo;

protected:

	const iDataSubject *mSubject;

	class Iterator
	{
	public:
		Iterator(const iDataSubject *subject, int var, vtkIdType beg, vtkIdType end);
		virtual bool Next(int step = 1);
		virtual float Value() const = 0;
		virtual double Weight() const = 0;
		inline vtkIdType Index() const { return Idx; }
		inline bool IsReady() const { return (this->NumFields != 0); }
	protected:
		vtkIdType Idx, End;
		const iDataSubject *Subject;
		float *DataPtr;
		int Var, NumFields;
	};

	template<unsigned int Rank> 
	class StdIterator : public Iterator
	{
	public:
		StdIterator(const iDataSubject *subject, int var, vtkIdType beg, vtkIdType end);
		virtual float Value() const;
		virtual double Weight() const;
	};

	virtual Iterator* MakeIterator(unsigned int rank, int var, vtkIdType beg, vtkIdType end) const;
};

#endif // IHISTOGRAMMAKER_H

