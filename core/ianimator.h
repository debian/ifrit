/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IANIMATOR_H
#define IANIMATOR_H


#include "iobject.h"
#include "iviewmodulecomponent.h"


#include "iarray.h"
#include "imath.h"
#include "iimage.h"

class iStereoImageArray;

class vtkImageData;
class vtkPolyData;


#define	iAnimatorPropertyMacro(_name_,_id_) \
	iType::Traits<iType::_id_>::Type Get##_name_() const { return mState._name_; } \
	bool Set##_name_(iType::Traits<iType::_id_>::Type s); \
	iPropertyScalar<iType::_id_> _name_


class iAnimator : public iObject, public iViewModuleComponent
{
	
public:
	
	vtkTypeMacro(iAnimator,iObject);
	static iAnimator* New(iViewModule *vm = 0);
		
	iAnimatorPropertyMacro(Style,Int);
	iAnimatorPropertyMacro(NumberOfFrames,Int);
	iAnimatorPropertyMacro(NumberOfBlendedFrames,Int);
	iAnimatorPropertyMacro(NumberOfTransitionFrames,Int);
	iAnimatorPropertyMacro(Phi,Float);
	iAnimatorPropertyMacro(Theta,Float);
	iAnimatorPropertyMacro(Zoom,Float);
	iAnimatorPropertyMacro(Roll,Float);
	
	iObjectPropertyMacro1S(TitlePageFile,String);
	iObjectPropertyMacro1S(LogoFile,String);

	iObjectPropertyMacro1S(NumberOfTitlePageFrames,Int);
	iObjectPropertyMacro1S(NumberOfTitlePageBlendedFrames,Int);

	iObjectPropertyMacro1S(LogoLocation,Int);
	iObjectPropertyMacro1S(LogoOpacity,Float);

	iObjectPropertyMacro1S(Debug,Int);

	iObjectPropertyMacroA(Reset);
	iObjectPropertyMacroA(Continue);

	iObjectPropertyMacro2Q(CurrentFrame,Int);
	iObjectPropertyMacro2Q(CurrentRecord,Int);

	inline const iImage& GetTitlePageImage() const { return mTitlePageImage; }
	inline const iImage& GetLogoImage() const { return mLogoImage; }

	//
	//  Animator controls
	//
	void Animate();

	void AddFollower(iAnimator *f);
	void RemoveFollower(iAnimator *f);
	void RemoveAllFollowers();

	inline void GetInfo(bool &nr, int &cr, int &cf) const { nr = mNewRec; cr = mCurRec; cf = mCurFrame; }

protected:

	virtual ~iAnimator();

private:
	
	iAnimator(iViewModule *vm, const iString &fname, const iString &sname);

	void SetLeader(iAnimator *l);

	void Start();
	void Stop();
	bool Frame(bool dumpImage = true);
	void WriteImages(iStereoImageArray &images);

	void ResetCurrentFile();
	void SaveInternalState();
	void RestoreState(bool with_camera);

	bool mStarted, mStartedRender;
	unsigned long mSeed;

	struct State 
	{
		int Style;
		int NumberOfFrames;
		int NumberOfBlendedFrames;
		int NumberOfTransitionFrames;
		float Phi;
		float Theta;
		float Zoom;
		float Roll;
		bool isBoundingBox;
		bool isTimeLabel;
		bool isColorBar;
		int currec;
		int cameraProjection;
		double cameraPosition[3];
		double cameraFocalPoint[3];
		double cameraViewUp[3];
		double cameraParallelScale;
		double cameraClippingRange[2];
	};
	State mState, mState2;

	float mRandStep;
	bool mNewRec;
	int mPrevRec, mCurRec, mTotFrame, mCurFrame;

	iArray<iStereoImageArray*> mBlenderBase;

	iImage mTitlePageImage, mLogoImage;

	iLookupArray<iAnimator*> mFollowers;
	iAnimator *mLeader;

	//
	//  Internal (work) variables
	//
	struct AnimatorData
	{
		double pos[3];
		float r, dphl0, dthl0, ramp;
		double xc1[3], xc2[3], x[3];
		float v[3], t, dt0, dt;
	} wData;
};

#endif // IANIMATOR_H

