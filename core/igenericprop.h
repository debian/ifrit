/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


//
//  A front-end for portable props - in case VTK guys decide to change the vtkProp interface again...
//
#ifndef IGENERICPROP_H
#define IGENERICPROP_H


#include "iarray.h"
#include "ivtk.h"

#include <vtkTimeStamp.h>

class vtkProp;
class vtkViewport;
class vtkWindow;


template<class Prop>
class iGenericProp : public Prop
{

public:

	virtual int RenderOverlay(vtkViewport *vp);
	virtual int RenderOpaqueGeometry(vtkViewport *vp);
	virtual int HasTranslucentPolygonalGeometry(); 
	virtual int RenderTranslucentPolygonalGeometry(vtkViewport *vp);
	virtual int RenderVolumetricGeometry(vtkViewport *vp);

	virtual void ReleaseGraphicsResources(vtkWindow *win);

protected:

	iGenericProp(bool self);

	virtual void UpdateGeometry(vtkViewport *vp) = 0;
	virtual void UpdateOverlay(vtkViewport *vp);
	virtual void Reset();

	inline bool IsEnabled() const { return mEnabled; }
	void PrependComponent(vtkProp *p);
	void AppendComponent(vtkProp *p);
	void RemoveComponent(vtkProp *p);
	void RemoveAllComponents();
	void ToggleComponent(vtkProp *p);
	void ShowComponents(int n, vtkProp **p);
	void Disable();

	vtkTimeStamp GetLastRenderMTime() const { return mLastRenderMTime; }

private:

	void MakeUpTodate(vtkViewport *vp);

	const bool mSelf;
	bool mDirty, mEnabled;
	iLookupArray<vtkProp*> mProps;

	vtkTimeStamp mLastRenderMTime;
};

#endif // IGENERICPROP_H
 
