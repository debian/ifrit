/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ireplicatedpolydata.h"


#include "ierror.h"
#include "imath.h"
#include "iviewmodule.h"
#include "iviewsubject.h"

#include <vtkCellArray.h>
#include <vtkFloatArray.h>
#include <vtkMath.h>
#include <vtkPointData.h>
#include <vtkPointLocator.h>
#include <vtkPolyData.h>

//
//  Templates
//
#include "igenericfilter.tlh"


namespace iReplicatedPolyData_Private
{
//
//  QuickFind
//
#define ARR(i)	lptr1[i]

bool Find(vtkIdType n, vtkIdType *lptr1, vtkIdType l, vtkIdType &ind)
{
	vtkIdType i1 = 0;
	vtkIdType i2 = n - 1;
	vtkIdType ic;

	if(l < ARR(i1))
	{
		ind = -1;
		return false;
	}
	
	if(l > ARR(i2))
	{
		ind = -1;
		return false;
	}

	while(i2-i1 > 1)
	{
		ic = (i1+i2)/2;
		if(l >= ARR(ic)) i1 = ic;
		if(l <= ARR(ic)) i2 = ic;
	}

	if(l == ARR(i1))
	{
		ind = i1;
		return true;
	}
	else if(l == ARR(i2))
	{
		ind = i2;
		return true;
	}
	else
	{
		ind = -1;
		return false;
	}
}
//
//  QuickSort
//
#define SAVE(CELL,i1)    { ltmp1 = lptr1[i1]; ltmp2 = lptr2[i1]; }
#define MOVE(i1,i2)      { lptr1[i1] = lptr1[i2]; lptr2[i1] = lptr2[i2]; }
#define RESTORE(i2,CELL) { lptr1[i2] = ltmp1; lptr2[i2] = ltmp2; }
#define SWAP(i1,i2)      { SAVE(1,i1); MOVE(i1,i2); RESTORE(i2,1); }

//
//  Recursive worker
//
void SortWorker(vtkIdType l, vtkIdType r, vtkIdType *lptr1, vtkIdType *lptr2)
{
	const int M = 8;
	vtkIdType i, j, v, ltmp1, ltmp2;

	if ((r-l)>M)
	{
		//
		// Use quicksort
		//
		i = (r+l)/2;
		if (ARR(l)>ARR(i)) SWAP(l,i);     // Tri-Median Method!
		if (ARR(l)>ARR(r)) SWAP(l,r);
		if (ARR(i)>ARR(r)) SWAP(i,r);
		
		j = r-1;
		SWAP(i,j);
		i = l;
		v = ARR(j);
		for(;;)
		{
			do i++; while(ARR(i) < v); // no ++i/--j in macro expansion!
			do j--; while(ARR(j) > v);
			if (j<i) break;
			SWAP(i,j);
		}
		SWAP(i,r-1);
		SortWorker(l,j,lptr1,lptr2);
		SortWorker(i+1,r,lptr1,lptr2);
	}
	else 
	{
		//
		// Array is small, use insertion sort. 
		//
		for (i=l+1; i<=r; i++)
		{
			SAVE(1,i);
			v = ARR(i);
			j = i;
			while (j>l && ARR(j-1)>v)
			{
				MOVE(j,j-1);
				j--;
			}
			RESTORE(j,1);
		}
    }
}

//
// Do our own quick sort for efficiency reason (based on a Java code by Denis Ahrens)
//
void Sort(vtkIdType n, vtkIdType *lptr1, vtkIdType *lptr2)
{
	SortWorker(0,n-1,lptr1,lptr2);
}

};


using namespace iReplicatedPolyData_Private;


iReplicatedPolyData::iReplicatedPolyData(iViewInstance *owner) : iGenericPolyDataFilter<vtkPolyDataAlgorithm>(owner,1,true), iReplicatedElement(true)
{
	this->ReplicateAs(owner->Owner());
}


void iReplicatedPolyData::UpdateReplicasBody()
{
	this->Modified();
}


void iReplicatedPolyData::ProvideOutput()
{
	vtkPolyData *input = this->InputData();
	vtkPolyData *output = this->OutputData();

	int extDown[3], extUp[3];
	int i;

	output->ShallowCopy(input);

	bool work = false;
	for(i=0; i<3; i++)
	{
		extDown[i] = -mReplicationFactors[2*i];
		extUp[i] = mReplicationFactors[2*i+1];
		if(extDown[i] != 0) work = true;
		if(extUp[i] != 0) work = true;
	}

	if(!work) return;

	vtkPolyData *tmp = vtkPolyData::New();
	if(tmp == 0) return;

	for(i=0; i<3; i++) if(extUp[i]>0 || extDown[i]<0)
	{
		tmp->ShallowCopy(output);
		this->ExtendDirection(tmp,output,i,extDown[i],extUp[i]);
	}
	tmp->Delete();
}


void iReplicatedPolyData::ExtendDirection(vtkPolyData *input, vtkPolyData *output, int dim, int extDown, int extUp)
{
	int i, j;
	if(input==0 || output==0 || dim<0 || dim>2 || extDown>0 || extUp<0) return;

	output->ShallowCopy(input);

	int next = extUp - extDown + 1;
	if(next < 2) return;

	vtkPoints *ipoi = input->GetPoints();
	if(ipoi == 0) return;
	vtkCellArray *iver = input->GetVerts();
	vtkCellArray *ilin = input->GetLines();
	vtkCellArray *ipol = input->GetPolys();
	vtkCellArray *isrp = input->GetStrips();
	vtkDataArray *isca = input->GetPointData()->GetScalars();
	vtkDataArray *inor = input->GetPointData()->GetNormals();

	float  *iptrpoiF = 0;
	double *iptrpoiD = 0;
	float *iptrsca = (isca == 0) ? 0 : (float *)isca->GetVoidPointer(0);
	float *iptrnor = (inor == 0) ? 0 : (float *)inor->GetVoidPointer(0);

	vtkIdType npoi = ipoi->GetNumberOfPoints();
	if(npoi == 0) return;
	vtkIdType nver = (iver == 0) ? 0 : iver->GetNumberOfCells();
	vtkIdType nlin = (ilin == 0) ? 0 : ilin->GetNumberOfCells();
	vtkIdType npol = (ipol == 0) ? 0 : ipol->GetNumberOfCells();
	vtkIdType nsrp = (isrp == 0) ? 0 : isrp->GetNumberOfCells();
	vtkIdType nsca = (isca == 0) ? 0 : isca->GetNumberOfComponents()*isca->GetNumberOfTuples();
	vtkIdType nnor = (inor == 0) ? 0 : inor->GetNumberOfComponents()*inor->GetNumberOfTuples();
	
	vtkPoints *opoi;
	opoi = vtkPoints::New(ipoi->GetDataType()); IERROR_CHECK_MEMORY(opoi);
	opoi->SetNumberOfPoints(next*npoi);

	float  *optrpoiF = 0;
	double *optrpoiD = 0;
	float *optrsca = 0, *optrnor = 0;
	vtkCellArray *over = 0, *olin = 0, *opol = 0, *osrp = 0;
	vtkFloatArray *osca = 0, *onor = 0;

	bool pointsAreFloat;
	switch(ipoi->GetDataType())
	{
	case VTK_FLOAT:
		{
			pointsAreFloat = true;
			iptrpoiF = (float  *)ipoi->GetVoidPointer(0);
			optrpoiF = (float  *)opoi->GetVoidPointer(0);
			break;
		}
	case VTK_DOUBLE:
		{
			pointsAreFloat = false;
			iptrpoiD = (double *)ipoi->GetVoidPointer(0);
			optrpoiD = (double *)opoi->GetVoidPointer(0);
			break;
		}
	default: 
		{
			vtkErrorMacro("Incorrect Points type");
			return;
		}
	}

	if(nver > 0) 
	{
		over = vtkCellArray::New(); IERROR_CHECK_MEMORY(over);
	}
	if(nlin > 0) 
	{
		olin = vtkCellArray::New(); IERROR_CHECK_MEMORY(olin);
	}
	if(npol > 0) 
	{
		opol = vtkCellArray::New(); IERROR_CHECK_MEMORY(opol);
	}
	if(nsrp > 0) 
	{
		osrp = vtkCellArray::New(); IERROR_CHECK_MEMORY(osrp);
	}
	if(nsca > 0) 
	{
		osca = vtkFloatArray::New(); IERROR_CHECK_MEMORY(osca);
		osca->SetNumberOfComponents(isca->GetNumberOfComponents());
		osca->SetNumberOfTuples(next*isca->GetNumberOfTuples());
		optrsca = (float *)osca->GetVoidPointer(0);
	}
	if(nnor > 0) 
	{
		onor = vtkFloatArray::New(); IERROR_CHECK_MEMORY(onor);
		onor->SetNumberOfComponents(3);
		onor->SetNumberOfTuples(next*inor->GetNumberOfTuples());
		optrnor = (float *)onor->GetVoidPointer(0);
	}

	vtkIdType npts, *pts, *pts1;

	int maxCellSize = 0, curmax;
	if(nver > 0)
	{
		curmax = iver->GetMaxCellSize();
		if(curmax > maxCellSize) maxCellSize = curmax;
	}
	if(nlin > 0)
	{
		curmax = ilin->GetMaxCellSize();
		if(curmax > maxCellSize) maxCellSize = curmax;
	}
	if(npol > 0)
	{
		curmax = ipol->GetMaxCellSize();
		if(curmax > maxCellSize) maxCellSize = curmax;
	}
	if(nsrp > 0)
	{
		curmax = isrp->GetMaxCellSize();
		if(curmax > maxCellSize) maxCellSize = curmax;
	}

	pts = new vtkIdType[maxCellSize]; IERROR_CHECK_MEMORY(pts);

	double s[3];
	s[0] = s[1] = s[2] = 0.0;
	s[dim] = 2.0;

	int off = 0;
	vtkIdType l, loff1, loff2, noff;
	for(j=extDown; j<=extUp; j++)
	{
		this->UpdateProgress(double(j-extDown)/next);
		if(this->GetAbortExecute()) break;
		
		noff = off*npoi;
		if(pointsAreFloat)
		{
			for(l=0; l<npoi; l++)
			{
				loff2 = 3*l;
				loff1 = 3*noff + loff2;
				optrpoiF[loff1+0] = iptrpoiF[loff2+0] + s[0]*j;
				optrpoiF[loff1+1] = iptrpoiF[loff2+1] + s[1]*j;
				optrpoiF[loff1+2] = iptrpoiF[loff2+2] + s[2]*j;
			}
		}
		else
		{
			for(l=0; l<npoi; l++)
			{
				loff2 = 3*l;
				loff1 = 3*noff + loff2;
				optrpoiD[loff1+0] = iptrpoiD[loff2+0] + s[0]*j;
				optrpoiD[loff1+1] = iptrpoiD[loff2+1] + s[1]*j;
				optrpoiD[loff1+2] = iptrpoiD[loff2+2] + s[2]*j;
			}
		}
		if(nver > 0) for(iver->InitTraversal(); iver->GetNextCell(npts,pts1)!=0; )
		{
			pts[0] = pts1[0] + noff;
			over->InsertNextCell(1,pts);
		}
		if(nlin > 0) for(ilin->InitTraversal(); ilin->GetNextCell(npts,pts1)!=0; )
		{
			for(i=0; i<npts && i<maxCellSize; i++) pts[i] = pts1[i] + noff;
			olin->InsertNextCell(npts,pts);
		}
		if(npol > 0) for(ipol->InitTraversal(); ipol->GetNextCell(npts,pts1)!=0; )
		{
			for(i=0; i<npts && i<maxCellSize; i++) pts[i] = pts1[i] + noff;
			opol->InsertNextCell(npts,pts);
		}
		if(nsrp > 0) for(isrp->InitTraversal(); isrp->GetNextCell(npts,pts1)!=0; )
		{
			for(i=0; i<npts && i<maxCellSize; i++) pts[i] = pts1[i] + noff;
			osrp->InsertNextCell(npts,pts);
		}
		if(nsca > 0) memcpy(optrsca+off*nsca,iptrsca,nsca*sizeof(float));
		if(nnor > 0) memcpy(optrnor+off*nnor,iptrnor,nnor*sizeof(float));
		off++;
	}
	
	delete [] pts;

	output->SetPoints(opoi);
	opoi->Delete();

	if(nver > 0)
	{
		output->SetVerts(over);
		over->Delete();
	}
	if(nlin > 0)
	{
		output->SetLines(olin);
		olin->Delete();
	}
	if(npol > 0)
	{
		output->SetPolys(opol);
		opol->Delete();
	}
	if(nsrp > 0)
	{
		output->SetStrips(osrp);
		osrp->Delete();
	}
	if(nsca > 0)
	{
		output->GetPointData()->SetScalars(osca);
		osca->Delete();
	}
	if(nnor > 0)
	{
		output->GetPointData()->SetNormals(onor);
		onor->Delete();
	}
	//
	//  Patch stitches
	//
	double *edges = new double[next-1];
	if(edges == 0) return;
	for(j=0; j<next-1; j++)
	{
		edges[j] = 1 + 2*(j+extDown);
	}
	iReplicatedPolyData::PatchStitches(output,dim,edges,next-1);
	delete [] edges;
}


float iReplicatedPolyData::GetMemorySize()
{
	if(this->IsReplicated()) return this->iGenericPolyDataFilter<vtkPolyDataAlgorithm>::GetMemorySize(); else return 0.0;
}


void iReplicatedPolyData::PatchStitches(vtkPolyData *input, int ndim, double *edges, vtkIdType nEdges)
{
	int i, j, k;
	if(input==0 || nEdges<=0 || ndim<0 || ndim>2) 
	{
		return;
	}

	vtkCellArray *cell[4];
	cell[0] = input->GetVerts();
	cell[1] = input->GetLines();
	cell[2] = input->GetPolys();
	cell[3] = input->GetStrips();
	vtkPoints *points = input->GetPoints();

	if(points == 0) return;

	float  *ptrF = 0;
	double *ptrD = 0;
	bool pointsAreFloat = false;
	double tolerance;
	switch (points->GetDataType())
	{
	case VTK_FLOAT:
		{
			pointsAreFloat = true;
			ptrF = (float *)points->GetVoidPointer(0);
			tolerance = iMath::_FloatTolerance;
			break;
		}
	case VTK_DOUBLE:
		{
			pointsAreFloat = false;
			ptrD = (double *)points->GetVoidPointer(0);
			tolerance = iMath::_DoubleTolerance;
			break;
		}
	default: tolerance = 0.0;
	}

	vtkIdList *edgeIds = vtkIdList::New(); IERROR_CHECK_MEMORY(edgeIds);
	vtkIdList *outIds = vtkIdList::New(); IERROR_CHECK_MEMORY(outIds);
	vtkIdList *repIds = vtkIdList::New(); IERROR_CHECK_MEMORY(repIds);
	vtkIdList *indIds = vtkIdList::New(); IERROR_CHECK_MEMORY(indIds);
	vtkPointLocator *loc = vtkPointLocator::New(); IERROR_CHECK_MEMORY(loc);
	vtkPoints *tmpPoints = vtkPoints::New(points->GetDataType()); IERROR_CHECK_MEMORY(tmpPoints);

	int div[3];
	for(i=0; i<3; i++)
	{
		div[i] = 100;
	}
	div[ndim] = 1;
	loc->SetTolerance(tolerance);

	vtkIdType np = points->GetNumberOfPoints();
	vtkIdType l, lid, ncell, *pcell;
	double xe, x[3], bounds[6];

	vtkIdType *outptr, *repptr, nout;
	float *nor, *norptr = 0;
	if(input->GetPointData()->GetNormals() != 0) norptr = (float *)input->GetPointData()->GetNormals()->GetVoidPointer(0);

	points->GetBounds(bounds);

	for(k=0; k<nEdges; k++)
	{
		//
		//  Select points near the edge.
		//
		edgeIds->Initialize();

		xe = edges[k];
		if(pointsAreFloat)
		{
			for(l=0; l<np; l++)
			{
				if(fabs(ptrF[ndim+3*l]-xe) < tolerance)
				{
					edgeIds->InsertNextId(l);
				}
			}
		}
		else
		{
			for(l=0; l<np; l++)
			{
				if(fabs(ptrD[ndim+3*l]-xe) < tolerance)
				{
					edgeIds->InsertNextId(l);
				}
			}
		}

		loc->Initialize();
		outIds->Initialize();
		repIds->Initialize();
		indIds->Initialize();
		tmpPoints->Initialize();
		loc->SetDivisions(div);

		bounds[2*ndim] = xe - 2*tolerance;
		bounds[2*ndim+1] = xe + 2*tolerance;

		loc->InitPointInsertion(tmpPoints,bounds);
		for(l=0; l<edgeIds->GetNumberOfIds(); l++)
		{
			points->GetPoint(edgeIds->GetId(l),x);

			if(loc->InsertUniquePoint(x,lid) == 0)
			{
				outIds->InsertNextId(edgeIds->GetId(l));
				repIds->InsertNextId(edgeIds->GetId(indIds->GetId(lid)));
			}
			else indIds->InsertNextId(l);
		}

		//
		//  We need to sort outIds and repIds arrays to make searching efficient
		//
		nout = outIds->GetNumberOfIds();
		bool *norFixed = new bool[nout];
		if(norFixed!=0 && nout>0)
		{
			outptr = outIds->GetPointer(0);
			repptr = repIds->GetPointer(0);
			Sort(nout,outptr,repptr);

			memset(norFixed,0,nout);
			//
			//  Replace outIds with repIds and fix normals on the way. make sure not to
			//  do it more than once.
			//
			for(i=0; i<4; i++) if(cell[i] != 0)
			{
				cell[i]->InitTraversal();
				while(cell[i]->GetNextCell(ncell,pcell) > 0)
				{
					for(l=0; l<ncell; l++)
					{
						if(Find(nout,outptr,pcell[l],lid))
						{
							if(norptr!=0 && !norFixed[lid])
							{
								nor = norptr + 3*repptr[lid];
								for(j=0; j<3; j++)
								{
									nor[j] = 0.5*(nor[j]+norptr[j+3*pcell[l]]);
								}
								vtkMath::Normalize(nor);
								norFixed[lid] = true;
							}
							pcell[l] = repptr[lid];
						}
					}
				}
			}
			delete [] norFixed;
		}
	}

	loc->Delete();
	edgeIds->Delete();
	tmpPoints->Delete();
	outIds->Delete();
	repIds->Delete();
	indIds->Delete();
}

