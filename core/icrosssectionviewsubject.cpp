/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "icrosssectionviewsubject.h"


#include "iactor.h"
#include "icamera.h"
#include "icolorbar.h"
#include "icommondatadistributors.h"
#include "icoredatasubjects.h"
#include "icrosssectionpipeline.h"
#include "idatalimits.h"
#include "ierror.h"
#include "imaterial.h"
#include "ihistogrammaker.h"
#include "ilookuptable.h"
#include "imarker.h"
#include "iorthoslicer.h"
#include "ipicker.h"
#include "irendertool.h"
#include "iuniformgriddata.h"
#include "iviewmodule.h"
#include "iviewobject.h"
#include "iviewsubjectparallelpipeline.h"

#include <vtkImageData.h>
#include <vtkProperty.h>
#include <vtkTexture.h>

//
//  Templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


using namespace iParameter;


//
// iCrossSectionViewSubject class
//
iCrossSectionViewInstance::iCrossSectionViewInstance(iCrossSectionViewSubject *owner) : iMultiViewInstance(owner,1,1,ColorBarPriority::CrossSection,true), mLocation(owner->GetViewModule())
{
	mMethod = 1;
	mDirection = 2;
	mSampleRate = 1;
	mInterpolateData = true;

	mLocation[0] = -1.0;
	mLocation[1] = -1.0;
	mLocation[2] = -1.0;

	mOverTheEdgeFlag = false;

	//
	//  Do VTK stuff
	//	
	mTexture = vtkTexture::New(); IERROR_CHECK_MEMORY(mTexture);
	mTextureData = vtkImageData::New(); IERROR_CHECK_MEMORY(mTextureData);

	mActors[0]->SetScalarVisibility(true);
	mActors[0]->ColorByArrayComponent(0,0);
	mActors[0]->GetProperty()->SetColor(1.0,1.0,1.0);
	mActors[0]->GetProperty()->SetOpacity(1.0);

	mTexture->SetLookupTable(mActors[0]->GetLookupTable());
	mTexture->SetQualityTo32Bit();

	//mTextureData->SetScalarTypeToFloat();
	mTexture->SetInputData(mTextureData);
}


iCrossSectionViewInstance::~iCrossSectionViewInstance()
{
	mTexture->Delete();
	mTextureData->Delete();
}


void iCrossSectionViewInstance::ConfigureBody()
{
	//
	//  Create pipeline (must be created after the object is fully created)
	//	
	this->AddMainPipeline(1);
	mActors[0]->SetInputConnection(this->Pipeline()->GetOutputPort(0));
}


void iCrossSectionViewInstance::FinishInitialization()
{
	this->SetMethod(mMethod);
	this->SetInterpolateData(mInterpolateData);
}


bool iCrossSectionViewInstance::SetMethod(int m)
{
	switch(m) 
	{
	case 0:
		{
			mActors[0]->SetTexture(0);
			break;
		}
	case 1: 
		{
			mActors[0]->SetTexture(mTexture); 
			this->UpdateTextureSize();
			break;
		}
	default: return false;
	}
	mMethod = m;
	this->Pipeline()->UpdateContents(iCrossSectionPipeline::KeyMethod);
	return true;
}


int iCrossSectionViewInstance::GetActualMethod() const
{
	if(iRequiredCast<iCrossSectionViewSubject>(INFO,this->Owner())->mForcePolygonalMethod) return 0; else return mMethod;
}


bool iCrossSectionViewInstance::SetDirection(int d)
{
	if(d>=0 && d<=2)
	{
		mDirection = d;
		this->Pipeline()->UpdateContents(iCrossSectionPipeline::KeyDirection);
		if(mMethod == 1) this->UpdateTextureSize();
		this->Owner()->UpdateAutomaticShading();
		return true;
	}
	else return false;
}


void iCrossSectionViewInstance::UpdatePainting()
{
	this->iMultiViewInstance::UpdatePainting();

	this->Pipeline()->UpdateContents(iCrossSectionPipeline::KeyPainting);
}


bool iCrossSectionViewInstance::SetBoxLocation(double p)
{
	mLocation.SetBoxValue(mDirection,p);
	this->UpdateLocation();
	return true;
}


double iCrossSectionViewInstance::GetBoxLocation() const
{
	return mLocation.BoxValue(mDirection);
}


bool iCrossSectionViewInstance::SetInterpolateData(bool s)
{
	mInterpolateData = s;
	if(mInterpolateData) 
	{
		mTexture->InterpolateOn(); 
		mActors[0]->GetProperty()->SetInterpolationToGouraud();
	}
	else 
	{
		mTexture->InterpolateOff(); 
		mActors[0]->GetProperty()->SetInterpolationToFlat();
	}
	this->Pipeline()->UpdateContents(iCrossSectionPipeline::KeyInterpolateData);
	return true;
}


bool iCrossSectionViewInstance::SetSampleRate(int p)
{
	mSampleRate = p;
	this->Pipeline()->UpdateContents(iCrossSectionPipeline::KeySampleRate);
	return true;
}


void iCrossSectionViewInstance::SetLocation(const iCoordinate &p)
{
	this->SetLocation(p.OpenGLValue());
}


void iCrossSectionViewInstance::SetLocation(double p)
{
	mLocation[mDirection] = p;
	this->UpdateLocation();
}


void iCrossSectionViewInstance::UpdateLocation()
{
	double minPos, maxPos;

	if(this->GetSubject()->GetData() != 0)
	{
		double bounds[6];
		this->GetSubject()->GetData()->GetBounds(bounds);
		minPos = bounds[2*mDirection+0];
		maxPos = bounds[2*mDirection+1];
	}
	else
	{
		minPos = -1.0;
		maxPos =  1.0;
	}
	
	if(mLocation[mDirection] > maxPos)
	{
		mLocation[mDirection] = maxPos;
		mOverTheEdgeFlag = true;
	}
	else if(mLocation[mDirection] < minPos)
	{
		mLocation[mDirection] = minPos;
		mOverTheEdgeFlag = true;
	}
	else
	{
		mOverTheEdgeFlag = false;
	}

	this->Pipeline()->UpdateContents(iCrossSectionPipeline::KeyLocation);
}


void iCrossSectionViewInstance::ShowBody(bool show)
{
	if(show)
	{
		this->UpdateTextureSize();
		mActors[0]->VisibilityOn();
	} 
	else 
	{
		mActors[0]->VisibilityOff();
	}
}


bool iCrossSectionViewInstance::SetToSpecialLocation(int n)
{
	switch(n)
	{
	case SpecialLocation::MaxData:
	case SpecialLocation::MinData:
		{
			if(this->IsThereData())
			{
				if(n == SpecialLocation::MinData)
				{
					mLocation = this->GetSubject()->GetHistogramMaker()->GetMin(mPaintVar).Position;
				}
				else
				{
					mLocation = this->GetSubject()->GetHistogramMaker()->GetMax(mPaintVar).Position;
				}
				this->UpdateLocation();
				this->GetViewModule()->GetRenderTool()->GetCamera()->ShiftTo(mLocation);
			}
			return true;
		}
	default:
		{
			mLocation.SetToSpecialLocation(n);
			this->UpdateLocation();
			this->GetViewModule()->GetRenderTool()->GetCamera()->ShiftTo(mLocation);
			return true;
		}
	}
}


bool iCrossSectionViewInstance::SyncWithDataBody()
{
	this->Pipeline()->SetInputData(this->GetSubject()->GetData());
	if(mMethod == 1) this->UpdateTextureSize();
	return true;
}


bool iCrossSectionViewInstance::CanBeShown() const
{
	return this->IsPainted();
}


void iCrossSectionViewInstance::UpdateTextureSize()
{
	int dims[3], dimsOut[3], oldDimsOut[3];
	int u, v;

	iUniformGridData *data = iUniformGridData::SafeDownCast(this->GetSubject()->GetData());
	if(data == 0) return;

	data->GetNumCells(dims);
	iOrthoSlicer::GetUV(mDirection,u,v);

	dims[u] /= mSampleRate;
	dims[v] /= mSampleRate;

	//
	//  Texture dimensions - make them a power of 2 for OpenGL
	//
    int xs = 1;
    int ys = 1;
    while(xs < dims[u]) xs = xs << 1;
    while(ys < dims[v]) ys = ys << 1;

	//
	//  if the BC are not periodic, expand the texture to allow for padding
	//
	if(!this->GetSubject()->IsDirectionPeriodic(u) && xs<dims[u]+2) xs = xs << 1;
    if(!this->GetSubject()->IsDirectionPeriodic(v) && ys<dims[v]+2) ys = ys << 1;

	dimsOut[0] = xs;
	dimsOut[1] = ys;
	dimsOut[2] = 1;

	mTextureData->GetDimensions(oldDimsOut);
	if(oldDimsOut[0]!=dimsOut[0] || oldDimsOut[1]!=dimsOut[1] || oldDimsOut[2]!=dimsOut[2])
	{
		mTextureData->Initialize();
		mTextureData->SetDimensions(dimsOut);
#ifdef IVTK_5
		mTextureData->SetScalarType(VTK_FLOAT);
		mTextureData->SetNumberOfScalarComponents(1);
		mTextureData->AllocateScalars();
#else
		mTextureData->AllocateScalars(VTK_FLOAT,1);
#endif
	}
}


float iCrossSectionViewInstance::GetMemorySize()
{
	return this->iDataConsumer::GetMemorySize() + mTextureData->GetActualMemorySize();
}


void iCrossSectionViewInstance::RemoveInternalData()
{
	this->iDataConsumer::RemoveInternalData();
	mTextureData->Initialize();
}


iViewSubjectPipeline* iCrossSectionViewInstance::CreatePipeline(int)
{
	return new iCrossSectionPipeline(this);
}


void iCrossSectionViewInstance::ConfigureMainPipeline(iViewSubjectPipeline *p, int)
{
	iViewSubjectParallelPipeline *pp = iRequiredCast<iViewSubjectParallelPipeline>(INFO,p);

	iImageDataDistributor *idd = new iImageDataDistributor(pp->GetDataManager());
	pp->GetDataManager()->AddDistributor(idd);
	iPolyDataCollector *pdc = new iPolyDataCollector(pp->GetDataManager(),idd);
	pdc->FixStiches(false); //  cannot fix stiches in flat mode
	pp->GetDataManager()->AddCollector(pdc);
}


//
// Main class
//
bool iCrossSectionViewSubject::mForcePolygonalMethod = false;


iCrossSectionViewSubject::iCrossSectionViewSubject(iViewObject *parent, const iDataType &type) : iMultiViewSubject(parent,parent->LongName(),parent->ShortName(),parent->GetViewModule(),type,ViewObject::Flag::HasNoColor|ViewObject::Flag::HasNoOpacity|ViewObject::Flag::IsNonReplicating,1),
	iViewSubjectPropertyConstructMacro(Int,iCrossSectionViewInstance,Method,m),
	iViewSubjectPropertyConstructMacro(Int,iCrossSectionViewInstance,Direction,d),
	iViewSubjectPropertyConstructMacro(Int,iCrossSectionViewInstance,SampleRate,sr),
	iViewSubjectPropertyConstructMacro(Bool,iCrossSectionViewInstance,InterpolateData,id),
	iViewSubjectPropertyConstructMacroGeneric(Double,iCrossSectionViewInstance,Location,l,SetBoxLocation,GetBoxLocation)
{
}


iCrossSectionViewSubject::~iCrossSectionViewSubject()
{
}


iCrossSectionViewInstance* iCrossSectionViewSubject::GetInstance(int i) const
{
	if(i>=0 && i<mInstances.Size())
	{
		return iRequiredCast<iCrossSectionViewInstance>(INFO,mInstances[i]);
	}
	else return 0;
}


void iCrossSectionViewSubject::ForcePolygonalMethod(bool s)
{
	mForcePolygonalMethod = s;
}


void iCrossSectionViewSubject::UpdateAutomaticShadingBody()
{
	int i;
	for(i=0; i<mInstances.Size()-1; i++)
	{
		iCrossSectionViewInstance *ins = iRequiredCast<iCrossSectionViewInstance>(INFO,mInstances[i]);
		int var = ins->GetPaintVar();
		int dir = ins->GetDirection();

		int j;
		for(j=i+1; j<mInstances.Size(); j++)
		{
			ins = iRequiredCast<iCrossSectionViewInstance>(INFO,mInstances[j]);
			if(ins->GetPaintVar()==var && ins->GetDirection()!=dir)
			{
				this->GetMaterial()->SetShading(true);
				return;
			}
		}
	}
	this->GetMaterial()->SetShading(false);
}


