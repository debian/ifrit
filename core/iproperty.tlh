//LICENSE A


#include "iproperty.h"


#include "iarray.h"
#include "ierror.h"
#include "iobject.h"


//
//  Variable property that caches its data
//
template<iType::TypeId id>
iPropertyDataVariable<id>::iPropertyDataVariable(iObject *owner, const iString &fname, const iString &sname, int rank) : iPropertyVariable(owner,fname,sname,rank)
{
}

template<iType::TypeId id>
iPropertyDataVariable<id>::~iPropertyDataVariable()
{
}


template<iType::TypeId id>
iType::TypeId iPropertyDataVariable<id>::GetType() const
{
	return id;
}


template<iType::TypeId id>
const iString& iPropertyDataVariable<id>::GetTypeName() const
{
	return iType::Traits<id>::TypeName();
}


template<iType::TypeId id>
iString iPropertyDataVariable<id>::ConvertToString(bool exact) const
{
	int i;
	iString str;
	for(i=0; i<this->Size(); i++)
	{
		if(i > 0) str += ",";
		str += iType::Traits<id>::ValueToString(this->GetValue(i),exact);
	}
	return str;
}


template<iType::TypeId id>
void iPropertyDataVariable<id>::SaveStateToString(iString &str, bool exact) const
{
	str = this->Owner()->GetFullName() + "." + this->LongName() + " = " + this->ConvertToString(exact);
}


template<iType::TypeId id>
bool iPropertyDataVariable<id>::LoadStateFromString(const iString &str)
{
	iString head = this->Owner()->GetFullName() + "." + this->LongName() + " = ";
	int i = str.Find(head);
	if(i == -1) return false;

	iString s = str.Part(i+head.Length()).Section("\n",0,0);

	int n = s.IsEmpty() ? 0 : (s.Contains(',')+1);
	if(n != this->Size())
	{
		if(!this->Resize(n,true)) return false;
	}

	typename iType::Traits<id>::ContainerType val;
	for(i=0; i<this->Size(); i++)
	{
		if(!iType::Traits<id>::StringToValue(s.Section(",",i,i),val)) return false;
		if(!this->SetValue(i,val)) return false;
	}

	return true;
}


template<iType::TypeId id>
bool iPropertyDataVariable<id>::Copy(const iPropertyVariable *p)
{
	const iPropertyDataVariable<id> *other = dynamic_cast<const iPropertyDataVariable<id>*>(p);
	if(other != 0)
	{
		if(this->Size() != other->Size()) return false;

		int i, n = this->Size();
		for(i=0; i<n; i++)
		{
			if(!this->SetValue(i,other->GetValue(i))) return false;
		}

		return true;
	}
	else return false;
}


template<iType::TypeId id>
bool iPropertyDataVariable<id>::CopyElement(int iy, int ix)
{
	if(ix == iy) return true;

	if(iy>=0 && iy<this->Size() && ix>=0 && ix<this->Size())
	{
		return this->SetValue(iy,this->GetValue(ix));
	}
	else return false;
}


template<iType::TypeId id>
bool iPropertyDataVariable<id>::_PostValue(int i, ArgT v) const
{
	if(this->IsImmediatePost())
	{
		if(this->SetValue(i,v))
		{
			this->RequestRender();
			return true;
		}
		else return false;
	}
	else
	{
		//
		//  Message queue
		//
		QueueItem qi;
		qi.Index = i;
		qi.Value = v;
		this->mQueue.Add(qi);
		return this->_RequestPush();
	}
}


template<iType::TypeId id>
bool iPropertyDataVariable<id>::PushPostedValue_() const
{
	bool ret = this->SetValue(this->mQueue[0].Index,this->mQueue[0].Value);
	this->mQueue.Remove(0);

	if(ret)
	{
		this->RequestRender();
		return true;
	}
	else return false;
}


template<iType::TypeId id>
bool iPropertyDataVariable<id>::HasPostedRequests() const
{
	return (this->mQueue.Size() > 0);
}


//
//  Scalar (1D) variable
//
template<iType::TypeId id>
iPropertyScalar<id>::iPropertyScalar(iObject *owner, SetterType setter, GetterType getter, const iString &fname, const iString &sname, int rank) : iPropertyDataVariable<id>(owner,fname,sname,rank)
{
	this->mSetter = setter; IASSERT(setter);
	this->mGetter = getter; IASSERT(getter);
}


template<iType::TypeId id>
typename iPropertyScalar<id>::ArgT iPropertyScalar<id>::GetValue(int) const
{
	return (this->Owner()->*this->mGetter)();
}


template<iType::TypeId id>
bool iPropertyScalar<id>::SetValue(int, ArgT v) const
{
	return (this->Owner()->*this->mSetter)(v);
}


//
//  Fixed-size vector variable
//
template<iType::TypeId id>
iPropertyVector<id>::iPropertyVector(iObject *owner, SetterType setter, GetterType getter, const iString &fname, const iString &sname, int dim, int rank) : iPropertyDataVariable<id>(owner,fname,sname,rank)
{
	if(dim < 1) dim = 1;

	this->mDim = dim;
	this->mSize = 0;
	this->mResize = 0;
	this->mSetter = setter; IASSERT(setter);
	this->mGetter = getter; IASSERT(getter);
}


template<iType::TypeId id>
iPropertyVector<id>::iPropertyVector(iObject *owner, SetterType setter, GetterType getter, const iString &fname, const iString &sname, SizeType size, ResizeType resize, int rank) : iPropertyDataVariable<id>(owner,fname,sname,rank)
{
	this->mDim = 0;
	this->mSize = size;
	this->mResize = resize;
	this->mSetter = setter; IASSERT(setter);
	this->mGetter = getter; IASSERT(getter);
}


template<iType::TypeId id>
typename iPropertyVector<id>::ArgT iPropertyVector<id>::GetValue(int i) const
{
	return (this->Owner()->*mGetter)(i);
}


template<iType::TypeId id>
bool iPropertyVector<id>::SetValue(int i, ArgT v) const
{
	if(i>=this->Size() && (this->mResize==0 || !(this->Owner()->*mResize)(i+1))) return false;
	return (this->Owner()->*mSetter)(i,v);
}


template<iType::TypeId id>
bool iPropertyVector<id>::Resize(int num, bool force)
{
	if(num>=0 && this->mResize!=0 && (this->Owner()->*mResize)(num))
	{
		return true;
	}
	else return false;
}


//
//  Function property (not a variable)
//
template<iType::TypeId id>
iPropertyTypeFunction<id>::iPropertyTypeFunction(iObject *owner, const iString &fname, const iString &sname, int rank) : iPropertyFunction(owner,fname,sname,rank)
{
}


template<iType::TypeId id>
iPropertyTypeFunction<id>::~iPropertyTypeFunction()
{
}


template<iType::TypeId id>
iType::TypeId iPropertyTypeFunction<id>::GetReturnType() const
{
	return id;
}


template<iType::TypeId id>
const iString& iPropertyTypeFunction<id>::GetReturnTypeName() const
{
	return iType::Traits<id>::TypeName();
}


//
//  An action with 1 argument
//
template<iType::TypeId id>
iPropertyAction1<id>::iPropertyAction1(iObject *owner, CallerType caller, const iString &fname, const iString &sname, int rank) : iPropertyTypeFunction<iType::Bool>(owner,fname,sname,rank)
{
	this->mCaller = caller; IASSERT(caller);
}


template<iType::TypeId id>
iType::TypeId iPropertyAction1<id>::GetArgumentType(int i) const
{
	if(i == 0)
	{
		return id;
	}
	else return iType::None;
}


template<iType::TypeId id>
const iString& iPropertyAction1<id>::GetArgumentTypeName(int i) const
{
	static iString none;

	if(i == 0)
	{
		return iType::Traits<id>::TypeName();
	}
	else return none;
}


template<iType::TypeId id>
bool iPropertyAction1<id>::CallAction(ArgT v) const
{
	return (this->Owner()->*mCaller)(v);
}


template<iType::TypeId id>
bool iPropertyAction1<id>::_PostCall(ArgT v) const
{
	if(this->IsImmediatePost())
	{
		if(this->CallAction(v))
		{
			this->RequestRender();
			return true;
		}
		else return false;
	}
	else
	{
		//
		//  Message queue
		//
		QueueItem qi;
		qi.Value = v;
		this->mQueue.Add(qi);
		return this->_RequestPush();
	}
}


template<iType::TypeId id>
bool iPropertyAction1<id>::PushPostedValue_() const
{
	bool ret = this->CallAction(this->mQueue[0].Value);
	this->mQueue.Remove(0);

	if(ret)
	{
		this->RequestRender();
		return true;
	}
	else return false;
}


template<iType::TypeId id>
bool iPropertyAction1<id>::HasPostedRequests() const
{
	return (this->mQueue.Size() > 0);
}


//
//  An action with 2 arguments
//
template<iType::TypeId id1, iType::TypeId id2>
iPropertyAction2<id1,id2>::iPropertyAction2(iObject *owner, CallerType caller, const iString &fname, const iString &sname, int rank) : iPropertyTypeFunction<iType::Bool>(owner,fname,sname,rank)
{
	this->mCaller = caller; IASSERT(caller);
}


template<iType::TypeId id1, iType::TypeId id2>
iType::TypeId iPropertyAction2<id1,id2>::GetArgumentType(int i) const
{
	switch(i)
	{
	case 0:
		{
			return id1;
		}
	case 1:
		{
			return id2;
		}
	default:
		{
			return iType::None;
		}
	}
}


template<iType::TypeId id1, iType::TypeId id2>
const iString& iPropertyAction2<id1,id2>::GetArgumentTypeName(int i) const
{
	static iString none;

	switch(i)
	{
	case 0:
		{
			return iType::Traits<id1>::TypeName();
		}
	case 1:
		{
			return iType::Traits<id2>::TypeName();
		}
	default:
		{
			return none;
		}
	}
}


template<iType::TypeId id1, iType::TypeId id2>
bool iPropertyAction2<id1,id2>::CallAction(ArgT1 v1, ArgT2 v2) const
{
	return (this->Owner()->*mCaller)(v1,v2);
}


template<iType::TypeId id1, iType::TypeId id2>
bool iPropertyAction2<id1,id2>::_PostCall(ArgT1 v1, ArgT2 v2) const
{
	if(this->IsImmediatePost())
	{
		if(this->CallAction(v1,v2))
		{
			this->RequestRender();
			return true;
		}
		else return false;
	}
	else
	{
		//
		//  Message queue
		//
		QueueItem qi;
		qi.Value1 = v1;
		qi.Value2 = v2;
		this->mQueue.Add(qi);
		return this->_RequestPush();
	}
}


template<iType::TypeId id1, iType::TypeId id2>
bool iPropertyAction2<id1,id2>::PushPostedValue_() const
{
	bool ret = this->CallAction(this->mQueue[0].Value1,this->mQueue[0].Value2);
	this->mQueue.Remove(0);

	if(ret)
	{
		this->RequestRender();
		return true;
	}
	else return false;
}


template<iType::TypeId id1, iType::TypeId id2>
bool iPropertyAction2<id1,id2>::HasPostedRequests() const
{
	return (this->mQueue.Size() > 0);
}


//
//  A simple query with no arguments
//
template<iType::TypeId id>
iPropertyQuery<id>::iPropertyQuery(iObject *owner, GetterType getter, const iString &fname, const iString &sname, int rank) : iPropertyTypeFunction<id>(owner,fname,sname,rank)
{
	this->mGetter = getter; IASSERT(getter);
}


template<iType::TypeId id>
typename iPropertyQuery<id>::RetT iPropertyQuery<id>::GetQuery() const
{
	return (this->Owner()->*this->mGetter)();
}

