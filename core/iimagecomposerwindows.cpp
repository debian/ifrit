/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iimagecomposerwindows.h"


#include "ierror.h"
#include "iimagecomposer.h"
#include "imath.h"
#include "imonitor.h"
#include "ishell.h"
#include "istereoimagearray.h"
#include "iviewmodule.h"

#include <vtkCamera.h>
#include <vtkRenderer.h>

//
//  Templates
//
#include "iarray.tlh"


iColor iImageComposerBackgroundWindow::mEmptyBackground = iColor::White();
int iImageComposerForegroundWindow::mPosQuantum = 10;


namespace iImageComposerWindow_Private
{
	void DrawPixel(int x, int y, float f, const iStereoImage &image, const iColor &color, bool swap = false)
	{
		if(x<0 || y<0) return;

		if(swap)
		{
			int tmp = x; x = y; y = tmp;
		}

		if(x>=image.Width() || y>=image.Height()) return;

		unsigned char rgb[4], *ptr;
		color.GetRGB(rgb);
		rgb[3] = 255;
		int eye, numEyes = 1;
		if(image.IsStereo()) numEyes = 2;

		int k, d = image.Depth(), w = image.Width();
		for(eye=0; eye<numEyes; eye++)
		{
			ptr = image.Eye(eye).DataPointer() + d*(x+w*y);
			for(k=0; k<d; k++)
			{
				ptr[k] = (unsigned char)iMath::Round2Int(f*rgb[k]+(1.0f-f)*ptr[k]);
			}
		}
	}

	//
	//  The code below is based on a psedo-code from wikipedia.
	//
	float fpart(float x){ return x - floor(x); }

	void DrawLine(int x1, int y1, int x2, int y2, const iStereoImage &image, const iColor &color, float f0 = 1.0f)
	{
		if(x1==x2 && y1==y2)
		{
			DrawPixel(x1,y1,f0,image,color);
			return;
		}

		int tmp;
		bool swap = (abs(y2-y1) > abs(x2-x1));

        if(swap)
		{
			tmp = x1; x1 = y1; y1 = tmp;
			tmp = x2; x2 = y2; y2 = tmp;
		}
 
		if(x2 < x1)
		{
			tmp = x1; x1 = x2; x2 = tmp;
			tmp = y1; y1 = y2; y2 = tmp;
		}

		float dx = x2 - x1;
		float dy = y2 - y1;
		float gradient = dy/dx; // dx cannot be zero

		//
		// main loop
		//
		int x;
		float f, y = y1 + 1.0e-3; // first y-intersection for the main loop
		for(x=x1; x<=x2; x++)
		{
			f = fpart(y);
			DrawPixel(x,int(y),f0*(1.0f-f),image,color,swap);
			DrawPixel(x,int(y)+1,f0*f,image,color,swap);
			y += gradient;
		}
	}

	void DrawWideLine(int x1, int y1, int x2, int y2, int w, const iStereoImage &image, const iColor &color, int corner = -1)
	{
		int i, j;

		if(w < 1)
		{
			return;
		}

		if(x1==x2 && y1==y2)
		{
			for(j=0; j<w; j++)
			{
				for(i=0; i<w; i++)
				{
					DrawPixel(x1+i,y1+j,1.0f,image,color);
				}
			}
			return;
		}
	
		int tmp;
		bool swap = (abs(y2-y1) > abs(x2-x1));

        if(swap)
		{
			tmp = x1; x1 = y1; y1 = tmp;
			tmp = x2; x2 = y2; y2 = tmp;
			if(corner != -1) corner = 2*(corner & 1) + (corner & 2)/2;
		}
 
		if(x2 < x1)
		{
			tmp = x1; x1 = x2; x2 = tmp;
			tmp = y1; y1 = y2; y2 = tmp;
			if(corner != -1) corner = (corner & 2) + 1 - (corner & 1);
		}

		float dx = x2 - x1;
		float dy = y2 - y1;
		float gradient = dy/dx; // dx cannot be zero
		float normal[2];
		normal[1] = 1.0/sqrt(1.0+gradient*gradient);
		normal[0] = -gradient*normal[1];

		int ymin, ymax;
		if(y1 < y2)
		{
			ymin = y1;
			ymax = y2 + w - 1;
		}
		else if(y1 > y2)
		{
			ymin = y2;
			ymax = y1 + w - 1;
		}
		else
		{
			ymin = y1;
			ymax = y1 + w - 1;
		}

		//
		// main loop
		//
		const int subsample = 4;
		const float scale = 1.0f/subsample, offset = 0.5f*(subsample-1), w2 = 0.5*w;

		float r1, r2;
		switch(w)
		{
		case 1:
			{
				r1 = r2 = w2;
				break;
			}
		case 2:
			{
				r1 = w2 - 0.75;
				r2 = w2 + 0.75;
				break;
			}
		default:
			{
				r1 = w2 - 0.75;
				r2 = w2 + 0.75;
				break;
			}
		}

		int x0, y0, k, kmax = w + 2;
		float p[2], f, r;
		float x = x1 + 0.5*(w-1), y = y1 + 0.5*(w-1), xmax = x2 + 0.5*(w-1) + 1.0e-2;
		while(x < xmax)
		{
			x0 = iMath::Round2Int(x);
			y0 = iMath::Round2Int(y);
			for(k=-kmax; k<=kmax; k++)
			{
				f = 0.0f;
				for(j=0; j<subsample; j++)
				{
					p[1] = y0 + k + scale*(j-offset) - y;
					for(i=0; i<subsample; i++)
					{
						p[0] = x0 + scale*(i-offset) - x;
						r = fabs(normal[0]*p[0]+normal[1]*p[1]);
                        if(r < r1) f += 1.0f; else if(r < r2) f += (r2-r)/(r2-r1);
					}
				}
				f *= (scale*scale);
				if(f>0.0 && y0+k>=ymin && y0+k<=ymax)
				{
					DrawPixel(x0,y0+k,f,image,color,swap);
				}
			}
			x += 1.0f;
			y += gradient;
		}
	}
};


using namespace iImageComposerWindow_Private;


//
// Helper classes
//
iImageComposerWindow::iImageComposerWindow(iViewModule *vm, iImageComposer *ic)
{ 
	mType = UNDEFINED;
	mComposer = ic;
	mViewModules[0] = vm;
	mViewModules[1] = mViewModules[2] = 0;
}


iImageComposerWindow::~iImageComposerWindow()
{
	while(mZoomTargets.Size() > 0)
	{
		mZoomTargets[mZoomTargets.MaxIndex()]->SetZoomSource(0);
	}
}


void iImageComposerWindow::SetViewModule(iViewModule *vm, int win)
{
	if(win>0 && win<3)
	{
		mViewModules[win] = vm;
	}
	else
	{
		mViewModules[0] = vm;
		if(vm == 0) mViewModules[1] = mViewModules[2] = 0;
	}
}


void iImageComposerWindow::UpdateWindow()
{
	int j, k;
	bool present;
	iShell *s = this->GetImageComposer()->GetShell();

	for(j=0; j<3; j++) if(mViewModules[j] != 0)
	{
		present = false;
		for(k=0; !present && k<s->GetNumberOfViewModules(); k++)
		{
			if(mViewModules[j] == s->GetViewModule(k)) present = true;
		}
		if(!present) this->SetViewModule(0,j);
	}
}


int iImageComposerWindow::GetWindowNumber(int win) const
{
	if(win<0 && win>2) win = 2;
	if(mViewModules[win] == 0) return -1; else return mViewModules[win]->GetWindowNumber();
}

	
bool iImageComposerWindow::RenderStereoImage(iStereoImage &im) const
{
	iMonitor h(this->GetImageComposer());

	if(mViewModules[0] == 0)
	{
		h.PostError("Unable to render image: ViewModule is not set.");
		return false;
	}

	if(mViewModules[1]!=0 || mViewModules[2]!=0)
	{
		iStereoImage imRed, imGreen, imBlue;
		mViewModules[0]->RenderStereoImage(imRed);
		if(h.IsStopped()) return false;

		im.SetStereo(imRed.IsStereo());

		if(mViewModules[1] != 0)
		{
			mViewModules[1]->RenderStereoImage(imGreen);
			if(h.IsStopped()) return false;
			if(imRed.Width()!=imGreen.Width() || imRed.Height()!=imGreen.Height())
			{
				imGreen.SetStereo(imRed.IsStereo());
				imGreen.Scale(imRed.Width(),imRed.Height());
			}
		}
		if(mViewModules[2] != 0)
		{
			mViewModules[2]->RenderStereoImage(imBlue);
			if(h.IsStopped()) return false;
			if(imRed.Width()!=imBlue.Width() || imRed.Height()!=imBlue.Height())
			{
				imBlue.SetStereo(imRed.IsStereo());
				imBlue.Scale(imRed.Width(),imRed.Height());
			}
		}
		if(!im.CombineInPseudoColor(imRed,imGreen,imBlue))
		{
			h.PostError("Unable to render image: error in pseudo-color composting.");
			return false;
		}
		else return true;
	}
	else
	{
		mViewModules[0]->RenderStereoImage(im);
		return !h.IsStopped();
	}
}


void iImageComposerWindow::OverlayImage(iStereoImage &outIm, const iStereoImage &inIm, const iColor &bg) const
{
	//
	//  Check that dimensions are ok.
	//
	iStereoImage winIm(inIm);
	winIm.Scale(this->GetContentsWidth(),this->GetContentsHeight());

	unsigned char *outPtr[2], *outPtr1, *winPtr, *winPtr1;
	//
	//  Prepare the left eye
	//
	outPtr[0] = outIm.LeftEye().DataPointer();
	//
	//  Begin creating a non-stereo image
	//
	int eye, numEyes = 1;
	outPtr[1] = 0; // for debugging - will crash if tries to write into the right eye image

	//
	//  Check whether we need to switch to a stereo mode
	//
	if(winIm.IsStereo())
	{
		numEyes = 2;
		if(!outIm.IsStereo())
		{
			//
			//  Prepare the right eye
			//
			outIm.CopyLeftEyeToRightEye();
			outPtr[1] = outIm.RightEye().DataPointer();
		}
	}

	int outWidth = outIm.Width();
	int outHeight = outIm.Height();

	int winWidth = winIm.Width();
	int winHeight = winIm.Height();

	int winDepth = winIm.Depth();
	int outDepth = outIm.Depth();
	int d = (winDepth < outDepth) ? winDepth : outDepth;

	//
	//  Fill in the background color; this may be needed since winIm may have smaller size/depth
	//
	int i, j, k, xoff, yoff, w1, h1;
	char rgb[4];
	bg.GetRGB(rgb);
	rgb[3] = 255;

	if(inIm.IsEmpty() || this->GetContentsWidth()<this->GetImageWidth() || this->GetContentsHeight()<this->GetImageHeight() || winDepth<outDepth)
	{
		xoff = this->GetImageX();
		yoff = this->GetImageY();
		w1 = this->GetImageWidth();
		h1 = this->GetImageHeight();

		for(eye=0; eye<numEyes; eye++)
		{
			for(j=0; j<h1; j++)
			{
				outPtr1 = outPtr[eye] + outDepth*(xoff+outWidth*(j+yoff));
				for(i=0; i<w1; i++, outPtr1+=outDepth)
				{
					for(k=0; k<outDepth; k++) outPtr1[k] = rgb[k];  // fastest assignment!!
				}
			}
		}
		if(inIm.IsEmpty()) return;
	}

	//
	//  Chop off too large an image
	//
	xoff = this->GetContentsX();
	yoff = this->GetContentsY();
	w1 = winWidth;
	h1 = winHeight;
	if(xoff+w1 > outWidth) w1 = outWidth - xoff;
	if(yoff+h1 > outHeight) h1 = outHeight - yoff;

	for(eye=0; eye<numEyes; eye++)
	{
		winPtr = winIm.Eye(eye).DataPointer();
		for(j=0; j<h1; j++)
		{
			outPtr1 = outPtr[eye] + outDepth*(xoff+outWidth*(j+yoff));
			winPtr1 = winPtr + winDepth*winWidth*j;  // Must be winWidth to have the right stride even if the foreground is too large
			for(i=0; i<w1; i++,outPtr1+=outDepth,winPtr1+=winDepth)
			{
				for(k=0; k<d; k++) outPtr1[k] = winPtr1[k];
			}
		}
	}
}


void iImageComposerWindow::RegisterZoomTarget(iImageComposerForegroundWindow *win)
{
	if(win!=0 && win!=this)
	{
		mZoomTargets.AddUnique(win);
	}
}


void iImageComposerWindow::UnRegisterZoomTarget(iImageComposerForegroundWindow *win)
{
	if(win!=0 && win!=this)
	{
		mZoomTargets.Remove(win);
	}
}


//
//  Background windows
//
iImageComposerBackgroundWindow* iImageComposerBackgroundWindow::New(int tx, int ty, iImageComposer *ic)
{
	return new iImageComposerBackgroundWindow(tx,ty,ic);
}


iImageComposerBackgroundWindow::iImageComposerBackgroundWindow(int tx, int ty, iImageComposer *ic) : iImageComposerWindow(0,ic)
{
	mType = BACKGROUND;
	mTileX = tx;
	mTileY = ty;
}


int iImageComposerBackgroundWindow::GetId() const
{
	iImageComposerBackgroundWindow *w;
	int j = 0;
	while((w = mComposer->GetBackgroundWindow(j))!=0 && w!=this) j++;
	if(w == this)
	{
		return -(1+j);
	}
	else
	{
		IBUG_WARN("Invalid id for a background window.");
		return 0;
	}
}


int iImageComposerBackgroundWindow::GetImageX() const
{
	int x = mComposer->GetBorderWidth() + mTileX*mComposer->GetTileWidth();
	if(mComposer->GetInnerBorder()) x += mTileX*mComposer->GetBorderWidth();
	return x;
}


int iImageComposerBackgroundWindow::GetImageY() const
{
	int y = mComposer->GetBorderWidth() + mTileY*mComposer->GetTileHeight();
	if(mComposer->GetInnerBorder()) y += mTileY*mComposer->GetBorderWidth();
	return y;
}


int iImageComposerBackgroundWindow::GetImageWidth() const
{
	return mComposer->GetTileWidth();
}


int iImageComposerBackgroundWindow::GetImageHeight() const
{
	return mComposer->GetTileHeight();
}


int iImageComposerBackgroundWindow::GetContentsX() const
{
	int x = this->GetImageX();
	if(!mComposer->GetScaleBackground()) x += (mComposer->GetTileWidth()-this->GetWindowWidth())/2;
	return x;
}


int iImageComposerBackgroundWindow::GetContentsY() const
{
	int y = this->GetImageY();
	if(!mComposer->GetScaleBackground()) y += (mComposer->GetTileHeight()-this->GetWindowHeight())/2;
	return y;
}


int iImageComposerBackgroundWindow::GetContentsWidth() const
{
	if(mComposer->GetScaleBackground()) return mComposer->GetTileWidth(); else return this->GetWindowWidth();
}


int iImageComposerBackgroundWindow::GetContentsHeight() const
{
	if(mComposer->GetScaleBackground()) return mComposer->GetTileHeight(); else return this->GetWindowHeight();
}


int iImageComposerBackgroundWindow::GetWindowWidth() const
{
	if(this->GetViewModule() != 0) return this->GetViewModule()->GetThisImageWidth(); else return mWallpaperImage.Width();
}


int iImageComposerBackgroundWindow::GetWindowHeight() const
{
	if(this->GetViewModule() != 0) return this->GetViewModule()->GetThisImageHeight(); else return mWallpaperImage.Height();
}


bool iImageComposerBackgroundWindow::IsEmpty() const
{
	return (this->GetViewModule()==0 && mWallpaperImage.IsEmpty());
}


void iImageComposerBackgroundWindow::Draw(iStereoImage &outIm) const
{
	iStereoImage winIm;
	if(this->GetViewModule() != 0)
	{
		//
		//  Render the image
		//
		if(this->RenderStereoImage(winIm)) this->OverlayImage(outIm,winIm,mEmptyBackground);
	}
	else
	{
		winIm = mWallpaperImage;
		this->OverlayImage(outIm,winIm,mEmptyBackground);
	}
}


bool iImageComposerBackgroundWindow::LoadWallpaperImage(const iString &s)
{
	if(s != mWallpaperFile)
	{
		if(s.IsEmpty())
		{
			mWallpaperImage.Clear();
			mWallpaperFile.Clear();
			mComposer->UpdateSize();
			return true;
		}
		else
		{
			bool ret = mWallpaperImage.LoadFromFile(s);
			if(ret) 
			{
				mWallpaperFile = s;
				mComposer->UpdateSize();
			}
			return ret;
		}
	}
	else return true;
}


//
//  Foreground windows
//
iImageComposerForegroundWindow* iImageComposerForegroundWindow::New(iViewModule *vm, iImageComposer *ic)
{
	return new iImageComposerForegroundWindow(vm,ic);
}


iImageComposerForegroundWindow::iImageComposerForegroundWindow(iViewModule *vm, iImageComposer *ic) : iImageComposerWindow(vm,ic)
{
	mType = FOREGROUND;
	mBorderWidth = 1;
	mScale = 0.5;
	mPosX = mPosY = 0;

	mZoomX = mZoomY = 0.5f;
	mZoomFactor = 0.2f;
	mZoomSource = 0;
	mZoomFlag = _4Lines;
	mZoomAuto = false;
}


iImageComposerForegroundWindow::~iImageComposerForegroundWindow()
{
	if(mZoomSource != 0) mZoomSource->UnRegisterZoomTarget(this);
}


int iImageComposerForegroundWindow::GetId() const
{
	iImageComposerForegroundWindow *w;
	int j = 0;
	while((w = mComposer->GetForegroundWindow(j))!=0 && w!=this) j++;
	if(w == this)
	{
		return (1+j);
	}
	else
	{
		IBUG_WARN("Invalid id for a foreground window.");
		return 0;
	}
}


int iImageComposerForegroundWindow::GetImageX() const
{
	return mPosX;
}


int iImageComposerForegroundWindow::GetImageY() const
{
	return mPosY;
}


int iImageComposerForegroundWindow::GetImageWidth() const
{
	return 2*mBorderWidth + this->GetContentsWidth();
}


int iImageComposerForegroundWindow::GetImageHeight() const
{
	return 2*mBorderWidth + this->GetContentsHeight();
}


int iImageComposerForegroundWindow::GetContentsX() const
{
	return mPosX + mBorderWidth;
}


int iImageComposerForegroundWindow::GetContentsY() const
{
	return mPosY + mBorderWidth;
}


int iImageComposerForegroundWindow::GetContentsWidth() const
{
	return iMath::Round2Int(mScale*this->GetWindowWidth());
}


int iImageComposerForegroundWindow::GetContentsHeight() const
{
	return iMath::Round2Int(mScale*this->GetWindowHeight());
}


int iImageComposerForegroundWindow::GetWindowWidth() const
{
	if(this->GetViewModule() != 0) return this->GetViewModule()->GetThisImageWidth(); else return 0;
}


int iImageComposerForegroundWindow::GetWindowHeight() const
{
	if(this->GetViewModule() != 0) return this->GetViewModule()->GetThisImageHeight(); else return 0;
}


void iImageComposerForegroundWindow::UpdateWindow()
{
	this->iImageComposerWindow::UpdateWindow();
	mZoomAuto = this->SyncZoom();
}


bool iImageComposerForegroundWindow::IsEmpty() const
{
	return (this->GetViewModule() == 0);
}


void iImageComposerForegroundWindow::Draw(iStereoImage &outIm) const
{
	if(this->GetViewModule() != 0)
	{
		iMonitor h(this->GetImageComposer());
		iStereoImage winIm;

		this->RenderStereoImage(winIm);
		if(h.IsStopped()) return;
		//
		// Draw zoom first
		//
		if(mZoomSource != 0)
		{
			int bw = this->GetBorderWidth();
			iColor bc(this->GetBorderColor());

			int zx1 = iMath::Round2Int(mZoomSource->GetContentsX()+(mZoomX*mZoomSource->GetContentsWidth()-0.5*this->GetZoomWidth()));
			int zy1 = iMath::Round2Int(mZoomSource->GetContentsY()+(mZoomY*mZoomSource->GetContentsHeight()-0.5*this->GetZoomHeight()));
			int zx2 = iMath::Round2Int(mZoomSource->GetContentsX()+(mZoomX*mZoomSource->GetContentsWidth()+0.5*this->GetZoomWidth()));
			int zy2 = iMath::Round2Int(mZoomSource->GetContentsY()+(mZoomY*mZoomSource->GetContentsHeight()+0.5*this->GetZoomHeight()));

			int i;
			for(i=0; i<bw; i++)
			{
				DrawLine(zx1+i,zy1+i,zx1+i,zy2-i,outIm,bc);
				DrawLine(zx1+i,zy2-i,zx2-i,zy2-i,outIm,bc);
				DrawLine(zx2-i,zy2-i,zx2-i,zy1+i,outIm,bc);
				DrawLine(zx2-i,zy1+i,zx1+i,zy1+i,outIm,bc);
			}

			zx2 += (1-bw);
			zy2 += (1-bw);

			int x, y;

			if(this->GetCoordinatesForZoomLine(0,x,y,mPosX,mPosY,this->GetImageWidth()-bw+1,this->GetImageHeight()-bw+1)) DrawWideLine(zx1,zy1,x,y,bw,outIm,bc,0);
			if(this->GetCoordinatesForZoomLine(1,x,y,mPosX,mPosY,this->GetImageWidth()-bw+1,this->GetImageHeight()-bw+1)) DrawWideLine(zx2,zy1,x,y,bw,outIm,bc,1);
			if(this->GetCoordinatesForZoomLine(2,x,y,mPosX,mPosY,this->GetImageWidth()-bw+1,this->GetImageHeight()-bw+1)) DrawWideLine(zx1,zy2,x,y,bw,outIm,bc,2);
			if(this->GetCoordinatesForZoomLine(3,x,y,mPosX,mPosY,this->GetImageWidth()-bw+1,this->GetImageHeight()-bw+1)) DrawWideLine(zx2,zy2,x,y,bw,outIm,bc,3);
		}
		//
		//  Draw the window
		//
		this->OverlayImage(outIm,winIm,mBorderColor);
	}
}


void iImageComposerForegroundWindow::SetScale(float s)
{
	if(s>0.0 && s<=1.0 && fabs(s-mScale)>1.0e-5) 
	{
		mScale = s;
		this->CorrectPosition();
	}
}


void iImageComposerForegroundWindow::SetPosition(int x, int y)
{
	if(x!=mPosX || y!=mPosY)
	{
		//
		//  Quantize the position but not if we are against the outer wall
		//
		if(x+this->GetImageWidth() < mComposer->GetImageWidth()) mPosX = mPosQuantum*(x/mPosQuantum); else mPosX = x;
		if(y+this->GetImageHeight() < mComposer->GetImageHeight()) mPosY = mPosQuantum*(y/mPosQuantum); else mPosY = y;
		this->CorrectPosition();
	}
}


void iImageComposerForegroundWindow::SetBorderWidth(int w)
{
	if(w>=0 && w!=mBorderWidth)
	{
		mBorderWidth = w;
		this->CorrectPosition();
	}
}


void iImageComposerForegroundWindow::SetBorderColor(const iColor& c)
{
	if(c != mBorderColor)
	{
		mBorderColor = c;
	}
}


void iImageComposerForegroundWindow::CorrectPosition()
{
	if(mPosX < 0) mPosX = 0;
	if(mPosY < 0) mPosY = 0;

	if(mBorderWidth > this->GetWindowWidth()/4) mBorderWidth = this->GetWindowWidth()/4;
	if(mBorderWidth > this->GetWindowHeight()/4) mBorderWidth = this->GetWindowHeight()/4;

	//
	//  Is scale small enough?
	//
	if(this->GetImageWidth() > mComposer->GetImageWidth())
	{
		mPosX = 0;
		mScale = float(mComposer->GetImageWidth()-2*mBorderWidth)/this->GetWindowWidth();
	}
		
	if(this->GetImageHeight() > mComposer->GetImageHeight()) 
	{
		mPosY = 0;
		mScale = float(mComposer->GetImageHeight()-2*mBorderWidth)/this->GetWindowHeight();
	}		
	//
	//  Now we are guaranteed to fit into the image. But are we fully inside?
	//
	if(mPosX+this->GetImageWidth() > mComposer->GetImageWidth()) mPosX = mComposer->GetImageWidth() - this->GetImageWidth();
	if(mPosY+this->GetImageHeight() > mComposer->GetImageHeight()) mPosY = mComposer->GetImageHeight() - this->GetImageHeight();

	this->UpdateZoomFlag();
}


void iImageComposerForegroundWindow::SetZoomSource(iImageComposerWindow *source)
{
	if(source != this)
	{
		if(mZoomSource != 0) mZoomSource->UnRegisterZoomTarget(this);
		mZoomSource = source;
		if(source != 0) source->RegisterZoomTarget(this);
		this->UpdateZoomPosition();
		this->UpdateZoomFlag();
	}
}


void iImageComposerForegroundWindow::SetZoomPosition(float x, float y)
{
	if(x>0.0f && x<1.0f && y>0.0f && y<1.0f && (fabs(x-mZoomX)>1.0e-5 || fabs(y-mZoomY)>1.0e-5))
	{
		mZoomX = x;
		mZoomY = y;
		if(mZoomSource != 0) this->UpdateZoomPosition();
	}
}


void iImageComposerForegroundWindow::SetZoomFactor(float factor)
{
	if(factor>0.0f && factor<1.0f && fabs(factor-mZoomFactor)>1.0e-5)
	{
		mZoomFactor = factor;
		if(mZoomSource != 0) this->UpdateZoomPosition();
	}
}


void iImageComposerForegroundWindow::SetZoomFlag(int f)
{
	if(f > 0)
	{
		mZoomFlag = f;
		this->UpdateZoomFlag();
	}
}


bool iImageComposerForegroundWindow::GetCoordinatesForZoomLine(int n, int &x, int &y, int wx, int wy, int ww, int wh) const
{
	if(n<0 || n>3 || (mZoomFlag & (1<<n)) == 0) return false;

	int sx = (mZoomFlag & _ShiftX)/_ShiftX;
	int sy = (mZoomFlag & _ShiftY)/_ShiftY;

	switch(n)
	{
	case 0:
		{
			x = wx + sx*(ww-1);
			y = wy + sy*(wh-1);
			break;
		}
	case 1:
		{
			x = wx + (1-sx)*(ww-1);
			y = wy + sy*(wh-1);
			break;
		}
	case 2:
		{
			x = wx + sx*(ww-1);
			y = wy + (1-sy)*(wh-1);
			break;
		}
	case 3:
		{
			x = wx + (1-sx)*(ww-1);
			y = wy + (1-sy)*(wh-1);
			break;
		}
	}

	return true;
}


void iImageComposerForegroundWindow::UpdateZoomFlag()
{
	if(mZoomSource==0 || mZoomFlag==_4Lines) return;

	mZoomFlag = 0;

	//
	//  Find lines that make up to a convex hull
	//
	bool ok;
	int i, j, s, d, pz[4][2], pw[4][2], n[2];

	pz[0][0] = pz[2][0] = iMath::Round2Int(mZoomSource->GetContentsX()+(mZoomX*mZoomSource->GetContentsWidth()-0.5*this->GetZoomWidth()));
	pz[1][0] = pz[3][0] = iMath::Round2Int(mZoomSource->GetContentsX()+(mZoomX*mZoomSource->GetContentsWidth()+0.5*this->GetZoomWidth()));
	pz[0][1] = pz[1][1] = iMath::Round2Int(mZoomSource->GetContentsY()+(mZoomY*mZoomSource->GetContentsHeight()-0.5*this->GetZoomHeight()));
	pz[2][1] = pz[3][1] = iMath::Round2Int(mZoomSource->GetContentsY()+(mZoomY*mZoomSource->GetContentsHeight()+0.5*this->GetZoomHeight()));

	pw[0][0] = pw[2][0] = mPosX;
	pw[1][0] = pw[3][0] = mPosX + this->GetImageWidth() - 1;
	pw[0][1] = pw[1][1] = mPosY;
	pw[2][1] = pw[3][1] = mPosY + this->GetImageHeight() - 1;

	for(j=0; j<4; j++)
	{
		n[0] = pw[j][1] - pz[j][1];
		n[1] = pz[j][0] - pw[j][0];

		ok = true;
		s = 0;
		for(i=0; ok && i<4; i++) if(i != j)
		{
			d = n[0]*(pz[i][0]-pz[j][0]) + n[1]*(pz[i][1]-pz[j][1]);
			if(s==0 && d!=0)
			{
				s = d/abs(d);
			}
			if(d*s < 0) ok = false;
		}
		for(i=0; ok && i<4; i++) if(i != j)
		{
			d = n[0]*(pw[i][0]-pz[j][0]) + n[1]*(pw[i][1]-pz[j][1]);
			if(d*s < 0) ok = false;
		}
		if(ok)
		{
			mZoomFlag = mZoomFlag | (1 << j);
		}
	}

	//
	//  Activate a switch if needed
	//
	if(mZoomFlag == (_Line11 | _Line12))
	{
		mZoomFlag = _Line21 | _Line22 | _ShiftX;
	}
	if(mZoomFlag == (_Line21 | _Line22))
	{
		mZoomFlag = _Line11 | _Line12 | _ShiftX;
	}
	if(mZoomFlag == (_Line11 | _Line21))
	{
		mZoomFlag = _Line12 | _Line22 | _ShiftY;
	}
	if(mZoomFlag == (_Line12 | _Line22))
	{
		mZoomFlag = _Line11 | _Line21 | _ShiftY;
	}
}


void iImageComposerForegroundWindow::UpdateZoomPosition()
{
	if(mZoomSource==0 || this->GetViewModule()==0 || (mZoomAuto=this->SyncZoom())) return;

	if(this->GetWindowWidth()*mZoomFactor > mZoomSource->GetContentsWidth())
	{
		mZoomFactor = float(mZoomSource->GetContentsWidth())/this->GetWindowWidth();
	}
	if(this->GetWindowHeight()*mZoomFactor > mZoomSource->GetContentsHeight())
	{
		mZoomFactor = float(mZoomSource->GetContentsHeight())/this->GetWindowHeight();
	}

	float w = 0.5*this->GetWindowWidth()*mZoomFactor/mZoomSource->GetContentsWidth();
	float h = 0.5*this->GetWindowHeight()*mZoomFactor/mZoomSource->GetContentsHeight();

	if(mZoomX < w) mZoomX = w;
	if(mZoomY < h) mZoomY = h;
	if(mZoomX > 1.0f-w) mZoomX = 1.0f - w;
	if(mZoomY > 1.0f-h) mZoomY = 1.0f - h;

	this->UpdateZoomFlag();
}


bool iImageComposerForegroundWindow::SyncZoom()
{
	if(mZoomSource==0 || mZoomSource->GetViewModule()==0 || this->GetViewModule()==0) return false;

	iViewModule *source = mZoomSource->GetViewModule();
	iViewModule *target = this->GetViewModule();

	//
	//  Do we have the same projections?
	//
	if(source->GetRenderer()->GetActiveCamera()->GetParallelProjection() != target->GetRenderer()->GetActiveCamera()->GetParallelProjection()) return false;

	//
	//  ViewUp vectors must coincide
	//
	int i;
	double sPos[3], tPos[3];
	source->GetRenderer()->GetActiveCamera()->GetViewUp(sPos);
	target->GetRenderer()->GetActiveCamera()->GetViewUp(tPos);
	for(i=0; i<3; i++) if(fabs(sPos[i]-tPos[i]) > iMath::_DoubleTolerance)
	{
		return false;
	}

	//
	//  In perspective projection camera focal points, view angles, and directions must coincide
	//
	if(source->GetRenderer()->GetActiveCamera()->GetParallelProjection() == 0)
	{
		source->GetRenderer()->GetActiveCamera()->GetFocalPoint(sPos);
		target->GetRenderer()->GetActiveCamera()->GetFocalPoint(tPos);
		for(i=0; i<3; i++) if(fabs(sPos[i]-tPos[i]) > iMath::_DoubleTolerance)
		{
			return false;
		}
		source->GetRenderer()->GetActiveCamera()->GetDirectionOfProjection(sPos);
		target->GetRenderer()->GetActiveCamera()->GetDirectionOfProjection(tPos);
		for(i=0; i<3; i++) if(fabs(sPos[i]-tPos[i]) > iMath::_DoubleTolerance)
		{
			return false;
		}
		if(fabs(source->GetRenderer()->GetActiveCamera()->GetViewAngle()-target->GetRenderer()->GetActiveCamera()->GetViewAngle()) > iMath::_DoubleTolerance)
		{
			return false;
		}
	}

	//
	//  Get target frustrum planes
	//
	double aspect[2], planes[24];
	target->GetRenderer()->GetAspect(aspect);
	target->GetRenderer()->GetActiveCamera()->GetFrustumPlanes(aspect[0]/aspect[1],planes);

	//
	//  Find the source focal plane
	//
	double fp[3], fplane[4];
	source->GetRenderer()->GetActiveCamera()->GetFocalPoint(fp);
	source->GetRenderer()->GetActiveCamera()->GetViewPlaneNormal(fplane);
	fplane[3] = -(fplane[0]*fp[0]+fplane[1]*fp[1]+fplane[2]*fp[2]);


	//
	//  Find the rectangle that we see at the camera focal point in the target window
	//
	double r[4][3];
	if(!iMath::Intersect3Planes(fplane,planes+4*0,planes+4*2,r[0])) return false; // -X & -Y planes
	if(!iMath::Intersect3Planes(fplane,planes+4*1,planes+4*2,r[1])) return false; // +X & -Y planes
	if(!iMath::Intersect3Planes(fplane,planes+4*0,planes+4*3,r[2])) return false; // -X & +Y planes
	if(!iMath::Intersect3Planes(fplane,planes+4*1,planes+4*3,r[3])) return false; // +X & +Y planes

	//
	//  Find the projections of this rectangle onto the source view plane
	//
	for(i=0; i<4; i++)
	{
		source->GetRenderer()->WorldToView(r[i][0],r[i][1],r[i][2]);
		source->GetRenderer()->ViewToNormalizedViewport(r[i][0],r[i][1],r[i][2]);
	}

	//
	//  Is it a rectangle?
	//
	if(fabs(r[0][0]-r[2][0])>iMath::_DoubleTolerance || fabs(r[1][0]-r[3][0])>iMath::_DoubleTolerance || fabs(r[0][1]-r[1][1])>iMath::_DoubleTolerance || fabs(r[2][1]-r[3][1])>iMath::_DoubleTolerance) return false;

	//
	//  Does it fits?
	//
	if(r[0][0]<0.0 || r[0][0]>1.0 || r[0][1]<0.0 || r[0][1]>1.0 || r[3][0]<0.0 || r[3][0]>1.0 || r[3][1]<0.0 || r[3][1]>1.0) return false;

	mZoomFactor = (r[3][1]-r[0][1])*mZoomSource->GetContentsHeight()/this->GetWindowHeight();
	mZoomX = 0.5*(r[0][0]+r[3][0]);
	mZoomY = 0.5*(r[0][1]+r[3][1]);

	this->UpdateZoomFlag();
	return true;
}

