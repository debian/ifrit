/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ipointglyph.h"


#include "ierror.h"
#include "ispheresource.h"

#include <vtkArrowSource.h>
#include <vtkCellData.h>
#include <vtkConeSource.h>
#include <vtkCylinderSource.h>
#include <vtkDecimatePro.h>
#include <vtkFloatArray.h>
#include <vtkMath.h>
#include <vtkPlatonicSolidSource.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkPointSource.h>
#include <vtkPolyData.h>
#include <vtkPolyDataNormals.h>
#include <vtkReverseSense.h>
#include <vtkSphereSource.h>


namespace iPointGlyph_Private
{
	struct PointType
	{
		int Type;  // for consistency checking
		bool IsOriented;
		const char *Name;
	};

	const PointType type[] = {
		{ 0, false, "Point" },
		{ 1, false, "Sphere" },
		{ 2, true,  "Tetrahydron" },
		{ 3, false, "Cube" },
		{ 4, false, "Octahedron" },
		{ 5, false, "Icosahedron" },
		{ 6, false, "Dodecahedron" },
		{ 7, true,  "Cone" },
		{ 8, true,  "Cylinder" },
		{ 9, true, "Arrow" },
		{ 10, false, "Cluster" },
		{ 11, true, "Galaxy" } };
};


using namespace iParameter;
using namespace iPointGlyph_Private;


iPointGlyph* iPointGlyph::New()
{
	return new iPointGlyph();
}


iPointGlyph::iPointGlyph()
{
	int i;

	mType = PointGlyphType::Point;

	for(i=0; i<PointGlyphType::SIZE; i++) 
	{
		if(type[i].Type != i) vtkErrorMacro("Incorrectly configured object.");
		mData[i] = vtkPolyData::New(); IERROR_CHECK_MEMORY(mData[i]);
	}

	this->SetNumberOfInputPorts(0);

	this->CreateData(3);
}


iPointGlyph::~iPointGlyph()
{
	int i;

	for(i=0; i<PointGlyphType::SIZE; i++) 
	{
		mData[i]->Delete();
	}
}

	
void iPointGlyph::CreateData(int res)
{
	int i;
	vtkIdType l, n;
	double x[3], r;
	vtkPoints *p, *op;
	vtkFloatArray *fa;
	vtkDataArray *da;
	bool done[PointGlyphType::SIZE];

	for(i=0; i<PointGlyphType::SIZE; i++) done[i] = false; // this is for internal checking

	if(res < 1) res = 1;
	if(res > 100) res = 100;

	vtkPolyDataNormals *pn = vtkPolyDataNormals::New(); IERROR_CHECK_MEMORY(pn);
	pn->AutoOrientNormalsOn();
	pn->ConsistencyOn();

	//
	//  Sphere
	//
	/*
	vtkSphereSource *ss = vtkSphereSource::New(); IERROR_CHECK_MEMORY(ss);
	vtkDecimatePro *sf = vtkDecimatePro::New(); IERROR_CHECK_MEMORY(sf);
	ss->SetThetaResolution(12*res);
	ss->SetPhiResolution(6*res);
	ss->SetRadius(0.5);
	ss->LatLongTessellationOff();
	ss->Update();
	ss->GetOutput()->GetPointData()->SetNormals(0);

	sf->SetTargetReduction(0.5);
	sf->PreserveTopologyOn();
	sf->SetInputConnection(ss->GetOutputPort());

	pn->ComputePointNormalsOn();
	pn->SetInputConnection(sf->GetOutputPort());

	pn->SetOutput(mData[PointGlyphType::Sphere]);
	pn->Update();
	sf->Delete();
	ss->Delete();
	done[PointGlyphType::Sphere] = true;
	*/

	iSphereSource *ss = iSphereSource::New(); IERROR_CHECK_MEMORY(ss);
	ss->SetResolution(3*res);
	ss->SetRadius(0.5);
	ss->SetOutput(mData[PointGlyphType::Sphere]);
	ss->Update();
	ss->Delete();
	done[PointGlyphType::Sphere] = true;

	//
	//  Platonic solids
	//
	vtkPlatonicSolidSource *ps = vtkPlatonicSolidSource::New(); IERROR_CHECK_MEMORY(ps);
	pn->ComputePointNormalsOff();
	pn->SetInputConnection(ps->GetOutputPort());

	pn->SetOutput(mData[PointGlyphType::Tetrahydron]);
	pn->ComputeCellNormalsOn();
	pn->FlipNormalsOn();
	ps->SetSolidTypeToTetrahedron();
	pn->Update();
	mData[PointGlyphType::Tetrahydron]->GetCellData()->Initialize(); // clears the cell colors that are set by vtkPlatonicSolidSource
	this->ScaleData(mData[PointGlyphType::Tetrahydron],0.5);
	done[PointGlyphType::Tetrahydron] = true;

	pn->SetOutput(mData[PointGlyphType::Cube]);
	pn->ComputeCellNormalsOff();
	pn->FlipNormalsOff();
	ps->SetSolidTypeToCube();
	pn->Update();
	mData[PointGlyphType::Cube]->GetCellData()->Initialize(); // clears the cell colors that are set by vtkPlatonicSolidSource
	this->ScaleData(mData[PointGlyphType::Cube],0.5);
	done[PointGlyphType::Cube] = true;

	pn->SetOutput(mData[PointGlyphType::Octahedron]);
	pn->ComputeCellNormalsOn();
	pn->FlipNormalsOff();
	ps->SetSolidTypeToOctahedron();
	pn->Update();
	mData[PointGlyphType::Octahedron]->GetCellData()->Initialize(); // clears the cell colors that are set by vtkPlatonicSolidSource
	this->ScaleData(mData[PointGlyphType::Octahedron],0.5);
	done[PointGlyphType::Octahedron] = true;

	pn->SetOutput(mData[PointGlyphType::Icosahedron]);
	pn->ComputeCellNormalsOn();
	pn->FlipNormalsOff();
	ps->SetSolidTypeToIcosahedron();
	pn->Update();
	mData[PointGlyphType::Icosahedron]->GetCellData()->Initialize(); // clears the cell colors that are set by vtkPlatonicSolidSource
	this->ScaleData(mData[PointGlyphType::Icosahedron],0.5);
	done[PointGlyphType::Icosahedron] = true;

	pn->SetOutput(mData[PointGlyphType::Dodecahedron]);
	pn->ComputeCellNormalsOn();
	pn->FlipNormalsOff();
	ps->SetSolidTypeToDodecahedron();
	pn->Update();
	mData[PointGlyphType::Dodecahedron]->GetCellData()->Initialize(); // clears the cell colors that are set by vtkPlatonicSolidSource
	this->ScaleData(mData[PointGlyphType::Dodecahedron],0.5);
	done[PointGlyphType::Dodecahedron] = true;

	pn->Delete();
	ps->Delete();
	
	//
	//  Cone
	//
	vtkConeSource *cs = vtkConeSource::New(); IERROR_CHECK_MEMORY(cs);
	cs->SetResolution(6*res);
	cs->SetOutput(mData[PointGlyphType::Cone]);
	cs->Update();
	cs->Delete();
	this->ScaleData(mData[PointGlyphType::Cone],0.8);
	this->SwapAxes(mData[PointGlyphType::Cone],0,2);
	this->ShiftData(mData[PointGlyphType::Cone],2,0.1);
	done[PointGlyphType::Cone] = true;

	//
	//  Cylinder
	//
	vtkCylinderSource *ts = vtkCylinderSource::New(); IERROR_CHECK_MEMORY(ts);
	ts->SetResolution(6*res);
	ts->SetOutput(mData[PointGlyphType::Cylinder]);
	ts->Update();
	ts->Delete();
	this->ScaleData(mData[PointGlyphType::Cylinder],0.7);
	this->SwapAxes(mData[PointGlyphType::Cylinder],1,2);
	done[PointGlyphType::Cylinder] = true;
	
	//
	//  Arrow
	//
	vtkArrowSource *as = vtkArrowSource::New(); IERROR_CHECK_MEMORY(as);
	as->SetTipResolution(6*res);
	as->SetShaftResolution(6*res);
	as->SetTipRadius(0.5);
	as->SetShaftRadius(0.2);
	as->SetOutput(mData[PointGlyphType::Arrow]);
	as->Update();
	as->Delete();
	this->ScaleData(mData[PointGlyphType::Arrow],0.93);
	this->SwapAxes(mData[PointGlyphType::Arrow],0,2);
	this->ShiftData(mData[PointGlyphType::Arrow],2,-0.45);
	done[PointGlyphType::Arrow] = true;
	
	//
	//  Point
	//
	vtkPointSource *cc;
	cc = vtkPointSource::New(); IERROR_CHECK_MEMORY(cc);
	cc->SetNumberOfPoints(1);
	cc->SetCenter(0.0,0.0,0.0);
	cc->SetRadius(1.0);
	cc->SetOutput(mData[PointGlyphType::Point]);
	cc->Update();
	cc->Delete();
	p = mData[PointGlyphType::Point]->GetPoints();
	p->SetPoint(0,0.0,0.0,0.0);
	done[PointGlyphType::Point] = true;
	
	//
	//  Cluster
	//
	cc = vtkPointSource::New(); IERROR_CHECK_MEMORY(cc);
	cc->SetNumberOfPoints(30*res);
	cc->SetCenter(0.0,0.0,0.0);
	cc->SetRadius(1.0);
	cc->SetOutput(mData[PointGlyphType::Cluster]);
	cc->Update();
	cc->Delete();
	//
	//  Rearrange points radially to have a centrally concentrated cluster
	//
	p = mData[PointGlyphType::Cluster]->GetPoints();
	n = p->GetNumberOfPoints();
	for(l=0; l<n; l++)
	{
		p->GetPoint(l,x);
		r = x[0]*x[0] + x[1]*x[1] + x[2]*x[2];
		r = 0.5*pow(2*r/(1.0+r),2.0);
		x[0] *= r;
		x[1] *= r;
		x[2] *= r;
		p->SetPoint(l,x);
	}
	done[PointGlyphType::Cluster] = true;
	
	//
	//  Galaxy
	//
	cc = vtkPointSource::New(); IERROR_CHECK_MEMORY(cc);
	cc->SetNumberOfPoints(30*res);
	cc->SetCenter(0.0,0.0,0.0);
	cc->SetRadius(1.0);
	cc->SetOutput(mData[PointGlyphType::Galaxy]);
	cc->Update();
	cc->Delete();
	//
	//  Rearrange points to have a spiral galaxy in z=0 plane.
	//
	double r1, phi;
	p = mData[PointGlyphType::Galaxy]->GetPoints();
	n = p->GetNumberOfPoints();
	for(l=0; l<n; l++)
	{
		p->GetPoint(l,x);
		r = x[0]*x[0] + x[1]*x[1] + x[2]*x[2];
		r = 0.5*(0.2*(1-r)+2*r)/(1.0+r);
		x[0] *= r;
		x[1] *= r;
		x[2] /= (5.0+20.0*r);

		if(l > 0.7*n)
		{
			r1 = sqrt(x[0]*x[0]+x[1]*x[1]);
			phi = atan2(x[1],x[0]);
			if(phi > vtkMath::Pi()) phi -= 2*vtkMath::Pi();
			if(phi > 0.0) phi = 0.5*vtkMath::Pi(); else phi = -0.5*vtkMath::Pi();
			x[0] = r1*cos(phi-5.0*r1);
			x[1] = r1*sin(phi-5.0*r1);
		}
		p->SetPoint(l,x);
	}
	done[PointGlyphType::Galaxy] = true;
	
	//
	//  Test that all are done
	//
	for(i=0; i<PointGlyphType::SIZE; i++)
	{
		if(!done[i]) vtkErrorMacro("Some of types are left undone.");
	}

	//
	//  Make all points float
	//
	for(i=0; i<PointGlyphType::SIZE; i++)
	{
		op = mData[i]->GetPoints();
		if(op->GetDataType() != VTK_FLOAT)
		{
			n = op->GetNumberOfPoints();
			p = vtkPoints::New(VTK_FLOAT);
			if(p == 0) break;
			p->SetNumberOfPoints(n);
			for(l=0; l<n; l++)
			{
				p->SetPoint(l,op->GetPoint(l));
			}
			mData[i]->SetPoints(p);
			p->Delete();
		}
	}

	//
	//  Make all normals float
	//
	for(i=0; i<PointGlyphType::SIZE; i++)
	{
		da = mData[i]->GetPointData()->GetNormals();
		if(da!=0 && da->GetDataType()!=VTK_FLOAT)
		{
			n = da->GetNumberOfTuples();
			fa = vtkFloatArray::New();
			if(fa == 0) break;
			fa->SetNumberOfComponents(3);
			fa->SetNumberOfTuples(n);
			for(l=0; l<n; l++)
			{
				fa->SetTuple(l,da->GetTuple(l));
			}
			mData[i]->GetPointData()->SetNormals(fa);
			fa->Delete();
		}

		da = mData[i]->GetCellData()->GetNormals();
		if(da!=0 && da->GetDataType()!=VTK_FLOAT)
		{
			n = da->GetNumberOfTuples();
			fa = vtkFloatArray::New();
			if(fa == 0) break;
			fa->SetNumberOfComponents(3);
			fa->SetNumberOfTuples(n);
			for(l=0; l<n; l++)
			{
				fa->SetTuple(l,da->GetTuple(l));
			}
			mData[i]->GetCellData()->SetNormals(fa);
			fa->Delete();
		}
	}
}


void iPointGlyph::ScaleData(vtkPolyData *d, double fac)
{
	double x[3];
	vtkPoints *p = d->GetPoints();
	vtkIdType l, n = p->GetNumberOfPoints();
	//
	//  Scale data
	//
	for(l=0; l<n; l++)
	{
		p->GetPoint(l,x);
		x[0] *= fac;
		x[1] *= fac;
		x[2] *= fac;
		p->SetPoint(l,x);
	}
}


void iPointGlyph::SwapAxes(vtkPolyData *d, int a1, int a2)
{
	double r, x[3];
	vtkPoints *p = d->GetPoints();
	vtkIdType l, n = p->GetNumberOfPoints();
	//
	//  Swap points
	//
	for(l=0; l<n; l++)
	{
		p->GetPoint(l,x);
		r = x[a1]; x[a1] = -x[a2]; x[a2] = r;
		p->SetPoint(l,x);
	}
	//
	//  Don't forget to rotate normals too (if they exist)
	//
	vtkDataArray *da = d->GetPointData()->GetNormals();
	if(da != 0)
	{
		n = da->GetNumberOfTuples();
		for(l=0; l<n; l++)
		{
			da->GetTuple(l,x);
			r = x[0]; x[0] = -x[2]; x[2] = r;
			da->SetTuple(l,x);
		}
	}
	da = d->GetCellData()->GetNormals();
	if(da != 0)
	{
		n = da->GetNumberOfTuples();
		for(l=0; l<n; l++)
		{
			da->GetTuple(l,x);
			r = x[0]; x[0] = -x[2]; x[2] = r;
			da->SetTuple(l,x);
		}
	}
}


void iPointGlyph::ShiftData(vtkPolyData *d, int a, double dx)
{
	double x[3];
	vtkPoints *p = d->GetPoints();
	vtkIdType l, n = p->GetNumberOfPoints();
	//
	//  Shift the data
	//
	for(l=0; l<n; l++)
	{
		p->GetPoint(l,x);
		x[a] += dx;
		p->SetPoint(l,x);
	}
}


bool iPointGlyph::GetOriented() const
{
	return iPointGlyph::GetOriented(mType);
}


const char* iPointGlyph::GetName() const
{
	return iPointGlyph::GetName(mType);
}


bool iPointGlyph::GetOriented(int t)
{
	if(t>=0 && t<PointGlyphType::SIZE)
	{
		return type[t].IsOriented;
	}
	else return false;
}


const char* iPointGlyph::GetName(int t)
{
	if(t>=0 && t<PointGlyphType::SIZE)
	{
		return type[t].Name;
	}
	else return 0;
}


void iPointGlyph::SetType(int t)
{
	if(t>=0 && t<PointGlyphType::SIZE)
	{
		mType = t;
		this->Modified();
	}
}


vtkPolyData* iPointGlyph::GetData()
{
	return this->vtkPolyDataAlgorithm::GetOutput();
}


int iPointGlyph::RequestData(vtkInformation *request, vtkInformationVector **inputVector, vtkInformationVector *outputVector)
{
	this->GetData()->ShallowCopy(mData[mType]);
	return 1;
}

