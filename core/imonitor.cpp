/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "imonitor.h"


#include "ierror.h"
#include "iobject.h"
#include "ishell.h"
#include "ishelleventobservers.h"


//
//  Convenience class
//
iMonitor::iMonitor(const iShellComponent *user)
{
	IASSERT(user);
	mObserver = user->GetShell()->GetExecutionEventObserver(); IASSERT(mObserver);
	mObserver->Start();
	mObserver->SetInterval(0.0,1.0);
}

iMonitor::iMonitor(const iShellComponent *user, const iString& label, double length)
{
	IASSERT(user);
	mObserver = user->GetShell()->GetExecutionEventObserver(); IASSERT(mObserver);
	mObserver->Start();
	mObserver->SetLabel(label);
	mObserver->SetInterval(0.0,length);
}


iMonitor::~iMonitor()
{
	IASSERT(mObserver);
	mObserver->Finish();
}


void iMonitor::SetInterval(double start, double length)
{
	IASSERT(mObserver);
	mObserver->SetInterval(start,length);
}


void iMonitor::SetInterval(const iString& label, double start, double length)
{
	IASSERT(mObserver);
	mObserver->SetLabel(label);
	mObserver->SetInterval(start,length);
}


void iMonitor::SetProgress(double prog)
{
	IASSERT(mObserver);
	mObserver->SetProgress(prog);
}


bool iMonitor::IsStopped() const
{
	IASSERT(mObserver);
	return (mObserver->IsAborted() || mObserver->IsInError());
}


bool iMonitor::IsAborted() const
{
	IASSERT(mObserver);
	return mObserver->IsAborted();
}


void iMonitor::PostError(const iString &text)
{
	IASSERT(mObserver);
	mObserver->PostError(text);
}


void iMonitor::Reset()
{
	IASSERT(mObserver);
	mObserver->Reset();
}


const iString& iMonitor::LastErrorMessage() const
{
	IASSERT(mObserver);
	return mObserver->LastErrorMessage();
}
