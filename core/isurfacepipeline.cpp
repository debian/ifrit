/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "isurfacepipeline.h"


#include "iboundedplanesource.h"
#include "iboundedspheresource.h"
#include "icontourfilter.h"
#include "ierror.h"
#include "iflipnormalsfilter.h"
#include "iidentityfilter.h"
#include "ioptimizepolydatafilter.h"
#include "iprobefilter.h"
#include "ireducepolydatafilter.h"
#include "ireducepolydatafilter2.h"
#include "ireplicatedpolydata.h"
#include "ishiftpolydatafilter.h"
#include "ismoothpolydatafilter.h"
#include "isurfaceviewsubject.h"
#include "ivtktype.h"

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"
#include "igenericfilter.tlh"
#include "iviewsubjectpipeline.tlh"


using namespace iParameter;


iPipelineKeyDefineMacro(iSurfacePipeline,Method);
iPipelineKeyDefineMacro(iSurfacePipeline,IsoVar);
iPipelineKeyDefineMacro(iSurfacePipeline,IsoLevel);
iPipelineKeyDefineMacro(iSurfacePipeline,IsoMethod);
iPipelineKeyDefineMacro(iSurfacePipeline,IsoPipeline);
iPipelineKeyDefineMacro(iSurfacePipeline,NormalsFlipped);
iPipelineKeyDefineMacro(iSurfacePipeline,PlaneDirection);
iPipelineKeyDefineMacro(iSurfacePipeline,PlaneResolution);
iPipelineKeyDefineMacro(iSurfacePipeline,SphereSize);
iPipelineKeyDefineMacro(iSurfacePipeline,SphereResolution);
iPipelineKeyDefineMacro(iSurfacePipeline,ObjectPosition);


//
// iSurfacePipeline class
//
iSurfacePipeline::iSurfacePipeline(iSurfaceViewInstance *s) : iReplicatedViewSubjectPipeline(s,1)
{
	mSpecialVar = 0;
	mOwner = s;

	//
	//  Do VTK stuff
	//	
	mIsoFilter = this->CreateFilter<iContourFilter>();
	mSmoothFilter = this->CreateFilter<iSmoothPolyDataFilter>();
	mReduceFilter = this->CreateFilter<iReducePolyDataFilter>();
	mReduce2Filter = this->CreateFilter<iReducePolyDataFilter2>();
	mOptimizeFilter = this->CreateFilter<iOptimizePolyDataFilter>();

	mSphereSource = this->CreateFilter<iBoundedSphereSource>();
	mPlaneSource = this->CreateFilter<iBoundedPlaneSource>();

	mProbeFilter = this->CreateFilter<iProbeFilter>();
	mFlipNormalsFilter = this->CreateFilter<iFlipNormalsFilter>();

	//mProbeFilter->RestrictToType(ivtkType::Name<vtkPolyData>());
	mFlipNormalsFilter->SetInputConnection(mProbeFilter->GetOutputPort());

	mSmoothFilter->BoundarySmoothingOff();
	mSmoothFilter->NonManifoldSmoothingOn();
	mSmoothFilter->FeatureEdgeSmoothingOn();

	this->CompletePipeline(mFlipNormalsFilter->GetOutputPort());
	this->UpdateContents();
}


iSurfacePipeline::~iSurfacePipeline()
{
}


vtkAlgorithm* iSurfacePipeline::GetIsoFilter() const
{
	return mIsoFilter;
}


bool iSurfacePipeline::PrepareInput()
{
	vtkDataSet *input = this->InputData();

	if(mIsoFilter->InputData() != input)
	{
		mIsoFilter->SetInputData(input);
		mProbeFilter->SetSourceData(input);
	}

	//
	//  Check whether fixed probe surfaces need to be adjusted to correspond to the input resolution
	//
	bool redo = false;
	if(mSphereSource->GetMTime() < input->GetMTime())
	{
		this->UpdateSphereResolution();
		redo = true;
	}
	if(mPlaneSource->GetMTime() < input->GetMTime())
	{
		this->UpdatePlaneResolution();
		redo = true;
	}

	if(redo)
	{
		double b[6];
		input->GetBounds(b);
		mPlaneSource->SetBounds(b[0],b[1],b[2],b[3],b[4],b[5]);
		mSphereSource->SetBounds(b[0],b[1],b[2],b[3],b[4],b[5]);
	}

	return true;
}


void iSurfacePipeline::UpdateIsoPipeline()
{ 
	switch(mOwner->GetIsoReduction())
	{
	case 1:
		{
			mReduceFilter->SetTargetReduction(0.75);
			mReduce2Filter->SetTargetReduction(0.75);
			break;
		}
	case 2:
		{
			mReduceFilter->SetTargetReduction(0.90);
			mReduce2Filter->SetTargetReduction(0.90);
			break;
		}
	case 3:
		{
			mReduceFilter->SetTargetReduction(0.99);
			mReduce2Filter->SetTargetReduction(0.99);
			break;
		}
	}

	mSmoothFilter->SetNumberOfIterations(2<<(2*mOwner->GetIsoSmoothing()));

	//
	//  Create the pipeline
	//
	vtkAlgorithmOutput *step1, *step2;

	if(mOwner->GetIsoReduction() > 0) 
	{
		mReduceFilter->SetInputConnection(this->GetIsoFilter()->GetOutputPort());
		mReduce2Filter->SetInputConnection(this->GetIsoFilter()->GetOutputPort());
		step1 = mOwner->GetAlternativeReductionMethod() ? mReduce2Filter->GetOutputPort() : mReduceFilter->GetOutputPort();
	}
	else
	{
		step1 = this->GetIsoFilter()->GetOutputPort();
	}

	mSmoothFilter->SetInputConnection(step1);

	if(mOwner->GetIsoSmoothing() > 0)
	{
		step2 = mSmoothFilter->GetOutputPort();
	}
	else
	{
		step2 = step1;
	}

	mOptimizeFilter->SetInputConnection(step2);
	mOptimizeFilter->SetEnabled(mOwner->GetIsoOptimization());

	this->Modified();
}


void iSurfacePipeline::UpdateIsoVar()
{
	mIsoFilter->SetCurrentVar(mOwner->GetIsoVar());
	this->Modified();
}


void iSurfacePipeline::UpdateIsoLevel()
{ 
	mIsoFilter->SetLevel(mOwner->GetIsoLevel());
	this->Modified();
}


void iSurfacePipeline::UpdateIsoMethod()
{ 
	mIsoFilter->SetMethod(mOwner->GetIsoMethod());
	this->Modified();
}


void iSurfacePipeline::UpdateNormalsFlipped()
{ 
	int var = mOwner->GetIsoVar();
	bool s = mOwner->GetNormalsFlipped();

	if(mOwner->GetMethod() != SurfaceMethod::Isosurface) var = -1;

	if((s && var!=mSpecialVar) || (!s && var==mSpecialVar)) 
	{
		mFlipNormalsFilter->ReverseNormalsOn(); 
		mFlipNormalsFilter->ReverseCellsOn();
	} 
	else 
	{
		mFlipNormalsFilter->ReverseNormalsOff();
		mFlipNormalsFilter->ReverseCellsOff();
	}
	this->Modified();
}


void iSurfacePipeline::UpdateMethod()
{
	switch(mOwner->GetMethod())
	{
	case SurfaceMethod::Isosurface:
		{
			this->UpdateIsoPipeline();
			mProbeFilter->SetInputConnection(mSmoothFilter->GetOutputPort());
			this->Modified();
			break;
		}
	case SurfaceMethod::Sphere:
		{
			mProbeFilter->SetInputConnection(mSphereSource->GetOutputPort());
			this->Modified();
			break;
		}
	case SurfaceMethod::Plane:
		{
			mProbeFilter->SetInputConnection(mPlaneSource->GetOutputPort());
			this->Modified();
			break;
		}
	}
	this->UpdateNormalsFlipped();
}


void iSurfacePipeline::UpdateSphereSize()
{
	mSphereSource->SetRadius(mOwner->GetSize());
	this->UpdateSphereResolution();
	this->Modified();
}


void iSurfacePipeline::UpdateObjectPosition()
{
	mSphereSource->SetCenter(mOwner->GetPosition());
	mPlaneSource->SetCenter(mOwner->GetPosition());
	this->Modified();
}


void iSurfacePipeline::UpdatePlaneDirection()
{
	const iVector3D &n(mOwner->GetDirection());
	mPlaneSource->SetNormal(n);
	this->Modified();
}


void iSurfacePipeline::UpdateSphereResolution()
{
	double dx;

	vtkDataSet *input = this->InputData();
	if(input == 0) return;

	if(input->IsA("vtkImageData"))
	{
		double spa[3];
		iRequiredCast<vtkImageData>(INFO,input)->GetSpacing(spa);
		dx = spa[0];
		if(dx < spa[1]) dx = spa[1];
		if(dx < spa[2]) dx = spa[2];
		if(dx < 1.0e-3) dx = 1.0e-3;
	}
	else dx = 0.01;

	float s = mSphereSource->GetRadius();
	int n = iMath::Round2Int(0.5*3.14*s/dx); // 0.5 is from diretc comparison with the x-section
	if(n < 6) n = 6;
	mSphereSource->SetResolution(n);
	this->Modified();
}


void iSurfacePipeline::UpdatePlaneResolution()
{
	double dx;

	vtkDataSet *input = this->InputData();
	if(input == 0) return;

	if(input->IsA("vtkImageData"))
	{
		double spa[3];
		iRequiredCast<vtkImageData>(INFO,input)->GetSpacing(spa);
		dx = spa[0];
		if(dx < spa[1]) dx = spa[1];
		if(dx < spa[2]) dx = spa[2];
		if(dx < 1.0e-3) dx = 1.0e-3;
	}
	else dx = 0.01;

	float s = 7.0;
	int n = iMath::Round2Int(s/dx);
	mPlaneSource->SetResolution(n);
	this->Modified();
}
