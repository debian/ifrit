/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ivolumedataconverter.h"


#include "idata.h"
#include "idatalimits.h"
#include "imath.h"
#include "iparallel.h"
#include "istretch.h"
#include "iviewmodule.h"
#include "iviewsubject.h"

#include <vtkImageData.h>
#include <vtkPointData.h>

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"
#include "igenericfilter.tlh"


using namespace iParameter;


iVolumeDataConverter::iVolumeDataConverter(iDataConsumer *consumer) : iGenericImageDataFilter<vtkImageAlgorithm>(consumer,1,true), iParallelWorker(consumer->GetViewModule()->GetParallelManager())
{
	mCurVar = 0;
}


void iVolumeDataConverter::SetCurrentVar(int n)
{ 
	if(n>=0 && n!= mCurVar)
	{
		mCurVar = n;
		this->Modified();
	}
}


void iVolumeDataConverter::OnLimitsChanged(int var, int mode)
{
	if(var==mCurVar && (mode==LimitsChangeMode::Range || mode==LimitsChangeMode::Stretch))
	{
		this->Modified();
	}
}


void iVolumeDataConverter::ProvideOutput()
{
	vtkImageData *input = this->InputData();
	vtkImageData *output = this->OutputData();
	int dims[3];
	double pos[3];
	
	output->Initialize();

	wNumComp = input->GetPointData()->GetScalars()->GetNumberOfComponents();
	if(wNumComp == 0) return;

	if(wNumComp != this->GetLimits()->GetNumVars())
	{
		vtkErrorMacro("Wrong number of components in the input data");
		return;
    }
	if(mCurVar<0 || mCurVar>=wNumComp)
	{
		return; 
    }
	
	vtkDebugMacro(<< "Converting StrucuturedPoints to char with limits");

	input->GetDimensions(dims);
	output->SetDimensions(dims);
	
	wSize = (vtkIdType)dims[0]*dims[1]*dims[2];
	
	input->GetSpacing(pos);
	output->SetSpacing(pos);
	
	input->GetOrigin(pos);
	output->SetOrigin(pos);
	
#ifdef IVTK_5
	output->SetScalarType(VTK_UNSIGNED_CHAR);
	output->SetNumberOfScalarComponents(1);
	output->AllocateScalars();
#else
	output->AllocateScalars(VTK_UNSIGNED_CHAR,1);
#endif

	wInPtr = (float *)input->GetPointData()->GetScalars()->GetVoidPointer(0) + mCurVar;
	wOutPtr = (unsigned char *)output->GetScalarPointer();
	
	wStretch = this->GetLimits()->GetStretchId(mCurVar);
	wFoffset = iStretch::Apply(this->GetLimits()->GetMin(mCurVar),wStretch,false);
	wFscale = 255.0f/(iStretch::Apply(this->GetLimits()->GetMax(mCurVar),wStretch,true)-wFoffset+1.0e-30f);

	if(this->ParallelExecute(0) != 0)
	{
		vtkErrorMacro("Parallel execution error in iVolumeDataConverter");
		this->SetAbortExecute(1);
		return;
	}
}


int iVolumeDataConverter::ExecuteStep(int, iParallel::ProcessorInfo &p)
{
	float f;
	vtkIdType l, lBeg, lEnd, lStp;
	iParallel::SplitRange(p,wSize,lBeg,lEnd,lStp);

	for(l=lBeg; l<lEnd; l++)
	{
		if(l%1000 == 0)
		{
			if(p.IsMaster()) this->UpdateProgress(double(l-lBeg)/lEnd);
			if(this->GetAbortExecute()) break;
		}

		f = wFscale*(iStretch::Apply(wInPtr[l*wNumComp],wStretch,false)-wFoffset);
		if(f < 0.0f) f = 0.0f;
		if(f > 255.0f) f = 255.0f;
		wOutPtr[l] = (unsigned char)iMath::Round2Int(f);
	}
	return 0;
}

