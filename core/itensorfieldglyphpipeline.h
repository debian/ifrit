/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef ITENSORFIELDGLYPHPIPELINE_H
#define ITENSORFIELDGLYPHPIPELINE_H


#include "ifieldglyphpipeline.h"


class iPointGlyph;
class iTensorGlyphFilter;
class iTensorFieldViewInstance;


class iTensorFieldGlyphPipeline : public iFieldGlyphPipeline
{

	friend class iTensorFieldViewInstance;

public:

	vtkTypeMacro(iTensorFieldGlyphPipeline,iFieldGlyphPipeline);

	iPipelineKeyDeclareMacro(iTensorFieldGlyphPipeline,Scaling);
	iPipelineKeyDeclareMacro(iTensorFieldGlyphPipeline,Painting);
	iPipelineKeyDeclareMacro(iTensorFieldGlyphPipeline,GlyphType);

protected:
	
	iTensorFieldGlyphPipeline(iTensorFieldViewInstance *owner);
	virtual ~iTensorFieldGlyphPipeline();

	virtual void SetGlyphFilterInput(vtkDataSet *input);
	virtual void SetGlyphFilterScale(double s);

	iTensorFieldViewInstance *mOwner;

	//
	//  VTK stuff
	//
	iPointGlyph *mGlyph;
	iTensorGlyphFilter *mGlyphFilter;
};

#endif // ITENSORFIELDGLYPHPIPELINE_H

