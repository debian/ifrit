/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ivectorglyphfilter.h"


#include "ierror.h"

#include <vtkFloatArray.h>
#include <vtkCellArray.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>

//
//  Templates
//
#include "igenericfilter.tlh"


iVectorGlyphFilter::iVectorGlyphFilter(iDataConsumer *consumer) : iGenericFilter<vtkHedgeHog,vtkDataSet,vtkPolyData>(consumer,1,true)
{
	mIncludeVerts = false;
}

	
void iVectorGlyphFilter::SetIncludeVerts(bool s)
{
	if(mIncludeVerts != s)
	{
		mIncludeVerts = s;
		this->Modified();
	}
}


void iVectorGlyphFilter::ProvideOutput()
{
	this->ExecuteParent();

	if(!mIncludeVerts) return;

	vtkPolyData *output = this->OutputData();

	vtkPoints *pts = output->GetPoints();
	if(pts == 0) return;

	vtkCellArray *verts = vtkCellArray::New(); IERROR_CHECK_MEMORY(verts);
	vtkIdType i, n = pts->GetNumberOfPoints()/2; // vtkHedheHog places end points of glyphs in the second half of the point array
	vtkIdType *ptr = verts->WritePointer(n,2*n);
	for(i=0; i<n; i++)
	{
		ptr[2*i+0] = 1;
		ptr[2*i+1] = i;
	}

	output->SetVerts(verts);
	verts->Delete();
}

