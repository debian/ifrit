/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iviewsubjectpipelinedatamanager.h"


#include "ierror.h"
#include "iviewsubject.h"

#include <vtkDataSet.h>

//
//  Templates
//
#include "iarray.tlh"


//
//  iViewSubjectPipelineDataDistributor class
//
iViewSubjectPipelineDataDistributor::iViewSubjectPipelineDataDistributor(iViewSubjectPipelineDataManager *manager, const iString &type)
{
	IASSERT(manager);
	
	mManager = manager;
	mType = type;
}


iViewSubjectPipelineDataDistributor::~iViewSubjectPipelineDataDistributor()
{
}


bool iViewSubjectPipelineDataDistributor::DistributeData(vtkDataSet *globalInput, iArray<vtkDataSet*> &localInputs)
{
	if(globalInput==0 || !globalInput->IsA(mType.ToCharPointer())) return false;

	return this->DistributeDataBody(globalInput,localInputs);
}


//
//  iViewSubjectPipelineDataCollector class
//
iViewSubjectPipelineDataCollector::iViewSubjectPipelineDataCollector(iViewSubjectPipelineDataManager *manager, const iString &type)
{
	IASSERT(manager);
	
	mManager = manager;
	mType = type;
}


iViewSubjectPipelineDataCollector::~iViewSubjectPipelineDataCollector()
{
}


bool iViewSubjectPipelineDataCollector::CollectData(iArray<vtkDataSet*> &localOutputs, vtkDataSet *globalOutput)
{
	if(globalOutput==0 || !globalOutput->IsA(mType.ToCharPointer())) return false;

	int i;
	for(i=0; i<localOutputs.Size(); i++)
	{
		if(localOutputs[i]==0 || !localOutputs[i]->IsA(mType.ToCharPointer())) return false;
	}

	return this->CollectDataBody(localOutputs,globalOutput);
}


//
//  iViewSubjectPipelineDataManager class
//
iViewSubjectPipelineDataManager* iViewSubjectPipelineDataManager::New(iViewInstance *owner)
{
	return new iViewSubjectPipelineDataManager(owner);
}


iViewSubjectPipelineDataManager::iViewSubjectPipelineDataManager(iViewInstance *owner) : iDataHandler(owner)
{
	mOwner = owner;
}


iViewSubjectPipelineDataManager::~iViewSubjectPipelineDataManager()
{
	while(mDistributors.Size() > 0) mDistributors.RemoveLast()->Delete();
	while(mCollectors.Size() > 0) mCollectors.RemoveLast()->Delete();
}


void iViewSubjectPipelineDataManager::DistributeData(vtkDataSet *globalInput, iArray<vtkDataSet*> &localInputs)
{
	int i;
	for(i=0; i<mDistributors.Size(); i++)
	{
		if(mDistributors[i]->DistributeData(globalInput,localInputs)) return;
	}

	IBUG_WARN("Unable to find a helper to distribute data in parallel. IFrIT will fall back to serial execution.");

	//
	//  Channel all data into the master pipeline
	//
	for(i=0; i<localInputs.Size(); i++)
	{
		localInputs[i] = globalInput->NewInstance();
	}
	localInputs[0]->ShallowCopy(globalInput);
}


void iViewSubjectPipelineDataManager::CollectData(iArray<vtkDataSet*> &localOutputs, vtkDataSet *globalOutput)
{
	int i;
	for(i=0; i<mCollectors.Size(); i++)
	{
		if(mCollectors[i]->CollectData(localOutputs,globalOutput)) return;
	}

	IBUG_WARN("Misconfiguration in iViewSubjectPipelineDataManager: unable to find a helper to collect the data.");

	//
	//  Use the master pipeline output
	//
	globalOutput->ShallowCopy(localOutputs[0]);
}


const iViewSubjectPipelineDataDistributor* iViewSubjectPipelineDataManager::GetDistributor(const iString &type) const
{
	int i;
	for(i=0; i<mDistributors.Size(); i++)
	{
		if(mDistributors[i]->GetType() == type) return mDistributors[i];
	}
	return 0;
}


const iViewSubjectPipelineDataCollector* iViewSubjectPipelineDataManager::GetCollector(const iString &type) const
{
	int i;
	for(i=0; i<mCollectors.Size(); i++)
	{
		if(mCollectors[i]->GetType() == type) return mCollectors[i];
	}
	return 0;
}


void iViewSubjectPipelineDataManager::AddDistributor(iViewSubjectPipelineDataDistributor *distributor)
{
	if(distributor != 0) mDistributors.Add(distributor);
}


void iViewSubjectPipelineDataManager::AddCollector(iViewSubjectPipelineDataCollector *collector)
{
	if(collector != 0) mCollectors.Add(collector);
}

