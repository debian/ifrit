/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iparticlepipeline.h"


#include "ierror.h"
#include "iparticledataconverter.h"
#include "iparticleviewsubject.h"
#include "ireplicatedpolydata.h"

//
//  Templates
//
#include "iarray.tlh"
#include "igenericfilter.tlh"
#include "iviewsubjectpipeline.tlh"


iPipelineKeyDefineMacro(iParticlePipeline,Type);
iPipelineKeyDefineMacro(iParticlePipeline,Scaling);
iPipelineKeyDefineMacro(iParticlePipeline,FixedSize);

	
//
// iParticlePipeline class
//
iParticlePipeline::iParticlePipeline(iParticleViewInstance *owner) : iReplicatedViewSubjectPipeline(owner,1)
{
	mOwner = owner;

	//
	//  Do VTK stuff
	//	
	mDataConverter = this->CreateFilter<iParticleDataConverter>();

	this->CompletePipeline(mDataConverter->GetOutputPort());
	this->UpdateContents();
}


iParticlePipeline::~iParticlePipeline()
{
}


bool iParticlePipeline::PrepareInput()
{
	vtkPolyData *input = vtkPolyData::SafeDownCast(this->InputData());
	if(input == 0)
	{
		IBUG_ERROR("iParticlePipeline is configured incorrectly.");
		return false;
	}

	if(mDataConverter->InputData() != input)
	{
		mDataConverter->SetInputData(input);
	}
	
	return true;
}


void iParticlePipeline::UpdateScaling()
{ 
	mDataConverter->Modified();
	this->Modified();
}


void iParticlePipeline::UpdateType()
{ 
	mDataConverter->SetType(mOwner->GetType());
	this->Modified();
}


void iParticlePipeline::UpdateFixedSize()
{ 
	mDataConverter->SetSize(mOwner->GetFixedSize());
	this->Modified();
}

