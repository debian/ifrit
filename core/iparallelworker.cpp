/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iparallelworker.h"


#include "ierror.h"
#include "iparallel.h"
#include "iparallelmanager.h"
#include "ishell.h"
#include "ishelleventobservers.h"
#include "iviewmodulecomponent.h"

#include <vtkAlgorithm.h>


using namespace iParallel;


iParallelWorker::iParallelWorker(iParallelManager *pm)
{
	mManager = pm; IASSERT(mManager);
	mSerial = false;
}


iParallelWorker::~iParallelWorker()
{
}


iParallelManager* iParallelWorker::GetManager() const
{
	return mManager;
}


int iParallelWorker::ParallelExecute(int step)
{
	int ret = 0;

	if(mSerial || mManager==0)
	{
		ProcessorInfo p;
		ret = this->ExecuteStep(step,p);
	}
	else
	{
		wStep = step;
		ret = mManager->ExecuteWorker(this);
	}

	if(ret != 0) IBUG_WARN("Failure in iParallelWorker::ParallelExecute.");
	return ret;
}


int iParallelWorker::ExecuteInternal(ProcessorInfo &p)
{
	//
	//  p is guaranteed to have the correct value for ThisProc and NumProcs (0<=ThisProc<NumProcs)
	//
	return this->ExecuteStep(wStep,p);
}
