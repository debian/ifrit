/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "idataformatter.h"


#include "idatalimits.h"

#include <vtkMath.h>


//
//  Templates
//
#include "iarray.tlh"


iDataFormatter* iDataFormatter::New(iViewModule *vm)
{
	IASSERT(vm);
	return new iDataFormatter(vm);
}


iDataFormatter::iDataFormatter(iViewModule *vm) : iViewModuleComponent(vm)
{
	mFormatHelper = "                                                          ";
}


iDataFormatter::~iDataFormatter()
{
}


void iDataFormatter::AddOneLine(const iString &name, const iString &data, const iString &unit)
{
	mVarNameLines.Add(name);
	mVarDataLines.Add(data);
	mVarUnitLines.Add(unit);
}


void iDataFormatter::GetReport(int i, iString& varName, iString &varData, iString &varUnit) const
{
	if(i>=0 && i<mVarNameLines.Size())
	{
		varName = mVarNameLines[i];
		varData = mVarDataLines[i];
		varUnit = mVarUnitLines[i];
	}
}


void iDataFormatter::ClearReport()
{
	mVarNameLines.Clear();
	mVarDataLines.Clear();
	mVarUnitLines.Clear();
}


void iDataFormatter::FormatScalarData(iDataLimits *lim, int n, float *v, bool useRank)
{
	int i;
	iString s1, s2;

	if(lim==0 || n<1 || v==0) return; 

	float vlog;
	int maxLen = 0;
	for(i=0; i<n; i++) if(!useRank || lim->GetRank(i)==0)
	{
		s2 = iString::FromNumber(v[i],"%.3e");
		if(maxLen < s2.Length()) maxLen = s2.Length();
	}
	while(mFormatHelper.Length() < maxLen) mFormatHelper += "          ";

	for(i=0; i<n; i++) if(!useRank || lim->GetRank(i)==0)
	{
		vlog = float(iMath::Log10(1.0e-30+fabs(v[i])));
		s2 = iString::FromNumber(v[i],"%.3e");
		if(s2.Length() < maxLen) s2 += mFormatHelper.Part(0,maxLen-s2.Length());
		s1 = s2 + " (" + iString::FromNumber(vlog,"% 5.2f") + " dex)";
		this->AddOneLine(lim->GetName(i),s1,lim->GetUnit(i));
	}
}


void iDataFormatter::FormatVectorData(iDataLimits *lim, int ivar, float *v, float *div, float *vort)
{
	int i, maxLen;
	iString s1, s2, unit;

	if(lim==0 || v==0) return; 

	if(ivar>=0 && ivar<lim->GetNumVars() && !lim->GetUnit(ivar).IsEmpty()) unit = lim->GetUnit(ivar);

	//
	//  Divergence and vorticity
	//
	if(div != 0)
	{
		s1 = iString::FromNumber(div[0],"%.3e");
	}

	if(vort != 0)
	{
		float vortVal = 0.0;
		for(i=0; i<3; i++) vortVal += vort[i]*vort[i];
		vortVal = float(sqrt(vortVal));
		s2 = iString::FromNumber(vortVal,"%.3e");
	}

	maxLen = s1.Length();
	if(s2.Length() > maxLen) maxLen = s2.Length();
	while(mFormatHelper.Length() < maxLen) mFormatHelper += "          ";

	if(!s1.IsEmpty()) this->AddOneLine("Divergence",s1+mFormatHelper.Part(0,maxLen-s1.Length()),unit);
	if(!s2.IsEmpty()) this->AddOneLine("Vorticity",s2+mFormatHelper.Part(0,maxLen-s2.Length()),unit);

	//
	//  Vector components
	//
	s1 = "( ";
	for(i=0; i<3; i++)
	{
		s1 += iString::FromNumber(v[i],"%.3e");
		if(i < 2) s1 += " , ";
	}
	s1 += " )";
	this->AddOneLine((ivar<0 || ivar>=lim->GetNumVars())?"Vector components":lim->GetNameForClass(ivar),s1,unit);
}


void iDataFormatter::FormatTensorData(iDataLimits *lim, int ivar, float *v, bool compressed)
{
	int i, j;
	iString s1, s2, unit;

	if(lim==0 || v==0) return; 

	if(ivar>=0 && ivar<lim->GetNumVars() && !lim->GetUnit(ivar).IsEmpty()) unit = lim->GetUnit(ivar);

	//
	// Compute the eigenvalues
	//
	float *mat[3], eig[3], *eiv[3], tmp;
	float mat0[3], mat1[3], mat2[3];
	float eiv0[3], eiv1[3], eiv2[3];
	//
	// Set up working matrices
	//
	mat[0] = mat0; mat[1] = mat1; mat[2] = mat2; 
	eiv[0] = eiv0; eiv[1] = eiv1; eiv[2] = eiv2;
	if(compressed)
	{
		mat[0][0] = *(v+0); mat[0][1] = *(v+1); mat[0][2] = *(v+2); 
		mat[1][0] = *(v+1); mat[1][1] = *(v+3); mat[1][2] = *(v+4); 
		mat[2][0] = *(v+2); mat[2][1] = *(v+4); mat[2][2] = *(v+5); 
	}
	else
	{
		mat[0][0] = *(v+0); mat[0][1] = *(v+1); mat[0][2] = *(v+2); 
		mat[1][0] = *(v+3); mat[1][1] = *(v+4); mat[1][2] = *(v+5); 
		mat[2][0] = *(v+6); mat[2][1] = *(v+7); mat[2][2] = *(v+8); 
	}
	vtkMath::Jacobi(mat,eig,eiv);
	//
	// Order eigenvalues
	//
	for(i=0; i<2; i++)
	{
		for(j=i+1; j<3; j++)
		{
            if(eig[i] > eig[j])
			{
				tmp = eig[i];
				eig[i] = eig[j];
				eig[j] = tmp;
			}
		}
	}

	int maxLen = 0;
	for(i=0; i<3; i++)
	{
		s2 = iString::FromNumber(eig[i],"%.3e");
		if(maxLen < s2.Length()) maxLen = s2.Length();
	}
	while(mFormatHelper.Length() < maxLen) mFormatHelper += "          ";

	for(i=0; i<3; i++)
	{
		s2 = iString::FromNumber(eig[i],"%.3e");
		this->AddOneLine("Eigenvalue #"+iString::FromNumber(i),s2+mFormatHelper.Part(0,maxLen-s2.Length()),unit);
	}

	//
	//  Tensor components
	//
	maxLen = 0;
	for(j=0; j<3; j++)
	{
		for(i=0; i<3; i++)
		{
			s2 = iString::FromNumber(mat[j][i],"%.3e");
			if(maxLen < s2.Length()) maxLen = s2.Length();
		}
	}
	while(mFormatHelper.Length() < maxLen) mFormatHelper += "          ";

	for(j=0; j<3; j++)
	{
		s1 = " ";
		for(i=0; i<3; i++)
		{
			s2 = iString::FromNumber(mat[j][i],"%.3e");
			if(s2.Length() < maxLen) s2 += mFormatHelper.Part(0,maxLen-s2.Length());
			s1 += s2;
			s1 += "  ";
		}
		if(j == 1) 
		{
			this->AddOneLine((ivar<0 || ivar>=lim->GetNumVars())?"Tensor components":lim->GetName(ivar),s1,unit);
		}
		else
		{
			this->AddOneLine("",s1,unit);
		}
	}
} 

