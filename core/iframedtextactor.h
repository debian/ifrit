/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IFRAMEDTEXTACTOR_H
#define IFRAMEDTEXTACTOR_H


#include "itextactor.h"


class vtkActor2D;
class vtkPoints;


class iFramedTextActor: public iTextActor
{
	
public:
	
	vtkTypeMacro(iFramedTextActor,iTextActor);
	static iFramedTextActor* New(iRenderTool *rt = 0);
	
	void SetTransparent(bool s);
	inline bool IsTransparent() const { return mTransparent; }

protected:
	
	iFramedTextActor(iRenderTool *rv);
	virtual ~iFramedTextActor();
	
	virtual void UpdateGeometry(vtkViewport *vp);

	float wBounds[4];

private:
	
	bool mTransparent;
	float wTrueBounds[4];

	vtkPoints           *mBorderPoints;
	vtkActor2D          *mBorderActor;
	vtkActor2D          *mLiningActor;
};

#endif // IFRAMEDTEXTACTOR_H
