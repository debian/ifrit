/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ivectorfieldstreamlinepipeline.h"


#include "idatalimits.h"
#include "ierror.h"
#include "ilinetobandfilter.h"
#include "ilinetotubefilter.h"
#include "ireplicatedpolydata.h"
#include "istreamlinefilter.h"
#include "ivectorfieldviewsubject.h"

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"
#include "igenericfilter.tlh"
#include "iviewsubjectpipeline.tlh"


using namespace iParameter;


iPipelineKeyDefineMacro(iVectorFieldStreamLinePipeline,Pipeline);
iPipelineKeyDefineMacro(iVectorFieldStreamLinePipeline,LineLength);
iPipelineKeyDefineMacro(iVectorFieldStreamLinePipeline,LineDirection);
iPipelineKeyDefineMacro(iVectorFieldStreamLinePipeline,LineQuality);
iPipelineKeyDefineMacro(iVectorFieldStreamLinePipeline,TubeSize);
iPipelineKeyDefineMacro(iVectorFieldStreamLinePipeline,TubeRangeFactor);
iPipelineKeyDefineMacro(iVectorFieldStreamLinePipeline,TubeVariationFactor);


//
//  iVectorFieldStreamLinePipeline class
//
iVectorFieldStreamLinePipeline::iVectorFieldStreamLinePipeline(iVectorFieldViewInstance *owner) : iReplicatedViewSubjectPipeline(owner,2)
{
	mOwner = owner;

	//
	//  Do VTK stuff
	//	
	mLine = this->CreateFilter<iStreamLineFilter>();
	mTubeFilter = this->CreateFilter<iLineToTubeFilter>();
	mBandFilter = this->CreateFilter<iLineToBandFilter>();
	
	mTubeFilter->SetNumberOfSides(6);
	mTubeFilter->SetVaryRadiusToVaryRadiusByScalar();

	mBandFilter->SetDistanceFactor(15.0);
	mBandFilter->SetRuledModeToPointWalk();
	mBandFilter->SetOffset(0);
	mBandFilter->SetOnRatio(2);
	mBandFilter->PassLinesOn();

	this->UpdateContents();
}


iVectorFieldStreamLinePipeline::~iVectorFieldStreamLinePipeline()
{
}


bool iVectorFieldStreamLinePipeline::PrepareInput()
{
	vtkDataSet *input = this->InputData(1);   // input(0) for Pipeline(1) is streamline sources for parallel execution
	vtkDataSet *source = this->InputData(0);

	if(input==0 || source==0)
	{
		return false;
	}

	if(mLine->SourceData() != source)
	{
		mLine->SetSourceData(source);
	}
	if(mLine->InputData() != input)
	{
		mLine->SetInputData(input);
	}

	return true;
}


void iVectorFieldStreamLinePipeline::UpdatePipeline()
{
	switch(mOwner->GetMethod())
	{
	case VectorField::Method::Glyph:
		{
			//
			//  mode=0 is handled by iVectorFieldPipeline1, ignore
			//
			break;
		}
	case VectorField::Method::StreamLine:
		{
			mLine->SetSplitLines(false);
			this->CompletePipeline(mLine->GetOutputPort()); 
			break;
		}
	case VectorField::Method::StreamTube:
		{
			mLine->SetSplitLines(false);
			mTubeFilter->SetInputConnection(mLine->GetOutputPort());
			this->CompletePipeline(mTubeFilter->GetOutputPort());
			break;
		}
	case VectorField::Method::StreamBand:
		{
			mLine->SetSplitLines(true);
			mBandFilter->SetInputConnection(mLine->GetOutputPort());
			this->CompletePipeline(mBandFilter->GetOutputPort());
			break;
		}
	default:
		{
#ifndef I_NO_CHECK
			IBUG_WARN("Internal check failed.");
#endif
		}
	}
	this->Modified();
}


void iVectorFieldStreamLinePipeline::UpdateLineDirection()
{ 
	mLine->SetDirection(mOwner->GetLineDirection());
	this->Modified();
}


void iVectorFieldStreamLinePipeline::UpdateLineLength()
{ 
	mLine->SetLength(2.0*mOwner->GetLineLength());
	this->Modified();
}


void iVectorFieldStreamLinePipeline::UpdateLineQuality()
{ 
	mLine->SetQuality(mOwner->GetLineQuality());
	this->Modified();
}


void iVectorFieldStreamLinePipeline::UpdateTubeSize()
{ 
	mTubeFilter->SetRadius(0.005*mOwner->GetTubeSize());
	this->Modified();
}


void iVectorFieldStreamLinePipeline::UpdateTubeRangeFactor()
{ 
	mTubeFilter->SetRadiusFactor(mOwner->GetTubeRangeFactor());
	this->Modified();
}


void iVectorFieldStreamLinePipeline::UpdateTubeVariationFactor()
{
	//
	//  Do not update unless necessary
	//
	if(mOwner->CanBeShown())
	{
		mLine->SetMinimumSpeed(mOwner->GetTubeVariationFactor()*this->GetLimits()->GetMax(0));
		this->Modified();
	}
}

