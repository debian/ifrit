/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iextendableobject.h"


#include "idata.h"

//
//  Templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


//
//  Helper class
//
iObjectExtension::iObjectExtension(iExtendableObject *owner, const iString &fname, const iString &sname) : iObject(owner,fname,sname)
{
	IASSERT(owner);

	mOwner = owner;
	mRenderMode = owner->GetRenderMode();
}


iObjectExtension::~iObjectExtension()
{
}


//
//  Main class
//
iExtendableObject::iExtendableObject(iObject *parent, const iString &fname, const iString &sname) : iObject(parent,fname,sname)
{
}


iExtendableObject::~iExtendableObject()
{
	while(mExtensions.Size() > 0) mExtensions.RemoveLast()->Delete();
}


iObjectExtension* iExtendableObject::GetExtensionByName(const iString &name) const
{
	int i;
	for(i=0; i<mExtensions.Size(); i++) if(mExtensions[i]->LongName() == name) return mExtensions[i];
	return 0;
}


iObjectExtension* iExtendableObject::GetExtensionByClassName(const iString &name) const
{
	int i;
	for(i=0; i<mExtensions.Size(); i++) if(mExtensions[i]->IsA(name.ToCharPointer())) return mExtensions[i];
	return 0;
}


void iExtendableObject::InstallExtension(iObjectExtension *object)
{
	IASSERT(object);
	mExtensions.AddUnique(object);

	//
	//  We don't treat extensions as children
	//
	mChildren.Remove(object);

	//
	//  Register extension properties
	//
	int j;
	for(j=0; j<object->mVariableProperties.Size(); j++)
	{
		mVariableProperties.Add(object->mVariableProperties[j]);
	}
	for(j=0; j<object->mFunctionProperties.Size(); j++)
	{
		mFunctionProperties.Add(object->mFunctionProperties[j]);
	}
}
