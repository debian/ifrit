/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef ICOMMONDATADISTRIBUTORS_H
#define ICOMMONDATADISTRIBUTORS_H


#include "iviewsubjectpipelinedatamanager.h"


#include "iarray.h"

#include <vtkSetGet.h>

class iAppendImageDataFilter;
class iAppendPolyDataFilter;

class vtkImageData;


class iFieldDataDistributor : public iViewSubjectPipelineDataDistributor
{

public:

	vtkTypeMacro(iFieldDataDistributor,iViewSubjectPipelineDataDistributor);
	iFieldDataDistributor(iViewSubjectPipelineDataManager *manager, const iString &type);

	inline int GetSplitDimension() const { return mSplitDim; }
	inline const iArray<double>& GetEdges() const { return mEdges; }

protected:

	int mSplitDim;
	iArray<double> mEdges;
};


class iImageDataDistributor : public iFieldDataDistributor
{

public:

	vtkTypeMacro(iImageDataDistributor,iFieldDataDistributor);
	iImageDataDistributor(iViewSubjectPipelineDataManager *manager);

protected:

	virtual bool DistributeDataBody(vtkDataSet *globalInput, iArray<vtkDataSet*> &localInputs);
};


class iImageDataCollector : public iViewSubjectPipelineDataCollector
{

public:

	vtkTypeMacro(iImageDataCollector,iViewSubjectPipelineDataCollector);
	iImageDataCollector(iViewSubjectPipelineDataManager *manager);

protected:

	virtual ~iImageDataCollector();

	virtual bool CollectDataBody(iArray<vtkDataSet*> &localOutputs, vtkDataSet *globalOutput);

	iAppendImageDataFilter *mCollector;
};


class iPolyDataDistributor : public iViewSubjectPipelineDataDistributor
{

public:

	vtkTypeMacro(iPolyDataDistributor,iViewSubjectPipelineDataDistributor);
	iPolyDataDistributor(iViewSubjectPipelineDataManager *manager);

protected:

	virtual bool DistributeDataBody(vtkDataSet *globalInput, iArray<vtkDataSet*> &localInputs);
};


class iPolyDataCollector : public iViewSubjectPipelineDataCollector
{

public:

	vtkTypeMacro(iPolyDataCollector,iViewSubjectPipelineDataCollector);
	iPolyDataCollector(iViewSubjectPipelineDataManager *manager, iFieldDataDistributor *distributor = 0);

	void FixStiches(bool s){ mFixStiches = s; }

protected:

	virtual ~iPolyDataCollector();

	virtual bool CollectDataBody(iArray<vtkDataSet*> &localOutputs, vtkDataSet *globalOutput);

	bool mFixStiches;
	iFieldDataDistributor *mDistributor;
	iAppendPolyDataFilter *mCollector;
};

#endif  // ICOMMONDATADISTRIBUTORS_H
