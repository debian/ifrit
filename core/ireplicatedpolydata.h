/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Takes PolyData data of any type and replicates them periodically.
//
#ifndef IREPLICATEDPOLYDATA_H
#define IREPLICATEDPOLYDATA_H


#include "igenericfilter.h"
#include <vtkPolyDataAlgorithm.h>
#include "ireplicatedelement.h"


class iViewInstance;


class iReplicatedPolyData : public iGenericPolyDataFilter<vtkPolyDataAlgorithm>, public iReplicatedElement
{

public:

	vtkTypeMacro(iReplicatedPolyData,iGenericPolyDataFilter<vtkPolyDataAlgorithm>);
	static iReplicatedPolyData* New(iViewInstance *owner = 0) { IERROR_CHECK_MEMORY(owner); return new iReplicatedPolyData(owner); }

	virtual float GetMemorySize();

	//
	//  Removes stitches between extensions in place
	//
	static void PatchStitches(vtkPolyData *input, int ndim, double *edges, vtkIdType nEdges);

protected:
	
	iReplicatedPolyData(iViewInstance *owner);

	virtual void ProvideOutput();
	virtual void UpdateReplicasBody();

private:

	void ExtendDirection(vtkPolyData *input, vtkPolyData *output, int dim, int extDown, int extUp);
};

#endif  // IREPLICATEDPOLYDATA_H


