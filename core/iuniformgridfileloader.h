/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  DataFileLoader for reading uniform mesh data
//
#ifndef IUNIFORMGRIDFILELOADER_H
#define IUNIFORMGRIDFILELOADER_H


#include "ifileloader.h"


#include "iarray.h"
#include "ibuffer.h"

#include <vtkObject.h>

class iUniformGridHelper;

class vtkImageData;


class iUniformGridFileLoader : public iFileLoader
{

	//friend class iUniformGridHelper;

public:

	vtkTypeMacro(iUniformGridFileLoader,iFileLoader);

	void SetVoxelLocation(int s);
	inline int GetVoxelLocation() const { return mVoxelLocation; }

	void SetScaledDimension(int v);
	inline int GetScaledDimension() const { return mScaledDim; }

	void SetMaxZDimension(int v);
	inline int GetMaxZDimension() const { return mMaxZDim; }

protected:

	iUniformGridFileLoader(iDataReader *r, int priority, bool usePeriodicOffsets);
	virtual ~iUniformGridFileLoader();

	inline float* GetDataPointer() const { return mBuffer; }

	virtual void LoadFileBody(const iString &suffix, const iString &fname);
	virtual void ShiftDataBody(vtkDataSet *data, const double *dx);
	virtual void Polish(vtkDataSet *ds);

	virtual void ReadBinFile(const iString &fname);
    virtual void ReadTxtFile(const iString &fname);

	virtual int GetNumComponents(int nvars) const = 0;
	virtual void AssignBinData(int ncoms, vtkIdType ntot, int com, float *d) = 0;
	virtual void AssignTxtData(int ncoms, vtkIdType ind, float *f) = 0;
	virtual void AttachBuffer(vtkImageData *data) = 0;

	void EraseBuffer();
	void ComputeSpacing(double org[3], double spa[3]);
	void ReleaseData();

	bool IsCompatible(iUniformGridFileLoader *other) const;

	double mOriginOffset[3];
	int mScaledDim;
	int mNumVars, mNumComponents;
	const int mMaxDimension;
	const int mBufferSizeOffset;

	int mVoxelLocation;
	int mFileDims[3], mDataDims[3];
	int mMaxZDim;

	vtkIdType mBufferSize;
	float *mBuffer;

private:

	const bool mUsePeriodicOffsets;
	iUniformGridHelper *mHelper;
};

#endif

