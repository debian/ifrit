/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IIMAGECOMPOSER_H
#define IIMAGECOMPOSER_H


#include "iobject.h"
#include "ishellcomponent.h"


#include "iarray.h"

class iImageComposerWindow;
class iImageComposerBackgroundWindow;
class iImageComposerForegroundWindow;
class iStereoImageArray;
class iViewModule;

#define iImageComposerPropertyMacro(_name_,_id_) \
	iObjectSlotMacro2V(_name_,iType::Traits<iType::_id_>::Type); \
	iPropertyVector<iType::_id_> _name_

class iImageComposer : public iObject, public iShellComponent
{

public:

	vtkTypeMacro(iImageComposer,iObject);
	static iImageComposer* New(iShell *s = 0);

	iObjectPropertyMacro1V(NumTiles,Int,2);
	iObjectPropertyMacro2S(NumForegroundWindows,Int);

	iObjectPropertyMacro1S(ScaleBackground,Bool);
	iObjectPropertyMacro1S(InnerBorder,Bool);
	iObjectPropertyMacro1S(BorderColor,Color);
	iObjectPropertyMacro1S(BorderWidth,Int);

	iImageComposerPropertyMacro(BackgroundWindowIndex,Int);
	iImageComposerPropertyMacro(BackgroundWindowIndex2,Int);
	iImageComposerPropertyMacro(BackgroundWindowIndex3,Int);
	iImageComposerPropertyMacro(BackgroundWindowWallpaperFile,String);

	iImageComposerPropertyMacro(ForegroundWindowIndex,Int);
	iImageComposerPropertyMacro(ForegroundWindowIndex2,Int);
	iImageComposerPropertyMacro(ForegroundWindowIndex3,Int);
	iImageComposerPropertyMacro(ForegroundWindowBorderColor,Color);
	iImageComposerPropertyMacro(ForegroundWindowScale,Float);
	iImageComposerPropertyMacro(ForegroundWindowBorderWidth,Int);
	iImageComposerPropertyMacro(ForegroundWindowPositionX,Int);
	iImageComposerPropertyMacro(ForegroundWindowPositionY,Int);
	iImageComposerPropertyMacro(ForegroundWindowZoomSource,Int);
	iImageComposerPropertyMacro(ForegroundWindowZoom4Line,Bool);
	iImageComposerPropertyMacro(ForegroundWindowZoomFactor,Float);
	iImageComposerPropertyMacro(ForegroundWindowZoomX,Float);
	iImageComposerPropertyMacro(ForegroundWindowZoomY,Float);

	iObjectPropertyMacro2Q(ImageWidth,Int);
	iObjectPropertyMacro2Q(ImageHeight,Int);
	int GetTileWidth() const { return mTileWidth; }
	int GetTileHeight() const { return mTileHeight; }
	int GetNumTilesX() const { return mNumTiles[0]; }
	int GetNumTilesY() const { return mNumTiles[1]; }

	int GetNumberOfBackgroundWindows() const { return mBackgroundWindows.Size(); }
	int GetNumberOfForegroundWindows() const { return mForegroundWindows.Size(); }

	inline iImageComposerBackgroundWindow* GetBackgroundWindow(int i, int j) const
	{
		return this->GetBackgroundWindow(i+mNumTiles[0]*j);
	}

	iImageComposerBackgroundWindow* GetBackgroundWindow(int i) const;
	iImageComposerForegroundWindow* GetForegroundWindow(int i) const;

	void AddForegroundWindow(iViewModule *v);
	void RemoveForegroundWindow(int n);

	//
	//  Other functions
	//
	inline bool InComposing() const { return mInComposing; }
	bool Compose(iViewModule *vm, iStereoImageArray &images);
	bool IsActive();

	void Update();  // update all
	void UpdateWindowList();
	void UpdateSize();

protected:

	virtual ~iImageComposer();

private:

	iImageComposer(iShell *s, const iString &fname, const iString &sname);

	bool mBlockUpdate, mInComposing;
	int mFullWidth, mFullHeight;
	int mTileWidth, mTileHeight;

	iArray<iImageComposerBackgroundWindow*> mBackgroundWindows;
	iArray<iImageComposerForegroundWindow*> mForegroundWindows;
};

#endif // IIMAGECOMPOSER_H

