/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IVECTOR3D_H
#define IVECTOR3D_H


//
//  3D vector as a type for the Calculator use
//
class iVector3D
{

	friend iVector3D operator+(const iVector3D &v1, const iVector3D &v2);
	friend iVector3D operator-(const iVector3D &v1, const iVector3D &v2);
	friend iVector3D operator*(const iVector3D &v, double s);

public:

	iVector3D();
	iVector3D(double x, double y, double z);
	iVector3D(const double *pos);
	iVector3D(const iVector3D &v);
	~iVector3D();

	iVector3D& operator=(const double *pos);  // for VTK
	iVector3D& operator=(const iVector3D &v);
	iVector3D& operator+=(const iVector3D &v);
	iVector3D& operator-=(const iVector3D &v);

	inline bool operator==(const iVector3D &v) const
	{
		return (mData[0]==v.mData[0]) && (mData[1]==v.mData[1]) && (mData[2]==v.mData[2]); 
	}
	inline bool operator!=(const iVector3D &v) const
	{
		return (mData[0]!=v.mData[0]) || (mData[1]!=v.mData[1]) || (mData[2]!=v.mData[2]); 
	}

	inline double x() const { return mData[0]; }
	inline double y() const { return mData[1]; }
	inline double z() const { return mData[2]; }
	inline double operator[](int i) const { return mData[i]; }
	inline double& operator[](int i) { return mData[i]; }
	inline operator double*(){ return mData; }					// Needed for some VTK operations
	inline operator const double*() const { return mData; }

private:

	double mData[3];
};

iVector3D operator+(const iVector3D &v1, const iVector3D &v2);
iVector3D operator-(const iVector3D &v1, const iVector3D &v2);
iVector3D operator*(const iVector3D &v, double s);

inline iVector3D operator*(double s, const iVector3D &v){ return operator*(v,s); }
inline iVector3D operator/(const iVector3D &v, double s){ return operator*(v,1.0/s); }

#endif  // IVECTOR3D_H
