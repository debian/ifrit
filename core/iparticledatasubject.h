/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A base class for all DataSubjects of particle types.
//
#ifndef IPARTICLEDATASUBJECT_H
#define IPARTICLEDATASUBJECT_H


#include "idatasubject.h"


class iParticleFileLoader;


class iParticleDataSubject : public iDataSubject
{

	friend class iParticleFileLoader;

public:

	vtkTypeMacro(iParticleDataSubject,iDataSubject);

	iObjectPropertyMacro2Q(UseExtras,Bool);
	iObjectPropertyMacro2S(TypeIncluded,Bool);
	iObjectPropertyMacro2S(DownsampleMode,Int);
	iObjectPropertyMacro2S(DownsampleFactor,Int);
	iObjectPropertyMacro2S(AddOrderAsVar,Bool);
	iObjectPropertyMacro2S(DensityVar,Int);

	virtual iProbeFilter* CreateProbeFilter(iDataConsumer *consumer) const;

protected:

	iParticleDataSubject(iParticleFileLoader *fl, const iDataType& type);

	virtual iDataLimits* CreateLimits() const;

private:

	iParticleFileLoader *mParticleLoader;
};


//
//  Useful macros to declare all members that have to be overwritten in children
//
#define iParticleDataSubjectDeclareClassMacro(_prefix_,_name_) \
class _prefix_##_name_##DataSubject : public iParticleDataSubject \
{ \
public: \
	vtkTypeMacro(_prefix_##_name_##DataSubject,iParticleDataSubject); \
	static const iDataType& DataType(); \
	const iDataType& GetDataType() const; \
	_prefix_##_name_##DataSubject(iParticleFileLoader *fl); \
}

#define iParticleDataSubjectDefineClassMacro(_prefix_,_name_,_fname_,_sname_,_keywords_,_environment_) \
iDataSubjectDefineTypeMacro(_prefix_##_name_##DataSubject,_prefix_##Extension::SubjectId(),_fname_,_sname_,0,_keywords_,_environment_); \
_prefix_##_name_##DataSubject::_prefix_##_name_##DataSubject(iParticleFileLoader *fl) : iParticleDataSubject(fl,_prefix_##_name_##DataSubject::DataType()){}


#endif

