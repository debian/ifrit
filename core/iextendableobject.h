/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  This a base class for objects that have other objects installed to them as extensions.
//
#ifndef IEXTENDABLEOBJECT_H
#define IEXTENDABLEOBJECT_H


#include "iobject.h"


#include "iarray.h"


class iDataInfo;
class iExtendableObject;


class iObjectExtension : public iObject
{

	friend class iExtendableObject;

public:

	vtkTypeMacro(iObjectExtension,iObject);

protected:

	iObjectExtension(iExtendableObject *owner, const iString &fname, const iString &sname);
	virtual ~iObjectExtension();

	iExtendableObject* Owner() const { return mOwner; }

private:

	iExtendableObject* mOwner;
};


class iExtendableObject : public iObject
{

	friend class iObjectFactory;

public:

	vtkTypeMacro(iExtendableObject,iObject);

	inline iObjectExtension* GetExtension(int n) const { return mExtensions[n]; } // will return 0 if out of bounds
	iObjectExtension* GetExtensionByName(const iString &name) const; // will return 0 if none has this name; if more than one has the same name, the first one will be returned
	iObjectExtension* GetExtensionByClassName(const iString &name) const; // will return 0 if none has this name; if more than one has the same name, the first one will be returned

protected:

	iExtendableObject(iObject *parent, const iString &fname, const iString &sname);
	virtual ~iExtendableObject();

	iLookupArray<iObjectExtension*> mExtensions;

private:

	//
	//  Constructor helpers accessible to iExtensionFactory only
	//
	void InstallExtension(iObjectExtension *object);
};


//
//  Useful macros
//
#define iObjectExtensionDeclareCommonMacro(_owner_,_self_) \
	public: \
	static _self_* Self(_owner_ *owner)

#define iObjectExtensionDeclareAbstractMacro(_owner_) \
	iObjectExtensionDeclareCommonMacro(_owner_,_owner_##Extension); \
	vtkTypeMacro(_owner_##Extension,iObjectExtension); \
	protected: \
	_owner_ *mRealOwner; \
	_owner_##Extension(_owner_ *owner, const iString &fname, const iString &sname)

#define iObjectExtensionDeclareSpecificMacro(_owner_,_self_) \
	iObjectExtensionDeclareCommonMacro(_owner_,_self_); \
	vtkTypeMacro(_self_,_owner_##Extension); \
	protected: \
	_self_(_owner_ *owner)


#define iObjectExtensionDefineCommonMacro(_owner_,_self_) \
	_self_* _self_::Self(_owner_ *owner) \
	{ \
		return iRequiredCast<_self_>(INFO,owner->GetExtensionByClassName(#_self_)); \
	}

#define iObjectExtensionAbstractConstructorBeginMacro(_owner_) \
	iObjectExtensionDefineCommonMacro(_owner_,_owner_##Extension) \
	_owner_##Extension::_owner_##Extension(_owner_ *owner, const iString &fname, const iString &sname) : iObjectExtension(owner,fname,sname), mRealOwner(owner)


#define iObjectExtensionSpecificConstructorBeginMacro(_owner_,_self_) \
	iObjectExtensionDefineCommonMacro(_owner_,_self_) \
	_self_::_self_(_owner_ *owner) : _owner_##Extension(owner,owner->LongName(),owner->ShortName())

#endif

