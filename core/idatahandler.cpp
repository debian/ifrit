/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "idatahandler.h"


#include "idataconsumer.h"
#include "idatasubject.h"
#include "ierror.h"
#include "ishell.h"
#include "iviewmodule.h"


iDataHandler::iDataHandler(iDataConsumer *consumer) : iViewModuleComponent((consumer==0)?0:consumer->GetViewModule())
{
	IASSERT(consumer);

	mConsumer = consumer;
	mConsumer->RegisterDataHandler(this);

	this->GetViewModule()->GetShell()->OnInitAtom();
}


iDataHandler::~iDataHandler()
{
	//
	//  On exit, garbare collector may delete consumer first; 
	//  then mConsumer would set to 0 by iDataConsumer destructor.
	//
	if(mConsumer != 0) mConsumer->UnRegisterDataHandler(this);
}


bool iDataHandler::IsOptimizedForSpeed() const
{
	return mConsumer->IsOptimizedForSpeed();
}


bool iDataHandler::IsOptimizedForMemory() const
{
	return mConsumer->IsOptimizedForMemory();
}


bool iDataHandler::IsOptimizedForQuality() const
{
	return mConsumer->IsOptimizedForQuality();
}


iDataLimits* iDataHandler::GetLimits() const
{
	return mConsumer->GetLimits();
}


void iDataHandler::GetPeriodicities(bool per[3]) const
{
	int i;

	for(i=0; i<3; i++) per[i] = mConsumer->GetSubject()->IsDirectionPeriodic(i);
}


bool iDataHandler::SyncWithLimits(int var, int mode)
{
	return true;
}

