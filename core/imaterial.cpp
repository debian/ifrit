/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "imaterial.h"


#include "ierror.h"
#include "imaterialobject.h"

#include <vtkProperty.h>
#include <vtkVolumeProperty.h>

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"
#include "iproperty.tlh"


using namespace iParameter;


iMaterial* iMaterial::New(iMaterialObject *parent)
{
	static iString LongName("Material");
	static iString ShortName("_mat");

	IASSERT(parent);
	return new iMaterial(parent,LongName,ShortName);
}


iMaterial::iMaterial(iMaterialObject *parent, const iString &fname, const iString &sname) : iObject(parent,fname,sname),
	iObjectConstructPropertyMacroS(Int,iMaterial,ShadingMode,sm),
	iObjectConstructPropertyMacroS(Float,iMaterial,Ambient,a),
	iObjectConstructPropertyMacroS(Float,iMaterial,Diffuse,d),
	iObjectConstructPropertyMacroS(Float,iMaterial,Specular,s),
	iObjectConstructPropertyMacroS(Float,iMaterial,SpecularPower,p)
{
	mRenderMode = RenderMode::Self;

	mRealParent = parent;

	mShadingMode = 0;
	mAmbient = 0.3;
	mDiffuse = 0.4;
	mSpecular = 0.2;
	mSpecularPower = 0.5;

	this->SetShading(true);
	this->UpdateProperties();
}


iMaterial::~iMaterial()
{
	while(mActorProperties.Size() > 0) mActorProperties.RemoveLast()->UnRegister(this);
	while(mVolumeProperties.Size() > 0) mVolumeProperties.RemoveLast()->UnRegister(this);
}


void iMaterial::AddProperty(vtkProperty *prop)
{
	IASSERT(prop);
	prop->Register(this);
	mActorProperties.AddUnique(prop);
	this->UpdateProperties();
}


void iMaterial::AddProperty(vtkVolumeProperty *prop)
{
	IASSERT(prop);
	prop->Register(this);
	mVolumeProperties.AddUnique(prop);
	this->UpdateProperties();
}


void iMaterial::RemoveProperty(vtkProperty *prop)
{
	IASSERT(prop);
	mActorProperties.Remove(prop);
	prop->UnRegister(this);
}


void iMaterial::RemoveProperty(vtkVolumeProperty *prop)
{
	IASSERT(prop);
	mVolumeProperties.Remove(prop);
	prop->UnRegister(this);
}


void iMaterial::SetShading(bool s)
{
	int i;

	if(mShadingMode != 0) return;

	for(i=0; i<mActorProperties.Size(); i++)
	{
		mActorProperties[i]->SetLighting(s);
	}

	for(i=0; i<mVolumeProperties.Size(); i++)
	{
		mVolumeProperties[i]->SetShade(s?1:0);
	}
}


void iMaterial::UpdateProperties()
{
	int i;

	if(mShadingMode > 0)
	{
		mShadingMode = 0;
		this->SetShading(true);
		mShadingMode = 1;
	}
	else if(mShadingMode < 0)
	{
		mShadingMode = 0;
		this->SetShading(false);
		mShadingMode = -1;
	}

	for(i=0; i<mActorProperties.Size(); i++)
	{
		mActorProperties[i]->SetAmbient(mAmbient);
		mActorProperties[i]->SetDiffuse(mDiffuse);
		mActorProperties[i]->SetSpecular(mSpecular);
		mActorProperties[i]->SetSpecularPower(128*mSpecularPower);
	}

	for(i=0; i<mVolumeProperties.Size(); i++)
	{
		mVolumeProperties[i]->SetAmbient(mAmbient);
		mVolumeProperties[i]->SetDiffuse(mDiffuse);
		mVolumeProperties[i]->SetSpecular(mSpecular);
		mVolumeProperties[i]->SetSpecularPower(128*mSpecularPower);
	}
}


bool iMaterial::SetShadingMode(int mode)
{
	mShadingMode = mode;
	this->UpdateProperties();

	if(mode == 0)
	{
		mRealParent->UpdateAutomaticShading();
	}

	return true;
}


bool iMaterial::SetAmbient(float v)
{
	mAmbient = v;
	this->UpdateProperties();
	return true;
}


bool iMaterial::SetDiffuse(float v)
{
	mDiffuse = v;
	this->UpdateProperties();
	return true;
}


bool iMaterial::SetSpecular(float v)
{
	mSpecular = v;
	this->UpdateProperties();
	return true;
}


bool iMaterial::SetSpecularPower(float v)
{
	mSpecularPower = v;
	this->UpdateProperties();
	return true;
}

