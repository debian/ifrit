/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Ifrit data filter: takes StructuredPoints data of type float and converts them into
//  StructuredPoints data of type char using lower and upper bounds.
//

#ifndef IVOLUMEDATACONVERTER_H
#define IVOLUMEDATACONVERTER_H


#include "igenericfilter.h"
#include <vtkImageAlgorithm.h>
#include "iparallelworker.h"


class iVolumeDataConverter : public iGenericImageDataFilter<vtkImageAlgorithm>, protected iParallelWorker
{

	iGenericFilterTypeMacro(iVolumeDataConverter,vtkImageAlgorithm);

public:

	inline int GetCurrentVar() const { return mCurVar; }
	void SetCurrentVar(int n);
	
protected:
	
	virtual void ProvideOutput();
	virtual int ExecuteStep(int step, iParallel::ProcessorInfo &p);

	virtual void OnLimitsChanged(int var, int mode);

private:

	int mCurVar;

	//
	//  Work variables
	//
	int wStretch, wNumComp;
	float *wInPtr, wFoffset, wFscale;
	unsigned char *wOutPtr;
	vtkIdType wSize;
};

#endif


