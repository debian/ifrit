/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef ISURFACEVIEWSUBJECT_H
#define ISURFACEVIEWSUBJECT_H


#include "imultiviewsubject.h"


class iSurfaceViewSubject;
class iViewObject;


namespace iParameter
{
	namespace SurfaceMethod
	{
		//
		// SurfaceViewSubject parameters
		//
		const int Isosurface = 0;
		const int Sphere = 1;
		const int Plane = 2;
	};
};


class iSurfaceViewInstance : public iMultiViewInstance, public iPropPlacementHelper
{
	
	friend class iSurfaceViewSubject;

public:
	
	iDataConsumerTypeMacro(iSurfaceViewInstance,iMultiViewInstance);
	
	iObjectSlotMacro1SV(Method,int);
	iObjectSlotMacro1SV(NormalsFlipped,bool);
	iObjectSlotMacro1SV(IsoVar,int);
	iObjectSlotMacro1SV(IsoLevel,float);
	iObjectSlotMacro1SV(IsoMethod,int);
	iObjectSlotMacro1SV(IsoSmoothing,int);
	iObjectSlotMacro1SV(IsoReduction,int);
	iObjectSlotMacro1SV(IsoOptimization,bool);
	iObjectSlotMacro1SV(AlternativeReductionMethod,bool);

	//
	//  Slot wrappers
	//
	bool SetPlaneDirection(const iVector3D& v){ return this->iPropPlacementHelper::SetDirection(v); }
	const iVector3D& GetPlaneDirection() const { return this->iPropPlacementHelper::GetDirection(); }

	bool SetObjectPosition(const iVector3D& v){ return this->iPropPlacementHelper::SetBoxPosition(v); }
	const iVector3D& GetObjectPosition() const { return this->iPropPlacementHelper::GetBoxPosition(); }

	bool SetSphereSize(double p){ return this->iPropPlacementHelper::SetBoxSize(p); }
	double GetSphereSize() const { return this->iPropPlacementHelper::GetBoxSize(); }

protected:
	
	iSurfaceViewInstance(iSurfaceViewSubject *owner);
	virtual ~iSurfaceViewInstance();
	virtual void ConfigureBody();
	virtual void ConfigureMainPipeline(iViewSubjectPipeline *p, int id);

	virtual void ShowBody(bool s);
	virtual bool CanBeShown() const;

	virtual iViewSubjectPipeline* CreatePipeline(int id);

	virtual void UpdateDirection();
	virtual void UpdatePosition();
	virtual void UpdateSize();

	void OutputProperties(iString& str) const;
};


class iSurfaceViewSubject : public iMultiViewSubject
{
	
	friend class iSurfaceViewInstance;

public:
	
	iViewSubjectTypeMacro(iSurfaceView,iMultiViewSubject);
	
	iType::vsp_int Method;
	iType::vsp_bool NormalsFlipped;
	iType::vsp_int IsoVar;
	iType::vsp_float IsoLevel;
	iType::vsp_int IsoMethod;
	iType::vsp_int IsoSmoothing;
	iType::vsp_int IsoReduction;
	iType::vsp_bool IsoOptimization;
	iType::vsp_bool AlternativeReductionMethod;
	iType::vsp_vector PlaneDirection;
	iType::vsp_double SphereSize;
	iType::vsp_vector Position;

	void OutputProperties(int i, iString& str) const;

	bool InterpolateBetweenTwoInstances(int i1, int i2, int n, bool testOnly = false);

protected:
	
	iSurfaceViewSubject(iViewObject *parent, const iDataType &type);
	virtual ~iSurfaceViewSubject();

	virtual int DepthPeelingRequestBody() const;
};

#endif // ISURFACEVIEWSUBJECT_H

