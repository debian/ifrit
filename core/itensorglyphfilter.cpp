/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "itensorglyphfilter.h"


#include "ierror.h"

#include <vtkDataSet.h>
#include <vtkPointData.h>
#include <vtkTensorGlyph.h>

//
//  Templates
//
#include "igenericfilter.tlh"


iTensorGlyphFilter::iTensorGlyphFilter(iDataConsumer *consumer) : iGenericFilter<vtkDataSetAlgorithm,vtkDataSet,vtkPolyData>(consumer,1,true)
{
	mWorker = vtkTensorGlyph::New(); IERROR_CHECK_MEMORY(mWorker);
	mWorker->ScalingOn();
	mWorker->ColorGlyphsOn();
	mWorker->ClampScalingOn();
	mWorker->SetColorModeToScalars();
}


iTensorGlyphFilter::~iTensorGlyphFilter()
{
	mWorker->Delete();
}


void iTensorGlyphFilter::SetScaling(bool s)
{
	if(s != this->GetScaling())
	{
		mWorker->SetScaling(s?1:0);
		this->Modified();
	}
}


bool iTensorGlyphFilter::GetScaling() const
{
	return (mWorker->GetScaling() != 0);
}


void iTensorGlyphFilter::SetScaleFactor(float s)
{
	if(s != this->GetScaleFactor())
	{
		mWorker->SetScaleFactor(s);
		this->Modified();
	}
}


float iTensorGlyphFilter::GetScaleFactor() const
{
	return mWorker->GetScaleFactor();
}


void iTensorGlyphFilter::SetCurrentVar(int n)
{ 
	if(n>=0 && n!= mCurVar)
	{
		mCurVar = n;
		this->Modified();
	}
}


void iTensorGlyphFilter::ProvideOutput()
{
	vtkDataSet *input = this->InputData();
	
	vtkDataSet *workerInput = input->NewInstance(); IERROR_CHECK_MEMORY(workerInput);
	workerInput->ShallowCopy(input);

	if(this->InputData()->GetPointData()!=0 && this->InputData()->GetPointData()->GetScalars()!=0)
	{
		//
		//  We have scalars
		//
		vtkFloatArray *scalars = iRequiredCast<vtkFloatArray>(INFO,input->GetPointData()->GetScalars());

		int var = mCurVar;
		if(var >= scalars->GetNumberOfComponents())
		{
			var = scalars->GetNumberOfComponents() - 1;
		}

		if(var >= 0) // we color by scalars
		{
			//
			//  vtkTensorGlyph always uses the first scalar component. We simple shift the data by mCurVar values
			//  so that the current value is always the 0 component.
			//
			vtkFloatArray *shiftedScalars = vtkFloatArray::New(); IERROR_CHECK_MEMORY(shiftedScalars);
			shiftedScalars->SetNumberOfComponents(scalars->GetNumberOfComponents());
			shiftedScalars->SetArray(scalars->GetPointer(var),scalars->GetSize(),1);
			workerInput->GetPointData()->SetScalars(shiftedScalars);
			shiftedScalars->Delete();
		}
	}

	mWorker->SetInputData(workerInput);
	mWorker->Update();
	workerInput->Delete();

	this->OutputData()->ShallowCopy(mWorker->GetOutput());
}


void iTensorGlyphFilter::SetGlyphConnection(vtkAlgorithmOutput *port)
{
	mWorker->SetSourceConnection(port);
}

