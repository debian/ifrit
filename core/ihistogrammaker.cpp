/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ihistogrammaker.h"


#include "idatalimits.h"
#include "idatasubject.h"
#include "ierror.h"
#include "ihistogram.h"
#include "imath.h"
#include "imonitor.h"
#include "iparallel.h"
#include "iparallelmanager.h"
#include "ishell.h"
#include "ishelleventobservers.h"
#include "istretch.h"

#include <vtkFloatArray.h>
#include <vtkIdTypeArray.h>
#include <vtkImageData.h>
#include <vtkMath.h>
#include <vtkPointData.h>

//
//  Templates
//
#include "iarray.tlh"


//
//  Abstract iterator
//
iHistogramMaker::Iterator::Iterator(const iDataSubject *subject, int var, vtkIdType beg, vtkIdType end)
{
	Subject = subject; IASSERT(Subject);

	Var = var;
	Idx = beg;
	End = end;

	DataPtr = 0;
	NumFields = 0;
}


bool iHistogramMaker::Iterator::Next(int step)
{
	if(step == 1)
	{
		Idx++;
	}
	else
	{
		int i;
		for(i=0; i<step && Idx<End; i++) Idx++;
	}
	return (Idx < End);
}


//
//  Templated iterator
//
template<unsigned int Rank>
double iHistogramMaker::StdIterator<Rank>::Weight() const
{
	return 1.0;
}


template<>
float iHistogramMaker::StdIterator<0U>::Value() const
{
#ifdef I_CHECK
	IASSERT(this->DataPtr);
#endif
	return this->DataPtr[this->Var+this->Idx*this->NumFields];
}


template<>
float iHistogramMaker::StdIterator<1U>::Value() const
{
#ifdef I_CHECK
	IASSERT(this->DataPtr);
#endif
	float *d = this->DataPtr + this->Idx*this->NumFields;
	return sqrt(d[0]*d[0]+d[1]*d[1]+d[2]*d[2]);
}


template<>
float iHistogramMaker::StdIterator<2U>::Value() const
{
#ifdef I_CHECK
	IASSERT(this->DataPtr);
#endif
	int i;
	float *m[3], e[3], *v[3], store[9];
	float *d = this->DataPtr + this->Idx*this->NumFields;
	
	for(i=0; i<3; i++)
	{
		v[i] = store + 3*i;
		m[i] = d + 3*i;
	}
	vtkMath::Jacobi(m,e,v);
	return e[this->Var];
}


//
//  VC++ syntax highlighter does not like something about these definitions, put it here not to confuse it in the rest of the file
//
template<>
iHistogramMaker::StdIterator<0U>::StdIterator(const iDataSubject *subject, int var, vtkIdType beg, vtkIdType end) : Iterator(subject,var,beg,end)
{
	vtkDataSet *data = Subject->GetData();
	if(this->Idx<0 || this->Var<0 || data==0 || data->GetPointData()==0) return;

	if(data->GetPointData()->GetScalars() == 0) return;

	//
	//  Access the data for both vtkPolyData and vtkImageData
	//
	vtkFloatArray *arr = vtkFloatArray::SafeDownCast(data->GetPointData()->GetScalars());
	if(arr == 0) return;
	this->DataPtr = arr->GetPointer(0);
	if(this->DataPtr == 0) return;
	if(this->Var >= arr->GetNumberOfComponents()) return;
	this->NumFields = arr->GetNumberOfComponents(); 
}


template<>
iHistogramMaker::StdIterator<1U>::StdIterator(const iDataSubject *subject, int var, vtkIdType beg, vtkIdType end) : iHistogramMaker::Iterator(subject,var,beg,end)
{
	vtkDataSet *data = Subject->GetData();
	if(this->Idx<0 || this->Var<0 || data==0 || data->GetPointData()==0) return;

	if(data->GetPointData()->GetVectors() == 0) return;

	//
	//  Access the data for both vtkPolyData and vtkImageData
	//
	vtkFloatArray *arr = vtkFloatArray::SafeDownCast(data->GetPointData()->GetVectors());
	if(arr == 0) return;
	this->DataPtr = arr->GetPointer(0);
	if(this->DataPtr == 0) return;
	if(this->Var!=0 || arr->GetNumberOfComponents()!=3) return;
	this->NumFields = 3; 
}


template<>
iHistogramMaker::StdIterator<2U>::StdIterator(const iDataSubject *subject, int var, vtkIdType beg, vtkIdType end) : iHistogramMaker::Iterator(subject,var,beg,end)
{
	vtkDataSet *data = Subject->GetData();
	if(this->Idx<0 || this->Var<0 || data==0 || data->GetPointData()==0) return;

	if(data->GetPointData()->GetTensors() == 0) return;

	//
	//  Access the data for both vtkPolyData and vtkImageData
	//
	vtkFloatArray *arr = vtkFloatArray::SafeDownCast(data->GetPointData()->GetTensors());
	if(arr == 0) return;
	this->DataPtr = arr->GetPointer(0);
	if(this->DataPtr == 0) return;
	if(this->Var>2 || arr->GetNumberOfComponents()!=9) return;
	this->NumFields = 9;
}


//
//  Helper class
//
class iHistogramMakerWorker
{

public:

	struct ProcDataItem
	{
		float ValMax;
		float ValMin;
		vtkIdType CellMax;
		vtkIdType CellMin;
		double *Values;
	};

	iHistogramMakerWorker()
	{
		NumBins = 0;
	}

	~iHistogramMakerWorker()
	{
		while(ProcData.Size() > 0) delete ProcData.RemoveLast().Values;
	}

	void Update(int nprocs, int nbins)
	{
		while(ProcData.Size() > nprocs) delete ProcData.RemoveLast().Values;

		if(nbins>0 && nbins!=NumBins)
		{
			int i;
			
			NumBins = nbins;

			for(i=0; i<ProcData.Size(); i++)
			{
				delete ProcData[i].Values;
				ProcData[i].Values = new double[NumBins+1]; IERROR_CHECK_MEMORY(ProcData[i].Values);
			}
		}

		while(ProcData.Size() < nprocs)
		{
			ProcDataItem d;
			d.Values = new double[NumBins+1]; IERROR_CHECK_MEMORY(d.Values);
			ProcData.Add(d);
		}
	}
	
	int NumBins;
	iArray<ProcDataItem> ProcData;

	typedef iArray<ProcDataItem> DataType;

	//
	//  Work variables
	//
	int Var, StretchId;
	bool Overflow;
};


//------------------------------------------------------------------------------
iHistogramMaker* iHistogramMaker::New(const iDataSubject *subject)
{
	IASSERT(subject);
	return new iHistogramMaker(subject);
}


iHistogramMaker::iHistogramMaker(const iDataSubject *subject) : iParallelWorker(subject->GetShell()->GetParallelManager()), mNullPointInfo(subject->GetViewModule())
{
	mSubject = subject;

	mWorker = new iHistogramMakerWorker; IASSERT(mWorker);

	this->AddObserver(vtkCommand::StartEvent,subject->GetShell()->GetExecutionEventObserver());
	this->AddObserver(vtkCommand::ProgressEvent,subject->GetShell()->GetExecutionEventObserver());
	this->AddObserver(vtkCommand::EndEvent,subject->GetShell()->GetExecutionEventObserver());
}


iHistogramMaker::~iHistogramMaker()
{
	while(mItems.Size() > 0) delete mItems.RemoveLast();
	delete mWorker;
}


bool iHistogramMaker::HasOverflow(int var)
{
	if(this->ComputeRange(var))
	{
		return mItems[var]->Overflow;
	}
	else return false;
}


const iPair iHistogramMaker::GetRange(int var)
{
	iPair p;
	this->GetRange(var,p.Min,p.Max);
	return p;
}


void iHistogramMaker::GetRange(int var, float &min, float &max)
{
	if(this->ComputeRange(var))
	{
		min = mItems[var]->Min.Value;
		max = mItems[var]->Max.Value;
	}
}


void iHistogramMaker::GetRange(int var, float range[2])
{
	if(this->ComputeRange(var))
	{
		range[0] = mItems[var]->Min.Value;
		range[1] = mItems[var]->Max.Value;
	}
}


const iHistogramMaker::PointInfo& iHistogramMaker::GetMin(int var)
{
	if(this->ComputePoints(var))
	{
		return mItems[var]->Min;
	}
	else return mNullPointInfo;
}


const iHistogramMaker::PointInfo& iHistogramMaker::GetMax(int var)
{
	if(this->ComputePoints(var))
	{
		return mItems[var]->Max;
	}
	else return mNullPointInfo;
}


const iHistogram& iHistogramMaker::GetHistogram(int var, int sid, bool full)
{
	if(this->ComputeHistogram(var,sid,full?1.0:0.1,full?1:1000))
	{
		return mItems[var]->Histogram[sid];
	}
	else return iHistogram::Null();
}


const iHistogram& iHistogramMaker::GetHistogram(int var, int sid, float fbins)
{
	if(this->ComputeHistogram(var,sid,fbins,1))
	{
		return mItems[var]->Histogram[sid];
	}
	else return iHistogram::Null();
}


bool iHistogramMaker::ComputeRange(int var)
{
	if(!mSubject->IsThereData() || var<0 || var>=this->GetNumberOfComponents()) return false;

	//
	//  Update Items array first
	//
	while(mItems.Size() > this->GetNumberOfComponents()) delete mItems.RemoveLast();
	while(mItems.Size() < this->GetNumberOfComponents())
	{
		VarItem *tmp = new VarItem(mSubject->GetViewModule()); IERROR_CHECK_MEMORY(tmp);
		mItems.Add(tmp);
	}


	VarItem &item(*(mItems[var]));

	//
	//  Do we need to recompute?
	//
	if(item.RangeMTime < mSubject->GetData()->GetMTime())
	{
		//	
		//  Recompute the limits
		//
		mWorker->Update(this->GetManager()->GetNumberOfProcessors(),0);
		mWorker->Overflow = false;
		mWorker->Var = var;

		iMonitor h(mSubject,"Computing",1.0);
		if(this->ParallelExecute(1) != 0) 
		{
			return false;
		}
		
		int i;
		iHistogramMakerWorker::DataType &d(mWorker->ProcData); // cache for speed

		float vmin = d[0].ValMin;
		float vmax = d[0].ValMax;
		vtkIdType cmin = d[0].CellMin;
		vtkIdType cmax = d[0].CellMax;
		for(i=1; i<d.Size(); i++)
		{
			if(vmin > d[i].ValMin) 
			{
				vmin = d[i].ValMin;
				cmin = d[i].CellMin;
			}
			if(vmax < d[i].ValMax) 
			{
				vmax = d[i].ValMax;
				cmax = d[i].CellMax;
			}
		}
		
		//
		//  Spread values a little bit if needed
		//
		if(vmax-vmin < 0.01*(fabs(vmax)+fabs(vmin)))
		{
			if(vmin < 0) vmin *= 1.001; else vmin *= 0.999;
			if(vmax < 0) vmax *= 0.999; else vmax *= 1.001;
		}

		item.Max.Value = vmax;
		item.Min.Value = vmin;
		item.Max.Cell = cmax;
		item.Min.Cell = cmin;

		item.Overflow = mWorker->Overflow;

		//
		//  Update the time
		//
		item.RangeMTime.Modified();
	}

	return true;
}


bool iHistogramMaker::ComputePoints(int var)
{
	if(this->ComputeRange(var))
	{
		VarItem &item(*(mItems[var]));

		//
		//  Do we need to recompute?
		//
		if(item.PointsMTime < mSubject->GetData()->GetMTime())
		{
			this->FindPositionForCell(item.Min.Cell,item.Min.Position);
			this->FindPositionForCell(item.Max.Cell,item.Max.Position);
		}

		item.PointsMTime.Modified();
		
		return true;
	}
	else return false;
}


bool iHistogramMaker::ComputeHistogram(int var, int sid, float fbins, int step)
{
	if(sid<0 || sid>=iStretch::Count) return false;
	if(!this->ComputeRange(var)) return false;

	VarItem &item(*(mItems[var]));

	int nbins = iMath::Round2Int(fbins*(1+2*pow(this->GetNumberOfCells(),0.333)));
	if(nbins < 3) nbins = 3;

	if(item.HistogramMTime[sid]<mSubject->GetData()->GetMTime() || item.Histogram[sid].NumBins()!=nbins || item.DataStep[sid]!=step)
	{
		//
		//  Check if we need to re-create the helper data 
		//
		int nprocs = this->GetManager()->GetNumberOfProcessors();
		mWorker->Update(nprocs,nbins);
#ifndef I_NO_CHECK
		if(nbins != mWorker->NumBins)
		{
			IBUG_ERROR("iHistogramMaker:Helper is configured incorrectly.");
			return false;
		}
#endif
		item.DataStep[sid] = step;
		mWorker->StretchId = sid;
		mWorker->Var = var;

		item.Histogram[sid].Reset(nbins,item.Min.Value,item.Max.Value,sid); // must be here for hist.Bin() to work

		iMonitor ph(mSubject,"Computing",1.0);
		if(this->ParallelExecute(2) != 0)
		{
			item.Histogram[sid].Clear();
			return false;
		}

		int i, j;
		iHistogramMakerWorker::DataType &d(mWorker->ProcData);
		iHistogram &h(item.Histogram[sid]);
		for(i=0; i<nbins; i++)
		{
			h.mValues[i] = d[0].Values[i];
			for(j=1; j<nprocs; j++) h.mValues[i] += d[j].Values[i];
		}

		//
		//  Update the time
		//
		item.HistogramMTime[sid].Modified();
	}

	return true;
}


int iHistogramMaker::ExecuteStep(int step, iParallel::ProcessorInfo &p)
{
	if(p.NumProcs != mWorker->ProcData.Size()) 
	{
		IBUG_WARN("Error calling iHistogramMaker in parallel.");
		return -1;
	}

	vtkIdType ntup = this->GetNumberOfCells();

	vtkIdType l, lbeg, lend, lstp;
	float v, vmin, vmax;
	int ret = 999;

	iHistogramMakerWorker::DataType &d(mWorker->ProcData);

	//
	//  Distribute data
	//
	iParallel::SplitRange(p,ntup,lbeg,lend,lstp);

	Iterator *it = this->MakeIterator(mSubject->GetDataRank(),mWorker->Var,lbeg,lend);
	if(!it->IsReady())
	{
		delete it;
		return -1;
	}

	//	
	//  Recompute the limits
	//
	if(step == 1)
	{
		vtkIdType lmin, lmax;
		
		vmin = vmax = it->Value();
		lmin = lmax = it->Index();

		while(it->Next())
		{
			if((l = it->Index())%10000 == 0)
			{
				if(p.IsMaster()) mSubject->GetShell()->GetExecutionEventObserver()->SetProgress(double(l-lbeg)/(lend-lbeg));
				if(mSubject->GetShell()->GetExecutionEventObserver()->IsAborted()) break;
			}

			v = it->Value();

			if(v < -iMath::_LargeFloat)
			{
				mWorker->Overflow = true;
				v = -iMath::_LargeFloat;
			}
			if(v > iMath::_LargeFloat)
			{
				mWorker->Overflow = true;
				v = iMath::_LargeFloat;
			}

			if(v < vmin)
			{
				vmin = v;
				lmin = l;
			}
			if(v > vmax)
			{
				vmax = v;
				lmax = l;
			}
		}

		d[p.ThisProc].ValMin = vmin;
		d[p.ThisProc].ValMax = vmax;
		d[p.ThisProc].CellMin = lmin;
		d[p.ThisProc].CellMax = lmax;
		
		ret = 0;
	}

	//	
	//  Compute the histogram
	//
	if(step == 2)
	{
		int iv;
		int nbins = mWorker->NumBins;
		int step = mItems[mWorker->Var]->DataStep[mWorker->StretchId];
		iHistogram &hist(mItems[mWorker->Var]->Histogram[mWorker->StretchId]);

		double *h = d[p.ThisProc].Values;
		memset(h,0,sizeof(double)*nbins);

		while(it->Next(step))
		{
			if((l = it->Index())%10000 == 0)
			{
				if(step==1 && p.IsMaster()) mSubject->GetShell()->GetExecutionEventObserver()->SetProgress(double(l-lbeg)/(lend-lbeg));
				if(mSubject->GetShell()->GetExecutionEventObserver()->IsAborted()) break;
			}
			
			iv = hist.Bin(it->Value());
			if(iv>=0 && iv<nbins)
			{
				h[iv] += it->Weight();
			} 
#ifdef I_CHECK
			else if(iv != -1)
			{
				IBUG_ERROR("Invalid index returned by iHistogram::Bin()");
				iv = hist.Bin(it->Value());
			}
#endif
		}

		ret = 0;
	}

	delete it;

	return ret;
}


int iHistogramMaker::GetNumberOfComponents()
{
	vtkDataSet *data = mSubject->GetData();
	if(data==0 || data->GetPointData()==0) return 0;

	switch(mSubject->GetDataRank())
	{
	case 0U:
		{
			if(data->GetPointData()->GetScalars() != 0) return data->GetPointData()->GetScalars()->GetNumberOfComponents(); else return 0;
		}
	case 1U:
		{
			if(data->GetPointData()->GetVectors() != 0) return 1; else return 0;
		}
	case 2U:
		{
			if(data->GetPointData()->GetTensors() != 0) return 3; else return 0;
		}
	}

	return 0;
}


vtkIdType iHistogramMaker::GetNumberOfCells()
{
	vtkDataSet *data = mSubject->GetData();
	if(data==0 || data->GetPointData()==0) return 0;

	switch(mSubject->GetDataRank())
	{
	case 0U:
	{
		if(data->GetPointData()->GetScalars() != 0) return data->GetPointData()->GetScalars()->GetNumberOfTuples(); else return 0;
	}
	case 1U:
	{
		if(data->GetPointData()->GetVectors() != 0) return data->GetPointData()->GetVectors()->GetNumberOfTuples(); else return 0;
	}
	case 2U:
	{
		if(data->GetPointData()->GetTensors() != 0) return data->GetPointData()->GetTensors()->GetNumberOfTuples(); else return 0;
	}
	}

	return 0;
}


void iHistogramMaker::FindPositionForCell(vtkIdType cell, iPosition &pos)
{
	int i;
	vtkImageData *id = vtkImageData::SafeDownCast(mSubject->GetData());

	if(id != 0)
	{
		int dims[3],ijk[3];
		double spa[3],org[3];
		id->GetDimensions(dims);
		id->GetSpacing(spa);
		id->GetOrigin(org);

		ijk[2] = cell / (dims[0] * dims[1]);
		ijk[1] = (cell - dims[0] * dims[1] * ijk[2]) / dims[0];
		ijk[0] = cell % dims[0];
		for(i=0; i<3; i++) pos[i] = org[i] + spa[i] * ijk[i];
	}
	else
	{
		for(i=0; i<3; i++) pos[i] = 0;
	}
}


iHistogramMaker::Iterator* iHistogramMaker::MakeIterator(unsigned int rank, int var, vtkIdType beg, vtkIdType end) const
{
	switch(mSubject->GetDataRank())
	{
	case 0U: return new StdIterator<0U>(mSubject,var,beg,end);
	case 1U: return new StdIterator<1U>(mSubject,var,beg,end);
	case 2U: return new StdIterator<2U>(mSubject,var,beg,end);
	default: return 0;
	}
}


iHistogramMaker::PointInfo::PointInfo(iViewModule *vm) : Position(vm)
{
}


iHistogramMaker::VarItem::VarItem(iViewModule *vm) : Max(vm), Min(vm)
{
}
