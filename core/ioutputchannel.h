/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Method for outputing messages
//
#ifndef IOUTPUTCHANNEL_H
#define IOUTPUTCHANNEL_H


#include <vtkObjectBase.h>
#include "ishellcomponent.h"


#include <vtkSetGet.h>

class iShell;
class iString;

class vtkCriticalSection;


class iOutputChannel : public vtkObjectBase, public iShellComponent
{
	
	friend class iShell;

public:
	
	vtkTypeMacro(iOutputChannel,vtkObjectBase);

	//
	//  Use this function to report errors and display text
	//
	void Display(int type, const iString &text);

protected:
	
	iOutputChannel(iShell *s);
	virtual ~iOutputChannel();

	virtual void DisplayBody(int type, const iString &text) = 0;

private:

	bool mInDisplay;
	vtkCriticalSection *mMutex;
};

#endif  // IOUTPUTCHANNEL_H
