/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IPIECEWISEFUNCTION_H
#define IPIECEWISEFUNCTION_H


#include "vtkObject.h"


#include "iarray.h"
#include "ipair.h"

class iPiecewiseFunctionHelper;
class iPiecewiseFunctionUser;

class vtkObject;
class vtkPiecewiseFunction;


class iPiecewiseFunction : public vtkObject
{
	
	friend class iPropertyPiecewiseFunction;
	friend class iPiecewiseFunctionHelper;

public:
	
	vtkTypeMacro(iPiecewiseFunction,vtkObject);
	static iPiecewiseFunction* New(float ymin = 0.0f, float ymax = 1.0f);
	
	inline int N() const { return mArray.Size(); }
	inline const iPair& P(int i) const { return mArray[i]; }
	inline float X(int i) const { return mArray[i].X; }
	inline float Y(int i) const { return mArray[i].Y; }
	inline float Min() const { return mYmin; }
	inline float Max() const { return mYmax; }

	void SetMinMax(float ymin, float ymax);
	void AddPoint(int n);
	void AddPoint(const iPair &p);
	void RemovePoint(int n);
	void MovePoint(int n, const iPair &p);
	int FindPoint(const iPair &p, float &d) const;
	float GetValue(float x) const;

	inline void AddPoint(float x, float y){ this->AddPoint(iPair(x,y)); }
	inline void MovePoint(int n, float x, float y){ this->MovePoint(n,iPair(x,y)); }

	void Copy(const iPiecewiseFunction *f);
	void Reset();
	
	vtkPiecewiseFunction* GetVTKFunction() const;

protected:
	
	iPiecewiseFunction(float ymin, float ymax, int inc);
	virtual ~iPiecewiseFunction();

	iOrderedArray<iOrderedPair> mArray;
	float mYmin, mYmax;
	iPiecewiseFunctionHelper *mHelper;
};

#endif // IPIECEWISEFUNCTION_H

