/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


//
//  The different kinds of data are distinguished by an associated class DataType. One instance
//  of DataType class describes a specific type of data loaded from a single data file (like 
//  UniformScalars, BasicParticles, etc). DataType(s) can be queried by string keywords. 
//  
//  A group of DataType(s) forms a DataInfo.
//

#ifndef IDATA_H
#define IDATA_H


#include "iarray.h"
#include "idataid.h"
#include "ipointer.h"
#include "istring.h"


class iDataInfo;
class iShell;


class iDataType
{

	friend class iDataId;

public:

	static const iDataType& Null();

	iDataType(int id, const iString& tname, const iString& sname, int rank, const iString &keywords, const iString &environment = "");
	~iDataType();

	bool operator==(const iDataType &type) const;
	bool operator!=(const iDataType &type) const;

	inline iDataId GetId() const { return iDataId(*this); }
	inline bool IsNull() const { return mId == 0; }

	inline const iString& TextName() const { return mTextName; }
	inline const iString& LongName() const { return mLongName; }
	inline const iString& ShortName() const { return mShortName; }

	inline int GetRank() const { return mRank; }

	iString GetEnvironment(iShell *shell) const;

	inline bool IsOfType(const iString &name) const { return name==mLongName || name==mShortName; }
	bool MatchesKeyword(const iString &str) const;
	static void FindTypesByKeywords(iDataInfo &info, const iString &str);

	static const iDataType& FindTypeById(iDataId id);
	static const iDataType& FindTypeByName(const iString &name);

private:

	const int mId, mRank;
	const iString mTextName, mKeywords;
	iString mLongName, mShortName, mEnv;

	iDataType(); // not implemented
	iDataType(const iDataType &type); // not implemented
	void operator=(const iDataType&); // not implemented
};


//
//  Helper class
//
class iDataTypePointer : public iPointer::Ordered<const iDataType>
{

public:

	iDataTypePointer(const iDataType *ptr = 0) : iPointer::Ordered<const iDataType>(ptr){}
};


class iDataInfo
{

public:

	static const iDataInfo& None();
	static const iDataInfo& Any();

	iDataInfo();
	iDataInfo(const iDataInfo &info);
	iDataInfo(const iDataType &type);
	~iDataInfo();

	void Clear();
	inline int Size() const { return mArr.Size(); }
	const iDataType& Type(int i) const;
	int Index(const iDataType &type) const;

	iDataInfo& operator=(const iDataInfo &info);
	iDataInfo& operator=(const iDataType &type);
	iDataInfo& operator+=(const iDataInfo &info);
	iDataInfo& operator+=(const iDataType &type);
	iDataInfo& operator-=(const iDataType &type);

	bool Includes(const iDataType &type) const;

private:

	iOrderedArray<iDataTypePointer> mArr;
};


inline bool iDataType::operator==(const iDataType &type) const
{
	return mId == type.mId;
}


inline bool iDataType::operator!=(const iDataType &type) const
{
	return mId != type.mId;
}


inline const iDataInfo operator+(const iDataInfo &info, const iDataType &type)
{
	iDataInfo tmp(info);
	tmp += type;
	return tmp;
}


inline const iDataInfo operator+(const iDataInfo &info1, const iDataInfo &info2)
{
	iDataInfo tmp(info1);
	tmp += info2;
	return tmp;
}


inline const iDataInfo operator-(const iDataInfo &info, const iDataType &type)
{
	iDataInfo tmp(info);
	tmp -= type;
	return tmp;
}

#endif // IDATA_H
