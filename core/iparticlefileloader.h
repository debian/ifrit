/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Abstract DataLoader for reading various particle files
//
#ifndef IPARTICLEFILELOADER_H
#define IPARTICLEFILELOADER_H


#include "ifileloader.h"


#include "iparticledownsampleiterator.h"

class iParticleDensityEstimator;
class iParticleHelper;

class vtkCellArray;
class vtkDataArray;
class vtkDoubleArray;
class vtkFloatArray;
class vtkPoints;
class vtkPolyData;


class iParticleFileLoader : public iFileLoader
{

	friend class iParticleHelper;

public:

	vtkTypeMacro(iParticleFileLoader,iFileLoader);

	void SetDownsampleMode(int v);
	int GetDownsampleMode() const;

	void SetTypeIncluded(int type, bool s);
	bool GetTypeIncluded(int type) const;

	void SetDownsampleFactor(int type, int value);
	int GetDownsampleFactor(int type) const;

	void SetDensityVariable(int type, int var);
	int GetDensityVariable(int type) const;

	void SetAddOrderAsVariable(int type, bool s);
	bool GetAddOrderAsVariable(int type) const;

	void SetNumNeighbors(int num);
	int GetNumNeighbors() const;

	inline bool IsUsingExtras() const { return mUseExtras; }

protected:

	iParticleFileLoader(iDataReader *r, int priority = 300, bool useExtras = true);
	iParticleFileLoader(iDataReaderExtension *ext, int priority = 300, bool useExtras = true);
	virtual ~iParticleFileLoader();

	struct ParticleStreamExtras
	{
		int DensityVariable;
		bool AddOrderAsVariable;

		ParticleStreamExtras();
	};

	struct ParticleStream : public Stream
	{
		bool Included, InFile;
		int DownsampleFactor;
		int NumVariablesInFile, NumVariablesInData;
		vtkIdType NumTotal;
		vtkIdType NumSelected;
		ParticleStreamExtras Extras;
		vtkPolyData *Data;

		ParticleStream();
		virtual ~ParticleStream();
		vtkPoints* Points() const;
		vtkCellArray* Verts() const;
		vtkFloatArray* Scalars() const;
	};
	virtual Stream* CreateNewStream() const;
	inline ParticleStream* GetStream(int i) const { return static_cast<ParticleStream*>(iFileLoader::GetStream(i)); } // no bound checking for speed

	virtual void LoadFileBody(const iString &suffix, const iString &fname);
	virtual void ShiftDataBody(vtkDataSet *data, const double *dx);

	virtual void LoadParticleFileBody(const iString &suffix, const iString &fname) = 0;

	virtual void AddExtras(ParticleStream *stream);

	//
	//  helper functions
	//
	void WrapPositions(vtkPoints *points);
	void FinalizePolyData(vtkPolyData *data);
	void ShiftPolyData(vtkPolyData *data, int d, double dx);

	bool ReadPositions(iFile &F, int inNum, int outOffset, double updateStart, double updateDuration, float *scale, float *offset);
	bool ReadPositions(iFile &F, int inNum, int outOffset, double updateStart, double updateDuration, double *scale, double *offset);
	bool ReadVariables(iFile &F, int inNum, int outOffset, double updateStart, double updateDuration, float *scale = 0);
	bool ReadIntVariables(iFile &F, int inNum, int outOffset, double updateStart, double updateDuration);

	void AutoScalePositions();

	void ConfigureStreams(vtkIdType *ntot, int *nvar, bool paf = true);

	iParticleDownsampleIterator mIterator; // value, not the pointer, for efficiency (to remove one level of indirection in loops)
	iParticleDensityEstimator *mDensityEstimator;

private:

	void Define();

	//
	//  Members
	//
	const bool mUseExtras;
	bool mHaveNormals;
	iParticleHelper *mHelper;
};


inline bool iParticleFileLoader::GetTypeIncluded(int type) const
{
	if(type>=0 && type<this->NumStreams())
	{
		return this->GetStream(type)->Included;
	}
	else return false;
}


inline int iParticleFileLoader::GetDownsampleFactor(int type) const
{
	if(type>=0 && type<this->NumStreams())
	{
		return this->GetStream(type)->DownsampleFactor;
	}
	else return 1;
}


inline int iParticleFileLoader::GetDensityVariable(int type) const
{
	if(type>=0 && type<this->NumStreams())
	{
		return this->GetStream(type)->Extras.DensityVariable;
	}
	else return 1;
}


inline bool iParticleFileLoader::GetAddOrderAsVariable(int type) const
{
	if(type>=0 && type<this->NumStreams())
	{
		return this->GetStream(type)->Extras.AddOrderAsVariable;
	}
	else return false;
}

#endif

