/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iprobefilter.h"


#include "idata.h"
#include "idatalimits.h"
#include "ierror.h"
#include "iuniformgriddata.h"
#include "iviewmodule.h"

#include <vtkDataObjectTypes.h>
#include <vtkDataSet.h>
#include <vtkFloatArray.h>
#include <vtkImageData.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>

//
//  Templates (needed for some compilers)
//
#include "igenericfilter.tlh"


iProbeFilter::iProbeFilter(iDataConsumer *consumer) : iGenericFilter<vtkProbeFilter,vtkDataSet,vtkDataSet>(consumer,2,true)
{
	mRestrictedType = 0;
}


void iProbeFilter::RestrictToType(const char *name)
{
	if(vtkDataObjectTypes::GetTypeIdFromClassName(name) != -1)
	{
		mRestrictedType = name;
	}
}


//
//  This filter is too generic, we need to specialize the output depending on the input
//
int iProbeFilter::FillInputPortInformation(int port, vtkInformation *info)
{
	if(mRestrictedType == 0)
	{
		return this->vtkProbeFilter::FillInputPortInformation(port,info);
	}
	else
	{
		info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(),mRestrictedType);
		return 1;
	}
}


int iProbeFilter::FillOutputPortInformation(int port, vtkInformation *info)
{
	if(mRestrictedType == 0)
	{
		return this->vtkProbeFilter::FillOutputPortInformation(port,info);
	}
	else
	{
		info->Set(vtkDataObject::DATA_TYPE_NAME(),mRestrictedType);
		return 1;
	}
}


void iProbeFilter::ProvideOutput()
{
	vtkDataSet *input = this->InputData();
	vtkDataSet *output = this->OutputData();
	vtkDataSet *source = this->SourceData();

	if(source->IsA("vtkImageData") && input->IsA("vtkPointSet") && output->IsA("vtkPointSet"))
	{
		this->ProduceOutputForImageData(vtkPointSet::SafeDownCast(input),vtkPointSet::SafeDownCast(output),vtkImageData::SafeDownCast(source));
	}
	else
	{
		output->ShallowCopy(input); // Probe does not copy normals...
		this->vtkProbeFilter::Probe(input,source,output);
	}
}


vtkDataSet* iProbeFilter::SourceData()
{
	return iRequiredCast<vtkDataSet>(INFO,this->GetSource());
}


void iProbeFilter::ProduceOutputForImageData(vtkPointSet *input, vtkPointSet *output, vtkImageData *source)
{
	output->ShallowCopy(input);

	vtkPoints *inpts = input->GetPoints();
	if(inpts == 0) return;

	vtkIdType numPts = inpts->GetNumberOfPoints();

	if(!wArray.Init(source->GetPointData()->GetScalars(),numPts))
	{
		this->SetAbortExecute(1);
		return;
	}
	
	int j, dims[3], ijk1[3], ijk2[3];
	double org[3], spa[3];
	source->GetDimensions(dims);
	source->GetSpacing(spa);
	source->GetOrigin(org);

	vtkIdType l, loff, loff111, loff112, loff121, loff122, loff211, loff212, loff221, loff222;
	double x[3];
	double d1[3], d2[3];

	bool per[3];
	this->GetPeriodicities(per);

	//	
	// Loop over all input points, interpolating source data
	//
	for(l=0; l<numPts; l++)
    {
		if(l%1000 == 0)
		{
			this->UpdateProgress(double(l)/numPts);
			if(this->GetAbortExecute()) break;
		}
		
		// Get the xyz coordinate of the point in the input dataset
		inpts->GetPoint(l,x);

		// Find the cell that contains xyz and get it
		iUniformGridData::CIC(per,dims,org,spa,x,ijk1,ijk2,d1,d2);
		loff111	= wArray.DimIn*(ijk1[0]+dims[0]*(ijk1[1]+(vtkIdType)dims[1]*ijk1[2]));
		loff112	= wArray.DimIn*(ijk1[0]+dims[0]*(ijk1[1]+(vtkIdType)dims[1]*ijk2[2]));
		loff121	= wArray.DimIn*(ijk1[0]+dims[0]*(ijk2[1]+(vtkIdType)dims[1]*ijk1[2]));
		loff122	= wArray.DimIn*(ijk1[0]+dims[0]*(ijk2[1]+(vtkIdType)dims[1]*ijk2[2]));
		loff211	= wArray.DimIn*(ijk2[0]+dims[0]*(ijk1[1]+(vtkIdType)dims[1]*ijk1[2]));
		loff212	= wArray.DimIn*(ijk2[0]+dims[0]*(ijk1[1]+(vtkIdType)dims[1]*ijk2[2]));
		loff221	= wArray.DimIn*(ijk2[0]+dims[0]*(ijk2[1]+(vtkIdType)dims[1]*ijk1[2]));
		loff222	= wArray.DimIn*(ijk2[0]+dims[0]*(ijk2[1]+(vtkIdType)dims[1]*ijk2[2]));
		
		loff = l*wArray.DimOut;
		for(j=0; j<wArray.DimOut; j++)
		{
			wArray.PtrOut[loff+j] = 
				wArray.PtrIn[loff111+j]*d1[0]*d1[1]*d1[2] +
				wArray.PtrIn[loff112+j]*d1[0]*d1[1]*d2[2] +
				wArray.PtrIn[loff121+j]*d1[0]*d2[1]*d1[2] +
				wArray.PtrIn[loff122+j]*d1[0]*d2[1]*d2[2] +
				wArray.PtrIn[loff211+j]*d2[0]*d1[1]*d1[2] +
				wArray.PtrIn[loff212+j]*d2[0]*d1[1]*d2[2] +
				wArray.PtrIn[loff221+j]*d2[0]*d2[1]*d1[2] +
				wArray.PtrIn[loff222+j]*d2[0]*d2[1]*d2[2];
		}
    }

	output->GetPointData()->SetScalars(wArray.ArrOut);
	wArray.ArrOut->Delete();
 }
