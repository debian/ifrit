/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ifieldglyphpipeline.h"


#include "idatalimits.h"
#include "ifieldviewsubject.h"
#include "ireplicatedpolydata.h"
#include "iresampleimagedatafilter.h"

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"
#include "igenericfilter.tlh"
#include "iviewsubjectpipeline.tlh"


iPipelineKeyDefineMacro(iFieldGlyphPipeline,GlyphSize);
iPipelineKeyDefineMacro(iFieldGlyphPipeline,GlyphSampleRate);

	
//
//  generic iFieldGlyphPipeline class
//
iFieldGlyphPipeline::iFieldGlyphPipeline(iFieldViewInstance *owner) : iReplicatedViewSubjectPipeline(owner,1)
{
	mOwner = owner;

	//
	//  Do VTK stuff
	//	
	mResampleFilter = this->CreateFilter<iResampleImageDataFilter>();
}


iFieldGlyphPipeline::~iFieldGlyphPipeline()
{
}


bool iFieldGlyphPipeline::PrepareInput()
{
	vtkDataSet *input = this->InputData();

	if(mResampleFilter!=0 && input->IsA("vtkImageData"))  // this is so that child classes can circumvent the resample filter by deleting it.
	{
		vtkImageData *inp = vtkImageData::SafeDownCast(input);
		if(mResampleFilter->InputData() != inp)
		{
			mResampleFilter->SetInputData(inp);
		}
	}
	else
	{
		this->SetGlyphFilterInput(input);
	}

	//
	//  Needed so that the glyph size is correct
	//
	this->UpdateGlyphSize();

	return true;
}


void iFieldGlyphPipeline::UpdateGlyphSize()
{
	//
	//  Do not update unless necessary
	//
	if(mOwner->CanBeShown())
	{
		double s = 10.0*mOwner->GetGlyphSize()*2.0/this->GetLimits()->GetMax(0); // 2.0 is the box size
		this->SetGlyphFilterScale(s);
		this->Modified();
	}
}


void iFieldGlyphPipeline::UpdateGlyphSampleRate()
{ 
	mResampleFilter->SetResampleRate(mOwner->GetGlyphSampleRate());
	this->Modified();
}

