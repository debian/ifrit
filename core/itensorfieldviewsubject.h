/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef ITENSORFIELDVIEWSUBJECT_H
#define ITENSORFIELDVIEWSUBJECT_H


#include "ifieldviewsubject.h"


class iPolyDataExtender;
class iTensorFieldViewSubject;
class iViewObject;


class iTensorFieldViewInstance : public iFieldViewInstance
{
	
	friend class iTensorFieldViewSubject;

public:
	
	iDataConsumerTypeMacroPass(iTensorFieldViewInstance,iFieldViewInstance);
		
	iObjectSlotMacro1SV(Method,int);
	iObjectSlotMacro1SV(Scaling,bool);
	iObjectSlotMacro1SV(GlyphType,int);

	virtual bool CanBeShown() const;

protected:
	
	iTensorFieldViewInstance(iTensorFieldViewSubject *owner);
	virtual ~iTensorFieldViewInstance();
	virtual void ConfigureBody();
	virtual void ConfigureMainPipeline(iViewSubjectPipeline *p, int id);

	virtual iViewSubjectPipeline* CreatePipeline(int id);
	virtual void ResetPipelineInput(vtkDataSet *input);

	virtual void ShowBody(bool s);

	virtual void UpdatePainting();
	virtual void UpdateGlyphSize();
	virtual void UpdateGlyphSampleRate();
};


class iTensorFieldViewSubject : public iFieldViewSubject
{
	
public:
	
	iViewSubjectTypeMacro2(iTensorFieldView,iFieldViewSubject);
		
	iType::vsp_int Method;
	iType::vsp_bool Scaling;
	iType::vsp_int GlyphType;

protected:
	
	iTensorFieldViewSubject(iViewObject *parent, const iDataType &type, const iDataType &stype);
	virtual ~iTensorFieldViewSubject();
};

#endif // ITENSORFIELDVIEWSUBJECT_H

