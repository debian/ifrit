/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IDATALIMITS_H
#define IDATALIMITS_H


#include <vtkObjectBase.h>


#include "iarray.h"
#include "imath.h"
#include "ipair.h"
#include "istretch.h"
#include "istring.h"

#include <vtkSetGet.h>

class iDataSubject;
class iDataType;


namespace iParameter
{
	namespace LimitsChangeMode
	{
		const int Name =    0;
		const int Range =   1;
		const int Stretch = 2;
	};
};


class iDataLimits : public vtkObjectBase
{

	friend class iDataSubject;
	friend class iEdition;
	friend class iFileLoader;

public:
	
	vtkTypeMacro(iDataLimits,vtkObjectBase);
	static iDataLimits* New(const iDataSubject *subject = 0);

	int GetNumVars() const;

	const iPair& GetRange(int n) const;
	const iString& GetStretch(int n) const;
	int GetStretchId(int n) const;

	float GetMax(int n) const;
	float GetMin(int n) const;
	float GetStretchedMax(int n) const;
	float GetStretchedMin(int n) const;

	unsigned int GetRank(int n) const;
	const iString& GetName(int n) const;
	const iString& GetUnit(int n) const;
	bool GetStretchFixed(int n) const;
	const iString& GetNameForClass(int n) const; // GetClassName conflicts with something under VC++

	bool SetRange(int n, const iPair& p);
	bool SetName(int n, const iString& v);
	bool SetStretch(int n, const iString& v);

	inline bool IsFrozen() const { return mIsFrozen; }

	const iDataType& GetDataType() const;
	//const iDataSubject* GetDataSubject() const { return mSubject; }
	const iPair GetDataRange(int n) const;
	float GetDataMax(int n) const;
	float GetDataMin(int n) const;

	void BlockNotifications(bool b);
	bool Resize(int n);  // calls DeclareVars unless frozen

protected:
	
	iDataLimits(const iDataSubject *subject, bool isfrozen);
	virtual ~iDataLimits();

	bool Activate(int nvars);
	void DeclareVars(int n); // for child use
	void AddOneVar(int ndecl = -1);

	virtual bool AddVars(int nvars);

	struct Var
	{
		iPair Range;
		int StretchId;
		unsigned int Rank;
		bool Included, StretchFixed;
		iString Name, Unit, ClassName;
		Var(int n = -1);
	};

	//
	//  Protected for children to be able to fill in directly
	//
	iArray<Var> mDeclaredVars; 

private:

	Var* CacheVar(int n);
	const Var* CacheVar(int n) const;
	void AddAllDeclared(bool s); // helper for the Subject to save a state

	const iDataSubject *mSubject;
	const bool mIsFrozen;

	iLookupArray<Var*> mVars, mVarsBackup;
	bool mBlockNotifications, mIsActive;
};


inline const iPair& iDataLimits::GetRange(int n) const
{
	static const iPair none(-iMath::_LargeFloat,iMath::_LargeFloat);
	const Var *v = this->CacheVar(n);
	if(v != 0) return v->Range; else return none;
}


inline float iDataLimits::GetMax(int n) const
{
	const Var *v = this->CacheVar(n);
	if(v != 0) return v->Range.Max; else return iMath::_LargeFloat;
}


inline float iDataLimits::GetMin(int n) const
{
	const Var *v = this->CacheVar(n);
	if(v != 0) return v->Range.Min; else return -iMath::_LargeFloat;
}


inline int iDataLimits::GetStretchId(int n) const
{
	const Var *v = this->CacheVar(n);
	if(v != 0) return v->StretchId; else return -1;
}


inline const iString& iDataLimits::GetName(int n) const
{
	static const iString null;
	const Var *v = this->CacheVar(n);
	if(v != 0) return v->Name; else return null;
}


inline const iString& iDataLimits::GetUnit(int n) const
{
	static const iString null;
	const Var *v = this->CacheVar(n);
	if(v != 0) return v->Unit; else return null;
}


inline bool iDataLimits::GetStretchFixed(int n) const
{
	const Var *v = this->CacheVar(n);
	if(v != 0) return v->StretchFixed; else return false;
}


inline unsigned int iDataLimits::GetRank(int n) const
{
	const Var *v = this->CacheVar(n);
	if(v != 0) return v->Rank; else return (unsigned int)-1;
}


inline const iString& iDataLimits::GetNameForClass(int n) const
{
	static const iString null;
	const Var *v = this->CacheVar(n);
	if(v != 0) return v->ClassName; else return null;
}


inline void iDataLimits::BlockNotifications(bool b)
{ 
	mBlockNotifications = b;
}

#endif // IDATALIMITS_H

