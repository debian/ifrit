/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "icrosssectionpipeline.h"


#include "icrosssectionviewsubject.h"
#include "ierror.h"
#include "iorthopolygonplanefilter.h"
#include "iorthoslicer.h"
#include "iorthotextureplanefilter.h"

#include <vtkImageData.h>

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"
#include "igenericfilter.tlh"
#include "iviewsubjectpipeline.tlh"


iPipelineKeyDefineMacro(iCrossSectionPipeline,Method);
iPipelineKeyDefineMacro(iCrossSectionPipeline,Location);
iPipelineKeyDefineMacro(iCrossSectionPipeline,Painting);
iPipelineKeyDefineMacro(iCrossSectionPipeline,Direction);
iPipelineKeyDefineMacro(iCrossSectionPipeline,SampleRate);
iPipelineKeyDefineMacro(iCrossSectionPipeline,InterpolateData);

	
//
// Main class
//
iCrossSectionPipeline::iCrossSectionPipeline(iCrossSectionViewInstance *owner)  : iViewSubjectPipeline(owner,1)
{
	mOwner = owner;

	//
	//  Do VTK stuff
	//	
	mSlicer = this->CreateFilter<iOrthoSlicer>();
	mPolygonConverter = this->CreateFilter<iOrthoPolygonPlaneFilter>();
	mTextureConverter = this->CreateFilter<iOrthoTexturePlaneFilter>();
	
	mSlicer->SetDir(mOwner->GetDirection());
	mSlicer->SetCurrentVar(mOwner->GetPaintVar());

	mPolygonConverter->SetInputConnection(mSlicer->GetOutputPort());
	mTextureConverter->SetInputConnection(mSlicer->GetOutputPort());
	
	this->UpdateContents();
}


iCrossSectionPipeline::~iCrossSectionPipeline()
{
}


void iCrossSectionPipeline::ProvideOutput()
{
	vtkPolyData *output = this->OutputData();
	vtkImageData *input = vtkImageData::SafeDownCast(this->InputData());
	if(input == 0)
	{
		IBUG_ERROR("iCrossSectionPipeline is configured incorrectly.");
		return;
	}

	if(mSlicer->InputData() != input)
	{
		mSlicer->SetInputData(input);
	}
	
	switch(mOwner->GetActualMethod())
	{
	case 0:
		{
			mPolygonConverter->Update();
			output->ShallowCopy(mPolygonConverter->OutputData());
			break;
		}
	case 1: 
		{
			//
			//  Need to assign texture offsets first
			//
			this->SetTextureOffset(input);
			mTextureConverter->Update(); 
			output->ShallowCopy(mTextureConverter->OutputData());
			break;
		}
	default: return;
	}
}


void iCrossSectionPipeline::SetTextureOffset(vtkImageData *input)
{
	//
	//  Assign texture offsets
	//
	if(mOwner->GetMethod() == 1)
	{
		double org[3], globalOrg[3], spa[3];
		int dims[3], globalDims[3];
		int Uidx, Vidx;
		int Axis = mOwner->GetDirection();
		iOrthoSlicer::GetUV(Axis,Uidx,Vidx);

		input->GetOrigin(org);
		input->GetSpacing(spa);
		input->GetDimensions(dims);

		dims[Uidx] /= mOwner->GetSampleRate();
		dims[Vidx] /= mOwner->GetSampleRate();

		vtkImageData *globalInput = vtkImageData::SafeDownCast(mGlobalInput);
		if(globalInput == 0)
		{
			IBUG_WARN("Incompatible global input.");
			return;
		}

		globalInput->GetOrigin(globalOrg);
		globalInput->GetDimensions(globalDims);

		globalDims[Uidx] /= mOwner->GetSampleRate();
		globalDims[Vidx] /= mOwner->GetSampleRate();

		if(dims[Axis] != globalDims[Axis])  
		{
			//
			//  Split along Axis
			//
			mTextureConverter->SetTexturePiece(mOwner->GetTextureData(),0,0,globalOrg,globalDims);
		}
		else
		{
			int offx = iMath::Round2Int((org[Uidx]-globalOrg[Uidx])/spa[Uidx]);
			int offy = iMath::Round2Int((org[Vidx]-globalOrg[Vidx])/spa[Vidx]);
			mTextureConverter->SetTexturePiece(mOwner->GetTextureData(),offx,offy,globalOrg,globalDims);
		}
	}
}


void iCrossSectionPipeline::UpdateMethod()
{
	switch(mOwner->GetActualMethod())
	{
	case 0:
		{
			mPolygonConverter->Modified();
			break;
		}
	case 1: 
		{
			mTextureConverter->Modified(); 
			break;
		}
	default: return;
	}
	this->Modified();
}


void iCrossSectionPipeline::UpdateDirection()
{ 
	mSlicer->SetDir(mOwner->GetDirection());
	mSlicer->SetPos(mOwner->GetLocation());
	this->Modified();
}


void iCrossSectionPipeline::UpdatePainting()
{ 
	mSlicer->SetCurrentVar(mOwner->GetPaintVar());
	this->Modified();
}


void iCrossSectionPipeline::UpdateLocation()
{ 
	mSlicer->SetPos(mOwner->GetLocation());
	this->Modified();
}


void iCrossSectionPipeline::UpdateInterpolateData()
{
	mPolygonConverter->SetInterpolation(mOwner->GetInterpolateData());
	mSlicer->SetInterpolation(mOwner->GetInterpolateData());
	this->Modified();
}


void iCrossSectionPipeline::UpdateSampleRate()
{
	mSlicer->SetSampleRate(mOwner->GetSampleRate());
	this->Modified();
}


void iCrossSectionPipeline::UpdateReplicas()
{
	// Not replicatable
}

