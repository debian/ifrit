/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "idatareader.h"


#include "ibuffer.h"
#include "icoredatasubjects.h"
#include "icorefileloaders.h"
#include "idata.h"
#include "idatalimits.h"
#include "idatareaderobserver.h"
#include "idatasubject.h"
#include "idirectory.h"
#include "iedition.h"
#include "ierror.h"
#include "ifile.h"
#include "imonitor.h"
#include "iobjectfactory.h"
#include "iparallelmanager.h"
#include "iparallelworker.h"
#include "ishell.h"
#include "ishelleventobservers.h"
#include "iuniformgriddata.h"
#include "iuniformgridfileloader.h"
#include "iviewmodule.h"

#include <vtkArrayCalculator.h>

//
//  Templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


using namespace iParameter;


//
//  Main class
//
iDataReader* iDataReader::New(iViewModule* vm)
{
	static iString LongName("DataReader");
	static iString ShortName("dr");

	int i, j, k;

	IASSERT(vm);
	iDataReader *tmp = new iDataReader(vm,LongName,ShortName); IERROR_CHECK_MEMORY(tmp);
	iObjectFactory::InstallExtensions(tmp);

	iFileLoader *s;
	for(i=0; i<tmp->mExtensions.Size(); i++)
	{
		j = 0;
		while((s = iRequiredCast<iDataReaderExtension>(INFO,tmp->mExtensions[i])->GetLoader(j++)) != 0)
		{
			for(k=0; k<s->NumStreams(); k++)
			{
				iEdition::ApplySettings(s->GetStream(k)->Subject);
			}
			tmp->mLoaders.Add(s);
		}
	}

	iEdition::ApplySettings(tmp);

	return tmp;
}


iDataReader::iDataReader(iViewModule *vm, const iString &fname, const iString &sname) : iExtendableObject(vm,fname,sname), iViewModuleComponent(vm),
	iObjectConstructPropertyMacroS(Int,iDataReader,BoundaryConditions,bc),
	iObjectConstructPropertyMacroS(Int,iDataReader,ScaledDimension,sd),
	iObjectConstructPropertyMacroS(Int,iDataReader,VoxelLocation,vl),
	iObjectConstructPropertyMacroS(Int,iDataReader,MaxZDimension,mzd),
	iObjectConstructPropertyMacroV(Double,iDataReader,Shift,s,3),
	iObjectConstructPropertyMacroA2(DataType,String,iDataReader,Load,l),
	iObjectConstructPropertyMacroA1(DataType,iDataReader,Erase,e),
	iObjectConstructPropertyMacroA(iDataReader,Reload,r)
{
	mRenderMode = RenderMode::Clones;

	mRecordLength = 4; 

	mTwoCopies = mInExtensionUpdate = false;

	mCellToPointMode = 2;
	mVoxelLocation = VoxelLocation::Vertex;
	mBoundaryConditions = BoundaryConditions::None;

	//delete mPropertyStack;
	//mPropertyStack = new iDataReaderPropertyStack(this); IERROR_CHECK_MEMORY(mPropertyStack);

	mVectorLoader = new iCoreData::VectorFileLoader(this); IERROR_CHECK_MEMORY(mVectorLoader);
    mScalarLoader = new iCoreData::ScalarFileLoader(this,mVectorLoader); IERROR_CHECK_MEMORY(mScalarLoader);
    mTensorLoader = new iCoreData::TensorFileLoader(this); IERROR_CHECK_MEMORY(mTensorLoader);
    mParticleLoader = new iCoreData::ParticleFileLoader(this); IERROR_CHECK_MEMORY(mParticleLoader);

	iCoreData::ScalarDataSubject *us = new iCoreData::ScalarDataSubject(mScalarLoader); IERROR_CHECK_MEMORY(us);
	iCoreData::VectorDataSubject *uv = new iCoreData::VectorDataSubject(mVectorLoader); IERROR_CHECK_MEMORY(uv);
	iCoreData::TensorDataSubject *ut = new iCoreData::TensorDataSubject(mTensorLoader); IERROR_CHECK_MEMORY(ut);
	iCoreData::ParticleDataSubject *bp = new iCoreData::ParticleDataSubject(mParticleLoader); IERROR_CHECK_MEMORY(bp);

	iEdition::ApplySettings(us);
	iEdition::ApplySettings(uv);
	iEdition::ApplySettings(ut);
	iEdition::ApplySettings(bp);

	mLoaders.Add(mScalarLoader);
	mLoaders.Add(mVectorLoader);
	mLoaders.Add(mTensorLoader);
	mLoaders.Add(mParticleLoader);

	mCurrentRecord = -1;
	mPeriodicityLeader = 0;

	mShift[0] = mShift[1] = mShift[2] = 0.0f;

	mScaledDimension = -1; //max dim
	mMaxZDimension = 0;

	mDataReloadInfo = new iDataInfo; IERROR_CHECK_MEMORY(mDataReloadInfo);

//	mLastFileName = mLastAttemptedFileName = "";

	mDirectory = new iDirectory; IERROR_CHECK_MEMORY(mDirectory);
}


iDataReader::~iDataReader()
{
	int i;

	delete mDirectory;
	delete mDataReloadInfo;

	for(i=0; i<mLoaders.Size(); i++) mLoaders[i]->Delete();
}


void iDataReader::EraseData(const iDataType &type)
{
	int i;

	for(i=0; i<mLoaders.Size(); i++)
	{
		if(mLoaders[i]->IsUsingData(type) && mLoaders[i]->IsThereData(type)) mLoaders[i]->EraseData();
	}
}


void iDataReader::SetTwoCopies(bool s)
{
	int i;

	if(mTwoCopies != s)
	{
		mTwoCopies = s;

		for(i=0; i<mLoaders.Size(); i++)
		{
			mLoaders[i]->SetTwoCopies(s);
		}
	}
}


void iDataReader::ReloadData(const iDataType &type)
{
	int i;

	for(i=0; i<mLoaders.Size(); i++)
	{
		if(mLoaders[i]->IsUsingData(type) && mLoaders[i]->IsThereData(type))
		{
			mLoaders[i]->LoadFile(mLoaders[i]->GetLastFileName(),mShift);
		}
	}
}


bool iDataReader::HasDataToReload() const
{
	return (mDataReloadInfo->Size()>0 && this->IsThereData(*mDataReloadInfo));
}


void iDataReader::RequestDataReload(const iDataInfo &info)
{
	if(this->IsThereData(info))
	{
		*mDataReloadInfo += info;
		
		if(this->GetShell()->GetDataReaderObserver()->AllowReload())
		{
			this->CallReload();
		}
	}
}


void iDataReader::ResetPipeline()
{
	int i;

	for(i=0; i<mLoaders.Size(); i++)
	{
		mLoaders[i]->Reset();
	}
}


bool iDataReader::IsThereData(const iDataInfo &info) const
{
	int i;

	for(i=0; i<info.Size(); i++)
	{
		if(this->IsThereData(info.Type(i))) return true;
	}
	return false;
}

	
bool iDataReader::IsThereData(const iDataType &type) const
{
	int i;

	for(i=0; i<mLoaders.Size(); i++)
	{
		if(mLoaders[i]->IsUsingData(type) && mLoaders[i]->IsThereData(type)) return true;
	}
	return false;
}


const iString& iDataReader::GetLastFileName(const iDataType &type) const
{
	static const iString null;
	int i;

	for(i=0; i<mLoaders.Size(); i++)
	{
		if(mLoaders[i]->IsUsingData(type)) return mLoaders[i]->GetLastFileName();
	}
	return null;
}


iDataLimits* iDataReader::GetLimits(const iDataType &type) const
{
	int i;

	for(i=0; i<mLoaders.Size(); i++)
	{
		if(mLoaders[i]->IsUsingData(type)) return mLoaders[i]->GetSubject(type)->GetLimits();
	}
	return 0;
}


iDataSubject* iDataReader::GetSubject(const iDataType &type) const
{
	int i;

	for(i=0; i<mLoaders.Size(); i++)
	{
		if(mLoaders[i]->IsUsingData(type)) return mLoaders[i]->GetSubject(type);
	}
	return 0;
}


bool iDataReader::IsSetLeader(const iFileLoader *loader) const
{
	return (mSetLoaders.Size()>0 && mSetLoaders[0]==loader);
}


void iDataReader::LoadFile(const iDataType &type, const iString &fname, bool savename)
{
	int i;
	
	//
	//  Find a loader that accepts this type.
	//
	for(i=0; i<mLoaders.Size(); i++)
	{
		if(mLoaders[i]->IsUsingData(type))
		{
			this->LoadFile(mLoaders[i],fname,savename);
			return;
		}
	}

	//
	//  We should not use a Monitor here, as it will wrap around DataReaderObserver->StartSet().
	//
	this->OutputText(MessageType::Error,"There is no loader that supports the data type <"+type.LongName()+">.");
}


void iDataReader::LoadFile(iFileLoader *loader, const iString &name, bool savename)
{
	if(loader == 0)
	{
		IBUG_WARN("Bug: trying to load file by a non-existent loader.");
	}

	int i;
	iString fname(name);
	iDirectory::ExpandFileName(fname);

	mLastAttemptedFileName = fname;

	this->GetViewModule()->GetParallelManager()->StartCounters();
	this->GetShell()->GetDataReaderObserver()->StartSet();

	//
	//  Extension can bring us in again here, so make sure the Monitor goes out of scope before the call to extension
	//
	{
		iMonitor h(this);
		loader->LoadFile(fname,mShift);
		if(h.IsStopped())
		{
			this->GetShell()->GetDataReaderObserver()->FinishSet(true);
			this->GetViewModule()->GetParallelManager()->StopCounters();
			return;
		}
	}

	iString root, suffix;
	int newrec = loader->DissectFileName(fname,root,suffix);

	//
	//  Update the set
	//
	if(newrec >= 0)
	{
		if(newrec == mCurrentRecord)
		{
			//
			//  Adding to the existent set if the subject is not there yet
			//
			if(mSetLoaders.Find(loader) < 0) mSetLoaders.Add(loader);
		}
		else
		{
			if(savename) this->InsertIntoVisitedFilesList(loader,fname);
			//
			//  Re-establish the set if we are not loading a new one
			//
			if(!this->IsSetLeader(loader))
			{
				this->BreakSet();
				mSetLoaders.Add(loader);
			}
			mCurrentRecord = newrec;
		}

		int m;
		iString s("File Set: ");
		for(m=0; m<mSetLoaders.Size(); m++)
		{
			if(m > 0) s += " + ";
			s += mSetLoaders[m]->GetDataType(0).TextName();
		}
		this->OutputText(MessageType::Information,s);
	}
	else
	{
		if(this->IsSetLeader(loader))
		{
			this->BreakSet();
		}
		if(savename) this->InsertIntoVisitedFilesList(loader,fname);
	}

	if(mPeriodicityLeader==0 || loader->GetPriority()<mPeriodicityLeader->GetPriority()) mPeriodicityLeader = loader;
	mLastFileName = fname;
	
	//
	//  Do we have a set? If yes, load the rest of it too
	//
	if(this->IsSetLeader(loader))
	{
		//
		//Is the filename consistent?
		//
		if(!mSetLoaders[0]->IsSeriesFileName(fname))
		{
			this->BreakSet();
		}
		else
		{
			//
			//  Load the data
			//
			iMonitor h(this);
			for(i=1; !h.IsStopped() && i<mSetLoaders.Size(); i++) // start at 1 since the leader has been loaded already
			{
				mSetLoaders[i]->LoadFile(mSetLoaders[i]->GetFileName(newrec),mShift);
			}
		}
	}

	this->GetViewModule()->UpdateAfterFileLoad();

	//
	//  If this reader has an extension, ask it do to its share of work, but guard against loops if it calls this function
	//
	if(loader->GetReaderExtension() != 0)
	{
		if(mInExtensionUpdate) return;
		mInExtensionUpdate = true;
		loader->GetReaderExtension()->AfterLoadFile(loader,fname);
		mInExtensionUpdate = false;
	}

	this->GetShell()->GetDataReaderObserver()->FinishSet(false);
	this->GetViewModule()->GetParallelManager()->StopCounters();
}


void iDataReader::BreakSet()
{
	mCurrentRecord = -1;
	mSetLoaders.Clear();
	mDirectory->Close();
}


bool iDataReader::LoadRecord(int rec, int skip, bool savename)
{
	iMonitor h(this);

	if(rec < 0) rec = mCurrentRecord;

	if(mSetLoaders.Size() == 0)
	{
		h.PostError("File set has not been established.");
		return true;
	}

	iString fname = mSetLoaders[0]->GetFileName(rec);
	if(!iFile::IsReadable(fname))
	{
		h.PostError("File is not accessible.");
		return true;
	}

	if(skip != 0)
	{
		if(!mDirectory->IsOpened() && !mDirectory->Open(fname))
		{
			h.PostError("Unable to read the directory.");
			return true;
		}
	
		while(skip>0 && !fname.IsEmpty())
		{
			fname = mDirectory->GetFile(fname,1);
			if(this->IsAnotherSetLeader(fname)) skip--;
		}

		//
		//  Is this filename from the same series?
		//
		if(!this->IsAnotherSetLeader(fname))
		{
			h.PostError("File "+fname+" does not exist.");
			return false;
		}
	}

	if(!fname.IsEmpty())
	{
		this->LoadFile(mSetLoaders[0],fname,savename);
		return true;
	}
	else
	{
		h.PostError("File "+fname+" does not exist.");
		return false;
	}
}


const iDataType& iDataReader::GetFileSetDataType(int member) const
{
	if(member<0 || member>=mSetLoaders.Size())
	{
		return iDataType::Null();
	}
	else
	{
		return mSetLoaders[member]->GetDataType(0);
	}
}


const iString& iDataReader::GetLastFileSetName() const
{
	static const iString none;

	if(mSetLoaders.Size() == 0) return none; else return mSetLoaders[0]->GetLastFileName();
}


const iString iDataReader::GetFileSetName(int rec) const
{
	static const iString none;
	
	if(mSetLoaders.Size()>0 && rec>=0) return mSetLoaders[0]->GetFileName(rec); else return none;
}


const iString iDataReader::GetFileSetRoot() const
{
	static const iString none;
	
	if(mSetLoaders.Size() > 0) return mSetLoaders[0]->GetFileRoot(); else return none;
}


bool iDataReader::IsCurrentSetMember(const iString &fname) const
{
	int i;
	for(i=0; i<mSetLoaders.Size(); i++)
	{
		if(mSetLoaders[i]->GetLastFileName() == fname) return true;
	}
	return false;
}


bool iDataReader::IsAnotherSetLeader(const iString &fname) const
{
	if(mSetLoaders.Size() > 0)
	{
		return mSetLoaders[0]->IsSeriesFileName(fname);
	}
	else return false;
}


bool iDataReader::CallLoad(const iDataType& type, const iString& name)
{
	iMonitor h(this);

	if(type.IsNull())
	{
		h.PostError("Invalid data type.");
		return false;
	}
	else
	{
		iString fname(name);
		if(fname[0] == '+')
		{
			fname.Replace(0,1,this->GetViewModule()->GetShell()->GetEnvironment(Environment::Data));
		}
		this->LoadFile(type,fname);
		return !h.IsStopped();
	}
}


bool iDataReader::CallErase(const iDataType& type)
{
	iMonitor h(this);

	if(type.IsNull())
	{
		h.PostError("Invalid data type.");
		return false;
	}
	else
	{
		this->EraseData(type);
		return !h.IsStopped();
	}
}


bool iDataReader::CallReload()
{
	int i;

	if(this->HasDataToReload())
	{
		iMonitor h(this);
		this->GetShell()->GetDataReaderObserver()->StartSet();
		for(i=0; i<mDataReloadInfo->Size(); i++)
		{
			this->ReloadData(mDataReloadInfo->Type(i));
		}
		this->GetShell()->GetDataReaderObserver()->FinishSet(h.IsStopped());
		mDataReloadInfo->Clear();
	}

	return true;
}


bool iDataReader::SetShift(int d, double dx)
{
	if(d>=0 && d<=2)
	{
		mShift[d] = dx;
		this->ShiftData();
		return true;
	}
	else return false;
}


void iDataReader::ShiftData(const double *dr)
{
	int i;

	if(dr == 0) dr = mShift;

	this->GetViewModule()->GetParallelManager()->StartCounters();

	for(i=0; i<mLoaders.Size(); i++) if(mLoaders[i]->IsThereData())
	{	
		mLoaders[i]->ShiftData(dr);
	}

	this->GetViewModule()->GetParallelManager()->StopCounters();
}


float iDataReader::GetMemorySize() const
{
	int i;
    float s = 0.0;
	
	for(i=0; i<mLoaders.Size(); i++)
	{	
		s += mLoaders[i]->GetMemorySize();
	}

	return s;
}


void iDataReader::InsertIntoVisitedFilesList(const iFileLoader *loader, const iString &name)
{
	VisitedFile v;
	v.Name = name;
	v.Loader = loader;
	mVisitedFilesList.Add(v);
}


//
//  Outputs
//
vtkDataSet* iDataReader::GetData(const iDataType &type) const
{
	int i;

	for(i=0; i<mLoaders.Size(); i++)
	{
		if(mLoaders[i]->IsUsingData(type))
		{
			return mLoaders[i]->GetData(type);
		}
	}

	return 0;
}


//
//  Decorator functions
//
bool iDataReader::SetBoundaryConditions(int v)
{
	int i;

	if(BoundaryConditions::IsValid(v))
	{
		mBoundaryConditions = v;
		for(i=0; i<mLoaders.Size(); i++)
		{
			this->RequestDataReload(mLoaders[i]->GetDataInfo());
		}
		return true;
	}
	else return false;
}


//
bool iDataReader::SetScaledDimension(int v)
{
	int i;

	iUniformGridFileLoader *ds;
	for(i=0; i<mLoaders.Size(); i++)
	{
		ds = iUniformGridFileLoader::SafeDownCast(mLoaders[i]);
		if(ds != 0)
		{
			ds->SetScaledDimension(v);
			mScaledDimension = ds->GetScaledDimension();
		}
	}
	return true;
}


bool iDataReader::SetMaxZDimension(int v)
{
	int i;

	iUniformGridFileLoader *ds;
	for(i=0; i<mLoaders.Size(); i++)
	{
		ds = iUniformGridFileLoader::SafeDownCast(mLoaders[i]);
		if(ds != 0)
		{
			ds->SetMaxZDimension(v);
			mMaxZDimension = ds->GetMaxZDimension();
		}
	}
	return true;
}


//
bool iDataReader::SetVoxelLocation(int v)
{
	int i;

	iUniformGridFileLoader *ds;
	for(i=0; i<mLoaders.Size(); i++)
	{
		ds = iUniformGridFileLoader::SafeDownCast(mLoaders[i]);
		if(ds != 0)
		{
			ds->SetVoxelLocation(v);
			mVoxelLocation = ds->GetVoxelLocation();
		}
	}
	return true;
}


//
//  Extension
//
iObjectExtensionAbstractConstructorBeginMacro(iDataReader)
{
}


void iDataReaderExtension::AfterLoadFile(iFileLoader *loader, const iString &fname)
{
	iMonitor h(loader);
	if(loader->GetReaderExtension() !=  this)
	{
		IBUG_ERROR("iDataReaderExtension is configured incorrectly.");
		h.PostError("Unable to read the data due to a bug.");
		return;
	}

	mLastFileName = fname;
	this->AfterLoadFileBody(loader,fname);
}


void iDataReaderExtension::AfterLoadFileBody(iFileLoader *, const iString &)
{
	//
	//  By default do nothing
	//
}

