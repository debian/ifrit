/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iboundingbox.h"


#include "iactor.h"
#include "iactorcollection.h"
#include "icamera.h"
#include "icubeaxesactor.h"
#include "ierror.h"
#include "irendertool.h"
#include "iviewmodule.h"

#include <vtkArrowSource.h>
#include <vtkCellArray.h>
#include <vtkCylinderSource.h>
#include <vtkFloatArray.h>
#include <vtkMapper.h>
#include <vtkPointData.h>
#include <vtkProperty.h>

//
//  Templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


using namespace iParameter;



namespace iBoundingBox_Private
{
	//
	//  Helper functions
	//
	iActor* CreateBoxA(iRenderTool *rt);
	iActorCollection* CreateBoxB(iRenderTool *rt);
	iActorCollection* CreateBoxC(iRenderTool *rt);
};


using namespace iBoundingBox_Private;


//
//  Main class
//
iBoundingBox* iBoundingBox::New(iViewModule *vm)
{
	static iString LongName("BoundingBox");
	static iString ShortName("bb");

	IASSERT(vm);
	return new iBoundingBox(vm,LongName,ShortName);
}


iBoundingBox::iBoundingBox(iViewModule *vm, const iString &fname, const iString &sname) : iViewModuleTool(vm,fname,sname),
	iObjectConstructPropertyMacroS(Int,iBoundingBox,Type,t),
	iObjectConstructPropertyMacroV(Float,iBoundingBox,AxesRanges,r,6),
	iObjectConstructPropertyMacroV(String,iBoundingBox,AxesLabels,l,3)
{
	//
	//  Bounding boxes
	//
	mBox0Actor = CreateBoxA(this->GetViewModule()->GetRenderTool());
	mBox1Actor = CreateBoxB(this->GetViewModule()->GetRenderTool());
	mBox2Actor = CreateBoxA(this->GetViewModule()->GetRenderTool());
	mBox3Actor = CreateBoxC(this->GetViewModule()->GetRenderTool());
	
	mBox0Actor->GetMapper()->ScalarVisibilityOn();
	mBox0Actor->GetProperty()->SetLineWidth(4.0);
	mBox2Actor->GetMapper()->ScalarVisibilityOff();
	mBox2Actor->GetProperty()->SetLineWidth(1.0);

	mBox3AxesLabels = iCubeAxesActor::New(this->GetViewModule()->GetRenderTool()); IERROR_CHECK_MEMORY(mBox3AxesLabels);
	mBox3AxesLabels->PickableOff();
	mBox3AxesLabels->VisibilityOff();
	mBox3AxesLabels->SetCamera(this->GetViewModule()->GetRenderTool()->GetCamera()->GetDevice());
	this->SetAxesLabels(0,"X");
	this->SetAxesLabels(1,"Y");
	this->SetAxesLabels(2,"Z");
	this->SetAxesRanges(0,-1.0);
	this->SetAxesRanges(1,1.0);
	this->SetAxesRanges(2,-1.0);
	this->SetAxesRanges(3,1.0);
	this->SetAxesRanges(4,-1.0);
	this->SetAxesRanges(5,1.0);
	this->GetViewModule()->GetRenderTool()->AddObject(mBox3AxesLabels);

	//
	//  Set bounding box
	//	
	mType = -1;
	this->SetType(BoundingBoxType::Default);
}


iBoundingBox::~iBoundingBox()
{
	
	this->GetViewModule()->GetRenderTool()->RemoveObject(mBox3AxesLabels);
	mBox3AxesLabels->Delete();

	this->GetViewModule()->GetRenderTool()->RemoveObject(mBox0Actor);
	this->GetViewModule()->GetRenderTool()->RemoveObject(mBox2Actor);
	mBox0Actor->Delete();
	mBox1Actor->Delete();
	mBox2Actor->Delete();
	mBox3Actor->Delete();
}


void iBoundingBox::SetMagnification(int m)
{
	if(m > 0)
	{
		mBox0Actor->GetProperty()->SetLineWidth(4.0*m);
		mBox2Actor->GetProperty()->SetLineWidth(1.0*m);
	}
}


void iBoundingBox::SetColor(const iColor &c)
{
	mBox2Actor->GetProperty()->SetColor(c.ToVTK());
}


bool iBoundingBox::SetAxesLabels(int i, const iString& label)
{
	switch(i)
	{
	case 0:
		{
			mAxesLabels[0] = label;
			mBox3AxesLabels->SetXLabel(label.ToCharPointer());
			return true;
		}
	case 1:
		{
			mAxesLabels[1] = label;
			mBox3AxesLabels->SetYLabel(label.ToCharPointer());
			return true;
		}
	case 2:
		{
			mAxesLabels[2] = label;
			mBox3AxesLabels->SetZLabel(label.ToCharPointer());
			return true;
		}
	default:
		{
			return false;
		}
	}
}


bool iBoundingBox::SetAxesRanges(int i, float v)
{
	if(i>=0 && i<6)
	{
		double b[6];
		mBox3AxesLabels->GetRanges(b);
		b[i] = v;
		mBox3AxesLabels->SetRanges(b);
		return true;
	}
	else return false;
}


float iBoundingBox::GetAxesRanges(int i) const
{
	if(i>=0 && i<6)
	{
		return mBox3AxesLabels->GetRanges()[i];
	}
	else return 0.0f;
}


bool iBoundingBox::ShowBody(bool s)
{
	mBox0Actor->VisibilityOff(); 
	mBox1Actor->VisibilityOff(); 
	mBox2Actor->VisibilityOff();
	mBox3Actor->VisibilityOff();
	mBox3AxesLabels->VisibilityOff();

	if(s)
	{
		switch(mType)
		{
		case BoundingBoxType::Default:
			{
				mBox0Actor->VisibilityOn();
				break;
			}
		case BoundingBoxType::Classic:
			{
				mBox1Actor->VisibilityOn();
				break;
			}
		case BoundingBoxType::HairThin:
			{
				mBox2Actor->VisibilityOn();
				break;
			}
		case BoundingBoxType::Axes:
			{
				mBox3Actor->VisibilityOn();
				mBox3AxesLabels->VisibilityOn();
				break;
			}
		default: return false;
		}
	}
	return true;
}


bool iBoundingBox::SetType(int t)
{
	if(BoundingBoxType::IsValid(t))
	{
		mType = t;
		this->ShowBody(mIsVisible);
		return true;
	}
	else return false;
}


//
//  Helper functions in a private namespace
//
namespace iBoundingBox_Private
{
	iActor* CreateBoxA(iRenderTool *rt)
	{
		//
		//  Bounding box 1
		//
		static float x[8][3]={{-1,-1,-1}, {1,-1,-1}, {1,1,-1}, {-1,1,-1}, {-1,-1,1}, {1,-1,1}, {1,1,1}, {-1,1,1}};
		static vtkIdType lns[12][2]={{0,1}, {1,2}, {2,3}, {3,0}, {0,4}, {1,5}, {2,6}, {3,7}, {4,5}, {5,6}, {6,7}, {7,4}};

		vtkPolyData *pd = vtkPolyData::New(); IERROR_CHECK_MEMORY(pd);
		vtkPoints *points = vtkPoints::New(VTK_FLOAT); IERROR_CHECK_MEMORY(points);
		vtkCellArray *polys = vtkCellArray::New(); IERROR_CHECK_MEMORY(polys);
		vtkFloatArray *scalars = vtkFloatArray::New(); IERROR_CHECK_MEMORY(scalars);
		//
		// Load the point, cell, and data attributes.
		//
		int i;
		for (i=0; i<8; i++) points->InsertPoint(i,x[i]);
		for (i=0; i<8; i++) scalars->InsertTuple1(i,(i<4));
		for (i=0; i<12; i++) polys->InsertNextCell(2,lns[i]);
		//
		// We now assign the pieces to the vtkPolyData.
		//
		pd->SetLines(polys);
		polys->Delete();
		pd->SetPoints(points);
		points->Delete();
		pd->GetPointData()->SetScalars(scalars);
		scalars->Delete();
		//
		//  Configure the actor
		//
		iActor* box1Actor = iActor::New(); IERROR_CHECK_MEMORY(box1Actor);
		box1Actor->PickableOff();
		box1Actor->VisibilityOff();
		//	mBox1Actor->SetScalarRange(0,1);
		box1Actor->GetProperty()->SetAmbient(1.0);
		box1Actor->GetProperty()->SetDiffuse(0.0);
		box1Actor->GetProperty()->SetSpecular(0.0);
		box1Actor->GetProperty()->SetLineWidth(4.0);
		box1Actor->GetProperty()->SetColor(0.0,0.0,0.0);
		box1Actor->SetInputData(pd);
		pd->Delete();

		rt->AddObject(box1Actor);

		return box1Actor;
	}

	iActorCollection* CreateBoxB(iRenderTool *rt)
	{
		//
		//  Bounding box 2
		//
		float diam = 0.02;
		iActor *actor[12];
		vtkCylinderSource *cs;

		int i;
		for(i=0; i<12; i++) 
		{
			actor[i] = iActor::New(); IERROR_CHECK_MEMORY(actor[i]);
			actor[i]->PickableOff();
			cs = vtkCylinderSource::New(); IERROR_CHECK_MEMORY(cs);
			cs->SetResolution(6);
			cs->SetHeight(2.0+2.0*diam);
			cs->SetRadius(diam);
			cs->Update();
			actor[i]->SetInputConnection(cs->GetOutputPort());
			cs->Delete();
		}

		for(i=0; i<4; i++) 
		{
			actor[i]->GetProperty()->SetColor(0.0,0.0,1.0);
			actor[i]->RotateX(90.0);
		}
		for(i=4; i<8; i++) actor[i]->GetProperty()->SetColor(0.0,1.0,0.0);
		for(i=8; i<12; i++) 
		{
			actor[i]->GetProperty()->SetColor(1.0,0.0,0.0);
			actor[i]->RotateZ(90.0);
		}

		actor[ 0]->SetPosition(-1.0,-1.0,0.0);
		actor[ 1]->SetPosition(-1.0, 1.0,0.0);
		actor[ 2]->SetPosition( 1.0,-1.0,0.0);
		actor[ 3]->SetPosition( 1.0, 1.0,0.0);
		actor[ 4]->SetPosition(-1.0,0.0,-1.0);
		actor[ 5]->SetPosition(-1.0,0.0, 1.0);
		actor[ 6]->SetPosition( 1.0,0.0,-1.0);
		actor[ 7]->SetPosition( 1.0,0.0, 1.0);
		actor[ 8]->SetPosition(0.0,-1.0,-1.0);
		actor[ 9]->SetPosition(0.0,-1.0, 1.0);
		actor[10]->SetPosition(0.0, 1.0,-1.0);
		actor[11]->SetPosition(0.0, 1.0, 1.0);

		iActorCollection* box2Actor = iActorCollection::New(); IERROR_CHECK_MEMORY(box2Actor);
		for(i=0; i<12; i++) 
		{
			actor[i]->GetProperty()->SetAmbient(0.2);
			actor[i]->GetProperty()->SetDiffuse(0.2);
			actor[i]->GetProperty()->SetSpecular(0.2);
			box2Actor->AddActor(actor[i]);
			rt->AddObject(actor[i]);
			actor[i]->Delete();
		}
		box2Actor->SetVisibility(false);

		return box2Actor;
	}


	iActorCollection* CreateBoxC(iRenderTool *rt)
	{
		//
		//  Bounding box 3
		//
		float diam = 0.02;
		iActor *actor[12];
		vtkArrowSource *as;
		vtkCylinderSource *cs;

		int i;
		for(i=0; i<3; i++) 
		{
			actor[i] = iActor::New(); IERROR_CHECK_MEMORY(actor[i]);
			actor[i]->PickableOff();
			as = vtkArrowSource::New(); IERROR_CHECK_MEMORY(as);
			as->SetShaftResolution(6);
			as->SetTipResolution(6);
			as->SetTipLength(0.15);
			as->SetShaftRadius(0.5*diam);
			as->SetTipRadius(1.5*diam);
			as->Update();
			actor[i]->SetInputConnection(as->GetOutputPort());
			as->Delete();
			actor[i]->SetAxisScale(2.5,2.5,2.5);
		}

		diam *= 0.75;
		for(i=3; i<12; i++) 
		{
			actor[i] = iActor::New(); IERROR_CHECK_MEMORY(actor[i]);
			actor[i]->PickableOff();
			cs = vtkCylinderSource::New(); IERROR_CHECK_MEMORY(cs);
			cs->SetResolution(6);
			cs->SetHeight(2.0+2.0*diam);
			cs->SetRadius(diam);
			actor[i]->SetInputConnection(cs->GetOutputPort());
			cs->Delete();
		}

		actor[0]->GetProperty()->SetColor(0.0,0.0,1.0);
		actor[0]->RotateY(-90.0);
		actor[1]->GetProperty()->SetColor(0.0,1.0,0.0);
		actor[1]->RotateZ(90.0);
		actor[2]->GetProperty()->SetColor(1.0,0.0,0.0);

		for(i=3; i<6; i++) 
		{
			actor[i]->GetProperty()->SetColor(0.0,0.0,1.0);
			actor[i]->RotateX(90.0);
		}
		for(i=6; i<9; i++) actor[i]->GetProperty()->SetColor(0.0,1.0,0.0);
		for(i=9; i<12; i++) 
		{
			actor[i]->GetProperty()->SetColor(1.0,0.0,0.0);
			actor[i]->RotateZ(90.0);
		}

		actor[ 0]->SetPosition(-1.0,-1.0,-1.0);
		actor[ 1]->SetPosition(-1.0,-1.0,-1.0);
		actor[ 2]->SetPosition(-1.0,-1.0,-1.0);

		actor[ 3]->SetPosition(-1.0, 1.0,0.0);
		actor[ 4]->SetPosition( 1.0,-1.0,0.0);
		actor[ 5]->SetPosition( 1.0, 1.0,0.0);

		actor[ 6]->SetPosition(-1.0,0.0, 1.0);
		actor[ 7]->SetPosition( 1.0,0.0,-1.0);
		actor[ 8]->SetPosition( 1.0,0.0, 1.0);

		actor[ 9]->SetPosition(0.0, 1.0,-1.0);
		actor[10]->SetPosition(0.0,-1.0, 1.0);
		actor[11]->SetPosition(0.0, 1.0, 1.0);

		iActorCollection* box3Actor = iActorCollection::New(); IERROR_CHECK_MEMORY(box3Actor);
		for(i=0; i<12; i++) 
		{
			actor[i]->GetProperty()->SetAmbient(0.2);
			actor[i]->GetProperty()->SetDiffuse(0.2);
			actor[i]->GetProperty()->SetSpecular(0.2);
			box3Actor->AddActor(actor[i]);
			rt->AddObject(actor[i]);
			actor[i]->Delete();
		}
		box3Actor->SetVisibility(false);

		return box3Actor;
	}
};

