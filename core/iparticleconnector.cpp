/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iparticleconnector.h"


#include "ierror.h"
#include "imath.h"

#include <vtkCellArray.h>
#include <vtkFloatArray.h>
#include <vtkIdTypeArray.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>

//
//  Templates
//
#include "igenericfilter.tlh"


#define EQTOL  1.001


namespace iParticleConnector_Private
{

//
// Do our own quick sort for efficiency reason (based on a Java code by Denis Ahrens)
//
#define SAVE(CELL,i1)    { itmp = indx[i1]; }
#define MOVE(i1,i2)      { indx[i1] = indx[i2]; }
#define RESTORE(i2,CELL) { indx[i2] = itmp; }
#define SWAP(i1,i2)      { SAVE(1,i1); MOVE(i1,i2); RESTORE(i2,1); }
#define GREATER1(ind1,ind2)  (data[mConnectVariable+nc*ind1]>data[mConnectVariable+nc*ind2])
#define GREATER2(ind1,ind2)  ((data[mConnectionBreakVariable+nc*ind1]>EQTOL*data[mConnectionBreakVariable+nc*ind2])||((EQTOL*data[mConnectionBreakVariable+nc*ind1]>data[mConnectionBreakVariable+nc*ind2])&&(data[mConnectVariable+nc*ind1]>data[mConnectVariable+nc*ind2])))

//
//  Recursive worker
//
void SortWorker1(iParticleConnector *me, vtkIdType l, vtkIdType r, int nc, int mConnectVariable, float *data, vtkIdType *indx)
{
	const int M = 8;
	vtkIdType i, j;
	vtkIdType v, itmp;

	if(me->GetAbortExecute()) return;

	if((r-l) > M)
	{
		//
		// Use quicksort
		//
		i = (r+l)/2;
		if(GREATER1(indx[l],indx[i]))      // Tri-Median Method!
		{
			SWAP(l,i);
		}
		if(GREATER1(indx[l],indx[r])) 
		{
			SWAP(l,r);
		}
		if(GREATER1(indx[i],indx[r])) 
		{
			SWAP(i,r);
		}
		
		j = r-1;
		SWAP(i,j);
		i = l;
		v = indx[j];
		for(;;)
		{
			do i++; while(GREATER1(v,indx[i])); // no ++i/--j in macro expansion!
			do j--; while(GREATER1(indx[j],v));
			if(j < i) break;
			SWAP(i,j);
		}
		SWAP(i,r-1);
		SortWorker1(me,l,j,nc,mConnectVariable,data,indx);
		SortWorker1(me,i+1,r,nc,mConnectVariable,data,indx);
	}
	else 
	{
		//
		// Array is small, use insertion sort. 
		//
		for(i=l+1; i<=r; i++)
		{
			SAVE(1,i);
			v = indx[i];
			j = i;
			while(j>l && GREATER1(indx[j-1],v))
			{
				MOVE(j,j-1);
				j--;
			}
			RESTORE(j,1);
		}
    }
}
//
//  Interface
//
void Index1(iParticleConnector *me, vtkIdType n, int nc, int mConnectVariable, float *data, vtkIdType *indx)
{
	vtkIdType j;
	for(j=0; j<n; j++) indx[j] = j;
	SortWorker1(me,0,n-1,nc,mConnectVariable,data,indx);
}

//
//  Recursive worker
//
void SortWorker2(iParticleConnector *me, vtkIdType l, vtkIdType r, int nc, int mConnectionBreakVariable, int mConnectVariable, float *data, vtkIdType *indx)
{
	const int M = 8;
	vtkIdType i, j;
	vtkIdType v, itmp;

	if(me->GetAbortExecute()) return;

	if((r-l) > M)
	{
		//
		// Use quicksort
		//
		i = (r+l)/2;
		if(GREATER2(indx[l],indx[i])) // Tri-Median Method!
		{
			SWAP(l,i);
		}
		if(GREATER2(indx[l],indx[r])) 
		{
			SWAP(l,r);
		}
		if(GREATER2(indx[i],indx[r])) 
		{
			SWAP(i,r);
		}
		
		j = r-1;
		SWAP(i,j);
		i = l;
		v = indx[j];
		for(;;)
		{
			do i++; while(GREATER2(v,indx[i])); // no ++i/--j in macro expansion!
			do j--; while(GREATER2(indx[j],v)); 
			if(j < i) break;
			SWAP(i,j);
		}
		SWAP(i,r-1);
		SortWorker2(me,l,j,nc,mConnectionBreakVariable,mConnectVariable,data,indx);
		SortWorker2(me,i+1,r,nc,mConnectionBreakVariable,mConnectVariable,data,indx);
	}
	else 
	{
		//
		// Array is small, use insertion sort. 
		//
		for(i=l+1; i<=r; i++)
		{
			SAVE(1,i);
			v = indx[i];
			j = i;
			while(j>l && GREATER2(indx[j-1],v))
			{
				MOVE(j,j-1);
				j--;
			}
			RESTORE(j,1);
		}
    }
}
//
//  Interface
//
void Index2(iParticleConnector *me, vtkIdType n, int nc, int mConnectionBreakVariable, int mConnectVariable, float *data, vtkIdType *indx)
{
	vtkIdType j;
	for(j=0; j<n; j++) indx[j] = j;
	SortWorker2(me,0,n-1,nc,mConnectionBreakVariable,mConnectVariable,data,indx);
}

};


using namespace iParticleConnector_Private;


iParticleConnector::iParticleConnector(iDataConsumer *consumer) : iGenericPolyDataFilter<vtkPolyDataAlgorithm>(consumer,1,true)
{
	mConnectVariable = mConnectionBreakVariable = -1;
}


void iParticleConnector::SetConnectVariable(int a)
{
	if(a>=-1 && a!=mConnectVariable)
	{
		mConnectVariable = a;
		this->Modified();
	}
}


void iParticleConnector::SetConnectionBreakVariable(int a)
{
	if(a>=-1 && a!=mConnectionBreakVariable)
	{
		mConnectionBreakVariable = a;
		this->Modified();
	}
}


void iParticleConnector::ProvideOutput()
{
	vtkPolyData *input = this->InputData();
	vtkPolyData *output = this->OutputData();

	output->ShallowCopy(input);

	if(mConnectVariable==-1 || input->GetPointData()->GetScalars()==0 || mConnectionBreakVariable==mConnectVariable) return;

	vtkFloatArray *inVars = iRequiredCast<vtkFloatArray>(INFO,input->GetPointData()->GetScalars());
	vtkIdType np = input->GetNumberOfPoints();
	vtkIdType nc = inVars->GetNumberOfComponents();

	if(mConnectVariable<0 || mConnectVariable>=nc || mConnectionBreakVariable<-1 || mConnectionBreakVariable>=nc) return;

	vtkCellArray *olin;
	olin = vtkCellArray::New(); IERROR_CHECK_MEMORY(olin);
	vtkIdType id[2];

	vtkIdType l;
	vtkIdType *ind;
	ind = new vtkIdType[np]; IERROR_CHECK_MEMORY(ind);
	float *data = inVars->GetPointer(0);

	if(mConnectionBreakVariable == -1) // no separation
	{
		Index1(this,np,nc,mConnectVariable,data,ind);
#ifdef I_CHECK
		for(l=0; l<np-1; l++) if(GREATER1(ind[l],ind[l+1]))
		{
			IBUG_WARN("Error in Index1.");
			break;
		}
#endif
		for(l=0; l<np-1; l++)
		{
			if(l%1000 == 0)
			{
				this->UpdateProgress(double(l)/np);
				if(this->GetAbortExecute()) break;
			}
			id[0] = ind[l];
			id[1] = ind[l+1];
			olin->InsertNextCell(2,id);
		}
	}
	else
	{
		Index2(this,np,nc,mConnectionBreakVariable,mConnectVariable,data,ind);
#ifdef I_CHECK
		for(l=0; l<np-1; l++) if(GREATER2(ind[l],ind[l+1]))
		{
			IBUG_WARN("Error in Index2.");
			break;
		}
#endif
		for(l=0; l<np-1; l++)
		{
			if(l%1000 == 0)
			{
				this->UpdateProgress(double(l)/np);
				if(this->GetAbortExecute()) break;
			}
			if(data[mConnectionBreakVariable+nc*ind[l]]*EQTOL > data[mConnectionBreakVariable+nc*ind[l+1]])
			{
				id[0] = ind[l];
				id[1] = ind[l+1];
				olin->InsertNextCell(2,id);
			}
		}

	}

	delete [] ind;
	output->SetLines(olin);
	olin->Delete();
}

