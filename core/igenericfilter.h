/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


//
//  A front-end for portable inheritance
//
#ifndef IGENERICFILTER_H
#define IGENERICFILTER_H


#include "idatahandler.h"


#include "ierror.h"
#include "ivtk.h"

#include <vtkType.h>


class vtkAlgorithm;
class vtkAlgorithmOutput;
class vtkDataArray;
class vtkDataObject;
class vtkDataSet;
class vtkFloatArray;
class vtkImageData;
class vtkInformation;
class vtkInformationVector;
class vtkPolyData;


template<class Filter, class InputType, class OutputType>
class iGenericFilter : public Filter, public iDataHandler
{

public:

	virtual float GetMemorySize();
	virtual void RemoveInternalData();

	//
	//  Expose a limited input/output API for better control
	//
	inline InputType* InputData(){ return this->InputData(0); }
	InputType* InputData(int n);

	inline OutputType* OutputData(){ return this->OutputData(0); }
	OutputType* OutputData(int n);

protected:

	iGenericFilter(iDataConsumer *consumer, int numInputs, bool ownsOutput);
	virtual ~iGenericFilter();

	virtual int FillInputPortInformation(int port, vtkInformation *info);
	virtual int FillOutputPortInformation(int port, vtkInformation *info);

	virtual int RequestInformation(vtkInformation *request, vtkInformationVector **inInfo, vtkInformationVector *outInfo);
	virtual int RequestUpdateExtent(vtkInformation *request, vtkInformationVector **inInfo, vtkInformationVector *outInfo);
	virtual int RequestData(vtkInformation *request, vtkInformationVector **inputVector, vtkInformationVector *outputVector);

	//
	//  Helper function for children use
	//
	vtkDataObject* GetFilterOutputIfPresent(vtkAlgorithm *alg, int port);

	void Reset();
	void ExecuteParent();

	virtual void ProvideInfo();				// called by RequestInformation
	virtual void ProvideExtent();			// called by RequestUpdateExtent
	virtual void ProvideOutput() = 0;		// called by RequestData
	virtual void VerifyOutput();			// called on every pipeline pass after Execute

	//
	//  Cached information during the execution pass
	//
	struct Cache
	{
		vtkInformation *LastRequest;
		vtkInformationVector **InputVector;
		vtkInformationVector *OutputVector;
		void Reset();
	};
	Cache wCache;

	//
	//  Helper members for transforming scalars
	//
	struct ArrayHelper
	{
		vtkFloatArray *ArrIn, *ArrOut;
		float *PtrIn, *PtrOut;
		int DimIn, DimOut;
		bool Init(vtkDataArray *arr, vtkIdType sizeOut = 0, int dimOut = 0);
	};
	ArrayHelper wArray;

private:

#ifdef I_DEBUG
	long mThreadId;
#endif

private:

	const bool mOwnsOutput;

#ifndef IVTK_5
	/* blocked */ virtual void SetInputDataObject(int port, vtkDataObject *data){ this->Filter::SetInputDataObject(port,data); }
	/* blocked */ virtual void SetInputDataObject(vtkDataObject *data)          { this->Filter::SetInputDataObject(data); }
	/* blocked */ virtual void AddInputDataObject(int port, vtkDataObject *data){ this->Filter::AddInputDataObject(port,data); }
	/* blocked */ virtual void AddInputDataObject(vtkDataObject *data)          { this->Filter::AddInputDataObject(data); }
#endif
	/* blocked */ vtkDataObject* GetInputDataObject(int port, int connection)   { return this->Filter::GetInputDataObject(port,connection); }
	/* blocked */ vtkDataObject* GetOutputDataObject(int port)                  { return this->Filter::GetOutputDataObject(port); }

	/* blocked */ vtkDataObject* GetInput()                                     { return this->Filter::GetInput(); }
	/* blocked */ vtkDataObject* GetInput(int port)                             { return this->Filter::GetInput(port); }
	/* blocked */ vtkDataObject* GetOutput()                                    { return this->Filter::GetOutput(); }
	/* blocked */ vtkDataObject* GetOutput(int port)                            { return this->Filter::GetOutput(port); }
};


template<class Filter>
class iGenericPolyDataFilter : public iGenericFilter<Filter,vtkPolyData,vtkPolyData>
{

protected:
	
	iGenericPolyDataFilter(iDataConsumer *consumer, int numInputs, bool ownsOutput);

	virtual void VerifyOutput();
};


template<class Filter>
class iGenericImageDataFilter : public iGenericFilter<Filter,vtkImageData,vtkImageData>
{

protected:
	
	iGenericImageDataFilter(iDataConsumer *consumer, int numInputs, bool ownsOutput);

	virtual void ProvideInfo();
};


//
//  Useful macro
//
#define iGenericFilterTypeMacro(_name_,_parent_) \
	public: \
		vtkTypeMacro(_name_,_parent_); \
		static _name_* New(iDataConsumer *consumer = 0) { IASSERT(consumer); return new _name_(consumer); } \
	protected: \
		_name_(iDataConsumer *consumer)

#endif // IGENERICFILTER_H
 
