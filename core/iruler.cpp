/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iruler.h"


#include "icamera.h"
#include "ierror.h"
#include "ioverlayhelper.h"
#include "irendertool.h"
#include "iviewmodule.h"

#include <vtkCamera.h>
#include <vtkRenderer.h>
#include <vtkTextProperty.h>

//
//  Templates
//
#include "iarray.tlh"
#include "igenericprop.tlh"
#include "iproperty.tlh"


iRulerActor::iRulerActor(iRuler *owner) : iAxis(owner->GetViewModule()->GetRenderTool()), iViewModuleComponent(owner->GetViewModule())
{
	mOwner = owner; 
	IASSERT(owner);

	mStarted = false;
	mBaseScale = 1.0;

	this->LabelTextProperty->SetVerticalJustificationToTop();
	this->TitleTextProperty->SetJustificationToCentered();
	this->TitleTextProperty->SetVerticalJustificationToBottom();

	this->SetPoint1(0.25,0.9);
	this->SetPoint2(0.75,0.9);
	this->SetRange(0.0,1.0);
	this->AdjustLabelsOff();
	this->SetNumberOfLabels(3);
}


void iRulerActor::UpdateGeometry(vtkViewport *vp)
{
	vtkCamera *cam = this->GetOverlayHelper()->GetCamera(vp);
	if(cam==0 || cam->GetParallelProjection()==0)
	{
		this->Disable();
		return;
	}
	
	int mag = this->GetOverlayHelper()->GetRenderingMagnification();

	if(mag == 1)
	{
		if(!mStarted)
		{
			mStarted = true;
			mBaseScale = cam->GetParallelScale();
		}

		float s;
		if(cam->GetParallelProjection() != 0)
		{
			s = cam->GetParallelScale()/mBaseScale;
		}
		else
		{
			s = cam->GetDistance()*(1.0e-35+tan(0.5*iMath::Deg2Rad(cam->GetViewAngle())))/mBaseScale;
		}
		
		double as[2];
		vp->GetAspect(as);
		float dx = 0.5/mBaseScale*as[1]/as[0];

		if(!this->GetViewModule()->InOpenGLCoordinates()) s *= this->GetViewModule()->GetBoxSize();
		this->SetRange(0.0,s);
		mOwner->SetScale(s);
		
		this->SetPoint1(0.5-dx,0.9);
		this->SetPoint2(0.5+dx,0.9);
	}

	this->iAxis::UpdateGeometry(vp);
}


void iRulerActor::UpdateOverlay(vtkViewport *vp)
{
	int mag = this->GetOverlayHelper()->GetRenderingMagnification();

	if(mag == 1)
	{
		int *sz = vp->GetSize();
		this->TitleActor->SetPosition(0.5*sz[0],0.91*sz[1]);
	}

	this->iAxis::UpdateOverlay(vp);
}


void iRulerActor::SetBaseScale(float s)
{
	if(s > 0.0)
	{
		mStarted = true;
		mBaseScale = s;
		this->Modified();
	}
}


void iRulerActor::SetScale(float s)
{
	if(!mStarted)
	{
		mStarted = true;
		mBaseScale = this->GetOverlayHelper()->GetRenderTool()->GetCamera()->GetDevice()->GetParallelScale();
	}

	if(s>0.0 && this->GetVisibility()!=0)
	{
		if(!this->GetViewModule()->InOpenGLCoordinates()) s /= this->GetViewModule()->GetBoxSize();
		this->GetOverlayHelper()->GetRenderTool()->GetCamera()->GetDevice()->SetParallelScale(s*mBaseScale);
		this->Modified();
	}
}


void iRulerActor::SetTitle(const iString &title)
{
	this->vtkAxisActor2D::SetTitle(title.ToCharPointer());
	if(title.IsEmpty()) this->TitleVisibilityOff(); else this->TitleVisibilityOn();
	this->Modified();
}


//
//  Main class
//
iRuler* iRuler::New(iViewModule *vm)
{
	static iString LongName("Ruler");
	static iString ShortName("rl");

	IASSERT(vm);
	return new iRuler(vm,LongName,ShortName);
}


iRuler::iRuler(iViewModule *vm, const iString &fname, const iString &sname) : iViewModuleTool(vm,fname,sname),
	iObjectConstructPropertyMacroS(Float,iRuler,Scale,s),
	iObjectConstructPropertyMacroS(String,iRuler,Title,t)
{
	mScale = 1.0;

	mActor = new iRulerActor(this); IERROR_CHECK_MEMORY(mActor);
	mActor->VisibilityOff();
	mActor->PickableOff();

	this->GetViewModule()->GetRenderTool()->AddObject(mActor);
}


iRuler::~iRuler()
{
	this->GetViewModule()->GetRenderTool()->RemoveObject(mActor);
	mActor->Delete();
}


bool iRuler::ShowBody(bool s)
{
	mActor->SetVisibility(s?1:0); 
	return true;
}


bool iRuler::SetScale(float s)
{
	if(s > 0.0)
	{
		mScale = s;
		mActor->SetScale(s);
		return true;
	}
	else return false;
}


bool iRuler::SetTitle(const iString& s)
{
	mTitle = s;
	mActor->SetTitle(s);
	return true;
}

