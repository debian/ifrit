/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  DataSubject(s) for core IFrIT DataType(s)
//
#ifndef ICOREDATASUBJECTS_H
#define ICOREDATASUBJECTS_H


#include "iparticledatasubject.h"

class iDataReader;


namespace iCoreData
{
	class ScalarFileLoader;


	class ScalarDataSubject : public iDataSubject
	{

		friend class ::iDataReader;

	public:

		vtkTypeMacro(ScalarDataSubject,iDataSubject);

		iObjectPropertyMacroA2(Smooth,Int,Float);

		//
		//  Inherited members
		//
		static const iDataType& DataType();
		virtual const iDataType& GetDataType() const;

		//
		//  Data subjects are treated by help differently from other objects
		//
		virtual const iString HelpTag() const;

	private:

		ScalarDataSubject(ScalarFileLoader *fl); // should not be created except by a Reader

		ScalarFileLoader *mConcreteLoader;
	};


	class VectorDataSubject : public iDataSubject
	{

		friend class ::iDataReader;

	public:

		vtkTypeMacro(VectorDataSubject,iDataSubject);

		//
		//  Inherited members
		//
		static const iDataType& DataType();
		virtual const iDataType& GetDataType() const;

		//
		//  Data subjects are treated by help differently from other objects
		//
		virtual const iString HelpTag() const;

	protected:

		virtual iDataLimits* CreateLimits() const;

	private:

		VectorDataSubject(iFileLoader *fl); // should not be created except by a Reader
	};


	class TensorDataSubject : public iDataSubject
	{

		friend class ::iDataReader;

	public:

		vtkTypeMacro(TensorDataSubject,iDataSubject);

		//
		//  Inherited members
		//
		static const iDataType& DataType();
		virtual const iDataType& GetDataType() const;

		//
		//  Data subjects are treated by help differently from other objects
		//
		virtual const iString HelpTag() const;

	protected:

		virtual iDataLimits* CreateLimits() const;

	private:

		TensorDataSubject(iFileLoader *fl); // should not be created except by a Reader
	};


	class ParticleDataSubject : public iParticleDataSubject
	{

		friend class ::iDataReader;

	public:

		vtkTypeMacro(ParticleDataSubject,iParticleDataSubject);

		//
		//  Inherited members
		//
		static const iDataType& DataType();
		virtual const iDataType& GetDataType() const;

		//
		//  Data subjects are treated by help differently from other objects
		//
		virtual const iString HelpTag() const;

	private:

		ParticleDataSubject(iParticleFileLoader *fl); // should not be created except by a Reader
	};
};

#endif

