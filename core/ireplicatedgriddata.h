/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Takes StructuredPoints data of any type and replicates them periodically.
//
#ifndef IREPLICATEDGRIDDATA_H
#define IREPLICATEDGRIDDATA_H


#include "igenericfilter.h"
#include <vtkImageAlgorithm.h>
#include "iparallelworker.h"
#include "ireplicatedelement.h"


class iViewInstance;


class iReplicatedGridData : public iGenericImageDataFilter<vtkImageAlgorithm>, protected iParallelWorker, public iReplicatedElement
{

public:

	vtkTypeMacro(iReplicatedGridData,vtkImageAlgorithm);

	static iReplicatedGridData* New(iViewInstance *owner = 0);

	virtual float GetMemorySize();

protected:
	
	iReplicatedGridData(iViewInstance *owner);

	virtual void ProvideInfo();
	virtual void ProvideOutput();
	virtual void UpdateReplicasBody();

	virtual int ExecuteStep(int step, iParallel::ProcessorInfo &p);

private:

	//
	//  Work variables
	//
	int wDimsIn[3], wDimsOut[3], wExt[3];
	vtkIdType wSize, wSize0, wSize1;
	char *wPtrIn, *wPtrOut;
};

#endif  // IREPLICATEDGRIDDATA_H


