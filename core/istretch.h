/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef ISTRETCH_H
#define ISTRETCH_H


#include "imath.h"

class iString;


namespace iStretch
{
	const int Lin = 0;
	const int Log = 1;
	const int Count = 2;

	template<class T> T Apply(T f, int id, bool up);
	template<class T> T Reset(T f, int id);

	const iString& GetName(int s);
	int GetId(const iString& name);
};


//
//  stretch and restore the data
//
namespace iStretch
{
	template<class T>
	inline T Apply(T f, int id, bool up)
	{
		const T zero = (T)0.0;
		const T tiny = (T)1.0e-36;
		const T invalid = (T)36.0;

		switch(id)
		{
		case Log:
			{
				if(f > zero) 
				{
					return iMath::Log10(tiny+f);
				}
				else 
				{
					return (up ? invalid : -invalid);
				}
				break;
			}
		default: return f;
		}
	}


	template<class T>
	inline T Reset(T f, int id)
	{
		switch(id)
		{
		case Log: 
			{
				return iMath::Pow10(f);
				break;
			}
		default: return f;
		}
	}
};

#endif // ISTRETCH_H

