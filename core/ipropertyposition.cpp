/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ipropertyposition.h"


#include "ierror.h"
#include "iobject.h"
#include "iposition.h"


//
//  Templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


iPropertyScalarPosition::iPropertyScalarPosition(iObject *owner, iViewModule *vm, SetterType setter, GetterType getter, const iString &name, const iString &sname) : iPropertyScalar<iType::Vector>(owner,(iPropertyScalar<iType::Vector>::SetterType)setter,(iPropertyScalar<iType::Vector>::GetterType)getter,name,sname), iViewModuleComponent(vm)
{
	mSetter = setter; IASSERT(setter);
	mGetter = getter; IASSERT(getter);
}


const iVector3D& iPropertyScalarPosition::GetValue(int i) const
{
	return (this->Owner()->*mGetter)().BoxValue(); 
}


bool iPropertyScalarPosition::SetValue(int i, const iVector3D& v) const
{
	iPosition pos(this->GetViewModule());
	pos.SetBoxValue(v);
	if((this->Owner()->*mSetter)(pos))
	{
		this->RequestRender();
		return true;
	}
	else return false;
}


iPropertyScalarDistance::iPropertyScalarDistance(iObject *owner, iViewModule *vm, SetterType setter, GetterType getter, const iString &name, const iString &sname) : iPropertyScalar<iType::Double>(owner,(iPropertyScalar<iType::Double>::SetterType)setter,(iPropertyScalar<iType::Double>::GetterType)getter,name,sname), iViewModuleComponent(vm)
{
	mSetter = setter; IASSERT(setter);
	mGetter = getter; IASSERT(getter);
}


double iPropertyScalarDistance::GetValue(int i) const
{
	return (this->Owner()->*mGetter)().BoxValue(); 
}


bool iPropertyScalarDistance::SetValue(int i, double v) const
{
	iDistance d(this->GetViewModule());
	d.SetBoxValue(v);
	if((this->Owner()->*mSetter)(d))
	{
		this->RequestRender();
		return true;
	}
	else return false;
}
