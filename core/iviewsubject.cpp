/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iviewsubject.h"


#include "icolorbar.h"
#include "idata.h"
#include "idatalimits.h"
#include "idatareader.h"
#include "idatasubject.h"
#include "ierror.h"
#include "imaterial.h"
#include "iviewmodule.h"
#include "iviewobject.h"
#include "iviewsubjectobserver.h"
#include "iviewsubjectparallelpipeline.h"

#include "iarray.tlh"
#include "iproperty.tlh"


using namespace iParameter;


//
//  Single instance class
//
iViewInstance::iViewInstance(iViewSubject *owner) : iDataConsumer(owner->GetViewModule(),owner->GetDataType()), iReplicatedElement(false), 
	mOwner(owner), mIndex(owner->mInstances.Size())
{
	mIsInitialized = mIsConfigured = mCreatingMainPipeline = false;
}


iViewInstance::~iViewInstance()
{
	this->RemoveColorBars();

	while(mPipelines.Size() > 0) mPipelines.RemoveLast()->Delete();

	//
	//  Make sure it does not matter in which order we are deleted and removed from the object list of instances
	//
	int i, idx;
	for(i=idx=0; i<mOwner->mInstances.Size(); i++) if(mOwner->mInstances[i] != this)
	{
		mOwner->mInstances[i]->mIndex = idx++;
	}
}


void iViewInstance::Configure()
{
	if(!mIsConfigured)
	{
		this->ConfigureBody();
		mIsConfigured = true;
	}
	else
	{
		IBUG_FATAL("ViewInstance has been already configured.");
	}
}


iViewSubjectPipeline* iViewInstance::CreateMainPipeline(int numInputs, int id)
{
	iViewSubjectPipeline *p = iViewSubjectParallelPipeline::New(this,numInputs,id);
	this->ConfigureMainPipeline(p,id);
	return p;
}


void iViewInstance::AddMainPipeline(int numInputs)
{
	if(numInputs > 0)
	{
		mCreatingMainPipeline = true;
		iViewSubjectPipeline *p = this->CreateMainPipeline(numInputs,mPipelines.Size()); IERROR_CHECK_MEMORY(p);
		mCreatingMainPipeline = false;
		mPipelines.Add(p);
	}
}


void iViewInstance::ConfigureMainPipeline(iViewSubjectPipeline *, int)
{
}


void iViewInstance::Reset()
{
	//
	//  Un-initialize, if needed
	//
	if(mIsInitialized)
	{
		this->RemoveColorBars();
		this->ResetBody();
		mIsInitialized = false;
	}
}


void iViewInstance::Show(bool s)
{
	if(!mIsConfigured)
	{
		IBUG_FATAL("ViewInstance has not been configured.");
		return;
	}
	if(s && !mIsInitialized)
	{
		this->FinishInitialization();
		mIsInitialized = true;
	}
	if(mIsInitialized)
	{
		this->ShowBody(s && this->CanBeShown());
		if(s)
		{
			this->UpdateColorBars();
		}
		else
		{
			this->RemoveColorBars();
		}
	}
}


void iViewInstance::AddColorBar(int priority, int var, int palette, const iDataType &type)
{
	if(this->Owner()->IsVisible())
	{
		iColorBarItem bar;
		bar.Var = var;
		bar.Palette = palette;
		bar.DataTypeId = type.GetId();
		if(this->GetViewModule()->GetColorBar()->Add(priority,bar)) mColorBars.Add(bar);
	}
}


void iViewInstance::RemoveColorBars()
{
	int i;
	for(i=0; i<mColorBars.Size(); i++) this->GetViewModule()->GetColorBar()->Remove(mColorBars[i]);
	mColorBars.Clear();
}


iViewSubjectPipeline* iViewInstance::CreatePipeline(int)
{
	return 0;
}


void iViewInstance::UpdateReplicasBody()
{
	int i;
	//
	//  We modify objects inside pipelines, but pipelines themselves would not execute if they remain unmodified.
	//
	for(i=0; i<mPipelines.Size(); i++) mPipelines[i]->UpdateReplicas();
}


void iViewInstance::UpdateOnMarkerChange()
{
}


bool iViewInstance::SyncWithDataBody()
{
	return true;
}


//
//  Main class
//
iViewSubject::iViewSubject(iObject *parent, const iString &fname, const iString &sname, iViewModule *vm, const iDataType &type, unsigned int flags, int minsize, int maxsize) : iMaterialObject(parent,fname,sname,1), iDataConsumer(vm,type), iReplicatedElement(false),
	IsNonReplicating((flags & ViewObject::Flag::IsNonReplicating) != 0), 
	iObjectConstructPropertyMacroGenericVOP(!IsNonReplicating,Int,iViewSubject,ReplicationFactors,rf,6,SetReplicationFactors,GetReplicationFactors),
	iObjectConstructPropertyMacroGenericS(Bool,iViewSubject,Visible,vis,Show,IsVisible),
	mMinSize(minsize), mMaxSize(maxsize)
{
	mRenderMode = RenderMode::Self;

	mObjectObserver = iViewSubjectObserver::New(this); IERROR_CHECK_MEMORY(mObjectObserver);

	Visible.AddFlag(iProperty::_DoNotSaveInState);
	Visible.AddFlag(iProperty::_DoNotIncludeInCopy);

	mIsVisible = false;
}


iViewSubject::~iViewSubject()
{
	while(mInstances.Size() > 0)
	{
		mInstances.RemoveLast()->Delete();
	}
	mObjectObserver->Delete();
}


void iViewSubject::InitInstances()
{
	iViewInstance *inst = this->MakeInstance(); IERROR_CHECK_MEMORY(inst);
	inst->Configure();
	mInstances.Add(inst);
}


void iViewSubject::Delete()
{
	while(mDataReplicated.Size() > 0) mDataReplicated[mDataReplicated.MaxIndex()]->ReplicateAs(0);
	while(mPropReplicated.Size() > 0) mPropReplicated[mPropReplicated.MaxIndex()]->ReplicateAs(0);
	this->iObject::Delete();
}


void iViewSubject::Reset()
{
	this->SyncWithData();

	int i;
	for(i=0; i<mInstances.Size(); i++) mInstances[i]->Reset();
}


bool iViewSubject::Show(bool s)
{
	int i;

	if(s)
	{
		if(!this->IsThereData()) return false;
		mIsVisible = true;
		for(i=0; i<mInstances.Size(); i++)
		{
			mInstances[i]->Show(true);
		}
		this->UpdateAutomaticShading();
	}
	else
	{
		for(i=0; i<mInstances.Size(); i++)
		{
			mInstances[i]->Show(false);
		}
		mIsVisible = false;
	}
	return true;
}


bool iViewSubject::SyncWithDataBody()
{
	//
	//  If we are visible, but data disappeared, hide us.
	//
	if(this->IsVisible())
	{
		if(!this->IsThereData()) this->Show(false);
	}
	//
	//  If data is non-periodic, remove replicas (done inside UpdateReplicas())
	//
	if(this->IsReplicated())
	{
		this->UpdateReplicas(true);
	}

	int i;
	for(i=0; i<mInstances.Size(); i++)
	{
		if(!mInstances[i]->SyncWithData()) return false;
	}

	return true;
}


float iViewSubject::GetMemorySize()
{
	int i;
	float s = this->iDataConsumer::GetMemorySize();

	for(i=0; i<mInstances.Size(); i++) s += mInstances[i]->GetMemorySize();
	
	return s;
}


void iViewSubject::RemoveInternalData()
{
	int i;
	//
	//  remove everything, including the pipeline outputs
	//
	for(i=0; i<mInstances.Size(); i++)
	{
		mInstances[i]->RemoveInternalData();
	}
	this->iDataConsumer::RemoveInternalData();
}


void iViewSubject::UpdateOnMarkerChange()
{
	int i;
	for(i=0; i<mInstances.Size(); i++) mInstances[i]->UpdateOnMarkerChange();
}


bool iViewSubject::CopyInstances(int iy, int ix)
{
	if(ix>=0 && ix<mInstances.Size() && iy>=0 && iy<mInstances.Size())
	{
		if(ix == iy) return true;

		int i;
		for(i=0; i<this->Variables().Size(); i++)
		{
			if(this->Variables()[i]->Size() == mInstances.Size()) // only ViewSubject properties
			{
				if(!this->Variables()[i]->CopyElement(iy,ix))
				{
					return false;
				}
			}
		}
		return true;
	}
	else return false;
}


bool iViewSubject::SetReplicationFactors(int d, int n)
{
	if(n>=0 && d>=0 && d<6)
	{
		if(n != mReplicationFactors[d])
		{
			mReplicationFactors[d] = n;
			this->UpdateReplicas();
		}
		return true;
	}
	else return false;
}


void iViewSubject::RegisterReplicatedElement(iReplicatedElement *r, bool isData)
{
	if(r != 0)
	{
		if(isData) mDataReplicated.AddUnique(r); else mPropReplicated.AddUnique(r);
		this->UpdateReplicas();
	}
}


void iViewSubject::UnRegisterReplicatedElement(iReplicatedElement *r)
{
	if(r != 0)
	{
		mDataReplicated.Remove(r);
		mPropReplicated.Remove(r);
		this->UpdateReplicas();
	}
}


void iViewSubject::UpdateReplicasHead()
{
	int i;

	if(IsNonReplicating)
	{
		for(i=0; i<6; i++) mReplicationFactors[i] = 0;
	}
	else
	{
		//
		//  If data is non-periodic, remove replicas
		//
		for(i=0; i<3; i++) if(!this->GetSubject()->IsDirectionPeriodic(i))
		{
			mReplicationFactors[2*i] = mReplicationFactors[2*i+1] = 0;
		}
	}
}


void iViewSubject::UpdateReplicasBody()
{
	int i;
	for(i=0; i<mDataReplicated.Size(); i++) mDataReplicated[i]->UpdateReplicas();
	for(i=0; i<mPropReplicated.Size(); i++) mPropReplicated[i]->UpdateReplicas();

	//
	//  We modify objects inside pipelines, but pipelines themselves would not execute if they remain unmodified.
	//
	for(i=0; i<mInstances.Size(); i++) mInstances[i]->UpdateReplicas(false);
}


void iViewSubject::WriteHead(iString &s) const
{
	s = "Data Type: " + this->GetDataType().LongName() + "/" + this->GetDataType().ShortName() + "\n";
}


void iViewSubject::BecomeClone(iViewSubject *other)
{
	if(other != 0)
	{
		this->Show(other->IsVisible());
	}
}


int iViewSubject::DepthPeelingRequest() const
{
	if(this->IsVisible()) return this->DepthPeelingRequestBody(); else return 0;
}


int iViewSubject::DepthPeelingRequestBody() const
{
	return 0;
}


const iString iViewSubject::GetQualifiedName() const
{
	iViewObject *parent = dynamic_cast<iViewObject*>(this->Parent());

	if(parent == 0)
	{
		return this->iObject::GetFullName();
	}
	else
	{
		return parent->GetFullName() + "[" + this->GetDataType().LongName() + "]";
	}
}


bool iViewSubject::CreateInstance()
{
	if(mInstances.Size() < mMaxSize)
	{
		//
		//  Create a new piece
		//
		iViewInstance *ins = this->MakeInstance();
		if(ins == 0)
		{
			return false;
		}
		ins->Configure();
		int oldLast = mInstances.MaxIndex();
		mInstances.Add(ins);

		//
		//  Set the correct piece number
		//
		this->CopyInstances(mInstances.MaxIndex(),oldLast);
		ins->SyncWithData();
		ins->Show(this->IsVisible());

		this->UpdateAutomaticShading();
		return true;
	}
	else return false;
}


bool iViewSubject::RemoveInstance(int n)
{
	if(mMinSize<mInstances.Size() && n>=0 && n<mInstances.Size())
	{
		mInstances[n]->Delete();
		mInstances.Remove(n);
		
		this->UpdateAutomaticShading();
		return true;
	}
	else return false;
}


bool iViewSubject::SetNumberOfInstances(int n)
{
	if(mMinSize<=n && n<=mMaxSize)
	{
		while(mInstances.Size() > n)
		{
			if(!this->RemoveInstance(mInstances.MaxIndex())) return false;
		}
		while(mInstances.Size() < n)
		{
			if(!this->CreateInstance()) return false;
		}
		return true;
	}
	else return false;
}


bool iViewSubject::SyncWithLimitsBody(int var, int mode)
{
	int i;
	for(i=0; i<mInstances.Size(); i++)
	{
		if(!mInstances[i]->SyncWithLimits(var,mode)) return false;
	}
	return true;
}
