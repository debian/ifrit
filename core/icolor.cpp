/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "icolor.h"


iColor::iColor()
{
	mAlpha = 1;
	mRed = mGreen = mBlue = 0;
}


iColor::iColor(const iColor &c)
{
	mRed = c.mRed;
	mGreen = c.mGreen;
	mBlue = c.mBlue;
	mAlpha = c.mAlpha;
}


iColor::iColor(int r, int g, int b, int a)
{
	mRed = r;
	mGreen = g;
	mBlue = b;
	mAlpha = a;
}


iColor::iColor(double c[3], double alpha)
{
	if(c == 0)
	{
		mRed = mGreen = mBlue = 0;
		mAlpha = 255;
	}
	else
	{
		mRed = int(255*c[0]);
		mGreen = int(255*c[1]);
		mBlue = int(255*c[2]);
		mAlpha = int(255*alpha);
	}
}


double* iColor::ToVTK() const
{
	mVTK[0] = mRed/255.0;
	mVTK[1] = mGreen/255.0;
	mVTK[2] = mBlue/255.0;
	mVTK[3] = mAlpha/255.0;

	return mVTK;
}


iColor iColor::Reverse() const
{
	return iColor(255-mRed,255-mGreen,255-mBlue,mAlpha);
}


iColor iColor::Shadow() const
{
    int c = (mRed+mGreen+mBlue)/3 > 127 ? 0 : 255;
	return iColor(c,c,c,mAlpha);
}


const iColor& iColor::IFrIT()
{
	static const iColor tmp(120,180,240);
	return tmp;
}


const iColor& iColor::Black()
{
	static const iColor tmp(0,0,0);
	return tmp;
}


const iColor& iColor::White()
{
	static const iColor tmp(255,255,255);
	return tmp;
}


const iColor& iColor::Invalid()
{
	static const iColor tmp(999,999,999,999);
	return tmp;
}


iColor& iColor::operator=(const iColor &c)
{
	mRed = c.mRed;
	mGreen = c.mGreen;
	mBlue = c.mBlue;
	mAlpha = c.mAlpha;

	return *this;
}

