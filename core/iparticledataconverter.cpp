/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iparticledataconverter.h"


//#include "idatalimits.h"
#include "ierror.h"
#include "imath.h"
#include "iparticleviewsubject.h"
#include "ipiecewisefunction.h"
#include "ipointglyph.h"
#include "istretch.h"
#include "iviewmodule.h"

#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkFloatArray.h>
#include <vtkMath.h>
#include <vtkPiecewiseFunction.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"
#include "igenericfilter.tlh"

using namespace iParameter;


iParticleDataConverter* iParticleDataConverter::New(iViewInstance *owner)
{
	IASSERT(owner);
	return new iParticleDataConverter(owner);
}


iParticleDataConverter::iParticleDataConverter(iViewInstance *owner) : iGenericPolyDataFilter<vtkPolyDataAlgorithm>(owner,1,true)
{
	mOwner = iRequiredCast<iParticleViewInstance>(INFO,owner);

	mSize = 0.005;
	mPoint = iPointGlyph::New(); IERROR_CHECK_MEMORY(mPoint);
	this->SetType(PointGlyphType::Point);
}


iParticleDataConverter::~iParticleDataConverter()
{
	mPoint->Delete();
}


int iParticleDataConverter::GetType() const
{
	return mPoint->GetType();
}


void iParticleDataConverter::SetType(int t)
{
	mPoint->SetType(t);
	this->Modified();
}


void iParticleDataConverter::SetSize(float s)
{
	if(s>0.0 && s<1000.1)
	{
		mSize = 0.005*s;
		this->Modified();
	}
}


void iParticleDataConverter::ProvideOutput()
{
	int i, j;
	vtkIdType lp, lv;
	vtkIdType nCell1, *pCell1, pCell2[999];
	double x0[3], x1[3], x2[3];
	
	vtkPolyData *input = this->InputData();
	vtkPolyData *output = this->OutputData();

	output->ShallowCopy(input);

	if(mPoint->GetType() == PointGlyphType::Point) return;
	
	vtkPoints *inpPnts = input->GetPoints();
	if(inpPnts == 0) return;

	vtkCellArray *inpVrts = input->GetVerts();
	if(inpVrts == 0) return;

	//
	//  Direct access to the input data
	//
	vtkIdType nInpVrts = inpVrts->GetNumberOfCells();
	vtkIdType nInpPnts = inpPnts->GetNumberOfPoints();

	if(nInpVrts<=0 || nInpPnts<=0) return;

	int sop;
	switch(inpPnts->GetDataType())
	{
	case VTK_FLOAT:
		{
			sop = sizeof(float);
			break;
		}
	case VTK_DOUBLE:
		{
			sop = sizeof(double);
			break;
		}
	default: return;
	}

	//
	//  Direct access to the glyph data
	//
	mPoint->Update();

	vtkPoints *srcPnts = mPoint->GetData()->GetPoints();
	if(srcPnts->GetDataType() != VTK_FLOAT)
	{
		vtkErrorMacro("Source points are not floats.");
		return;
	}
	float *srcPntsArr = (float *)srcPnts->GetVoidPointer(0);

	bool srcCellPoly = true;
	vtkCellArray *srcClls = mPoint->GetData()->GetPolys();
	if(srcClls==0 || srcClls->GetNumberOfCells()==0)
	{
		srcClls = mPoint->GetData()->GetVerts();
		srcCellPoly = false;
	}
	if(srcClls==0 || srcClls->GetNumberOfCells()==0)
	{
		vtkErrorMacro("Incorrect input to iParticleDataConverter");
		return;
	}
	
	int sSrcNmls = 0;
	vtkFloatArray *srcNmls = 0;
	if(mPoint->GetData()->GetPointData()->GetNormals() != 0) 
	{
		srcNmls = iRequiredCast<vtkFloatArray>(INFO,mPoint->GetData()->GetPointData()->GetNormals());
		sSrcNmls = 1;
	}
	else if(mPoint->GetData()->GetCellData()->GetNormals() != 0) 
	{
		srcNmls = iRequiredCast<vtkFloatArray>(INFO,mPoint->GetData()->GetCellData()->GetNormals());
		sSrcNmls = -1;
	}
	float *srcNmlsArr = 0;
	if(srcNmls != 0) srcNmlsArr = (float *)srcNmls->GetVoidPointer(0);

	vtkIdType nSrcPnts = srcPnts->GetNumberOfPoints();
	vtkIdType nSrcClls = srcClls->GetNumberOfCells(); 
	vtkIdType nSrcCellMax = srcClls->GetMaxCellSize();

	bool rotate = mPoint->GetOriented();
	bool orient = false;

	//
	//  Check if lines specified too
	//
	vtkIdType nOutPntsOff = 0;
	vtkCellArray *inpLins = 0;
	float *inpPntsDir = 0;
	if(input->GetLines()!=0 && input->GetLines()->GetNumberOfCells()>0)
	{
		inpLins = input->GetLines();
		nOutPntsOff = nInpPnts;
		//
		//  If lines are present, arrows and cones are oriented along the lines
		//
		if(mPoint->GetType()==PointGlyphType::Cone || mPoint->GetType()==PointGlyphType::Arrow)
		{
			orient = true;
			inpPntsDir = new float[3*nInpPnts];
			if(inpPntsDir == 0)
			{
				vtkErrorMacro("There is not enough memory to convert the particle set data");
				this->SetAbortExecute(1);
				return;
			}
			memset(inpPntsDir,0,3*nInpPnts*sizeof(float));
			inpLins->InitTraversal();
			while(inpLins->GetNextCell(nCell1,pCell1) != 0)
			{
#ifndef I_NO_CHECK
				if(nCell1 != 2)
				{
					IBUG_WARN("Line contains more than 2 points!");
					break;
				}
#endif
				inpPnts->GetPoint(pCell1[0],x1);
				inpPnts->GetPoint(pCell1[1],x2);
				for(j=0; j<3; j++)
				{
					x0[j] = x2[j] - x1[j];
				}
				vtkMath::Normalize(x0);
				for(j=0; j<3; j++)
				{
					inpPntsDir[3*pCell1[0]+j] += x0[j];
					inpPntsDir[3*pCell1[1]+j] += x0[j];
				}
			}
			for(lp=0; lp<nInpPnts; lp++)
			{
				vtkMath::Normalize(inpPntsDir+3*lp);
			}
		}
	}

	//
	//  Create output data
	//
	vtkIdType nOutPnts = nInpVrts*nSrcPnts + nOutPntsOff;
	vtkIdType nOutClls = nInpVrts*nSrcClls;
	vtkPoints *outPnts = vtkPoints::New(inpPnts->GetDataType()); IERROR_CHECK_MEMORY(outPnts);
	vtkCellArray *outClls = vtkCellArray::New(); IERROR_CHECK_MEMORY(outClls);

	//	
	// Allocates and Sets MaxId
	//
	outPnts->SetNumberOfPoints(nOutPnts);

	//
	//  If not enough memory - revert to points
	//
	if(outPnts->GetVoidPointer(0)==0 || outClls->Allocate(outClls->EstimateSize(nOutClls,nSrcCellMax))==0) 
	{
		if(inpPntsDir != 0) delete [] inpPntsDir;
		outPnts->Delete();
		outClls->Delete();
		vtkErrorMacro("There is not enough memory to convert the particle set data");
		this->SetAbortExecute(1);
		return;
	}
	
	//
	//  Output normals
	//
	vtkFloatArray *outNmls = vtkFloatArray::New(); IERROR_CHECK_MEMORY(outNmls);
	outNmls->SetNumberOfComponents(3);
	float *outNmlsArr = 0;
	if(sSrcNmls > 0) 
	{
		// Allocates and Sets MaxId
		outNmls->SetNumberOfTuples(nOutPnts);
		outNmlsArr = (float *)outNmls->GetVoidPointer(0);
	} 
	else if(sSrcNmls < 0) 
	{
		// Allocates and Sets MaxId
		outNmls->SetNumberOfTuples(nOutClls);
		outNmlsArr = (float *)outNmls->GetVoidPointer(0);
	} 
	if(sSrcNmls!=0 && outNmlsArr==0) 
	{
		if(inpPntsDir != 0) delete [] inpPntsDir;
		outPnts->Delete();
		outClls->Delete();
		outNmls->Delete();
		vtkErrorMacro("There is not enough memory to convert the particle set data");
		this->SetAbortExecute(1);
		return;
	}

	bool hasScalars = (input->GetPointData()->GetScalars() != 0);
	if(hasScalars && !wArray.Init(input->GetPointData()->GetScalars(),nOutPnts))
	{
		outPnts->Delete();
		outClls->Delete();
		outNmls->Delete();
		this->SetAbortExecute(1);
		return;
	}

	//
	//  Variable for scaling
	//
	int cvar = mOwner->GetScaleVar();
	
	//
	//  Set fixed seed to make sure particles do not jump in an animation
	//
	vtkMath::RandomSeed(123456789);

	//
	//  If lines specified, add original points first
	//
	if(nOutPntsOff > 0) 
	{
		memcpy(outPnts->GetVoidPointer(0),inpPnts->GetVoidPointer(0),nInpPnts*3*sop);
		if(hasScalars) memcpy(wArray.PtrOut,wArray.PtrIn,nInpPnts*wArray.DimIn*sizeof(float));
		if(sSrcNmls != 0) 
		{
			memset(outNmlsArr,0,nInpPnts*3*sizeof(float));
//			for(lp=0; lp<nInpPnts; lp++)
//			{
//				for(j=0; j<3; j++) outNmlsArr[3*lp+j] = snArr[j];  // this is faster than memcpy!
//			}
		}
	}

	//
	//  Prepare for scaling
	//
	float scale = 2.0*mOwner->GetScaleFactor();

	//
	//  Main loop
	//
	double mat[3][3];
	double en, e[4];  // Euler quaternion
	float *s0 = 0;
	double s, ct, st, cp, sp;
	vtkIdType llPnts = nOutPntsOff, llClls = 0;

	outClls->InitTraversal();
	inpVrts->InitTraversal();
	for(lv=0; lv<nInpVrts; lv++)
	{
		if(lv%1000 == 0)
		{
			this->UpdateProgress(double(lv)/nInpVrts);
			if(this->GetAbortExecute()) break;
		}

		inpVrts->GetNextCell(nCell1,pCell1);
#ifndef I_NO_CHECK
		if(nCell1 != 1)
		{
			IBUG_WARN("Vert contains more than 1 point!");
			break;
		}
#endif
		lp = pCell1[0];

		inpPnts->GetPoint(lp,x0);
		s = mSize;
		if(hasScalars)
		{
			s0 = wArray.PtrIn + wArray.DimIn*lp;
			if(cvar >= 0)
			{
				s = scale*s0[cvar];
			}
		}
		
		if(rotate)
		{
			if(orient)
			{
				ct = inpPntsDir[3*lp+2];
				cp = inpPntsDir[3*lp+0];
				st = sqrt(1.0-ct*ct);
				sp = sqrt(1.0-cp*cp);
				if(inpPntsDir[3*lp+1] < 0.0) sp = -sp;
				mat[0][0] = ct*cp;
				mat[1][0] = ct*sp;
				mat[2][0] = -st;
				mat[0][1] = -sp;
				mat[1][1] = cp;
				mat[2][1] = 0.0;
				mat[0][2] = st*cp;
				mat[1][2] = st*sp;
				mat[2][2] = ct;
			}
			else
			{
				en = 0.0;
				for(j=0; j<4; j++)
				{
					e[j] = vtkMath::Random();
					en += e[j]*e[j];
				}
				en = 1.0/sqrt(en);
				for(j=0; j<4; j++) e[j] *= en;
				//
				//  There is a bug in VTK: the quaternion must be normalized, despite the claim to the opposite in the help
				//
				vtkMath::QuaternionToMatrix3x3(e,mat);
			}
		}
		
		srcClls->InitTraversal();
		for(i=0; i<nSrcClls; i++)
		{
			srcClls->GetNextCell(nCell1,pCell1);
			for(j=0; j<nCell1; j++) pCell2[j] = pCell1[j] + llPnts;
			outClls->InsertNextCell(nCell1,pCell2);
			if(sSrcNmls < 0) // cell normals
			{
				for(j=0; j<3; j++) outNmlsArr[3*llClls+j] = srcNmlsArr[3*i+j];
			}
			llClls++;
		}

		for(i=0; i<nSrcPnts; i++)
		{
			for(j=0; j<3; j++) x1[j] = srcPntsArr[3*i+j]; // cannot use iTune, these are of different type
			if(rotate)
			{
				vtkMath::Multiply3x3(mat,x1,x2);
				for(j=0; j<3; j++) x1[j] = x2[j]; // this is the fastest assignment
			}
			for(j=0; j<3; j++) x1[j] = s*x1[j] + x0[j];
			outPnts->SetPoint(llPnts,x1);
			if(hasScalars) 
			{
				for(j=0; j<wArray.DimIn; j++) wArray.PtrOut[wArray.DimOut*llPnts+j] = s0[j];
			}
			if(sSrcNmls > 0) // point normals 
			{
				for(j=0; j<3; j++) outNmlsArr[3*llPnts+j] = srcNmlsArr[3*i+j];
			}
			llPnts++;
		}
	}
	
	if(inpPntsDir != 0) delete [] inpPntsDir;

	output->SetPoints(outPnts);
	outPnts->Delete();
	
	//
	//  remove verts
	//
	output->SetVerts(0);

	if(srcCellPoly)
	{
		output->SetPolys(outClls);
	}
	else
	{
		output->SetVerts(outClls);
	}
	outClls->Delete();
	
	if(hasScalars)
	{
		output->GetPointData()->SetScalars(wArray.ArrOut);
		wArray.ArrOut->Delete();
	}

	if(sSrcNmls > 0) 
	{
		output->GetPointData()->SetNormals(outNmls);
	} 
	else if(sSrcNmls < 0) 
	{
		output->GetCellData()->SetNormals(outNmls);
	}
	else
	{
		output->GetPointData()->SetNormals(0);
		output->GetCellData()->SetNormals(0);
	}
	outNmls->Delete();
}


float iParticleDataConverter::GetMemorySize()
{
	if(this->GetType() == PointGlyphType::Point) return 0.0; else return this->iGenericPolyDataFilter<vtkPolyDataAlgorithm>::GetMemorySize();
}

