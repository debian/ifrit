/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


//
//  A base class for all GUI shells around the VTK-based core
//

#ifndef ISHELL_H
#define ISHELL_H


#include "iobject.h"


#include "iarray.h"
#include "istring.h"

class iDataReaderObserver;
class iEventObserver;
class iFile;
class iImageComposer;
class iInteractorEventObserver;
class iOutputChannel;
class iPaletteCollection;
class iParallelManager;
class iParallelUpdateEventObserver;
class iExecutionEventObserver;
class iRunner;
class iShellExitObserver;
class iViewModule;
class iViewModuleCollection;

class vtkCriticalSection;
class vtkMultiThreader;
class vtkRenderWindow;
class vtkRenderWindowInteractor;
class vtkTimerLog;


namespace iParameter
{
	namespace Environment
	{
		enum Type
		{
			Base,
			Data,
			Image,
			Script,
			Palette,
			ScalarsData,
			VectorsData,
			TensorsData,
			ParticlesData,
			SIZE
		};
	};

	namespace ShellObserver
	{
		enum Type
		{
			Execution,
			ParallelUpdate
		};
	};

	namespace ViewModuleObserver
	{
		enum Type
		{
			Render,
			Pick
		};
	};
};


class iShell : public iObject
{

	friend class iRunner;

public:

	vtkTypeMacro(iShell,iObject);

	virtual void Delete();

	iObjectPropertyMacro1S(AutoRender,Bool);
	iObjectPropertyMacro2S(OptimizationMode,Int);
	iObjectPropertyMacro2S(NumberOfProcessors,Int);
	iObjectPropertyMacro1S(SynchronizeInteractors,Bool);
	iObjectPropertyMacro1S(Modal,Bool);
	iObjectPropertyMacroA1(SaveState,String);
	iObjectPropertyMacroA1(RestoreState,String);
	iObjectPropertyMacroA1(SynchronizeCameras,Int);

	//
	//  Access to components
	//
	inline const iString& Type() const { return mType; }
	inline bool IsRunning() const { return mIsRunning; }
	inline bool IsStopped() const { return mIsStopped; }
	inline bool IsThreaded() const { return mIsThreaded; }
	inline bool IsFailed() const { return mIsFailed; }
	inline bool IsAutoRendering() const { return !mBlockAutoRender && mAutoRender; }

	const iString& GetEnvironment(iParameter::Environment::Type type) const;

	virtual bool SaveStateToFile(const iString& filename);
	virtual bool RestoreStateFromFile(const iString& filename);

	//
	//  mod = -1: sum up all view modules
	//
	float GetMemorySize(int mod = -1) const;

	//
	//  Interface for step-by-step execution
	//
	bool PrintHelp();
	bool Initialize();
	void Start();
	void Stop();

	//
	//  Access to components
	//
	int GetNumberOfViewModules() const;
	iViewModule* GetViewModule(int mod) const;
	void RequestRender(iViewModule *vm, bool force = false);

	inline iImageComposer* GetImageComposer() const { return mComposer; }
	inline iParallelManager* GetParallelManager() const { return mParallelManager; } 
	inline iPaletteCollection* GetPalettes() const { return mPalettes; }
	inline iDataReaderObserver* GetDataReaderObserver() const { return mDataReaderObserver; }
	inline iExecutionEventObserver* GetExecutionEventObserver() const { return mExecutionObserver; }
	iEventObserver* GetInteractorObserver() const;

	void OnInitAtom();

	void SetExitObserver(iShellExitObserver *obs);

	//
	//  I still haven't decided what the proper way to output text is - either
	//  through the "cloud" iOutput or via shells own OutputChannel. To avoid
	//  invasive changes in the future, this choice is made in this function, and
	//  all ShellComponents should use it to display output.
	//
	void OutputText(int type, const iString &text) const;

	//
	//  Factory methods for ViewModule
	//
	virtual vtkRenderWindow* CreateRenderWindow(iViewModule *vm, bool stereo) const = 0;
	virtual vtkRenderWindowInteractor* CreateRenderWindowInteractor(iViewModule *vm) const = 0;
	virtual iEventObserver* CreateViewModuleEventObserver(iParameter::ViewModuleObserver::Type type, iViewModule *vm) const = 0;

protected:

	iShell(iRunner *runner, bool threaded, const iString &type, int argc, const char **argv);
	virtual ~iShell();  

	//
	//  For thread safety, the constructor of a child shell should be trivial 
	//  (only set command-line options and initialize simple members), and all 
	//  constructor/destructor work must be done in the following functions:
	//
	virtual bool StartConstructor();   // ran before visual modules are created
	virtual bool ConstructorBody() = 0;  
	virtual void DestructorBody() = 0;  
	virtual void FinishDestructor();   // ran after visual modules are deleted
	virtual void StartBody() = 0;
	virtual void StopBody() = 0;

	//
	// These functions can be overwritten in a child class to provide more information about the initialization process
	//
	virtual void OnInitStart();
	virtual void OnInitState();
	virtual void OnInitFinish();

	//
	//  These functions can be used to animate the initialization/load state file progress.
	//  Do nothing by default.
	//
	virtual void OnInitAtomBody(bool timed);
	virtual void OnLoadStateAtomBody(int prog);

	//
	//  This functions parse command-line options and other argumenst
	//
	virtual bool OnCommandLineOption(const iString &sname, const iString& value);
	virtual bool OnCommandLineArguments(iArray<iString> &argv);

	//
	//  Factory methods for itself
	//
	virtual iDataReaderObserver* CreateDataReaderObserver() = 0;
	virtual iEventObserver* CreateShellEventObserver(iParameter::ShellObserver::Type type) = 0;
	virtual iOutputChannel* CreateOutputChannel() = 0;

	//
	//  *** no inheritable functions below this point ***
	//
	virtual void PassRenderRequest(int mode);
	virtual bool IsImmediatePost() const;

	//
	//  This functions ads command-line options.
	//
	void AddCommandLineOption(const iString &sname, const iString& lname, const iString &help, bool value);

	void OnLoadStateAtom();

	//
	//  Slots
	//
	void SetStateFile(const iString &name);
	void SetNumProcs(const iString &str);
	void ForceGPU();
	void ForceNoGPU();
	
	//
	//  A timer for diverse use
	//
	vtkTimerLog *mWorkTimer;
	bool mBlockAutoRender, mLoadedState, mIsFailed;

	//
	//  View Modules
	//
	iViewModuleCollection *mViewModules;

	static iShell* New(){ return 0; }

private:

	//
	//  Factory method
	//
	static iShell* New(iRunner *runner, int argc, const char **argv);

	struct Option
	{
		iString ShortName, LongName;
		iString Help;
		bool NeedsValue;
		Option() { NeedsValue = false; }
	};

	bool ParseCommandLine();

	static const iString mShortOptionPrefix, mLongOptionPrefix;
	iArray<Option> mOptions;
	iArray<iString> mArgV;

	iImageComposer *mComposer;
	iPaletteCollection *mPalettes;
	iDataReaderObserver *mDataReaderObserver;
	iExecutionEventObserver *mExecutionObserver;
	iInteractorEventObserver *mInteractorObserver;

	iOutputChannel *mOutputChannel;
	//
	//  Parallel objects
	//
	iParallelUpdateEventObserver *mParallelObserver;
	iParallelManager *mParallelManager;

	bool mIsRunning, mIsStopped, mIsThreaded, mCanBeDeleted;
	const iString mType;
	int mNumProcs, mProgCur, mProgMax;

	//
	//  Initialization
	//
	int mInitSteps;
	iString mStateFileName;

	//
	//  Environment
	//
	iString mEnvironment[iParameter::Environment::SIZE];

	//
	//  Our Runner etc
	//
	iRunner *mRunner;
	iShellExitObserver *mExitObserver;

	//
	//  This is a part of message subsystem for thread-safe execution.
	//  Functions begining with underscore can be called from the driver thread.
	//  Functions ending with underscore should be called from the slave thread.
	//
public:

	virtual bool _RequestPush(iProperty::Key key);

protected:

	void PushAllRequests_();
	bool HasPostedRequests() const;

private:

	//
	//  Push Request queue
	//
	iArray<iProperty::Key> mPushRequestQueue;
	vtkCriticalSection *mMutex;
	bool mLastPushResult, mInPush;
};


//
//  Helper class to do various clean-up after shell exists naturally or via an exception
//
class iShellExitObserver
{

	friend class iShell;

protected:

	virtual void OnExit(iShell *shell) = 0;
};

#endif  // ISHELL_H
