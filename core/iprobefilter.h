/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Optimized version of vtkProbeFilter for vtkPolyData input and vtkImageData source;
//  Also, vtkProbeFilter does not pass through normals of the input (for a good reason!).
//  This filter adds passing normals.
//

#ifndef IPROBEFILTER_H
#define IPROBEFILTER_H


#include "igenericfilter.h"
#include <vtkProbeFilter.h>


class vtkImageData;
class vtkPointSet;


class iProbeFilter : public iGenericFilter<vtkProbeFilter,vtkDataSet,vtkDataSet>
{

	iGenericFilterTypeMacro(iProbeFilter,vtkProbeFilter);

public:

	void RestrictToType(const char *name);

	vtkDataSet* SourceData();

protected:
	
	virtual int FillInputPortInformation(int port, vtkInformation *info);
	virtual int FillOutputPortInformation(int port, vtkInformation *info);

	virtual void ProvideOutput();

private:

	void ProduceOutputForImageData(vtkPointSet *input, vtkPointSet *output, vtkImageData *source);

	const char *mRestrictedType;

private:

	/* blocked */ vtkDataObject *GetSource(){ return this->vtkProbeFilter::GetSource(); }
};

#endif  // IPROBEFILTER_H


