/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


//
//  A combined class for ALL IFrIT actors & mappers - they must be the same so that renderers
//  can be copied. Thus, it is the most general scaled LOD actor.
//  This actor will render itself efficiently in multiple windows.
//
//
#ifndef IACTOR_H
#define IACTOR_H


#include <vtkLODActor.h>


#include "iarray.h"
#include "ivtk.h"

class iActor;
class iDataLimits;
class iLookupTable;
struct iPair;
class iPalette;

class vtkAlgorithmOutput;
class vtkPlaneCollection;
class vtkPolyData;
class vtkPolyDataMapper;
class vtkRandomAttributeGenerator;


class iActorSubject
{
	//
	//  It is really just a struct...
	//
	friend class iActor;
	friend class iActorSubjectPointer;

private:

	iActorSubject(iActor *parent, vtkWindow *win);
	virtual ~iActorSubject();

	vtkWindow *mWindow;
	vtkPolyDataMapper *mMapper;
	vtkMapperCollection *mLODMappers;
};


class iActorSubjectPointer
{

public:

	iActorSubjectPointer(iActorSubject *subject);
	iActorSubjectPointer(){ mPointer = 0; }

	inline iActorSubject* Pointer() const { return mPointer; }
	inline iActorSubject* operator->() const { return mPointer; }

	void operator=(iActorSubject *p);
	void operator=(const iActorSubjectPointer &p);
	bool operator==(const iActorSubjectPointer &p) const;

private:

	iActorSubject *mPointer;
};


class iActor: public vtkLODActor 
{
	
public:
	
	vtkTypeMacro(iActor,vtkLODActor);
	static iActor* New(vtkPlaneCollection *planes = 0);

	virtual void ReleaseGraphicsResources(vtkWindow *win);
	virtual void Render(vtkRenderer *ren, vtkMapper *m);

	inline bool GetScaled(){ return mScaled; }
	void SetScaled(bool s);

	inline double GetBasicScale(){ return mBasicScale; }
	void SetBasicScale(double s);

	inline const double* GetAxisScale(){ return mAxisScale; }
	void SetAxisScale(double sx, double sy, double sz);

	//
	//  Mapper functionality
	//
	void SetInputData(vtkPolyData *data);
	void SetInputConnection(vtkAlgorithmOutput *port);

	void SetScalarVisibility(bool s);
	void SetScalarRange(float min, float max);
	void SetScalarRange(const iPair &p);

	void SyncWithLimits(iDataLimits *l, int v);
	inline iLookupTable* GetLookupTable() const { return mLookupTable; }

	void AddMapperObserver(unsigned long event, vtkCommand *obs);

	void ColorByArrayComponent(int arrayNum, int component);

#ifdef I_DEBUG
	static void SetRandomColoring(bool s){ mRandomColoring = s; }
#endif

protected:
	
	iActor(vtkPlaneCollection *planes = 0);
	virtual ~iActor();
	
private:

	bool SetCurrentWindow(vtkWindow *win);

	double mBasicScale, mAxisScale[3];
	bool mScaled;
	iLookupArray<iActorSubjectPointer> mSubjects;
	iActorSubject *mDummySubject, *mCurrentSubject;
	iActorSubjectPointer mDummySubjectPointer;
	vtkPolyDataMapper *mDefaultMapper;
	vtkMapperCollection *mDefaultLODMappers; 
	iLookupTable *mLookupTable;

#ifdef I_DEBUG
	vtkRandomAttributeGenerator *mRandomizer;
	static bool mRandomColoring;
	int mSavedInterpolationMode;
#endif
};

#endif // IACTOR_H
