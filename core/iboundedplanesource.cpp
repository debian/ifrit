/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iboundedplanesource.h"


#include "ierror.h"

#include <vtkPlaneSource.h>
#include <vtkPolyData.h>
#include <vtkTransformPolyDataFilter.h>


//
// iBoundedPlaneSource class
//
iBoundedPlaneSource* iBoundedPlaneSource::New(iDataConsumer *consumer)
{
	IASSERT(consumer);
	return new iBoundedPlaneSource(consumer);
}


iBoundedPlaneSource::iBoundedPlaneSource(iDataConsumer *consumer) : iBoundedPolyDataSource(consumer,true,false)
{
	mSource = vtkPlaneSource::New(); IERROR_CHECK_MEMORY(mSource);

	mSource->SetOrigin(0.0,0.0,0.0);
	mSource->SetPoint1(7.0,0.0,0.0);
	mSource->SetPoint2(0.0,7.0,0.0);
	mSource->SetCenter(0.0,0.0,0.0);

	mFilter->SetInputConnection(mSource->GetOutputPort());
}


iBoundedPlaneSource::~iBoundedPlaneSource()
{
	mSource->Delete();
}


void iBoundedPlaneSource::AddObserverToSource(unsigned long e, vtkCommand *c, float p)
{
	mSource->AddObserver(e,c,p);
}


void iBoundedPlaneSource::UpdateSourceResolution()
{
	mSource->SetResolution(mResolution,mResolution);
}


float iBoundedPlaneSource::GetSourceMemorySize() const
{
	float s = 0.0;
	s += mSource->GetOutput()->GetActualMemorySize();
	return s;
}


void iBoundedPlaneSource::UpdateBoundaryConditions()
{
}

