/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "idirectory.h"


#include "ierror.h"

#include <vtkDirectory.h>

//
//  templates
//
#include "iarray.tlh"


iDirectory::iDirectory()
{
}


bool iDirectory::Open(const iString &fname)
{
	vtkDirectory *d = vtkDirectory::New(); IERROR_CHECK_MEMORY(d);

	if(d->Open(fname.ToCharPointer()) == 0)
	{
		//
		//  Is the file name attached?
		//
		if(fname.Contains("/")==0 || d->Open(fname.Section("/",0,-2).ToCharPointer())==0) return false; else
		{
			mDirectory = fname.Section("/",0,-2) + "/";
		}
	}
	else
	{
		mDirectory = fname;
		if(fname[-1] != '/') mDirectory += "/";
	}

	mFiles.Clear();
	int i, n = d->GetNumberOfFiles();
	for(i=0; i<n; i++)
	{
		mFiles.Add(d->GetFile(i)); // assume that all files are different, so AddUnique is not needed.
	}

	d->Delete();

	return true;
}

	
void iDirectory::Close()
{
	mDirectory.Clear();
	mFiles.Clear();
}


const iString iDirectory::GetFile(int i, bool prefixed) const
{
	static const iString null = "";

	if(i>=0 && i<mFiles.Size()) return (prefixed) ? mDirectory+mFiles[i] : mFiles[i]; else return null;
}


const iString iDirectory::GetFile(const iString &name, int skip) const
{
	bool prefixed;

	iString fname(name);
	if(fname.Contains(mDirectory))
	{
		fname = fname.Part(mDirectory.Length());	
		prefixed = true; 
	}
	else
	{
		prefixed = false;
	}

	int i = mFiles.Find(fname);
	if(i >= 0) i += skip;
	return this->GetFile(i,prefixed);
}


bool iDirectory::Make(const iString &name)
{
	return (vtkDirectory::MakeDirectory(name.ToCharPointer()) != 0);
}


const iString& iDirectory::Separator()
{
	static const iString sep("/");
	return sep;
}


const iString& iDirectory::Current()
{
	static const iString cur("./");
	return cur;
}


void iDirectory::ExpandFileName(iString& fname, const iString &path)
{
#ifdef _WIN32
	//
	//  Make sure we are Windows compatible
	//
	fname.Replace("\\",iDirectory::Separator());
#endif

	//
	//  Apply standard expansion of the plus sign
	//
	if(!path.IsEmpty() && fname[0]=='+') fname.Replace(0,1,path);

	//
	//  Is this an unprefixed file name?
	//
	if(fname.Contains(iDirectory::Separator()) == 0) fname = iDirectory::Current() + fname;
}

