/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ivtktype.h"


#include "ivtk.h"

#include <vtkImageData.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>


namespace ivtkType
{
	template<> const char* Name<vtkDataSet>()
	{
		return "vtkDataSet";
	}

	template<> const char* Name<vtkImageData>()
	{
		return "vtkImageData";
	}

	template<> const char* Name<vtkPolyData>()
	{
		return "vtkPolyData";
	}


	bool IsThereScalarData(vtkDataSet *d)
	{
		return (d!=0 && d->GetPointData()!=0 && d->GetPointData()->GetScalars()!=0 &&
			d->GetPointData()->GetScalars()->GetSize()>0 &&
			d->GetPointData()->GetScalars()->GetDataType()==VTK_FLOAT);
	}


	bool IsThereVectorData(vtkDataSet *d)
	{
		return (d!=0 && d->GetPointData()!=0 && d->GetPointData()->GetVectors()!=0 &&
			d->GetPointData()->GetVectors()->GetSize()>0 &&
			d->GetPointData()->GetVectors()->GetDataType()==VTK_FLOAT &&
			d->GetPointData()->GetVectors()->GetNumberOfComponents()==3);
	}


	bool IsThereTensorData(vtkDataSet *d)
	{
		return (d!=0 && d->GetPointData()!=0 && d->GetPointData()->GetTensors()!=0 &&
			d->GetPointData()->GetTensors()->GetSize()>0 &&
			d->GetPointData()->GetTensors()->GetDataType()==VTK_FLOAT &&
			d->GetPointData()->GetTensors()->GetNumberOfComponents()==9);
	}
};

