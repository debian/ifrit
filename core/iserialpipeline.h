/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  This is base class for all parallellizable pipelines
//

#ifndef ISERIALPIPELINE_H
#define ISERIALPIPELINE_H


#include "igenericfilter.h"


template<class Filter, class InputType>
class iSerialPipeline : public Filter
{

public:

	virtual iSerialPipeline<Filter,InputType>* Copy() const = 0;

	virtual float GetMemorySize()
	{
		return this->Filter::GetMemorySize() + this->GetContentsMemorySize();
	}

	virtual void UpdateContents(int n, int info = 0) = 0;

	void SetGlobalInput(InputType *i)
	{
		this->mGlobalInput = i;
	}

	InputType* GetGlobalInput() const {	return this->mGlobalInput; }

	virtual void SetNthInput(int num, vtkDataObject *input){ Filter::SetNthInput(num,input); } // vtkProcessObject::SetNthInput(num,input) is protected

protected:

	iSerialPipeline(iViewSubject *vo, int numInputs) : Filter(vo,numInputs,true,true)
	{
		this->mGlobalInput = 0;
	}

	virtual float GetContentsMemorySize() const = 0;

	InputType *mGlobalInput;
};


//
//  Create specific filters
//
typedef iSerialPipeline<iAbstractPolyDataToPolyDataFilter,vtkPolyData>  iPolyDataToPolyDataPipeline;
typedef iSerialPipeline<iAbstractGridDataToPolyDataFilter,vtkImageData> iGridDataToPolyDataPipeline;
typedef iSerialPipeline<iAbstractGridDataToGridDataFilter,vtkImageData> iGridDataToGridDataPipeline;
typedef iSerialPipeline<iAbstractAnyDataToPolyDataFilter,vtkDataSet> iAnyDataToPolyDataPipeline;

#endif  // ISERIALPIPELINE_H
