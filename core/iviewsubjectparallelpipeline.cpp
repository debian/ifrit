/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iviewsubjectparallelpipeline.h"


#include "ierror.h"
#include "iparallel.h"
#include "iparallelmanager.h"
#include "iviewmodule.h"
#include "iviewsubject.h"
#include "iviewsubjectpipelinedatamanager.h"

#include <vtkDataSet.h>
#include <vtkPolyData.h>

//
//  Templates
//
#include "iarray.tlh"
#include "igenericfilter.tlh"


iViewSubjectParallelPipeline* iViewSubjectParallelPipeline::New(iViewInstance *owner, int numInputs, int id)
{
	IASSERT(owner);
	return new iViewSubjectParallelPipeline(owner,numInputs,id);
}


iViewSubjectParallelPipeline::iViewSubjectParallelPipeline(iViewInstance *owner, int numInputs, int id) : iViewSubjectPipeline(owner,numInputs), iParallelWorker(owner->GetViewModule()->GetParallelManager()), mId(id)
{
	mConfigureTime = 0UL;
	mOwner = owner;

	//
	//  Create the master thread which is always present
	//
	iViewSubjectPipeline *f = mOwner->CreatePipeline(mId); IERROR_CHECK_MEMORY(f);
	mWorkPipelines.Add(f);

	this->SetNumberOfOutputPorts(f->GetNumberOfOutputPorts());

#ifndef I_NO_CHECK
	if(this->GetNumberOfInputPorts() != f->GetNumberOfInputPorts())
	{
		IBUG_FATAL("Misconfiguration in iViewSubjectParallelPipeline.");
	}
#endif

	//
	//  Create data manager
	//
	mDataManager = iViewSubjectPipelineDataManager::New(owner); IERROR_CHECK_MEMORY(mDataManager);
}


iViewSubjectParallelPipeline::~iViewSubjectParallelPipeline()
{
	while(mWorkPipelines.Size() > 0) mWorkPipelines.RemoveLast()->Delete();
	mDataManager->Delete();
}


int iViewSubjectParallelPipeline::FillInputPortInformation(int port, vtkInformation *info)
{
	return mWorkPipelines[0]->FillInputPortInformation(port,info);
}


int iViewSubjectParallelPipeline::FillOutputPortInformation(int port, vtkInformation *info)
{
	return mWorkPipelines[0]->FillOutputPortInformation(port,info);
}


void iViewSubjectParallelPipeline::Modified()
{
	int i;
	for(i=0; i<mWorkPipelines.Size(); i++) 
	{
		mWorkPipelines[i]->Modified();
	}
	this->iViewSubjectPipeline::Modified();
}


void iViewSubjectParallelPipeline::UpdateContents()
{
	int i;
	for(i=0; i<mWorkPipelines.Size(); i++) 
	{
		mWorkPipelines[i]->UpdateContents();
	}
	this->Modified();
}


void iViewSubjectParallelPipeline::UpdateContents(const iViewSubjectPipeline::Key &key)
{
	int i;
	for(i=0; i<mWorkPipelines.Size(); i++) 
	{
		mWorkPipelines[i]->UpdateContents(key);
	}
	this->Modified();
}


void iViewSubjectParallelPipeline::Reconfigure()
{
	//
	//  Actual reconfiguring is done at run-time
	//
	mConfigureTime = 0UL;
}


void iViewSubjectParallelPipeline::ConfigurePipelines(vtkDataSet *input)
{
	int i, np = this->GetManager()->GetNumberOfProcessors();

	if(np < 1)
	{
		IBUG_FATAL("Cannot execute on 0 processors.");
		return;
	}

	//
	//  Delete extra pipelines
	//
	while(mWorkPipelines.Size() > np) mWorkPipelines.RemoveLast()->Delete();

	//
	//  Create missing ones
	//
	while(mWorkPipelines.Size() < np)
	{
		iViewSubjectPipeline *f = mOwner->CreatePipeline(mId); IERROR_CHECK_MEMORY(f);
		mWorkPipelines.Add(f);
	}

	//
	//  Bring all pipelines up to date
	//
	for(i=0; i<mWorkPipelines.Size(); i++) mWorkPipelines[i]->UpdateContents();

	//
	//  Assign global inputs to parallel filters
	//
 	for(i=0; i<mWorkPipelines.Size(); i++)
	{
		mWorkPipelines[i]->SetGlobalInput(input);
	}

	//
	//  Ask data manager to distribute the data 
	//
	iArray<vtkDataSet*> workInputs;
	workInputs.Resize(mWorkPipelines.Size());

	if(workInputs.Size() == 1)
	{
		workInputs[0] = input->NewInstance();
		workInputs[0]->ShallowCopy(input);
	}
	else
	{
		this->mDataManager->DistributeData(input,workInputs);
	}

	for(i=0; i<mWorkPipelines.Size(); i++)
	{
		mWorkPipelines[i]->SetInputData(workInputs[i]);
		workInputs[i]->Delete();
	}

	//
	//  If the filters accept more than one input, just pass other inputs
	//
	if(this->GetNumberOfInputPorts() > 1)
	{
		int j;
		for(i=0; i<mWorkPipelines.Size(); i++)
		{
			for(j=1; j<this->GetNumberOfInputPorts(); j++) this->mWorkPipelines[i]->SetInputConnection(j,this->GetInputConnection(j,0));
		}
	}

	this->iViewSubjectPipeline::Modified();
	mConfigureTime = this->GetMTime();
}


int iViewSubjectParallelPipeline::ExecuteStep(int, iParallel::ProcessorInfo &p)
{
	if(p.NumProcs != mWorkPipelines.Size()) return 1;

	this->mWorkPipelines[p.ThisProc]->Update();
	return 0;
}


void iViewSubjectParallelPipeline::ProvideInfo()
{
#ifdef IVTK_5
	this->OutputData()->SetUpdateExtentToWholeExtent();
#else
	this->SetUpdateExtentToWholeExtent();
#endif
}


void iViewSubjectParallelPipeline::ProvideOutput()
{
	vtkDataSet *input = this->InputData();
	int i, j;

	for(j=0; j<this->GetNumberOfOutputPorts(); j++)
	{
		this->OutputData(j)->Initialize();
	}

	//this->SetUpdateExtentToWholeExtent();

	//
	//  Do we need to re-distribute the data?
	//
	bool redo = (mWorkPipelines.Size() != this->GetManager()->GetNumberOfProcessors());
	for(j=0; !redo && j<this->GetNumberOfInputPorts(); j++) 
	{
		if(this->InputData(j)->GetMTime() > this->mConfigureTime) redo = true;
	}
	if(redo)
	{
		this->ConfigurePipelines(input);
	}

	//
	//  Now run the pipelines
	//
	if(mWorkPipelines.Size() == 1)
	{
		//
		//  Serial execution
		//
		this->mWorkPipelines[0]->Update();
	}
	else
	{
		//
		//  Parallel execution
		//
#ifdef I_DEBUG
		switch(iParallelManager::DebugSwitch)
		{
		case 1:
			{
				mWorkPipelines[0]->Update();
				break;
			}
		case 2:
			{
				mWorkPipelines[1]->Update();
				break;
			}
		case 3:
			{
				mWorkPipelines[0]->Update();
				mWorkPipelines[1]->Update();
				break;
			}
		default:
			{
				this->ParallelExecute(0);
			}
		}
#else
		this->ParallelExecute(0);
#endif
	}

	if(this->GetAbortExecute()) return;

	//
	//  Ask DataManager to collect the data 
	//
	iArray<vtkDataSet*> workOutputs;
	workOutputs.Resize(mWorkPipelines.Size());

	for(j=0; j<this->GetNumberOfOutputPorts(); j++)
	{
	 	for(i=0; i<mWorkPipelines.Size(); i++)
		{
			workOutputs[i] = mWorkPipelines[i]->OutputData(j);
		}

		mDataManager->CollectData(workOutputs,this->OutputData(j));
	}

	if(this->IsOptimizedForMemory())
	{
		this->RemoveInternalData();
	}
}


void iViewSubjectParallelPipeline::RemoveInternalData()
{
	int i;

 	for(i=0; i<mWorkPipelines.Size(); i++)
	{
		mWorkPipelines[i]->RemoveInternalDataAndOutputs();
	}
}


float iViewSubjectParallelPipeline::GetMemorySize()
{
	int i;
	float s = this->iViewSubjectPipeline::GetMemorySize();

	for(i=0; i<mWorkPipelines.Size(); i++) 
	{
		s += mWorkPipelines[i]->GetMemorySize();
	}
	
	return s;
}


void iViewSubjectParallelPipeline::UpdateReplicas()
{
}
