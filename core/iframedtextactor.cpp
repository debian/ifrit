/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iframedtextactor.h"


#include "ierror.h"
#include "ioverlayhelper.h"

#include <vtkActor2D.h>
#include <vtkCellArray.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper2D.h>
#include <vtkProperty2D.h>
#include <vtkRenderer.h>
#include <vtkTextProperty.h>

//
//  Templates
//
#include "iarray.tlh"
#include "igenericprop.tlh"


iFramedTextActor* iFramedTextActor::New(iRenderTool *rt)
{
	IASSERT(rt);
	return new iFramedTextActor(rt);
}


iFramedTextActor::iFramedTextActor(iRenderTool *rt) : iTextActor(rt)
{
	this->SetPadding(0.02);

	vtkPolyData *data;
	vtkCellArray *cells;
	vtkPolyDataMapper2D *mapper;
	vtkCoordinate *coord;

	mBorderPoints = vtkPoints::New(VTK_FLOAT); IERROR_CHECK_MEMORY(mBorderPoints);
	mBorderPoints->SetNumberOfPoints(4);

	//
	//  Border
	//
	data = vtkPolyData::New(); IERROR_CHECK_MEMORY(data);
	data->SetPoints(mBorderPoints);

	cells = vtkCellArray::New(); IERROR_CHECK_MEMORY(cells);
	cells->InsertNextCell(5);
	cells->InsertCellPoint(0);
	cells->InsertCellPoint(1);
	cells->InsertCellPoint(2);
	cells->InsertCellPoint(3);
	cells->InsertCellPoint(0);
	data->SetLines(cells);
	cells->Delete();

	mapper = vtkPolyDataMapper2D::New(); IERROR_CHECK_MEMORY(mapper);
	mapper->SetInputData(data);
	data->Delete();
	coord = vtkCoordinate::New(); IERROR_CHECK_MEMORY(coord);
	coord->SetCoordinateSystemToNormalizedViewport();
	mapper->SetTransformCoordinate(coord);
	coord->Delete();

	mBorderActor = vtkActor2D::New(); IERROR_CHECK_MEMORY(mBorderActor);
	this->PrependComponent(mBorderActor);
	mBorderActor->SetMapper(mapper);
	mapper->Delete();

	//
	//  Lining
	//
	data = vtkPolyData::New(); IERROR_CHECK_MEMORY(data);
	data->SetPoints(mBorderPoints);

	cells = vtkCellArray::New(); IERROR_CHECK_MEMORY(cells);
	cells->InsertNextCell(4);
	cells->InsertCellPoint(0);
	cells->InsertCellPoint(1);
	cells->InsertCellPoint(2);
	cells->InsertCellPoint(3);
	data->SetPolys(cells);
	cells->Delete();
	
	mapper = vtkPolyDataMapper2D::New(); IERROR_CHECK_MEMORY(mapper);
	mapper->SetInputData(data);
	data->Delete();
	coord = vtkCoordinate::New(); IERROR_CHECK_MEMORY(coord);
	coord->SetCoordinateSystemToNormalizedViewport();
	mapper->SetTransformCoordinate(coord);
	coord->Delete();

	mLiningActor = vtkActor2D::New(); IERROR_CHECK_MEMORY(mLiningActor);
	this->PrependComponent(mLiningActor);
	mLiningActor->SetMapper(mapper);
	mapper->Delete();


	this->SetTransparent(true);
}


iFramedTextActor::~iFramedTextActor()
{
	mBorderPoints->Delete();
	mBorderActor->Delete();
	mLiningActor->Delete();
}


void iFramedTextActor::SetTransparent(bool s)
{
	if(s != mTransparent)
	{
		mTransparent = s;
		mLiningActor->SetVisibility(mTransparent?0:1);
		this->Modified();
	}
}


void iFramedTextActor::UpdateGeometry(vtkViewport* vp)
{
	this->iTextActor::UpdateGeometry(vp);
	if(!this->IsEnabled()) return;
	
	int mag = this->GetOverlayHelper()->GetRenderingMagnification();
	if(mag == 1)
	{
		//
		//  Assign properties
		//
		mBorderActor->GetProperty()->SetColor(this->GetOverlayHelper()->GetColor(vp).ToVTK());
		mLiningActor->GetProperty()->SetColor(vp->GetBackground());
	}

	//
	//  Scale lines
	//
	mBorderActor->GetProperty()->SetLineWidth(2*mag);

	//
	//  Place border and lining
	//
	if(mag == 1)
	{
		this->GetBounds(vp,wBounds);

		int j;
		for(j=0; j<4; j++) wTrueBounds[j] = wBounds[j];
	}
	else
	{
		int winij[2];
		this->GetOverlayHelper()->ComputePositionShiftsUnderMagnification(winij);
		
		wBounds[0] = mag*wTrueBounds[0] - winij[0];
		wBounds[1] = mag*wTrueBounds[1] - winij[0];
		wBounds[2] = mag*wTrueBounds[2] - winij[1];
		wBounds[3] = mag*wTrueBounds[3] - winij[1];
	}

	//	
	//  Define the border
	//
	mBorderPoints->SetPoint(0,wBounds[0],wBounds[2],0.0);
	mBorderPoints->SetPoint(1,wBounds[1],wBounds[2],0.0);
	mBorderPoints->SetPoint(2,wBounds[1],wBounds[3],0.0);
	mBorderPoints->SetPoint(3,wBounds[0],wBounds[3],0.0);
}
