/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  This is base class for all parallellizable pipelines
//

#ifndef IVIEWSUBJECTPIPELINE_H
#define IVIEWSUBJECTPIPELINE_H


#include "igenericfilter.h"
#include <vtkDataSetAlgorithm.h>


#include "iarray.h"
#include "istring.h"

class iViewInstance;


class iViewSubjectPipeline : public iGenericFilter<vtkDataSetAlgorithm,vtkDataSet,vtkPolyData>
{

	friend class iViewInstance;
	friend class iViewSubjectParallelPipeline;

protected:

	//
	//  Mechanism for automatically keeping track of pipeline parameters (call them controls for brevity)
	//  Access them with keys
	//
	class Key
	{
		friend class iViewSubjectPipeline;
	public:
		typedef void (iViewSubjectPipeline::*UpdaterType)();
		Key(UpdaterType updater, const char *cname, const char *mname);
		~Key();
		inline UpdaterType GetUpdater() const { return mUpdater; }
		inline const iString& Tag() const { return mTag; }
		inline const char* ClassName() const { return mClassName; }
		inline const char* MethodName() const { return mMethodName; }
		inline bool operator==(const Key& other) const { return (mTag == other.mTag); }
		inline bool operator<(const Key& other) const { return (mTag < other.mTag); }
		void Update(iViewSubjectPipeline *pipeline) const;
	private:
		UpdaterType mUpdater;
		const char *mClassName, *mMethodName;
		const iString mTag;
		typedef iGenericOrderedArray<const Key*,iString> RegistryType;
		static iGenericOrderedArray<const Key*,iString>& Registry();
		static const iString& Lookup(const Key* const &ptr);
	};

public:

	vtkTypeMacro(iViewSubjectPipeline,vtkDataSetAlgorithm);

	virtual void UpdateContents();
	virtual void UpdateContents(const Key &key);

	void SetGlobalInput(vtkDataSet *input);
	vtkDataSet* GetGlobalInput() const { return mGlobalInput; }

	virtual float GetMemorySize();
	virtual void RemoveInternalData();
	void RemoveInternalDataAndOutputs();

protected:

	iViewSubjectPipeline(iViewInstance *owner, int numInputs);
	~iViewSubjectPipeline();

	//
	//  This function should be used to create all filters. Then they will be deleted automatically
	//  and Clear() will automatically remove the data a filter uses.
	//
	template<class Filter> Filter* CreateFilter();
	//
	//  This function can be used in case a child class wants to remove a filter. 
	//  Filters created with CreateFilter are deleted automatically together with the pipeline, 
	//  normally so there is no need to use this function.
	//
	template<class Filter> void DeleteFilter(Filter *filter);

	virtual void UpdateReplicas() = 0;

	//
	//  Our members
	//
	iViewInstance *mOwner;
	vtkDataSet *mGlobalInput;
	mutable iLookupArray<vtkAlgorithm*> mInternalFilters;
};


//
//  Useful macros
//
#define iPipelineKeyDeclareMacro(_class_,_method_) \
	public: \
	static const Key Key##_method_; \
	protected: \
	virtual void Update##_method_()


#define iPipelineKeyDefineMacro(_class_,_method_) \
	const iViewSubjectPipeline::Key _class_::Key##_method_(static_cast<iViewSubjectPipeline::Key::UpdaterType>(&_class_::Update##_method_),#_class_,"Update"#_method_)

#endif  // IVIEWSUBJECTPIPELINE_H
