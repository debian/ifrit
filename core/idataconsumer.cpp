/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "idataconsumer.h"


#include "idata.h"
#include "idatahandler.h"
#include "idatalimits.h"
#include "idatareader.h"
#include "idatasubject.h"
#include "ierror.h"
#include "iviewmodule.h"


//
//  Templates
//
#include "iarray.tlh"


using namespace iParameter;


int iDataConsumer::mModeGlobal = DataConsumerOptimizationMode::OptimizeForSpeed;


//
//  Base class for all objects that use data
//
iDataConsumer::iDataConsumer(iViewModule *vm, const iDataType &type) : iViewModuleComponent(vm), mDataType(type), mPrimaryDataInfo(new iDataInfo(type))
{
	IASSERT(vm);
	IERROR_CHECK_MEMORY(mPrimaryDataInfo);
	mSecondaryDataInfo = new iDataInfo; IERROR_CHECK_MEMORY(mSecondaryDataInfo);

	mModeLocal = mModeGlobal;
	mOverrideGlobal = false;
}


iDataConsumer::~iDataConsumer()
{
	delete mPrimaryDataInfo;
	delete mSecondaryDataInfo;

	//
	//  Check that all DataHandlers have been deleted too. 
	//  Fails for VTK 5.0.0 - Garbage collection is more convoluted.
	//
	if(mDataHandlers.Size() > 0)
	{
		int i;
		for(i=0; i<mDataHandlers.Size(); i++) mDataHandlers[i]->mConsumer = 0;
	}
}


void iDataConsumer::AddSecondaryDataType(const iDataType &type)
{
	*mSecondaryDataInfo += type;
}


float iDataConsumer::GetMemorySize()
{
	int i;
	float s = 0.0;

	for(i=0; i<mDataHandlers.Size(); i++) s += mDataHandlers[i]->GetMemorySize();

	return s;
}


void iDataConsumer::RemoveInternalData()
{
	int i;
	for(i=0; i<mDataHandlers.Size(); i++) mDataHandlers[i]->RemoveInternalData();
}


void iDataConsumer::SetGlobalOptimizationMode(int mode)
{
	if(DataConsumerOptimizationMode::IsValid(mode))
	{
		mModeGlobal = mode;
	}
}


void iDataConsumer::SetOptimizationMode(int mode)
{
	//
	//  Reset the global mode;
	//
	if(DataConsumerOptimizationMode::IsValid(mode))
	{
		mModeLocal = mode;
		mOverrideGlobal = true;
	}
	else if(mode==DataConsumerOptimizationMode::ResetToGlobal)
	{
		mModeLocal = mModeGlobal;
		mOverrideGlobal = false;
	}
}


int iDataConsumer::GetOptimizationMode() const 
{
	if(mOverrideGlobal) return mModeLocal; else return mModeGlobal;
}


//
//  DataHandler registry functionality
//
void iDataConsumer::RegisterDataHandler(iDataHandler *c)
{
	mDataHandlers.AddUnique(c);
}


void iDataConsumer::UnRegisterDataHandler(iDataHandler *c)
{
	mDataHandlers.Remove(c);
}


bool iDataConsumer::IsUsingData(const iDataType &type, bool onlyprimary) const
{
	if(mDataType == type) return true;

	return (!onlyprimary && mSecondaryDataInfo->Includes(type));
}


iDataSubject* iDataConsumer::GetSubject() const
{
	iDataSubject *subject = this->GetViewModule()->GetReader()->GetSubject(mDataType);
	IASSERT(subject);
	return subject;
}


iDataLimits* iDataConsumer::GetLimits() const
{
	return this->GetSubject()->GetLimits();
}


bool iDataConsumer::IsThereData() const
{
	return this->GetSubject()->IsThereData();
}


bool iDataConsumer::SyncWithLimits(int var, int mode)
{
	int i;
	if(!this->SyncWithLimitsBody(var,mode)) return false;
	for(i=0; i<mDataHandlers.Size(); i++)
	{
		if(!mDataHandlers[i]->SyncWithLimits(var,mode)) return false;
	}
	return true;
}


bool iDataConsumer::SyncWithLimitsBody(int var, int mode)
{
	return true;
}


//
//  iSynchRequest class and its specializations
//
iSyncRequest::iSyncRequest(const iDataType& type) : mDataType(type)
{
}


iSyncRequest::~iSyncRequest()
{
}



iSyncWithDataRequest::iSyncWithDataRequest(const iDataType& type) : iSyncRequest(type)
{
}


void iSyncWithDataRequest::Sync(iDataConsumer *consumer) const
{
	IASSERT(consumer);

	consumer->SyncWithData();
}



iSyncWithLimitsRequest::iSyncWithLimitsRequest(int var, int mode, const iDataType& type) : iSyncRequest(type), Var(var), Mode(mode)
{
}


void iSyncWithLimitsRequest::Sync(iDataConsumer *consumer) const
{
	IASSERT(consumer);

	consumer->SyncWithLimits(Var,Mode);
}
