/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


//
//  This is base class for all parallellizable pipelines
//

#ifndef IVIEWSUBJECTPARALLELPIPELINE_H
#define IVIEWSUBJECTPARALLELPIPELINE_H


#include "iviewsubjectpipeline.h"
#include "iparallelworker.h"


#include "iarray.h"

class iViewSubjectPipelineDataManager;


class iViewSubjectParallelPipeline : public iViewSubjectPipeline, protected iParallelWorker
{

public:

	vtkTypeMacro(iViewSubjectParallelPipeline,iViewSubjectPipeline);
	static iViewSubjectParallelPipeline* New(iViewInstance *owner = 0, int numInputs = 0, int id = 0);

	virtual void UpdateContents();
	virtual void UpdateContents(const iViewSubjectPipeline::Key &key);

	void Modified();
	void Reconfigure();

	inline iViewSubjectPipelineDataManager* GetDataManager() const { return mDataManager; }

	virtual float GetMemorySize();
	virtual void RemoveInternalData();

protected:

	iViewSubjectParallelPipeline(iViewInstance *owner, int numInputs, int id);
	virtual ~iViewSubjectParallelPipeline();

	virtual int FillInputPortInformation(int port, vtkInformation *info);
	virtual int FillOutputPortInformation(int port, vtkInformation *info);

	virtual void ProvideInfo();
	virtual void ProvideOutput(); 
	virtual void UpdateReplicas();

	virtual int ExecuteStep(int step, iParallel::ProcessorInfo &p);

private:

	void ConfigurePipelines(vtkDataSet *input);

	const int mId;
	unsigned long mConfigureTime;
	iViewInstance *mOwner;
	iViewSubjectPipelineDataManager *mDataManager;
	iArray<iViewSubjectPipeline*> mWorkPipelines;
};

#endif  // IVIEWSUBJECTPARALLELPIPELINE_H

