/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iboundedspheresource.h"


#include "idataconsumer.h"
#include "idatasubject.h"
#include "ierror.h"
#include "ispheresource.h"
#include "iviewmodule.h"

#include <vtkAppendPolyData.h>
#include <vtkPolyData.h>
#include <vtkTransformPolyDataFilter.h>


//
// iBoundedSphereSource class
//
iBoundedSphereSource* iBoundedSphereSource::New(iDataConsumer *consumer)
{
	IASSERT(consumer);
	return new iBoundedSphereSource(consumer);
}


iBoundedSphereSource::iBoundedSphereSource(iDataConsumer *consumer) : iBoundedPolyDataSource(consumer,false,true)
{
	mAppend = vtkAppendPolyData::New(); IERROR_CHECK_MEMORY(mAppend);
	for(int i=0; i<8; i++)
	{
		mSource[i] = iSphereSource::New(); IERROR_CHECK_MEMORY(mSource[i]);
	}
	mAppend->UserManagedInputsOn();
	mAppend->SetInputConnection(mSource[0]->GetOutputPort());

	mSource[0]->SetResolution(1);
	mSource[0]->SetRadius(1.0);

	mFilter->SetInputConnection(mSource[0]->GetOutputPort());
}


iBoundedSphereSource::~iBoundedSphereSource()
{
	mAppend->Delete();
	for(int i=0; i<8; i++)
	{
		mSource[i]->Delete();
	}
}


void iBoundedSphereSource::AddObserverToSource(unsigned long e, vtkCommand *c, float p)
{
	mAppend->AddObserver(e,c,p);
	for(int i=0; i<8; i++)
	{
		mSource[i]->AddObserver(e,c,p);
	}
}


void iBoundedSphereSource::UpdateSourceResolution()
{
	mSource[0]->SetResolution(mResolution);
}


float iBoundedSphereSource::GetSourceMemorySize() const
{
	float s = 0.0;
	for(int i=0; i<8; i++)
	{
		s += mSource[i]->GetOutput()->GetActualMemorySize();
	}
	s += mAppend->GetOutput()->GetActualMemorySize();
	return s;
}


void iBoundedSphereSource::UpdateBoundaryConditions()
{
	int i, j, l, side[3];
	double cen[3], xb;

	//
	//  Test for out-of-bounds pieces
	//
	for(i=0; i<3; i++)
	{
		side[i] = 0;
		if(this->Consumer()->GetSubject()->IsDirectionPeriodic(i))
		{
			xb = mCenter[i] - mRadius;
			if(xb < mBounds[2*i+0]) side[i] = -1;  //  we should only cross one boundary
			xb = mCenter[i] + mRadius;
			if(xb > mBounds[2*i+1]) side[i] =  1;
		}
	}

	if(side[0]==0 && side[1]==0 && side[2]==0)
	{
		//
		//  Fully inside
		//
		mFilter->SetInputConnection(mSource[0]->GetOutputPort());
		return;
	}

	mFilter->SetInputConnection(mAppend->GetOutputPort());

	l = 1;
	for(i=0; i<3; i++)
	{
		if(side[i] != 0)
		{
			mAppend->SetNumberOfInputs(2*l);
			for(j=l; j<2*l; j++)
			{
				mSource[j]->SetRadius(1.0);
				mSource[j]->SetResolution(mResolution);
				mSource[j-l]->GetCenter(cen);
				cen[i] -= 2.0*side[i]/mRadius;
				mSource[j]->SetCenter(cen);
#ifdef IVTK_5
				mAppend->SetInputByNumber(j,mSource[j]->GetOutput());
#else
				mAppend->SetInputConnectionByNumber(j,mSource[j]->GetOutputPort());
#endif
			}
			l *= 2;
		}
	}
}






