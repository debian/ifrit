/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Some useful system-level functions
//
#ifdef _WIN32
#include <windows.h>
//#include <winbase.h>
#else
#include <limits.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#endif


#include <vtkTimerLog.h>


namespace iSystem
{
	//
	//  Sleeps for a given # of milliseconds
	//
	void Sleep(int msec)
	{
		if(msec <= 0) return;
#ifdef _WIN32
		::Sleep(msec);
#else
		usleep((unsigned long)msec*1000UL);
#endif
	}


	//
	//  Gives the physical memory for the machine
	//
#define __MB	(float)1048576.0
	float GetMemoryLimit()
	{
#ifdef _WIN32
		MEMORYSTATUS r;
		::GlobalMemoryStatus(&r);
		return float(r.dwTotalPhys)/__MB;
#else
		static struct rlimit r;
		int d = getrlimit(RLIMIT_DATA,&r);
		float v1 = float(r.rlim_cur)/__MB;
#if defined(_SC_PHYS_PAGES) && defined(_SC_PAGESIZE)
		unsigned long np = sysconf(_SC_PHYS_PAGES);
		unsigned long ps = sysconf(_SC_PAGESIZE);
		float v2 = float(np)*float(ps)/__MB;
		if(v1 > v2) v1 = v2;
#endif
		if(d == 0) return v1; else return ULONG_MAX/__MB;
#endif
	}


	//
	//  Get the endiness of the machine.
	//
	bool IsBigEndianMachine()
	{
		unsigned char c[4];
		c[0] = c[1] = c[2] = (unsigned char)0;
		c[3] = (unsigned char)1;
		
		return (*((int *)c) == 1);
	}
};
