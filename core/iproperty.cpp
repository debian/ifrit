/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iproperty.h"


#include "ierror.h"
#include "ipiecewisefunction.h"

//
//  Templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


iProperty::Key iProperty::KeyCounter = 1;
iGenericOrderedArray<const iProperty*,iProperty::Key> iProperty::mRegistry(iProperty::EntryLookup);


iProperty::iProperty(iObject *owner, const iString &fname, const iString &sname, int rank) : mRank(rank), mLongName(fname), mShortName(sname), mTag(iString::FromNumber(9-rank,"%1d")+sname) ,mKey(KeyCounter++)
{
	IASSERT(owner);
	mOwner = owner;

	if(mRank<0 || mRank>9)
	{
		IBUG_FATAL("Property rank must be between 0 and 9");
	}

	mFlags = 0;

	mRegistry.Add(this);
}


iProperty::~iProperty()
{
	mRegistry.Remove(this);
}


bool iProperty::IsImmediatePost() const
{
	return mOwner->IsImmediatePost();
}


void iProperty::RequestRender() const
{
	if(!this->TestFlag(_DoNotRequestRender))
	{
		mOwner->RequestRender();
	}
}


bool iProperty::_RequestPush() const
{
	return mOwner->_RequestPush(mKey);
}



const iString iProperty::HelpTag() const
{
	return this->Owner()->HelpTag() + "." + this->ShortName();
}


void iProperty::AddFlag(Flag f)
{
	mFlags |= f;
}


bool iProperty::TestFlag(Flag f) const
{
	return (mFlags & f) != 0;
}


const iProperty* iProperty::FindPropertyByKey(const Key& key)
{
	int i = mRegistry.FindByKey(key);
	if(i > -1) return mRegistry[i]; else return 0;
}


const iProperty::Key& iProperty::EntryLookup(const iProperty* const &ptr)
{
	return ptr->GetKey();
}


//
//  Abstract variable property
//
iPropertyVariable::iPropertyVariable(iObject *owner, const iString &fname, const iString &sname, int rank) : iProperty(owner,fname,sname,rank)
{
	if(!this->ShortName().IsEmpty()) this->Owner()->mVariableProperties.Add(this);
}


iPropertyVariable::~iPropertyVariable()
{
	if(!this->ShortName().IsEmpty()) this->Owner()->mVariableProperties.Remove(this);
}


//
//  Abstract function property (not a variable)
//
iPropertyFunction::iPropertyFunction(iObject *owner, const iString &fname, const iString &sname, int rank) : iProperty(owner,fname,sname,rank)
{
	this->Owner()->mFunctionProperties.Add(this);
}


iPropertyFunction::~iPropertyFunction()
{
	this->Owner()->mFunctionProperties.Remove(this);
}


//
//  Special property that correctly updates the piecewise function
//
iPropertyPiecewiseFunction::iPropertyPiecewiseFunction(iObject *owner, const iString &fname, const iString &sname, int rank) : iPropertyDataVariable<iType::Pair>(owner,fname,sname,rank)
{
	mFunction = 0;
}


iPropertyPiecewiseFunction::~iPropertyPiecewiseFunction()
{
}


void iPropertyPiecewiseFunction::AttachFunction(iPiecewiseFunction *f)
{
	mFunction = f;
}


int iPropertyPiecewiseFunction::Size() const
{
	if(mFunction != 0) return mFunction->N(); else return 0;
}


bool iPropertyPiecewiseFunction::Resize(int dim, bool force)
{
	if(dim>1 && mFunction!=0)
	{
		int i, olddim = mFunction->N();
		for(i=olddim; i<dim; i++) mFunction->AddPoint(mFunction->N()-2);
		for(i=dim; i<olddim; i++) mFunction->RemovePoint(mFunction->N()-2);
		return true;
	}
	else return false;
}


const iPair& iPropertyPiecewiseFunction::GetValue(int i) const
{
	static const iPair null(0,0);

	if(mFunction != 0) return mFunction->P(i); else return null;
}


bool iPropertyPiecewiseFunction::SetValue(int i, const iPair& p) const
{
	if(mFunction != 0)
	{
		mFunction->MovePoint(i,p);
		return true;
	}
	else return false;
}


bool iPropertyPiecewiseFunction::Copy(const iPropertyVariable *p)
{
	const iPropertyPiecewiseFunction *other = dynamic_cast<const iPropertyPiecewiseFunction*>(p);
	if(other != 0)
	{
		this->Resize(other->Size(),true);
		return iPropertyDataVariable<iType::Pair>::Copy(p);
	}
	else return false;
}


//
//  Simple command with no arguments
//
iPropertyAction::iPropertyAction(iObject *owner, CallerType caller, const iString &fname, const iString &sname, int rank) : iPropertyTypeFunction<iType::Bool>(owner,fname,sname,rank)
{
	mCaller = caller; IASSERT(caller);

	mQueue = 0;
}

	
bool iPropertyAction::CallAction() const
{
	return (this->Owner()->*mCaller)();
}


bool iPropertyAction::_PostCall() const
{
	if(this->IsImmediatePost())
	{
		if(this->CallAction())
		{
			this->RequestRender();
			return true;
		}
		else return false;
	}
	else
	{
		//
		//  Message queue
		//
		mQueue++;
		return this->_RequestPush();
	}
}


bool iPropertyAction::PushPostedValue_() const
{
	bool ret = this->CallAction();
	mQueue--;

	if(ret)
	{
		this->RequestRender();
		return true;
	}
	else return false;
}


bool iPropertyAction::HasPostedRequests() const
{
	return (mQueue > 0);
}

