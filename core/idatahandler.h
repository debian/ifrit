/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IDATAHANDLER_H
#define IDATAHANDLER_H


#include "iviewmodulecomponent.h"


class iDataConsumer;
class iDataLimits;


class iDataHandler : public iViewModuleComponent
{

	friend class iDataConsumer;

public:

	bool IsOptimizedForSpeed() const;
	bool IsOptimizedForMemory() const;
	bool IsOptimizedForQuality() const;

	iDataLimits* GetLimits() const;
	void GetPeriodicities(bool per[3]) const; // helper function

	virtual float GetMemorySize() = 0;
	virtual void RemoveInternalData() = 0;

protected:

	iDataHandler(iDataConsumer *consumer);
    virtual ~iDataHandler();

	iDataConsumer* Consumer() const { return mConsumer; }

	virtual bool SyncWithLimits(int var, int mode);

private:

	iDataConsumer *mConsumer;
};

#endif // IVISUALMODULEDATAHANDLER_H
