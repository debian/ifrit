/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "isurfaceviewsubject.h"


#include "iactor.h"
#include "icolorbar.h"
#include "icommondatadistributors.h"
#include "icoredatasubjects.h"
#include "idatalimits.h"
#include "idatasubject.h"
#include "ierror.h"
#include "ihistogrammaker.h"
#include "isurfacepipeline.h"
#include "iviewmodule.h"
#include "iviewobject.h"
#include "iviewsubjectobserver.h"
#include "iviewsubjectparallelpipeline.h"

#include <vtkMassProperties.h>
#include <vtkPolyDataConnectivityFilter.h>
#include <vtkProperty.h>
#include <vtkTriangleFilter.h>

//
//  Templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


using namespace iParameter;


//
// A prop class
//
iSurfaceViewInstance::iSurfaceViewInstance(iSurfaceViewSubject *owner) : iMultiViewInstance(owner,1,1,ColorBarPriority::Surface,false), iPropPlacementHelper(owner->GetViewModule())
{
	mIsoVar = 0;
	mIsoMethod = 0;
	mIsoLevel = 0.0;

	//
	//  Pay tribute to IFrITness
	//
	int nvar = this->GetLimits()->GetNumVars();
	if(mIsoVar==0 && (this->GetLimits()->GetName(0)=="Neutral fraction" || this->GetLimits()->GetName(0)=="HI fraction"))
	{
		mIsoLevel = 0.5; 
		if(nvar > 1)
		{
			mPaintVar = 1;
			mActors[0]->SyncWithLimits(this->GetLimits(),1);
			mActors[0]->SetScalarVisibility(true);
		}
	} 
	else mIsoLevel = this->GetLimits()->GetRange(mIsoVar).Max;
	
	mIsoReduction = 1;
	mIsoSmoothing = 0;
	mNormalsFlipped = false;
	mIsoOptimization = false;
	mAlternativeReductionMethod = false;

	mMethod = SurfaceMethod::Isosurface;
	mSize = 0.5;
}


iSurfaceViewInstance::~iSurfaceViewInstance()
{
}


void iSurfaceViewInstance::ConfigureBody()
{
	//
	//  Create pipeline (must be created after the object is fully created)
	//	
	this->AddMainPipeline(1);
	mActors[0]->SetInputConnection(this->Pipeline()->GetOutputPort(0));
}


bool iSurfaceViewInstance::SetIsoMethod(int m)
{
	if(m>=0 && m<=1)
	{
		if(mIsoMethod == m) return true;

		mIsoMethod = m;
		this->Pipeline()->UpdateContents(iSurfacePipeline::KeyIsoMethod);
		return true;
	}
	else return false;
}


bool iSurfaceViewInstance::SetIsoVar(int v)
{
	if(v<0 || (this->IsThereData() && v>=this->GetLimits()->GetNumVars())) return false;

	if(v != mIsoVar)
	{
		mIsoVar = v;
		if(this->IsThereData())
		{
			this->Pipeline()->UpdateContents(iSurfacePipeline::KeyIsoVar);
			this->SetIsoLevel(this->GetLimits()->GetRange(mIsoVar).Max);
			this->SetNormalsFlipped(mNormalsFlipped);
		}
	}
	return true;
}


bool iSurfaceViewInstance::SetIsoLevel(float l)
{
	if(mIsoVar < this->GetLimits()->GetNumVars())
	{
		if(this->IsThereData())
		{
			float range[2];
			this->GetSubject()->GetHistogramMaker()->GetRange(mIsoVar,range);
			if(l < range[0]) l = range[0];
			if(l > range[1]) l = range[1];
		}
		mIsoLevel = l;
		this->Pipeline()->UpdateContents(iSurfacePipeline::KeyIsoLevel);
		return true;
	}
	else return false;
}


bool iSurfaceViewInstance::SetIsoReduction(int s)
{
	mIsoReduction = s; 
	this->Pipeline()->UpdateContents(iSurfacePipeline::KeyIsoPipeline);
	return true;
} 


bool iSurfaceViewInstance::SetAlternativeReductionMethod(bool s)
{
	mAlternativeReductionMethod = s; 
	this->Pipeline()->UpdateContents(iSurfacePipeline::KeyIsoPipeline);
	return true;
} 


bool iSurfaceViewInstance::SetIsoOptimization(bool s)
{
	mIsoOptimization = s; 
	this->Pipeline()->UpdateContents(iSurfacePipeline::KeyIsoPipeline);
	return true;
} 


bool iSurfaceViewInstance::SetIsoSmoothing(int p)
{
	mIsoSmoothing = p;
	this->Pipeline()->UpdateContents(iSurfacePipeline::KeyIsoPipeline);
	return true;
}


bool iSurfaceViewInstance::SetNormalsFlipped(bool s)
{ 
	mNormalsFlipped = s;
	this->Pipeline()->UpdateContents(iSurfacePipeline::KeyNormalsFlipped);
	return true;
}


void iSurfaceViewInstance::ShowBody(bool show)
{
	if(show)
	{
		if(mPaintVar >= 0) 
		{
			mActors[0]->SyncWithLimits(this->GetLimits(),mPaintVar);
		}
		mActors[0]->VisibilityOn();
	} 
	else 
	{
		mActors[0]->VisibilityOff();
	}
}


bool iSurfaceViewInstance::SetMethod(int m)
{
	if(m>=0 && m<3)
	{
		mMethod = m;
		this->Pipeline()->UpdateContents(iSurfacePipeline::KeyMethod);
		return true;
	}
	else return false;
}


void iSurfaceViewInstance::UpdateSize()
{
	this->Pipeline()->UpdateContents(iSurfacePipeline::KeySphereSize);
}


void iSurfaceViewInstance::UpdatePosition()
{
	mPosition.PeriodicWrap();
	this->Pipeline()->UpdateContents(iSurfacePipeline::KeyObjectPosition);
}


void iSurfaceViewInstance::UpdateDirection()
{
	this->Pipeline()->UpdateContents(iSurfacePipeline::KeyPlaneDirection);
}


void iSurfaceViewInstance::OutputProperties(iString& str) const
{
	if(!this->CanBeShown()) return;

	float l = this->GetViewModule()->InOpenGLCoordinates() ? 1 : 0.5*this->GetViewModule()->GetBoxSize();

	vtkTriangleFilter *tf = vtkTriangleFilter::New(); IERROR_CHECK_MEMORY(tf);
	tf->SetInputConnection(this->Pipeline()->GetOutputPort(0));
	tf->PassLinesOff();
	tf->PassVertsOff();

	//
	//  Surface area & volume
	//
	vtkMassProperties *f1 = vtkMassProperties::New(); IERROR_CHECK_MEMORY(f1);
	f1->SetInputConnection(tf->GetOutputPort());
	f1->Update();
	str = "Area:   " + iString::FromNumber(f1->GetSurfaceArea()*l*l) + "\n";
	str += "Volume: " + iString::FromNumber(f1->GetVolume()*l*l*l) + "\n";
	f1->Delete();

	//
	//  Connectivity info
	//
	vtkPolyDataConnectivityFilter *f2 = vtkPolyDataConnectivityFilter::New(); IERROR_CHECK_MEMORY(f2);
	f2->SetInputConnection(tf->GetOutputPort());
	f2->ScalarConnectivityOff();
	f2->SetExtractionModeToAllRegions();
	f2->Update();
	str += "Regions: " + iString::FromNumber(f2->GetNumberOfExtractedRegions()) + "\n";
	f2->Delete();

	tf->Delete();
}


bool iSurfaceViewInstance::CanBeShown() const
{
	return (this->Owner()->GetSubject()->IsThereScalarData() && mIsoVar<this->GetLimits()->GetNumVars());
}


bool iSurfaceViewInstance::SyncWithDataBody()
{
	this->Pipeline()->SetInputData(this->GetSubject()->GetData());

	if(mIsoVar >= this->GetLimits()->GetNumVars())
	{
		this->SetIsoVar(-1);
	}
	return true;
}


iViewSubjectPipeline* iSurfaceViewInstance::CreatePipeline(int)
{
	return new iSurfacePipeline(this);
}


void iSurfaceViewInstance::ConfigureMainPipeline(iViewSubjectPipeline *p, int)
{
	iViewSubjectParallelPipeline *pp = iRequiredCast<iViewSubjectParallelPipeline>(INFO,p);

	iImageDataDistributor *idd = new iImageDataDistributor(pp->GetDataManager());
	pp->GetDataManager()->AddDistributor(idd);
	pp->GetDataManager()->AddCollector(new iPolyDataCollector(pp->GetDataManager(),idd));
}


//
//  Main class
//
iSurfaceViewSubject::iSurfaceViewSubject(iViewObject *parent, const iDataType &type) : iMultiViewSubject(parent,parent->LongName(),parent->ShortName(),parent->GetViewModule(),type,ViewObject::Flag::IsSameOpacity|ViewObject::Flag::IsSameColor,1),
	iViewSubjectPropertyConstructMacro(Int,iSurfaceViewInstance,Method,m),
	iViewSubjectPropertyConstructMacro(Bool,iSurfaceViewInstance,NormalsFlipped,nf),
	iViewSubjectPropertyConstructMacro(Int,iSurfaceViewInstance,IsoVar,v),
	iViewSubjectPropertyConstructMacro(Float,iSurfaceViewInstance,IsoLevel,l),
	iViewSubjectPropertyConstructMacro(Int,iSurfaceViewInstance,IsoMethod,im),
	iViewSubjectPropertyConstructMacro(Int,iSurfaceViewInstance,IsoSmoothing,sm),
	iViewSubjectPropertyConstructMacro(Int,iSurfaceViewInstance,IsoReduction,rd),
	iViewSubjectPropertyConstructMacro(Bool,iSurfaceViewInstance,IsoOptimization,op),
	iViewSubjectPropertyConstructMacro(Bool,iSurfaceViewInstance,AlternativeReductionMethod,rda),
	iViewSubjectPropertyConstructMacro(Vector,iSurfaceViewInstance,PlaneDirection,pd),
	iViewSubjectPropertyConstructMacro(Double,iSurfaceViewInstance,SphereSize,ss),
	iViewSubjectPropertyConstructMacroGeneric(Vector,iSurfaceViewInstance,Position,x,SetObjectPosition,GetObjectPosition)
{
	IsoVar.AddFlag(iProperty::_FunctionsAsIndex);
}


iSurfaceViewSubject::~iSurfaceViewSubject()
{
}


void iSurfaceViewSubject::OutputProperties(int i, iString& str) const
{
	if(i>=0 && i<mInstances.Size())
	{
		iRequiredCast<iSurfaceViewInstance>(INFO,mInstances[i])->OutputProperties(str);
	}
}


int iSurfaceViewSubject::DepthPeelingRequestBody() const
{
	int i;
	for(i=0; i<mInstances.Size(); i++)
	{
		if(Opacity.GetValue(i) < 1) return 1;
	}

	return 0;
}


bool iSurfaceViewSubject::InterpolateBetweenTwoInstances(int i1, int i2, int n, bool testOnly)
{
	//
	//  Split one if for debugging
	//
	if(i1<0 || i1>=mInstances.Size() || i2<0 || i2>=mInstances.Size() || i1==i2) return false;

	if(i1 > i2)
	{
		int i = i2;
		i2 = i1;
		i1 = i2;
	}

	iSurfaceViewInstance *sub1 = iRequiredCast<iSurfaceViewInstance>(INFO,mInstances[i1]);
	iSurfaceViewInstance *sub2 = iRequiredCast<iSurfaceViewInstance>(INFO,mInstances[i2]);

	int m1 = sub1->GetMethod();
	int m2 = sub2->GetMethod();
	int v1 = sub1->GetIsoVar();
	int v2 = sub2->GetIsoVar();
	int q1 = sub1->GetPaintVar();
	int q2 = sub2->GetPaintVar();
	float l1 = sub1->GetIsoLevel(); 
	float l2 = sub2->GetIsoLevel();

	if(m1!=m2 || q1!=q2 || (m1==SurfaceMethod::Isosurface && (v1!=v2 || l1==l2))) return false;
	if(testOnly) return true;

	const iVector3D& p1 = sub1->GetPosition().BoxValue(); 
	const iVector3D& p2 = sub2->GetPosition().BoxValue();
	float s1 = sub1->GetSphereSize();
	float s2 = sub2->GetSphereSize();
	const iVector3D& d1 = sub1->GetPlaneDirection();
	const iVector3D& d2 = sub2->GetPlaneDirection();
	float o1 = sub1->GetOpacity();
	float o2 = sub2->GetOpacity();
	iColor c1 = sub1->GetColor();
	iColor c2 = sub2->GetColor();

	float os = (o2-o1)/(n+1);
	float cs[3];
	cs[0] = float(c2.Red()-c1.Red())/(n+1);
	cs[1] = float(c2.Green()-c1.Green())/(n+1);
	cs[2] = float(c2.Blue()-c1.Blue())/(n+1);
	float ls = (l2-l1)/(n+1);
	float ss = (s2-s1)/(n+1);
	double ps[3], ds[3];
	int i;
	for(i=0; i<3; i++)
	{
		ps[i] = (p2[i]-p1[i])/(n+1);
		ds[i] = (d2[i]-d1[i])/(n+1);
	}

	int j;
	iVector3D p, d;

	//
	//  The reverse order because we insert instances in place of i2
	//
	for(j=n; j>=1; j--)
	{
		float o = o1 + os*j;
		iColor c = iColor(iMath::Round2Int(c1.Red()+cs[0]*j),iMath::Round2Int(c1.Green()+cs[1]*j),iMath::Round2Int(c1.Blue()+cs[2]*j));
		float l = l1 + ls*j;
		double s = s1 + ss*j;
		for(i=0; i<3; i++)
		{
			p[i] = p1[i] + ps[i]*j;
			d[i] = d1[i] + ds[i]*j;
		}

		//
		//  Create a new instance
		//
		iSurfaceViewInstance *sub = new iSurfaceViewInstance(this);
		if(sub == 0)
		{
			return false;
		}
		sub->Configure();

		mInstances.Add(sub);
		for(i=mInstances.Size()-1; i>i2; i--)
		{
			mInstances[i] = mInstances[i-1];
		}
		mInstances[i2] = sub;
		this->CopyInstances(i2,i1);

		//
		//  Set the parameters of the new instance
		//
		sub->SetMethod(m1);
		sub->SetOpacity(o);
		sub->SetColor(c);
		sub->SetIsoVar(v1);
		sub->SetPaintVar(q1);
		sub->SetIsoLevel(l);
		sub->SetBoxPosition(p);
		sub->SetSphereSize(s);
		sub->SetPlaneDirection(d);

		sub->SyncWithData();
		sub->Show(this->IsVisible());
	}

	this->UpdateAutomaticShading();
	return true;
}

