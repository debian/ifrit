/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iuniformgriddata.h"


namespace iUniformGridData_Private
{
	template<class T>
	void CICTemplate(bool periodic[3], int dims[3], double org[3], double spa[3], T pos[3], int ijk1[3], int ijk2[3], double d1[3], double d2[3]);
};


using namespace iParameter;
using namespace iUniformGridData_Private;


iUniformGridData* iUniformGridData::New()
{
	return new iUniformGridData();
}


iUniformGridData::iUniformGridData()
{
	mVoxelLocation = VoxelLocation::Vertex;
	mPeriodicOffsets[0] = mPeriodicOffsets[1] = mPeriodicOffsets[2] = 0;
}


iUniformGridData::~iUniformGridData()
{
}


void iUniformGridData::SetInternalProperties(int voxLoc, int fileDims[3], int dataDims[3])
{
	if(VoxelLocation::IsValid(voxLoc) && fileDims[0]<=dataDims[0] && fileDims[1]<=dataDims[1] && fileDims[2]<=dataDims[2])
	{
		int i;
		for(i=0; i<3; i++) mPeriodicOffsets[i] = dataDims[i] - fileDims[i];
		mVoxelLocation = voxLoc;
	}
	else
	{
		vtkErrorMacro("Invalid attempt to set internal properties.");
	}
}


void iUniformGridData::GetPeriodicOffsets(int off[3]) const
{
	int i;
	for(i=0; i<3; i++) off[i] = mPeriodicOffsets[i];
}


void iUniformGridData::GetNumCells(int numCells[3])
{
	int i, dims[3];

	this->GetDimensions(dims);

	for(i=0; i<3; i++)
	{
		if(mVoxelLocation == VoxelLocation::Vertex)
		{
			numCells[i] = dims[i] - 1;
		}
		else
		{
			numCells[i] = dims[i] - mPeriodicOffsets[i];
		}
	}
}


void iUniformGridData::GetBounds(double min[3], double max[3])
{
	int i, dims[3];
	double org[3], spa[3];

	this->GetOrigin(org);
    this->GetSpacing(spa);
	this->GetDimensions(dims);

	for(i=0; i<3; i++)
	{
		if(mVoxelLocation == VoxelLocation::Vertex)
		{
			min[i] = org[i];
			max[i] = min[i] + spa[i]*(dims[i]-1);
		}
		else
		{
			min[i] = org[i] - 0.5*spa[i];
			max[i] = min[i] + spa[i]*(dims[i]-mPeriodicOffsets[i]);
		}
	}
}


void iUniformGridData::GetGlobalOrigin(double org[3])
{
	int i;
	double spa[3];

	this->GetOrigin(org);
    this->GetSpacing(spa);

	if(mVoxelLocation != VoxelLocation::Vertex)
	{
		for(i=0; i<3; i++)
		{
			org[i] -= 0.5*spa[i];
		}
	}
}


void iUniformGridData::Initialize()
{
	mVoxelLocation = VoxelLocation::Vertex;
	mPeriodicOffsets[0] = mPeriodicOffsets[1] = mPeriodicOffsets[2] = 0;

	this->vtkImageData::Initialize();
}


void iUniformGridData::ShallowCopy(vtkDataObject *obj)
{
	iUniformGridData *data = iUniformGridData::SafeDownCast(obj);
	if(data != 0)
    {
		int i;
		for(i=0; i<3; i++) mPeriodicOffsets[i] = data->mPeriodicOffsets[i];
		mVoxelLocation = data->mVoxelLocation;
	}

	this->vtkImageData::ShallowCopy(obj);
}


void iUniformGridData::DeepCopy(vtkDataObject *obj)
{
	iUniformGridData *data = iUniformGridData::SafeDownCast(obj);
	if(data != 0)
    {
		int i;
		for(i=0; i<3; i++) mPeriodicOffsets[i] = data->mPeriodicOffsets[i];
		mVoxelLocation = data->mVoxelLocation;
	}

	this->vtkImageData::DeepCopy(obj);
}


//
//  Two helper functions
//
void iUniformGridData::CIC(bool periodic[3], int dims[3], double org[3], double spa[3], float pos[3], int ijk1[3], int ijk2[3], double d1[3], double d2[3])
{
	CICTemplate(periodic,dims,org,spa,pos,ijk1,ijk2,d1,d2);
}


void iUniformGridData::CIC(bool periodic[3], int dims[3], double org[3], double spa[3], double pos[3], int ijk1[3], int ijk2[3], double d1[3], double d2[3])
{
	CICTemplate(periodic,dims,org,spa,pos,ijk1,ijk2,d1,d2);
}


namespace iUniformGridData_Private
{
	template<class T>
	void CICTemplate(bool periodic[3], int dims[3], double org[3], double spa[3], T pos[3], int ijk1[3], int ijk2[3], double d1[3], double d2[3])
	{
		int i;
		for(i=0; i<3; i++)
		{
			d1[i] = (pos[i]-org[i])/spa[i];
			ijk1[i] = (int)floor(d1[i]);
			d2[i] = d1[i] - ijk1[i];
			d1[i] = 1.0 - d2[i];

			if(periodic[i])
			{
				while(ijk1[i] < 0) ijk1[i] += dims[i];  // in case it is extended 
				while(ijk1[i] >= dims[i]) ijk1[i] -= dims[i];
			}
			else
			{
				if(ijk1[i] < 0) ijk1[i] = 0;  
				if(ijk1[i] >= dims[i]) ijk1[i] = dims[i] - 1;
			}

			ijk2[i] = ijk1[i] + 1;

			if(periodic[i])
			{
				while(ijk2[i] >= dims[i]) ijk2[i] -= dims[i]; 
			}
			else
			{
				if(ijk2[i] >= dims[i]) ijk2[i] = dims[i] - 1;
			}
		}
	}
};

