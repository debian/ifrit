/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "idataobject.h"


#include "idatasubject.h"
#include "ierror.h"
#include "iviewmodule.h"

//
//  templates
//
#include "iarray.tlh"


//
//  Main class
//
iDataObject* iDataObject::New(iViewModule *parent)
{
	static iString LongName = "Data";
	static iString ShortName = "d";

	return new iDataObject(parent,LongName,ShortName);
}


iDataObject::iDataObject(iViewModule *parent, const iString &fname, const iString &sname) : iObject(parent,fname,sname), iViewModuleComponent(parent)
{
	mRenderMode = iParameter::RenderMode::NoRender;
}


iDataObject::~iDataObject()
{
}


void iDataObject::WriteState(iString &s) const
{
	int i;
	iString s1;

	s.Init(1000);

	this->WriteHead(s);
	s += "Known data types:\n";

	for(i=0; i<mChildren.Size(); i++)
	{
		iDataSubject *sub = iRequiredCast<iDataSubject>(INFO,mChildren[i]);
		s1 = "    " + sub->LongName();
		if(sub->IsThereData())
		{
			while(s1.Length() < 20) s1 += " ";
			s1 += "loaded";
		}
		s += s1 + "\n";
	}
}
