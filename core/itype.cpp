/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "itype.h"


#include "idata.h"
#include "idataid.h"


namespace iType
{
	//
	//  Helper functions
	//
	template<TypeId id, char prefix, int n> iString ValueToStringHelper(typename Traits<id>::Type val, bool exact);
	template<TypeId id, char prefix, int n> bool StringToValueHelper(const iString &str, typename Traits<id>::ContainerType &val);

	//
	//  Instantiated templates
	//
	const iString& Traits<Int>::TypeName() { static iString name("int"); return name; }
	iString Traits<Int>::ValueToString(int val, bool)
	{
		return iString::FromNumber(val);
	}
	bool Traits<Int>::StringToValue(const iString &str, int &val)
	{
		bool ok;
		int ret = str.ToInt(ok);
		if(ok)
		{
			val = ret;
			return true;
		}
		else return false;
	}


	const iString& Traits<Bool>::TypeName() { static iString name("bool"); return name; }
	iString Traits<Bool>::ValueToString(bool val, bool)
	{
		static const iString t("true");
		static const iString f("false");

		return val ? t : f;
	}
	bool Traits<Bool>::StringToValue(const iString &str, bool &val)
	{
		static const iString t("true");
		static const iString f("false");

		if(str == t)
		{
			val = true;
			return true;
		} 
		else if(str == f)
		{
			val = false;
			return true;
		}
		else return false;
	}


	const iString& Traits<Float>::TypeName() { static iString name("float"); return name; }
	iString Traits<Float>::ValueToString(float val, bool exact)
	{
		return iString::FromNumber(val,exact?"%.8g":"%g");
	}
	bool Traits<Float>::StringToValue(const iString &str, float &val)
	{
		bool ok;
		float ret = str.ToFloat(ok);
		if(ok)
		{
			val = ret;
			return true;
		}
		else return false;
	}


	const iString& Traits<Double>::TypeName() { static iString name("double"); return name; }
	iString Traits<Double>::ValueToString(double val, bool exact)
	{
		return iString::FromNumber(val,exact?"%.18lg":"%lg");
	}
	bool Traits<Double>::StringToValue(const iString &str, double &val)
	{
		bool ok;
		double ret = str.ToDouble(ok);
		if(ok)
		{
			val = ret;
			return true;
		}
		else return false;
	}


	const iString& Traits<String>::TypeName() { static iString name("string"); return name; }
	iString Traits<String>::ValueToString(const iString& val, bool)
	{
		static iString q("\"");

		return (q + val + q);
	}
	bool Traits<String>::StringToValue(const iString &str, iString &val)
	{
		if(str[0]!='"' || str[str.Length()-1]!='"')
		{
			return false;
		}
		val = str.Part(1,str.Length()-2);
		return true;
	}


	const iString& Traits<DataType>::TypeName() { static iString name("data"); return name; }
	const iString& Traits<DataType>::ValueToString(const iDataType& val, bool)
	{
		return val.LongName();
	}
	bool Traits<DataType>::StringToValue(const iString &str, iDataId &val)
	{
		const iDataType &t(iDataType::FindTypeByName(str));

		if(t.IsNull()) return false; else
		{
			val = t.GetId();
			return true;
		}
	}


	const iString& Traits<Pair>::TypeName() { static iString name("pair"); return name; }
	iString Traits<Pair>::ValueToString(const iPair& val, bool exact)
	{
		return ValueToStringHelper<Pair,'F',2>(val,exact);
	}
	bool Traits<Pair>::StringToValue(const iString &str, iPair &val)
	{
		return StringToValueHelper<Pair,'F',2>(str,val);
	}


	const iString& Traits<Color>::TypeName() { static iString name("color"); return name; }
	iString Traits<Color>::ValueToString(const iColor& val, bool exact)
	{
		return ValueToStringHelper<Color,'C',4>(val,exact);
	}
	bool Traits<Color>::StringToValue(const iString &str, iColor &val)
	{
		if(StringToValueHelper<Color,'C',4>(str,val)) return true;
		if(StringToValueHelper<Color,'C',3>(str,val)) return true;
		return false;
	}


	const iString& Traits<Vector>::TypeName() { static iString name("vector"); return name; }
	iString Traits<Vector>::ValueToString(const iVector3D& val, bool exact)
	{
		return ValueToStringHelper<Vector,'V',3>(val,exact);
	}
	bool Traits<Vector>::StringToValue(const iString &str, iVector3D &val)
	{
		return StringToValueHelper<Vector,'V',3>(str,val);
	}


	//
	//  Helper functions
	//
	template<TypeId id, char prefix, int n> iString ValueToStringHelper(typename Traits<id>::Type val, bool exact)
	{
		static const char p[3] = { prefix, '{', 0 };
		static const iString s("/");
		static const iString o(p);
		static const iString c("}");

		int i;
		iString str = o;
		for(i=0; i<n; i++)
		{
			if(i > 0) str += s;
			str += Traits<Traits<id>::FieldTypeId>::ValueToString(val[i],exact);
		}
		str += c;
		return str;
	}
	template<TypeId id, char prefix, int n> bool StringToValueHelper(const iString &str, typename Traits<id>::ContainerType &val)
	{
		static const iString s("/");

		if(str[0]!=prefix || str[1]!='{' || str[str.Length()-1]!='}') return false;

		typename Traits<id>::ContainerType ret;
		iString tmp = str.Part(2,str.Length()-3);
		if(tmp.Contains(s)+1 == n)
		{
			int i;
			for(i=0; i<n; i++)
			{
				if(!Traits<Traits<id>::FieldTypeId>::StringToValue(tmp.Section(s,i,i),ret[i])) return false;
			}
			val = ret;
			return true;
		}
		else
		{
			return false;
		}
	}
};

