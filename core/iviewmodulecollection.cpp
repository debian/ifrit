/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iviewmodulecollection.h"


#include "ierror.h"
#include "iimagecomposer.h"
#include "imonitor.h"
#include "ishell.h"

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"
#include "iproperty.tlh"
#include "iobjectcollection.tlh"


iViewModuleCollection* iViewModuleCollection::New(iShell *s)
{
	static iString LongName("Window");
	static iString ShortName("w");

	IASSERT(s);

	iViewModuleCollection *tmp = new iViewModuleCollection(s,LongName,ShortName);
	tmp->CreateMember();
	return tmp;
}

 
iViewModuleCollection::iViewModuleCollection(iShell *s, const iString &fname, const iString &sname) : iObjectCollectionResizable<iViewModule>(s,fname,sname,1), iShellComponent(s),
	CreateNew(iObject::Self(),static_cast<iPropertyAction::CallerType>(&iViewModuleCollection::CallCreateNew),"New","new"),
	CreateCopy(iObject::Self(),static_cast<iPropertyAction1<iType::Int>::CallerType>(&iViewModuleCollection::CallCreateCopy),"Copy","copy"),
	CreateClone(iObject::Self(),static_cast<iPropertyAction1<iType::Int>::CallerType>(&iViewModuleCollection::CallCreateClone),"Clone","clone")
{
	mRenderMode = iParameter::RenderMode::All;
}


iViewModuleCollection::~iViewModuleCollection()
{
	int i;
	//
	//  Deleting view modules is tricky: we need to delete clones first so that
	//  data reader and limits are not deleted before the view object are.
	//
	for(i=0; i<this->Size(); i++) if(this->GetViewModule(i)->IsClone()) this->RemoveMember(i);
}



iViewModule* iViewModuleCollection::GetViewModule(int i) const
{
	return iRequiredCast<iViewModule>(INFO,this->GetMember(i));
}


bool iViewModuleCollection::CreateMember()
{
	if(this->iObjectCollectionResizable<iViewModule>::CreateMember())
	{
		//
		//  Update window numbers and composer
		//
		this->UpdateWindows();
		return true;
	}
	else return false;
}


bool iViewModuleCollection::RemoveMember(int i)
{
	//
	//  First check whether it is the only non-cloned one, then it cannot be deleted
	//
	if(this->GetViewModule(i) == 0) return false;
	if(!this->GetViewModule(i)->IsClone())
	{
		int j;
		bool ok = true;
		for(j=0; ok && j<this->Size(); j++) if(j!=i && !this->GetViewModule(j)->IsClone()) ok = false;
		if(ok) return false;
	}

	//
	//  If this is not a clone, delete all its clones
	//
	if(!this->GetViewModule(i)->IsClone())
	{
		while(this->GetViewModule(i)->mClones.Size() > 0)
		{
			if(!this->RemoveMember(this->GetViewModule(i)->mClones[0]->ParentIndex()))
			{
				this->UpdateWindows();
				return false;
			}
		}
	}
	if(this->iObjectCollectionResizable<iViewModule>::RemoveMember(i))
	{
		//
		//  Update window numbers and composer
		//
		this->UpdateWindows();
		return true;
	}
	else return false;
}


iViewModule* iViewModuleCollection::NewObject()
{
	return new iViewModule(this,this->LongName(),this->ShortName());
}


bool iViewModuleCollection::CallCreateNew()
{
	if(this->CreateMember()) 
	{
		this->GetViewModule(mChildren.MaxIndex())->RequestRender();
		return true;
	}
	return false;
}


bool iViewModuleCollection::CallCreateCopy(int v)
{
	if(v>=0 && v<this->Size() && this->CreateMember()) 
	{
		//
		//  Set the type of the instance created
		//
		iMonitor h(this);
		if(!this->GetViewModule(mChildren.MaxIndex())->CopyState(this->GetViewModule(v)))
		{
			h.PostError("Unable to fully copy the window, some properties may not be set.");
		}
		
		this->GetViewModule(mChildren.MaxIndex())->RequestRender();
		return true;
	}
	return false;
}


bool iViewModuleCollection::CallCreateClone(int v)
{
	if(v>=0 && v<this->Size() && this->CreateMember()) 
	{
		//
		//  Set the type of the instance created
		//
		if(this->GetViewModule(mChildren.MaxIndex())->BecomeClone(this->GetViewModule(v)))
		{
			this->GetViewModule(mChildren.MaxIndex())->RequestRender();
			return true;
		}
		else
		{
			this->RemoveMember(mChildren.MaxIndex());
			return false;
		}
	}
	return false;
}


void iViewModuleCollection::UpdateWindows()
{
	this->GetShell()->GetImageComposer()->UpdateWindowList();
}

