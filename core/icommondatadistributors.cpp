/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "icommondatadistributors.h"


#include "iappendimagedatafilter.h"
#include "iappendpolydatafilter.h"
#include "ierror.h"
#include "iparallelmanager.h"
#include "ireplicatedpolydata.h"
#include "iviewsubject.h"

#include <vtkCellArray.h>
#include <vtkFloatArray.h>
#include <vtkImageData.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>

//
//  Templates
//
#include "iarray.tlh"
#include "igenericfilter.tlh"


//
// Helper class
//
class iBasicDataDistributorHelper
{

public:

	template<class Collector, class Type>
	static bool CollectData(Collector *collector, iArray<vtkDataSet*> &localOutputs, vtkDataSet *globalOutput);

	static vtkDataArray* SplitArray(vtkDataArray *inArray, vtkIdType offset, vtkIdType size);
};


template<class Collector, class Type>
bool iBasicDataDistributorHelper::CollectData(Collector *collector, iArray<vtkDataSet*> &localOutputs, vtkDataSet *globalOutput)
{
	IASSERT(collector);

	//
	//  Append the local data
	//
	if(localOutputs.Size() == 1)
	{
		globalOutput->ShallowCopy(localOutputs[0]);
	}
	else
	{
		int i;
		Type *tmpOutput, *lo;

		collector->RemoveAllInputs();
		for(i=0; i<localOutputs.Size(); i++) 
		{
			lo = Type::SafeDownCast(localOutputs[i]);
			if(lo == 0) return false;
			tmpOutput = Type::New(); IERROR_CHECK_MEMORY(tmpOutput);
			tmpOutput->ShallowCopy(lo);
			collector->AddInputData(tmpOutput);
			tmpOutput->Delete();
		}
		collector->Update();
		collector->RemoveAllInputs();
		globalOutput->ShallowCopy(collector->OutputData());
	}
	return true;
}


vtkDataArray* iBasicDataDistributorHelper::SplitArray(vtkDataArray *inArray, vtkIdType offset, vtkIdType size)
{
	if(inArray != 0)
	{
		vtkDataArray *outArray = inArray->NewInstance(); IERROR_CHECK_MEMORY(outArray);
		int nc = inArray->GetNumberOfComponents();
		outArray->SetNumberOfComponents(nc);
		outArray->SetVoidArray(inArray->GetVoidPointer(nc*offset),nc*size,1);
		return outArray;
	}
	else return 0;
}


//
//  Abstract field data distributor
//
iFieldDataDistributor::iFieldDataDistributor(iViewSubjectPipelineDataManager *manager, const iString &type) : iViewSubjectPipelineDataDistributor(manager,type)
{
	mSplitDim = 2;
}


//
//  vtkImageData distributor
//
iImageDataDistributor::iImageDataDistributor(iViewSubjectPipelineDataManager *manager) : iFieldDataDistributor(manager,"vtkImageData")
{
}


bool iImageDataDistributor::DistributeDataBody(vtkDataSet *globalInput, iArray<vtkDataSet*> &localInputs)
{
	//
	//  Default implementation: split the data in uniform chunks along the last non-trivial dimensions
	//
	vtkImageData *input = vtkImageData::SafeDownCast(globalInput);
	if(input == 0) return false;

	int nproc = localInputs.Size();

	//
	//  Read input structure and choose the split direction
	//
	int i, dims[3], dimsPiece[3];
	double org[3], orgPiece[3], spa[3];

	input->GetOrigin(org);
	input->GetSpacing(spa);
	input->GetDimensions(dims);

	int mSplitDim = 2;
	while(mSplitDim>=0 && dims[mSplitDim]<=1) mSplitDim--;
	if(mSplitDim < 0)
	{
		//
		//  Nothing to split
		//
		for(i=1; i<nproc; i++)
		{
			localInputs[i] = globalInput->NewInstance(); IERROR_CHECK_MEMORY(localInputs[i]);
		}
		nproc = 1;
		mSplitDim = 2;
	}

	//
	//  Adjust the number of processors
	//
	if(dims[mSplitDim]>0 && nproc>dims[mSplitDim])
	{
		for(i=nproc; i<dims[mSplitDim]; i++) 
		{
			localInputs[i] = globalInput->NewInstance(); IERROR_CHECK_MEMORY(localInputs[i]);
		}
		nproc = dims[mSplitDim];
	}

	if(nproc == 1)
	{
		//
		//  Nothing to do for 1 proc
		//
		localInputs[0] = globalInput->NewInstance(); IERROR_CHECK_MEMORY(localInputs[0]);
		localInputs[0]->ShallowCopy(input);
		return true;
	}

    mEdges.Resize(nproc-1);

	//
	//  Split the data between processors
	//
	int kstp, kbeg, kend;
	kstp = (dims[mSplitDim]+nproc-2)/nproc; //  This is point data, hence -2 rather than -1 in kstp

	for(i=0; i<3; i++)
	{
		dimsPiece[i] = dims[i];
		orgPiece[i] = org[i];
	}

	vtkIdType dataOffset = 1, dataSize;
	for(i=0; i<mSplitDim; i++) dataOffset *= dims[i];

	vtkImageData *li;
	vtkDataArray *array;

	for(i=0; i<nproc; i++)
	{
		kbeg = kstp*i;
		kend = kbeg + kstp + 1;  //  This is point data, so the array boundaries must overlap, hence +1 in kend
		if(i == nproc-1) kend = dims[mSplitDim]; else
		{
			mEdges[i] = org[mSplitDim] + spa[mSplitDim]*(kend-1);
		}

		dimsPiece[mSplitDim] = kend - kbeg;
		orgPiece[mSplitDim] = org[mSplitDim] + spa[mSplitDim]*kbeg;

		dataSize = (vtkIdType)dimsPiece[0]*(vtkIdType)dimsPiece[1]*(vtkIdType)dimsPiece[2];

		li = vtkImageData::SafeDownCast(globalInput->NewInstance()); IERROR_CHECK_MEMORY(li);
		localInputs[i] = li;

		li->CopyStructure(input);  // a better way of setting all parameters
		li->SetDimensions(dimsPiece);
		li->SetOrigin(orgPiece);

		array = iBasicDataDistributorHelper::SplitArray(input->GetPointData()->GetScalars(),dataOffset*kbeg,dataSize);
		if(array != 0)
		{
			li->GetPointData()->SetScalars(array);
			array->Delete();
		}

		array = iBasicDataDistributorHelper::SplitArray(input->GetPointData()->GetVectors(),dataOffset*kbeg,dataSize);
		if(array != 0)
		{
			li->GetPointData()->SetVectors(array);
			array->Delete();
		}
		array = iBasicDataDistributorHelper::SplitArray(input->GetPointData()->GetTensors(),dataOffset*kbeg,dataSize);
		if(array != 0)
		{
			li->GetPointData()->SetTensors(array);
			array->Delete();
		}
	}
	return true;
}


//
//  vtkImageData collector
//
iImageDataCollector::iImageDataCollector(iViewSubjectPipelineDataManager *manager) : iViewSubjectPipelineDataCollector(manager,"vtkImageData")
{
	mCollector = iAppendImageDataFilter::New(); IERROR_CHECK_MEMORY(mCollector);
}


iImageDataCollector::~iImageDataCollector()
{
	mCollector->Delete();
}


bool iImageDataCollector::CollectDataBody(iArray<vtkDataSet*> &localOutputs, vtkDataSet *globalOutput)
{
	mCollector->SetNumberOfThreads(localOutputs.Size());
	return iBasicDataDistributorHelper::CollectData<iAppendImageDataFilter,vtkImageData>(mCollector,localOutputs,globalOutput);
}


//
//  vtkPolyData distributor
//
iPolyDataDistributor::iPolyDataDistributor(iViewSubjectPipelineDataManager *manager) : iViewSubjectPipelineDataDistributor(manager,"vtkPolyData")
{
}


bool iPolyDataDistributor::DistributeDataBody(vtkDataSet *globalInput, iArray<vtkDataSet*> &localInputs)
{
	int i;

	//
	//  Default implementation: split the data in uniform chunks by the particle index.
	//  Only split Vert data, assign Line, Poly, and Strip data to the master thread
	//
	vtkPolyData *input = vtkPolyData::SafeDownCast(globalInput);
	if(input == 0) return false;

	int nPieces = localInputs.Size();
	vtkIdType nParticles = input->GetNumberOfPoints();

	if(nPieces > nParticles) nPieces = nParticles;
	if(nPieces < 1) nPieces = 1;
	vtkIdType kbeg, kstp = (nParticles+nPieces-1)/nPieces;

	vtkIdType *inPtr = input->GetVerts()->GetData()->GetPointer(0);
	if(inPtr == 0)
	{
		//
		//  We have no vertices - do not split.
		//
		for(i=0; i<localInputs.Size(); i++)
		{
			localInputs[i] = vtkPolyData::New(); IERROR_CHECK_MEMORY(localInputs[i]);
		}
		localInputs[0]->ShallowCopy(input);
		return true;
	}

	vtkCellArray *tmpCells;
	vtkIdTypeArray *tmpData;
	
	vtkPolyData *li;

	for(i=0; i<nPieces; i++)
	{
		kbeg = kstp*i;
		if(i == nPieces-1) kstp = nParticles - kbeg;
		
		li = vtkPolyData::New(); IERROR_CHECK_MEMORY(li);
		localInputs[i] = li;

		li->Initialize();
		li->ShallowCopy(input);
		if(i > 0)
		{
			li->SetVerts(0);
			li->SetLines(0);
			li->SetPolys(0);
			li->SetStrips(0);
		}

		tmpCells = vtkCellArray::New();
		if(tmpCells != 0)
		{
			tmpData = vtkIdTypeArray::New();
			if(tmpData != 0)
			{
				tmpData->SetArray(inPtr+2*kbeg,2*kstp,1);
				tmpCells->SetCells(kstp,tmpData);
				li->SetVerts(tmpCells);
				tmpData->Delete();
			}
			tmpCells->Delete();
		}
	}

	for(i=nPieces; i<localInputs.Size(); i++)
	{
		localInputs[i] = vtkPolyData::New(); IERROR_CHECK_MEMORY(localInputs[i]);
	}

	return true;
}


//
//  vtkPolyData collector
//
iPolyDataCollector::iPolyDataCollector(iViewSubjectPipelineDataManager *manager, iFieldDataDistributor *distributor) : iViewSubjectPipelineDataCollector(manager,"vtkPolyData")
{
	mFixStiches = true;
	mDistributor = distributor;
	mCollector = iAppendPolyDataFilter::New(manager->Owner()); IERROR_CHECK_MEMORY(mCollector);
}


iPolyDataCollector::~iPolyDataCollector()
{
	mCollector->Delete();
}


bool iPolyDataCollector::CollectDataBody(iArray<vtkDataSet*> &localOutputs, vtkDataSet *globalOutput)
{
	if(!iBasicDataDistributorHelper::CollectData<iAppendPolyDataFilter,vtkPolyData>(mCollector,localOutputs,globalOutput)) return false;

	if(mDistributor==0 || !mFixStiches) return true;

#ifdef I_DEBUG
	if(iParallelManager::DebugSwitch == 4) return true;
#endif

	//
	//  Remove edges
	//
	int splitDim = mDistributor->GetSplitDimension();
	const iArray<double> &innerEdges(mDistributor->GetEdges());
	int extDown = 0;
	int next = 1;
	if(mManager->Owner()->Owner()->GetDataReplicated() != 0)
	{
		int r[6];
		mManager->Owner()->Owner()->GetDataReplicated()->GetReplicationFactors(r);
		extDown = r[2*splitDim];
		next += r[2*splitDim+1] + extDown;
	}

	int i, j, numEdges = innerEdges.Size();
	if(numEdges > 0) 
	{
		int ntot = numEdges*next;
		double *allEdges = new double[ntot]; IERROR_CHECK_MEMORY(allEdges);

		for(j=0; j<next; j++)
		{
			for(i=0; i<numEdges; i++)
			{
				allEdges[i+numEdges*j] = innerEdges[i] + 2*(j-extDown);
			}
		}

		iReplicatedPolyData::PatchStitches(vtkPolyData::SafeDownCast(globalOutput),splitDim,allEdges,ntot);

		delete [] allEdges;
	}

	return true;
}


