/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IPARTICLESPLITTER_H
#define IPARTICLESPLITTER_H


#include "igenericfilter.h"
#include <vtkPolyDataAlgorithm.h>


#include "iarray.h"
#include "ipair.h"

class iParticleViewSubject;

class vtkPoints;
class vtkCellArray;
class vtkFloatArray;
class vtkFloatArray;


class iParticleSplitter : public iGenericPolyDataFilter<vtkPolyDataAlgorithm>
{
	
	friend class iParticleViewSubject;
	iGenericFilterTypeMacro(iParticleSplitter,vtkPolyDataAlgorithm);

public:
	
	inline int GetNumberOfPieces() const { return mRanges.Size(); }

	void SetSplitVariable(int v);
	inline int GetSplitVariable() const { return mSplitVariable; }
	
	void SetSorterMode(bool s);
	inline bool GetSorterMode() const { return mSorterMode; }

	const iPair& GetPieceRange(int n) const { return mRanges[n]; }
	void SetPieceRange(int n, const iPair &p);

	bool CreatePiece(const iPair &p);
	bool DeletePiece(int n);

	void TakeOverData(bool);
	float GetMemorySize() const;

protected:
	
	virtual ~iParticleSplitter();

	virtual void ProvideOutput();
	
private:

	int mSplitVariable;
	bool mOwnsData, mSorted, mSorterMode;

	iArray<iPair> mRanges;

	vtkPoints *mSavedPoints;
	vtkCellArray *mSavedVerts;
	vtkFloatArray *mSavedNorms;
	vtkFloatArray *mSavedVars;

	unsigned long mSortingMTime;
};

#endif // IPARTICLESPLITTER_H
 
