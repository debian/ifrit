/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IPARTICLEVIEWSUBJECT_H
#define IPARTICLEVIEWSUBJECT_H


#include "ipaintedviewsubject.h"


#include "ipair.h"


class iEventObserver;
class iParticleConnector;
class iParticleViewSubject;
class iParticleSplitter;
class iViewObject;

class vtkAlgorithmOutput;
class vtkPolyData;


class iParticleViewInstance : public iPaintedViewInstance
{
	
	friend class iParticleViewSubject;

public:
	
	iDataConsumerTypeMacro(iParticleViewInstance,iPaintedViewInstance);
		
	iObjectSlotMacro1SV(Type,int);

	iObjectSlotMacro1SV(ScaleVar,int);
	iObjectSlotMacro1SV(ScaleFactor,float);
	
	iObjectSlotMacro1SV(FixedSize,float);
	iObjectSlotMacro1SV(LineWidth,int);
	iObjectSlotMacro1SV(AutoScaled,bool);

	iObjectSlotMacro2S(ConnectVar,int);
	iObjectSlotMacro2S(ConnectionBreakVar,int);

protected:

	void SetInputConnection(vtkAlgorithmOutput *port);

	//
	//  Inherited members
	//

protected:
	
	iParticleViewInstance(iParticleViewSubject *owner);
	virtual ~iParticleViewInstance();
	virtual void ConfigureBody();
	virtual void ConfigureMainPipeline(iViewSubjectPipeline *p, int id);
	virtual iViewSubjectPipeline* CreatePipeline(int id);

	virtual void ShowBody(bool s);
	virtual bool CanBeShown() const;

	virtual void UpdateColorBars(){}

	virtual bool IsConnectedToPaintingData() const;

private:

	//
	//  VTK members
	//
	iParticleConnector *mConnector;
	iEventObserver *mCameraEventObserver;
	iParticleViewSubject *mRealOwner;
};


class iParticleViewSubject : public iPaintedViewSubject
{

public:
	
	iViewSubjectTypeMacro1(iParticleView,iPaintedViewSubject);
	static iParticleViewSubject* New(iViewObject *parent, const iDataType &type, const iColor &color);

	//
	//  Splitter functionality (must be first on the list)
	//
	iType::ps_int SplitVar;
	iObjectSlotMacro1SV(SplitVar,int);
	iObjectPropertyMacro3V(SplitRanges,Pair);
	iObjectPropertyMacro1S(SplitRangesTiled,Bool);

	//
	//  Group properties
	//
	iType::vsp_int Type;

 	iType::vsp_int ScaleVar;
	iType::vsp_float ScaleFactor;
	
	iType::vsp_float FixedSize;
	iType::vsp_int LineWidth;
	iType::vsp_bool AutoScaled;

	iType::vsp_int ConnectVar;
	iType::vsp_int ConnectionBreakVar;

	iType::ps_int RenderSortVar;
	iObjectSlotMacro1SV(RenderSortVar,int);

	virtual void BecomeClone(iViewSubject *other);

protected:
	
	iParticleViewSubject(iViewObject *parent, const iDataType &type, const iColor &color = iColor::Black());
	virtual ~iParticleViewSubject();

	virtual void InitInstances();

	virtual bool CreateInstance();
	virtual bool RemoveInstance(int n);

	virtual int DepthPeelingRequestBody() const;

	iParticleSplitter *mSplitter;
};

#endif // IPARTICLEVIEWOBJECT_H
 
