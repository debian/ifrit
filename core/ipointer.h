/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IPOINTER_H
#define IPOINTER_H


//
//  various simple but useful pointer types
//
namespace iPointer
{
	//
	//  helper function
	//
	void Validate(void *ptr);


	//
	//  Small common functionality
	//
	template<class T>
	class TrivialBase
	{

	public:

		//
		// Cast to the holder's type
		//
		inline operator T*() const { return this->mPtr; }

		//
		// Dereference the pointer and return a reference to the contained object.
		//
		inline T& operator*() const { return *this->mPtr; }

		//
		// Provides normal pointer target member access using operator ->.
		//
		inline T* operator->() const { return this->mPtr; }

	protected:

		TrivialBase(T* ptr)
		{
			this->mPtr = ptr;
		}

		T* mPtr;

	private:

		//
		//  Not implemented
		//
		TrivialBase(); // cannot have an automatic default constructor - otherwise the compiler will not require explicit instantination
		TrivialBase& operator=(TrivialBase&);
	};


	//
	//  Simple non-assignable, non-transferable auto-ptr
	//
	template<class T>
	class AutoDeleted : public TrivialBase<T>
	{

	public:

		template<class P>
		AutoDeleted(P parent) : TrivialBase<T>(T::New(parent))
		{
			Validate(this->mPtr);
		}

		AutoDeleted() : TrivialBase<T>(T::New())
		{
			Validate(this->mPtr);
		}

		~AutoDeleted()
		{
			this->mPtr->Delete();
		}

	private:

		//
		//  Not implemented
		//
		//AutoDeleted(); // cannot have an automatic default constructor - otherwise the compiler will not require explicit instantination
		AutoDeleted& operator=(AutoDeleted&);
	};


	//
	//  A pointer that cannot have a NULL value
	//
	template<class T>
	class AlwaysValid : public TrivialBase<T>
	{

	public:

		AlwaysValid(T *ptr) : TrivialBase<T>(ptr)
		{
			Validate(this->mPtr);
		}

	private:

		//
		//  Not implemented
		//
		AlwaysValid();
		AlwaysValid& operator=(AlwaysValid&);
	};


	//
	//  A pointer ordered by GetId() call of the argument class
	//
	template<class T>
	class Ordered : public TrivialBase<T>
	{

	public:

		Ordered(T *ptr) : TrivialBase<T>(ptr)
		{ 
		}

		//
		//  Allow assignment operator
		//
		void operator=(const Ordered<T> &p)
		{
			this->mPtr = p.mPtr;
		}

		bool operator==(const Ordered<T> &other) const
		{
			return (this->mPtr==0 ? other.mPtr==0 : (other.mPtr!=0 && this->mPtr->GetId()==other.mPtr->GetId()));
		}

		bool operator<(const Ordered<T> &other) const
		{
			return (this->mPtr==0 ? other.mPtr!=0 : (other.mPtr!=0 && this->mPtr->GetId()<other.mPtr->GetId()));
		}

	private:

		//
		//  Not implemented
		//
		Ordered();
	};
};

#endif // IPOINTER_H

