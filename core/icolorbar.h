/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef ICOLORBAR_H
#define ICOLORBAR_H


#include "iviewmoduletool.h"


#include "iarray.h"
#include "icolorbaritem.h"
#include "idataid.h"

class iColorBarActor;
class iColorBarLabel;
class iDataType;
class iPalette;

class vtkActor2D;


namespace iParameter
{
	namespace ColorBarPriority
	{
		const int Surface =			0; 
		const int Volume =			1;
		const int CrossSection =	2;
		const int Field =			3;
		const int Particles =		4;
	};
};


class iColorBar: public iViewModuleTool
{
	
public:
	
	vtkTypeMacro(iColorBar,iViewModuleTool);
	static iColorBar* New(iViewModule *vm = 0);
	
	iObjectPropertyMacro1S(Automatic,Bool);
	iObjectPropertyMacro1S(Color,Color);
	iObjectPropertyMacro2S(SideOffset,Float);
	iObjectPropertyMacro2S(Size,Float);
	iType::pv_int Var;
	iType::pv_int Palette;
	iPropertyVector<iType::DataType> DataType;

	iObjectSlotMacro2V(Var,int);
	iObjectSlotMacro2V(Palette,int);
	iObjectSlotMacro2V(DataType,const iDataType&);

	bool Add(int priority, const iColorBarItem &bar);
	void Remove(const iColorBarItem &bar);

	const iColorBarItem* GetBar(int id);
	vtkActor2D* GetActor(int id) const;

	static void SetMethodToImage(bool s);
	static bool SetMethodToImage();

protected:
	
	virtual ~iColorBar();
	
	virtual bool ShowBody(bool s);

private:

	iColorBar(iViewModule *vm, const iString &fname, const iString &sname);

	struct Item
	{
		iColorBarItem Bar;
		int Priority;
		int Count;
		bool operator<(const Item &item) const;
		bool operator==(const Item &item) const;
	};

	iOrderedArray<Item> mAutomaticQueue;
	Item mManualQueue[2];

	iColorBarActor *mActors[2];
};

#endif // ICOLORBAR_H
