/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IARRAY_H
#define IARRAY_H

//
//  Automatically-resizable array of type T; the type must have operator =.
//

template<class T>
class iArray
{

public:

	iArray(int inc = 10);
	virtual ~iArray();

    T& operator[](int i);
    const T& operator[](int i) const;

	inline int Size() const { return this->mNum; }
	inline int MaxIndex() const { return this->mNum-1; }

	virtual void Add(const T &val);
	void Remove(int n);
	void Clear();
	void Resize(int n);

	inline T& Last(){ return this->mArr[this->mNum-1]; }
    inline const T& Last() const { return this->mArr[this->mNum-1]; }
	void AddLast();
	T RemoveLast();  // useful for deleteting all: while(arr.Size() > 0) delete arr.RemoveLast();

	inline const T* Data() const { return this->mArr; }

protected:

	void Extend(int len);

	T* mArr;
	int mNum, mLen;
	const int mInc;

private:

	iArray(const iArray<T>&); // not implemented
	void operator=(const iArray<T>&); // not implemented
};


//
//  iLookupArray class
//  Needs operator == for type T
//
template<class T>
class iLookupArray : public iArray<T>
{

public:

	iLookupArray(int inc = 10);

	virtual int Find(const T &val) const;
	bool AddUnique(const T &val);
	bool Remove(const T &val);

private:

};


//
//  iGenericOrderedArray class
//  Needs operator < for the key type K
//
template<class T, class K>
class iGenericOrderedArray : public iLookupArray<T>
{

public:

	iGenericOrderedArray(const K& (*key)(const T &val), int inc = 10);

	//
	//  In case of duplicate keys, add the new value last
	//
	virtual void Add(const T &val);
	virtual int Find(const T &val) const;
	//
	//  In case of duplicate keys, return the first value
	//
	int FindByKey(const K &key) const;

private:

	const K& (*Key)(const T &val);

	iGenericOrderedArray(const iGenericOrderedArray<T,K>&); // not implemented
	void operator=(const iGenericOrderedArray<T,K>&); // not implemented
};


//
//  iOrderedArray class (order by its own value)
//
template<class T>
class iOrderedArray : public iGenericOrderedArray<T,T>
{

public:

	iOrderedArray(int inc = 10) : iGenericOrderedArray<T,T>(Self,inc){}

private:

	static const T& Self(const T &val){ return val; }

	iOrderedArray(const iOrderedArray<T>&); // not implemented
	void operator=(const iOrderedArray<T>&); // not implemented
};

#endif // IARRAY_H
