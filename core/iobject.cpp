/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iobject.h"


#include "ierror.h"
#include "ihelpfactory.h"
#include "iproperty.h"

//
//  Templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


using namespace iParameter;


//
//  *****************************************************************
//
// iObject implementation
//
//  *****************************************************************
//
const iString& iObject::ChildLookup(iObject* const &ptr)
{
	return ptr->Tag();
}


const iString& iObject::VariableLookup(iPropertyVariable* const &ptr)
{
	 return ptr->Tag();
}


const iString& iObject::FunctionLookup(iPropertyFunction* const &ptr)
{
	 return ptr->Tag();
}

	
iObject::iObject(iObject *parent, const iString &fname, const iString &sname, int level) :
	mChildren(iObject::ChildLookup),
	mVariableProperties(iObject::VariableLookup),
	mFunctionProperties(iObject::FunctionLookup),
	mParent(parent), 
	mLongName(fname), 
	mShortName(sname),
	mTag(iString::FromNumber(level,"%1d")+sname)
{
	if(mParent != 0)
	{
		mParent->mChildren.Add(this);
	}

	mRenderMode = -1;  // unset
}


iObject::~iObject()
{
	if(mParent != 0) mParent->mChildren.Remove(this);
	//
	//  Delete children in reverse order in case they create static instances of reference-counted variables
	//
	while(mChildren.Size() > 0) mChildren.RemoveLast()->Delete();
}


const iString iObject::HelpTag() const
{
	return this->ShortName();
}


int iObject::ParentIndex() const
{
	if(mParent==0 || mParent->ShortName()!=this->ShortName())
	{
		return -1;
	}
	else
	{
		//
		//  Parent is our collection
		//
		iObject *obj = const_cast<iObject*>(this); IASSERT(obj);
		return mParent->mChildren.Find(obj);
	}
}


void iObject::RequestRender()
{
	switch(mRenderMode)
	{
	case -1:
		{
			IBUG_FATAL("RenderMode for object "+this->LongName()+" was not set.");
			break;
		}
	case RenderMode::NoRender:
		{
			break;
		}
	default:
		{
			this->PassRenderRequest(mRenderMode);
		}
	}
}


bool iObject::IsImmediatePost() const
{
	if(mParent != 0)
	{
		return mParent->IsImmediatePost();
	}
	else return true;
}


void iObject::PassRenderRequest(int mode)
{
	if(mParent != 0)
	{
		if(mode < mRenderMode) mode = mRenderMode;
		mParent->PassRenderRequest(mode);
	}
}


bool iObject::CopyState(const iObject *p)
{
	if(this == p) return true;

#ifdef I_DEBUG
	iOutput::DisplayDebugMessage("Copying object: "+this->LongName());
#endif

	if(this->LongName() != p->LongName())
	{
		return false;
	}
	if(mVariableProperties.Size() != p->mVariableProperties.Size())
	{
		return false;
	}

	int i;
	for(i=0; i<mVariableProperties.Size(); i++) if(!mVariableProperties[i]->TestFlag(iPropertyVariable::_DoNotIncludeInCopy))
	{
		if(!mVariableProperties[i]->Copy(p->mVariableProperties[i]))
		{
#ifdef I_DEBUG
			mVariableProperties[i]->Copy(p->mVariableProperties[i]);
#endif
			return false;
		}
	}

	for(i=0; i<mChildren.Size(); i++)
	{
		if(!mChildren[i]->CopyState(p->mChildren[i]))
		{
			return false;
		}
	}

	return true;
}


const iObject* iObject::FindByName(const iString &name) const
{
	if(name==mShortName || name==mLongName) return this;

	iString cname(name);
	if(name.BeginsWith(mShortName+".") || name.BeginsWith(mLongName+"."))
	{
		cname = name.Section(".",1); // one of our children
	}

	int i;
	const iObject *obj;
	for(i=0; i<mChildren.Size(); i++)
	{
		obj = mChildren[i]->FindByName(cname);
		if(obj != 0)
		{
			return obj;
		}
	}

	return 0;
}


const iProperty* iObject::FindPropertyByName(const iString &name) const
{
	int idx = name.FindLast('.');
	if(idx == -1) return 0; // not a property name

	const iObject *obj = this->FindByName(name.Part(0,idx));
	if(obj == 0) return 0;

	return obj->GetLocalProperty(name.Part(idx+1));
}


const iProperty* iObject::GetLocalProperty(const iString &name) const
{
#ifndef I_NO_CHECK
	if(name.Contains(".") > 0)
	{
		IBUG_ERROR("Incorrect call to iObject::GetLocalProperty.");
		return 0;
	}
#endif

	//
	//  Could be our local property or not a property name at all
	//
	int i;
	for(i=0; i<mVariableProperties.Size(); i++)
	{
		if(mVariableProperties[i]->ShortName()==name || mVariableProperties[i]->LongName()==name)
		{
			return mVariableProperties[i];
		}
	}

	for(i=0; i<mFunctionProperties.Size(); i++)
	{
		if(mFunctionProperties[i]->ShortName()==name || mFunctionProperties[i]->LongName()==name)
		{
			return mFunctionProperties[i];
		}
	}

	return 0;
}


void iObject::WriteHead(iString &s) const
{
	s = "Object: " + this->LongName() + "/" + this->ShortName() + "\n";
}


void iObject::WriteState(iString &s) const
{
	int i, j;
	iString s1;

	s.Init(1000);

	this->WriteHead(s);

	//
	//  Write alphabetically
	//
	if(mVariableProperties.Size() > 0)
	{
		s += "Variables:\n";
		for(i=0; i<mVariableProperties.Size(); i++)
		{	
			s1 = mVariableProperties[i]->GetTypeName();
			if(!mVariableProperties[i]->IsFixedSize()) s1 += "[*]"; else if(mVariableProperties[i]->Size() > 1) s1 += "[" + iString::FromNumber(mVariableProperties[i]->Size()) + "]";
			while(s1.Length() < 12) s1 += " ";
			s1 += mVariableProperties[i]->LongName() + "/" + mVariableProperties[i]->ShortName();
			while(s1.Length() < 32) s1 += " ";

			s1 += "\t = ";
			iString s2;
			mVariableProperties[i]->SaveStateToString(s2,false);
			s2 = s2.Section(" = ",1);
			if(mVariableProperties[i]->Size() > 1) s2 = "(" + s2 + ")";
			s1 += s2 + "\n";

			s += "    " + s1;
		}
	}

	if(mFunctionProperties.Size() > 0)
	{
		s += "Functions:\n";
		for(i=0; i<mFunctionProperties.Size(); i++)
		{
			s1 = mFunctionProperties[i]->GetReturnTypeName();
			while(s1.Length() < 12) s1 += " ";
			s1 += mFunctionProperties[i]->LongName() + "/" + mFunctionProperties[i]->ShortName() + "(";
			for(j=0; j<mFunctionProperties[i]->NumArguments(); j++)
			{
				s1 += " " + mFunctionProperties[i]->GetArgumentTypeName(j);
				if(j < mFunctionProperties[i]->NumArguments()-1) s1 += " ,";
			}
			s1 += " )\n";
			s += "    " + s1;
		}
	}
}


bool iObject::IsHidden() const
{
	return (mShortName[0] == '@');
}


void iObject::RegisterWithHelpFactory() const
{
	if(this->IsHidden()) return;

	iString s = this->HelpTag() + "+" + this->LongName() + "/" + this->ShortName() + "+";
	if(mParent != 0)
	{
		s += mParent->HelpTag();
	}
	s += "+";

	int i;
	for(i=0; i<mChildren.Size(); i++)
	{
		if(i > 0) s += ",";
		s += mChildren[i]->HelpTag();
	}

	iHelpFactory::RegisterObject(s);

	this->RegisterPropertiesWithHelpFactory();

	for(i=0; i<mChildren.Size(); i++)
	{
		mChildren[i]->RegisterWithHelpFactory();
	}
}


void iObject::RegisterPropertiesWithHelpFactory() const
{
	int i, j;
	iString s;

	for(i=0; i<mVariableProperties.Size(); i++) if(!mVariableProperties[i]->ShortName().IsEmpty())
	{
		s = mVariableProperties[i]->HelpTag() + "+" + mVariableProperties[i]->LongName() + "/" + mVariableProperties[i]->ShortName() + "+";
		s += mVariableProperties[i]->GetTypeName() + "+";
		if(!mVariableProperties[i]->IsFixedSize()) s += "[*]"; else if(mVariableProperties[i]->Size() > 1) s += "[" + iString::FromNumber(mVariableProperties[i]->Size()) + "]";
		iHelpFactory::RegisterObjectProperty(s);
	}

	for(i=0; i<mFunctionProperties.Size(); i++) if(!mFunctionProperties[i]->ShortName().IsEmpty())
	{
		s = mFunctionProperties[i]->HelpTag() + "+" + mFunctionProperties[i]->LongName() + "/" + mFunctionProperties[i]->ShortName() + "+";
		s += mFunctionProperties[i]->GetReturnTypeName() + "+(";
		for(j=0; j<mFunctionProperties[i]->NumArguments(); j++)
		{
			s += " " + mFunctionProperties[i]->GetArgumentTypeName(j);
			if(j < mFunctionProperties[i]->NumArguments()-1) s += " ,";
		}
		s += " )";
		iHelpFactory::RegisterObjectProperty(s);
	}
}


void iObject::SaveStateToString(iString &s) const
{
	s.Init(1000);
	s = this->GetFullName() + "#+++\n";

	int i;
	iString s1;
	this->SavePropertiesToString(s1);
	s += s1 + this->GetFullName() + "#***\n";

	for(i=0; i<mChildren.Size(); i++)
	{
		mChildren[i]->SaveStateToString(s1);
		s += s1;
	}

	s += this->GetFullName() + "#---\n";
}


void iObject::SavePropertiesToString(iString &s) const
{
	int i;
	iString s1;

	s.Clear();
	for(i=0; i<mVariableProperties.Size(); i++) if(!mVariableProperties[i]->TestFlag(iPropertyVariable::_DoNotSaveInState))
	{
		mVariableProperties[i]->SaveStateToString(s1,true);
		s += s1 + "\n";
	}
}


bool iObject::LoadStateFromString(const iString &str)
{
	iString head = this->GetFullName() + "#+++\n";
	iString s = str;

	int ib = str.Find(head);
	int ie = str.Find(this->GetFullName()+"#***\n");
	if(ib>-1 && ie>-1)
	{
		ib += head.Length();
		s = str.Part(ib,ie-ib);
		ib = ie + head.Length();
	}

	int i;
	bool ok = true;
	for(i=0; i<mVariableProperties.Size(); i++) if(!mVariableProperties[i]->TestFlag(iPropertyVariable::_DoNotSaveInState))
	{
		if(!mVariableProperties[i]->LoadStateFromString(s) && !mVariableProperties[i]->TestFlag(iProperty::_OptionalInState))
		{
			ok = false;
#ifdef I_DEBUG
			mVariableProperties[i]->LoadStateFromString(s);
#endif
		}
		this->OnLoadStateAtom();
	}

	ie = str.Find(this->GetFullName()+"#---\n");
	if(ib>-1 && ie>-1)
	{
		s = str.Part(ib,ie-ib);
	}

	for(i=0; i<mChildren.Size(); i++)
	{
		if(!mChildren[i]->LoadStateFromString(s))
		{
			ok = false;
#ifdef I_DEBUG
			mChildren[i]->LoadStateFromString(s);
#endif
		}
	}

	return ok;
}


const iString iObject::GetFullName() const
{
	iString str;

	if(mParent != 0)
	{
		str += mParent->GetFullName();
		if(mParent->LongName() != this->LongName()) str += "." + this->LongName();
	}
	else
	{
		str += this->LongName();
	}

	if(this->ParentIndex() >= 0)
	{
		str += "[" + iString::FromNumber(this->ParentIndex()) + "]";
	}

	return str;
}


void iObject::OnLoadStateAtom()
{
	if(mParent != 0) mParent->OnLoadStateAtom();
}


//
//  This is a part of message subsystem for thread-safe execution.
//  Functions begining with underscore can be called from the driver thread.
//  Functions ending with underscore should be called from the slave thread.
//
bool iObject::_RequestPush(iProperty::Key key)
{
	IASSERT(mParent); // root object overwrites this
	return mParent->_RequestPush(key);
}

