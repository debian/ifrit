/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ipiecewisefunction.h"

#include "ierror.h"
#include "imath.h"

#include <vtkPiecewiseFunction.h>

//
//  Templates
//
#include "iarray.tlh"


class iPiecewiseFunctionHelper : public vtkPiecewiseFunction
{

public:

	iPiecewiseFunctionHelper(iPiecewiseFunction *p)
	{
		mParent = p;
	}

	virtual void Update()
	{
		int i;

		this->RemoveAllPoints();
		for(i=0; i<mParent->mArray.Size(); i++) this->AddPoint(255*mParent->mArray[i].X,mParent->mArray[i].Y);
	}

	void ParentChanged()
	{
		this->Update();
		mParent->Modified();
	}

private:

	iPiecewiseFunction *mParent;
};


//
// iPiecewiseFunction class
//
iPiecewiseFunction* iPiecewiseFunction::New(float ymin, float ymax)
{
	iPiecewiseFunction *tmp = new iPiecewiseFunction(ymin,ymax,10); IERROR_CHECK_MEMORY(tmp);
	return tmp;
}


iPiecewiseFunction::iPiecewiseFunction(float ymin, float ymax, int inc) : mArray(inc)
{
	mYmin = ymin;
	mYmax = ymax;
	mArray.Add(iPair(0.0,ymin));
	mArray.Add(iPair(1.0,ymax));
	
	mHelper = new iPiecewiseFunctionHelper(this); IERROR_CHECK_MEMORY(mHelper);
	mHelper->ParentChanged();
}


iPiecewiseFunction::~iPiecewiseFunction()
{
	mHelper->Delete();
}


vtkPiecewiseFunction* iPiecewiseFunction::GetVTKFunction() const
{
	return mHelper;
}


void iPiecewiseFunction::Copy(const iPiecewiseFunction *f)
{
	if(this==f || f==0) return;

	mArray.Clear();
	int i;
	for(i=0; i<f->N(); i++)
	{
		mArray.Add(f->mArray[i]);
	}
	mYmin = f->mYmin;
	mYmax = f->mYmax;

	mHelper->ParentChanged();
}


void iPiecewiseFunction::SetMinMax(float ymin, float ymax)
{
	mYmin = ymin;
	mYmax = ymax;
	for(int i=0; i<mArray.Size(); i++) 
	{
		mArray[i].Y = (mArray[i].Y > mYmax) ? mYmax : mArray[i].Y;
		mArray[i].Y = (mArray[i].Y < mYmin) ? mYmin : mArray[i].Y;
	}

	mHelper->ParentChanged();
}


void iPiecewiseFunction::AddPoint(int n) 
{
	n++;
	if(n < 1) n = 1;
	if(n > mArray.MaxIndex()) n = mArray.MaxIndex();

	mArray.Add(iPair(0.5*(mArray[n-1].X+mArray[n].X),0.5*(mArray[n-1].Y+mArray[n].Y)));
	mHelper->ParentChanged();
}


void iPiecewiseFunction::AddPoint(const iPair &p) 
{
	float x = p.X;
	if(x < 0.0) x = 0.0;
	if(x > 1.0) x = 1.0;
	float y = p.Y;
	if(y < mYmin) y = mYmin;
	if(y > mYmax) y = mYmax;
	mArray.Add(iPair(x,y));
	mHelper->ParentChanged();
}


void iPiecewiseFunction::RemovePoint(int n) 
{
	if(n>0 && n<mArray.MaxIndex() && mArray.Size()>2) 
	{
		mArray.iArray<iOrderedPair>::Remove(n);
		mHelper->ParentChanged();
	}
}


void iPiecewiseFunction::MovePoint(int n, const iPair &p) 
{
	if(n>0 && n<mArray.MaxIndex()) 
	{
		float x = p.X;
		if(x < mArray[n-1].X) x = mArray[n-1].X;
		if(x > mArray[n+1].X) x = mArray[n+1].X;
		float y = p.Y;
		if(y < mYmin) y = mYmin;
		if(y > mYmax) y = mYmax;
		mArray[n] = iPair(x,y);
	} 
	else if(n==0 || n==mArray.MaxIndex()) 
	{
		float y = p.Y;
		if(y < mYmin) y = mYmin;
		if(y > mYmax) y = mYmax;
		mArray[n].Y = y;
	}

	mHelper->ParentChanged();
}


int iPiecewiseFunction::FindPoint(const iPair &p, float &dmin) const
{
	int i, imin;
	float d, dx, dy;
	
	dmin = iMath::_LargeFloat;
	imin = -1;
	for(i=0; i<mArray.Size(); i++) 
	{
		dx = p.X - mArray[i].X;
		dy = p.Y - mArray[i].Y;
		d = dx*dx + dy*dy;
		if(d < dmin) 
		{
			dmin = d;
			imin = i;
		}
	}
	
	dmin = sqrt(dmin);
	return imin;
}


float iPiecewiseFunction::GetValue(float x) const
{
	int i;
	
	if(x < mArray[0].X) return mArray[0].Y;
	if(x > mArray[mArray.MaxIndex()].X) return mArray[mArray.MaxIndex()].Y;

	for(i=1; i<mArray.MaxIndex() && mArray[i].X<x; i++);
	return mArray[i].Y + (mArray[i].Y-mArray[i-1].Y)/(mArray[i].X-mArray[i-1].X+1.0e-30)*(x-mArray[i].X);
}


void iPiecewiseFunction::Reset()
{
	mArray.Clear();
	mArray.Add(iPair(0.0,mYmin));
	mArray.Add(iPair(1.0,mYmax));

	mHelper->ParentChanged();
}

