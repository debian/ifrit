/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "itensorfieldglyphpipeline.h"


#include "idatalimits.h"
#include "ierror.h"
#include "ipointglyph.h"
#include "ireplicatedpolydata.h"
#include "iresampleimagedatafilter.h"
#include "itensorglyphfilter.h"
#include "itensorfieldviewsubject.h"

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"
#include "igenericfilter.tlh"
#include "iviewsubjectpipeline.tlh"


iPipelineKeyDefineMacro(iTensorFieldGlyphPipeline,Scaling);
iPipelineKeyDefineMacro(iTensorFieldGlyphPipeline,Painting);
iPipelineKeyDefineMacro(iTensorFieldGlyphPipeline,GlyphType);


//
//  iTensorFieldGlyphPipeline class
//
iTensorFieldGlyphPipeline::iTensorFieldGlyphPipeline(iTensorFieldViewInstance *owner) : iFieldGlyphPipeline(owner)
{
	mOwner = owner;

	//
	//  Do VTK stuff
	//	
	mGlyph = iPointGlyph::New(); IERROR_CHECK_MEMORY(mGlyph);

	mGlyphFilter = this->CreateFilter<iTensorGlyphFilter>();
	mGlyph->SetType(iParameter::PointGlyphType::Cube);

	mGlyphFilter->SetInputConnection(mResampleFilter->GetOutputPort());
	mGlyphFilter->SetGlyphConnection(mGlyph->GetOutputPort());

	this->CompletePipeline(mGlyphFilter->GetOutputPort());
	this->UpdateContents();
}


iTensorFieldGlyphPipeline::~iTensorFieldGlyphPipeline()
{
	mGlyph->Delete();
}


void iTensorFieldGlyphPipeline::SetGlyphFilterInput(vtkDataSet *input)
{
	if(mGlyphFilter->InputData() != input)
	{
		mGlyphFilter->SetInputData(input);
	}
}


void iTensorFieldGlyphPipeline::SetGlyphFilterScale(double s)
{
	mGlyphFilter->SetScaleFactor(s);
}


void iTensorFieldGlyphPipeline::UpdateGlyphType()
{
	switch(mOwner->GetGlyphType())
	{
	case 1:
	{
		mGlyph->SetType(iParameter::PointGlyphType::Sphere);
		mGlyphFilter->Modified();
		break;
	}
	default:
	{
		mGlyph->SetType(iParameter::PointGlyphType::Cube);
		mGlyphFilter->Modified();
	}
	};
	this->Modified();
}


void iTensorFieldGlyphPipeline::UpdateScaling()
{
	mGlyphFilter->SetScaling(mOwner->GetScaling());
	this->Modified();
}


void iTensorFieldGlyphPipeline::UpdatePainting()
{
	mGlyphFilter->SetCurrentVar(mOwner->GetPaintVar());
	this->Modified();
}

