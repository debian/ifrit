/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef ICAPTION_H
#define ICAPTION_H


#include "iframedtextactor.h"


class iActor;

class vtkCoordinate;
class vtkPolyData;


class iCaption: public iFramedTextActor
{
	
public:
	
	vtkTypeMacro(iCaption,iFramedTextActor);
	static iCaption* New(iRenderTool *rt = 0);
	
	void SetAttachmentPoint(double x[3]);
	const double* GetAttachmentPoint();

	static bool Transparent;

protected:
	
	virtual ~iCaption();
	
	virtual void UpdateGeometry(vtkViewport *vp);

private:
	
	iCaption(iRenderTool *rv);

	vtkCoordinate *mAttachmentPoint;

	float mUnmagx1[2], mUnmagx2[2];

	//
	//  Leader
	//
	vtkPolyData *mLeaderInput;
	iActor      *mLeaderActor;
};

#endif // ICAPTION_H
