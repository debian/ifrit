/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ioverlayhelper.h"


#include "ierror.h"
#include "imagnifier.h"
#include "imath.h"
#include "irendertool.h"

#include <vtkCamera.h>
#include <vtkProperty2D.h>
#include <vtkRenderer.h>
#include <vtkTextProperty.h>


using namespace iParameter;


iOverlayHelper* iOverlayHelper::New(iRenderTool *rt)
{
	IASSERT(rt);
	return new iOverlayHelper(rt);
}


iOverlayHelper::iOverlayHelper(iRenderTool *rt) : mRenderTool(rt)
{
	mIsAutoColor = true;
	mFontFactor = 1.0f;
}


iOverlayHelper::~iOverlayHelper()
{
}


const iColor& iOverlayHelper::GetColor(vtkViewport* viewport)
{
	if(mIsAutoColor && viewport!=0)
	{
		int i;
		double bg[3];
		viewport->GetBackground(bg);
		for(i=0; i<3; i++) bg[i] = 1.0 - bg[i];
		wColor = iColor(bg);
	}
	else
	{
		wColor = mFixedColor;
	}
	return wColor;
}


int iOverlayHelper::GetRenderingMagnification() const
{
	return mRenderTool->GetRenderingMagnification();
}


void iOverlayHelper::ComputePositionShiftsUnderMagnification(int winij[2])
{
	int mag = mRenderTool->GetRenderingMagnification();
	//
	//  Compute position shifts if under magnification
	//
	if(mag > 1)
	{
		mRenderTool->GetMagnifier()->GetTile(winij[0],winij[1]);
	}
	else winij[0] = winij[1] = 0;
}


vtkCamera* iOverlayHelper::GetCamera(vtkViewport *vp) const
{
	vtkRenderer *ren = vtkRenderer::SafeDownCast(vp);
	if(ren == 0) return 0; else return ren->GetActiveCamera();
}


int iOverlayHelper::GetFontSize(vtkViewport *vp, float factor) const
{
	float v = factor*16*mFontFactor*exp(0.3*mRenderTool->GetFontScale())/480;
	vtkRenderer *ren = vtkRenderer::SafeDownCast(vp);
	if(ren != 0)
	{
		if(ren->GetActiveCamera()->GetUseHorizontalViewAngle() != 0)
		{
			return iMath::Round2Int(v*vp->GetSize()[0]);
		}
		else
		{
			return iMath::Round2Int(v*vp->GetSize()[1]);
		}
	}
	else return iMath::Round2Int(v*480);
}


void iOverlayHelper::UpdateTextProperty(vtkViewport *vp, vtkTextProperty *prop)
{
#ifndef I_NO_CHECK
	int mag = mRenderTool->GetRenderingMagnification();
	if(mag > 1)
	{
		IBUG_WARN("Extra call to iOverlayHelper::UpdateTextProperty");
	}
#endif

	if(prop != 0)
	{
		switch(mRenderTool->GetFontType())
		{
		case TextType::Arial:
			{
				prop->SetFontFamilyToArial();
				mFontFactor = 1.0;
				break;
			}
		case TextType::Courier:
			{
				prop->SetFontFamilyToCourier();
				mFontFactor = 1.2;
				break;
			}
		case TextType::Times:
			{
				prop->SetFontFamilyToTimes();
				mFontFactor = 1.0;
				break;
			}
		}
		prop->ItalicOff();
		prop->ShadowOn();
		prop->SetColor(this->GetColor(vp).ToVTK());
	}
}

