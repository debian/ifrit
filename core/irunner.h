/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IRUNNER_H
#define IRUNNER_H


class iShell;
class iString;

class vtkMultiThreader;


//
//  Interface for starting the code in various ways
//
class iRunner
{

	friend class iShell;

public:

	iRunner();
	virtual ~iRunner();

	inline iShell* GetShell() const { return mShell; }

	//
	//  The main driver that creates, runs, and deletes a shell in one call
	//  Event loop is implemented via iScript interface
	//
	void Run(int argc, const char **argv);

	//
	//  For external driving (launches but does not run an event loop in the extended mode).
	//  Launched shell needs to be stopped explicitly for thread safety.
	//
	bool Launch(int argc, const char **argv);
	void Stop();

private:

	iShell *mShell;
	vtkMultiThreader *mThreader;
	int mThreadId;
};

#endif  // IRUNNER_H
