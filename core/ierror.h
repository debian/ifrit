/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  Some useful error reporting macros and function(s)
//

#ifndef IERROR_H
#define IERROR_H


#include "ioutput.h"


#define IBUG_WARN(_m_)				{ iOutput::ReportBug(_m_,__FILE__,__LINE__,2); }
#define IBUG_ERROR(_m_)				{ iOutput::ReportBug(_m_,__FILE__,__LINE__,1); }
#define IBUG_FATAL(_m_)				{ iOutput::ReportBug(_m_,__FILE__,__LINE__,0); }
#define IASSERT(_p_)				{ if((_p_) == 0) IBUG_FATAL("Invalid use of a null pointer."); }
#define IERROR_CHECK_MEMORY(_p_)	{ if((_p_) == 0) iOutput::ReportBug("There is not enough memory to proceed.",__FILE__,__LINE__,-1); }

//
//  error-reporting dynamic_cast - make it a global function to reduce de-referencing
//  (ideally we would avoid using it all - use iRequiredCast(INFO,vtkObject*) function whenever possible).
//
template<class O, class I>
inline O* iDynamicCast(int line, const char *file, I* p)
{
	if(p == 0) return 0;
	O* tmp = dynamic_cast<O*>(p);
	if(tmp == 0) iOutput::ReportBug("Invalid cast.",file,line,0);
	return tmp;
}

//
//  RequiredCast for children of vtkObjectBase (which must be all cast-able objects)
//
class vtkObjectBase;
template<class O>
inline O* iRequiredCast(int line, const char *file, vtkObjectBase *p)
{
	if(p == 0) return 0;
	O* tmp = O::SafeDownCast(p);
	if(tmp == 0) iOutput::ReportBug("Invalid cast.",file,line,0);
	return tmp;
}

#define INFO __LINE__,__FILE__

#endif  // IERROR_H

