/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "irendertoolbackground.h"


#include "ierror.h"
#include "iimage.h"
#include "imath.h"
#include "ivtk.h"

#include <vtkActor2D.h>
#include <vtkImageMapper.h>
#include <vtkProperty2D.h>
#include <vtkRenderer.h>


class iRenderToolBackgroundImageMapper : public vtkImageMapper
{

public:

	static iRenderToolBackgroundImageMapper* New()
	{
		return new iRenderToolBackgroundImageMapper;
	}

	virtual void RenderData(vtkViewport *, vtkImageData *, vtkActor2D *)
	{
	}

	virtual void RenderOverlay(vtkViewport *viewport, vtkActor2D *actor)
	{
		int *size = viewport->GetSize();
		
		if(this->GetMTime()>mBuildTime || size[0]!=mOldSize[0] || size[1]!=mOldSize[1])
		{
			mOldSize[0] = size[0];
			mOldSize[1] = size[1];

			mImageTile = mImageFull;;

			if(mTile[0]>0.0 || mTile[2]<1.0 || mTile[1]>0.0 || mTile[3]<1.0)
			{
				int fw = mImageFull.Width();
				int fh = mImageFull.Height();
				
				if(mFixedAspect)
				{
					float sx = float(fw)/size[0];
					float sy = float(fh)/size[1];

					if(sx > sy)
					{
						fw = iMath::Round2Int(sy*size[0]);
					}
					else
					{
						fh = iMath::Round2Int(sx*size[1]);
					}
				}
				
				int x = iMath::Round2Int(mTile[0]*fw); 
				int w = iMath::Round2Int(mTile[2]*fw) - x;
				int y = iMath::Round2Int(mTile[1]*fh);
				int h = iMath::Round2Int(mTile[3]*fh) - y;

				mImageTile.Crop(x,y,w,h);
			}

			mImageTile.Scale(size[0],size[1]);

			mWorker->SetInputData(mImageTile.DataObject());
			mBuildTime.Modified();
		}

		mWorker->RenderOverlay(viewport,actor);
	}

	void SetImageFixedAspect(bool s)
	{
		if(mFixedAspect != s)
		{
			mFixedAspect = s;
			this->Modified();
		}
	}

	bool GetImageFixedAspect()
	{
		return mFixedAspect;
	}

	void SetImageTile(const float tile[4])
	{
		if(tile[0]!=mTile[0] || tile[1]!=mTile[1] || tile[2]!=mTile[2] || tile[3]!=mTile[3])
		{
			int j;
			for(j=0; j<4; j++)
			{
				mTile[j] = tile[j];
			}
			this->Modified();
		}
	}

	void SetImage(const iImage &im)
	{
		mImageFull = im;
		this->Modified();
	}

protected:

	iRenderToolBackgroundImageMapper()
	{
		mFixedAspect = false;
		mTile[0] = mTile[1] = 0.0;
		mTile[2] = mTile[3] = 1.0;
		mOldSize[0] = mOldSize[1] = 0;

		mWorker = vtkImageMapper::New(); IERROR_CHECK_MEMORY(mWorker);
		mWorker->SetColorWindow(255.0);
		mWorker->SetColorLevel(127.5);
		mWorker->RenderToRectangleOff();
		mWorker->UseCustomExtentsOff();
	}

	~iRenderToolBackgroundImageMapper()
	{
		mWorker->Delete();
	}

	bool mFixedAspect;
	float mTile[2], mMinExtent[2], mMaxExtent[2];
	int mOldSize[2];

	iImage mImageFull, mImageTile;
	vtkImageMapper *mWorker;
	vtkTimeStamp mBuildTime;
};


//
//  Main class
//
iRenderToolBackground* iRenderToolBackground::New()
{
	return new iRenderToolBackground;
}


iRenderToolBackground::iRenderToolBackground()
{
	mRenderer = vtkRenderer::New(); IERROR_CHECK_MEMORY(mRenderer);

	mActor = vtkActor2D::New(); IERROR_CHECK_MEMORY(mActor);
	mMapper = iRenderToolBackgroundImageMapper::New(); IERROR_CHECK_MEMORY(mMapper);

	mActor->SetMapper(mMapper);
	mMapper->Delete();
	mActor->GetPositionCoordinate()->SetCoordinateSystemToNormalizedViewport();
	mActor->SetPosition(0.0,0.0);
	mActor->GetPosition2Coordinate()->SetCoordinateSystemToNormalizedViewport();
	mActor->SetPosition2(1.0,1.0);
	mActor->VisibilityOff();
	mActor->PickableOff();

	mActor->GetProperty()->SetDisplayLocationToBackground();

	mRenderer->AddActor2D(mActor);
	mRenderer->SetLayer(IRENDERTOOLBACKGROUND_BACK_LAYER);
	mRenderer->SetInteractive(0);
}


iRenderToolBackground::~iRenderToolBackground()
{
	mRenderer->RemoveActor2D(mActor);
	mRenderer->Delete();

	mActor->Delete();
}


void iRenderToolBackground::SetColor(const iColor &color)
{
	if(color != mColor)
	{
		mColor = color;
		mRenderer->SetBackground(color.ToVTK());
	}
}


void iRenderToolBackground::SetImage(const iImage &image)
{
	if(image != mImage)
	{
		if(image.IsEmpty())
		{
			mImage.Clear();
			mActor->VisibilityOff();
		}
		else
		{
			mImage = image;
			mMapper->SetImage(mImage);
			mActor->VisibilityOn();
		}
	}
}


void iRenderToolBackground::SetImageFixedAspect(bool s)
{
	mMapper->SetImageFixedAspect(s);
}


void iRenderToolBackground::SetImageTile(const float tile[4])
{
	mMapper->SetImageTile(tile);
}


void iRenderToolBackground::Copy(const iRenderToolBackground *source)
{
	if(source != 0)
	{
		this->SetColor(source->mColor);
		this->SetImage(source->mImage);
		mMapper->SetImageFixedAspect(source->mMapper->GetImageFixedAspect());
	}
}

