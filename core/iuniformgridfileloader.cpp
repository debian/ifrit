/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iuniformgridfileloader.h"


#include "idataconsumer.h"
#include "idatasubject.h"
#include "ifile.h"
#include "imonitor.h"
#include "ioutputchannel.h"
#include "iparallel.h"
#include "iparallelmanager.h"
#include "iparallelworker.h"
#include "ishell.h"
#include "ishelleventobservers.h"
#include "isystem.h"
#include "iuniformgriddata.h"
#include "iviewmodule.h"
#include "ivtk.h"

#include <vtkFloatArray.h>
#include <vtkInformation.h>
#include <vtkPointData.h>

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"
#include "ibuffer.tlh"


using namespace iParameter;


//
//  Helper class for parallel executions
//
class iUniformGridHelper : protected iParallelWorker
{

public:

	iUniformGridHelper(iUniformGridFileLoader *loader);

	void ShiftData(int numCom, int dim, int dn, int dataDims[3], float *data, float *buf);
	void ExpandUp(float *array, int nc, int oldDims[3], int newDims[3]);
	void CollapseDown(float *array, int nc, int oldDims[3], int newDims[3]);
	void PadPeriodically(float *array, int nc, int oldDims[3], int newDims[3]);

protected:

	virtual int ExecuteStep(int step, iParallel::ProcessorInfo &p);

	void ExecuteShiftData(iParallel::ProcessorInfo &p);
	void ExecutePadPeriodically(iParallel::ProcessorInfo &p);

	iUniformGridFileLoader *mLoader;

	int mNumCom;
	int *mDims;
	float *mArr1, *mArr2;

	float mProgStart, mProgStep;

	int mItmp1, mItmp2;
};


//
//  Main class
//
iUniformGridFileLoader::iUniformGridFileLoader(iDataReader *r, int priority, bool usePeriodicOffsets) : iFileLoader(r,priority), mMaxDimension(1670000), mBufferSizeOffset(1), mUsePeriodicOffsets(usePeriodicOffsets)
{
	mNumVars = mNumComponents = 0;

	mOriginOffset[0] = mOriginOffset[1] = mOriginOffset[2] = 0.0;

	mVoxelLocation = VoxelLocation::Vertex;

	mFileDims[0] = mFileDims[1] = mFileDims[2] = 0;
	mDataDims[0] = mDataDims[1] = mDataDims[2] = 0;

	mBufferSize = 0;
	mBuffer = 0;

	mScaledDim = -1;
	mMaxZDim = 0;

	mHelper = new iUniformGridHelper(this); IERROR_CHECK_MEMORY(mHelper);
}


iUniformGridFileLoader::~iUniformGridFileLoader()
{
	delete mHelper;
}


void iUniformGridFileLoader::LoadFileBody(const iString &suffix, const iString &fname)
{
	//
	//  is the suffix valid?
	//
	iMonitor h(this);
	h.SetInterval(0.0,0.9);

	if(suffix.Lower()!="txt" && suffix.Lower()!="bin")
	{
		h.PostError("Incorrect file suffix.");
		return;
	}

	mNumVars = mNumComponents = 0;

	int i;
	for(i=0; i<3; i++) mFileDims[i] = mDataDims[i] = 0;

	//
	//  Read the data
	//
	if(suffix.Lower() == "txt") this->ReadTxtFile(fname); else this->ReadBinFile(fname);
	if(h.IsStopped()) return;

	h.SetInterval("Formatting",0.9,0.1);

	//
	//  Specify data dimensions
	//
	for(i=0; i<3; i++) mDataDims[i] = mFileDims[i];

	//
	//  Set periodicities
	//
	int nmax = mFileDims[0];
	for(i=1; i<3; i++)
	{
		if(nmax < mFileDims[i]) 
		{
			nmax = mFileDims[i];
		}
	}
	for(i=0; i<3; i++) this->SetDirectionPeriodic(i,mFileDims[i]==nmax);
	
	//
	//  Implement periodic BC
	//
	if(mUsePeriodicOffsets && mVoxelLocation==VoxelLocation::Center && this->GetBoundaryConditions()==BoundaryConditions::Periodic)
	{
		//
		//  Expand the box and add extra values.
		//
		for(i=0; i<3; i++) if(this->IsDirectionPeriodic(i)) mDataDims[i]++;
		mHelper->ExpandUp(mBuffer,mNumComponents,mFileDims,mDataDims);
		mHelper->PadPeriodically(mBuffer,mNumComponents,mFileDims,mDataDims);
	}

	this->ReleaseData();
}


void iUniformGridFileLoader::ReadBinFile(const iString &fname)
{
	int i;
	float *d;
	iFile F(fname);
	bool err;

	iMonitor h(this);
	if(!F.Open(iFile::_Read,iFile::_Binary))
	{
		h.PostError("File "+fname+" does not exist.");
		return;
	}

	//
	// auto-detect data endiness
	//
	if(!this->DetectFortranFileStructure(F,3*sizeof(int)))
	{ 
		F.Close(); 
		h.PostError("Corrupted header.");
		return;
	}

	//
	//  Read the header
	//
	err = false;
	int ndim[3];
	if(!this->ReadFortranRecord(F,Buffer(ndim,3))) err = true;

    if(ndim[0]<1 || ndim[0]>mMaxDimension || ndim[1]<1 || ndim[1]>mMaxDimension || ndim[2]<1 || ndim[2]>mMaxDimension) err = true;

	if(err) 
	{ 
		F.Close(); 
		h.PostError("Corrupted header.");
		return;
	}

	//
	//  Measure the file size by skipping blocks with size of one variable
	//
	int nvars;
	int mar = F.SetMarker();
	vtkIdType ntotFile = (vtkIdType)ndim[0]*ndim[1]*ndim[2];
	for(nvars=0; nvars<999 && this->SkipFortranRecord(F,sizeof(float)*ntotFile); nvars++);
	F.ReturnToMarker(mar,true);

	//
	//  If no records, exit
	//
	if(nvars < 1)
	{ 
		F.Close(); 
		h.PostError("Corrupted data.");
		return; 
	}
	h.SetProgress(0.01);

	//
	//  Check that we fit into memory
	//
	bool full = true;
	if(mMaxZDim > 0)
	{
		if(ndim[2] > mMaxZDim)
		{
			full = false;
			ndim[2] = mMaxZDim;
			this->OutputText(MessageType::Warning,iString("Limiting Z dimension of the data to ")+iString::FromNumber(ndim[2])+".");
		}
	}
	vtkIdType ntotData = (vtkIdType)ndim[0]*ndim[1]*ndim[2];

	//
	//  Set the number of components
	//
	int ncoms = this->GetNumComponents(nvars);

	//
	//  parameters for the Progress Bar
	//
	float prog1 = 0.99/nvars;

	//
	//  Allocate memory; erase the old mBuffer if needed.
	//
	vtkIdType newBufferSize = (vtkIdType)(ndim[0]+mBufferSizeOffset)*(vtkIdType)(ndim[1]+mBufferSizeOffset)*(vtkIdType)(ndim[2]+mBufferSizeOffset)*ncoms;
	if(newBufferSize>mBufferSize || newBufferSize<mBufferSize/2) // do not use too much extra memory
	{
		if(mBuffer != 0) delete [] mBuffer;
		mBuffer = new float[newBufferSize];
		mBufferSize = newBufferSize;
		if(mBuffer == 0)
		{
			F.Close();
			h.PostError("Not enough memory to load the data.");
			return;
		}
	}
	//
	//  Read the data from the file.
	//
	d = new float[ntotData]; 
	if(d == 0)
	{
		this->EraseBuffer();
		F.Close();
		h.PostError("Not enough memory to load the data.");
		return;
	}

	for(i=0; !err && i<nvars; i++) 
	{
		err = !this->ReadFortranRecord(F,Buffer(d,ntotData),0.01+prog1*i,prog1,true,full);
		if(err || h.IsAborted()) break;

		this->AssignBinData(ncoms,ntotData,i,d);
	}

	delete [] d;

	F.Close();

	if(err || h.IsAborted())
	{
		this->EraseBuffer();
		if(err) h.PostError("Corrupted data.");
		return;
	}

	h.SetProgress(1.0);
	mNumVars = nvars;
	mNumComponents = ncoms;
	for(i=0; i<3; i++) mFileDims[i] = ndim[i];
}


void iUniformGridFileLoader::ReadTxtFile(const iString &fname)
{
	int n1, n2, n3, ret;
	iString s;
	vtkIdType ntot, l;
	float f[999];
	iFile F(fname);
	bool err;

	iMonitor h(this);
	if(!F.Open(iFile::_Read,iFile::_Text))
	{
		h.PostError("File "+fname+" does not exist.");
		return;
	}

	//
	//  Read the header
	//
	err = false;
	if(!F.ReadLine(s)) err = true;

	ret = sscanf(s.ToCharPointer(),"%d %d %d",&n1,&n2,&n3);
	if(ret != 3) err = true;

	if(n1<1 || n1>mMaxDimension || n2<1 || n2>mMaxDimension || n3<1 || n3>mMaxDimension) err = true;

	if(err)
	{ 
		F.Close();
		h.PostError("Corrupted header.");
		return;
	}

	ntot = (vtkIdType)n1*n2*n3;
	//
	//  Find out the number of variables
	//
	if(!F.ReadLine(s)) err = true;
	ret = sscanf(s.ToCharPointer(),"%g %g %g %g %g %g %g %g %g %g",&f[0],&f[1],&f[2],&f[3],&f[4],&f[5],&f[6],&f[7],&f[8],&f[9]);
	if(ret<1 || ret>10) err = true;
	int nvars = ret;

	//
	//  If no records or an error, exit
	//
	if(nvars<1 || err)
	{ 
		F.Close();
		h.PostError("Corrupted data.");
		return;
	}

	h.SetProgress(0.01);
	//
	//  Check that we fit into memory
	//
	if(mMaxZDim > 0.0)
	{
		if(n3 > mMaxZDim)
		{
			n3 = mMaxZDim;
			ntot = (vtkIdType)n1*n2*n3;
			this->OutputText(MessageType::Warning,iString("Limiting Z dimension of the data to ")+iString::FromNumber(n3)+".");
		}
	}

	//
	//  Set the number of components
	//
	int ncoms = this->GetNumComponents(nvars);

	//
	//  Allocate memory; erase the old mBuffer if needed.
	//
	vtkIdType newBufferSize = (vtkIdType)(n1+mBufferSizeOffset)*(vtkIdType)(n2+mBufferSizeOffset)*(vtkIdType)(n3+mBufferSizeOffset)*ncoms;
	if(newBufferSize>mBufferSize || newBufferSize<mBufferSize/2) // do not use too much extra memory
	{
		if(mBuffer != 0) delete [] mBuffer;
		mBuffer = new float[newBufferSize];
		mBufferSize = newBufferSize;
		if(mBuffer == 0)
		{
			F.Close();
			h.PostError("Not enough memory to create the data.");
			return;
		}
	}

	//
	//  Save the first line
	//
	this->AssignTxtData(ncoms,0,f);

	//
	//  Read the data from the file.
	//
	for(l=1; !err && l<ntot; l++) 
	{
		if(!F.ReadLine(s)) err = true;
		ret = sscanf(s.ToCharPointer(),"%g %g %g %g %g %g %g %g %g %g",&f[0],&f[1],&f[2],&f[3],&f[4],&f[5],&f[6],&f[7],&f[8],&f[9]);
		if(ret != nvars) err = true;
		if((100*l)/ntot < (100*(l+1))/ntot) 
		{
			h.SetProgress(0.01+0.99*double(l)/ntot);
			if(h.IsAborted())
			{
				this->EraseBuffer();
				F.Close();
				return;
			}
		}

		this->AssignTxtData(ncoms,l,f);
	}	

	F.Close();

	if(err || h.IsAborted())
	{
		this->EraseBuffer();
		if(err) h.PostError("Corrupted data.");
		return;
	}

	h.SetProgress(1.0);

	mNumVars = nvars;
	mNumComponents = ncoms;
	mFileDims[0] = n1;
	mFileDims[1] = n2;
	mFileDims[2] = n3;
}


void iUniformGridFileLoader::EraseBuffer()
{
	delete [] mBuffer; 
	mBuffer = 0; 
	mBufferSize = 0;
}


void iUniformGridFileLoader::SetVoxelLocation(int m)
{ 
	if(VoxelLocation::IsValid(m) && mVoxelLocation!=m)
	{
		int oldLoc = mVoxelLocation;
		mVoxelLocation = m;

		if(this->IsThereData())
		{
			if(mUsePeriodicOffsets && this->GetBoundaryConditions()==BoundaryConditions::Periodic)
			{
				int i;
				if(oldLoc==VoxelLocation::Vertex && mVoxelLocation==VoxelLocation::Center)
				{
					for(i=0; i<3; i++) if(this->IsDirectionPeriodic(i)) mDataDims[i] = mFileDims[i] + 1;
					mHelper->ExpandUp(mBuffer,mNumComponents,mFileDims,mDataDims);
					mHelper->PadPeriodically(mBuffer,mNumComponents,mFileDims,mDataDims);
				}
				if(oldLoc==VoxelLocation::Center && mVoxelLocation==VoxelLocation::Vertex)
				{
					mHelper->CollapseDown(mBuffer,mNumComponents,mDataDims,mFileDims);
					for(i=0; i<3; i++) mDataDims[i] = mFileDims[i];
				}
			}
			this->ReleaseData();

			iSyncWithDataRequest req(this->GetStream(0)->Subject->GetDataType());
			this->GetViewModule()->SyncWithData(req);
		}
	}
}


void iUniformGridFileLoader::ReleaseData()
{
	double org[3], spa[3];
	this->ComputeSpacing(org,spa);

	iUniformGridData *data = iUniformGridData::New(); IERROR_CHECK_MEMORY(data);
	data->SetDimensions(mDataDims);
	data->SetOrigin(org);
	data->SetSpacing(spa);

	data->SetInternalProperties(mVoxelLocation,mFileDims,mDataDims);

	this->AttachBuffer(data);
	this->AttachDataToStream(0,data);
		
	data->Delete();
}


void iUniformGridFileLoader::Polish(vtkDataSet *ds)
{
#ifdef IVTK_5
	//
	//  Extra class needed under VTK 5. What a mess!
	//
	vtkImageData *d = vtkImageData::SafeDownCast(ds);
	if(d != 0)
	{
		vtkInformation *info = d->GetPipelineInformation();
		if(info == 0)
		{
			info = vtkInformation::New(); IERROR_CHECK_MEMORY(info);
			d->SetPipelineInformation(info);
		}

		double spa[3], org[3];
		d->GetOrigin(org);
		d->GetSpacing(spa);

		info->Set(vtkDataObject::ORIGIN(),org,3);
		info->Set(vtkDataObject::SPACING(),spa,3);
	}
#endif
}


void iUniformGridFileLoader::SetMaxZDimension(int v)
{
	if(v >= 0)
	{
		mMaxZDim = v;
	}
}


void iUniformGridFileLoader::SetScaledDimension(int v)
{
	if(v>=-2 && v<=2)
	{
		mScaledDim = v;

		if(this->IsThereData())
		{
			//
			//  Reset spacing
			//
			vtkImageData *data = iRequiredCast<vtkImageData>(INFO,this->GetStream(0)->ReleasedData);

			double org[3], spa[3];
			this->ComputeSpacing(org,spa);
			data->SetOrigin(org);
			data->SetSpacing(spa);
		}
	}
}


void iUniformGridFileLoader::ComputeSpacing(double org[3], double spa[3])
{
	int i;

	if(mFileDims[0]<=0 || mFileDims[1]<=0 || mFileDims[2]<=0) return;

	//
	//  Now we need to decide whether the data are CellData or PointData. We use voxelLocation for that: 
	//  if it is 0, we have point data and we place the first point into (-1,-1,-1) corner;
	//  if it is 1, we have cell data and we place the first point into the center of the first cell (mOriginOffset).
	//
	float shift;
	if(mVoxelLocation == VoxelLocation::Vertex)
	{
		shift = 0.0;
	}
	else
	{
		shift = 0.5;
	}
	for(i=0; i<3; i++) mOriginOffset[i] = shift*2.0/mFileDims[i];

	//
	//  Find longest and short dimensions
	//
	int nmax, nmin;
	int imax = 0, imin = 0;
	nmax = nmin = mFileDims[0];
	for(i=1; i<3; i++)
	{
		if(nmax < mFileDims[i]) 
		{
			nmax = mFileDims[i];
			imax = i;
		}
		if(mFileDims[i]>1 && nmin>mFileDims[i]) 
		{
			nmin = mFileDims[i];
			imin = i;
		}
	}

	switch (mScaledDim)
	{
	case -1:
		{
			i = imin;
			break;
		}
	case  0:
	case  1:
	case  2:
		{
			if(mFileDims[mScaledDim] > 1) i = mScaledDim; else i = imin;
			break;
		}
	case -2: 
	default:
		{ 
			i = imax;
		}
	}

	double s = (2.0-2*mOriginOffset[i])/(mFileDims[i]-1);

	for(i=0; i<3; i++) 
	{	
		org[i] = -1.0 + shift*s;
		spa[i] = s;
	}
}


void iUniformGridFileLoader::ShiftDataBody(vtkDataSet *dataIn, const double *dx)
{
	vtkImageData *data = vtkImageData::SafeDownCast(dataIn);
	if(data==0 || !this->IsThereData(0)) return;

	int d, dn;
	for(d=0; d<3; d++)
	{
		if(this->IsDirectionPeriodic(d))
		{
			double orgIdeal[3], orgActual[3], spa[3];
			this->ComputeSpacing(orgIdeal,spa); // origin we should have ideally
			data->GetOrigin(orgActual);

			dn = iMath::Round2Int((dx[d]+0.5*(orgActual[d]-orgIdeal[d]))*mDataDims[d]) % mDataDims[d];
			if(dn != 0)
			{
				vtkIdType size = (vtkIdType)mDataDims[0]*mDataDims[1]*mDataDims[2]*mNumComponents;
				float *buf = new float[mNumComponents*mDataDims[d]]; if(buf == 0) return;
				if(data->GetPointData()!=0 && data->GetPointData()->GetScalars()!=0)
				{
					mHelper->ShiftData(mNumComponents,d,dn,mDataDims,(float *)data->GetPointData()->GetScalars()->GetVoidPointer(0),buf);
				}
				if(data->GetPointData()!=0 && data->GetPointData()->GetVectors()!=0)
				{
					mHelper->ShiftData(mNumComponents,d,dn,mDataDims,(float *)data->GetPointData()->GetVectors()->GetVoidPointer(0),buf);
				}
				if(data->GetPointData()!=0 && data->GetPointData()->GetTensors()!=0)
				{
					mHelper->ShiftData(mNumComponents,d,dn,mDataDims,(float *)data->GetPointData()->GetTensors()->GetVoidPointer(0),buf);
				}
				delete[] buf;
			}

			orgActual[d] += 2.0*(dx[d]-float(dn)/mDataDims[d]);
			data->SetOrigin(orgActual);
#ifndef I_NO_CHECK
			if(fabs(orgActual[d]-orgIdeal[d]) > 0.5*spa[d])
			{
				IBUG_WARN("Non-critical bug in data placement.");
			}
#endif
		}
		else
		{
			double org[3];
			data->GetOrigin(org);
			org[d] += 2.0*dx[d];
			data->SetOrigin(org);
		}
	}

	data->Modified();
}


bool iUniformGridFileLoader::IsCompatible(iUniformGridFileLoader *other) const
{
	return (this->IsThereData() && other!=0 && other->IsThereData() && mVoxelLocation==other->mVoxelLocation && mFileDims[0]==other->mFileDims[0] && mFileDims[1]==other->mFileDims[1] && mFileDims[2]==other->mFileDims[2]);
}


//
//  Helper class
//
iUniformGridHelper::iUniformGridHelper(iUniformGridFileLoader *loader) : iParallelWorker(loader->GetViewModule()->GetParallelManager())
{
	mLoader = loader; IASSERT(loader);
}


void iUniformGridHelper::ShiftData(int numCom, int dim, int dn, int dataDims[3], float *data, float *buf)
{
	mNumCom = numCom;
	mDims = dataDims;
	mItmp2 = dn;
	mItmp1 = dim;
	mArr1 = data;
	mArr2 = buf;

	this->ParallelExecute(1);
}


void iUniformGridHelper::ExpandUp(float *array, int nc, int oldDims[3], int newDims[3])
{
	//
	//  Can we do this in parallel?
	//
	int i, j, k;
	float *ptr;

	if(array==0 || nc<1 || oldDims[0]<1 || oldDims[1]<1 || oldDims[2]<1)
	{
		IBUG_FATAL("Invalid use of iUniformGridHelper::ExpandUp.");
		return;
	}
	for(i=0; i<3; i++) if(oldDims[i]!=newDims[i] && oldDims[i]+1!=newDims[i])
	{
		IBUG_FATAL("Incorrect dimensions for expanding data.");
		return;
	}

	iMonitor h(mLoader,"Formatting",1.0);

	vtkIdType oldN12 = nc*oldDims[0]*oldDims[1]; 
	vtkIdType newN12 = nc*newDims[0]*newDims[1];
	vtkIdType size12 = oldN12*sizeof(float);

	if(newN12 > oldN12)
	{
		//
		//  Move plains
		//
		for(k=oldDims[2]-1; k>=1; k--)
		{
			h.SetProgress(0.5*float(oldDims[2]-1-k)/oldDims[2]);
			if(h.IsAborted()) return;

			memmove(array+k*newN12,array+k*oldN12,size12); // will memcpy work?
		}
	}

	vtkIdType oldN1 = nc*oldDims[0]; 
	vtkIdType newN1 = nc*newDims[0];
	vtkIdType size1 = oldN1*sizeof(float);

	if(newN1 > oldN1)
	{
		//
		//  Move rows
		//
		for(k=0; k<oldDims[2]; k++)
		{
			h.SetProgress(0.5+0.5*float(k)/oldDims[2]);
			if(h.IsAborted()) return;
			ptr = array + k*newN12;
			for(j=oldDims[1]-1; j>=1; j--)
			{
				memmove(ptr+j*newN1,ptr+j*oldN1,size1);
			}
		}
	}

	h.SetProgress(1.0);
}


void iUniformGridHelper::CollapseDown(float *array, int nc, int oldDims[3], int newDims[3])
{
	//
	//  Can we do this in parallel?
	//
	int i, j, k;
	float *ptr;

	if(array==0 || nc<1 || newDims[0]<1 || newDims[1]<1 || newDims[2]<1)
	{
		IBUG_FATAL("Invalid use of iUniformGridHelper::CollapseDown.");
		return;
	}
	for(i=0; i<3; i++) if(oldDims[i]!=newDims[i] && oldDims[i]-1!=newDims[i])
	{
		IBUG_FATAL("Incorrect dimensions for collapsing data.");
		return;
	}

	vtkIdType oldN12 = nc*oldDims[0]*oldDims[1]; 
	vtkIdType oldN1 = nc*oldDims[0]; 
	vtkIdType newN1 = nc*newDims[0];
	vtkIdType size1 = newN1*sizeof(float);

	iMonitor h(mLoader,"Formatting",1.0);

	if(newN1 < oldN1)
	{
		//
		//  Move rows
		//
		for(k=0; k<newDims[2]; k++)
		{
			h.SetProgress(0.5*float(k)/newDims[2]);
			if(h.IsAborted()) return;

			ptr = array + k*oldN12;
			for(j=1; j<newDims[1]; j++)
			{
				memmove(ptr+j*newN1,ptr+j*oldN1,size1);
			}
		}
	}

	vtkIdType newN12 = nc*newDims[0]*newDims[1];
	vtkIdType size12 = newN12*sizeof(float);

	if(newN12 < oldN12)
	{
		//
		//  Move plains
		//
		for(k=1; k<newDims[2]; k++)
		{
			h.SetProgress(0.5+0.5*float(k)/newDims[2]);
			if(h.IsAborted()) return;
			memmove(array+k*newN12,array+k*oldN12,size12); // will memcpy work?
		}
	}

	h.SetProgress(1.0);
}


void iUniformGridHelper::PadPeriodically(float *array, int nc, int oldDims[3], int newDims[3])
{
	int i, j, k;
	float *ptr;
	vtkIdType size, offset;

	if(array==0 || nc<1 || oldDims[0]<1 || oldDims[1]<1 || oldDims[2]<1)
	{
		IBUG_FATAL("Invalid use of iUniformGridHelper::PadPeriodically.");
		return;
	}

	for(i=0; i<3; i++) if(oldDims[i]!=newDims[i] && oldDims[i]+1!=newDims[i])
	{
		IBUG_FATAL("Incorrect dimensions for padding data.");
		return;
	}

	//
	//  Do x-direction
	//
	if(newDims[0] > oldDims[0])
	{
		size = nc*sizeof(float);
		offset = oldDims[0]*nc;
		for(k=0; k<oldDims[2]; k++)
		{
			for(j=0; j<oldDims[1]; j++)
			{
				ptr = array + nc*newDims[0]*(j+newDims[1]*k);
				memcpy(ptr+offset,ptr,size);
			}
		}
	}

	//
	//  Do y-direction
	//
	if(newDims[1] > oldDims[1])
	{
		size = newDims[0]*nc*sizeof(float);
		offset = oldDims[1]*newDims[0]*nc;
		for(k=0; k<oldDims[2]; k++)
		{
			ptr = array + nc*newDims[0]*newDims[1]*k;
			memcpy(ptr+offset,ptr,size);
		}
	}

	//
	//  Do z-direction
	//
	if(newDims[2] > oldDims[2])
	{
		size = newDims[1]*newDims[0]*nc*sizeof(float);
		offset = oldDims[2]*newDims[1]*newDims[0]*nc;

		memcpy(array+offset,array,size);
	}

#ifdef I_CHECK
	bool ok = true;
	int l, ijk1[3], ijk2[3];
	for(k=0; ok && k<3; k++) if(newDims[k] > oldDims[k])
	{
		i = (k+1) % 3;
		j = (k+2) % 3;
		ijk1[k] = 0;
		ijk2[k] = oldDims[k];

		for(ijk1[j]=ijk2[j]=0; ok && ijk1[j]<newDims[j]; ijk2[j]=++ijk1[j])
		{
			for(ijk1[i]=ijk2[i]=0; ok && ijk1[i]<newDims[i]; ijk2[i]=++ijk1[i])
			{
				for(l=0; ok && l<nc; l++)
				{
					ok = (fabs(array[l+nc*(ijk1[0]+newDims[0]*(ijk1[1]+newDims[1]*ijk1[2]))]-array[l+nc*(ijk2[0]+newDims[0]*(ijk2[1]+newDims[1]*ijk2[2]))]) < iMath::_FloatTolerance);
				}
			}
		}
	}
	if(!ok)
	{
		IBUG_FATAL("Failed check in iUniformGridHelper::PadPeriodically.");
	}
#endif
}


int iUniformGridHelper::ExecuteStep(int step, iParallel::ProcessorInfo &p)
{
	switch(step)
	{
	case 1:
		{
			this->ExecuteShiftData(p);
			return 0;
		}
	default: 
		{
#ifndef I_NO_CHECK
			IBUG_WARN("Internal check failed.");
#endif
			return 2;
		}
	}
}


//
//  Shift an nc-component vertical array (treat the mesh array as nvar 1-component vertical arrays)
//
void iUniformGridHelper::ExecuteShiftData(iParallel::ProcessorInfo &p)
{
	int n, l1, l2, l, lold, add1;
	int d1 = 0, d2 = 0;
	
	int d = mItmp1;
	int dn = mItmp2;

	switch(d) 
	{
	case 0: { d1 = 1; d2 = 2; break; }
	case 1: { d1 = 0; d2 = 2; break; }
	case 2: { d1 = 0; d2 = 1; break; }
	}
	
	int ddim = mDims[d];
	vtkIdType off0 = 0, off1 = 0, add2;
	
	int l2beg, l2end, l2stp;
	iParallel::SplitRange(p,mDims[d2],l2beg,l2end,l2stp);

	for(l2=l2beg; l2<l2end; l2++)
	{
		if(p.IsMaster()) mLoader->GetShell()->GetExecutionEventObserver()->SetProgress((d+double(l2-l2beg)/(l2end-l2beg))/3.0);
		if(mLoader->GetShell()->GetExecutionEventObserver()->IsAborted()) return;

		for(l1=0; l1<mDims[d1]; l1++)
		{
			switch(d) 
			{
			case 0: { off0 = mNumCom*(vtkIdType)mDims[0]*(l1+mDims[1]*l2); off1 = mNumCom;	break; }
			case 1: { off0 = mNumCom*(l1+(vtkIdType)mDims[0]*mDims[1]*l2); off1 = mNumCom*mDims[0]; break; }
			case 2: { off0 = mNumCom*(l1+(vtkIdType)mDims[0]*l2); off1 = mNumCom*(vtkIdType)mDims[0]*mDims[1]; break; }
			}
			
			for(l=0; l<ddim; l++) 
			{
				lold = l - dn;
				while(lold < 0) lold += ddim;
				while(lold >= ddim) lold -= ddim;
				
				add1 = mNumCom*l;
				add2 = off0 + off1*lold;

				for(n=0; n<mNumCom; n++) mArr2[n+add1] = mArr1[n+add2];
			}
			
			for(l=0; l<ddim; l++) 
			{
				add1 = mNumCom*l;
				add2 = off0 + off1*l;
				
				for(n=0; n<mNumCom; n++) mArr1[n+add2] = mArr2[n+add1];
			}
		}
	}
}

