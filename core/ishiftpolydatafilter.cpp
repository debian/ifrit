/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ishiftpolydatafilter.h"


#include "ierror.h"

#include <vtkFloatArray.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>


//
//  Templates
//
#include "igenericfilter.tlh"


iShiftPolyDataFilter::iShiftPolyDataFilter(iDataConsumer *consumer) : iGenericPolyDataFilter<vtkPolyDataAlgorithm>(consumer,1,true)
{
	mShift = 0.003f;
}


void iShiftPolyDataFilter::SetShift(float s)
{
	mShift = s;
	this->Modified();
}


void iShiftPolyDataFilter::ProvideOutput()
{
	vtkPolyData *input = this->InputData();
	vtkPolyData *output = this->OutputData();

	output->DeepCopy(input);

	if(fabs(mShift) < 1.0e-30) return;

	vtkFloatArray *nor = vtkFloatArray::SafeDownCast(output->GetPointData()->GetNormals());
	vtkPoints *poi = output->GetPoints();

	vtkIdType n = nor->GetNumberOfTuples();
	if(n != poi->GetNumberOfPoints()) return;

	n *= 3;
	vtkIdType l; 
	float *nptr = (float *)nor->GetVoidPointer(0);

	switch(poi->GetDataType())
	{
	case VTK_FLOAT:
		{
			float *xptr = (float *)poi->GetVoidPointer(0);
			for(l=0; l<n; l++) xptr[l] += mShift*nptr[l];
			return;
		}
	case VTK_DOUBLE:
		{
			double *xptr = (double *)poi->GetVoidPointer(0);
			for(l=0; l<n; l++) xptr[l] += mShift*nptr[l];
			return;
		}
	default:
		{
			vtkErrorMacro("Invalid points data type");
			return;
		}
	}
}

