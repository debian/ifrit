/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "icaptioninteractorstyle.h"


#include "icaption.h"
#include "imarker.h"
#include "irendertool.h"
#include "iviewmodule.h"
#include "iviewobject.h"

#include <vtkActor2D.h>
#include <vtkCamera.h>
#include <vtkCellArray.h>
#include <vtkMath.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper2D.h>
#include <vtkProperty2D.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"


iCaptionInteractorStyle* iCaptionInteractorStyle::New(iViewModule *vm)
{
	IASSERT(vm);
	return new iCaptionInteractorStyle(vm);
}


iCaptionInteractorStyle::iCaptionInteractorStyle(iViewModule *vm) : iViewModuleComponent(vm)
{
	mInteractionMarker = 0;

	//
	//  Experimental: gray screen for moving captions
	//
	vtkPolyData *data = vtkPolyData::New(); IERROR_CHECK_MEMORY(data);

	vtkPoints *pts = vtkPoints::New(VTK_FLOAT); IERROR_CHECK_MEMORY(pts);
	pts->SetNumberOfPoints(4);
	pts->SetPoint(0,0.0,0.0,0.0);
	pts->SetPoint(1,1.0,0.0,0.0);
	pts->SetPoint(2,1.0,1.0,0.0);
	pts->SetPoint(3,0.0,1.0,0.0);
	data->SetPoints(pts);
	pts->Delete();

	vtkCellArray *cells = vtkCellArray::New(); IERROR_CHECK_MEMORY(cells);
	cells->InsertNextCell(4);
	cells->InsertCellPoint(0);
	cells->InsertCellPoint(1);
	cells->InsertCellPoint(2);
	cells->InsertCellPoint(3);
	data->SetPolys(cells);
	cells->Delete();
	
	vtkPolyDataMapper2D *mapper = vtkPolyDataMapper2D::New(); IERROR_CHECK_MEMORY(mapper);
	mapper->SetInputData(data);
	data->Delete();
	vtkCoordinate *c = vtkCoordinate::New(); IERROR_CHECK_MEMORY(c);
	c->SetCoordinateSystemToNormalizedViewport();
	mapper->SetTransformCoordinate(c);
	c->Delete();

	mScreen = vtkActor2D::New(); IERROR_CHECK_MEMORY(mScreen);
	mScreen->SetMapper(mapper);
	mapper->Delete();

	mScreen->GetPositionCoordinate()->SetCoordinateSystemToNormalizedViewport();
	mScreen->SetPosition(0.0,0.0);
	mScreen->SetLayerNumber(10);
	mScreen->GetProperty()->SetOpacity(0.7);
	mScreen->GetProperty()->SetColor(0.5,0.5,0.5);
	this->GetViewModule()->GetRenderTool()->AddObject(mScreen);
	mScreen->SetVisibility(0);

	wNextStyle = 0;
	wTransparentCaptions = true;
}


iCaptionInteractorStyle::~iCaptionInteractorStyle()
{
	this->GetViewModule()->GetRenderTool()->RemoveObject(mScreen);
	mScreen->Delete();
}


void iCaptionInteractorStyle::Launch(vtkInteractorStyle *next)
{
	mScreen->SetVisibility(1);
	wNextStyle = next;
	wTransparentCaptions = iCaption::Transparent;
	iCaption::Transparent = false;
}


void iCaptionInteractorStyle::FindPickedActor(int x, int y)
{
	int i;

	if(this->CurrentRenderer == 0) return;
	
	for(i=0; i<this->GetViewModule()->GetNumberOfMarkers(); i++) if(this->GetViewModule()->GetMarker(i)->GetMarkerCaption()->GetVisibility() == 1)
	{
		int *s = this->CurrentRenderer->GetSize();
		float b[4];
		this->GetViewModule()->GetMarker(i)->GetMarkerCaption()->GetBounds(this->CurrentRenderer,b);
		if(x>b[0]*s[0] && y>b[2]*s[1] && x<b[1]*s[0] && y<b[3]*s[1])
		{
			mInteractionMarker = this->GetViewModule()->GetMarker(i);
			return;
		}
	}
	mInteractionMarker = 0;
}

//
//  Rotating - a copy of vtkInteractorStyleTrackballActor::Rotate() with 
//  no changes
//
void iCaptionInteractorStyle::Rotate()
{
	if(CurrentRenderer==0 || mInteractionMarker==0)
    {
		return;
    }
	
	vtkRenderWindowInteractor *rwi = this->Interactor;
	
	int *size = CurrentRenderer->GetSize();
	float dx = rwi->GetEventPosition()[0] - rwi->GetLastEventPosition()[0];
	float dy = rwi->GetEventPosition()[1] - rwi->GetLastEventPosition()[1];
	
	const iPair &x(mInteractionMarker->GetCaptionPosition());
	mInteractionMarker->SetCaptionPosition(iPair(x.X+dx/size[0],x.Y+dy/size[1]));
	
    rwi->Render();
}

//
// Need to overload this function too because it is not declared virtual in the parent class 
//
void iCaptionInteractorStyle::OnLeftButtonDown() 
{
	int x = Interactor->GetEventPosition()[0];
	int y = Interactor->GetEventPosition()[1];
	
	this->FindPokedRenderer(x, y);
	this->FindPickedActor(x, y);
	if(CurrentRenderer!=0 && mInteractionMarker!=0)
    {
	    this->StartRotate();
	}
	else
	{
		this->Finish();
    }
	
}


void iCaptionInteractorStyle::OnLeftButtonUp()
{
	//
	//  Quantize the position
	//
	if(mInteractionMarker != 0)
	{
		int *size = CurrentRenderer->GetSize();
		const iPair &x0(mInteractionMarker->GetCaptionPosition());
		iPair x;
		x.X = 10.0*floor(0.1*x0.X*size[0])/size[0];
		x.Y = 10.0*floor(0.1*x0.Y*size[1])/size[1];
		mInteractionMarker->SetCaptionPosition(x);
		Interactor->Render();
	}

	mInteractionMarker = 0;
}

void iCaptionInteractorStyle::Finish()
{
	if(mScreen->GetVisibility() != 0)
	{
		mScreen->SetVisibility(0);
		iCaption::Transparent = wTransparentCaptions;
		Interactor->Render();

		if(wNextStyle != 0)
		{
			this->GetInteractor()->SetInteractorStyle(wNextStyle);
		}
	}
}
