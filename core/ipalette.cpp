/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ipalette.h"


#include "ierror.h"
#include "iimage.h"
#include "imath.h"
//#include "ipalettecollection.h"
#include "ipiecewisefunction.h"

#include <vtkColorTransferFunction.h> 
#include <vtkLookupTable.h>

//
//  Templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


//
// Implementation of iPalette class
//
iPalette::iPalette(iObject *parent, const iString &fname, const iString &sname) : iObject(parent,fname,sname),
	Name(iObject::Self(),static_cast<iType::ps_string::SetterType>(&iPalette::SetName),static_cast<iType::ps_string::GetterType>(&iPalette::GetName),"Name","n",4),
	Red(iObject::Self(),"Red","r",3),
	Green(iObject::Self(),"Green","g",2),
	Blue(iObject::Self(),"Blue","b",1)
{
	mRenderMode = iParameter::RenderMode::All;

	mName = "unnamed";

	mRed = iPiecewiseFunction::New(); IERROR_CHECK_MEMORY(mRed);
	mGreen = iPiecewiseFunction::New(); IERROR_CHECK_MEMORY(mGreen);
	mBlue = iPiecewiseFunction::New(); IERROR_CHECK_MEMORY(mBlue);

	Red.AttachFunction(mRed);
	Green.AttachFunction(mGreen);
	Blue.AttachFunction(mBlue);

	mCTF[0] = vtkColorTransferFunction::New(); IERROR_CHECK_MEMORY(mCTF[0]);
	mCTF[1] = vtkColorTransferFunction::New(); IERROR_CHECK_MEMORY(mCTF[1]);
	mLT[0] = vtkLookupTable::New(); IERROR_CHECK_MEMORY(mLT[0]);
	mLT[1] = vtkLookupTable::New(); IERROR_CHECK_MEMORY(mLT[1]);
	
	mCTF[0]->SetColorSpaceToRGB();
	mCTF[1]->SetColorSpaceToRGB();
	mLT[0]->SetNumberOfTableValues(256);
	mLT[1]->SetNumberOfTableValues(256);

	//
	//  Image representation
	//
	mImage[0] = new iImage(3); IERROR_CHECK_MEMORY(mImage[0]);
	mImage[1] = new iImage(3); IERROR_CHECK_MEMORY(mImage[1]);
	mImage[2] = new iImage(3); IERROR_CHECK_MEMORY(mImage[2]);
	mImageNeedsUpdate = true;
	mImageWidth = 256;
	mImageHeight = 32;
}


iPalette::~iPalette()
{
	delete mImage[2];
	delete mImage[1];
	delete mImage[0];

	mCTF[0]->Delete(); 
	mCTF[1]->Delete(); 
	mLT[0]->Delete(); 
	mLT[1]->Delete();

	mRed->Delete();
	mGreen->Delete();
	mBlue->Delete();
}


bool iPalette::SetName(const iString& s)
{
	mName = s;
	return true;
}


void iPalette::SetComponents(const iPiecewiseFunction *r, const iPiecewiseFunction *g, const iPiecewiseFunction *b)
{
	if(r != 0) mRed->Copy(r);
	if(g != 0) mGreen->Copy(g);
	if(b != 0) mBlue->Copy(b);

	this->Update();
}


iPiecewiseFunction* iPalette::GetComponent(int n) const
{
	switch(n)
	{
	case 0: return mRed;
	case 1: return mGreen;
	case 2: return mBlue;
	default: return 0;
	}
}

	
iColor iPalette::GetColor(int n) const
{
	n = ( n < 0 ) ? 0 : n;
	n = ( n > 255 ) ? 255 : n;
	float x = float(n)/255.0;
	float r = mRed->GetValue(x);
	float g = mGreen->GetValue(x);
	float b = mBlue->GetValue(x);
	return iColor(iMath::Round2Int(255.0*r),iMath::Round2Int(255.0*g),iMath::Round2Int(255.0*b));
}


void iPalette::Update()
{
	int i, ir, ig, ib;
	float x;

	//
	//  Update vtkColorTransferFunction
	//
	mCTF[0]->RemoveAllPoints();
	mCTF[1]->RemoveAllPoints();
	ir = ig = ib = 0;
	while(ir<mRed->N() && ig<mGreen->N() && ib<mBlue->N())
	{
		x = mRed->X(ir);
		if(x > mGreen->X(ig)) x = mGreen->X(ig);
		if(x > mBlue->X(ib)) x = mBlue->X(ib);
		
		mCTF[0]->AddRGBPoint(255.0*x,mRed->GetValue(x),mGreen->GetValue(x),mBlue->GetValue(x));
		mCTF[1]->AddRGBPoint(255.0*(1.0-x),mRed->GetValue(x),mGreen->GetValue(x),mBlue->GetValue(x));
		
		if(fabs(x-mRed->X(ir)) < 1.0e-4) ir++;
		if(fabs(x-mGreen->X(ig)) < 1.0e-4) ig++;
		if(fabs(x-mBlue->X(ib)) < 1.0e-4) ib++;
	}

	//
	//  Update vtkLookupTable
	//
	for(i=0; i<256; i++)
	{
		x = float(i)/255.0;
		mLT[0]->SetTableValue(i,mRed->GetValue(x),mGreen->GetValue(x),mBlue->GetValue(x));
		mLT[1]->SetTableValue(255-i,mRed->GetValue(x),mGreen->GetValue(x),mBlue->GetValue(x));
	}

	mImageNeedsUpdate = true;
}


bool iPalette::CopyState(const iObject *other)
{
	const iPalette *p = dynamic_cast<const iPalette*>(other);
	if(p == 0) return false;

	mRed->Copy(p->mRed);
	mGreen->Copy(p->mGreen);
	mBlue->Copy(p->mBlue);
	this->SetName(p->GetName());

	this->Update();
	return true;
}


const iImage* iPalette::GetImage(int shape)
{
	if(shape<0 || shape>2) return 0;

	if(mImageNeedsUpdate)
	{
		mImageNeedsUpdate = false;

		//
		//  Update the image
		//
		mImage[0]->Scale(mImageWidth,mImageHeight);
		mImage[1]->Scale(mImageHeight,mImageWidth);
		mImage[2]->Scale(mImageHeight,mImageWidth);

		unsigned char *dPtr0 = mImage[0]->DataPointer();
		unsigned char *dPtr1 = mImage[1]->DataPointer();
		unsigned char *dPtr2 = mImage[2]->DataPointer();

		int i, j, ir;
		float x;
		unsigned char r, g, b;

		for(i=0; i<mImageWidth; i++)
		{
			ir = mImageWidth - 1 - i;
			x = float(i)/(mImageWidth-1);
			r = char(iMath::Round2Int(255.0*mRed->GetValue(x)));
			g = char(iMath::Round2Int(255.0*mGreen->GetValue(x)));
			b = char(iMath::Round2Int(255.0*mBlue->GetValue(x)));

			dPtr0[0+3*i] = r;
			dPtr0[1+3*i] = g;
			dPtr0[2+3*i] = b;

			for(j=0; j<mImageHeight; j++)
			{
				dPtr1[0+3*(j+mImageHeight*i)] = r;
				dPtr1[1+3*(j+mImageHeight*i)] = g;
				dPtr1[2+3*(j+mImageHeight*i)] = b;
				dPtr2[0+3*(j+mImageHeight*ir)] = r;
				dPtr2[1+3*(j+mImageHeight*ir)] = g;
				dPtr2[2+3*(j+mImageHeight*ir)] = b;
			}	
		}

		//
		//  Copy to the rest of rows
		//
		for(i=1; i<mImageHeight; i++) memcpy(dPtr0+3*i*mImageWidth,dPtr0,3*mImageWidth);

	}

	return mImage[shape];
}

