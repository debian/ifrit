/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IPALETTECOLLECTION_H
#define IPALETTECOLLECTION_H


#include "iobjectcollection.h"
#include "ipalette.h"
#include "ishellcomponent.h"


class vtkColorTransferFunction;
class vtkImageData;
class vtkLookupTable;


//
//  We inherit from iObject to get access to state saving
//
class iPaletteCollection : public iObjectCollectionFixed<iPalette>, public iShellComponent
{

	friend class iShell;

public:

	vtkTypeMacro(iPaletteCollection,iObjectCollectionFixed<iPalette>);
	static iPaletteCollection* New(iShell *s = 0);
	
	iPropertyAction Create;

	//
	//  Global set, available everywhere
	//
	static const iPaletteCollection* Global(){ return mGlobal; }

	iPalette* GetPalette(int n) const;
	vtkImageData* GetImageData(int id) const;
	vtkLookupTable* GetLookupTable(int id) const;
	vtkColorTransferFunction* GetColorTransferFunction(int id) const;

protected:

	virtual ~iPaletteCollection();

	virtual iPalette* NewObject();

private:

	iPaletteCollection(iShell *s, const iString &fname, const iString &sname);

	void CreateDefaultPalettes();
	static void WritePaletteImages(iShell *s, const iString& dirname);

	static iPaletteCollection* mGlobal;
};

#endif  // IPALETTECOLLECTION_H
