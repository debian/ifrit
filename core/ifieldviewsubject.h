/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IFIELDVIEWSUBJECT_H
#define IFIELDVIEWSUBJECT_H


#include "ipaintedviewsubject.h"


class iFieldViewSubject;
class iMergeDataFilter;
class iViewModule;


class iFieldViewInstance : public iPaintedViewInstance
{

	friend class iFieldViewSubject;
	
public:
	
	iDataConsumerTypeMacro(iFieldViewInstance,iPaintedViewInstance);
		
	iObjectSlotMacro1SV(GlyphSize,float);
	iObjectSlotMacro1SV(GlyphSampleRate,int);

protected:
	
	iFieldViewInstance(iFieldViewSubject *owner, int numactors);
	virtual ~iFieldViewInstance();

	iFieldViewSubject* Owner() const;

	virtual bool IsConnectedToPaintingData() const;

	virtual void UpdateGlyphSize() = 0;
	virtual void UpdateGlyphSampleRate() = 0;
	virtual void ResetPipelineInput(vtkDataSet *input) = 0;

	iMergeDataFilter *mMergeDataFilter;
};


class iFieldViewSubject : public iPaintedViewSubject
{
	
public:
	
	iDataConsumerTypeMacroPass(iFieldViewSubject,iPaintedViewSubject);

	iType::vsp_float GlyphSize;
	iType::vsp_int GlyphSampleRate;

	iObjectPropertyMacro2Q(IsConnectedToScalars,Bool); 

protected:
	
	iFieldViewSubject(iObject *parent, const iString &fname, const iString &sname, iViewModule *vm, const iDataType &type, const iDataType &sdt, unsigned int flags);
	virtual ~iFieldViewSubject();
};

#endif // IFIELDVIEWSUBJECT_H

