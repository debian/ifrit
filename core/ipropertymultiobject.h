/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
// A property for an Object with multiple Instances (MarkerObject, ViewSubject)
//

#ifndef IPROPERTYMULTIOBJECT_H
#define IPROPERTYMULTIOBJECT_H


#include "iproperty.h"


template<class Object, class Instance, iType::TypeId id>
class iPropertyMultiObject : public iPropertyDataVariable<id>
{

public:

	typedef typename iType::Traits<id>::Type ArgT;
	typedef bool (Instance::*SetterType)(ArgT val);
	typedef ArgT (Instance::*GetterType)() const;

	iPropertyMultiObject(Object *owner, SetterType setter, GetterType getter, const iString &fname, const iString &sname, int rank = 0);

	virtual int Size() const;
	virtual bool IsFixedSize() const { return false; }

	virtual ArgT GetValue(int i) const;
	virtual bool SetValue(int i, ArgT v) const;

protected:

	virtual bool Resize(int, bool force = false){ return false; }

private:

	Object *mRealOwner;
	SetterType mSetter;
	GetterType mGetter;
};



template<class Object, class Instance, iType::TypeId id>
iPropertyMultiObject<Object,Instance,id>::iPropertyMultiObject(Object *owner, SetterType setter, GetterType getter, const iString &fname, const iString &sname, int rank) : iPropertyDataVariable<id>(owner,fname,sname,rank)
{
	this->mRealOwner = owner;
	this->mSetter = setter;
	this->mGetter = getter;
}


template<class Object, class Instance, iType::TypeId id>
int iPropertyMultiObject<Object,Instance,id>::Size() const
{
	return this->mRealOwner->mInstances.Size();
}


template<class Object, class Instance, iType::TypeId id>
typename iPropertyMultiObject<Object,Instance,id>::ArgT iPropertyMultiObject<Object,Instance,id>::GetValue(int i) const
{
	return (this->mRealOwner->mInstances[i]->*this->mGetter)();
}


template<class Object, class Instance, iType::TypeId id>
bool iPropertyMultiObject<Object,Instance,id>::SetValue(int i, ArgT v) const
{
	return (this->mRealOwner->mInstances[i]->*this->mSetter)(v);
}

#endif // IPROPERTYMULTIOBJECT_H

