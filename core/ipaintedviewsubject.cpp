/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ipaintedviewsubject.h"


#include "iactor.h"
#include "icolorbar.h"
#include "idatalimits.h"
#include "idatareader.h"
#include "ierror.h"
#include "ilookuptable.h"
#include "ipalette.h"
#include "ipalettecollection.h"
#include "iviewmodule.h"

//
//  Templates (needed for some compilers)
//
#include "iarray.tlh"
#include "iproperty.tlh"


using namespace iParameter;


//
//  Single instance class
//
iPaintedViewInstance::iPaintedViewInstance(iPaintedViewSubject *owner, int numactors, float maxopacity, int numpaintedactors, int priority, bool alwayspainted) : iActorViewInstance(owner,numactors,maxopacity), mNumPaintedActors((numactors<numpaintedactors)?numactors:numpaintedactors), mColorBarPriority(priority), mAlwaysPainted(alwayspainted)
{
	mPaintVar = alwayspainted ? 0 : -1;
	mPalette = 1;

	mPaintOffset = 0;

	int i;
	for(i=0; i<mNumPaintedActors; i++)
	{
		mActors[i]->SetScalarVisibility(alwayspainted);
		mActors[i]->GetLookupTable()->SetPaletteId(mPalette);
		mActors[i]->ColorByArrayComponent(0,0); 
	}
}


iPaintedViewInstance::~iPaintedViewInstance()
{
}


iPaintedViewSubject* iPaintedViewInstance::Owner() const
{
	return iRequiredCast<iPaintedViewSubject>(INFO,this->iActorViewInstance::Owner());
}


void iPaintedViewInstance::UpdateColorBars()
{
	if(mColorBars.Size()==0 && this->IsPainted())
	{
		this->AddColorBar(mColorBarPriority,mPaintVar,mPalette,this->GetPaintingLimits()->GetDataType());
	}
}


bool iPaintedViewInstance::SetPaintVar(int v)
{
	if(v<-1 || v>=this->GetPaintingLimits()->GetNumVars()) return false;

	if(v != mPaintVar)
	{
		this->RemoveColorBars();
		mPaintVar = v;
		this->UpdateColorBars();
		this->UpdatePainting();
		this->Owner()->UpdateAutomaticShading();
		if(mAlwaysPainted && v==-1)
		{
			this->Show(false);
		}
	}
	return true;
}


bool iPaintedViewInstance::SetPalette(int pid)
{
	if(abs(pid) > iPaletteCollection::Global()->Size()) return false;

	this->RemoveColorBars();
	mPalette = pid;
	this->UpdateColorBars();
	
	int i;
	for(i=0; i<mNumPaintedActors; i++)
	{
		mActors[i]->GetLookupTable()->SetPaletteId(pid);
	}

	return true;
}


void iPaintedViewInstance::UpdatePainting()
{
	int i;

	if(this->IsPainted())
	{
		for(i=0; i<mNumPaintedActors; i++)
		{
			mActors[i]->SyncWithLimits(this->GetPaintingLimits(),mPaintVar);
			mActors[i]->ColorByArrayComponent(0,mPaintVar+mPaintOffset); 
			mActors[i]->SetScalarVisibility(true);
		}
	}
	else
	{
		for(i=0; i<mNumPaintedActors; i++)
		{
			mActors[i]->SetScalarVisibility(false);
		}
		if(mAlwaysPainted) this->Show(false);
	}
}


bool iPaintedViewInstance::IsPainted() const
{
	return (this->IsConnectedToPaintingData() && mPaintVar>=0 && mPaintVar<this->GetPaintingLimits()->GetNumVars());
}


bool iPaintedViewInstance::SyncWithDataBody()
{
	if(mPaintVar >= this->GetPaintingLimits()->GetNumVars())
	{
		this->SetPaintVar(-1);
	}
	this->UpdatePainting();
	return true;
}


bool iPaintedViewInstance::SyncWithLimitsBody(int var, int mode)
{
	if(var == mPaintVar)
	{
		this->UpdatePainting();
	}
	return true;
}


iDataLimits* iPaintedViewInstance::GetPaintingLimits() const
{
	iDataLimits *lim = this->GetViewModule()->GetReader()->GetLimits(this->Owner()->GetPaintingDataType());
	IASSERT(lim);
	return lim;
}


iPaintedViewSubject::iPaintedViewSubject(iObject *parent, const iString &fname, const iString &sname, iViewModule *vm, const iDataType &type, const iDataType &pdt, unsigned int flags, int minsize, int maxsize) : iActorViewSubject(parent,fname,sname,vm,type,flags,minsize,maxsize), mPaintingDataType(pdt),
	iViewSubjectPropertyConstructMacro(Int,iPaintedViewInstance,Palette,p),
	iViewSubjectPropertyConstructMacro(Int,iPaintedViewInstance,PaintVar,pv)
{
	PaintVar.AddFlag(iProperty::_FunctionsAsIndex);
}


iPaintedViewSubject::~iPaintedViewSubject()
{
}

