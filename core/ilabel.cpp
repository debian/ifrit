/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ilabel.h"


#include "idatareader.h"
#include "iedition.h"
#include "ierror.h"
#include "irendertool.h"
#include "itextactor.h"
#include "iviewmodule.h"

//
//  Templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


//
//  Main class
//
iLabel* iLabel::New(iViewModule *vm)
{
	static iString LongName("Label");
	static iString ShortName("lb");

	IASSERT(vm);
	return new iLabel(vm,LongName,ShortName);
}


iLabel::iLabel(iViewModule *vm, const iString &fname, const iString &sname) : iViewModuleTool(vm,fname,sname),
	iObjectConstructPropertyMacroS(String,iLabel,Name,n),
	iObjectConstructPropertyMacroS(String,iLabel,Unit,u),
	iObjectConstructPropertyMacroS(Float,iLabel,Offset,o),
	iObjectConstructPropertyMacroS(Float,iLabel,Scale,s),
	iObjectConstructPropertyMacroS(Int,iLabel,NumDigits,d)
{
	mName = "Record";
	mUnit = "";
	mScale = 1.0;
	mOffset = 0.0;
	mNumDigits = 2;

	mLeftJustification = false;

	//
	//  Text label actors
	//
	mActor = iTextActor::New(this->GetViewModule()->GetRenderTool()); IERROR_CHECK_MEMORY(mActor);
	mActor->SetText("");
	mActor->SetJustification(1,1);
	mActor->SetPosition(0.95,0.9);
	mActor->VisibilityOff();
	mActor->PickableOff();
	this->GetViewModule()->GetRenderTool()->AddObject(mActor);

	iEdition::ApplySettings(this);
}


iLabel::~iLabel()
{
	this->GetViewModule()->GetRenderTool()->RemoveObject(mActor);
	mActor->Delete();
}


void iLabel::Update()
{
	if(this->GetViewModule()->GetReader()->IsFileAnimatable())
	{
		iString s;
		int r = this->GetViewModule()->GetReader()->GetRecordNumber();
		float v = 0.0;
		iString unit(mUnit);
		if(fabs(mScale) > 0.0) v = mScale*(r-mOffset); else
		{
			unit.Clear();
			if(this->GetViewModule()->GetReader()->GetFileSetRoot().EndsWith("1.")) r += 10000;
			v = 1.0e4/((r>0)?r:1)-1.0;
		}
		//
		//  Auto unit expansion
		//
		if(!unit.IsEmpty() && unit[0]=='.')
		{
			//
			//  Use SI prefixes (from wikipedia.org)
			//
			if(v > 1.0e24)  { v *= 1.0e-24; unit.Replace(0,1,"Y"); } else
			if(v > 1.0e21)  { v *= 1.0e-21; unit.Replace(0,1,"Z"); } else
			if(v > 1.0e18)  { v *= 1.0e-18; unit.Replace(0,1,"E"); } else
			if(v > 1.0e15)  { v *= 1.0e-15; unit.Replace(0,1,"P"); } else
			if(v > 1.0e12)  { v *= 1.0e-12; unit.Replace(0,1,"T"); } else
			if(v > 1.0e9)   { v *= 1.0e-9;  unit.Replace(0,1,"G"); } else
			if(v > 1.0e6)   { v *= 1.0e-6;  unit.Replace(0,1,"M"); } else
			if(v > 1.0e3)   { v *= 1.0e-3;  unit.Replace(0,1,"k"); } else
			if(v>0.0 && v<1.0e-23) { v *= 1.0e24;  unit.Replace(0,1,"y"); } else
			if(v>0.0 && v<1.0e-20) { v *= 1.0e21;  unit.Replace(0,1,"z"); } else
			if(v>0.0 && v<1.0e-17) { v *= 1.0e18;  unit.Replace(0,1,"a"); } else
			if(v>0.0 && v<1.0e-14) { v *= 1.0e15;  unit.Replace(0,1,"f"); } else
			if(v>0.0 && v<1.0e-11) { v *= 1.0e12;  unit.Replace(0,1,"p"); } else
			if(v>0.0 && v<1.0e-8)  { v *= 1.0e9;   unit.Replace(0,1,"n"); } else
			if(v>0.0 && v<1.0e-5)  { v *= 1.0e6;   unit.Replace(0,1,"u"); } else
			if(v>0.0 && v<1.0e-2)  { v *= 1.0e3;   unit.Replace(0,1,"m"); } else
			unit = unit.Part(1);

		}

		iString format = "%-." + iString::FromNumber(mNumDigits) + "f";
		iString num = iString::FromNumber(v,format.ToCharPointer());
		//
		//  Beatify
		//
		int i = num.Find('.');
		if(i == -1) i = num.Length();
		while(i > 3)
		{
			i -= 3;
			num = num.Part(0,i) + "," + num.Part(i);
		}
		//
		//  Form label
		//
		s = mName + "=" + num + unit;
		mActor->SetText(s);
	} 
	else
	{
		mActor->SetText("");
	}
}


void iLabel::JustifyLeft(bool s)
{
	if(s != mLeftJustification)
	{
		mLeftJustification = s;
		mActor->MoveJustification(s?-1:1);
	}
}


bool iLabel::SetName(const iString& s)
{
	mName = s;
	this->Update();
	return true;
}


bool iLabel::SetUnit(const iString& s)
{ 
	mUnit = s; 
	this->Update();
	return true;
}


bool iLabel::SetScale(float v)
{
	mScale = v;
	this->Update();
	return true;
}


bool iLabel::SetOffset(float v)
{
	mOffset = v;
	this->Update();
	return true;
}


bool iLabel::SetNumDigits(int v)
{
	if(v>=0 && v<=7)
	{
		mNumDigits = v;
		this->Update();
		return true;
	}
	else return false;
}


bool iLabel::ShowBody(bool s)
{
	mActor->SetVisibility(s?1:0);
	this->Update();
	return true;
}
