/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ireplicatedactor.h"


#include "iarray.h"
#include "iclipplane.h"
#include "ierror.h"
#include "iviewmodule.h"
#include "iviewsubject.h"

#include <vtkMapperCollection.h>
#include <vtkMatrix4x4.h>
#include <vtkTransform.h>

//
//  Templates
//
#include "iarray.tlh"


//
//  We need to take over Device to know which mapper to use on all replicas
//
class iReplicatedActorDevice: public vtkActor 
{
	
	friend class iReplicatedActor;

private:
	
	struct Replica
	{
		vtkActor *Actor;
		int Pos[3];
		Replica(){ Actor = 0; Pos[0] = Pos[1] = Pos[2] = 0; }
	};

	static iReplicatedActorDevice* New(iReplicatedActor *parent)
	{
		return new iReplicatedActorDevice(parent);
	}

	virtual void Render(vtkRenderer *ren, vtkMapper *mapper)
	{
		int i;

		for(i=0; i<mReplicas.Size(); i++)
		{
			//
			//  Keep the replica up-to-date
			//
			if(mReplicas[i].Actor->GetMTime() < mParent->GetMTime())
			{
				mReplicas[i].Actor->ShallowCopy(mParent);
				mReplicas[i].Actor->SetPosition(2.0*mReplicas[i].Pos[0],2.0*mReplicas[i].Pos[1],2.0*mReplicas[i].Pos[2]);
				mReplicas[i].Actor->Modified(); // Actor is not modifed on ShallowCopy
			}
			mReplicas[i].Actor->Render(ren,mapper);
		}
	}

	void CreateReplica(int i, int j, int k)
	{
		mReplicas.Add(Replica());

		Replica &tmp = mReplicas[mReplicas.MaxIndex()];
		tmp.Pos[0] = i;
		tmp.Pos[1] = j;
		tmp.Pos[2] = k;
		tmp.Actor = vtkActor::New(); IERROR_CHECK_MEMORY(tmp.Actor);
		tmp.Actor->SetPosition(2.0*tmp.Pos[0],2.0*tmp.Pos[1],2.0*tmp.Pos[2]);
		tmp.Actor->SetProperty(this->GetProperty());
	}

	//
	//  Delete unneeded actors
	//
	void DeleteReplicas(int numReplicas[6])
	{
		int k;
	
		for(k=0; k<mReplicas.Size(); k++)
		{
			if(mReplicas[k].Pos[0]<-numReplicas[0] || mReplicas[k].Pos[0]>numReplicas[1] || mReplicas[k].Pos[1]<-numReplicas[2] || mReplicas[k].Pos[1]>numReplicas[3] || mReplicas[k].Pos[2]<-numReplicas[4] || mReplicas[k].Pos[2]>numReplicas[5])
			{
				mReplicas[k].Actor->Delete();
				mReplicas.Remove(k);
				k--;
			}
		}
	}

	iReplicatedActorDevice(iReplicatedActor *parent)
	{
		mParent = parent;
		IASSERT(parent);
	
		vtkMatrix4x4 *m = vtkMatrix4x4::New(); IERROR_CHECK_MEMORY(m);
		this->SetUserMatrix(m);
		m->Delete();

		this->CreateReplica(0,0,0);
	}

	virtual ~iReplicatedActorDevice()
	{
		while(mReplicas.Size() > 0) mReplicas.RemoveLast().Actor->Delete();
	}

	iArray<Replica> mReplicas;
	iReplicatedActor *mParent;
};


//
//  Main class
//
iReplicatedActor* iReplicatedActor::New(iViewSubject *owner)
{
	IASSERT(owner);
	return new iReplicatedActor(owner);
}


iReplicatedActor::iReplicatedActor(iViewSubject *owner) : iActor(owner->GetViewModule()->GetClipPlane()->GetPlanes()), iReplicatedElement(false)
{
	//
	//  Must create property first - it is not created by default
	//
	IASSERT(this->GetProperty());

	//
	//  Substitute the device
	//
	this->Device->Delete();
	this->Device = mRealDevice = iReplicatedActorDevice::New(this); IERROR_CHECK_MEMORY(mRealDevice);
	this->ReplicateAs(owner);
}


iReplicatedActor::~iReplicatedActor()
{
}


double* iReplicatedActor::GetBounds()
{
	int i;

	iActor::GetBounds();

	if(this->Bounds!=0 && mReplicatedBoundsMTime<this->BoundsMTime)
	{
		mReplicatedBoundsMTime.Modified();
		for(i=0; i<3; i++)
		{
			this->Bounds[2*i+0] -= 2.0*mReplicationFactors[2*i+0];
			this->Bounds[2*i+1] += 2.0*mReplicationFactors[2*i+1];
		}
	}

	return this->Bounds;
}


void iReplicatedActor::Render(vtkRenderer *ren, vtkMapper *m)
{
	iActor::Render(ren,m);
	//
	//  Update time
	//
	this->EstimatedRenderTime *= (mReplicationFactors[0]+mReplicationFactors[1])*(mReplicationFactors[2]+mReplicationFactors[3])*(mReplicationFactors[4]+mReplicationFactors[5]);
}


void iReplicatedActor::UpdateReplicasBody()
{
	int i, j, k;

	//
	//  Add needed actors
	//
	for(k=-mReplicationFactors[4]; k<=mReplicationFactors[5]; k++)
	{
		for(j=-mReplicationFactors[2]; j<=mReplicationFactors[3]; j++)
		{
			for(i=-mReplicationFactors[0]; i<=mReplicationFactors[1]; i++)
			{
				if(i<-mOldReplicationFactors[0] || i>mOldReplicationFactors[1] || j<-mOldReplicationFactors[2] || j>mOldReplicationFactors[3] || k<-mOldReplicationFactors[4] || k>mOldReplicationFactors[5])
				{
					mRealDevice->CreateReplica(i,j,k);
				}
			}
		}
	}

	//
	//  Delete unneeded actors
	//
	mRealDevice->DeleteReplicas(mReplicationFactors);

	//
	//  Need to redo bounds
	//
	this->BoundsMTime.Modified();
}

