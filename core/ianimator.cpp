/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
// Implementation of ianimator.h
//

#include "ianimator.h"


#include "iboundingbox.h"
#include "icolorbar.h"
#include "icrosssectionviewsubject.h"
#include "idatareader.h"
#include "idirectory.h"
#include "ierror.h"
#include "ifile.h"
#include "iimagecomposer.h"
#include "ilabel.h"
#include "imath.h"
#include "imonitor.h"
#include "ishell.h"
#include "isystem.h"
#include "iviewmodule.h"
#include "iviewmoduleeventobservers.h"
#include "iviewobject.h"

#include <vtkCamera.h>
#include <vtkMath.h>
#include <vtkImageBlend.h>
#include <vtkPolyData.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>


using namespace iParameter;

//
//  Templates
//
#include "iarray.tlh"
#include "iproperty.tlh"


#define DR this->GetViewModule()->GetReader()
#define CAM this->GetViewModule()->GetRenderer()->GetActiveCamera()

#define RAN_U (2.0*vtkMath::Random()-1.0)
#define RAN_N (1.4142*tan(2.0*RAN_U/3.1415926))
#define LEN(x) (sqrt(x[0]*x[0]+x[1]*x[1]+x[2]*x[2]))


//
//  iAnimator class
//
iAnimator* iAnimator::New(iViewModule *vm)
{
	static iString LongName("Animator");
	static iString ShortName("a");

	IASSERT(vm);

	return new iAnimator(vm,LongName,ShortName); // non-inheritable, so no need to use Object Factory
}


iAnimator::iAnimator(iViewModule *vm, const iString &fname, const iString &sname) : iObject(vm,fname,sname), iViewModuleComponent(vm),
	iObjectConstructPropertyMacroS(Int,iAnimator,Style,s),
	iObjectConstructPropertyMacroS(Int,iAnimator,NumberOfFrames,nf),
	iObjectConstructPropertyMacroS(Int,iAnimator,NumberOfBlendedFrames,nb),
	iObjectConstructPropertyMacroS(Int,iAnimator,NumberOfTransitionFrames,nt),
	iObjectConstructPropertyMacroS(Float,iAnimator,Phi,dp),
	iObjectConstructPropertyMacroS(Float,iAnimator,Theta,dt),
	iObjectConstructPropertyMacroS(Float,iAnimator,Zoom,dz),
	iObjectConstructPropertyMacroS(Float,iAnimator,Roll,dr),
	iObjectConstructPropertyMacroS(String,iAnimator,TitlePageFile,tf),
	iObjectConstructPropertyMacroS(String,iAnimator,LogoFile,lf),
	iObjectConstructPropertyMacroS(Int,iAnimator,NumberOfTitlePageFrames,tnf),
	iObjectConstructPropertyMacroS(Int,iAnimator,NumberOfTitlePageBlendedFrames,tbf),
	iObjectConstructPropertyMacroS(Int,iAnimator,LogoLocation,ll),
	iObjectConstructPropertyMacroS(Float,iAnimator,LogoOpacity,lo),
	iObjectConstructPropertyMacroS(Int,iAnimator,Debug,d),
	iObjectConstructPropertyMacroA(iAnimator,Reset,r),
	iObjectConstructPropertyMacroA(iAnimator,Continue,c),
	iObjectConstructPropertyMacroQ(Int,iAnimator,CurrentFrame,cf),
	iObjectConstructPropertyMacroQ(Int,iAnimator,CurrentRecord,cr)
{
	mRenderMode = RenderMode::Self;

	mStarted = mStartedRender = false;
	mDebug = 0;

	mNewRec = false;
	mCurRec = mPrevRec = -1;
	mCurFrame = mTotFrame = 0;

	mState.Style = 1;
	mState.NumberOfFrames = 1;
	mState.NumberOfBlendedFrames = 0;
	mState.NumberOfTransitionFrames = 0;
	mState.Phi = 1.0;
	mState.Theta = 0.0;
	mState.Zoom = 1.0;
	mState.Roll = 0.0;

	mNumberOfTitlePageFrames = 0;
	mNumberOfTitlePageBlendedFrames = 0;
	mLogoOpacity = 0.5;
	mLogoLocation = 0;

	mRandStep = 0.03;
	mSeed = 12345;

	mLeader = 0;
}


iAnimator::~iAnimator()
{
	this->RemoveAllFollowers();
	if(mLeader != 0) mLeader->RemoveFollower(this);

	while(mBlenderBase.Size() > 0) delete mBlenderBase.RemoveLast();
}


//
//  This is the main driver
//
void iAnimator::Animate()
{
	while(this->CallContinue());
}


bool iAnimator::CallReset()
{
	mState.Style = 0;
	mState.NumberOfFrames = 1;
	mState.Phi = 0.0;
	mState.Theta = 0.0;
	mState.Zoom = 1.0;
	mState.Roll = 0.0;
	mState.NumberOfBlendedFrames = 0;
	mState.NumberOfTransitionFrames = 0;

	return true;
}


//
//  Keep animating until stopped
//
bool iAnimator::CallContinue()
{
	if(!mStarted)
	{
		this->Start();
	}

	if(this->Frame(true))
	{
		return true;
	}
	else
	{
		this->Stop();
		return false;
	}
}


int iAnimator::GetCurrentFrame() const
{
	return mCurFrame;
}


int iAnimator::GetCurrentRecord() const
{
	return mCurRec;
}


//
//  Start the animator
//
void iAnimator::Start()
{
	iMonitor h(this);

	if(mStarted)
	{
		h.PostError("Attempting to start an already started animation.");
		return;
	}

	//
	//  Are we using a script?
	//
	this->SaveInternalState();

	//
	//  Disactivate Progress Dialog
	//
	iRenderEventObserver::BlockRenderEventObservers(true);

	this->GetViewModule()->StartAnimation();
	mStarted = true;

	int i;
	for(i=0; i<mFollowers.Size(); i++) mFollowers[i]->Start();
}


//
//  Stop animation
//
void iAnimator::Stop()
{
	iMonitor h(this);

	int i;
	for(i=0; i<mFollowers.Size(); i++) mFollowers[i]->Stop();

	if(!mStarted)
	{
		h.PostError("Attempting to finish a never started animation.");
		return;
	}

	this->GetViewModule()->FinishAnimation();

	//
	//  Activate Progress Dialog
	//
	iRenderEventObserver::BlockRenderEventObservers(false);

	//
	//  Reset to initial state
	//
	mStartedRender = false;
	this->GetViewModule()->GetLabel()->JustifyLeft(false);

	//
	//  Restore the original state after execution of a script - must do it after reset()
	//  so that projection is Set properly in case it was changed in a script before the
	//  first render
	//
	this->RestoreState(true);
	mStarted = false;
}


void iAnimator::AddFollower(iAnimator *f)
{
	if(f!=0 && f!=this)
	{
		mRenderMode = RenderMode::All;
		mFollowers.AddUnique(f);
		f->SetLeader(this);
	}
}


void iAnimator::RemoveFollower(iAnimator *f)
{
	if(f != 0)
	{
		mFollowers.Remove(f);
		f->SetLeader(0);
		if(mFollowers.Size() == 0) 	mRenderMode = RenderMode::Self;
	}
}


void iAnimator::SetLeader(iAnimator *l)
{
	if(l != this)
	{
		mLeader = l;
	}
}


void iAnimator::RemoveAllFollowers()
{
	int i;
	for(i=0; i<mFollowers.Size(); i++) mFollowers[i]->SetLeader(0);
	mFollowers.Clear();
	mRenderMode = RenderMode::Self;
}


//
//  Internal functions
//
bool iAnimator::SetDebug(int s) 
{ 
	mDebug = s;
	this->GetViewModule()->SetDebugMode(s > 1);
	return true;
}


void iAnimator::ResetCurrentFile()
{
	mNewRec = true;
	mPrevRec = mCurRec;
	mCurRec = DR->GetRecordNumber();
	mCurFrame = mTotFrame = 0;
}


void iAnimator::SaveInternalState()
{
	mState.isBoundingBox = this->GetViewModule()->GetBoundingBox()->IsVisible();
	mState.isTimeLabel = this->GetViewModule()->GetLabel()->IsVisible();
	mState.isColorBar = this->GetViewModule()->GetColorBar()->IsVisible();

	if(DR != 0) mState.currec = this->GetViewModule()->GetReader()->GetRecordNumber();

	mState.cameraProjection = CAM->GetParallelProjection();
	CAM->GetPosition(mState.cameraPosition);
	CAM->GetFocalPoint(mState.cameraFocalPoint);
	CAM->GetViewUp(mState.cameraViewUp);
	mState.cameraParallelScale = CAM->GetParallelScale();
	CAM->GetClippingRange(mState.cameraClippingRange);

	mState2 = mState;
}


void iAnimator::RestoreState(bool with_camera)
{
	mState = mState2;

	if(DR!=0 && mDebug<2)
	{
		DR->LoadRecord(mState.currec,0,true);
	}

	this->GetViewModule()->GetBoundingBox()->Show(mState.isBoundingBox );
	this->GetViewModule()->GetColorBar()->Show(mState.isColorBar);
	this->GetViewModule()->GetLabel()->Show(mState.isTimeLabel);

	if(with_camera)
	{
		CAM->SetParallelProjection(mState.cameraProjection);
		CAM->SetPosition(mState.cameraPosition);
		CAM->SetFocalPoint(mState.cameraFocalPoint);
		CAM->SetViewUp(mState.cameraViewUp);
		CAM->SetParallelScale(mState.cameraParallelScale);
	}
}


bool iAnimator::SetStyle(int ma)
{ 
	if(ma>=0 && ma<3) 
	{
		mState.Style = ma; 
		return true;
	}
	else return false;
}


bool iAnimator::SetNumberOfFrames(int na)
{
	if(na > 0)
	{
		mState.NumberOfFrames = na;
		return true;
	}
	else return false;
}


bool iAnimator::SetPhi(float va)
{
	mState.Phi = va; 
	return true;
}


bool iAnimator::SetTheta(float va)
{
	mState.Theta = va; 
	return true;
}


bool iAnimator::SetZoom(float va)
{
	mState.Zoom = va; 
	return true;
}


bool iAnimator::SetRoll(float va)
{
	mState.Roll = va; 
	return true;
}


bool iAnimator::SetNumberOfBlendedFrames(int na)
{
	if(na >= 0)
	{
		mState.NumberOfBlendedFrames = na; 
		return true;
	}
	else return false;
}


bool iAnimator::SetNumberOfTransitionFrames(int na)
{
	if(na >= 0)
	{
		mState.NumberOfTransitionFrames = na; 
		return true;
	}
	else return false;
}


bool iAnimator::SetNumberOfTitlePageFrames(int n)
{
	if(n >= 0)
	{
		mNumberOfTitlePageFrames = n; 
		return true;
	}
	else return false;
}


bool iAnimator::SetNumberOfTitlePageBlendedFrames(int n)
{
	if(n >= 0)
	{
		mNumberOfTitlePageBlendedFrames = n; 
		return true;
	}
	else return false;
}


bool iAnimator::SetLogoLocation(int n)
{
	if(n>=0 && n<5)
	{
		mLogoLocation = n; 
		return true;
	}
	else return false;
}


bool iAnimator::SetLogoOpacity(float v)
{
	mLogoOpacity = v; 
	return true;
}


bool iAnimator::SetLogoFile(const iString& s)
{
	return true;
	bool ok = mLogoImage.LoadFromFile(s);
	if(ok) mLogoFile = s; else 
	{ 
		mLogoFile.Clear();
		mLogoImage.Clear();
	}
	return ok;
}


bool iAnimator::SetTitlePageFile(const iString& s)
{
	return true;
	bool ok = mTitlePageImage.LoadFromFile(s);
	if(ok) mTitlePageFile = s; else 
	{
		mTitlePageFile.Clear();
		mTitlePageImage.Clear();
	}
	return ok;
}


bool iAnimator::Frame(bool dumpImage)
{
	static float Pi = 3.1415927;
	int i;
	iMonitor h(this);

	mNewRec = false;

	if(!mStartedRender)
	{
		if(!DR->IsFileAnimatable())
		{
			h.PostError("File is not animatable.");
			return false;
		}
		this->GetViewModule()->GetLabel()->JustifyLeft(true);
		
		vtkMath::RandomSeed(mSeed);

		if(mState.Style == 2)
		{
			wData.dphl0 = mState.Phi;
			wData.dthl0 = mState.Theta;
			wData.r = 0.0;
			wData.ramp = RAN_N;
		}
		
		if(mState.Style == 3)
		{
			for(i=0; i<3; i++)
			{
				wData.xc1[i] = 0.5*RAN_U;
				wData.xc2[i] = 0.5*RAN_U;
			}
			CAM->SetParallelProjection(0);
			CAM->GetPosition(wData.x);
			if(mState.cameraProjection == 1)
			{
				for(i=0; i<3; i++) wData.x[i] = 0.5*wData.x[i];
				CAM->SetPosition(wData.x);
			}
			else
			{
				CAM->GetFocalPoint(wData.xc1);
			}
			CAM->SetFocalPoint(wData.xc1);
			float d = LEN(wData.x);
			wData.v[0] = d*0.5*RAN_U;
			wData.v[1] = d*0.5*RAN_U;
			wData.v[2] = 0.0;
			wData.t = 0.0;
			wData.dt0 = 0.1*Pi;
			wData.dt = wData.dt0;
		}
		
		mCurFrame = mTotFrame = 0;

		mPrevRec = -1;
		mCurRec = DR->GetRecordNumber();
		mStartedRender = true;
	} 

	if(mCurFrame == mState.NumberOfFrames)
	{
		bool ret = DR->LoadRecord(-1,1,mDebug>1);
		if(h.IsStopped() && !ret)
		{
			h.Reset();
			return false;
		}
		this->GetViewModule()->UpdateLabel();
		mNewRec = true;
		mPrevRec = mCurRec;
		mCurRec = DR->GetRecordNumber();
		mCurFrame = 0;
	} 
		
	if(h.IsStopped()) 
	{
		return false;
	}

	mCurFrame++;
	mTotFrame++;

	//
	//	Add transformations for rotate & tumble
	//	
	if(mState.Style==1 || mState.Style==2)
	{		
		CAM->Azimuth(-mState.Phi);
		CAM->Elevation(-mState.Theta);
		CAM->Zoom(mState.Zoom);
		CAM->Roll(mState.Roll);
		CAM->OrthogonalizeViewUp();

		if(mState.Style == 2)
		{
			wData.r = wData.r + mRandStep;
			float cr = cos(wData.r*wData.ramp);
			float sr = sin(wData.r*wData.ramp);
			mState.Phi =  wData.dphl0*cr + wData.dthl0*sr;
			mState.Theta =  wData.dthl0*cr - wData.dphl0*sr;
			if(wData.r > 1.0)
			{
				wData.r = 0.0;
				wData.ramp = RAN_N;
				wData.dphl0 =  mState.Phi;
				wData.dthl0 =  mState.Theta;
			}
		}
	}

	//
	//  Image data holder
	//
	iStereoImageArray images;

	//
	//  Create the primary image set
	//
	this->GetViewModule()->CreateImages(images);
	if(h.IsStopped()) return false;

	//
	//  Title page
	//
	if(mTotFrame==1 && dumpImage && !mTitlePageImage.IsEmpty() && mDebug==0)
	{
		iImage tmp = mTitlePageImage;
		//
		//  We must update composer here because there is no automatic way to call
		//  composer->Update() when vtkRenderWindow changes its size (it does not invoke an event).
		//
		this->GetViewModule()->GetShell()->GetImageComposer()->Update();
		tmp.Scale(this->GetViewModule()->GetFullImageWidth(),this->GetViewModule()->GetFullImageHeight());

		iStereoImageArray tmpset;
		tmpset.Copy(images);

		int n;
		for(n=0; n<mNumberOfTitlePageFrames; n++)
		{
			tmpset.Fill(tmp);
			this->WriteImages(tmpset);
			if(h.IsStopped()) return false;
		}

		for(n=0; n<mNumberOfTitlePageBlendedFrames; n++)
		{
			//
			//  Dissolve it; image already contains the correct image
			//
			tmpset.Copy(images);
			for(i=0; i<images.Size(); i++) tmpset[i].Blend(tmp,float(n)/mNumberOfTitlePageBlendedFrames);

			this->WriteImages(tmpset);
			if(h.IsStopped()) return false;
		}
	}

	//
	//  Transition effects
	//
	bool doTransitionFrames = dumpImage && mTotFrame>0 && mPrevRec>0 && mCurFrame<=mState.NumberOfTransitionFrames && mState.NumberOfTransitionFrames>0 && mDebug==0;
	if(doTransitionFrames)
	{
		iStereoImageArray tmpset;

		//
		//  Objects for transition effects (blending with previous record)
		//
		DR->LoadRecord(mPrevRec,0,false);
		if(h.IsStopped()) return false;

		this->GetViewModule()->UpdateLabel();
		this->GetViewModule()->CreateImages(tmpset);
		if(h.IsStopped()) return false;

		float ops = float(mCurFrame)/mState.NumberOfTransitionFrames;

		DR->LoadRecord(mCurRec,0,false);	
		if(h.IsStopped()) return false;
	}

	//
	//  Blending of images
	//
	bool doBlendedFrames = dumpImage && mTotFrame >0 && mState.NumberOfBlendedFrames>0 && mDebug==0;
	if(doBlendedFrames)
	{
		int k;
		//
		//  Update the image list
		//
		while(mBlenderBase.Size() < mState.NumberOfBlendedFrames)
		{
			iStereoImageArray *ptmparr = new iStereoImageArray; IERROR_CHECK_MEMORY(ptmparr);
			ptmparr->Copy(images);
			mBlenderBase.Add(ptmparr);
		}
		while(mBlenderBase.Size() > mState.NumberOfBlendedFrames)
		{
			delete mBlenderBase.RemoveLast();
		}

		delete mBlenderBase[0];
		for(k=0; k<mBlenderBase.MaxIndex(); k++) mBlenderBase[k]->Copy(*mBlenderBase[k+1]);
		mBlenderBase[mBlenderBase.MaxIndex()]->Copy(images);

		//
		//  Make sure that all the arrays are of the same size
		//
		for(k=0; k<mBlenderBase.MaxIndex(); k++)
		{
			while(mBlenderBase[k]->Size() > images.Size()) mBlenderBase[k]->RemoveLast();
			while(mBlenderBase[k]->Size() < images.Size()) mBlenderBase[k]->Add(images[mBlenderBase[k]->Size()]);
		}

		//
		//  Blend the arrays
		//
		int n = 1;
		float ops;
		images.Copy(*mBlenderBase[0]);
		for(k=1; k<mBlenderBase.Size(); k++)
		{
			n += (k+1);
			ops = float(k+1)/n;
			for(i=0; i<images.Size(); i++)
			{
				images[i].Blend((*mBlenderBase[k])[i],ops);
			}
		}
	}
	
	for(i=0; i<mFollowers.Size(); i++)
	{
		if(!mFollowers[i]->Frame(false)) break;
	}

	if(mDebug==0 && dumpImage)
	{
		this->WriteImages(images);
	}

	return !h.IsStopped();
}


//
//  Dump an image array, optionally adding a logo
//
void iAnimator::WriteImages(iStereoImageArray &images)
{
	if(images.Size()>0 && mDebug==0 && !mLogoImage.IsEmpty())
	{
		//
		//  If the logo is more than 20% of the image, scale it down.
		//  Use tmp as a temp storage
		//
		iImage tmp = mLogoImage;
		if(tmp.Width()>images[0].Width()/5 || tmp.Height()>images[0].Height()/5)
		{
			tmp.Scale(images[0].Width()/5,images[0].Height()/5);
		}

		if(tmp.Width()>=2 && tmp.Height()>=2)
		{
			//
			//  tmp is now the proper logo image
			//
			int i, ioff, joff;
			//
			//  Where do we place the logo?
			//
			ioff = tmp.Width()/5;
			joff = tmp.Height()/5;
			switch(mLogoLocation)
			{
			case 1:
				{
					//  upper right corner 
					ioff = images[0].Width() - tmp.Width() - ioff;
					joff = images[0].Height() - tmp.Height() - joff;
					break;
				}
			case 2:
				{
					//  lower left right corner 
					break;
				}
			case 3:
				{
					//  lower right corner 
					ioff = images[0].Width() - tmp.Width() - ioff;
					break;
				}
			default:
				{
					//  upper left corner - the default choice
					joff = images[0].Height() - tmp.Height() - joff;
					break;
				}
			}
			for(i=0; i<images.Size(); i++) images[i].Overlay(ioff,joff,tmp,mLogoOpacity);
		}
	}

	iMonitor h(this);
	this->GetViewModule()->WriteImages(images,ImageType::MovieFrame);
}

