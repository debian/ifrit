/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
// A self-saveable/loadable property for iObject class
//

#ifndef IPROPERTY_H
#define IPROPERTY_H


#include "iarray.h"
#include "itype.h"

class iObject;
class iPiecewiseFunction;
//class iShell;


//
//  Base class for all object properties (now sure we need it - keep for now)
//
class iProperty
{

	friend class iObject;

public:

	typedef size_t Key;

	enum Flag
	{
		_OptionalInState	= 0x0001,
		_FunctionsAsIndex	= 0x0002,
		_DoNotSaveInState	= 0x0004,
		_DoNotIncludeInCopy	= 0x0008,
		_DoNotRequestRender	= 0x0010
	};

	virtual ~iProperty();

	inline const iString& LongName() const { return mLongName; }
	inline const iString& ShortName() const { return mShortName; }
	inline const iString& Tag() const { return mTag; } // for ordering

	iObject* Owner() const { return mOwner; }
	const iString HelpTag() const;

	void AddFlag(Flag f);
	bool TestFlag(Flag f) const;

	static const iProperty* FindPropertyByKey(const Key& key);
	const Key& GetKey() const { return mKey; }

protected:

	iProperty(iObject *owner, const iString &fname, const iString &sname, int rank);

	void RequestRender() const;
	bool IsImmediatePost() const;

private:

	iObject *mOwner;
	const int mRank;
	const iString mLongName, mShortName, mTag;
	int mFlags;

	//
	//  Registry functionality
	//
	const Key mKey;
	static Key KeyCounter;
	static iGenericOrderedArray<const iProperty*,Key> mRegistry;
	static const Key& EntryLookup(const iProperty* const &ptr);

	iProperty(const iProperty&);		// Not implemented.
	void operator=(const iProperty&);			// Not implemented.

	//
	//  This is a part of message subsystem for thread-safe execution.
	//  Functions begining with underscore can be called from the driver thread.
	//  Functions ending with underscore should be called from the slave thread only.
	//
public:

	virtual bool PushPostedValue_() const = 0;
	virtual bool HasPostedRequests() const = 0;

protected:

	bool _RequestPush() const;
};


//
//  Abstract variable property
//
class iPropertyVariable : public iProperty
{

public:

	virtual ~iPropertyVariable();

	virtual iType::TypeId GetType() const = 0;
	virtual const iString& GetTypeName() const = 0;
	virtual iString ConvertToString(bool exact) const = 0;

	virtual void SaveStateToString(iString &str, bool exact) const = 0;
	virtual bool LoadStateFromString(const iString &str) = 0;

	virtual int Size() const = 0;
	virtual bool IsFixedSize() const = 0;

	virtual bool Copy(const iPropertyVariable *p) = 0;
	virtual bool CopyElement(int iy, int ix) = 0;

protected:

	iPropertyVariable(iObject *owner, const iString &fname, const iString &sname, int rank);

	virtual bool Resize(int num, bool force = false) = 0;
};


//
//  Abstract function property (not a variable)
//
class iPropertyFunction : public iProperty
{

public:

	virtual ~iPropertyFunction();

	virtual iType::TypeId GetReturnType() const = 0;
	virtual const iString& GetReturnTypeName() const = 0;

	virtual int NumArguments() const = 0;
	virtual iType::TypeId GetArgumentType(int i) const = 0;
	virtual const iString& GetArgumentTypeName(int i) const = 0;

protected:

	iPropertyFunction(iObject *owner, const iString &fname, const iString &sname, int rank);
};


//
//  Abstract variable property of a given type
//
template<iType::TypeId id>
class iPropertyDataVariable : public iPropertyVariable
{

public:

	typedef typename iType::Traits<id>::Type ArgT;

	virtual ~iPropertyDataVariable();

	virtual iType::TypeId GetType() const;
	virtual const iString& GetTypeName() const;
	virtual iString ConvertToString(bool exact) const;

	virtual void SaveStateToString(iString &str, bool exact) const;
	virtual bool LoadStateFromString(const iString &str);

	virtual bool Copy(const iPropertyVariable *p);
	virtual bool CopyElement(int iy, int ix);

	//
	//  SetValue does not cause a render, PostValue does
	//
	virtual ArgT GetValue(int i) const = 0;
	virtual bool SetValue(int i, ArgT v) const = 0;

protected:

	iPropertyDataVariable(iObject *owner, const iString &fname, const iString &sname, int rank);

	//
	//  This is a part of message subsystem for thread-safe execution.
	//  Functions begining with underscore can be called from the driver thread.
	//  Functions ending with underscore should be called from the slave thread only.
	//
public:

	bool _PostValue(int i, ArgT v) const;
	virtual bool PushPostedValue_() const;
	virtual bool HasPostedRequests() const;

	struct QueueItem
	{
		int Index;
		typename iType::Traits<id>::ContainerType Value;
	};
	mutable iArray<QueueItem> mQueue;
};


//
//  Scalar (1D) variable
//
template<iType::TypeId id>
class iPropertyScalar : public iPropertyDataVariable<id>
{

public:

	typedef typename iType::Traits<id>::Type ArgT;
	typedef bool (iObject::*SetterType)(ArgT val);
	typedef ArgT (iObject::*GetterType)() const;

	iPropertyScalar(iObject *owner, SetterType setter, GetterType getter, const iString &fname, const iString &sname, int rank = 0);

	virtual int Size() const { return 1; }
	virtual bool IsFixedSize() const { return true; }

	virtual ArgT GetValue(int i) const;
	virtual bool SetValue(int i, ArgT v) const;

protected:

	virtual bool Resize(int, bool force = false){ return false; }

private:

	SetterType mSetter;
	GetterType mGetter;
};


//
//  Fixed-size vector variable
//
template<iType::TypeId id>
class iPropertyVector : public iPropertyDataVariable<id>
{

public:

	typedef typename iType::Traits<id>::Type ArgT;
	typedef  int (iObject::*SizeType)() const;
	typedef bool (iObject::*ResizeType)(int n);
	typedef bool (iObject::*SetterType)(int i, ArgT val);
	typedef ArgT (iObject::*GetterType)(int i) const;

	iPropertyVector(iObject *owner, SetterType setter, GetterType getter, const iString &fname, const iString &sname, int dim, int rank = 0);
	iPropertyVector(iObject *owner, SetterType setter, GetterType getter, const iString &fname, const iString &sname, SizeType size, ResizeType resize = 0, int rank = 0);

	virtual int Size() const { return (this->mSize == 0) ? this->mDim : (this->Owner()->*mSize)(); }
	virtual bool IsFixedSize() const { return (this->mResize == 0); }

	virtual ArgT GetValue(int i) const;
	virtual bool SetValue(int i, ArgT v) const;

protected:

	virtual bool Resize(int num, bool force = false);

private:

	int mDim;
	SizeType mSize;
	ResizeType mResize;
	SetterType mSetter;
	GetterType mGetter;
};


//
//  Special property that correctly updates the piecewise function.
//
class iPropertyPiecewiseFunction : public iPropertyDataVariable<iType::Pair>
{

public:

	iPropertyPiecewiseFunction(iObject *owner, const iString &fname, const iString &sname, int rank = 0);
	virtual ~iPropertyPiecewiseFunction();

	void AttachFunction(iPiecewiseFunction *f);
	inline iPiecewiseFunction* Function() const { return mFunction; }

	virtual int Size() const;
	virtual bool IsFixedSize() const { return false; }

	virtual bool Copy(const iPropertyVariable *p);

	virtual iType::Traits<iType::Pair>::Type GetValue(int i) const;
	virtual bool SetValue(int i, iType::Traits<iType::Pair>::Type p) const;

protected:

	virtual bool Resize(int dim, bool force = false);

private:

	iPiecewiseFunction *mFunction;
};


//
//  Function with a given return type
//
template<iType::TypeId id>
class iPropertyTypeFunction : public iPropertyFunction
{

public:

	iPropertyTypeFunction(iObject *owner, const iString &fname, const iString &sname, int rank);
	virtual ~iPropertyTypeFunction();

	virtual iType::TypeId GetReturnType() const;
	virtual const iString& GetReturnTypeName() const;
};


//
//  Simple action with no arguments
//
class iPropertyAction : public iPropertyTypeFunction<iType::Bool>
{

public:

	typedef bool (iObject::*CallerType)();

	iPropertyAction(iObject *owner, CallerType caller, const iString &fname, const iString &sname, int rank = 0);

	virtual int NumArguments() const { return 0; }
	virtual iType::TypeId GetArgumentType(int) const { return iType::None; }
	virtual const iString& GetArgumentTypeName(int) const { static iString none; return none; }

	virtual bool CallAction() const;

private:

	CallerType mCaller;

	//
	//  This is a part of message subsystem for thread-safe execution.
	//  Functions begining with underscore can be called from the driver thread.
	//  Functions ending with underscore should be called from the slave thread only.
	//
public:

	bool _PostCall() const;
	virtual bool PushPostedValue_() const;
	virtual bool HasPostedRequests() const;

	mutable int mQueue;
};


//
//  An action with 1 argument
//
template<iType::TypeId id>
class iPropertyAction1 : public iPropertyTypeFunction<iType::Bool>
{

public:

	typedef typename iType::Traits<id>::Type ArgT;
	typedef bool (iObject::*CallerType)(ArgT v);

	iPropertyAction1(iObject *owner, CallerType caller, const iString &fname, const iString &sname, int rank = 0);

	virtual int NumArguments() const { return 1; }
	virtual iType::TypeId GetArgumentType(int i) const;
	virtual const iString& GetArgumentTypeName(int i) const;

	virtual bool CallAction(ArgT v) const;

private:

	CallerType mCaller;

	//
	//  This is a part of message subsystem for thread-safe execution.
	//  Functions begining with underscore can be called from the driver thread.
	//  Functions ending with underscore should be called from the slave thread only.
	//
public:

	bool _PostCall(ArgT v) const;
	virtual bool PushPostedValue_() const;
	virtual bool HasPostedRequests() const;

	struct QueueItem
	{
		typename iType::Traits<id>::ContainerType Value;
	};
	mutable iArray<QueueItem> mQueue;
};


//
//  An action with 2 arguments
//
template<iType::TypeId id1, iType::TypeId id2>
class iPropertyAction2 : public iPropertyTypeFunction<iType::Bool>
{

public:

	typedef typename iType::Traits<id1>::Type ArgT1;
	typedef typename iType::Traits<id2>::Type ArgT2;
	typedef bool (iObject::*CallerType)(ArgT1 v1, ArgT2 v2);

	iPropertyAction2(iObject *owner, CallerType caller, const iString &fname, const iString &sname, int rank = 0);

	virtual int NumArguments() const { return 2; }
	virtual iType::TypeId GetArgumentType(int i) const;
	virtual const iString& GetArgumentTypeName(int i) const;

	virtual bool CallAction(ArgT1 v1, ArgT2 v2) const;

private:

	CallerType mCaller;

	//
	//  This is a part of message subsystem for thread-safe execution.
	//  Functions begining with underscore can be called from the driver thread.
	//  Functions ending with underscore should be called from the slave thread only.
	//
public:

	bool _PostCall(ArgT1 v1, ArgT2 v2) const;
	virtual bool PushPostedValue_() const;
	virtual bool HasPostedRequests() const;

	struct QueueItem
	{
		typename iType::Traits<id1>::ContainerType Value1;
		typename iType::Traits<id2>::ContainerType Value2;
	};
	mutable iArray<QueueItem> mQueue;
};


//
//  A simple query with no arguments
//
template<iType::TypeId id>
class iPropertyQuery : public iPropertyTypeFunction<id>
{

public:

	typedef typename iType::Traits<id>::Type RetT;
	typedef RetT (iObject::*GetterType)() const;

	iPropertyQuery(iObject *owner, GetterType getter, const iString &fname, const iString &sname, int rank = 0);

	virtual int NumArguments() const { return 0; }
	virtual iType::TypeId GetArgumentType(int) const { return iType::None; }
	virtual const iString& GetArgumentTypeName(int) const { static iString none; return none; }

	virtual RetT GetQuery() const;

private:

	GetterType mGetter;

	//
	//  This is a part of message subsystem for thread-safe execution.
	//  Functions begining with underscore can be called from the driver thread.
	//  Functions ending with underscore should be called from the slave thread only.
	//
public:

	//
	//  Query should never modify the core, hence assume it is thread safe
	//
	virtual bool PushPostedValue_() const { return true; }
	virtual bool HasPostedRequests() const { return false; }
};


namespace iType
{
	typedef iPropertyScalar<iType::Int>			ps_int;
	typedef iPropertyScalar<iType::Bool>		ps_bool;
	typedef iPropertyScalar<iType::Float>		ps_float;
	typedef iPropertyScalar<iType::Double>		s_double;
	typedef iPropertyScalar<iType::String>		ps_string;
	typedef iPropertyScalar<iType::Pair>		ps_pair;
	typedef iPropertyScalar<iType::Color>		ps_color;
	typedef iPropertyScalar<iType::Vector>		ps_vector;

	typedef iPropertyVector<iType::Int>			pv_int;
	typedef iPropertyVector<iType::Bool>		pv_bool;
	typedef iPropertyVector<iType::Float>		pv_float;
	typedef iPropertyVector<iType::Double>		pv_double;
	typedef iPropertyVector<iType::String>		pv_string;
	typedef iPropertyVector<iType::Pair>		pv_pair;
	typedef iPropertyVector<iType::Color>		pv_color;
	typedef iPropertyVector<iType::Vector>		pv_vector;
};

#endif // IPROPERTY_H

