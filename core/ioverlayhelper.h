/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  A helper class for all 2D objects; includes basic functionality for working under
//  magnification.
//
#ifndef IOVERLAYHELPER_H
#define IOVERLAYHELPER_H


#include <vtkObjectBase.h>


#include "icolor.h"

#include "ipointermacro.h"

#include <vtkSetGet.h>

class iRenderTool;

class vtkCamera;
class vtkProperty2D;
class vtkTextProperty;
class vtkViewport;


class iOverlayHelper : public vtkObjectBase
{

	IPOINTER_AS_PART(RenderTool);

public:

	vtkTypeMacro(iOverlayHelper,vtkObjectBase);
	static iOverlayHelper* New(iRenderTool *rv = 0);

	int GetFontSize(vtkViewport *vp, float factor = 1.0f) const;
	void UpdateTextProperty(vtkViewport *vp, vtkTextProperty *prop);

	vtkCamera* GetCamera(vtkViewport *viewport) const;
	int GetRenderingMagnification() const;
	void ComputePositionShiftsUnderMagnification(int winij[2]);

	void SetFixedColor(const iColor &c){ mFixedColor = c; }
	inline const iColor& GetFixedColor() const { return mFixedColor; }
	const iColor& GetColor(vtkViewport* viewport);

	void SetAutoColor(bool s){ mIsAutoColor = s; }
	inline bool IsAutoColor() const { return mIsAutoColor; } 

protected:

	virtual ~iOverlayHelper();

private:
	
	iOverlayHelper(iRenderTool *rv);

	iColor mFixedColor;
	bool mIsAutoColor; 
	float mFontFactor;

	//
	//  Work variable
	//
	iColor wColor;
};

#endif // IOVERLAYHELPER_H
