/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


//
//  A container class that collects all ViewSubjects of the same type but accepting different DataType(s).
//
#ifndef IVIEWOBJECT_H
#define IVIEWOBJECT_H


#include "iobject.h"
#include "iviewmodulecomponent.h"


#include "iarray.h"

class iDataInfo;
class iSyncRequest;
class iViewInstance;
class iViewSubject;


//
//  Main class
//
class iViewObject : public iObject, public iViewModuleComponent
{
	
public:
	
	vtkTypeMacro(iViewObject,iObject);
	static iViewObject* New(iViewModule *parent, const iString &fname, const iString &sname);

	int GetNumberOfSubjects() const;
	iViewSubject* GetSubject(int id) const;
	iViewSubject* GetSubject(const iDataType &type) const;

	void Reset();
	float GetMemorySize() const;

	virtual void BecomeClone(iViewObject *v);

	virtual void WriteState(iString &s) const;
	virtual void RegisterWithHelpFactory() const;
	virtual const iObject* FindByName(const iString &name) const;

	virtual int DepthPeelingRequest() const;

	//
	//  Constructor helpers used by extensions
	//
	void AddSubject(iViewSubject *subject);

	virtual bool IsUsingData(const iDataType &type, bool onlyprimary) const;
	void SyncWithData(const iSyncRequest &request);

protected:
	
	virtual ~iViewObject();

	virtual void WriteHead(iString &s) const;

private:

	iViewObject(iViewModule *parent, const iString &fname, const iString &sname);

	//
	//  Members
	//
	iArray<iViewSubject*> mSubjects;

	static iViewObject* New(){ return 0; }
};

#endif // IVIEWOBJECT_H
