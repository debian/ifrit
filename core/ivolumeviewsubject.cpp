/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ivolumeviewsubject.h"


#include "iclipplane.h"
#include "icolorbar.h"
#include "icoredatasubjects.h"
#include "idatalimits.h"
#include "idatareader.h"
#include "idatasubject.h"
#include "ierror.h"
#include "imaterial.h"
#include "ipalette.h"
#include "ipalettecollection.h"
#include "ipiecewisefunction.h"
#include "ireplicatedgriddata.h"
#include "ireplicatedvolume.h"
#include "ishell.h"
#include "iviewmodule.h"
#include "iviewobject.h"
#include "ivolumedataconverter.h"
#include "ivtk.h"

#include <vtkToolkits.h>

#include <vtkFixedPointVolumeRayCastMapper.h> 
#include <vtkGPUVolumeRayCastMapper.h>
#include <vtkImageData.h>
#include <vtkRenderWindow.h>
#include <vtkVolumeProperty.h>
#include <vtkVolumeRayCastCompositeFunction.h>
#include <vtkVolumeRayCastMIPFunction.h>
#include <vtkVolumeRayCastMapper.h>
#include <vtkVolumeTextureMapper2D.h>
#include <vtkVolumeTextureMapper3D.h>

//
//  Allow for the IFrIT own support for VolumePro1000
//
#ifdef I_CUSTOM_VP1000
	#ifndef VTK_USE_VOLUMEPRO
		#define VTK_USE_VOLUMEPRO
	#endif
	#include "ivolumepromapper.h"
#else
	#ifdef VTK_USE_VOLUMEPRO
		#include <vtkVolumeProHelper.h>
	#else
		#define vtkVolumeProHelper int
	#endif
	#define iVolumeProHelper vtkVolumeProHelper
#endif


//
//  Templates
//
#include "iarray.tlh"
#include "iproperty.tlh"
#include "igenericfilter.tlh"


using namespace iParameter;


int iVolumeViewSubject::mGPUMode = 0;  // auto


namespace iVolumeViewSubject_Private
{
	class UniformGridVolumeRenderingHelper : public iVolumeRenderingHelper
	{

	public:

		UniformGridVolumeRenderingHelper(iVolumeViewInstance *parent, const iString &name) : iVolumeRenderingHelper(parent,name,false)
		{
		}

		virtual bool IsUsingData(vtkDataSet *ds)
		{
			return (ds!=0 && ds->IsA("vtkImageData")!=0);
		}

		virtual bool IsAvailable()
		{
			return true;
		}
	};


	class RayCastHelper : public UniformGridVolumeRenderingHelper
	{

	public:

		RayCastHelper(iVolumeViewInstance *parent, vtkAlgorithmOutput *port) : UniformGridVolumeRenderingHelper(parent,"Ray casting")
		{
			mMapper = vtkVolumeRayCastMapper::New(); IERROR_CHECK_MEMORY(mMapper);
			mMapper->AddObserver(vtkCommand::ProgressEvent,parent->GetViewModule()->GetRenderEventObserver());
			mMapper->IntermixIntersectingGeometryOn();
			mMapper->AutoAdjustSampleDistancesOn();
			mMapper->SetInputConnection(port);

			mCF = vtkVolumeRayCastCompositeFunction::New(); IERROR_CHECK_MEMORY(mCF);
			mCF->SetCompositeMethodToInterpolateFirst();
			
			mMF = vtkVolumeRayCastMIPFunction::New(); IERROR_CHECK_MEMORY(mMF);
			
			mMapper->SetVolumeRayCastFunction(mCF);
		}

		virtual ~RayCastHelper()
		{
			mCF->Delete();
			mMF->Delete();
			mMapper->Delete();
		}

		virtual vtkVolumeMapper* GetMapper()
		{
			return mMapper;
		}

		virtual void SetBlendMode(int m)
		{
			switch(m)
			{
			case 0:
				{
					mMapper->SetVolumeRayCastFunction(mCF);
					break;
				}
			case 1:
				{
					mMapper->SetVolumeRayCastFunction(mMF);
					break;
				}
			}
		}

	protected:

		virtual void AdjustSpacing()
		{
			if(mMapper->GetAutoAdjustSampleDistances() == 0)
			{
				mMapper->SetImageSampleDistance(mImageDownsampleFactor);
			}
			mMapper->SetSampleDistance(mDepthDownsampleFactor*mDataSpacing);
		}

	private:

		vtkVolumeRayCastMapper *mMapper;
		vtkVolumeRayCastCompositeFunction *mCF;
		vtkVolumeRayCastMIPFunction *mMF;
	};


	class Texture2DHelper : public UniformGridVolumeRenderingHelper
	{

	public:

		Texture2DHelper(iVolumeViewInstance *parent, vtkAlgorithmOutput *port) : UniformGridVolumeRenderingHelper(parent,"2D textures")
		{
			mMapper = vtkVolumeTextureMapper2D::New(); IERROR_CHECK_MEMORY(mMapper);
			mMapper->AddObserver(vtkCommand::ProgressEvent,parent->GetViewModule()->GetRenderEventObserver());
			mMapper->SetInputConnection(port);
		}

		~Texture2DHelper()
		{
			mMapper->Delete();
		}

		virtual vtkVolumeMapper* GetMapper()
		{
			return mMapper;
		}

		virtual void SetBlendMode(int m)
		{
			mMapper->SetBlendMode(m);
		}

	private:

		vtkVolumeTextureMapper2D *mMapper;
	};


	class Texture3DHelper : public UniformGridVolumeRenderingHelper
	{

	public:

		Texture3DHelper(iVolumeViewInstance *parent, vtkAlgorithmOutput *port) : UniformGridVolumeRenderingHelper(parent,"3D textures")
		{
			mMapper = vtkVolumeTextureMapper3D::New(); IERROR_CHECK_MEMORY(mMapper);
			mMapper->AddObserver(vtkCommand::ProgressEvent,parent->GetViewModule()->GetRenderEventObserver());
			mMapper->SetInputConnection(port);
		}

		~Texture3DHelper()
		{
			mMapper->Delete();
		}

		virtual bool IsUsingData(vtkDataSet *ds)
		{
			if(!this->UniformGridVolumeRenderingHelper::IsUsingData(ds)) return false;
			return this->IsAvailable();
		}

		virtual bool IsAvailable()
		{
#ifdef IVTK_5_PRE8
			return (mMapper->IsRenderSupported(mParent->GetVolume()->GetProperty()) != 0);
#else
			return (mMapper->IsRenderSupported(mParent->GetVolume()->GetProperty(),mParent->GetViewModule()->GetRenderer()) != 0);
#endif
		}

		virtual vtkVolumeMapper* GetMapper()
		{
			return mMapper;
		}

		virtual void SetBlendMode(int m)
		{
			mMapper->SetBlendMode(m);
		}

	protected:

		virtual void AdjustSpacing()
		{
			mMapper->SetSampleDistance(mDepthDownsampleFactor*mDataSpacing);
		}

	private:

		vtkVolumeTextureMapper3D *mMapper;
	};


	class FixedPointRayCastHelper  : public UniformGridVolumeRenderingHelper
	{

	public:

		FixedPointRayCastHelper(iVolumeViewInstance *parent, vtkAlgorithmOutput *port) : UniformGridVolumeRenderingHelper(parent,"Fixed point ray casting")
		{
			mMapper = vtkFixedPointVolumeRayCastMapper::New(); IERROR_CHECK_MEMORY(mMapper);
			mMapper->AddObserver(vtkCommand::ProgressEvent,parent->GetViewModule()->GetRenderEventObserver());
			mMapper->IntermixIntersectingGeometryOn();
			mMapper->AutoAdjustSampleDistancesOn();
			mMapper->SetInputConnection(port);
			mMapper->SetBlendModeToComposite();
		}

		virtual ~FixedPointRayCastHelper()
		{
			mMapper->Delete();
		}

		virtual vtkVolumeMapper* GetMapper()
		{
			return mMapper;
		}

		virtual void SetBlendMode(int m)
		{
			mMapper->SetBlendMode(m);
		}

	protected:

		virtual void AdjustSpacing()
		{
			if(mMapper->GetAutoAdjustSampleDistances() == 0)
			{
				mMapper->SetImageSampleDistance(mImageDownsampleFactor);
			}
			mMapper->SetSampleDistance(mDepthDownsampleFactor*mDataSpacing);
		}

	private:

		vtkFixedPointVolumeRayCastMapper *mMapper;
	};


	class GPURayCastHelper  : public UniformGridVolumeRenderingHelper
	{

	public:

		GPURayCastHelper(iVolumeViewInstance *parent, vtkAlgorithmOutput *port) : UniformGridVolumeRenderingHelper(parent,"GPU ray casting")
		{
			mMapper = vtkGPUVolumeRayCastMapper::New(); IERROR_CHECK_MEMORY(mMapper);
			mMapper->AddObserver(vtkCommand::ProgressEvent,parent->GetViewModule()->GetRenderEventObserver());
			//mMapper->IntermixIntersectingGeometryOn();
			mMapper->AutoAdjustSampleDistancesOn();
			mMapper->SetInputConnection(port);
			mMapper->SetBlendModeToComposite();
		}

		virtual ~GPURayCastHelper()
		{
			mMapper->Delete();
		}

		virtual void Reset()
		{
			mMapper->ReleaseGraphicsResources(mParent->GetViewModule()->GetRenderWindow());
		}

		virtual vtkVolumeMapper* GetMapper()
		{
			return mMapper;
		}

		virtual void SetBlendMode(int m)
		{
			mMapper->SetBlendMode(m);
		}

		virtual bool IsUsingData(vtkDataSet *ds)
		{
			if(!this->UniformGridVolumeRenderingHelper::IsUsingData(ds)) return false;
			return this->IsAvailable();
		}

		virtual bool IsAvailable()
		{
			if(iVolumeViewSubject::GPUMode() < 0) return false;
			if(iVolumeViewSubject::GPUMode() > 0) return true;
			return (mMapper->IsRenderSupported(mParent->GetViewModule()->GetRenderWindow(),mParent->GetVolume()->GetProperty()) != 0);
		}

	protected:

		virtual void AdjustSpacing()
		{
			if(mMapper->GetAutoAdjustSampleDistances() == 0)
			{
				mMapper->SetImageSampleDistance(mImageDownsampleFactor);
			}
			mMapper->SetSampleDistance(mDepthDownsampleFactor*mDataSpacing);
		}

	private:

		vtkGPUVolumeRayCastMapper *mMapper;
	};

#ifdef VTK_USE_VOLUMEPRO
	class VolumeProHelper : public UniformGridVolumeRenderingHelper
	{

	public:

		VolumeProHelper(iVolumeViewInstance *parent, vtkAlgorithmOutput *port) : UniformGridVolumeRenderingHelper(parent,"VolumePro board")
		{
			mMapper = iVolumeProHelper::New(); IERROR_CHECK_MEMORY(mThisMapper);
			mMapper->AddObserver(vtkCommand::ProgressEvent,parent->GetViewModule()->GetAbortRenderEventObserver());
			mMapper->IntermixIntersectingGeometryOn();
			mMapper->SuperSamplingOn();
			mMapper->SetInputConnection(port);
		}

		~VolumeProHelper()
		{
			mMapper->Delete();
		}

		virtual bool IsUsingData(vtkDataSet *ds)
		{
			if(!this->UniformGridVolumeRenderingHelper::IsUsingData(ds)) return false;
			return (mMapper->GetNoHardware()==0 && mMapper->GetWrongVLIVersion()==0 && mMapper->GetNumberOfBoards()>0);
		}

		virtual vtkVolumeMapper* GetMapper()
		{
			return mMapper;
		}

		virtual void SetBlendMode(int m)
		{
			mMapper->SetBlendMode(m);
		}

	protected:

		virtual void AdjustSpacing()
		{
			double d[3];
			mMapper->GetSuperSamplingFactor(d);
			mMapper->SetSuperSamplingFactor(1.0/mImageDownsampleFactor,1.0/mImageDownsampleFactor,1.0/mDepthDownsampleFactor);
		}

	private:

		iVolumeProHelper *mMapper;
	};
#endif
};


using namespace iVolumeViewSubject_Private;


//
//  Single instance class
// 
iVolumeViewInstance::iVolumeViewInstance(iVolumeViewSubject *owner) : iViewInstance(owner)
{
	mVar = 0;
	mPalette = 1;
	mMethodId = 0;
	
	mBlendMode = 0;
	mInterpolationType = 1; 

	mOpacityScaleFactor = mImageDownsampleFactor = mDepthDownsampleFactor = 1.0;
	mBlendMode = 0;
	
	mOpacityFunction = iPiecewiseFunction::New(); IERROR_CHECK_MEMORY(mOpacityFunction);

	mVolume = iReplicatedVolume::New(owner); IERROR_CHECK_MEMORY(mVolume);
	mVolume->SetPosition(0.0,0.0,0.0);
	
	mVolume->GetProperty()->SetScalarOpacity(mOpacityFunction->GetVTKFunction());
	mVolume->GetProperty()->SetColor(this->GetViewModule()->GetShell()->GetPalettes()->GetColorTransferFunction(1));
	mVolume->GetProperty()->SetInterpolationTypeToLinear();
	
	mDataConverter = iVolumeDataConverter::New(this); IERROR_CHECK_MEMORY(mDataConverter);
	mDataReplicated = iReplicatedGridData::New(this); IERROR_CHECK_MEMORY(mDataReplicated);
	
	mDataConverter->AddObserver(vtkCommand::ProgressEvent,this->GetViewModule()->GetRenderEventObserver());
	mDataReplicated->AddObserver(vtkCommand::ProgressEvent,this->GetViewModule()->GetRenderEventObserver());

	mDataReplicated->SetInputConnection(mDataConverter->GetOutputPort());

	mVolume->SetVisibility(0);
	this->GetViewModule()->AddObject(mVolume);
	this->Owner()->GetMaterial()->AddProperty(mVolume->GetProperty());
}


iVolumeViewInstance::~iVolumeViewInstance()
{
	this->iViewInstance::Owner()->GetMaterial()->RemoveProperty(mVolume->GetProperty());
	this->GetViewModule()->RemoveObject(mVolume);

	while(mHelpers.Size() > 0) delete mHelpers.RemoveLast();

	mOpacityFunction->Delete();

	mDataConverter->Delete();
	mDataReplicated->Delete();
	mVolume->Delete();
}


void iVolumeViewInstance::ConfigureBody()
{
	iVolumeRenderingHelper *m;

	//
	//  Create helpers; do it here so that children can replace all mappers
	//
	m = new RayCastHelper(this,mDataReplicated->GetOutputPort()); IERROR_CHECK_MEMORY(m); mHelpers.Add(m);

	m = new FixedPointRayCastHelper(this,mDataReplicated->GetOutputPort()); IERROR_CHECK_MEMORY(m); mHelpers.Add(m);
//	m = new ShearWarpRayCastHelper(this,mDataReplicated->GetOutput()); IERROR_CHECK_MEMORY(m); mHelpers.Add(m);

	m = new Texture2DHelper(this,mDataReplicated->GetOutputPort()); IERROR_CHECK_MEMORY(m); mHelpers.Add(m);
	m = new Texture3DHelper(this,mDataReplicated->GetOutputPort()); IERROR_CHECK_MEMORY(m); mHelpers.Add(m);
	mMethodId = 3;

	m = new GPURayCastHelper(this,mDataReplicated->GetOutputPort()); IERROR_CHECK_MEMORY(m); mHelpers.Add(m);
#ifdef VTK_USE_VOLUMEPRO
	m = new VolumeProHelper(this,mDataReplicated->GetOutputPort()); IERROR_CHECK_MEMORY(m); mHelpers.Add(m);
#endif
}


void iVolumeViewInstance::FinishInitialization()
{
	this->SetMethodId(mMethodId);
	this->SetBlendMode(mBlendMode);

	int i;
	for(i=0; i<mHelpers.Size(); i++)
	{
		mHelpers[i]->GetMapper()->SetClippingPlanes(this->GetViewModule()->GetClipPlane()->GetPlanes());
	}
}


void iVolumeViewInstance::ResetBody()
{
	mVolume->SetVisibility(0);
}


void iVolumeViewInstance::ShowBody(bool show)
{
	if(show)
	{
		mVolume->SetVisibility(1);
	} 
	else 
	{
		mVolume->SetVisibility(0);
	}
}


void iVolumeViewInstance::UpdateColorBars()
{
	if(mColorBars.Size() == 0)
	{
		this->AddColorBar(ColorBarPriority::Volume,mVar,mPalette,this->GetDataType());
	}
}


void iVolumeViewInstance::UpdateIndex()
{
	int i;

	for(i=0; i<mHelpers.Size(); i++)
	{
		mHelpers[i]->SetVar(mVar);
	}

	mDataConverter->SetCurrentVar(mVar);
}


bool iVolumeViewInstance::SetVar(int v)
{
	if(v<0 || (this->IsThereData() && v>=this->GetLimits()->GetNumVars())) return false;

	if(v != mVar)
	{
		mVar = v;
		if(this->IsThereData())
		{
			this->RemoveColorBars();
			this->UpdateColorBars();
			this->UpdateIndex();
			if(this->Owner()->IsVisible()) this->Show(true);
		}
	}
	return true;
}


bool iVolumeViewInstance::SetPalette(int p)
{ 
	vtkColorTransferFunction *ctf = this->GetViewModule()->GetShell()->GetPalettes()->GetColorTransferFunction(p);

	if(ctf != 0)
	{
		this->RemoveColorBars();
		mPalette = p;
		this->UpdateColorBars();
		mVolume->GetProperty()->SetColor(ctf);
		if(mMethodId != -1) mHelpers[mMethodId]->Reset();
		return true;
	}
	else return false;
}


bool iVolumeViewInstance::SetMethodId(int m)
{
	if(m>=0 && m<mHelpers.Size())
	{
		if(this->IsThereData() && !mHelpers[m]->IsUsingData(this->GetSubject()->GetData()))
		{
			int i;
			for(i=mHelpers.MaxIndex(); i>=0; i--)
			{
				if(mHelpers[i]->IsUsingData(this->GetSubject()->GetData())) break;
			}
			m = i;
		}
		if(mMethodId != -1) mHelpers[mMethodId]->Reset();
		mMethodId = m;
		if(mMethodId != -1)
		{
            mVolume->SetMapper(mHelpers[mMethodId]->GetMapper());
		}
		else
		{
			this->Show(false);
		}
		return true;
	}
	else return false;
}


bool iVolumeViewInstance::SetInterpolationType(int m) 
{
	if(m>=0 && m<2) 
	{
		mInterpolationType = m;
		switch(m)
		{
		case 0:
			{
				mVolume->GetProperty()->SetInterpolationTypeToNearest();
				break;
			}
		case 1:
			{
				mVolume->GetProperty()->SetInterpolationTypeToLinear();
				break;
			}
		}
		return true;
	}
	else return false;
}


bool iVolumeViewInstance::SetOpacityScaleFactor(float m) 
{
	if(m < 0.5) return false;

	mVolume->GetProperty()->SetScalarOpacityUnitDistance(1.0/m);

	mOpacityScaleFactor = m;
	return true;
}


bool iVolumeViewInstance::SetDepthDownsampleFactor(float m) 
{
	if(m < 1.0e-3) return false;

	int i;

	for(i=0; i<mHelpers.Size(); i++)
	{
		mHelpers[i]->SetDepthDownsampleFactor(m);
	}

	mDepthDownsampleFactor = m;
	return true;
}


bool iVolumeViewInstance::SetImageDownsampleFactor(float m) 
{
	if(m < 1.0e-3) return false;

	int i;

	for(i=0; i<mHelpers.Size(); i++)
	{
		mHelpers[i]->SetImageDownsampleFactor(m);
	}

	mImageDownsampleFactor = m;
	return true;
}


bool iVolumeViewInstance::SetBlendMode(int m) 
{
	if(m<0 || m>1) return false;
	
	int i;

	for(i=0; i<mHelpers.Size(); i++)
	{
		mHelpers[i]->SetBlendMode(m);
	}

	mBlendMode = m;
	return true;
}


float iVolumeViewInstance::GetMemorySize()
{
	int i;
	float s = this->iViewInstance::GetMemorySize();

	for(i=0; i<mHelpers.Size(); i++)
	{
		s += mHelpers[i]->GetMemorySize();
	}
	s += mDataConverter->GetMemorySize();
	s += mDataReplicated->GetMemorySize();

	return s;
}


const iString& iVolumeViewInstance::GetMethod() const
{
	return this->GetMethodName(mMethodId);
}


bool iVolumeViewInstance::SetMethod(const iString& name)
{
	int i;

	for(i=0; i<mHelpers.Size(); i++)
	{
		if(name == mHelpers[i]->GetName()) return this->SetMethodId(i);
	}

	return false;
}



const iString& iVolumeViewInstance::GetMethodName(int i) const
{
	static const iString none;

	if(i>=0 && i<mHelpers.Size()) return mHelpers[i]->GetName(); else return none;
}


bool iVolumeViewInstance::IsMethodAvailable(int i) const
{
	if(i>=0 && i<mHelpers.Size())
	{
		if(!this->IsThereData()) return true; else return mHelpers[i]->IsUsingData(this->GetSubject()->GetData());
	}
	else return false;
}


bool iVolumeViewInstance::SyncWithDataBody()
{
	if(this->IsThereData())
	{
		//
		//  Input is ImageData
		//
		int i;
		vtkImageData *id = vtkImageData::SafeDownCast(this->GetSubject()->GetData());
	 	if(id != 0)
		{
			mDataConverter->SetInputData(id);

			double s = id->GetSpacing()[2];
			for(i=0; i<mHelpers.Size(); i++)
			{
				mHelpers[i]->SetDataSpacing(s);
			}
		}

		//
		//  Reset input
		//
		for(i=0; i<mHelpers.Size(); i++) if(mHelpers[i]->IsAttachedDirectly())
		{
			mHelpers[i]->GetMapper()->SetInputData(this->GetSubject()->GetData());
		}

		this->SetMethodId(mMethodId); // may need to reset the mapper.
	}

	return true;
}


bool iVolumeViewInstance::CanBeShown() const
{
	return (this->IsThereData() && mVar<this->GetLimits()->GetNumVars());
}


//
//  Main class
//
iVolumeViewSubject::iVolumeViewSubject(iViewObject *parent, const iDataType &type) : iViewSubject(parent,parent->LongName(),parent->ShortName(),parent->GetViewModule(),type,0U,1,1),
	iViewSubjectPropertyConstructMacro(Int,iVolumeViewInstance,Var,v),
	iViewSubjectPropertyConstructMacro(String,iVolumeViewInstance,Method,m),
	iViewSubjectPropertyConstructMacro(Int,iVolumeViewInstance,MethodId,mid),
	iViewSubjectPropertyConstructMacro(Int,iVolumeViewInstance,Palette,p),
	iViewSubjectPropertyConstructMacro(Int,iVolumeViewInstance,BlendMode,bm),
	iViewSubjectPropertyConstructMacro(Int,iVolumeViewInstance,InterpolationType,it),
	iViewSubjectPropertyConstructMacro(Float,iVolumeViewInstance,OpacityScaleFactor,os),
	iViewSubjectPropertyConstructMacro(Float,iVolumeViewInstance,ImageDownsampleFactor,di),
	iViewSubjectPropertyConstructMacro(Float,iVolumeViewInstance,DepthDownsampleFactor,dd),
	OpacityFunction(iObject::Self(),"OpacityFunction","of"),
	iObjectConstructPropertyMacroQ(String,iVolumeViewSubject,AllMethods,am)
{
	Var.AddFlag(iProperty::_FunctionsAsIndex);
}


iVolumeViewSubject::~iVolumeViewSubject()
{
}


const iString& iVolumeViewSubject::GetMethodName(int i) const
{
	return iRequiredCast<iVolumeViewInstance>(INFO,mInstances[0])->GetMethodName(i);
}


const iString& iVolumeViewSubject::GetAllMethods() const
{
	if(mMethodsCache.IsEmpty())
	{
		int i = 0;
		while(!this->GetMethodName(i).IsEmpty())
		{
			if(!mMethodsCache.IsEmpty()) mMethodsCache += ",";
			mMethodsCache += this->GetMethodName(i);
			i++;
		}
	}

	return mMethodsCache;
}


bool iVolumeViewSubject::IsMethodAvailable(int i) const
{
	return iRequiredCast<iVolumeViewInstance>(INFO,mInstances[0])->IsMethodAvailable(i);
}


void iVolumeViewSubject::InitInstances()
{
	this->iViewSubject::InitInstances();
	OpacityFunction.AttachFunction(iRequiredCast<iVolumeViewInstance>(INFO,mInstances[0])->mOpacityFunction);
}


void iVolumeViewSubject::SetGPUMode(int m)
{
	mGPUMode = m;
}


void iVolumeViewSubject::UpdateAutomaticShadingBody()
{
	this->GetMaterial()->SetShading(false);
}


//
//  Helper class
//
iVolumeRenderingHelper::iVolumeRenderingHelper(iVolumeViewInstance *parent, const iString &name, bool attachedDirectly) : mParent(parent), mName(name), mAttachedDirectly(attachedDirectly)
{
	IASSERT(parent);

	mDataSpacing = 1.0;
	mImageDownsampleFactor = mDepthDownsampleFactor = 1.0f;
}


iVolumeRenderingHelper::~iVolumeRenderingHelper()
{
}


void iVolumeRenderingHelper::SetDataSpacing(double v)
{
	if(v > 0.0)
	{
		mDataSpacing  = v;
		this->AdjustSpacing();
	}
}


void iVolumeRenderingHelper::SetImageDownsampleFactor(float v)
{
	if(v > 0.0f)
	{
		mImageDownsampleFactor  = v;
		this->AdjustSpacing();
	}
}


void iVolumeRenderingHelper::SetDepthDownsampleFactor(float v)
{
	if(v > 0.0f)
	{
		mDepthDownsampleFactor  = v;
		this->AdjustSpacing();
	}
}
