/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
// A small set of common functionality for all IFrIT viewization objects
//

#ifndef IOBJECT_H
#define IOBJECT_H


#include <vtkObjectBase.h>


#include "iarray.h"
#include "iproperty.h"
#include "istring.h"

#include <vtkSetGet.h>

class iProperty;
class iPropertyFunction;
class iPropertyVariable;


namespace iParameter
{
	namespace RenderMode
	{
		const int NoRender  =  0;
		const int Self		=  1;
		const int Clones	=  2;
		const int All		=  3;
	}
};
	

class iObject : public vtkObjectBase
{
	
	friend class iPropertyVariable;
	friend class iPropertyFunction;

public:

	vtkTypeMacro(iObject,vtkObjectBase);

	inline const iString& LongName() const { return mLongName; }
	inline const iString& ShortName() const { return mShortName; }
	inline const iString& Tag() const { return mTag; } // for ordering

	const iString GetFullName() const;
	virtual const iString HelpTag() const;

	int ParentIndex() const;

	inline int GetRenderMode() const { return mRenderMode; }
	virtual void RequestRender();
	virtual bool IsImmediatePost() const;

	virtual bool CopyState(const iObject *p);

	virtual void WriteState(iString &s) const;
	virtual void RegisterWithHelpFactory() const;
	bool IsHidden() const;

	virtual const iObject* FindByName(const iString &name) const;
	virtual const iProperty* FindPropertyByName(const iString &name) const;
	virtual const iProperty* GetLocalProperty(const iString &name) const;

	virtual void SaveStateToString(iString &str) const;
	virtual bool LoadStateFromString(const iString &str);

	//
	//  Must be public for shell access
	//
	inline const iArray<iPropertyVariable*>& Variables() const { return mVariableProperties; }
	inline const iArray<iPropertyFunction*>& Functions() const { return mFunctionProperties; }
	inline const iArray<iObject*>& Children() const { return mChildren; }

protected:

	iObject(iObject *parent, const iString &fname, const iString &sname, int level = 0);
	virtual ~iObject(); 

	iObject* Self(){ return this; }  // needed to avoid compiler warning
	iObject* Parent() const { return mParent; }
	
	virtual void PassRenderRequest(int mode);

	virtual void WriteHead(iString &s) const;

	virtual void SavePropertiesToString(iString &str) const;
	virtual void OnLoadStateAtom();

	void RegisterPropertiesWithHelpFactory() const;

	//
	//  Child-parent hierarchy (inherited classes need to change this, hence it is protected)
	//
	iGenericOrderedArray<iObject*,iString> mChildren;
	static const iString& ChildLookup(iObject* const &ptr);
	int mRenderMode;

	//
	//  Object properties
	//
	iGenericOrderedArray<iPropertyVariable*,iString> mVariableProperties;
	iGenericOrderedArray<iPropertyFunction*,iString> mFunctionProperties;
	static const iString& VariableLookup(iPropertyVariable* const &ptr);
	static const iString& FunctionLookup(iPropertyFunction* const &ptr);

private:

	iObject *mParent;
	const iString& mLongName;
	const iString& mShortName;
	const iString mTag;

	//
	//  This is a part of message subsystem for thread-safe execution.
	//  Functions begining with underscore can be called from the driver thread.
	//
public:

	virtual bool _RequestPush(iProperty::Key key);
};


//
//  Helper macros
//
#define	iObjectSlotMacro1S(_name_,_type_,_argt_) \
	protected: _type_ m##_name_; \
	public: _argt_ Get##_name_() const { return m##_name_; } \
	bool Set##_name_(_argt_ s)

#define	iObjectSlotMacro1SV(_name_,_type_) \
	iObjectSlotMacro1S(_name_,_type_,_type_)
 
#define	iObjectSlotMacro1SR(_name_,_type_) \
	iObjectSlotMacro1S(_name_,_type_,const _type_&)

#define	iObjectSlotMacro2S(_name_,_argt_) \
	_argt_ Get##_name_() const; \
	bool Set##_name_(_argt_ s)

#define	iObjectSlotMacro1V(_name_,_type_,_argt_,_dim_) \
	protected: _type_ m##_name_[_dim_]; \
	public: _argt_ Get##_name_(int i) const { return m##_name_[i]; } \
	bool Set##_name_(int i, _argt_ s)

#define	iObjectSlotMacro2V(_name_,_argt_) \
	_argt_ Get##_name_(int i) const; \
	bool Set##_name_(int i, _argt_ s)

#define	iObjectSlotMacro3V(_name_,_argt_) \
	int Size##_name_() const; \
	bool Resize##_name_(int n); \
	_argt_ Get##_name_(int i) const; \
	bool Set##_name_(int i, _argt_ s)

#define iObjectSlotMacro1Q(_name_,_type_,_rett_) \
	protected: _type_ m##_name_; \
	public: _rett_ Get##_name_() const { return m##_name_; } 

#define iObjectSlotMacro2Q(_name_,_rett_) \
	public: _rett_ Get##_name_() const

#define iObjectSlotMacroA(_name_) \
	bool Call##_name_()

#define iObjectSlotMacroA1(_name_,_argt_) \
	bool Call##_name_(_argt_ v)

#define iObjectSlotMacroA2(_name_,_argt1_,_argt2_) \
	bool Call##_name_(_argt1_ v1, _argt2_ v2)
//
//
#define	iObjectPropertyMacro1S(_name_,_id_) \
	iObjectSlotMacro1S(_name_,iType::Traits<iType::_id_>::ContainerType,iType::Traits<iType::_id_>::Type); \
	iPropertyScalar<iType::_id_> _name_

#define	iObjectPropertyMacro2S(_name_,_id_) \
	iObjectSlotMacro2S(_name_,iType::Traits<iType::_id_>::Type); \
	iPropertyScalar<iType::_id_> _name_

#define	iObjectPropertyMacro1V(_name_,_id_,_dim_) \
	iObjectSlotMacro1V(_name_,iType::Traits<iType::_id_>::ContainerType,iType::Traits<iType::_id_>::Type,_dim_); \
	iPropertyVector<iType::_id_> _name_

#define	iObjectPropertyMacro2V(_name_,_id_) \
	iObjectSlotMacro2V(_name_,iType::Traits<iType::_id_>::Type); \
	iPropertyVector<iType::_id_> _name_

#define	iObjectPropertyMacro3V(_name_,_id_) \
	iObjectSlotMacro3V(_name_,iType::Traits<iType::_id_>::Type); \
	iPropertyVector<iType::_id_> _name_

#define	iObjectPropertyMacro1Q(_name_,_id_) \
	iObjectSlotMacro1Q(_name_,iType::Traits<iType::_id_>::ContainerType,iType::Traits<iType::_id_>::Type); \
	iPropertyQuery<iType::_id_> _name_

#define	iObjectPropertyMacro2Q(_name_,_id_) \
	iObjectSlotMacro2Q(_name_,iType::Traits<iType::_id_>::Type); \
	iPropertyQuery<iType::_id_> _name_

#define iObjectPropertyMacroA(_name_) \
	iObjectSlotMacroA(_name_); \
	iPropertyAction _name_

#define	iObjectPropertyMacroA1(_name_,_id_) \
	iObjectSlotMacroA1(_name_,iType::Traits<iType::_id_>::Type); \
	iPropertyAction1<iType::_id_> _name_

#define	iObjectPropertyMacroA2(_name_,_id1_,_id2_) \
	iObjectSlotMacroA2(_name_,iType::Traits<iType::_id1_>::Type,iType::Traits<iType::_id2_>::Type); \
	iPropertyAction2<iType::_id1_,iType::_id2_> _name_
//
//
#define iObjectConstructPropertyMacroGenericS(_id_,_subject_,_fname_,_sname_,_setter_,_getter_) \
	_fname_(iObject::Self(),static_cast<iPropertyScalar<iType::_id_>::SetterType>(&_subject_::_setter_),static_cast<iPropertyScalar<iType::_id_>::GetterType>(&_subject_::_getter_),#_fname_,#_sname_)

#define iObjectConstructPropertyMacroGenericSOP(YES,_id_,_subject_,_fname_,_sname_,_setter_,_getter_) \
	_fname_(iObject::Self(),static_cast<iPropertyScalar<iType::_id_>::SetterType>(&_subject_::_setter_),static_cast<iPropertyScalar<iType::_id_>::GetterType>(&_subject_::_getter_),(YES)?#_fname_:"",(YES)?#_sname_:"")

#define iObjectConstructPropertyMacroS(_id_,_subject_,_fname_,_sname_)			iObjectConstructPropertyMacroGenericS(_id_,_subject_,_fname_,_sname_,Set##_fname_,Get##_fname_)
#define iObjectConstructPropertyMacroSOP(YES,_id_,_subject_,_fname_,_sname_)		iObjectConstructPropertyMacroGenericSOP(YES,_id_,_subject_,_fname_,_sname_,Set##_fname_,Get##_fname_)
//
//
#define iObjectConstructPropertyMacroGenericV(_id_,_subject_,_fname_,_sname_,_dim_,_setter_,_getter_) \
	_fname_(iObject::Self(),static_cast<iPropertyVector<iType::_id_>::SetterType>(&_subject_::_setter_),static_cast<iPropertyVector<iType::_id_>::GetterType>(&_subject_::_getter_),#_fname_,#_sname_,_dim_)

#define iObjectConstructPropertyMacroGenericVOP(YES,_id_,_subject_,_fname_,_sname_,_dim_,_setter_,_getter_) \
	_fname_(iObject::Self(),static_cast<iPropertyVector<iType::_id_>::SetterType>(&_subject_::_setter_),static_cast<iPropertyVector<iType::_id_>::GetterType>(&_subject_::_getter_),(YES)?#_fname_:"",(YES)?#_sname_:"",_dim_)

#define iObjectConstructPropertyMacroV(_id_,_subject_,_fname_,_sname_,_dim_)		iObjectConstructPropertyMacroGenericV(_id_,_subject_,_fname_,_sname_,_dim_,Set##_fname_,Get##_fname_)
//
//
#define iObjectConstructPropertyMacroGenericQ(_id_,_subject_,_fname_,_sname_,_getter_) \
	_fname_(iObject::Self(),static_cast<iPropertyQuery<iType::_id_>::GetterType>(&_subject_::_getter_),#_fname_,#_sname_)

#define iObjectConstructPropertyMacroQ(_id_,_subject_,_fname_,_sname_)			iObjectConstructPropertyMacroGenericQ(_id_,_subject_,_fname_,_sname_,Get##_fname_)
//
//
#define iObjectConstructPropertyMacroGenericA(_subject_,_fname_,_sname_,_caller_) \
	_fname_(iObject::Self(),static_cast<iPropertyAction::CallerType>(&_subject_::_caller_),#_fname_,#_sname_)

#define iObjectConstructPropertyMacroA(_subject_,_fname_,_sname_)					iObjectConstructPropertyMacroGenericA(_subject_,_fname_,_sname_,Call##_fname_)
//
//
#define iObjectConstructPropertyMacroGenericA1(_id_,_subject_,_fname_,_sname_,_caller_) \
	_fname_(iObject::Self(),static_cast<iPropertyAction1<iType::_id_>::CallerType>(&_subject_::_caller_),#_fname_,#_sname_)

#define iObjectConstructPropertyMacroA1(_id_,_subject_,_fname_,_sname_)			iObjectConstructPropertyMacroGenericA1(_id_,_subject_,_fname_,_sname_,Call##_fname_)
//
//
#define iObjectConstructPropertyMacroGenericA2(_id1_,_id2_,_subject_,_fname_,_sname_,_caller_) \
	_fname_(iObject::Self(),static_cast<iPropertyAction2<iType::_id1_,iType::_id2_>::CallerType>(&_subject_::_caller_),#_fname_,#_sname_)

#define iObjectConstructPropertyMacroA2(_id1_,_id2_,_subject_,_fname_,_sname_)		iObjectConstructPropertyMacroGenericA2(_id1_,_id2_,_subject_,_fname_,_sname_,Call##_fname_)


#endif // IOBJECT_H

