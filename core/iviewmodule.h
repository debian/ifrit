/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef IVIEWMODULE_H
#define IVIEWMODULE_H


#include "iextendableobject.h"
#include "ishellcomponent.h"


#include "imath.h"
#include "istereoimagearray.h"


class iActorCollection;
class iAnimator;
class iBoundingBox;
class iCaptionInteractorStyle;
class iCameraOrthoActor;
class iClipPlane;
class iColorBar;
class iCrossSectionViewSubject;
class iDataObject;
class iDataReader;
class iDataType;
class iLabel;
class iLights;
class iMarkerInstance;
class iMarkerObject;
class iMeasuringBox;
class iParallelManager;
class iParticleViewSubject;
class iPicker;
class iRenderEventObserver;
class iRenderTool;
class iRuler;
class iSurfaceViewSubject;
class iSyncRequest;
class iTensorFieldViewSubject;
class iVectorFieldViewSubject;
class iViewObject;
class iViewModuleCollection;
class iVolumeViewSubject;
class iWriter;

class vtkActor2D;
class vtkInteractorStyle;
class vtkPlaneCollection;
class vtkProp;
class vtkRenderer;
class vtkRenderWindow;
class vtkRenderWindowCollection;
class vtkRenderWindowInteractor;
class vtkTimerLog;


namespace iParameter
{
	namespace ImageType
	{
		const int Image =		0;
		const int MovieFrame =	1;
	};

	namespace InteractorStyle
	{
		const int Trackball =	0;
		const int Joystick =	1;
		const int Flight =		2;
		const int Keyboard =	3;
		const int SIZE =		4;
	};
};


class iViewModule : public iExtendableObject, public iShellComponent
{
	
	friend class iDataConsumer;
	friend class iShell;
	friend class iViewModuleCollection;

private:

	iViewModule *mCloneOfModule;

public:

	vtkTypeMacro(iViewModule,iExtendableObject);
	
	iObjectSlotMacro2S(CloneOfWindow,int);
	iPropertyScalar<iType::Int> CloneOfWindow;
	virtual bool BecomeClone(iViewModule *v);
	bool IsClone(iViewModule *vm = 0) const;

	iObjectPropertyMacro1S(BoxSize,Float);
	bool InOpenGLCoordinates() const;

	iObjectPropertyMacro1V(Antialiasing,Bool,3);
	iObjectPropertyMacro1S(BackgroundImageFixedAspect,Bool);
	iObjectPropertyMacro1S(BackgroundColor,Color);
	iObjectPropertyMacro1S(BackgroundImage,String);
	iObjectPropertyMacro1S(CameraAlignmentLabel,Bool);
	iObjectPropertyMacro1S(CurrentInteractorStyle,Int);
	iObjectPropertyMacro2S(FontScale,Int);
	iObjectPropertyMacro2S(FontType,Int);
	iObjectPropertyMacro1S(ImageMagnification,Int);
	iObjectPropertyMacro1S(StereoAlignmentMarks,Bool);
	iObjectPropertyMacro2S(StereoMode,Int);
	iObjectPropertyMacro1S(TrueRendering,Int);
	iObjectPropertyMacro1S(UpdateRate,Int);

	void StartAnimation();
	void FinishAnimation();

	iObjectPropertyMacro2V(Position,Int);
	iObjectPropertyMacro2V(Size,Int);

	iObjectPropertyMacroA1(Export,String);
	iObjectPropertyMacroA(WriteImage);
	iObjectPropertyMacroA(Render);

	//
	//  Render tools
	//
	iRenderTool* GetRenderTool() const { return mRenderTool; }
	vtkRenderer* GetRenderer() const;
	vtkRenderWindow* GetRenderWindow() const;
	vtkRenderWindowInteractor* GetRenderWindowInteractor() const;
	vtkRenderWindowCollection* GetRenderWindowCollection(bool inited = false) const;

	void FlyTo(const double *pos, float time) const;

	//
	//  Tools
	//
	inline iLabel* GetLabel() const { return mLabel; }
	inline iRuler* GetRuler() const { return mRuler; }
	inline iColorBar* GetColorBar() const { return mColorBar; }
	inline iClipPlane* GetClipPlane() const { return mClipPlane; }
	inline iBoundingBox* GetBoundingBox() const { return mBoundingBox; }
	inline iMeasuringBox* GetMeasuringBox() const { return mMeasuringBox; }

	inline iRenderEventObserver* GetRenderEventObserver() const { return mRenderObserver; }
	inline iPicker* GetPicker() const { return mPicker; }

	void AddObject(vtkProp* p);
	void RemoveObject(vtkProp* p);

	inline iAnimator* GetAnimator() const { return mAnimator; }
	inline iDataReader* GetReader() const { return mDataReader; }
	inline iDataObject* GetDataObject() const { return mDataObject; }
	iParallelManager* GetParallelManager() const;
	
	void RenderStereoImage(iStereoImage &image);
	void CreateImages(iStereoImageArray &images);
	void RenderImages(iStereoImageArray &images);
	void WriteImages(int mode);
	virtual void WriteImages(const iStereoImageArray& images, int mode);
	inline iWriter* GetImageWriter() const { return mWriter; }

	void UpdateLabel();

	inline int GetWindowNumber() const { return this->ParentIndex(); }

	inline float GetUpdateTime() const { return mUpdateTime; }
	virtual float GetMemorySize() const;
	void UpdatePerformance();
	void UpdateAfterFileLoad();

	//
	//  iViewSubject-related functions
	//
	inline iMarkerObject* GetMarkerObject() const { return mMarkerObject; }
	iMarkerInstance* GetMarker(int n = -1) const;
	int GetNumberOfMarkers() const;
	void LaunchInteractorStyleCaption();

	int GetViewObjectIndex(const iString &name) const;
	iViewObject* GetViewObjectByName(const iString &name) const;
	inline iViewObject* GetViewObjectByIndex(int n) const { if(n>=0 && n<mViewObjects.Size()) return mViewObjects[n]; else return 0; }
	//inline int GetNumberOfViewObjects() const { return mViewObjects.Size(); }

	virtual int GetFullImageWidth();  // Get the image size from Image Composer
	virtual int GetFullImageHeight();
	virtual int GetThisImageWidth();  // Get the image size of this window only
	virtual int GetThisImageHeight();

	inline bool IsRenderingImage() const { return mInImageRender; }
	void ForceCrossSectionToUsePolygonalMethod(bool s);

	inline bool GetPointAntialising() const { return mAntialiasing[0]; }
	inline bool GetLineAntialising() const { return mAntialiasing[1]; }
	inline bool GetPolyAntialising() const { return mAntialiasing[2]; }

	virtual int DepthPeelingRequest() const;

	void SyncWithData(const iSyncRequest &request);

	void SetDebugMode(bool s);

protected:

	virtual ~iViewModule();

private:
	
	iViewModule(iViewModuleCollection *parent, const iString &fname, const iString &sname);

	void SyncWithDataBody(const iSyncRequest &request);

	//
	//  Rendering pipeline
	//
	virtual void PassRenderRequest(int mode);
	void ActualRender();

	//
	//  Members
	//
	int mStereoType, mSavedProjection;

	iLookupArray<iViewModule*> mClones;

	iStereoImageArray mImages;

	bool mIsDebug, mInImageRender;
	int mOldParentIndex, mOldNumWindows;
	float mUpdateTime;
	bool mInAnimation;

	//
	//  Rendering pipeline
	//
	iRenderTool *mRenderTool;
	iLights *mLights;

	//
	//  Tools
	//
	iLabel* mLabel;
	iRuler* mRuler;
	iColorBar* mColorBar;
	iClipPlane* mClipPlane;
	iBoundingBox* mBoundingBox;
	iMeasuringBox* mMeasuringBox;

	//
	//  Actors displayed by this class
	//
	iActorCollection *mDebugActor;
	iCameraOrthoActor *mCameraOrthoActor;

	//
	//  Prop registries
	//
	iLookupArray<vtkProp*> mProps, mVisibleProps;

	//
	//  Other components
	//
	iAnimator *mAnimator;
	iDataObject *mDataObject;
	iDataReader *mDataReader;
	iPicker *mPicker;
	vtkTimerLog *mUpdateTimer;
	iWriter *mWriter;

	vtkInteractorStyle *mInteractorStyle[iParameter::InteractorStyle::SIZE];
	iCaptionInteractorStyle *mInteractorStyleCaption;

	//
	//  Event observer
	//
	iRenderEventObserver *mRenderObserver;

	//
	//  iViewSubject-related members
	//
	iMarkerObject *mMarkerObject; 
	iArray<iViewObject*> mViewObjects; 
};


inline bool iViewModule::IsClone(iViewModule *vm) const
{
	if(vm == 0)	return (mCloneOfModule != 0); else return (mCloneOfModule == vm);
}


inline int iViewModule::GetCloneOfWindow() const
{
	if(mCloneOfModule != 0) return mCloneOfModule->ParentIndex(); else return -1;
}


inline bool iViewModule::InOpenGLCoordinates() const
{ 
	return (mBoxSize == 0.0); 
}


class iViewModuleExtension : public iObjectExtension
{

	iObjectExtensionDeclareAbstractMacro(iViewModule);

public:

	virtual iViewObject* CreateObject(int n) const = 0;
};

#endif // IVIEWMODULE_H

