/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "iorthopolygonplanefilter.h"


#include "ierror.h"
#include "iorthoslicer.h"
#include "ivtk.h"

#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkFloatArray.h>
#include <vtkImageData.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>

//
//  Templates
//
#include "igenericfilter.tlh"


#ifndef IVTK_QUAD_ORDER
#error Misconfiguration: missing IVTK_QUAD_ORDER define.
!TERMINATE!
#endif


iOrthoPolygonPlaneFilter::iOrthoPolygonPlaneFilter(iDataConsumer *consumer) : iGenericFilter<vtkDataSetAlgorithm,vtkImageData,vtkPolyData>(consumer,1,true)
{
	mInterpolate = true;

	mOldDimU = mOldDimV = 0;

	//
	//  Keep our quds so that we do not have to re-create them unless something changes
	//
	mNewQuads = vtkCellArray::New(); IERROR_CHECK_MEMORY(mNewQuads);
}


iOrthoPolygonPlaneFilter::~iOrthoPolygonPlaneFilter()
{
	mNewQuads->Delete();
}


void iOrthoPolygonPlaneFilter::SetInterpolation(bool s)
{
	mInterpolate = s;
	this->Modified();
}


void iOrthoPolygonPlaneFilter::ProvideOutput()
{
	int dims[3];
	double org[3], spa[3];

	vtkImageData *input = this->InputData();
	vtkPolyData *output = this->OutputData();

	output->Initialize();

	//
	//  Do we have cell or point data?
	//
	bool isPointData;
	if(input->GetPointData()->GetScalars() != 0)
	{
		isPointData = true;
	}
	else if(input->GetCellData()->GetScalars() != 0)
	{
		isPointData = false;
	}
	else
	{
		//
		//  Must be quiet here for parallel execution
		//
		return;
	}

	input->GetOrigin(org);
    input->GetSpacing(spa);
    input->GetDimensions(dims);

	int i, j, k, Axis = -1;
	for(i=0; i<3; i++)
	{
		if(dims[i] == 1) Axis = i;
	}
	if(Axis == -1)
	{
		vtkErrorMacro("iOrthoPolygonPlaneFilter: input data are not a plane.");
		return;
	}

    int u, v, dimU, dimV;
	iOrthoSlicer::GetUV(Axis,u,v);

	if(isPointData)
	{
		dimU = dims[u];
		dimV = dims[v];
	}
	else
	{
		dimU = dims[u] + 1;
		dimV = dims[v] + 1;
	}

	if(dimU<1 || dimV<1)
	{
		return;
	}

	vtkIdType numPts = (vtkIdType)dimU*dimV;
    vtkPoints *newPts = vtkPoints::New(VTK_FLOAT); IERROR_CHECK_MEMORY(newPts);
    newPts->SetNumberOfPoints(numPts);
    
	float xyz[3];
	xyz[Axis] = org[Axis];
	float *p = (float *)newPts->GetVoidPointer(0);
	for(j=0; j<dimV; j++)
	{
		this->UpdateProgress(float(j)/dimV);
		if(this->GetAbortExecute()) break;
				
		xyz[v] = org[v] + spa[v]*j;
		for(i=0; i<dimU; i++)
		{
			xyz[u] = org[u] + spa[u]*i;
			for(k=0; k<3; k++) p[k+3*(i+j*dimU)] = xyz[k];
		}
	}

	if(dimU!=mOldDimU || dimV!=mOldDimV) 
	{
		mNewQuads->Delete();
		mNewQuads = vtkCellArray::New(); IERROR_CHECK_MEMORY(mNewQuads);
		mNewQuads->Allocate(mNewQuads->EstimateSize((dimV-1)*(dimU-1),4));
		vtkIdType l[4];
		for(j=0; j<dimV-1; j++)
		{
			this->UpdateProgress(float(j)/(dimV-1));
			if(this->GetAbortExecute()) break;

			for(i=0; i<dimU-1; i++)
			{ 
#if (IVTK_QUAD_ORDER == 0)
				l[0] = i + j*dimU;
				l[1] = l[0] + dimU;
				l[2] = l[1] + 1;
				l[3] = l[0] + 1;
#endif
#if (IVTK_QUAD_ORDER == 3)
				l[3] = i + j*dimU;
				l[0] = l[3] + dimU;
				l[1] = l[0] + 1;
				l[2] = l[3] + 1;
#endif
				mNewQuads->InsertNextCell(4,l);
			}
		}

		mOldDimU = dimU;
		mOldDimV = dimV;
    }
    
	output->SetPolys(mNewQuads);
    output->SetPoints(newPts);
    newPts->Delete();

	//
	//  Assign scalars
	//
	if(isPointData)
	{
		if(!mInterpolate)
		{
			//
			//  Correct for flat shading
			//
			if(!wArray.Init(input->GetPointData()->GetScalars()))
			{
				this->SetAbortExecute(1);
				return;
			}

			float *pl, *ql;
			for(j=0; j<dimV-1; j++)
			{
				pl = wArray.PtrIn + wArray.DimIn*j*dimU;
				ql = wArray.PtrOut + wArray.DimOut*j*dimU;
				for(i=0; i<dimU-1; i++)
				{
					for(k=0; k<wArray.DimIn; k++)
					{
						ql[k] = 0.25*(pl[k]+pl[wArray.DimIn+k]+pl[wArray.DimIn*dimU+k]+pl[wArray.DimIn*(dimU+1)+k]);
					}
					pl += wArray.DimIn;
					ql += wArray.DimOut;
				}
			}

			output->GetPointData()->SetScalars(wArray.ArrOut);
			wArray.ArrOut->Delete();
		}
		else
		{
			output->GetPointData()->SetScalars(input->GetPointData()->GetScalars());
		}
	}
	else
	{
		if(!wArray.Init(input->GetCellData()->GetScalars(),numPts))
		{
			this->SetAbortExecute(1);
			return;
		}

		int du = dimU - 1;

		if(mInterpolate)
		{
			float *ql;
			int i1, j1, i2, j2;
			for(j=0; j<dimV; j++)
			{
				j1 = j;
				if(j1 == dimV-1) j1 = dimV - 2;
				j2 = j - 1;
				if(j2 < 0) j2 = 0;
				ql = wArray.PtrOut + wArray.DimOut*j*dimU;
				for(i=0; i<dimU; i++)
				{
					i1 = i;
					if(i1 == du) i1 = du - 1;
					i2 = i - 1;
					if(i2 < 0) i2 = 0;
					for(k=0; k<wArray.DimIn; k++)
					{
						ql[k] = 0.25f*(wArray.PtrIn[k+wArray.DimIn*(i1+j1*du)]+wArray.PtrIn[k+wArray.DimIn*(i2+j1*du)]+wArray.PtrIn[k+wArray.DimIn*(i1+j2*du)]+wArray.PtrIn[k+wArray.DimIn*(i2+j2*du)]);
					}
					ql += wArray.DimOut;
				}
			}
		}
		else
		{
			float *pl, *ql;
			for(j=0; j<dimV-1; j++)
			{
				pl = wArray.PtrIn + wArray.DimIn*j*du;
				ql = wArray.PtrOut + wArray.DimOut*j*dimU;
				for(i=0; i<du; i++)
				{
					for(k=0; k<wArray.DimIn; k++)
					{
						ql[k] = pl[k];
					}
					pl += wArray.DimIn;
					ql += wArray.DimOut;
				}
			}
		}

		output->GetPointData()->SetScalars(wArray.ArrOut);
		wArray.ArrOut->Delete();
	}
}

