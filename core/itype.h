/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#ifndef ITYPE_H
#define ITYPE_H


#include "icolor.h"
#include "idataid.h"
#include "ipair.h"
#include "istring.h"
#include "ivector3D.h"


//
//  Types of object properties 
//
namespace iType
{
	enum TypeId
	{
		None = 0,
		Int,
		Bool,
		Float,
		Double,
		String,
		DataType,
		Pair,
		Color,
		Vector
	};


	template<TypeId id> struct Traits
	{
	};


	//
	//  "Primitive" types
	//
	template<> struct Traits<Int>
	{
		typedef int Type;
		typedef int ContainerType;
		static const iString& TypeName();
		static iString ValueToString(int val, bool exact);
		static bool StringToValue(const iString &str, int &val);
	};


	template<> struct Traits<Bool>
	{
		typedef bool Type;
		typedef bool ContainerType;
		static const iString& TypeName();
		static iString ValueToString(bool val, bool exact);
		static bool StringToValue(const iString &str, bool &val);
	};


	template<> struct Traits<Float>
	{
		typedef float Type;
		typedef float ContainerType;
		static const iString& TypeName();
		static iString ValueToString(float val, bool exact);
		static bool StringToValue(const iString &str, float &val);
	};


	template<> struct Traits<Double>
	{
		typedef double Type;
		typedef double ContainerType;
		static const iString& TypeName();
		static iString ValueToString(double val, bool exact);
		static bool StringToValue(const iString &str, double &val);
	};


	template<> struct Traits<String>
	{
		typedef const iString& Type;
		typedef iString ContainerType;
		static const iString& TypeName();
		static iString ValueToString(const iString& val, bool exact);
		static bool StringToValue(const iString &str, iString &val);
	};


	template<> struct Traits<DataType>
	{
		typedef const iDataType& Type;
		typedef iDataId ContainerType;
		static const iString& TypeName();
		static const iString& ValueToString(const iDataType& val, bool exact);
		static bool StringToValue(const iString &str, iDataId &val);
	};


	//
	//  Compound types
	//
	template<> struct Traits<Pair>
	{
		typedef const iPair& Type;
		typedef iPair ContainerType;
		static const iString& TypeName();
		static iString ValueToString(const iPair& val, bool exact);
		static bool StringToValue(const iString &str, iPair &val);
		static const int NumFields = 2;
		static const TypeId FieldTypeId = Float;
	};


	template<> struct Traits<Color>
	{
		typedef const iColor& Type;
		typedef iColor ContainerType;
		static const iString& TypeName();
		static iString ValueToString(const iColor& val, bool exact);
		static bool StringToValue(const iString &str, iColor &val);
		static const int NumFields = 4;
		static const TypeId FieldTypeId = Int;
	};


	template<> struct Traits<Vector>
	{
		typedef const iVector3D& Type;
		typedef iVector3D ContainerType;
		static const iString& TypeName();
		static iString ValueToString(const iVector3D& val, bool exact);
		static bool StringToValue(const iString &str, iVector3D &val);
		static const int NumFields = 3;
		static const TypeId FieldTypeId = Double;
	};
};


#endif  // ITYPE_H
