//LICENSE A

//
//  The IFrIT logo has been created using:
//  1. Microsoft Agent software and Microsoft Agent character "Genie"; copyright (c) 1996-1998 Microsoft Corporation. All rights reserved.
//  2. ACS Viewer 1.1, build 12; copyright (c) 2005 Grigory Filatov.
//
#include "irunner.h"

#include <string.h>

#ifdef I_DEBUG
#include <stdlib.h>
#include "ihelpfactory.h"
#endif


namespace ifrit
{
	void TestPerformance();
};


int main(int argc, const char **argv)
{

#ifdef I_DEBUG
	//iHelpFactory::CreateUserGuide();
	//exit(0);
#endif

	//
	//  Skip program name
	//
	argc--;
	argv++;

	//
	//  Check if a test call
	//
	if(argc==1 && (strcmp(argv[0],"test")==0 || strcmp(argv[0],"-test")==0 || strcmp(argv[0],"--test")==0))
	{
		ifrit::TestPerformance();
		return 0;
	}

	iRunner app; // that loads the factories
	app.Run(argc,argv);

	return 0;
}


#include "ivtk.h"

#include <stdio.h>

#include <vtkActor.h>
#include <vtkCamera.h>
#include <vtkCubeSource.h>
#include <vtkGlyph3D.h>
#include <vtkImageData.h>
#include <vtkLookupTable.h>
#include <vtkOutputWindow.h>
#include <vtkPointData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkTimerLog.h>


namespace ifrit
{
	float TestRenderingPerformance(vtkRenderer *ren)
	{
		//
		// Set up the timer
		//
		ren->GetActiveCamera()->SetPosition(0.0,0.0,1.0);
		ren->ResetCamera();

		ren->GetRenderWindow()->Render();

		vtkTimerLog *timer = vtkTimerLog::New();
		timer->StartTimer();

		int i;
		float r = 0;
		do
		{
			for(i=0; i<35; i++)
			{
				ren->GetActiveCamera()->Azimuth(9);
				ren->GetRenderWindow()->Render();
			}
			for(i=0; i<35; i++)
			{
				ren->GetActiveCamera()->Elevation(9);
				ren->GetActiveCamera()->OrthogonalizeViewUp();
				ren->GetRenderWindow()->Render();
			}
			for(i=0; i<40; i++)
			{
				ren->GetActiveCamera()->Roll(9);
				ren->GetRenderWindow()->Render();
			}
			for(i=0; i<5; i++)
			{
				ren->GetActiveCamera()->Elevation(9);
				ren->GetActiveCamera()->OrthogonalizeViewUp();
				ren->GetRenderWindow()->Render();
			}
			for(i=0; i<5; i++)
			{
				ren->GetActiveCamera()->Azimuth(9);
				ren->GetRenderWindow()->Render();
			}
			timer->StopTimer();
			r += 1;
		}
		while(timer->GetElapsedTime() < 3.0);

		r /= timer->GetElapsedTime();

		timer->Delete();

		return r;
	}


	void TestPerformance()
	{
		int i;
		const float renRateScale[2] = { 7.5f, 7.5f };

		vtkRenderer *ren = vtkRenderer::New();
		vtkRenderWindow *win = vtkRenderWindow::New();
		win->AddRenderer(ren);
		ren->Delete();

		vtkImageData *data = vtkImageData::New();
		data->SetDimensions(17,17,17);
		data->SetSpacing(0.125,0.125,0.125);
		data->SetOrigin(-1.0,-1.0,-1.0);
#ifdef IVTK_5
		data->SetScalarType(VTK_UNSIGNED_CHAR);
		data->SetNumberOfScalarComponents(1);
		data->AllocateScalars();
#else
		data->AllocateScalars(VTK_UNSIGNED_CHAR,1);
#endif

		unsigned char *ptr = (unsigned char *)data->GetScalarPointer();
		for(i=0; i<data->GetNumberOfPoints(); i++)
		{
			ptr[i] = i % 256;
		}

		vtkCubeSource *cube = vtkCubeSource::New();
		cube->Update();

		vtkGlyph3D *glyph = vtkGlyph3D::New();
		glyph->SetScaleModeToDataScalingOff();
		glyph->SetColorModeToColorByScalar();
		glyph->SetScaleFactor(0.08);
		glyph->SetSourceConnection(cube->GetOutputPort());
		glyph->SetInputData(data);
		cube->Delete();
		glyph->Update();

		vtkPolyDataMapper *mapper = vtkPolyDataMapper::New();
		vtkActor *actor = vtkActor::New();

		mapper->ScalarVisibilityOn();
		mapper->SetColorModeToMapScalars();

		vtkLookupTable *lut = vtkLookupTable::New();
		lut->SetRange(0.0,255.0);
		//lut->SetHueRange(0.0,255.0);
		mapper->SetLookupTable(lut);
		mapper->UseLookupTableScalarRangeOn();
		lut->Delete();

		actor->GetProperty()->SetColor(0.0,0.0,0.0);
		actor->SetMapper(mapper);
		mapper->Delete();

		mapper->SetInputConnection(glyph->GetOutputPort());
		glyph->Delete();

		ren->AddActor(actor);
		actor->Delete();

		//
		// set the scene
		//
		win->SetSize(512,512);
		ren->SetBackground(1.0,1.0,1.0);

		//
		// draw the resulting scene
		//
		win->SetWindowName("IFrIT - Performance Test");
		win->Render();
		win->SetWindowName("IFrIT - Performance Test");

		actor->GetProperty()->SetOpacity(1.0);
		actor->GetProperty()->SetColor(0.0,0.0,1.0);
		float rr1 = TestRenderingPerformance(ren)*renRateScale[0];

		actor->GetProperty()->SetOpacity(0.03);
		actor->GetProperty()->SetColor(1.0,0.0,0.0);
		float rr2 = TestRenderingPerformance(ren)*renRateScale[1];

		char str[1000];
		sprintf(str,
			"Rendering performance:\n\n"
			"Opaque:\t\t %.1f\n"
			"Transparent:\t %.1f\n\n"
			"Representative rangers for values:\n"
			"1-2\tLaptop with a chipset\n"
			"5-20\tDesktop with a low-end video card\n"
			"50-200\tWorkstation with a high-end video card\n\n"
			"VTK Version: %s\n",rr1,rr2,VTK_VERSION);

		vtkOutputWindow::GetInstance()->DisplayText(str);

		data->Delete();
		win->RemoveRenderer(ren);
		win->Delete();
	}
};
