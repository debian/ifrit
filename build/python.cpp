

#include "iconfigure.h"
#if ISCRIPT_INCLUDED(ISCRIPT_PYTHON)
#include <Python.h>


namespace ipyPrivate
{
	PyObject* Start(PyObject *module, PyObject *args);
	void Free(void *ptr);
};


PyMethodDef ModuleMethods[] =
{
	{"start", ipyPrivate::Start, METH_VARARGS, "Start IFrIT."},
	{NULL, NULL, 0, NULL}        /* Sentinel */
};

#if (PY_MAJOR_VERSION > 2)

PyModuleDef Module =
{
	PyModuleDef_HEAD_INIT,
	"ifrit",
	"Module that provides access to IFrIT objects and their properties.",
	0,
	ModuleMethods, NULL, NULL, NULL, NULL
};

PyMODINIT_FUNC PyInit_ifrit(void) 
{
	//
	//  Create the module
	//
	Module.m_free = &ipyPrivate::Free;

	PyObject* module = PyModule_Create(&Module);
	return module;
}
#else
#if (PY_MINOR_VERSION < 7)
#error IFrIT needs Python 2.7 or Python 3
#endif

PyMODINIT_FUNC initifrit(void) 
{
	//
	//  Create the module
	//
	PyObject *Module = Py_InitModule("ifrit",ModuleMethods);
	Py_InitModule4("ifrit",ModuleMethods,"Module that provides access to IFrIT objects and their properties.",Module,PYTHON_API_VERSION);
}
#endif

#endif
