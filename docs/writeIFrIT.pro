;
;  Write to text scalar field data file
;
pro WriteIFrITUniformScalarsTxtFile, n1, n2, n3, var1, var2, var3, filename
; n1, n2, n3: size of the computational mesh in 3 directions
; var1, var2, var3: three scalar variables
; filename: name of the file
openw, 1, filename
printf, 1, n1, n2, n3
for k=0,n3-1 do $
for j=0,n2-1 do $
for i=0,n1-1 do $
printf, 1, var1[i,j,k], var2[i,j,k], var3[i,j,k]
close, 1
end
;
;  Write to binary scalar field data file
;
pro WriteIFrITUniformScalarsBinFile, n1, n2, n3, var1, var2, var3, filename
; n1, n2, n3: size of the computational mesh in 3 directions
; var1, var2, var3: three scalar variables
; filename: name of the file
openw, 1, filename, /F77_UNFORMATTED
writeu, 1, long([n1,n2,n3])
writeu, 1, var1
writeu, 1, var2
writeu, 1, var3
close, 1
end
;
;  Write to text uniform vectors data file
;
pro WriteIFrITUniformVectorsTxtFile, n1, n2, n3, vect, filename
; n1, n2, n3: size of the computational mesh in 3 directions
; vect[3,n1,n2,n3]: uniform vectors
; filename: name of the file
openw, 1, filename
printf, 1, n1, n2, n3
for k=0,n3-1 do $
for j=0,n2-1 do $
for i=0,n1-1 do $
printf, 1, vect[0,i,j,k], vect[1,i,j,k], vect[2,i,j,k]
close, 1
end
;
;  Write to binary uniform vectors data file
;
pro WriteIFrITUniformVectorsBinFile, n1, n2, n3, vect, filename
; n1, n2, n3: size of the computational mesh in 3 directions
; vect[3,n1,n2,n3]: uniform vectors
; filename: name of the file
openw, 1, filename, /F77_UNFORMATTED
writeu, 1, long([n1,n2,n3])
writeu, 1, vect[0,*,*,*]
writeu, 1, vect[1,*,*,*]
writeu, 1, vect[2,*,*,*]
close, 1
end
;
;  Write to text uniform tensors data file
;
pro WriteIFrITUniformTensorsTxtFile, n1, n2, n3, tens, filename
; n1, n2, n3: size of the computational mesh in 3 directions
; tens[6,n1,n2,n3]: uniform tensors
; filename: name of the file
openw, 1, filename
printf, 1, n1, n2, n3
for k=0,n3-1 do $
for j=0,n2-1 do $
for i=0,n1-1 do $
printf, 1, tens[0,i,j,k], tens[1,i,j,k], tens[2,i,j,k], $
           tens[3,i,j,k], tens[4,i,j,k], tens[5,i,j,k]
close, 1
end
;
;  Write to binary uniform tensors data file
;
pro WriteIFrITUniformTensorsBinFile, n1, n2, n3, tens, filename
; n1, n2, n3: size of the computational mesh in 3 directions
; tens[6,n1,n2,n3]: uniform vectors
; filename: name of the file
openw, 1, filename, /F77_UNFORMATTED
writeu, 1, long([n1,n2,n3])
writeu, 1, tens[0,*,*,*]
writeu, 1, tens[1,*,*,*]
writeu, 1, tens[2,*,*,*]
writeu, 1, tens[3,*,*,*]
writeu, 1, tens[4,*,*,*]
writeu, 1, tens[5,*,*,*]
close, 1
end
;
;  Write to text basic particles data file
;
pro WriteIFrITBasicParticlesTxtFile, n, xl, yl, zl, xh, yh, zh, $
x, y, z, attr1, attr2, attr3, filename
; n: number of particles
; xl, yl, zl, xh, yh, zh: bounding box (can be double)
; x, y, z: particle positions (can be double)
; attr1, attr2, attr3: particle attributes/
; filename: name of the file
openw, 1, filename
printf, 1, n
printf, 1, xl, yl, zl, xh, yh, zh
for i=0,n-1 do $
printf, 1, x[i], y[i], z[i], attr1[i], attr2[i], attr3[i]
close, 1
end
;
;  Write to binary basic particles data file
;
pro WriteIFrITBasicParticlesBinFile, n, xl, yl, zl, xh, yh, zh, $
x, y, z, attr1, attr2, attr3, filename
; n: number of particles
; xl, yl, zl, xh, yh, zh: bounding box (can be double)
; x, y, z: particle positions (can be double)
; attr1, attr2, attr3: particle attributes/
; filename: name of the file
openw, 1, filename, /F77_UNFORMATTED
writeu, 1, long(n)
writeu, 1, float([xl,yl,zl,xh,yh,zh])
writeu, 1, x
writeu, 1, y
writeu, 1, z
writeu, 1, attr1
writeu, 1, attr2
writeu, 1, attr3
close, 1
end
