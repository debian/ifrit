C
C  Write to text uniform scalars data file
C
      subroutine WriteIFrITUniformScalarsTxtFile(n1,n2,n3,var1,var2,var3,
     .     filename)
      integer n1, n2, n3  ! Size of the computational mesh in 3 directions
      real*4 var1(n1,n2,n3)
      real*4 var2(n1,n2,n3) ! Three scalar variables
      real*4 var3(n1,n2,n3)  
      character*(*) filename  ! Name of the file
      open(unit=1, file=filename)
      write(1,*) n1, n2, n3
      do k=1,n3
         do j=1,n2
            do i=1,n1
               write(1,*) var1(i,j,k), var2(i,j,k), var3(i,j,k)
            enddo
         enddo
      enddo
      close(1)
      return
      end
C
C  Write to binary uniform scalars data file
C
      subroutine WriteIFrITUniformScalarsBinFile(n1,n2,n3,var1,var2,var3,
     .     filename)
      integer n1, n2, n3  ! Size of the computational mesh in 3 directions
      real*4 var1(n1,n2,n3)
      real*4 var2(n1,n2,n3) ! Three scalar variables
      real*4 var3(n1,n2,n3)  
      character*(*) filename  ! Name of the file
      open(unit=1, file=filename, form='unformatted')
      write(1) n1, n2, n3
      write(1) (((var1(i,j,k),i=1,n1),j=1,n2),k=1,n3)
      write(1) (((var2(i,j,k),i=1,n1),j=1,n2),k=1,n3)
      write(1) (((var3(i,j,k),i=1,n1),j=1,n2),k=1,n3)
      close(1)
      return
      end
C
C  Write to text uniform vectors data file
C
      subroutine WriteIFrITUniformVectorsTxtFile(n1,n2,n3,vect,filename)
      integer n1, n2, n3  ! Size of the computational mesh in 3 directions
      real*4 vect(3,n1,n2,n3)  ! Vector field
      character*(*) filename  ! Name of the file
      open(unit=1, file=filename)
      write(1,*) n1, n2, n3
      do k=1,n3
         do j=1,n2
            do i=1,n1
               write(1,*) vect(1,i,j,k), vect(2,i,j,k), vect(3,i,j,k)
            enddo
         enddo
      enddo
      close(1)
      return
      end
C
C  Write to binary uniform vectors data file
C
      subroutine WriteIFrITUniformVectorsBinFile(n1,n2,n3,vect,filename)
      integer n1, n2, n3  ! Size of the computational mesh in 3 directions
      real*4 vect(3,n1,n2,n3)  ! Vector field
      character*(*) filename  ! Name of the file
      open(unit=1, file=filename, form='unformatted')
      write(1) n1, n2, n3
      write(1) (((vect(1,i,j,k),i=1,n1),j=1,n2),k=1,n3)
      write(1) (((vect(2,i,j,k),i=1,n1),j=1,n2),k=1,n3)
      write(1) (((vect(3,i,j,k),i=1,n1),j=1,n2),k=1,n3)
      close(1)
      return
      end
C
C  Write to text uniform tensors data file
C
      subroutine WriteIFrITUniformTensorsTxtFile(n1,n2,n3,tens,filename)
      integer n1, n2, n3  ! Size of the computational mesh in 3 directions
      real*4 tens(6,n1,n2,n3)  ! Tensor field
      character*(*) filename  ! Name of the file
      open(unit=1, file=filename)
      write(1,*) n1, n2, n3
      do k=1,n3
         do j=1,n2
            do i=1,n1
               write(1,*) tens(1,i,j,k), tens(2,i,j,k), tens(3,i,j,k),
     .              tens(4,i,j,k), tens(5,i,j,k), tens(6,i,j,k)
            enddo
         enddo
      enddo
      close(1)
      return
      end
C
C  Write to binary uniform tensors data file
C
      subroutine WriteIFrITUniformTensorsBinFile(n1,n2,n3,tens,filename)
      integer n1, n2, n3  ! Size of the computational mesh in 3 directions
      real*4 tens(6,n1,n2,n3)  ! Tensor field
      character*(*) filename  ! Name of the file
      open(unit=1, file=filename, form='unformatted')
      write(1) n1, n2, n3
      write(1) (((tens(1,i,j,k),i=1,n1),j=1,n2),k=1,n3)
      write(1) (((tens(2,i,j,k),i=1,n1),j=1,n2),k=1,n3)
      write(1) (((tens(3,i,j,k),i=1,n1),j=1,n2),k=1,n3)
      write(1) (((tens(4,i,j,k),i=1,n1),j=1,n2),k=1,n3)
      write(1) (((tens(5,i,j,k),i=1,n1),j=1,n2),k=1,n3)
      write(1) (((tens(6,i,j,k),i=1,n1),j=1,n2),k=1,n3)
      close(1)
      return
      end
C
C  Write to text basic particles data file
C
      subroutine WriteIFrITBasicParticlesTxtFile(n,xl,yl,zl,xh,yh,zh,
     .     x,y,z,attr1,attr2,attr3,filename)
      integer n  ! Number of particles
      real*4 xl, yl, zl, xh, yh, zh  ! Bounding box 
      real*4 x(n), y(n), z(n)  ! Particle positions (can be real*8)
      real*4 attr1(n), attr2(n), attr3(n)  ! Particle attributes
      character*(*) filename  ! Name of the file
      open(unit=1, file=filename)
      write(1,*) n
      write(1,*) xl, yl, zl, xh, yh, zh
      do i=1,n
         write(1,*) x(i), y(i), z(i), attr1(i), attr2(i), attr3(i)
      enddo
      close(1)
      return
      end
C
C  Write to binary basic particles data file
C
      subroutine WriteIFrITBasicParticlesBinFile(n,xl,yl,zl,xh,yh,zh,
     .     x,y,z,attr1,attr2,attr3,filename)
      integer n  ! Number of particles
      real*4 xl, yl, zl, xh, yh, zh  ! Bounding box
      real*4 x(n), y(n), z(n)  ! Particle positions (can be real*8)
      real*4 attr1(n), attr2(n), attr3(n)  ! Particle attributes
      character*(*) filename  ! Name of the file
      open(unit=1, file=filename, form='unformatted')
      write(1) n
      write(1) xl, yl, zl, xh, yh, zh
      write(1) (x(i),i=1,n)
      write(1) (y(i),i=1,n)
      write(1) (z(i),i=1,n)
      write(1) (attr1(i),i=1,n)
      write(1) (attr2(i),i=1,n)
      write(1) (attr3(i),i=1,n)
      close(1)
      return
      end
