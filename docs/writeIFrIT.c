
#include <stdio.h>

/*  Write to text uniform scalars data file  */

int WriteIFrITUniformScalarsTxtFile(
    int n1, int n2, int n3, /* Size of the computational mesh in 3 directions */
    float *var1, float *var2, float *var3, /* Three scalar variables */
    char *filename) /* Name of the file */
{
   int i, j, k; FILE *F;
   F = fopen(filename,"w"); if(F == NULL) return 1;
   fprintf(F,"%d %d %d\n",n1,n2,n3);
   for(k=0; k<n3; k++)
   {
      for(j=0; j<n2; j++)
      {
         for(i=0; i<n1; i++)
         {
            fprintf(F,"%g %g %g\n",var1[i+n1*(j+n2*k)],
                                   var2[i+n1*(j+n2*k)],
                                   var3[i+n1*(j+n2*k)]);
         }
      }
   }
   fclose(F);
   return 0;
}

/*  Write to binary uniform scalars data file  */

int WriteIFrITUniformScalarsBinFile(
    int n1, int n2, int n3, /* Size of the computational mesh in 3 directions */
    float *var1, float *var2, float *var3, /* Three scalar variables */
    char *filename) /* Name of the file */
{
   int ntemp; FILE *F; /* ntemp should be declared long on a 16-bit machine */
   F = fopen(filename,"wb"); if(F == NULL) return 1;
   ntemp = 12;
   fwrite(&ntemp,4,1,F);
   fwrite(&n1,4,1,F);
   fwrite(&n2,4,1,F);
   fwrite(&n3,4,1,F);
   fwrite(&ntemp,4,1,F);
   ntemp = 4*n1*n2*n3;
   fwrite(&ntemp,4,1,F); fwrite(var1,4,n1*n2*n3,F); fwrite(&ntemp,4,1,F);
   fwrite(&ntemp,4,1,F); fwrite(var2,4,n1*n2*n3,F); fwrite(&ntemp,4,1,F);
   fwrite(&ntemp,4,1,F); fwrite(var3,4,n1*n2*n3,F); fwrite(&ntemp,4,1,F);
   fclose(F);
   return 0;
}


/*  Write to text uniform vectors data file  */

int WriteIFrITUniformVectorsTxtFile(
    int n1, int n2, int n3, /* Size of the computational mesh in 3 directions */
    float *vect, /* Vector field */
    char *filename) /* Name of the file */
{
   int i, j, k; FILE *F;
   F = fopen(filename,"w"); if(F == NULL) return 1;
   fprintf(F,"%d %d %d\n",n1,n2,n3);
   for(k=0; k<n3; k++)
   {
      for(j=0; j<n2; j++)
      {
         for(i=0; i<n1; i++)
         {
            fprintf(F,"%g %g %g\n",vect[0+3*(i+n1*(j+n2*k))],
                                   vect[1+3*(i+n1*(j+n2*k))],
                                   vect[2+3*(i+n1*(j+n2*k))]);
         }
      }
   }
   fclose(F);
   return 0;
}

/*  Write to binary uniform vectors data file  */

int WriteIFrITUniformVectorsBinFile(
    int n1, int n2, int n3, /* Size of the computational mesh in 3 directions */
    float *vect, /* Vector field */
    char *filename) /* Name of the file */
{
   int i, ntemp; FILE *F; /* ntemp should be declared long on a 16-bit machine */
   F = fopen(filename,"wb"); if(F == NULL) return 1;
   ntemp = 12;
   fwrite(&ntemp,4,1,F);
   fwrite(&n1,4,1,F);
   fwrite(&n2,4,1,F);
   fwrite(&n3,4,1,F);
   fwrite(&ntemp,4,1,F);
   ntemp = 4*n1*n2*n3;
   fwrite(&ntemp,4,1,F); 
   for(i=0; i<n1*n2*n3; i++) fwrite(vect+3*i+0,4,1,F); 
   fwrite(&ntemp,4,1,F);
   fwrite(&ntemp,4,1,F); 
   for(i=0; i<n1*n2*n3; i++) fwrite(vect+3*i+1,4,1,F); 
   fwrite(&ntemp,4,1,F);
   fwrite(&ntemp,4,1,F);
   for(i=0; i<n1*n2*n3; i++) fwrite(vect+3*i+2,4,1,F); 
   fwrite(&ntemp,4,1,F);
   fclose(F);
   return 0;
}

/*  Write to text uniform tensors data file  */

int WriteIFrITUniformTensorsTxtFile(
    int n1, int n2, int n3, /* Size of the computational mesh in 3 directions */
    float *tens, /* Tensor field */
    char *filename) /* Name of the file */
{
   int i, j, k; FILE *F;
   F = fopen(filename,"w"); if(F == NULL) return 1;
   fprintf(F,"%d %d %d\n",n1,n2,n3);
   for(k=0; k<n3; k++)
   {
      for(j=0; j<n2; j++)
      {
         for(i=0; i<n1; i++)
         {
            fprintf(F,"%g %g %g %g %g %g\n",tens[0+6*(i+n1*(j+n2*k))],
                                            tens[1+6*(i+n1*(j+n2*k))],
                                            tens[2+6*(i+n1*(j+n2*k))],
                                            tens[3+6*(i+n1*(j+n2*k))],
                                            tens[4+6*(i+n1*(j+n2*k))],
                                            tens[5+6*(i+n1*(j+n2*k))]);
         }
      }
   }
   fclose(F);
   return 0;
}

/*  Write to binary uniform tensors data file  */

int WriteIFrITUniformTensorsBinFile(
    int n1, int n2, int n3, /* Size of the computational mesh in 3 directions */
    float *tens, /* Tensor field */
    char *filename) /* Name of the file */
{
   int i, ntemp; FILE *F; /* ntemp should be declared long on a 16-bit machine */
   F = fopen(filename,"wb"); if(F == NULL) return 1;
   ntemp = 12;
   fwrite(&ntemp,4,1,F);
   fwrite(&n1,4,1,F);
   fwrite(&n2,4,1,F);
   fwrite(&n3,4,1,F);
   fwrite(&ntemp,4,1,F);
   ntemp = 4*n1*n2*n3;
   fwrite(&ntemp,4,1,F); 
   for(i=0; i<n1*n2*n3; i++) fwrite(tens+6*i+0,4,1,F); 
   fwrite(&ntemp,4,1,F);
   fwrite(&ntemp,4,1,F); 
   for(i=0; i<n1*n2*n3; i++) fwrite(tens+6*i+1,4,1,F); 
   fwrite(&ntemp,4,1,F);
   fwrite(&ntemp,4,1,F);
   for(i=0; i<n1*n2*n3; i++) fwrite(tens+6*i+2,4,1,F); 
   fwrite(&ntemp,4,1,F);
   fwrite(&ntemp,4,1,F);
   for(i=0; i<n1*n2*n3; i++) fwrite(tens+6*i+3,4,1,F); 
   fwrite(&ntemp,4,1,F);
   fwrite(&ntemp,4,1,F);
   for(i=0; i<n1*n2*n3; i++) fwrite(tens+6*i+4,4,1,F); 
   fwrite(&ntemp,4,1,F);
   fwrite(&ntemp,4,1,F);
   for(i=0; i<n1*n2*n3; i++) fwrite(tens+6*i+5,4,1,F); 
   fwrite(&ntemp,4,1,F);
   fclose(F);
   return 0;
}

/*  Write to text basic particles data file  */

int WriteIFrITBasicParticlesTxtFile(
    int n, /* Number of particles */
    float xl, float yl, float zl, float xh, float yh, float zh, /* Bounding box */
    float *x, float *y, float *z, /* Particle positions (can be double) */
    float *attr1, float *attr2, float *attr3, /* Particle attributes */
    char *filename) /* Name of the file */
{
   int i; FILE *F;
   F = fopen(filename,"w"); if(F == NULL) return 1;
   fprintf(F,"%d\n",n);
   fprintf(F,"%g %g %g %g %g %g\n",xl,yl,zl,xh,yh,zh);
   for(i=0; n>i; i++)
   {
      fprintf(F,"%g %g %g %g %g %g\n",x[i],y[i],z[i],
                          attr1[i],attr2[i],attr3[i]);
   }
   fclose(F);
   return 0;
}

/*  Write to binary basic particles data file  */

int WriteIFrITBasicParticlesBinFile(
    int n, /* Number of particles */
    float xl, float yl, float zl, float xh, float yh, float zh, /* Bounding box */
    float *x, float *y, float *z, /* Particle positions (can be double) */
    float *attr1, float *attr2, float *attr3, /* Particle attributes */
    char *filename) /* Name of the file */
{
   int i, ntemp; FILE *F;
   F = fopen(filename,"wb"); if(F == NULL) return 1;
   ntemp = 4;
   fwrite(&ntemp,4,1,F); fwrite(&n,4,1,F); fwrite(&ntemp,4,1,F);
   ntemp = 24;fwrite(&ntemp,4,1,F);
   fwrite(&xl,4,1,F);
   fwrite(&yl,4,1,F);
   fwrite(&zl,4,1,F);
   fwrite(&xh,4,1,F);
   fwrite(&yh,4,1,F);
   fwrite(&zh,4,1,F);
   fwrite(&ntemp,4,1,F);
   ntemp = sizeof(x[0])*n;
   fwrite(&ntemp,4,1,F); fwrite(x,sizeof(x[0]),n,F); fwrite(&ntemp,4,1,F);
   fwrite(&ntemp,4,1,F); fwrite(y,sizeof(y[0]),n,F); fwrite(&ntemp,4,1,F);
   fwrite(&ntemp,4,1,F); fwrite(z,sizeof(z[0]),n,F); fwrite(&ntemp,4,1,F);
   ntemp = 4*n;
   fwrite(&ntemp,4,1,F); fwrite(attr1,4,n,F); fwrite(&ntemp,4,1,F);
   fwrite(&ntemp,4,1,F); fwrite(attr2,4,n,F); fwrite(&ntemp,4,1,F);
   fwrite(&ntemp,4,1,F); fwrite(attr3,4,n,F); fwrite(&ntemp,4,1,F);
   fclose(F);
   return 0;
}

