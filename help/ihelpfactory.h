/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/

//
//  This interface provides help text for all IFrIT components. It can also
//  create shell help systems and a hardcopy user guide.
//

#ifndef IHELPFACTORY_H
#define IHELPFACTORY_H


#include "iarray.h"
#include "istring.h"


class iObject;
class iProperty;


struct iHelpData
{
	unsigned int CompressedSize;
	int Length;
	const char *Tag;
	const char *Title;
	const char *Extra;
	const char *Buffer;
};


class iHelpDataBuffer
{

	friend class iHelpFactory;

public:

 	iHelpDataBuffer(const iHelpDataBuffer&);

	iString GetTitle() const;
	iString GetText(int length = 0) const;
	iString GetHTML(bool untagged = false) const;

	inline bool IsNull() const { return mData.Tag[0] == 0; }

	//
	//  Extra controls
	//
	const iString GetExtraTag(int type) const;
	inline const char* GetTag() const { return mData.Tag; }

protected:

	//
	//  Cannot be created except by the factory or children
	//
	iHelpDataBuffer(iHelpData &data);
 
	void TranformDataToString(iString &s) const;

	iHelpData &mData;

private:

 	iHelpDataBuffer();  // Not implemented.
	void operator=(const iHelpDataBuffer&);  // Not implemented.
};


//
//  Implement the factory as a static class (rather than a singleton) 
//  so that it does not depend on the order of initialization of static variables
//
class iHelpFactory
{

public:

	enum InfoSource
	{
		_UserGuide,
		_ShellReference,
		_ObjectReference,
		_All
	};

	enum ObjectType
	{
		_AllObjects,
		_DataObjects,
		_MainObjects
	};

	struct ChapterInfo
	{
		iString Name;
		const char *Tag;
		const char **SectionTags;
		ChapterInfo(const char *t, const iString &n, const char **s) : Name(n), Tag(t), SectionTags(s){}
	};

#ifdef I_DEBUG
	static void CreateUserGuide();
#endif

	static void CreateTopicList(iString &text);

	//
	//  Find data by tag
	//
	static iHelpDataBuffer FindData(const char *ref, InfoSource source = _All, bool replaceRefs = true, int level = 0);

	//
	//  Functions for working with private data - they are the only ones that explicitly depend on the extension configuration
	//
	static void InterpretExtraElements(iString &text, const iString &tag = iString());
	static const iString& GetIconTag(const char *extra);

	static void RegisterObject(const iString &tag);
	static void RegisterObjectProperty(const iString &tag);

	static const iString FormObjectTree(ObjectType type);
	static const iString FormReference(const iString &tag, bool bold);
	static const iString FormObjectDescription(const iString &tag, bool full);
	static const iString FormPropertyDescription(const iString &tag, bool line, bool full);

private:


	static iOrderedArray<iString> mObjectTags;
	static iOrderedArray<iString> mPropertyTags;
};


#endif // IHELPFACTORY_H

