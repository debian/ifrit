/*=========================================================================

  Program:   Ionization FRont Interactive Tool (IFRIT)
  Language:  C++


Copyright (c) 2002-2012 Nick Gnedin 
All rights reserved.

This file may be distributed and/or modified under the terms of the
GNU General Public License version 2 as published by the Free Software
Foundation and appearing in the file LICENSE.GPL included in the
packaging of this file.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=========================================================================*/


#include "ihelpfactory.h"


#include "iconfigure.h"
#include "ierror.h"
#include "ifile.h"
#include "iobject.h"
#include "iversion.h"

#include <vtkZLibDataCompressor.h>

//
//  Templates
//
#include "iarray.tlh"


using namespace iParameter;


#define I_CHECK_MISSING_REFERENCES


iOrderedArray<iString> iHelpFactory::mObjectTags;
iOrderedArray<iString> iHelpFactory::mPropertyTags;


namespace iHelpFactory_Private
{
	const int _IconTag = 0;
	const int _BeginExtraTag = 1;
	const int _EndExtraTag = 2;

	void Uncompress(const char *data, unsigned int csize, iString &str, int usize)
	{
		static vtkZLibDataCompressor *dataCompressor = vtkZLibDataCompressor::New();
		
		unsigned char *destData = (unsigned char *)str.GetWritePointer(usize);
		if(destData == 0)
		{
			str.Clear();
			return;
		}

		dataCompressor->Uncompress((unsigned char *)data,csize,destData,usize);
		destData[usize] = 0;
	}

	void WriteFile(const iString &fname, const iString &text)
	{
		iFile f(fname);
		if(f.Open(iFile::_Write,iFile::_Text))
		{
			f.WriteLine(text);
			f.Close();
		}
	}

	//
	//  A generic function for querying data
	//
	iHelpData* GetData(const char *tag, iHelpData *data, int &num, bool &sorted)
	{
		//
		//  Measure the number of records
		//
		if(num == -1)
		{
			num = 0;
			while(data[num].Length > -1) num++;
			//
			//  Check that the data are ordered
			//
			int i;
#ifdef I_DEBUG
			const char *is = 0;
			bool ss = sorted;
#endif
			sorted = true;
			for(i=1; i<num; i++)
			{
				if(strcmp(data[i-1].Tag,data[i].Tag) >= 0) 
				{
					sorted = false;
#ifdef I_DEBUG
					is = data[i].Tag;
#endif
				}
			}
#ifdef I_DEBUG
			if(ss && !sorted)
			{
				IBUG_WARN("Help data are not sorted.");
			}
#endif
		}
		
		if(sorted)
		{
			//
			//  If the data are ordered, use a fast binary search 
			//
			int c, ia, ib, ic;
			
			ia = 0; 
			ib = num - 1;
			while(ib > ia+1)
			{
				ic = (ia+ib)/2;
				c = strcmp(data[ic].Tag,tag);
				if(c < 0) ia = ic; else if(c > 0) ib = ic; else return &data[ic];
			}
			if(strcmp(data[ia].Tag,tag) == 0) 
			{
				return &data[ia];
			}
			else if(strcmp(data[ib].Tag,tag) == 0) 
			{
				return &data[ib];
			}
		}
		else
		{
			//
			//  If the data are not ordered, use a slow direct search 
			//
			int i;
			for(i=0; i<num; i++) if(strcmp(data[i].Tag,tag) == 0) 
			{
				return &data[i];
			}
		}
		
		return 0;
	}

	void CreateChapter(iString &text, const iHelpFactory::ChapterInfo info, iHelpFactory::InfoSource source, bool nl = false)
	{
		int i;
		
		text += "<p>&nbsp;";
		text += iString("<p><a name=\"") + info.Tag + "\"><h2>" + info.Name + "</h2></a>\n";
		
		for(i=0; info.SectionTags[i]!=0; i++)
		{
			const iHelpDataBuffer &b(iHelpFactory::FindData(info.SectionTags[i],source));
			if(!b.IsNull())
			{
				text += b.GetExtraTag(_BeginExtraTag) + "<a name=\"" + info.SectionTags[i] + "\"></a><p><h3>" + b.GetTitle() + "</h3><p>\n" + b.GetHTML(true) + b.GetExtraTag(_EndExtraTag);
			}
		}
		//
		//  Images
		//
		text.Replace("\"images::","\"images/");
		text.Replace("\"ug::","\"images/");

		if(nl) text += "<!-- PAGE BREAK -->\n";
	}

	void CreateTopicEntry(iString &text, const iHelpFactory::ChapterInfo info, iHelpFactory::InfoSource source)
	{
		int i;
		
		text += "<p><b>" + info.Name + "</b><ul>";
		
		for(i=0; info.SectionTags[i]!=0; i++)
		{
			const iHelpDataBuffer &b(iHelpFactory::FindData(info.SectionTags[i],source));
			if(!b.IsNull())
			{
				text += b.GetExtraTag(_BeginExtraTag) + "<li><a href=\"" + info.SectionTags[i] + "\">" + b.GetTitle() + "</a></li>" + b.GetExtraTag(_EndExtraTag);
			}
		}
		text += "</ul>";
	}

	void CreateCompressedHeader(const iString &label, iHelpData *data)
	{
		//
		//  Actually create the data
		//
		iString ws, wlist;

		iFile f("Code/help/ihelpfactorydata"+label+".h");
		if(!f.Open(iFile::_Write,iFile::_Text))  return;

		f.WriteLine("//LICENSE A");
		f.WriteLine("#if 0");
		f.WriteLine("#include \"../../Work/help/helpdata"+label+".txt\"");
		f.WriteLine("#else");
		f.WriteLine("static iHelpData data[] = {");

		int i, j, usize, csize;
		unsigned long cmax;
		//
		//  Thread-safe
		//
		vtkZLibDataCompressor *dataCompressor = vtkZLibDataCompressor::New();
		for(i=0; data[i].Length>-1; i++)
		{
			if(data[i].Buffer[0] == '&')
			{
				//
				//  Reference, write as is
				//
				wlist = "{ 0U, 0, \"" + iString(data[i].Tag) + "\",\"" + iString(data[i].Title) + "\",";
				if(data[i].Extra == 0) wlist += "0"; else wlist += iString("\"") + data[i].Extra + "\"";
				wlist += ",\n\"" + iString(data[i].Buffer) + "\"},";
			}
			else
			{
				//
				//  Not a reference, compress
				//
				usize = int(strlen(data[i].Buffer));
				cmax = dataCompressor->GetMaximumCompressionSpace(usize);
				csize = dataCompressor->Compress((const unsigned char *)data[i].Buffer,usize,(unsigned char*)ws.GetWritePointer(cmax),cmax);

				//
				//  Add list entry
				//
				wlist = "{" + iString::FromNumber(csize,"%u") + "," + iString::FromNumber(usize) + ",\"" + data[i].Tag + "\",\"" + data[i].Title + "\",";
				if(data[i].Extra == 0) wlist += "0"; else wlist += iString("\"") + data[i].Extra + "\"";
				wlist += ",\n\"";
				for(j=0; j<(int)csize; j++)
				{
					wlist += iString::FromNumber(int((unsigned char)ws[j]),"\\%o");
					if(j%300 == 299) wlist += "\"\n\"";
				}
				wlist += "\"},";
			}
			f.WriteLine(wlist);
		}
		dataCompressor->Delete();
		f.WriteLine("{ 0U, -1, 0, 0, 0, 0 }");
		f.WriteLine("};");
		f.WriteLine("#endif");
		f.Close();

		iOutput::Display(MessageType::Warning,"IFrIT is creating compressed help data...\nPlease compile again and re-run.");

		exit(0);
	}

	//
	//  Help data live inside these functions, hidden in a private namespace to avoid
	//  cluttering the class namespace. Is there a better design?
	//
	iHelpData* GetDataUG(const char *tag)
	{
		static bool sorted = false;
		static int num = -1;
		//
		//  data live here
		//
#include "ihelpfactorydataUG.h"
		if(data!=0 && data[0].CompressedSize==0U)
		{
			CreateCompressedHeader("UG",data);
		}

		return GetData(tag,data,num,sorted);
	}

	iHelpData* GetDataOR(const char *tag)
	{
		static bool sorted = true;
		static int num = -1;
		//
		//  data live here
		//
#include "ihelpfactorydataOR.h"
		if(data!=0 && data[0].CompressedSize==0U)
		{
			CreateCompressedHeader("OR",data);
		}

		return GetData(tag,data,num,sorted);
	}

	iHelpData* GetDataSR(const char *tag)
	{
		static bool sorted = false;
		static int num = -1;
		//
		//  data live here
		//
#include "ihelpfactorydataSR.h"
		if(data!=0 && data[0].CompressedSize==0U)
		{
			CreateCompressedHeader("SR",data);
		}

		return GetData(tag,data,num,sorted);
	}

	void TreeDisplayHelper(int id, const iArray<iString>& tags, iString &text)
	{
		text += iHelpFactory::FormReference(tags[id],true);

		iString s, cds = tags[id].Section("+",3);
		if(cds.IsEmpty()) return;

		text += "<ul>";
		int i, j, n = cds.Contains(',');
		iOrderedArray<iString> arr;
		for(i=0; i<=n; i++)
		{
			s = cds.Section(",",i,i);
			for(j=0; j<tags.Size(); j++)
			{
#ifdef I_DEBUG
				iString str = tags[j];
#endif
				if(tags[j].Section("+",0,0) == s)
				{
					arr.AddUnique(tags[j].Section("+",1,2));  // arr will be alphabetically ordered; including the parent is to separate Data.Particles/p from Window.Particles/p
					break;
				}
			}
		}
		for(i=0; i<arr.Size(); i++)
		{
			for(j=0; j<tags.Size(); j++)
			{
				if(tags[j].Section("+",1,2) == arr[i])
				{
					TreeDisplayHelper(j,tags,text);
				}
			}
		}
		text += "</ul>";
	}

	//
	//  Chapter components (they are small, so are kept here
	//
	const char *gdTags[] = { "ug.gd.ov", "ug.gd.gs", "ug.gd.op", "ug.gd.do", 0 };
	const iHelpFactory::ChapterInfo ugOverview("ug.gd","Overview",gdTags);

	const char *cnTags[] = { "ug.cn.i", "ug.cn.ms", "ug.cn.ev", "ug.cn.cl", "ug.cn.ef", 0 };
	const iHelpFactory::ChapterInfo ugControls("ug.cn","Running IFrIT",cnTags);

	const char *ffTags[] = { "ug.ff.i", "ug.ff.cs", "ug.ff.cv", "ug.ff.ct", "ug.ff.cp", 0 };
	const iHelpFactory::ChapterInfo ugFormats("ug.ff","File Formats",ffTags);

	const char *asTags[] = { "ug.as.i", "ug.as.f", "ug.as.r", 0 };
	const iHelpFactory::ChapterInfo ugAnimation("ug.as","Animation Support",asTags);

	//const char *scTags[] = { "ug.sc.i", "ug.sc.st", "ug.sc.va", "ug.sc.ex", "ug.sc.cc", "ug.sc.cs", "ug.sc.as", 0 };
	//const iHelpFactory::ChapterInfo ugScripts("ug.sc","Animation and Control Scripts",scTags);

	const char *paTags[] = { "ug.pa.i", 0 };
	const iHelpFactory::ChapterInfo ugPalettes("ug.pa","IFrIT Palettes",paTags);

	const char *ceTags[] = { "ug.ce.i", "ug.ce.f", "ug.ce.c", "ug.ce.l", 0 };
	const iHelpFactory::ChapterInfo ugCodes("ug.ce","Codes For Writing IFrIT Data Files",ceTags);

	const char *liTags[] = { "ug.li.i", "ug.li.g", "ug.li.h", 0 };
	const iHelpFactory::ChapterInfo ugLicense("ug.li","License Agreement",liTags);

	const char *clTags[] = { "sr.cl.i", "sr.os.i", 0 };
	const iHelpFactory::ChapterInfo ugCLShell("sr.cl","Non-GUI Shell Reference",clTags);

	const char *ggTags[] = { "sr.gg.i", "sr.gg.dsw", "sr.gg.dpe", "sr.gg.dde", "sr.gg.dfe", "sr.gg.dic", "sr.gg.dpi", "sr.gg.dpc", "sr.gg.hfo", "sr.gg.hcw", "sr.gg.hme", 0 };
	const iHelpFactory::ChapterInfo ugGGShell("sr.gg","GUI Shell Reference",ggTags);

	const char *orTags[] = { "ug.gd.op", "ug.gd.oh", 0 };
	const iHelpFactory::ChapterInfo ugObjects("or.op","Overview",orTags);
};


using namespace iHelpFactory_Private;

//
//  Helper class
//
iHelpDataBuffer::iHelpDataBuffer(iHelpData &data) : mData(data)
{
	if(mData.CompressedSize == 0)
	{
		//
		//  Uncompressed data, set the length
		//
		mData.Length = int(strlen(mData.Buffer));
	}
}


iHelpDataBuffer::iHelpDataBuffer(const iHelpDataBuffer &other) : mData(other.mData)
{
}


iString iHelpDataBuffer::GetTitle() const
{
	return mData.Title;
}


iString iHelpDataBuffer::GetText(int length) const
{
	iString s;

	this->TranformDataToString(s);
	//
	//  Default implementation uses HTML formatting, so strip formatting
	//
	s.ReformatHTMLToText(length);

	return s;
}


iString iHelpDataBuffer::GetHTML(bool untagged) const
{
	iString s;

	this->TranformDataToString(s);
	//
	//  Default implementation uses HTML formatting, so thus return the data, but
	//  add an anchor with the tag in front
	//
	if(untagged || s[0]=='*' || s[0]=='&') return s; else return iString("<a name=\"") + mData.Tag + "\"></a>" + s;
}


void iHelpDataBuffer::TranformDataToString(iString &s) const
{
	if(mData.CompressedSize > 0)
	{
		//
		//  Compressed data
		//
		Uncompress(mData.Buffer,mData.CompressedSize,s,mData.Length);
	}
	else
	{
		s = iString(mData.Buffer);
	}
}


const iString iHelpDataBuffer::GetExtraTag(int type) const
{
	static const iString none;

	switch(type)
	{
	case _IconTag:
		{
			return iHelpFactory::GetIconTag(mData.Extra);
		}
	case _BeginExtraTag:
		{
			if(mData.Extra!=0 && mData.Extra[0]=='-' && (mData.Extra[1]=='-' || mData.Extra[1]=='[')) return "<extra type="+iString(mData.Extra+2)+">"; else return none;
		}
	case _EndExtraTag:
		{
			if(mData.Extra!=0 && mData.Extra[0]=='-' && (mData.Extra[1]=='-' || mData.Extra[1]==']')) return "</extra>"; else return none;
		}
	default:
		{
			return none;
		}
	}
}


//
//  iHelpFactory implementation
//
iHelpDataBuffer iHelpFactory::FindData(const char *ref, InfoSource source, bool replaceRefs, int level)
{
	static iHelpData null = { 0U, 0, "", 0, 0, "* No help is available for this item." };
	iHelpData *tmp = 0;

	if(level > 9)
	{
		IBUG_WARN("References are looped.");
		return iHelpDataBuffer(null);
	}

	if(tmp==0 && (source==_All || source==_UserGuide))
	{
		tmp = GetDataUG(ref);
	}

	if(tmp==0 && (source==_All || source==_ShellReference))
	{
		tmp = GetDataSR(ref);
	}

	if(tmp==0 && (source==_All || source==_ObjectReference))
	{
		tmp = GetDataOR(ref);
		if(tmp == 0)
		{
			iString s = iString(ref).Section(".",0,1) + ".*";
			tmp = GetDataOR(s.ToCharPointer());
			if(tmp!=0 && tmp->Buffer!=0 && tmp->Buffer[0]=='&')
			{
				s = iString(tmp->Buffer+1) + "." + iString(ref).Section(".",2);
				return FindData(s.ToCharPointer(),_ObjectReference,replaceRefs,level+1);
			}
			else tmp = 0;
		}
	}

	if(tmp == 0)
	{
		//
		//  Special case - object hierarchy is created dynamically
		//
		if(strcmp(ref,"ug.gd.oh") == 0)
		{
			static iString tree = FormObjectTree(iHelpFactory::_AllObjects);
			static iHelpData data = { 0U, 0, "ug.gd.oh", "Object Hierarchy", 0, tree.ToCharPointer() };

			return iHelpDataBuffer(data);
		}

#if defined(I_DEBUG) && defined (I_CHECK_MISSING_REFERENCES)
		if(iString(ref).Contains(".-") == 0)
		{
			IBUG_WARN("Missing reference:"+iString(ref));
		}
#endif
		return iHelpDataBuffer(null);
	}
	else
	{
		if(tmp->Buffer[0]=='&' && tmp->Buffer[1]=='=')
		{
			//
			//  '=' causes immediate replacement of data
			//
			return FindData(tmp->Buffer+2,_All,replaceRefs,level+1);
		}
		else
		{
			if(replaceRefs && tmp->Buffer[0]=='&' && tmp->Buffer[1]==':')
			{
				return FindData(tmp->Buffer+2,_All,replaceRefs,level+1);
			}
			return iHelpDataBuffer(*tmp);
		}
	}
}


void iHelpFactory::RegisterObject(const iString &tag)
{
	mObjectTags.AddUnique(tag);
}


void iHelpFactory::RegisterObjectProperty(const iString &tag)
{
	mPropertyTags.AddUnique(tag);
}


const iString iHelpFactory::FormObjectTree(ObjectType type)
{
	int i, head;
	iString text;
	iArray<iString> tags;

	switch(type)
	{
	case _DataObjects:
		{
			for(i=0; i<mObjectTags.Size(); i++)
			{
				if(mObjectTags[i].Section("+",0,0) == "d")
				{
					head = tags.Size();
					tags.Add(mObjectTags[i]);
				}
				if(mObjectTags[i][0] == '~')
				{
					tags.Add(mObjectTags[i]);
				}
			}
			break;
		}
	case _MainObjects:
		{
			for(i=0; i<mObjectTags.Size(); i++)
			{
				if(mObjectTags[i].Section("+",2,2).IsEmpty()) head = tags.Size();
				if(mObjectTags[i][0]!='~' && mObjectTags[i][0]!='_')
				{
					tags.Add(mObjectTags[i]);
				}
			}
			break;
		}
	default:
		{
			for(i=0; i<mObjectTags.Size(); i++)
			{
				tags.Add(mObjectTags[i]);
				if(mObjectTags[i].Section("+",2,2).IsEmpty()) head = i;
			}
			break;
		}
	}

	if(head < tags.Size())
	{
		TreeDisplayHelper(head,tags,text);
	}

	return text;
}


const iString iHelpFactory::FormReference(const iString &tag, bool bold)
{
	iString tmp, ref = "or." + tag.Section("+",0,0);
	const iHelpDataBuffer &b = iHelpFactory::FindData(ref.ToCharPointer(),iHelpFactory::_ObjectReference,false);
	if(!b.IsNull()) tmp += b.GetExtraTag(_BeginExtraTag);
	tmp += "<li><a href=\"or." + tag.Section("+",0,0) + "\">" + (bold?"<b>":"") + tag.Section("+",1,1).Section("/",0,0) + (bold?"</b>":"") + "</a></li>";
	if(!b.IsNull()) tmp += b.GetExtraTag(_EndExtraTag);
	return tmp;
}


const iString iHelpFactory::FormObjectDescription(const iString &str, bool full)
{
	int i;
	iString tag, ref, text;

	if(str.Contains('+') > 0)
	{
		tag = str; // complete tag
	}
	else
	{
		//
		//  Find our tag: binary search of the ordered array
		//
		int ia, ib, i;
		ia = 0;
		ib = mObjectTags.Size() - 1;
		while(ib > ia+1)
		{
			i = (ia+ib)/2;
			if(mObjectTags[i] < str) ia = i; else ib = i;
		}

		for(i=ia; i<=ib; i++)
		{
			if(mObjectTags[i].BeginsWith(str+"+"))
			{
				tag = mObjectTags[i];
			}
		}
		if(tag.IsEmpty()) return tag; // not found
	}

	iString head = tag.Section("+",0,0) + ".";

	//
	//  Sort properties alphabetically.
	//
	iOrderedArray<iString> arr;
	for(i=0; i<mPropertyTags.Size(); i++) if(mPropertyTags[i].BeginsWith(head))
	{
		arr.Add(mPropertyTags[i].Section("+",1,1));
	}

	ref = "or." + tag.Section("+",0,0);
	const iHelpDataBuffer &b = FindData(ref.ToCharPointer(),_ObjectReference,true);

	text = b.GetExtraTag(_BeginExtraTag);
	text += "<p><a name=\"or." + tag.Section("+",0,0) + "\"><h3>" + tag.Section("+",1,1).Section("/",0,0);
	text += ((tag[0]!='~') ? " object " : " data object ");
	text += b.GetExtraTag(_IconTag) + "</h3><ul>Short form: <b>" + tag.Section("+",1,1).Section("/",1,1) + "</b><p>" + b.GetHTML() + "<p>Available properties:<p><ul>\n";

	if(arr.Size() == 0) text += "(This object has no properties.)";

	int j;
	for(j=0; j<arr.Size(); j++)
	{
		for(i=0; i<mPropertyTags.Size(); i++) if(mPropertyTags[i].BeginsWith(head) && mPropertyTags[i].Section("+",1,1)==arr[j])
		{
			ref = "or." + mPropertyTags[i].Section("+",0,0);
			const iHelpDataBuffer &b = iHelpFactory::FindData(ref.ToCharPointer(),iHelpFactory::_ObjectReference,true);
			iString s = b.GetHTML();
			if(s[0] == '&')
			{
				//
				//  Reference
				//
				const iHelpDataBuffer &b = iHelpFactory::FindData(s.ToCharPointer(2),iHelpFactory::_ObjectReference,true);
#ifdef I_DEBUG
				if(b.GetTitle().IsEmpty())
				{
					IBUG_WARN("Referenced item is missing a title.");
				}
#endif
				s = iString("<br>See <a href=\"") + b.GetTag() + "\">" + b.GetTitle() + "</a>.";
			}
			else
			{
				s = "<li>" + FormPropertyDescription(mPropertyTags[i],true,full) + "</li>";
#ifdef I_DEBUG
				if(s == "<li></li>")
				{
					iString tmp = FormPropertyDescription(mPropertyTags[i],true,full);
				}
#endif
			}

			text += s;
			break;
		}
	}

	text += "</ul>\n";

	iString cds = tag.Section("+",3);
	if(!cds.IsEmpty())
	{
		text += "<br><p>Child objects:<p><ul>\n";
		int i, j, n = cds.Contains(',');
		iOrderedArray<iString> arr;
		for(i=0; i<=n; i++)
		{
			iString s = cds.Section(",",i,i);
			for(j=0; j<mObjectTags.Size(); j++)
			{
				if(mObjectTags[j].Section("+",0,0) == s)
				{
					arr.AddUnique(mObjectTags[j].Section("+",1,1).Section("/",0,0)+"/"+s);
					break;
				}
			}
		}
		for(i=0; i<arr.Size(); i++)
		{
			text += "<li><a href=\"or." + arr[i].Section("/",1,1) + "\"><b>" + arr[i].Section("/",0,0) + "</b></a></li>\n";
		}
	}

	text += "</ul></ul>" + b.GetExtraTag(_EndExtraTag) + "\n";
	return text;
}


const iString iHelpFactory::FormPropertyDescription(const iString &str, bool line, bool full)
{
	iString tag;
	if(str.Contains('+') > 0)
	{
		tag = str; // complete tag
	}
	else
	{
		//
		//  Find our tag: binary search of the ordered array
		//
		int ia, ib, i;
		ia = 0;
		ib = mPropertyTags.Size() - 1;
		while(ib > ia+1)
		{
			i = (ia+ib)/2;
			if(mPropertyTags[i] < str) ia = i; else ib = i;
		}

		for(i=ia; i<=ib; i++)
		{
			if(mPropertyTags[i].BeginsWith(str+"+"))
			{
				tag = mPropertyTags[i];
			}
		}
	}

	iString text, ref;

	bool inherited;
	if(tag.IsEmpty())
	{
		//
		//  Inherited property
		//
		ref = "or." + str;
		tag = str;
		inherited = true;
	}
	else
	{
		ref = "or." + tag.Section("+",0,0);
		inherited = false;
	}
	const iHelpDataBuffer &b(iHelpFactory::FindData(ref.ToCharPointer(),iHelpFactory::_ObjectReference,false));
	if(b.IsNull())
	{
		IBUG_WARN("Missing reference "+ref);
		return text;
	}

	text = b.GetExtraTag(_BeginExtraTag);

	if(inherited || line || !full)
	{
		if(full)
		{
			text += "<a name=\"or.";
		}
		else
		{
			text += "<a href=\"or.";
		}
		text += tag.Section("+",0,0) + "\">";
		text += "<b>" + tag.Section("+",1,1).Section("/",0,0) + "</b></a>";
		if(full && !inherited)
		{
			text += b.GetExtraTag(_IconTag);
			text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(short form: <b>" + tag.Section("+",1,1).Section("/",1,1) + "</b>);&nbsp;&nbsp;<b><tt>" + tag.Section("+",2,2) + tag.Section("+",3) + "</tt></b>\n";
		}
	}
	else
	{
		text += "Long name: <b>" + tag.Section("+",1,1).Section("/",0,0) + "</b><br>";
		text += "Short name: <b>" + tag.Section("+",1,1).Section("/",1,1) + "</b><br>";
		text += "Type: <b>" + tag.Section("+",2,2) + tag.Section("+",3) + "</b><p>";
	}
	
	if(full)
	{
		iString s = b.GetHTML();
		if(s[0] == '&')
		{
			//
			//  Reference
			//
			const iHelpDataBuffer &b = iHelpFactory::FindData(s.ToCharPointer(2),iHelpFactory::_ObjectReference,true);
#ifdef I_DEBUG
			if(b.GetTitle().IsEmpty())
			{
				IBUG_WARN("Referenced item is missing a title.");
			}
#endif
			text += iString("<br>See <a href=\"") + b.GetTag() + "\">" + b.GetTitle() + "</a>.";
		}
		else
		{
			text += "<br>" + s;
		}
	}

	text += b.GetExtraTag(_EndExtraTag);
	return text;
}


#ifdef I_DEBUG
void iHelpFactory::CreateUserGuide()
{
	static const iString afterPartTitle("<p>&nbsp;");
	iString text, stmp;

	//
	//  Create the title page
	//
	stmp = "<html><head><title>Title page</title></head><body><center><img src=\"../images/genie2.png\"><p><img src=\"images/_logoug.png\" alt=\"<h1>IFrIT<br>User Guide</h1>\"><p><h2>by</h2><h1>Nick Gnedin and IFrIT</h1><p>&nbsp;<p>&nbsp;<p>&nbsp;<p><h3>IFrIT Version " + iVersion::GetVersion() + "</center>";

	//
	//  Create preface
	//
	stmp += "<!-- NEW SHEET -->";
	stmp += "<!-- HALF PAGE -->";
	stmp += iHelpFactory::FindData("ug.pf").GetHTML();
	stmp += "</body></html>";
	WriteFile("Work/docs/ugTitle.html",stmp);

	//
	//  Create part on user guide
	//
	text += "<center><a name=\"ug\"><h1 TYPE=\"1\">User Guide</h1></a></center>\n";
	text += afterPartTitle;

	//
	//  Create overview chapter
	//
	CreateChapter(text,ugOverview,_UserGuide);

	//
	//  Create chapter on controls
	//
	CreateChapter(text,ugControls,_UserGuide);

	//
	//  Create chapter on file formats
	//
	CreateChapter(text,ugFormats,_UserGuide);

	//
	//  Create chapter on palettes
	//
	CreateChapter(text,ugPalettes,_UserGuide);

	//
	//  Create chapter on animation support
	//
	CreateChapter(text,ugAnimation,_UserGuide);

	//
	//  Create chapter on script syntax
	//
	//CreateChapter(text,ugScripts,_UserGuide);

	//
	//  Create part on shell reference
	//
	text += "<center><a name=\"sr\"><h1>Shell Reference</h1></a></center>\n";
	text += afterPartTitle;

	//
	//  GUI shell
	//
	CreateChapter(text,ugGGShell,_ShellReference);

	//
	//  Command-line shell
	//
	CreateChapter(text,ugCLShell,_ShellReference);

	//
	//  Create object reference
	//
	text += "<center><a name=\"or\"><h1>Object Reference</h1></a></center>\n";
	text += afterPartTitle;

	//
	//  Intro
	//
	CreateChapter(text,ugObjects,_UserGuide,false);

	//
	//  Include all objects, sorted alphabetically, but place data objects at the end
	//
	int i;
	iOrderedArray<iString> arr;
	for(i=0; i<mObjectTags.Size(); i++) if(!mObjectTags[i].BeginsWith("~"))
	{
		arr.AddUnique(mObjectTags[i].Section("+",1,1));
	}
	int idata = arr.Size();
	for(i=0; i<mObjectTags.Size(); i++) if(mObjectTags[i].BeginsWith("~"))
	{
		arr.AddUnique("~"+mObjectTags[i].Section("+",1,1));
	}

	text += "<p><h2>Regular (Non-Data) Objects</h2><p>\n";

	int j;
	for(i=0; i<arr.Size(); i++)
	{
		if(i == idata)
		{
			text += "<p><h2>Data Objects</h2><p>\n";
		}

		for(j=0; j<mObjectTags.Size(); j++)
		{
			if((!mObjectTags[j].BeginsWith("~") && mObjectTags[j].Section("+",1,1)==arr[i]) || (mObjectTags[j].BeginsWith("~") && ("~"+mObjectTags[j].Section("+",1,1))==arr[i]))
			{
				text += FormObjectDescription(mObjectTags[j],true);
				break;
			}
		}
	}

	//
	//  Create appendices
	//
	text += "<center><a name=\"ap\"><h1 VALUE=\"1\" TYPE=\"A\">Appendices</h1></a></center>\n";

	CreateChapter(text,ugCodes,_UserGuide);
	CreateChapter(text,ugLicense,_UserGuide);

	//
	//  Verify integrity
	//
#if defined(I_DEBUG) && defined (I_CHECK_MISSING_REFERENCES)
	i = -1;
	bool ok = true;
	iString mr, context;
	while((i = text.Find("<a href=\"",i+1)) > -1)
	{
		context = text.Part(i);
		stmp = context.Part(9).Section("\"",0,0);
		if(stmp[2] == '.')  // omit volume references
		{
			if(FindData(stmp.ToCharPointer(),_All,true).IsNull())
			{
				mr += stmp + ",";
				ok = false;
			}
		}
	}
	if(!ok)
	{
		IBUG_WARN("Missing references: "+mr);
	}
#endif

	//
	//  If extra info is included, remove special <extra type=...> </extra> and <pdfonly></pdfonly> tags 
	//
	text.RemoveFormatting("<extra type=",">");
	text.Replace("</extra>","");
	text.Replace("<pdfonly>","");
	text.Replace("</pdfonly>","");

	//
	//  Create the html file
	//
	WriteFile("Work/docs/ug.html","<html><title>&copy; 2005-2012 by Nick Gnedin</title><body>"+text+"</body></html>");

	system("Work\\scripts\\createpdf.csh");
}
#endif


void iHelpFactory::CreateTopicList(iString &text)
{
	text.Clear();

	text = "<center><h3>Available Help Topics</h3></center><p>";

	//
	//  Create overview chapter
	//
	CreateTopicEntry(text,ugOverview,_UserGuide);

	//
	//  Create chapter on controls
	//
	CreateTopicEntry(text,ugControls,_UserGuide);

	//
	//  Create chapter on file formats
	//
	CreateTopicEntry(text,ugFormats,_UserGuide);

	//
	//  Create chapter on palettes
	//
	CreateTopicEntry(text,ugPalettes,_UserGuide);

	//
	//  Create chapter on animation support
	//
	CreateTopicEntry(text,ugAnimation,_UserGuide);

	//
	//  Create chapter on script syntax
	//
	//CreateTopicEntry(text,ugScripts,_UserGuide);

	//
	//  GUI shell
	//
	CreateTopicEntry(text,ugGGShell,_ShellReference);

	//
	//  Create object reference
	//
	//CreateTopicEntry(text,ugObjects,_UserGuide);

	//
	//  Create appendices
	//
	CreateTopicEntry(text,ugCodes,_UserGuide);
	CreateTopicEntry(text,ugLicense,_UserGuide);

	//
	//  Finalize
	//
	InterpretExtraElements(text);
}


//
//  Explicitly configued members
//
void iHelpFactory::InterpretExtraElements(iString &text, const iString &tag)
{
	if(tag.IsEmpty() || tag=="ART")
	{
#if IEXTENSION_INCLUDED(IEXTENSION_ART)
		text.Replace("<extra type=ART>","");
		text.Replace("</extra>","");
#else
		text.RemoveFormatting("<extra type=ART>","</extra>");
#endif
	}
	if(tag.IsEmpty() || tag=="GADGET")
	{
#if IEXTENSION_INCLUDED(IEXTENSION_GADGET)
		text.Replace("<extra type=GADGET>","");
		text.Replace("</extra>","");
#else
		text.RemoveFormatting("<extra type=GADGET>","</extra>");
#endif
	}
	if(tag.IsEmpty() || tag=="VTK")
	{
#if IEXTENSION_INCLUDED(IEXTENSION_VTK)
		text.Replace("<extra type=VTK>","");
		text.Replace("</extra>","");
#else
		text.RemoveFormatting("<extra type=VTK>","</extra>");
#endif
	}
}


const iString& iHelpFactory::GetIconTag(const char *extra)
{
	static const iString none;

#if IEXTENSION_INCLUDED(IEXTENSION_ART)
	static const iString hTag("<img src=\"images::art.png\">");
	if(extra!=0 && ((extra[0]=='-' && strcmp(extra+2,"ART")==0) || strcmp(extra,"ART")==0)) return hTag;
#endif
#if IEXTENSION_INCLUDED(IEXTENSION_GADGET)
	static const iString gTag("<img src=\"images::gadget.png\">");
	if(extra!=0 && ((extra[0]=='-' && strcmp(extra+2,"GADGET")==0) || strcmp(extra,"GADGET")==0)) return gTag;
#endif
#if IEXTENSION_INCLUDED(IEXTENSION_VTK)
	static const iString vtkTag("<img src=\"images::vtk.png\">");
	if(extra!=0 && ((extra[0]=='-' && strcmp(extra+2,"VTK")==0) || strcmp(extra,"VTK")==0)) return vtkTag;
#endif

	return none;
}

