/*=========================================================================

  Program:   Visualization Toolkit
  Language:  C++

  Copyright (c) 1993-2002 Ken Martin, Will Schroeder, Bill Lorensen 
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

//----
//
// Substantially modified by N.Gnedin. The above disclaimer applies in full.
//
//----

#include "ivolumepromapper.h"
#include <vtkRenderer.h>
#include <vtkToolkits.h>

#if defined (I_VTK_HAVE_VP1000)
#include "ivolumeprovp1000mapper.h"
#endif

#include <vtkDebugLeaks.h>
#include <vtkMultiProcessController.h>

#include <vtkObjectFactory.h>

//vtkCxxRevisionMacro(iVolumeProMapper, "$Revision$");

//----------------------------------------------------------------------------
// Needed when we don't use the vtkStandardNewMacro.
//vtkInstantiatorNewMacro(iVolumeProMapper);
//----------------------------------------------------------------------------

// Create the mapper. No context has been created, no volume has
// been created yet.
iVolumeProMapper::iVolumeProMapper()
{
  int               i;

  this->Context = NULL;
  this->Volume = NULL;
  this->VolumeInput = NULL;
  this->VolumeBuildTime = vtkTimeStamp::New();
  this->Lights  = NULL;
  this->NumberOfLights = 0;
  this->BlendMode = VTK_BLEND_MODE_COMPOSITE;
  
  // Disable the subvolume
  for ( i = 0; i < 6; i++ )
    {
    this->SubVolume[i] = -1;
    }

  this->GradientOpacityModulation  = 0;
  this->GradientDiffuseModulation  = 0;
  this->GradientSpecularModulation = 0;

  this->Cursor            = 0;
  this->CursorType        = VTK_CURSOR_TYPE_CROSSHAIR;
  this->CursorPosition[0] = 0.0;
  this->CursorPosition[1] = 0.0;
  this->CursorPosition[2] = 0.0;
  
  this->CursorXAxisColor[0] = 1.0;
  this->CursorXAxisColor[1] = 0.0;
  this->CursorXAxisColor[2] = 0.0;
  
  this->CursorYAxisColor[0] = 0.0;
  this->CursorYAxisColor[1] = 1.0;
  this->CursorYAxisColor[2] = 0.0;
  
  this->CursorZAxisColor[0] = 0.0;
  this->CursorZAxisColor[1] = 0.0;
  this->CursorZAxisColor[2] = 1.0;
  
  this->CutPlane                   = 0;
  this->CutPlaneEquation[0]        = 1.0;
  this->CutPlaneEquation[1]        = 0.0;
  this->CutPlaneEquation[2]        = 0.0;
  this->CutPlaneEquation[3]        = 0.0;
  this->CutPlaneThickness          = 0.0;
  this->CutPlaneFallOffDistance    = 0;

  this->SuperSampling = 0;
  this->SuperSamplingFactor[0] = 1.0;
  this->SuperSamplingFactor[1] = 1.0;
  this->SuperSamplingFactor[2] = 1.0;

  this->NumberOfBoards      = 0;
  this->MajorBoardVersion   = 0;
  this->MinorBoardVersion   = 0;

  this->NoHardware        = 0;
  this->WrongVLIVersion   = 0;
  this->DisplayedMessage  = 0;

  this->Cut = NULL;
  
  this->IntermixIntersectingGeometry = 0;
  //
  //  Parallel processing
  //
  controller = vtkMultiProcessController::New();
  int argc = 0;
  char **argv = 0;
  controller->Initialize(&argc,&argv);
  
  if(controller->IsA("vtkThreadedController"))
    {
      controller->SetNumberOfProcesses(2);
    }
  
}

// Destroy the mapper. Delete the context, volume build time, and the
// volume if necessary
iVolumeProMapper::~iVolumeProMapper()
{
  controller->Finalize();
  controller->Delete();
  this->VolumeBuildTime->Delete();
}

// Simplified version - just assume the mapper type
iVolumeProMapper *iVolumeProMapper::New()
{ 
  // First try to create the object from the vtkObjectFactory
  vtkObject* ret = vtkObjectFactory::CreateInstance("iVolumeProMapper");
  if(ret)
    {
    return static_cast<iVolumeProMapper*>(ret);
    }
  
#if defined (I_VTK_HAVE_VP1000)
  vtkDebugLeaks::DestructClass("iVolumeProMapper");
  return iVolumeProVP1000Mapper::New();
#else
  // if not using vli, then return the stub class, which will render
  // nothing....
  return new iVolumeProMapper;
#endif
}

int iVolumeProMapper::StatusOK()
{
  if ( this->NoHardware )
    {
    if ( !this->DisplayedMessage )
      {
      vtkErrorMacro( << "No Hardware Found!" );
      this->DisplayedMessage = 1;
      }
    return 0;
    }

  if ( this->WrongVLIVersion )
    {
    if ( !this->DisplayedMessage )
      {
      vtkErrorMacro( << "Wrong VLI Version found!" );
      this->DisplayedMessage = 1;
      }
    return 0;
    }

  if ( this->Context == NULL )
    {
    return 0;
    }

  if ( this->LookupTable == NULL )
    {
    return 0;
    }

  if ( this->Cut == NULL )
    {
    return 0;
    }

  return 1;
}

void iVolumeProMapper::SetSuperSamplingFactor( double x, double y, double z )
{
  if ( x < 0.0 || x > 1.0 ||
       y < 0.0 || y > 1.0 ||
       z < 0.0 || z > 1.0  )
    {
    vtkErrorMacro( << "Invalid supersampling factor" << endl <<
      "Each component must be between 0 and 1" );
    return;
    }

  this->SuperSamplingFactor[0] = x;
  this->SuperSamplingFactor[1] = y;
  this->SuperSamplingFactor[2] = z;

  this->Modified();
}

  
int iVolumeProMapper::GetNumberOfProcesses()
{ 
  return controller->GetNumberOfProcesses(); 
}


void iVolumeProMapper::SetNumberOfProcesses(int n)
{
  if(controller->IsA("vtkThreadedController") && n>0)
    {
      controller->SetNumberOfProcesses(n);
    }
}
