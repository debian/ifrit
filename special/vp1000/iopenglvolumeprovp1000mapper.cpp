/*=========================================================================

 Program:   Visualization Toolkit
 Language:  C++

  Copyright (c) 1993-2002 Ken Martin, Will Schroeder, Bill Lorensen 
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.
  
   This software is distributed WITHOUT ANY WARRANTY; without even 
   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
   PURPOSE.  See the above copyright notice for more information.
   
=========================================================================*/

//----
//
// Substantially modified by N.Gnedin. The above disclaimer applies in full.
// 
//----


#include "iglobals.h"
#include "iopengl.h"
#include "iopenglvolumeprovp1000mapper.h"

#include <vtkCamera.h>
#include <vtkObjectFactory.h>
#include <vtkOpenGLRenderWindow.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkTransform.h>
#include <vtkVolume.h>

#include <vtkTimerLog.h>


void reportNullPointer(int);

//vtkCxxRevisionMacro(iOpenGLVolumeProVP1000Mapper, "$Revision$");
//vtkStandardNewMacro(iOpenGLVolumeProVP1000Mapper);

iOpenGLVolumeProVP1000Mapper* iOpenGLVolumeProVP1000Mapper::New()
{
  return new iOpenGLVolumeProVP1000Mapper;
}
//
//  Added by N.Gnedin
//
iOpenGLVolumeProVP1000Mapper::iOpenGLVolumeProVP1000Mapper()
{
	
  textureSize[0] = textureSize[1] = 32;
  textureData = new unsigned int[textureSize[0]*textureSize[1]];
  if(textureData == 0) reportNullPointer(2899);
	
  mode = 0;
	
}


iOpenGLVolumeProVP1000Mapper::~iOpenGLVolumeProVP1000Mapper() 
{
  delete [] textureData;
}


void iOpenGLVolumeProVP1000Mapper::RenderImageBuffer(vtkRenderer *ren, vtkVolume *vol, int xlow, int ylow, int width, int height, unsigned int *outData)
{
  vtkFloat depthVal, nearestPt[3], testZ, minZ;
  vtkFloat planeCoords[4][4];
  float tCoords[4][2];
  int i, j, k;
  int newTextureSize[2];
  vtkFloat bounds[6];
  
  if(mode == 0)
    {
      ren->GetRenderWindow()->SetRGBACharPixelData(xlow,ylow,xlow+width-1,ylow+height-1,(unsigned char *)outData,!ren->GetRenderWindow()->GetDoubleBuffer(),1);
      return;
    }
  // else
	
  newTextureSize[0] = newTextureSize[1] = 32;
  while (newTextureSize[0] < width)
    {
      newTextureSize[0] *= 2;
    }
  while (newTextureSize[1] < height)
    {
      newTextureSize[1] *= 2;
    }
  //
  //  Check whether we need to recreate textureData
  //
  if(newTextureSize[0]!=textureSize[0] || newTextureSize[1]!=textureSize[1])
    {
      textureSize[0] = newTextureSize[0];
      textureSize[1] = newTextureSize[1];
      delete [] textureData;
      textureData = new unsigned int[textureSize[0]*textureSize[1]];
    }
  
  int sz1 = 4*width;
  // int sz2 = 4*(textureSize[0]-width);
  // int sz3 = 4*textureSize[0];
  //
  //  In fact, filling texture outside of image size with zeros is not
  //  necessary, since these portions are not displayed anyway. So, these
  //  are commented out.
  //
  for (j = 0; j < height; j++)
    {
      memcpy((void *)(textureData+j*textureSize[0]),(const void *)(outData+j*width),sz1);
      //      memset((void *)(textureData+j*textureSize[0]+width),0,sz2);
    }
  for (j = height; j < textureSize[1]; j++)
    {
      //      memset((void *)(textureData+j*textureSize[0]),0,sz3);
    }
  
  if ( ! this->IntermixIntersectingGeometry )
    {
      ren->SetWorldPoint(vol->GetCenter()[0],
			 vol->GetCenter()[1],
			 vol->GetCenter()[2],
			 1.0);
    }
  else
    {
      minZ = 1;
      vol->GetBounds(bounds);
      
      for (k = 0; k < 2; k++)
	{
	  for (j = 0; j < 2; j++)
	    {
	      for (i = 0; i < 2; i++)
		{
		  ren->SetWorldPoint(bounds[i+0], bounds[j+2], bounds[k+4], 1.0);
		  ren->WorldToDisplay();
		  testZ = ren->GetDisplayPoint()[2];
		  if (testZ < minZ)
		    {
		      minZ = testZ;
		      nearestPt[0] = bounds[i+0];
		      nearestPt[1] = bounds[j+2];
		      nearestPt[2] = bounds[k+4];
		    }
		}
	    }
	}
      ren->SetWorldPoint(nearestPt[0], nearestPt[1], nearestPt[2], 1.0);
    }
  
  ren->WorldToView();
  depthVal = ren->GetViewPoint()[2];
  
  vtkFloat aspect[2];
  ren->GetAspect(aspect);
  
  ren->SetViewPoint(-aspect[0], -aspect[1], depthVal);
  ren->ViewToWorld();
  ren->GetWorldPoint(planeCoords[0]);
  
  ren->SetViewPoint(aspect[0], -aspect[1], depthVal);
  ren->ViewToWorld();
  ren->GetWorldPoint(planeCoords[1]);
  
  ren->SetViewPoint(aspect[0], aspect[1], depthVal);
  ren->ViewToWorld();
  ren->GetWorldPoint(planeCoords[2]);
  
  ren->SetViewPoint(-aspect[0], aspect[1], depthVal);
  ren->ViewToWorld();
  ren->GetWorldPoint(planeCoords[3]);
  
  // OpenGL stuff
  glDisable( GL_LIGHTING );
  
  glEnable( GL_TEXTURE_2D );
  glDepthMask( 0 );
  glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  
  // Specify the texture
  glColor3f(1.0,1.0,1.0);
#ifdef GL_VERSION_1_1
  glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA8,
		textureSize[0], textureSize[1],
		0, GL_RGBA, GL_UNSIGNED_BYTE, textureData );
#else
  glTexImage2D( GL_TEXTURE_2D, 0, 4,
		textureSize[0], textureSize[1],
		0, GL_RGBA, GL_UNSIGNED_BYTE, textureData );
#endif 
  
  tCoords[0][0] = 0.0;
  tCoords[0][1] = 0.0;
  tCoords[1][0] = (float)width/(float)textureSize[0];
  tCoords[1][1] = 0.0;
  tCoords[2][0] = (float)width/(float)textureSize[0];
  tCoords[2][1] = (float)height/(float)textureSize[1];
  tCoords[3][0] = 0.0;
  tCoords[3][1] = (float)height/(float)textureSize[1];
  
  glBegin( GL_POLYGON );
  for (i = 0; i < 4; i++)
    {
      glTexCoord2fv(tCoords[i]);
#ifdef VTKFLOAT_IS_FLOAT
      glVertex3fv(planeCoords[i]);
#else
      glVertex3dv(planeCoords[i]);
#endif
    }
  glEnd();
  glDisable( GL_TEXTURE_2D);
  glDepthMask( 1 );
  glEnable( GL_LIGHTING );
  
  glFlush();
  
}


void iOpenGLVolumeProVP1000Mapper::GetDepthBufferValues(vtkRenderer *ren, int xlow, int ylow, int width, int height, unsigned int *outData)
{
  //
  //  Copied from vtkOpenGLRenderWindow::GetZbufferData
  //
  // set the current window 
  static_cast<vtkOpenGLRenderWindow*>(ren->GetRenderWindow())->MakeCurrent();

  // Error checking
  // Must clear previous errors first.
  while(glGetError() != GL_NO_ERROR);

  // Turn of texturing in case it is on - some drivers have a problem
  // getting / setting pixels with texturing enabled.
  glDisable(GL_TEXTURE_2D);
  glDisable(GL_SCISSOR_TEST);
  //  glPixelStorei(GL_PACK_ALIGNMENT,1);

  glReadPixels(xlow,ylow,width,height,GL_DEPTH_COMPONENT,GL_FLOAT,outData);

  if (glGetError() != GL_NO_ERROR)
    {
      vtkErrorMacro("could not get Z buffer data");
      return;
    }

}


void iOpenGLVolumeProVP1000Mapper::RescaleDepthBufferValues(vtkRenderer *ren, int xlow, int ylow, int width, int height, unsigned int *outData)
{
  int i, length, rescale;
  
  length = width*height;
  rescale = 16777215; // 2^24 - 1
	
  float *zData = (float *)outData;

  for (i = 0; i < length; i++)
    {
      outData[i] = (unsigned int)(zData[i] * rescale);
    }

}


void iOpenGLVolumeProVP1000Mapper::RenderBoundingBox(vtkRenderer *ren, vtkVolume *vol)
{
  vtkFloat background[3], bounds[6];
  float color[3];
  ren->GetBackground(background);
  if (background[0] > 0.5 && background[1] > 0.5 && background[2] > 0.5)
    {
      // black
      color[0] = color[1] = color[2] = 0.0;
    }
  else
    {
      // white
      color[0] = color[1] = color[2] = 1.0;
    }
  
  vol->GetBounds(bounds);
  
  glColor3fv(color);
  glDisable( GL_LIGHTING );
  
  glBegin( GL_LINE_LOOP );
#ifdef VTKFLOAT_IS_FLOAT
  glVertex3f(bounds[0], bounds[2], bounds[4]);
  glVertex3f(bounds[1], bounds[2], bounds[4]);
  glVertex3f(bounds[1], bounds[2], bounds[5]);
  glVertex3f(bounds[0], bounds[2], bounds[5]);
#else
  glVertex3d(bounds[0], bounds[2], bounds[4]);
  glVertex3d(bounds[1], bounds[2], bounds[4]);
  glVertex3d(bounds[1], bounds[2], bounds[5]);
  glVertex3d(bounds[0], bounds[2], bounds[5]);
#endif
  glEnd();
  glBegin( GL_LINE_LOOP );
#ifdef VTKFLOAT_IS_FLOAT
  glVertex3f(bounds[0], bounds[3], bounds[4]);
  glVertex3f(bounds[1], bounds[3], bounds[4]);
  glVertex3f(bounds[1], bounds[3], bounds[5]);
  glVertex3f(bounds[0], bounds[3], bounds[5]);
#else
  glVertex3d(bounds[0], bounds[3], bounds[4]);
  glVertex3d(bounds[1], bounds[3], bounds[4]);
  glVertex3d(bounds[1], bounds[3], bounds[5]);
  glVertex3d(bounds[0], bounds[3], bounds[5]);
#endif
  glEnd();
  glBegin( GL_LINES );
#ifdef VTKFLOAT_IS_FLOAT
  glVertex3f(bounds[0], bounds[2], bounds[4]);
  glVertex3f(bounds[0], bounds[3], bounds[4]);
  glVertex3f(bounds[1], bounds[2], bounds[4]);
  glVertex3f(bounds[1], bounds[3], bounds[4]);
  glVertex3f(bounds[1], bounds[2], bounds[5]);
  glVertex3f(bounds[1], bounds[3], bounds[5]);
  glVertex3f(bounds[0], bounds[2], bounds[5]);
  glVertex3f(bounds[0], bounds[3], bounds[5]);
#else
  glVertex3d(bounds[0], bounds[2], bounds[4]);
  glVertex3d(bounds[0], bounds[3], bounds[4]);
  glVertex3d(bounds[1], bounds[2], bounds[4]);
  glVertex3d(bounds[1], bounds[3], bounds[4]);
  glVertex3d(bounds[1], bounds[2], bounds[5]);
  glVertex3d(bounds[1], bounds[3], bounds[5]);
  glVertex3d(bounds[0], bounds[2], bounds[5]);
  glVertex3d(bounds[0], bounds[3], bounds[5]);
#endif
  glEnd();
  
  glEnable( GL_LIGHTING );
  glFlush();

}
