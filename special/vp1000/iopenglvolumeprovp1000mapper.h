/*=========================================================================

 Program:   Visualization Toolkit
 Language:  C++
 
  Copyright (c) 1993-2002 Ken Martin, Will Schroeder, Bill Lorensen 
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.
  
   This software is distributed WITHOUT ANY WARRANTY; without even 
   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
   PURPOSE.  See the above copyright notice for more information.
   
=========================================================================*/

//----
//
// Substantially modified by N.Gnedin. The above disclaimer applies in full.
// 
//----


// .NAME iOpenGLVolumeProVP1000Mapper - Concrete class for VolumePRO mapper
//
// .SECTION Description
// iOpenGLVolumeProVP1000Mapper is the concrete implementation of a 
// iVolumeProMapper based on the VP1000 chip running with OpenGL. 
// Users should not create this class directly - a iVolumeProMapper will 
// automatically create the object of the right type.
//
// This class is not included in the Rendering CMakeLists by default. If you
// want to add this class to your vtk build, you need to have the vli header
// and library files.  Please see the iVolumeProVP1000Mapper.h file for
// instructions on how to use the vli library with vtk.
//
// For more information on the VolumePRO hardware, please see:
//
//   http://www.terarecon.com/3d_products.shtml
//
// If you encounter any problems with this class, please inform Kitware, Inc.
// at kitware@kitware.com.
//
//
// .SECTION See Also
// vtkVolumeMapper iVolumeProMapper iVolumeProVP1000Mapper
//

#ifndef __iOpenGLVolumeProVP1000Mapper_h
#define __iOpenGLVolumeProVP1000Mapper_h

#include "ivolumeprovp1000mapper.h"

class iOpenGLVolumeProVP1000Mapper : public iVolumeProVP1000Mapper
{
  
 public:
  //  vtkTypeRevisionMacro(iOpenGLVolumeProVP1000Mapper,iVolumeProVP1000Mapper);
  static iOpenGLVolumeProVP1000Mapper *New();
	
 protected:

  iOpenGLVolumeProVP1000Mapper();
  ~iOpenGLVolumeProVP1000Mapper();
	
  // Render the hexagon returned by the hardware to the screen.
  void RenderImageBuffer(vtkRenderer *ren, vtkVolume *vol, int xlow, int ylow, int width, int height, unsigned int *outData );
	
  // Get the OpenGL depth buffer values in a the form needed for the
  // VolumePro board
  virtual void GetDepthBufferValues(vtkRenderer *ren, int xlow, int ylow, int width, int height, unsigned int *outData);
  virtual void RescaleDepthBufferValues(vtkRenderer *ren, int xlow, int ylow, int width, int height, unsigned int *outData);

  // Render a bounding box of the volume because the texture map would be
  // too large
  virtual void RenderBoundingBox(vtkRenderer *ren, vtkVolume *vol);
	
  int textureSize[2];
  unsigned int *textureData;
	
  int mode;
	
 private:
	
  iOpenGLVolumeProVP1000Mapper(const iOpenGLVolumeProVP1000Mapper&); // Not implemented
  void operator=(const iOpenGLVolumeProVP1000Mapper&); // Not implemented

};


#endif



